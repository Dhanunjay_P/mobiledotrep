﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.Utilities;
using System.Data;
using System.Configuration;

namespace BLL.ExtraUtilities
{
    public class ClientUtility : ServerBase
    {
        public void SetDatabaseConnectionString(ServerBase objServerBase, String database)
        {
            IDbConnection connection = DBObjectFactory.GetConnectionObject();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["DeGrToolWorkingConnectionString"].ConnectionString;
            if (connection.Database == database)
                objServerBase.DBConnection.ConnectionString = ConfigurationManager.ConnectionStrings["DeGrToolWorkingConnectionString"].ConnectionString;
            else
                objServerBase.DBConnection.ConnectionString = ConfigurationManager.ConnectionStrings["DeGrToolLiveConnectionString"].ConnectionString;
            connection = null;
        }

        public DataTable GetTableSchema(ref IDbDataAdapter adapter, String TableName, ref String Message)
        {
            try
            {
                adapter.SelectCommand.Parameters.Clear();
                StringBuilder SQLSelect = new StringBuilder();
                SQLSelect.Append(" SELECT   * ");
                SQLSelect.Append(" FROM     " + TableName);
                SQLSelect.Append(" WHERE    1<>1 ");
                adapter.SelectCommand.CommandText = SQLSelect.ToString();
                adapter.TableMappings.Clear();
                adapter.TableMappings.Add("Table", TableName);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                if (ds.Tables.Count > 0)
                {
                    Message = TableName + " structure retrieved successfully.";
                    return ds.Tables[0];
                }
                else
                {
                    Message = "Unable to get " + TableName + " structure.";
                    return ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                ServerLog.ExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
        }

        public DataTable GetTableData(ref IDbDataAdapter adapter, String TableName, String TopClause, String WhereCriteria, ref String Message)
        {
            try
            {
                adapter.SelectCommand.Parameters.Clear();
                StringBuilder SQLSelect = new StringBuilder();
                SQLSelect.Append(" SELECT   " + (TopClause != null && TopClause.Trim() != String.Empty ? TopClause : String.Empty) + " * ");
                SQLSelect.Append(" FROM     " + TableName + " ");
                if (WhereCriteria != null && WhereCriteria.Trim() != String.Empty)
                    SQLSelect.Append(WhereCriteria);
                adapter.SelectCommand.CommandText = SQLSelect.ToString();
                adapter.TableMappings.Clear();
                adapter.TableMappings.Add("Table", TableName);
                DataSet ds = new DataSet();
                adapter.Fill(ds);

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    Message = TableName + " data retrieve successfully.";
                    return ds.Tables[0];
                }
                else
                {
                    Message = "There is no data exists in " + TableName;
                    return ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                ServerLog.ExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
        }        
    }
}
