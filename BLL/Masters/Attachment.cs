﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.Utilities;

namespace BLL.Masters
{
    public class Attachment : ServerBase
    {
        public String GetNextAttachmentNo(ref String Message)
        {
            Document objDocument = new Document();
            String AttachmentNo = null;
            if (DBConnection.State == System.Data.ConnectionState.Closed)
                DBConnection.Open();
            DBCommand.Transaction = DBConnection.BeginTransaction();
            Boolean result = objDocument.GetNextDocumentNo(ref DBCommand, "Attach", "Admin", "10.0.0.0", ref AttachmentNo, ref Message);
            if (result)
            {
                DBCommand.Transaction.Commit();
                return AttachmentNo;
            }
            else
            {
                DBCommand.Transaction.Rollback();
                return null;
            }
        }
    }
}
