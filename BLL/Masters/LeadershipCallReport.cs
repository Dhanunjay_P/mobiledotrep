﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.Utilities;
using System.Configuration;
using System.Data;

namespace BLL.Masters
{
    public class LeadershipCallReport : ServerBase
    {
        public LeadershipCallReport()
        {
            DBConnection = DBObjectFactory.GetConnectionObject();
            DBConnection.ConnectionString = ConfigurationManager.ConnectionStrings["DeGrToolLiveConnectionString"].ToString();
            DBCommand = DBObjectFactory.GetCommandObject();
            DBCommand.Connection = DBConnection;
            DBDataAdpterObject = DBObjectFactory.GetDataAdapterObject(DBCommand);
            DBDataAdpterObject.SelectCommand.CommandTimeout = 60;
        }

        public override SendReceiveJSon ProcessRequest(SendReceiveJSon receive_obj)
        {
            switch (receive_obj.requestObjectInfo.ObjectProfile.MethodName)
            {
                case "GetReportData":
                    return GetReportData(receive_obj);
                default:
                    SendReceiveJSon send_obj = new SendReceiveJSon();
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "Called method not found " + receive_obj.requestObjectInfo.ObjectProfile.MethodName + ". Please contact to administrator.";
                    return send_obj;
            }
        }

        private SendReceiveJSon GetReportData(SendReceiveJSon receive_obj)
        {
            SendReceiveJSon send_obj = new SendReceiveJSon();
            try
            {
                String Message = String.Empty;
                DataTable dtLeadershipCall = GetLeadershipCallData(ref Message);
                if (dtLeadershipCall != null && dtLeadershipCall.Rows.Count > 0)
                {
                    send_obj.responseObjectInfo.Status = 1;
                    send_obj.responseObjectInfo.Message = Message;
                    send_obj.responseObjectInfo.dt_ReturnedTables = new dynamic[1];
                    send_obj.responseObjectInfo.dt_ReturnedTables[0] = dtLeadershipCall;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                }
                else
                {
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = Message;
                    send_obj.responseObjectInfo.dt_ReturnedTables = null;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                }
            }
            catch (Exception ex)
            {
                ServerLog.ExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                send_obj.responseObjectInfo.Status = 2;
                send_obj.responseObjectInfo.Message = ex.Message;
                send_obj.responseObjectInfo.dt_ReturnedTables = null;
            }

            return send_obj;
        }

        public DataTable GetLeadershipCallData(ref String Message)
        {
            try
            {
                String SQLSelect = @" SELECT     sp_dt_AccountMst.SPORID, sp_dt_AccountMst.CustomerName, sp_dt_AccountMst.Address, sp_dt_AccountMst.City, sp_dt_AccountMst.ST AS State, 
                                                 sp_dt_HCO_LshpCalls.callDate, sp_dt_HCO_LshpCalls.calledBy
                                      FROM       sp_dt_HCO_LshpCalls 
                                      INNER JOIN sp_dt_AccountMst ON sp_dt_HCO_LshpCalls.SPORID = sp_dt_AccountMst.SPORID
                                      WHERE      (sp_dt_HCO_LshpCalls.callDate IS NOT NULL) AND (sp_dt_HCO_LshpCalls.calledBy IS NOT NULL) 
                                      AND        (sp_dt_HCO_LshpCalls.callDate <> '-') AND (sp_dt_HCO_LshpCalls.calledBy <> '-') 
                                      AND        (LTRIM(RTRIM(sp_dt_HCO_LshpCalls.callDate)) <> '') AND (LTRIM(RTRIM(sp_dt_HCO_LshpCalls.calledBy)) <> '') 
                                      ORDER BY   sp_dt_AccountMst.CustomerName";
                DBDataAdpterObject.SelectCommand.Parameters.Clear();
                DBDataAdpterObject.SelectCommand.CommandText = SQLSelect;
                DBDataAdpterObject.TableMappings.Clear();
                DBDataAdpterObject.TableMappings.Add("Table", "LeadershipCallReport");
                DataSet ds = new DataSet();
                DBDataAdpterObject.Fill(ds);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    Message = "Leadership Call data retrieved successfully.";
                    return ds.Tables[0];
                }
                else
                {
                    Message = "Leadership Call data not found.";
                    return null;
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                ServerLog.ExceptionLog(ex.Message.ToString() + Environment.NewLine + ex.StackTrace);
                return null;
            }
        }
    }
}
