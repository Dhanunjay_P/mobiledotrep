﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.Utilities;
using System.Data;
using System.Configuration;
using Newtonsoft.Json;
using BLL.ExtraUtilities;

namespace BLL.Masters
{
    public class ProfilerWebConfigTool : ServerBase
    {
        public override SendReceiveJSon ProcessRequest(SendReceiveJSon receive_obj)
        {
            switch (receive_obj.requestObjectInfo.ObjectProfile.MethodName)
            {
                case "GetInitialData":
                    return GetInitialData(receive_obj);
                case "GetAccountTypeAndCategoryAndAttributeList":
                    return GetAccountTypeAndCategoryAndAttributeList(receive_obj);
                case "GetAccountTypeDetails":
                    return GetAccountTypeDetails(receive_obj);
                case "SaveAccountTypeData":
                    return SaveAccountTypeData(receive_obj);
                case "GetCategoryDetails":
                    return GetCategoryDetails(receive_obj);
                case "SaveCategoryData":
                    return SaveCategoryData(receive_obj);
                case "GetAttributeDetails":
                    return GetAttributeDetails(receive_obj);
                case "SaveAttributeData":
                    return SaveAttributeData(receive_obj);
                case "GetColumnList":
                    return GetColumnList(receive_obj);
                default:
                    SendReceiveJSon send_obj = new SendReceiveJSon();
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "Called method not found " + receive_obj.requestObjectInfo.ObjectProfile.MethodName + ". Please contact to administrator.";
                    return send_obj;
            }
        }

        private SendReceiveJSon GetInitialData(SendReceiveJSon receive_obj)
        {
            SendReceiveJSon send_obj = new SendReceiveJSon();
            String Message = String.Empty;

            try
            {
                IDbConnection connection = DBObjectFactory.GetConnectionObject();
                DataTable dtDatabase = new DataTable("tDatabase");
                dtDatabase.Columns.Add("code");
                dtDatabase.Columns.Add("desc");
                connection.ConnectionString = ConfigurationManager.ConnectionStrings["DeGrToolWorkingConnectionString"].ConnectionString;
                dtDatabase.Rows.Add(new Object[] { connection.Database, connection.Database });
                connection.ConnectionString = ConfigurationManager.ConnectionStrings["DeGrToolLiveConnectionString"].ConnectionString;
                dtDatabase.Rows.Add(new Object[] { connection.Database, connection.Database });
                connection = null;
                if (dtDatabase != null && dtDatabase.Rows.Count > 0)
                {
                    send_obj.responseObjectInfo.Status = 1;
                    send_obj.responseObjectInfo.Message = "Initial data retrieved successfully.";
                    send_obj.responseObjectInfo.dt_ReturnedTables = new dynamic[1];
                    send_obj.responseObjectInfo.dt_ReturnedTables[0] = dtDatabase;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                }
                else
                {
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "Initial data not found.";
                    send_obj.responseObjectInfo.dt_ReturnedTables = null;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                }
            }
            catch (Exception ex)
            {
                ServerLog.ExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                send_obj.responseObjectInfo.Status = 2;
                send_obj.responseObjectInfo.Message = ex.Message;
                send_obj.responseObjectInfo.dt_ReturnedTables = null;
            }

            return send_obj;
        }

        private SendReceiveJSon GetAccountTypeAndCategoryAndAttributeList(SendReceiveJSon receive_obj)
        {
            SendReceiveJSon send_obj = new SendReceiveJSon();
            String Message = String.Empty;

            try
            {
                MgmtSales objMgmtSales = new MgmtSales();
                String database = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("database") && receive_obj.requestObjectInfo.OpArgs["database"] != null && receive_obj.requestObjectInfo.OpArgs["database"].ToString().Trim() != String.Empty)
                    database = receive_obj.requestObjectInfo.OpArgs["database"].ToString().Trim();

                ClientUtility objClientUtility = new ClientUtility();
                objClientUtility.SetDatabaseConnectionString(objMgmtSales, database);

                DataTable dtAccountTypeList = objMgmtSales.GetAccountTypeMst(null, ref Message);
                if (dtAccountTypeList == null || dtAccountTypeList.Rows.Count <= 0)
                {
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "AccountType list data not found." + Environment.NewLine + Message;
                    send_obj.responseObjectInfo.dt_ReturnedTables = null;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                    return send_obj;
                }

                DataTable dtCategoryList = objMgmtSales.GetCategoryMst(null, null, ref Message);
                if (dtCategoryList == null || dtCategoryList.Rows.Count <= 0)
                {
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "Category List data not found." + Environment.NewLine + Message;
                    send_obj.responseObjectInfo.dt_ReturnedTables = null;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                    return send_obj;
                }

                DataTable dtAttributeList = objMgmtSales.GetAttributeCategoryMap(null, null, null, ref Message);
                if (dtAttributeList == null || dtAttributeList.Rows.Count <= 0)
                {
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "Attribute List data not found." + Environment.NewLine + Message;
                    send_obj.responseObjectInfo.dt_ReturnedTables = null;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                    return send_obj;
                }

                send_obj.responseObjectInfo.Status = 1;
                send_obj.responseObjectInfo.Message = "AccountType & Category & Attribute List retrieved successfully.";
                send_obj.responseObjectInfo.dt_ReturnedTables = new dynamic[3];
                send_obj.responseObjectInfo.dt_ReturnedTables[0] = dtAccountTypeList;
                send_obj.responseObjectInfo.dt_ReturnedTables[1] = dtCategoryList;
                send_obj.responseObjectInfo.dt_ReturnedTables[2] = dtAttributeList;
                send_obj.responseObjectInfo.ObjRetArgs = null;
                return send_obj;
            }
            catch (Exception ex)
            {
                ServerLog.ExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                send_obj.responseObjectInfo.Status = 2;
                send_obj.responseObjectInfo.Message = ex.Message;
                send_obj.responseObjectInfo.dt_ReturnedTables = null;
                return send_obj;
            }            
        }

        private SendReceiveJSon GetAccountTypeDetails(SendReceiveJSon receive_obj)
        {
            SendReceiveJSon send_obj = new SendReceiveJSon();
            String Message = String.Empty;

            try
            {
                MgmtSales objMgmtSales = new MgmtSales();
                String database = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("database") && receive_obj.requestObjectInfo.OpArgs["database"] != null && receive_obj.requestObjectInfo.OpArgs["database"].ToString().Trim() != String.Empty)
                    database = receive_obj.requestObjectInfo.OpArgs["database"].ToString().Trim();

                String account_type = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("account_type") && receive_obj.requestObjectInfo.OpArgs["account_type"] != null && receive_obj.requestObjectInfo.OpArgs["account_type"].ToString().Trim() != String.Empty && receive_obj.requestObjectInfo.OpArgs["account_type"].ToString().Trim().ToUpper() != "ALL")
                    account_type = "'" + receive_obj.requestObjectInfo.OpArgs["account_type"].ToString().Trim() + "'";

                ClientUtility objClientUtility = new ClientUtility();
                objClientUtility.SetDatabaseConnectionString(objMgmtSales, database);

                DataTable dtAccountTypeList = objMgmtSales.GetAccountTypeMst(account_type, ref Message);
                if (dtAccountTypeList == null || dtAccountTypeList.Rows.Count <= 0)
                {
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "Account Type detail data not found." + Environment.NewLine + Message;
                    send_obj.responseObjectInfo.dt_ReturnedTables = null;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                    return send_obj;
                }

                send_obj.responseObjectInfo.Status = 1;
                send_obj.responseObjectInfo.Message = "Account Type detail data retrieved successfully.";
                send_obj.responseObjectInfo.dt_ReturnedTables = new dynamic[1];
                send_obj.responseObjectInfo.dt_ReturnedTables[0] = dtAccountTypeList;                
                send_obj.responseObjectInfo.ObjRetArgs = null;
                return send_obj;
            }
            catch (Exception ex)
            {
                ServerLog.ExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                send_obj.responseObjectInfo.Status = 2;
                send_obj.responseObjectInfo.Message = ex.Message;
                send_obj.responseObjectInfo.dt_ReturnedTables = null;
                return send_obj;
            }            
        }

        private SendReceiveJSon SaveAccountTypeData(SendReceiveJSon receive_obj)
        {
            SendReceiveJSon send_obj = new SendReceiveJSon();
            String Message = String.Empty;

            try
            {
                Dictionary<string, object> dicRecieveData = receive_obj.requestObjectInfo.ds_UploadData;
                String jsonTypedAccountTypeMst = null;
                DataSet dS_Config_AccountTypeMstClient = null;
                if (dicRecieveData.ContainsKey("New"))
                {
                    jsonTypedAccountTypeMst = JsonConvert.SerializeObject(dicRecieveData["New"]);
                    dS_Config_AccountTypeMstClient = JsonConvert.DeserializeObject<DataSet>(jsonTypedAccountTypeMst);
                }

                #region Primary Validation
                if (dS_Config_AccountTypeMstClient == null || dS_Config_AccountTypeMstClient.Tables.Count <= 0 || dS_Config_AccountTypeMstClient.Tables["Config_AccountTypeMst"] == null || dS_Config_AccountTypeMstClient.Tables["Config_AccountTypeMst"].Rows.Count <= 0)
                {
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "No data available to Save Account Type Master. Operation cancelled.";
                    return send_obj;
                }
                #endregion

                String database = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("database") && receive_obj.requestObjectInfo.OpArgs["database"] != null && receive_obj.requestObjectInfo.OpArgs["database"].ToString().Trim() != String.Empty)
                    database = receive_obj.requestObjectInfo.OpArgs["database"].ToString().Trim();

                String NewEdit = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("NewEdit") && receive_obj.requestObjectInfo.OpArgs["NewEdit"] != null && receive_obj.requestObjectInfo.OpArgs["NewEdit"].ToString().Trim() != String.Empty)
                    NewEdit = receive_obj.requestObjectInfo.OpArgs["NewEdit"].ToString().Trim();

                ClientUtility objClientUtility = new ClientUtility();
                objClientUtility.SetDatabaseConnectionString(this, database);
                if (DBConnection.State == ConnectionState.Open)
                    DBConnection.Close();
                DBConnection.Open();
                DBCommand.Transaction = DBConnection.BeginTransaction();

                DataTable dtConfig_AccountTypeMstClient = dS_Config_AccountTypeMstClient.Tables["Config_AccountTypeMst"];
                DataTable dtConfig_AccountTypeMstServer = objClientUtility.GetTableSchema(ref DBDataAdpterObject, dtConfig_AccountTypeMstClient.TableName, ref Message);
                dtConfig_AccountTypeMstServer.PrimaryKey = new DataColumn[] { dtConfig_AccountTypeMstServer.Columns["AccountType"] };
                dtConfig_AccountTypeMstServer.ImportRow(dtConfig_AccountTypeMstClient.Rows[0]);
                dtConfig_AccountTypeMstServer.AcceptChanges();
                if (NewEdit == "New")
                    dtConfig_AccountTypeMstServer.Rows[0].SetAdded();
                else if (NewEdit == "Edit")
                    dtConfig_AccountTypeMstServer.Rows[0].SetModified();

                BLGeneralUtil.UpdateTableInfo utf = BLGeneralUtil.UpdateTable(ref DBCommand, dtConfig_AccountTypeMstServer, BLGeneralUtil.UpdateWhereMode.KeyColumnsOnly);
                if (!utf.Status || utf.TotalRowsAffected != dtConfig_AccountTypeMstServer.Rows.Count)
                {
                    DBCommand.Transaction.Rollback();
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "An error occurred while Save Account Type data. " + utf.ErrorMessage;
                    send_obj.responseObjectInfo.dt_ReturnedTables = null;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                }
                else
                {
                    DBCommand.Transaction.Commit();
                    MgmtSales objMgmtSales = new MgmtSales();
                    objClientUtility.SetDatabaseConnectionString(objMgmtSales, database);
                    DataTable dtAccountTypeList = objMgmtSales.GetAccountTypeMst(null, ref Message);
                    send_obj.responseObjectInfo.Status = 1;
                    send_obj.responseObjectInfo.Message = "Account Type data saved successfully.";
                    send_obj.responseObjectInfo.dt_ReturnedTables = new dynamic[1];
                    send_obj.responseObjectInfo.dt_ReturnedTables[0] = dtAccountTypeList;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                }
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
            catch (Exception ex)
            {
                ServerLog.ExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                send_obj.responseObjectInfo.Status = 2;
                send_obj.responseObjectInfo.Message = ex.Message;
                send_obj.responseObjectInfo.dt_ReturnedTables = null;
            }

            return send_obj;
        }

        private SendReceiveJSon GetCategoryDetails(SendReceiveJSon receive_obj)
        {
            SendReceiveJSon send_obj = new SendReceiveJSon();
            String Message = String.Empty;

            try
            {
                MgmtSales objMgmtSales = new MgmtSales();
                String database = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("database") && receive_obj.requestObjectInfo.OpArgs["database"] != null && receive_obj.requestObjectInfo.OpArgs["database"].ToString().Trim() != String.Empty)
                    database = receive_obj.requestObjectInfo.OpArgs["database"].ToString().Trim();

                String account_type = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("account_type") && receive_obj.requestObjectInfo.OpArgs["account_type"] != null && receive_obj.requestObjectInfo.OpArgs["account_type"].ToString().Trim() != String.Empty && receive_obj.requestObjectInfo.OpArgs["account_type"].ToString().Trim().ToUpper() != "ALL")
                    account_type = "'" + receive_obj.requestObjectInfo.OpArgs["account_type"].ToString().Trim() + "'";

                String CategoryCode = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("CategoryCode") && receive_obj.requestObjectInfo.OpArgs["CategoryCode"] != null && receive_obj.requestObjectInfo.OpArgs["CategoryCode"].ToString().Trim() != String.Empty && receive_obj.requestObjectInfo.OpArgs["CategoryCode"].ToString().Trim().ToUpper() != "ALL")
                    CategoryCode = receive_obj.requestObjectInfo.OpArgs["CategoryCode"].ToString().Trim();

                ClientUtility objClientUtility = new ClientUtility();
                objClientUtility.SetDatabaseConnectionString(objMgmtSales, database);

                DataTable dtCategoryDetail = objMgmtSales.GetCategoryMst(account_type, CategoryCode, ref Message);
                if (dtCategoryDetail == null || dtCategoryDetail.Rows.Count <= 0)
                {
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "Category detail data not found." + Environment.NewLine + Message;
                    send_obj.responseObjectInfo.dt_ReturnedTables = null;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                    return send_obj;
                }

                send_obj.responseObjectInfo.Status = 1;
                send_obj.responseObjectInfo.Message = "Category detail data retrieved successfully.";
                send_obj.responseObjectInfo.dt_ReturnedTables = new dynamic[1];
                send_obj.responseObjectInfo.dt_ReturnedTables[0] = dtCategoryDetail;                
                send_obj.responseObjectInfo.ObjRetArgs = null;
                return send_obj;
            }
            catch (Exception ex)
            {
                ServerLog.ExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                send_obj.responseObjectInfo.Status = 2;
                send_obj.responseObjectInfo.Message = ex.Message;
                send_obj.responseObjectInfo.dt_ReturnedTables = null;
                return send_obj;
            }
        }

        private SendReceiveJSon SaveCategoryData(SendReceiveJSon receive_obj)
        {
            SendReceiveJSon send_obj = new SendReceiveJSon();
            String Message = String.Empty;

            try
            {
                Dictionary<string, object> dicRecieveData = receive_obj.requestObjectInfo.ds_UploadData;
                String jsonTypedCategoryMst = null;
                DataSet dS_Config_CategoryMstClient = null;
                if (dicRecieveData.ContainsKey("New"))
                {
                    jsonTypedCategoryMst = JsonConvert.SerializeObject(dicRecieveData["New"]);
                    dS_Config_CategoryMstClient = JsonConvert.DeserializeObject<DataSet>(jsonTypedCategoryMst);
                }

                #region Primary Validation
                if (dS_Config_CategoryMstClient == null || dS_Config_CategoryMstClient.Tables.Count <= 0 || dS_Config_CategoryMstClient.Tables["Config_CategoryMst"] == null || dS_Config_CategoryMstClient.Tables["Config_CategoryMst"].Rows.Count <= 0)
                {
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "No data available to Save Category Master. Operation cancelled.";
                    return send_obj;
                }
                #endregion

                String database = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("database") && receive_obj.requestObjectInfo.OpArgs["database"] != null && receive_obj.requestObjectInfo.OpArgs["database"].ToString().Trim() != String.Empty)
                    database = receive_obj.requestObjectInfo.OpArgs["database"].ToString().Trim();

                String account_type = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("account_type") && receive_obj.requestObjectInfo.OpArgs["account_type"] != null && receive_obj.requestObjectInfo.OpArgs["account_type"].ToString().Trim() != String.Empty)
                    account_type = "'" + receive_obj.requestObjectInfo.OpArgs["account_type"].ToString().Trim() + "'";

                String NewEdit = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("NewEdit") && receive_obj.requestObjectInfo.OpArgs["NewEdit"] != null && receive_obj.requestObjectInfo.OpArgs["NewEdit"].ToString().Trim() != String.Empty)
                    NewEdit = receive_obj.requestObjectInfo.OpArgs["NewEdit"].ToString().Trim();

                ClientUtility objClientUtility = new ClientUtility();
                objClientUtility.SetDatabaseConnectionString(this, database);
                if (DBConnection.State == ConnectionState.Open)
                    DBConnection.Close();
                DBConnection.Open();
                DBCommand.Transaction = DBConnection.BeginTransaction();

                DataTable dtConfig_CategoryMstClient = dS_Config_CategoryMstClient.Tables["Config_CategoryMst"];
                DataTable dtConfig_CategoryMstServer = objClientUtility.GetTableSchema(ref DBDataAdpterObject, dtConfig_CategoryMstClient.TableName, ref Message);
                dtConfig_CategoryMstServer.PrimaryKey = new DataColumn[] { dtConfig_CategoryMstServer.Columns["CategoryCode"], dtConfig_CategoryMstServer.Columns["AccountType"] };
                dtConfig_CategoryMstServer.ImportRow(dtConfig_CategoryMstClient.Rows[0]);
                dtConfig_CategoryMstServer.AcceptChanges();
                if (NewEdit == "New")
                    dtConfig_CategoryMstServer.Rows[0].SetAdded();
                else if (NewEdit == "Edit")
                    dtConfig_CategoryMstServer.Rows[0].SetModified();
                
                BLGeneralUtil.UpdateTableInfo utf = BLGeneralUtil.UpdateTable(ref DBCommand, dtConfig_CategoryMstServer, BLGeneralUtil.UpdateWhereMode.KeyColumnsOnly);
                if (!utf.Status || utf.TotalRowsAffected != dtConfig_CategoryMstServer.Rows.Count)
                {
                    DBCommand.Transaction.Rollback();
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "An error occurred while Save Category data. " + utf.ErrorMessage;
                    send_obj.responseObjectInfo.dt_ReturnedTables = null;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                }
                else
                {
                    DBCommand.Transaction.Commit();
                    MgmtSales objMgmtSales = new MgmtSales();
                    objClientUtility.SetDatabaseConnectionString(objMgmtSales, database);
                    DataTable dtCategoryList = objMgmtSales.GetCategoryMst(account_type, null, ref Message);
                    send_obj.responseObjectInfo.Status = 1;
                    send_obj.responseObjectInfo.Message = "Category data saved successfully.";
                    send_obj.responseObjectInfo.dt_ReturnedTables = new dynamic[1];
                    send_obj.responseObjectInfo.dt_ReturnedTables[0] = dtCategoryList;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                }
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();                               
            }
            catch (Exception ex)
            {
                ServerLog.ExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                send_obj.responseObjectInfo.Status = 2;
                send_obj.responseObjectInfo.Message = ex.Message;
                send_obj.responseObjectInfo.dt_ReturnedTables = null;
            }

            return send_obj;
        }

        private SendReceiveJSon GetAttributeDetails(SendReceiveJSon receive_obj)
        {
            SendReceiveJSon send_obj = new SendReceiveJSon();
            String Message = String.Empty;

            try
            {
                MgmtSales objMgmtSales = new MgmtSales();
                String database = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("database") && receive_obj.requestObjectInfo.OpArgs["database"] != null && receive_obj.requestObjectInfo.OpArgs["database"].ToString().Trim() != String.Empty)
                    database = receive_obj.requestObjectInfo.OpArgs["database"].ToString().Trim();

                String account_type = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("account_type") && receive_obj.requestObjectInfo.OpArgs["account_type"] != null && receive_obj.requestObjectInfo.OpArgs["account_type"].ToString().Trim() != String.Empty && receive_obj.requestObjectInfo.OpArgs["account_type"].ToString().Trim().ToUpper() != "ALL")
                    account_type = "'" + receive_obj.requestObjectInfo.OpArgs["account_type"].ToString().Trim() + "'";

                String CategoryCode = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("CategoryCode") && receive_obj.requestObjectInfo.OpArgs["CategoryCode"] != null && receive_obj.requestObjectInfo.OpArgs["CategoryCode"].ToString().Trim() != String.Empty && receive_obj.requestObjectInfo.OpArgs["CategoryCode"].ToString().Trim().ToUpper() != "ALL")
                    CategoryCode = receive_obj.requestObjectInfo.OpArgs["CategoryCode"].ToString().Trim();

                String AttributeCode = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("AttributeCode") && receive_obj.requestObjectInfo.OpArgs["AttributeCode"] != null && receive_obj.requestObjectInfo.OpArgs["AttributeCode"].ToString().Trim() != String.Empty && receive_obj.requestObjectInfo.OpArgs["AttributeCode"].ToString().Trim().ToUpper() != "ALL")
                    AttributeCode = receive_obj.requestObjectInfo.OpArgs["AttributeCode"].ToString().Trim();

                ClientUtility objClientUtility = new ClientUtility();
                objClientUtility.SetDatabaseConnectionString(objMgmtSales, database);

                DataTable dtAttributeDetail = objMgmtSales.GetAttributeCategoryMap(account_type, CategoryCode, AttributeCode, ref Message);
                if (dtAttributeDetail == null || dtAttributeDetail.Rows.Count <= 0)
                {
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "Category detail data not found." + Environment.NewLine + Message;
                    send_obj.responseObjectInfo.dt_ReturnedTables = null;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                    return send_obj;
                }

                account_type = "'" + dtAttributeDetail.Rows[0]["AccountType"].ToString() + "'";
                DataTable dtCategoryList = objMgmtSales.GetCategoryMst(account_type, null, ref Message);
                if (dtCategoryList == null || dtCategoryList.Rows.Count <= 0)
                {
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "Category List data not found." + Environment.NewLine + Message;
                    send_obj.responseObjectInfo.dt_ReturnedTables = null;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                    return send_obj;
                }

                send_obj.responseObjectInfo.Status = 1;
                send_obj.responseObjectInfo.Message = "Attribute Category Map data retrieved successfully.";
                send_obj.responseObjectInfo.dt_ReturnedTables = new dynamic[2];
                send_obj.responseObjectInfo.dt_ReturnedTables[0] = dtAttributeDetail;
                send_obj.responseObjectInfo.dt_ReturnedTables[1] = dtCategoryList;                
                send_obj.responseObjectInfo.ObjRetArgs = null;
                return send_obj;
            }
            catch (Exception ex)
            {
                ServerLog.ExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                send_obj.responseObjectInfo.Status = 2;
                send_obj.responseObjectInfo.Message = ex.Message;
                send_obj.responseObjectInfo.dt_ReturnedTables = null;
                return send_obj;
            }
        }

        private SendReceiveJSon SaveAttributeData(SendReceiveJSon receive_obj)
        {
            SendReceiveJSon send_obj = new SendReceiveJSon();
            String Message = String.Empty;

            try
            {
                Dictionary<string, object> dicRecieveData = receive_obj.requestObjectInfo.ds_UploadData;
                String jsonTypedAttributeMst = null;
                DataSet dS_Config_AttributeCategoryMapClient = null;
                if (dicRecieveData.ContainsKey("New"))
                {
                    jsonTypedAttributeMst = JsonConvert.SerializeObject(dicRecieveData["New"]);
                    dS_Config_AttributeCategoryMapClient = JsonConvert.DeserializeObject<DataSet>(jsonTypedAttributeMst);
                }

                #region Primary Validation
                if (dS_Config_AttributeCategoryMapClient == null || dS_Config_AttributeCategoryMapClient.Tables.Count <= 0 || dS_Config_AttributeCategoryMapClient.Tables["Config_AttributeCategoryMap"] == null || dS_Config_AttributeCategoryMapClient.Tables["Config_AttributeCategoryMap"].Rows.Count <= 0)
                {
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "No data available to Save Attribute Category Map. Operation cancelled.";
                    return send_obj;
                }
                #endregion

                String database = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("database") && receive_obj.requestObjectInfo.OpArgs["database"] != null && receive_obj.requestObjectInfo.OpArgs["database"].ToString().Trim() != String.Empty)
                    database = receive_obj.requestObjectInfo.OpArgs["database"].ToString().Trim();

                String account_type = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("account_type") && receive_obj.requestObjectInfo.OpArgs["account_type"] != null && receive_obj.requestObjectInfo.OpArgs["account_type"].ToString().Trim() != String.Empty)
                    account_type = "'" + receive_obj.requestObjectInfo.OpArgs["account_type"].ToString().Trim() + "'";

                String NewEdit = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("NewEdit") && receive_obj.requestObjectInfo.OpArgs["NewEdit"] != null && receive_obj.requestObjectInfo.OpArgs["NewEdit"].ToString().Trim() != String.Empty)
                    NewEdit = receive_obj.requestObjectInfo.OpArgs["NewEdit"].ToString().Trim();

                ClientUtility objClientUtility = new ClientUtility();
                objClientUtility.SetDatabaseConnectionString(this, database);
                if (DBConnection.State == ConnectionState.Open)
                    DBConnection.Close();
                DBConnection.Open();
                DBCommand.Transaction = DBConnection.BeginTransaction();

                DataTable dtConfig_AttributeCategoryMapClient = dS_Config_AttributeCategoryMapClient.Tables["Config_AttributeCategoryMap"];                
                DataTable dtConfig_AttributeCategoryMapServer = objClientUtility.GetTableSchema(ref DBDataAdpterObject, dtConfig_AttributeCategoryMapClient.TableName, ref Message);
                dtConfig_AttributeCategoryMapServer.PrimaryKey = new DataColumn[] { dtConfig_AttributeCategoryMapServer.Columns["AttributeCode"], dtConfig_AttributeCategoryMapServer.Columns["CategoryCode"], dtConfig_AttributeCategoryMapServer.Columns["AccountType"] };
                dtConfig_AttributeCategoryMapServer.ImportRow(dtConfig_AttributeCategoryMapClient.Rows[0]);
                dtConfig_AttributeCategoryMapServer.AcceptChanges();
                if (NewEdit == "New")
                    dtConfig_AttributeCategoryMapServer.Rows[0].SetAdded();
                else if (NewEdit == "Edit")
                    dtConfig_AttributeCategoryMapServer.Rows[0].SetModified();

                BLGeneralUtil.UpdateTableInfo utf = BLGeneralUtil.UpdateTable(ref DBCommand, dtConfig_AttributeCategoryMapServer, BLGeneralUtil.UpdateWhereMode.KeyColumnsOnly);
                if (!utf.Status || utf.TotalRowsAffected != dtConfig_AttributeCategoryMapServer.Rows.Count)
                {
                    DBCommand.Transaction.Rollback();
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "An error occurred while Save Attribute Category Map data. " + utf.ErrorMessage;
                    send_obj.responseObjectInfo.dt_ReturnedTables = null;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                }
                else
                {
                    DBCommand.Transaction.Commit();
                    MgmtSales objMgmtSales = new MgmtSales();
                    objClientUtility.SetDatabaseConnectionString(objMgmtSales, database);
                    DataTable dtAttributeList = objMgmtSales.GetAttributeCategoryMap(account_type, null, null, ref Message);
                    send_obj.responseObjectInfo.Status = 1;
                    send_obj.responseObjectInfo.Message = "Attribute Category Map data saved successfully.";
                    send_obj.responseObjectInfo.dt_ReturnedTables = new dynamic[1];
                    send_obj.responseObjectInfo.dt_ReturnedTables[0] = dtAttributeList;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                }
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
            catch (Exception ex)
            {
                ServerLog.ExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                send_obj.responseObjectInfo.Status = 2;
                send_obj.responseObjectInfo.Message = ex.Message;
                send_obj.responseObjectInfo.dt_ReturnedTables = null;
            }

            return send_obj;
        }

        private SendReceiveJSon GetColumnList(SendReceiveJSon receive_obj)
        {
            SendReceiveJSon send_obj = new SendReceiveJSon();
            String Message = String.Empty;

            try
            {
                MgmtSales objMgmtSales = new MgmtSales();
                String database = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("database") && receive_obj.requestObjectInfo.OpArgs["database"] != null && receive_obj.requestObjectInfo.OpArgs["database"].ToString().Trim() != String.Empty)
                    database = receive_obj.requestObjectInfo.OpArgs["database"].ToString().Trim();

                String TableName = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("TableName") && receive_obj.requestObjectInfo.OpArgs["TableName"] != null && receive_obj.requestObjectInfo.OpArgs["TableName"].ToString().Trim() != String.Empty)
                    TableName = receive_obj.requestObjectInfo.OpArgs["TableName"].ToString().Trim();

                ClientUtility objClientUtility = new ClientUtility();
                objClientUtility.SetDatabaseConnectionString(objMgmtSales, database);

                DataTable dtColumnList = objMgmtSales.GetColumnList(TableName, ref Message);
                if (dtColumnList == null || dtColumnList.Rows.Count <= 0)
                {
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "Column List data not found." + Environment.NewLine + Message;
                    send_obj.responseObjectInfo.dt_ReturnedTables = null;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                    return send_obj;
                }

                send_obj.responseObjectInfo.Status = 1;
                send_obj.responseObjectInfo.Message = "Column List data retrieved successfully.";
                send_obj.responseObjectInfo.dt_ReturnedTables = new dynamic[1];
                send_obj.responseObjectInfo.dt_ReturnedTables[0] = dtColumnList;                
                send_obj.responseObjectInfo.ObjRetArgs = null;
                return send_obj;
            }
            catch (Exception ex)
            {
                ServerLog.ExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                send_obj.responseObjectInfo.Status = 2;
                send_obj.responseObjectInfo.Message = ex.Message;
                send_obj.responseObjectInfo.dt_ReturnedTables = null;
                return send_obj;
            }
        }        
    }
}
