﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.Utilities;
using System.Configuration;
using System.Data;
using XSD.General;

namespace BLL.Masters
{
    public class MgmtMessaging : ServerBase
    {
        public MgmtMessaging()
        {
            DBConnection = DBObjectFactory.GetConnectionObject();
            DBConnection.ConnectionString = ConfigurationManager.ConnectionStrings["MgmtMessagingConnectionString"].ToString();
            DBCommand = DBObjectFactory.GetCommandObject();
            DBCommand.Connection = DBConnection;
            DBDataAdpterObject = DBObjectFactory.GetDataAdapterObject(DBCommand);
            DBDataAdpterObject.SelectCommand.CommandTimeout = 60;
        }

        public Byte SaveMessageQueue(DataSet dSUpload, ref String Message)
        {
            try
            {
                if (dSUpload == null || dSUpload.Tables.Count <= 0 || dSUpload.Tables[0].Rows.Count <= 0)
                {
                    Message = "No data available to Save. Operation Cancelled.";
                    return 2;
                }
                DS_MessageQueue dS_MessageQueue = (DS_MessageQueue)dSUpload;

                try
                {
                    dS_MessageQueue.EnforceConstraints = true;
                }
                catch (ConstraintException ce)
                {
                    Message = ce.Message;
                    ServerLog.MgmtExceptionLog(ce.Message + Environment.NewLine + ce.StackTrace);
                    return 2;
                }
                catch (Exception ex)
                {
                    Message = ex.Message;
                    ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                    return 2;
                }

                //Establish DataBase Connection.
                if (DBConnection.State == ConnectionState.Closed)
                    DBConnection.Open();
                //Begin Transaction.
                DBCommand.Transaction = DBConnection.BeginTransaction();

                DS_MessageQueue dS_MessageQueueServer = new DS_MessageQueue();
                BLGeneralUtil.UpdateTableInfo objUpdateTableInfo;
                int MessageId, index;
                foreach (DS_MessageQueue.MessageQueueRow rows in dS_MessageQueue.MessageQueue.Rows)
                {
                    dS_MessageQueueServer.MessageQueue.Clear();
                    dS_MessageQueueServer.MessageQueue.ImportRow(rows);
                    //Insert in MessageQueue
                    objUpdateTableInfo = BLGeneralUtil.UpdateTable(ref DBCommand, dS_MessageQueueServer.MessageQueue, BLGeneralUtil.UpdateWhereMode.KeyColumnsOnly);
                    if (!objUpdateTableInfo.Status || objUpdateTableInfo.TotalRowsAffected != dS_MessageQueueServer.MessageQueue.Rows.Count)
                    {
                        DBCommand.Transaction.Rollback();
                        if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                        Message = objUpdateTableInfo.ErrorMessage;
                        return 2;
                    }

                    if (dS_MessageQueue.MessageAttachment.Rows.Count > 0)
                    {
                        dS_MessageQueueServer.MessageAttachment.Clear();

                        //Get MessageId.
                        DBCommand.Parameters.Clear();
                        DBCommand.CommandText = "SELECT @@IDENTITY AS 'Identity';";
                        Object objMessageId = DBCommand.ExecuteScalar();
                        if (objMessageId != null && objMessageId.ToString().Trim() != String.Empty)
                        {
                            //Set MessageId to Attachment.
                            MessageId = Convert.ToInt32(objMessageId);
                            foreach (DS_MessageQueue.MessageAttachmentRow attachmentRow in dS_MessageQueue.MessageAttachment.Rows)
                            {
                                dS_MessageQueueServer.MessageAttachment.ImportRow(attachmentRow);
                                index = dS_MessageQueueServer.MessageAttachment.Rows.Count - 1;
                                dS_MessageQueueServer.MessageAttachment[index].MessageId = MessageId;
                            }
                            //Insert in MessageAttachment
                            objUpdateTableInfo = BLGeneralUtil.UpdateTable(ref DBCommand, dS_MessageQueueServer.MessageAttachment, BLGeneralUtil.UpdateWhereMode.KeyColumnsOnly);
                            if (!objUpdateTableInfo.Status || objUpdateTableInfo.TotalRowsAffected != dS_MessageQueueServer.MessageAttachment.Rows.Count)
                            {
                                DBCommand.Transaction.Rollback();
                                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                                Message = objUpdateTableInfo.ErrorMessage;
                                return 2;
                            }
                        }
                        else
                        {
                            DBCommand.Transaction.Rollback();
                            if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                            Message = "Fail to save Message Queue data.";
                            return 1;
                        }
                    }
                }

                DBCommand.Transaction.Commit();
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                Message = "Message Queue data saved successfully.";
                return 1;
            }
            catch (Exception ex)
            {
                if (DBCommand.Transaction != null)
                    DBCommand.Transaction.Rollback();
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return 2;
            }
        }

        public Byte AddDeviceInfo(String AppName, String RepId, String DeviceId, String TokenId, String DeviceInfo, String OS, String IMEINo, ref String Message)
        {
            try
            {
                DS_DeviceInfo dS_DeviceInfo = new DS_DeviceInfo();
                dS_DeviceInfo.EnforceConstraints = false;
                DS_DeviceInfo.DeviceInfoRow row = dS_DeviceInfo.DeviceInfo.NewDeviceInfoRow();
                row.AppName = AppName;
                row.RepId = RepId;
                row.DeviceId = (DeviceId == null ? String.Empty : DeviceId);
                row.TokenId = TokenId;
                if (DeviceInfo != null && DeviceInfo.Trim() != String.Empty)
                    row.DeviceInfo = DeviceInfo;
                else
                    row.SetDeviceInfoNull();
                row.OS = OS;
                if (IMEINo != null && IMEINo.Trim() != String.Empty)
                    row.IMEINo = IMEINo;
                else
                    row.SetIMEINoNull();
                dS_DeviceInfo.DeviceInfo.AddDeviceInfoRow(row);

                try
                {
                    dS_DeviceInfo.EnforceConstraints = true;
                }
                catch (ConstraintException ce)
                {
                    Message = ce.Message;
                    ServerLog.MgmtExceptionLog(ce.Message + Environment.NewLine + ce.StackTrace);
                    return 2;
                }
                catch (Exception ex)
                {
                    Message = ex.Message;
                    ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                    return 2;
                }

                //Establish DataBase Connection.
                if (DBConnection.State == ConnectionState.Closed)
                    DBConnection.Open();
                //Begin Transaction.
                DBCommand.Transaction = DBConnection.BeginTransaction();

                //Delete already existing DeviceInfo with same AppName and DeviceId
                if (OS.ToUpper() == Constant.OS_Android.ToUpper())
                {
                    DBCommand.Parameters.Clear();
                    StringBuilder SQLDelete = new StringBuilder();
                    SQLDelete.Append("DELETE FROM DeviceInfo WHERE AppName=@AppName AND (DeviceId=@DeviceId");
                    if (IMEINo != null && IMEINo.Trim() != String.Empty)
                    {
                        SQLDelete.Append(" OR IMEINo=@IMEINo");
                        DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@IMEINo", DbType.String, IMEINo));
                    }
                    SQLDelete.Append(")");
                    DBCommand.CommandText = SQLDelete.ToString();
                    DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@AppName", DbType.String, AppName));
                    DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@DeviceId", DbType.String, (DeviceId == null ? String.Empty : DeviceId)));
                    DBCommand.ExecuteNonQuery();

                    DBCommand.Parameters.Clear();
                    SQLDelete = new StringBuilder();
                    SQLDelete.Append("DELETE FROM MessageQueueFinal WHERE AppName=@AppName AND SentStatus='N' AND (DeviceId=@DeviceId");
                    if (IMEINo != null && IMEINo.Trim() != String.Empty)
                    {
                        SQLDelete.Append(" OR IMEINo=@IMEINo");
                        DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@IMEINo", DbType.String, IMEINo));
                    }
                    SQLDelete.Append(")");
                    DBCommand.CommandText = SQLDelete.ToString();
                    DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@AppName", DbType.String, AppName));
                    DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@DeviceId", DbType.String, (DeviceId == null ? String.Empty : DeviceId)));
                    DBCommand.ExecuteNonQuery();
                }
                else
                {
                    DBCommand.Parameters.Clear();
                    StringBuilder SQLDelete = new StringBuilder();
                    SQLDelete.Append("DELETE FROM DeviceInfo WHERE AppName=@AppName AND (DeviceId=@DeviceId OR TokenId=@TokenId)");
                    DBCommand.CommandText = SQLDelete.ToString();
                    DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@AppName", DbType.String, AppName));
                    DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@DeviceId", DbType.String, (DeviceId == null ? String.Empty : DeviceId)));
                    DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@TokenId", DbType.String, TokenId));
                    DBCommand.ExecuteNonQuery();

                    DBCommand.Parameters.Clear();
                    SQLDelete = new StringBuilder();
                    SQLDelete.Append("DELETE FROM MessageQueueFinal WHERE AppName=@AppName AND (DeviceId=@DeviceId OR TokenId=@TokenId) AND SentStatus='N'");
                    DBCommand.CommandText = SQLDelete.ToString();
                    DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@AppName", DbType.String, AppName));
                    DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@DeviceId", DbType.String, (DeviceId == null ? String.Empty : DeviceId)));
                    DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@TokenId", DbType.String, TokenId));
                    DBCommand.ExecuteNonQuery();
                }

                BLGeneralUtil.UpdateTableInfo objUpdateTableInfo = BLGeneralUtil.UpdateTable(ref DBCommand, dS_DeviceInfo.DeviceInfo, BLGeneralUtil.UpdateWhereMode.KeyColumnsOnly);
                if (!objUpdateTableInfo.Status || objUpdateTableInfo.TotalRowsAffected != dS_DeviceInfo.DeviceInfo.Rows.Count)
                {
                    DBCommand.Transaction.Rollback();
                    if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                    Message = "Device registered fail." + Environment.NewLine + objUpdateTableInfo.ErrorMessage;
                    return 2;
                }
                else
                {
                    DBCommand.Transaction.Commit();
                    if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                    Message = "Device registered successfully.";
                    return 1;
                }
            }
            catch (Exception ex)
            {
                if (DBCommand.Transaction != null)
                    DBCommand.Transaction.Rollback();
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return 2;
            }
        }

        public Byte RemoveDeviceInfo(String AppName, String DeviceId, ref String Message)
        {
            try
            {
                //Establish DataBase Connection.
                if (DBConnection.State == ConnectionState.Closed)
                    DBConnection.Open();
                //Begin Transaction.
                DBCommand.Transaction = DBConnection.BeginTransaction();

                DBCommand.Parameters.Clear();
                StringBuilder SQLDelete = new StringBuilder();
                SQLDelete.Append("DELETE FROM DeviceInfo WHERE AppName=@AppName AND (DeviceId=@DeviceId OR TokenId=@TokenId)");
                DBCommand.CommandText = SQLDelete.ToString();
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@AppName", DbType.String, AppName));
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@DeviceId", DbType.String, DeviceId));
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@TokenId", DbType.String, DeviceId));
                int NoOfRowsDelete = DBCommand.ExecuteNonQuery();
                //if (NoOfRowsDelete <= 0)
                //{
                //    DBCommand.Transaction.Rollback();
                //    if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                //    Message = "Device unregistered fail.";
                //    return 2;
                //}
                //else
                {
                    DBCommand.Parameters.Clear();
                    SQLDelete = new StringBuilder();
                    SQLDelete.Append("DELETE FROM MessageQueueFinal WHERE AppName=@AppName AND (DeviceId=@DeviceId OR TokenId=@TokenId) AND SentStatus='N'");
                    DBCommand.CommandText = SQLDelete.ToString();
                    DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@AppName", DbType.String, AppName));
                    DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@DeviceId", DbType.String, DeviceId));
                    DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@TokenId", DbType.String, DeviceId));
                    NoOfRowsDelete = DBCommand.ExecuteNonQuery();

                    DBCommand.Transaction.Commit();
                    if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                    Message = "Device unregistered successfully.";
                    return 1;
                }
            }
            catch (Exception ex)
            {
                if (DBCommand.Transaction != null)
                    DBCommand.Transaction.Rollback();
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return 2;
            }
        }

        public Byte UpdateUnreadCount(String AppName, String RepId, String DeviceId, int UnreadCount, ref String Message)
        {
            try
            {
                //Establish DataBase Connection.
                if (DBConnection.State == ConnectionState.Closed)
                    DBConnection.Open();
                //Begin Transaction.
                DBCommand.Transaction = DBConnection.BeginTransaction();

                DBCommand.Parameters.Clear();
                StringBuilder SQLUpdate = new StringBuilder();
                SQLUpdate.Append("UPDATE DeviceInfo ");
                SQLUpdate.Append("SET    UnreadCount=@UnreadCount ");
                SQLUpdate.Append("WHERE  AppName=@AppName AND RepId=@RepId AND DeviceId=@DeviceId");
                DBCommand.CommandText = SQLUpdate.ToString();
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@UnreadCount", DbType.Int32, UnreadCount));
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@AppName", DbType.String, AppName));
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@RepId", DbType.String, RepId));
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@DeviceId", DbType.String, DeviceId));
                int NoOfRowsDelete = DBCommand.ExecuteNonQuery();
                if (NoOfRowsDelete <= 0)
                {
                    DBCommand.Transaction.Rollback();
                    if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                    Message = "UnreadCount update fail.";
                    return 2;
                }
                else
                {
                    DBCommand.Transaction.Commit();
                    if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                    Message = "UnreadCount updated successfully.";
                    return 1;
                }
            }
            catch (Exception ex)
            {
                if (DBCommand.Transaction != null)
                    DBCommand.Transaction.Rollback();
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return 2;
            }
        }

        public DataTable GetMessages(String RepId, String AppName, int NoOfMessageInGetMessage, DateTime? LastMessageDateTime, ref String Message)
        {
            try
            {
                int TimeZoneDifferenceInMinute;
                ParameterMst objParameterMst = new ParameterMst();
                DataTable dtParameter = objParameterMst.GetParameter(ref DBDataAdpterObject, "TimeZoneDifferenceInMinute", null, ref Message);
                if (dtParameter != null && dtParameter.Rows.Count > 0)
                {
                    //TimeZoneDifferenceInMinute
                    DataRow[] drArray = dtParameter.Select("Code='TimeZoneDifferenceInMinute'");
                    if (drArray != null && drArray.Length > 0 && drArray[0]["Value"].ToString().Trim() != String.Empty)
                        TimeZoneDifferenceInMinute = Convert.ToInt32(drArray[0]["Value"]);
                    else
                        TimeZoneDifferenceInMinute = Constant.TimeZoneDifferenceInMinute;
                }
                else
                    TimeZoneDifferenceInMinute = Constant.TimeZoneDifferenceInMinute;

                DBDataAdpterObject.SelectCommand.Parameters.Clear();
                StringBuilder SQLSelect = new StringBuilder();
                SQLSelect.Append(" SELECT  *    FROM ");
                SQLSelect.Append(" (SELECT      MessageQueue.MessageId, Date, Sender, Subject, Message, RepId, TerritoryNum, ISNULL(IsAnyAttachment, 'N') AS IsAnyAttachment, CASE WHEN ISNULL(DeleteFlag, 'N') = 'Y' THEN 'D' WHEN ISNULL(ReadFlag, 'N') = 'Y' THEN 'R' ELSE 'U' END AS Status, LastUpdateDateTime, ROW_NUMBER() OVER(ORDER BY Date DESC) as row_num, ");
                SQLSelect.Append("              SrNo, AttachmentType, AttachmentName, AttachmentPath , AcctKeyId, AcctType, ShowInInbox, ShortMessage, ExpirationDate, EventType, ISNULL(IsResponseRequired, 'N') AS IsResponseRequired, ISNULL(IsResponded, 'N') AS IsResponded, ResponseLastDate ");
                SQLSelect.Append("  FROM    MessageQueue ");
                SQLSelect.Append("  LEFT OUTER JOIN MessageAttachment ON MessageQueue.MessageId = MessageAttachment.MessageId ");
                SQLSelect.Append("  WHERE    (RepId = @RepId or RepId is null) AND (TargetApp = @TargetApp) ");//AND (DeleteFlag = @DeleteFlag) ");
                SQLSelect.Append("  AND      (DateTimeToSend IS NULL OR GETDATE() >= DateTimeToSend) ");
                //SQLSelect.Append("  AND      (ExpirationDate IS NULL OR DATEADD(minute, @TimeZoneDifferenceInMinute, GETDATE()) <= ExpirationDate)  ");
                SQLSelect.Append("  AND      (ExpirationDate IS NULL OR DATEADD(minute, @TimeZoneDifferenceInMinute, @ToDay) <= ExpirationDate)  ");
                if (LastMessageDateTime.HasValue)
                {
                    SQLSelect.Append("  AND    (Date >= @LastMessageDateTime OR LastUpdateDateTime >= @LastMessageDateTime) ");
                    DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@LastMessageDateTime", DbType.DateTime, LastMessageDateTime.Value.AddSeconds(1)));
                }
                SQLSelect.Append(" ) AS TempMessageQueue ");
                SQLSelect.Append(" WHERE   (row_num <= " + NoOfMessageInGetMessage + ") ");//Last 20 Message
                SQLSelect.Append(" ORDER BY row_num ");
                DBDataAdpterObject.SelectCommand.CommandText = SQLSelect.ToString();
                DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@RepId", DbType.String, RepId));
                DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@TargetApp", DbType.String, AppName));
                DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@TimeZoneDifferenceInMinute", DbType.Int32, TimeZoneDifferenceInMinute));
                DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@ToDay", DbType.DateTime, DateTime.Today));
                //DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@DeleteFlag", DbType.String, Constant.FLAG_N));
                DBDataAdpterObject.TableMappings.Clear();
                DBDataAdpterObject.TableMappings.Add("Table", "MessageQueue");
                DataSet ds = new DataSet();
                DBDataAdpterObject.Fill(ds);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    Message = "MessageQueue retrieved succesfully for Rep Id : " + RepId;
                    return ds.Tables[0];
                }
                else
                {
                    Message = "MessageQueue not found for Rep Id : " + RepId;
                    return null;
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
            finally
            {
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
        }

        public DataTable GetEventTypes(String RepId, String AppName, ref String Message)
        {
            try
            {
                DBDataAdpterObject.SelectCommand.Parameters.Clear();
                StringBuilder SQLSelect = new StringBuilder();
                SQLSelect.Append(" SELECT  * FROM EventTypeMst");

                DBDataAdpterObject.TableMappings.Clear();
                DBDataAdpterObject.TableMappings.Add("Table", "EventTypeMst");
                DBDataAdpterObject.SelectCommand.CommandText = SQLSelect.ToString();
                DataSet ds = new DataSet();
                DBDataAdpterObject.Fill(ds);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    Message = "EventType retrieved succesfully for Rep Id : " + RepId;
                    return ds.Tables[0];
                }
                else
                {
                    Message = "EventType not found for Rep Id : " + RepId;
                    return null;
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
            finally
            {
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
        }

        public DataTable GetMessageAttachment(String MessageIds, ref String Message)
        {
            try
            {
                DBDataAdpterObject.SelectCommand.Parameters.Clear();
                StringBuilder SQLSelect = new StringBuilder();
                SQLSelect.Append(" SELECT  * ");
                SQLSelect.Append(" FROM    MessageAttachment ");
                if (MessageIds != null && MessageIds.Trim() != String.Empty)
                    SQLSelect.Append(" WHERE   (MessageId IN (" + MessageIds + ")) ");
                SQLSelect.Append(" ORDER BY MessageId, SrNo ");
                DBDataAdpterObject.SelectCommand.CommandText = SQLSelect.ToString();
                DBDataAdpterObject.TableMappings.Clear();
                DBDataAdpterObject.TableMappings.Add("Table", "MessageAttachment");
                DataSet ds = new DataSet();
                DBDataAdpterObject.Fill(ds);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    Message = "MessageAttachment retrieved succesfully for MessageIds : " + MessageIds;
                    return ds.Tables[0];
                }
                else
                {
                    Message = "MessageAttachment not found for MessageIds : " + MessageIds;
                    return null;
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
            finally
            {
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
        }

        public Byte DeleteMessage(String MessageId, String AppName, ref String Message)
        {
            try
            {
                //Establish DataBase Connection.
                if (DBConnection.State == ConnectionState.Closed)
                    DBConnection.Open();
                //Begin Transaction.
                DBCommand.Transaction = DBConnection.BeginTransaction();

                StringBuilder SQLUpdate = new StringBuilder();
                SQLUpdate.Append("UPDATE MessageQueue ");
                SQLUpdate.Append("SET    DeleteFlag=@DeleteFlag, LastUpdateDateTime=@LastUpdateDateTime ");
                SQLUpdate.Append("WHERE  (MessageId IN (" + MessageId + ")) AND (TargetApp=@TargetApp) ");
                DBCommand.Parameters.Clear();
                DBCommand.CommandText = SQLUpdate.ToString();
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@TargetApp", DbType.String, AppName));
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@DeleteFlag", DbType.String, Constant.FLAG_Y));
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@LastUpdateDateTime", DbType.DateTime, DateTime.Now));
                int NoOfRowsUpdate = DBCommand.ExecuteNonQuery();
                if (NoOfRowsUpdate <= 0)
                {
                    DBCommand.Transaction.Rollback();
                    if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                    Message = "Fail to delete Message for MessageId : " + MessageId;
                    return 2;
                }

                SQLUpdate = new StringBuilder();
                SQLUpdate.Append("UPDATE MessageQueueFinal ");
                SQLUpdate.Append("SET    DeleteFlag=@DeleteFlag, LastUpdateDateTime=@LastUpdateDateTime ");
                SQLUpdate.Append("WHERE  (MessageId IN (" + MessageId + ")) AND (AppName=@AppName) ");
                DBCommand.Parameters.Clear();
                DBCommand.CommandText = SQLUpdate.ToString();
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@AppName", DbType.String, AppName));
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@DeleteFlag", DbType.String, Constant.FLAG_Y));
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@LastUpdateDateTime", DbType.DateTime, DateTime.Now));
                NoOfRowsUpdate = DBCommand.ExecuteNonQuery();
                //if (NoOfRowsUpdate <= 0)
                //{
                //    DBCommand.Transaction.Rollback();
                //    if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                //    Message = "Fail to delete Message for MessageId : " + MessageId;
                //    return 2;
                //}

                DBCommand.Transaction.Commit();
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                Message = "Successfully delete Message for MessageId : " + MessageId;
                return 1;
            }
            catch (Exception ex)
            {
                if (DBCommand.Transaction != null)
                    DBCommand.Transaction.Rollback();
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return 2;
            }
        }

        public Byte MarkReadMessage(String MessageId, String AppName, ref String Message)
        {
            try
            {
                //Establish DataBase Connection.
                if (DBConnection.State == ConnectionState.Closed)
                    DBConnection.Open();
                //Begin Transaction.
                DBCommand.Transaction = DBConnection.BeginTransaction();

                StringBuilder SQLUpdate = new StringBuilder();
                SQLUpdate.Append("UPDATE MessageQueue ");
                SQLUpdate.Append("SET    ReadFlag=@ReadFlag, LastUpdateDateTime=@LastUpdateDateTime ");
                SQLUpdate.Append("WHERE  (MessageId IN (" + MessageId + ")) AND (TargetApp=@TargetApp) ");
                DBCommand.Parameters.Clear();
                DBCommand.CommandText = SQLUpdate.ToString();
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@TargetApp", DbType.String, AppName));
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@ReadFlag", DbType.String, Constant.FLAG_Y));
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@LastUpdateDateTime", DbType.DateTime, DateTime.Now));
                int NoOfRowsUpdate = DBCommand.ExecuteNonQuery();
                if (NoOfRowsUpdate <= 0)
                {
                    DBCommand.Transaction.Rollback();
                    if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                    Message = "Fail to Mark Read Message for MessageId : " + MessageId;
                    return 2;
                }

                SQLUpdate = new StringBuilder();
                SQLUpdate.Append("UPDATE MessageQueueFinal ");
                SQLUpdate.Append("SET    ReadFlag=@ReadFlag, LastUpdateDateTime=@LastUpdateDateTime ");
                SQLUpdate.Append("WHERE  (MessageId IN (" + MessageId + ")) AND (AppName=@AppName) ");
                DBCommand.Parameters.Clear();
                DBCommand.CommandText = SQLUpdate.ToString();
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@AppName", DbType.String, AppName));
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@ReadFlag", DbType.String, Constant.FLAG_Y));
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@LastUpdateDateTime", DbType.DateTime, DateTime.Now));
                NoOfRowsUpdate = DBCommand.ExecuteNonQuery();
                //if (NoOfRowsUpdate <= 0)
                //{
                //    DBCommand.Transaction.Rollback();
                //    if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                //    Message = "Fail to Mark Read Message for MessageId : " + MessageId;
                //    return 2;
                //}

                DBCommand.Transaction.Commit();
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                Message = "Successfully Mark Read Message for MessageId : " + MessageId;
                return 1;
            }
            catch (Exception ex)
            {
                if (DBCommand.Transaction != null)
                    DBCommand.Transaction.Rollback();
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return 2;
            }
        }

        public Byte MarkRespondedMessage(String MessageId, String AppName, ref String Message)
        {
            try
            {
                //Establish DataBase Connection.
                if (DBConnection.State == ConnectionState.Closed)
                    DBConnection.Open();
                //Begin Transaction.
                DBCommand.Transaction = DBConnection.BeginTransaction();

                StringBuilder SQLUpdate = new StringBuilder();
                SQLUpdate.Append("UPDATE MessageQueue ");
                SQLUpdate.Append("SET    IsResponded=@IsResponded, LastUpdateDateTime=@LastUpdateDateTime ");
                SQLUpdate.Append("WHERE  (MessageId IN (" + MessageId + ")) AND (TargetApp=@TargetApp) ");
                DBCommand.Parameters.Clear();
                DBCommand.CommandText = SQLUpdate.ToString();
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@TargetApp", DbType.String, AppName));
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@IsResponded", DbType.String, Constant.FLAG_Y));
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@LastUpdateDateTime", DbType.DateTime, DateTime.Now));
                int NoOfRowsUpdate = DBCommand.ExecuteNonQuery();
                if (NoOfRowsUpdate <= 0)
                {
                    DBCommand.Transaction.Rollback();
                    if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                    Message = "Fail to Mark Responded Message for MessageId : " + MessageId;
                    return 2;
                }

                DBCommand.Transaction.Commit();
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                Message = "Successfully Mark Responded Message for MessageId : " + MessageId;
                return 1;
            }
            catch (Exception ex)
            {
                if (DBCommand.Transaction != null)
                    DBCommand.Transaction.Rollback();
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return 2;
            }
        }

        //By Dhanunjay on 30-Aug-2017
        public Byte SaveNPPAlertResponse(DataSet dS_MessageResponse, ref DataTable dtListUpdateStatus, ref String Message)
        {
            try
            {
                EmailSender objEmailSender = new EmailSender();

                if (dtListUpdateStatus == null || dtListUpdateStatus.Rows.Count == 0)
                {
                    dtListUpdateStatus = new DataTable("dtListUpdateStatus");
                    if (!dtListUpdateStatus.Columns.Contains("MessageId"))
                        dtListUpdateStatus.Columns.Add("MessageId");
                    if (!dtListUpdateStatus.Columns.Contains("Status"))
                        dtListUpdateStatus.Columns.Add("Status");
                }

                int cnt = 0;
                DataTable Dt_MessageResponse = new DataTable();
                MgmtSales objMgmtSales = new MgmtSales();

                Dt_MessageResponse = dS_MessageResponse.Tables[0];
                //DataTable dtEmailAccounts = objEmailSender.GetEmailAcountsDetail(ref Message);

                if (Dt_MessageResponse.Rows.Count > 0) //Should get into the Edit Mode
                {

                    int NoOfRowsEffected = 0;

                    if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                    DBConnection.Open();
                    DBCommand.Transaction = DBConnection.BeginTransaction();

                    for (int i = 0; i < Dt_MessageResponse.Rows.Count; i++)
                    {
                        dtListUpdateStatus.Rows.Add(dtListUpdateStatus.NewRow());
                        dtListUpdateStatus.Rows[dtListUpdateStatus.Rows.Count - 1]["MessageId"] = Dt_MessageResponse.Rows[i]["MessageId"].ToString();

                        DBCommand.Parameters.Clear();
                        StringBuilder SQLQuery = new StringBuilder();
                        StringBuilder SQLSelect = new StringBuilder();
                        SQLQuery.Clear();
                        SQLSelect.Clear();

                        DataSet dS_Response = objMgmtSales.ConvertJsonToDataSetXML(Dt_MessageResponse.Rows[i]["Response"].ToString());

                        String[] StrResponse = dS_Response.Tables[0].Rows[0]["MessageData"].ToString().Split(new Char[] { '~' }, StringSplitOptions.None);

                        DataTable DtNPPAlert = GetNPPAlertMaster(dS_Response.Tables[0].Rows[0]["AccountID"].ToString(), Dt_MessageResponse.Rows[i]["TransactionID"].ToString(), null);
                        if (DtNPPAlert == null || DtNPPAlert.Rows.Count == 0)
                        {
                            dtListUpdateStatus.Rows[dtListUpdateStatus.Rows.Count - 1]["Status"] = "Fail";
                            DBCommand.Transaction.Rollback();
                            if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                            Message = "Fail to save List for Rep Id : " + Dt_MessageResponse.Rows[i]["RepId"].ToString();
                            return 2;
                        }
                        else
                        {
                            SQLQuery.Append("Update MessageForNPPAlert ");
                            if (DtNPPAlert.Rows[0]["RepRequest"].ToString() == "1")
                                SQLQuery.Append("Set RepwillVisit=@RepwillVist ");
                            if (DtNPPAlert.Rows[0]["SamplesRequest"].ToString() == "1" && DtNPPAlert.Rows[0]["RepRequest"].ToString() == "1")
                                SQLQuery.Append(" , SampleToBeSent=@SampleToBeSent ");
                            else if (DtNPPAlert.Rows[0]["SamplesRequest"].ToString() == "1")
                                SQLQuery.Append(" Set SampleToBeSent=@SampleToBeSent ");
                            else if (DtNPPAlert.Rows[0]["RepRequest"].ToString() == "0" && DtNPPAlert.Rows[0]["SamplesRequest"].ToString() == "0")
                                SQLQuery.Append("Set RepwillVisit=@RepwillVist, SampleToBeSent=@SampleToBeSent ");
                            SQLQuery.Append(" , ResponseUpdateDateTime=@ResponseUpdateDateTime ");
                            SQLQuery.Append("WHERE  ZSP_ID=@ZSPID AND TransitionID=@TransactionId ");
                            DBCommand.CommandText = SQLQuery.ToString();
                            if (DtNPPAlert.Rows[0]["RepRequest"].ToString() == "1")
                                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@RepwillVist", DbType.String, StrResponse[0]));
                            if (DtNPPAlert.Rows[0]["SamplesRequest"].ToString() == "1" && DtNPPAlert.Rows[0]["RepRequest"].ToString() == "1")
                                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@SampleToBeSent", DbType.String, StrResponse[1]));
                            else if (DtNPPAlert.Rows[0]["SamplesRequest"].ToString() == "1")
                                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@SampleToBeSent", DbType.String, StrResponse[0]));
                            else if (DtNPPAlert.Rows[0]["RepRequest"].ToString() == "0" && DtNPPAlert.Rows[0]["SamplesRequest"].ToString() == "0")
                            {
                                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@RepwillVist", DbType.String, StrResponse[0]));
                                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@SampleToBeSent", DbType.String, StrResponse[1]));
                            }
                            DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@ResponseUpdateDateTime", DbType.DateTime, DateTime.Now));
                            DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@ZSPID", DbType.String, dS_Response.Tables[0].Rows[0]["AccountID"].ToString()));
                            DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@TransactionId", DbType.String, Dt_MessageResponse.Rows[i]["TransactionId"].ToString()));
                            NoOfRowsEffected = DBCommand.ExecuteNonQuery();

                            if (NoOfRowsEffected == 1)
                            {
                                dtListUpdateStatus.Rows[dtListUpdateStatus.Rows.Count - 1]["Status"] = "Success";
                                cnt++;

                                //                            //string FinalResponse = "";

                                //                            //if (StrResponse[1] == "Yes")
                                //                            //{
                                //                            //    FinalResponse = "I am Available to visit this physian within the next three days : Yes";
                                //                            //}
                                //                            //else if (StrResponse[3] == "Y")
                                //                            //{
                                //                            //    FinalResponse = "I am Available to drop of samples within the next three days : Yes";
                                //                            //}

                                //                            String Subject = "HCP NPP Opt-In Response From " + Dt_MessageResponse.Rows[i]["RepId"].ToString();
                                //                            String Body = @"<html><head></head><body>
                                //
                                //                                                        " + DtAccount.Rows[0]["NameFull"].ToString() + @"<br>
                                //
                                //                                                        " + DtAccount.Rows[0]["Address"].ToString() + @"<br>
                                //
                                //                                                        Specialty : " + DtAccount.Rows[0]["PrimarySpecialtyDescription"].ToString() + @"<br>
                                //
                                //                                                        Tier : " + DtAccount.Rows[0]["Territory_Tier"].ToString() + @"<br/><br />
                                //                                                        
                                //                                                        I am Available to visit this physian within the next three days : " + StrResponse[0] + @"<br />
                                //
                                //                                                        I am Available to drop of samples within the next three days : " + StrResponse[1] + @"<br />
                                //
                                //                                    </body></html>";

                                //                            Byte result = objEmailSender.SendMail_Air(dtEmailAccounts.Rows[0]["EmailAccount"].ToString(), "ZS Pharma",
                                //                                                                                dtEmailAccounts.Rows[0]["Password"].ToString(),
                                //                                                                                "dhanunjay132@gmail.com",
                                //                                                                                dtEmailAccounts.Rows[0]["SendServerName"].ToString(),
                                //                                                                                Convert.ToInt32(dtEmailAccounts.Rows[0]["SendPortNum"]),
                                //                                                                                Convert.ToBoolean(dtEmailAccounts.Rows[0]["SendsslEnabled"].ToString() == "Y" ? true : false),
                                //                                                                               null, Subject, true, Body, null, ref Message);
                                //                            //Emails kosta@phipublications.com,pradeep@flexiwaresolutions.com,
                                //                            if (result == 1)
                                //                            {
                                //                                dtListUpdateStatus.Rows[dtListUpdateStatus.Rows.Count - 1]["Status"] = "Success";
                                //                                cnt++;
                                //                            }
                                //                            else
                                //                            {

                                //                                dtListUpdateStatus.Rows[dtListUpdateStatus.Rows.Count - 1]["Status"] = "Fail";
                                //                                DBCommand.Transaction.Rollback();
                                //                                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                                //                                Message = "Fail to save List for Rep Id : " + Dt_MessageResponse.Rows[i]["RepId"].ToString();
                                //                                return 2;
                                //                            }
                            }

                            else
                            {
                                dtListUpdateStatus.Rows[dtListUpdateStatus.Rows.Count - 1]["Status"] = "Fail";
                                DBCommand.Transaction.Rollback();
                                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                                Message = "Fail to save List for Rep Id : " + Dt_MessageResponse.Rows[i]["RepId"].ToString();
                                return 2;
                            }
                        }
                    }

                    if (cnt == Dt_MessageResponse.Rows.Count)
                    {
                        DBCommand.Transaction.Commit();
                        if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                        Message = "Data Saved succesfully.";
                        return 1;
                    }
                    else
                    {
                        DBCommand.Transaction.Rollback();
                        if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                        Message = "Fail to Save Data";
                        return 2;
                    }
                }
                else
                {
                    Message = "No Data to Save";
                    return 2;
                }

            }
            catch (Exception ex)
            {
                DBCommand.Transaction.Rollback();
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return 2;
            }
        }

        //By Dhanunjay On 11-10-2017
        public DataTable GetNPPAlertMaster(String AccountID, String TransactionID, String MessageSendFlag)
        {
            try
            {
                DataSet ds_temp = new DataSet();
                DataSet DS = new DataSet();

                String SqlSelect = @" SELECT * FROM MessageForNPPAlert Where ";
                if (MessageSendFlag != null && MessageSendFlag != String.Empty && MessageSendFlag == "Y")
                    SqlSelect += " IsMessageSend='N' ";
                if (AccountID != null && AccountID != String.Empty)
                    SqlSelect += " ZSP_ID=" + AccountID + "";
                if (TransactionID != null && TransactionID != String.Empty)
                    SqlSelect += " AND TransitionID=" + TransactionID + "";
                DBDataAdpterObject.SelectCommand.CommandText = SqlSelect;
                DBDataAdpterObject.SelectCommand.Parameters.Clear();
                DBDataAdpterObject.TableMappings.Clear();
                DBDataAdpterObject.TableMappings.Add("Table", "DtNPPAlertMaster");
                DataSet ds = new DataSet();
                DBDataAdpterObject.Fill(ds);

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count >= 0)
                {
                    return ds.Tables[0];
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                String Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
        }

        //by nitu 30-7-2016
        public DataTable GetParameterMst()
        {
            try
            {
                DBDataAdpterObject.SelectCommand.Parameters.Clear();
                StringBuilder SQLSelect = new StringBuilder();
                SQLSelect.Append(" SELECT  * FROM ParameterMst");

                DBDataAdpterObject.TableMappings.Clear();
                DBDataAdpterObject.TableMappings.Add("Table", "ParameterMst");
                DBDataAdpterObject.SelectCommand.CommandText = SQLSelect.ToString();
                DataSet ds = new DataSet();
                DBDataAdpterObject.Fill(ds);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    // Message = "ParameterMst retrieved succesfully ";
                    return ds.Tables[0];
                }
                else
                {
                    //Message = "ParameterMst not found";
                    return null;
                }
            }
            catch (Exception ex)
            {
                //Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
            finally
            {
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
        }
    }
}
