﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.Utilities;
using System.Data;
using System.Configuration;
using BLL.ExtraUtilities;
using Newtonsoft.Json;
using Kaliido.QuickBlox;
using Kaliido.QuickBlox.Parameters;

namespace BLL.Masters
{
    public class UserMst : ServerBase
    {
        public override SendReceiveJSon ProcessRequest(SendReceiveJSon receive_obj)
        {
            switch (receive_obj.requestObjectInfo.ObjectProfile.MethodName)
            {
                case "GetInitialData":
                    return GetInitialData(receive_obj);
                case "GetUserDetails":
                    return GetUserDetails(receive_obj);
                case "SaveUserData":
                    return SaveUserData(receive_obj);
                default:
                    SendReceiveJSon send_obj = new SendReceiveJSon();
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "Called method not found " + receive_obj.requestObjectInfo.ObjectProfile.MethodName + ". Please contact to administrator.";
                    return send_obj;
            }
        }

        private SendReceiveJSon GetInitialData(SendReceiveJSon receive_obj)
        {
            SendReceiveJSon send_obj = new SendReceiveJSon();
            String Message = String.Empty;

            try
            {
                IDbConnection connection = DBObjectFactory.GetConnectionObject();
                DataTable dtDatabase = new DataTable("tDatabase");
                dtDatabase.Columns.Add("code");
                dtDatabase.Columns.Add("desc");
                connection.ConnectionString = ConfigurationManager.ConnectionStrings["DeGrToolWorkingConnectionString"].ConnectionString;
                dtDatabase.Rows.Add(new Object[] { connection.Database, connection.Database });
                connection.ConnectionString = ConfigurationManager.ConnectionStrings["DeGrToolLiveConnectionString"].ConnectionString;
                dtDatabase.Rows.Add(new Object[] { connection.Database, connection.Database });
                connection = null;
                if (dtDatabase != null && dtDatabase.Rows.Count > 0)
                {
                    send_obj.responseObjectInfo.Status = 1;
                    send_obj.responseObjectInfo.Message = "Initial data retrieved successfully.";
                    send_obj.responseObjectInfo.dt_ReturnedTables = new dynamic[1];
                    send_obj.responseObjectInfo.dt_ReturnedTables[0] = dtDatabase;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                }
                else
                {
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "Initial data not found.";
                    send_obj.responseObjectInfo.dt_ReturnedTables = null;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                }
            }
            catch (Exception ex)
            {
                ServerLog.ExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                send_obj.responseObjectInfo.Status = 2;
                send_obj.responseObjectInfo.Message = ex.Message;
                send_obj.responseObjectInfo.dt_ReturnedTables = null;
            }

            return send_obj;
        }

        private SendReceiveJSon GetUserDetails(SendReceiveJSon receive_obj)
        {
            SendReceiveJSon send_obj = new SendReceiveJSon();
            String Message = String.Empty;

            try
            {
                String database = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("database") && receive_obj.requestObjectInfo.OpArgs["database"] != null && receive_obj.requestObjectInfo.OpArgs["database"].ToString().Trim() != String.Empty)
                    database = receive_obj.requestObjectInfo.OpArgs["database"].ToString().Trim();

                String RepId = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("RepId") && receive_obj.requestObjectInfo.OpArgs["RepId"] != null && receive_obj.requestObjectInfo.OpArgs["RepId"].ToString().Trim() != String.Empty)
                    RepId = receive_obj.requestObjectInfo.OpArgs["RepId"].ToString().Trim();

                ClientUtility objClientUtility = new ClientUtility();
                objClientUtility.SetDatabaseConnectionString(this, database);

                DataTable dtUserList = GetUserDetail(RepId, ref Message);
                if (dtUserList == null || dtUserList.Rows.Count <= 0)
                {
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "User list data not found." + Environment.NewLine + Message;
                    send_obj.responseObjectInfo.dt_ReturnedTables = null;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                    return send_obj;
                }

                DataTable dtRepTerritoryMap = GetRepTerritoryMap(RepId, ref Message);
                if (dtRepTerritoryMap == null || dtRepTerritoryMap.Rows.Count <= 0)
                {
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "RepTerritoryMap data not found." + Environment.NewLine + Message;
                    send_obj.responseObjectInfo.dt_ReturnedTables = null;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                    return send_obj;
                }

                if (dtUserList != null && dtUserList.Rows.Count > 0)
                {
                    if (!dtUserList.Columns.Contains("TerritoryNum"))
                        dtUserList.Columns.Add("TerritoryNum");
                    if (dtRepTerritoryMap != null && dtRepTerritoryMap.Rows.Count > 0)
                    {
                        DataRow[] drArray = null;
                        String TerritoryNum = String.Empty;
                        foreach (DataRow drUser in dtUserList.Rows)
                        {
                            TerritoryNum = String.Empty;
                            drArray = dtRepTerritoryMap.Select("RepId='" + drUser["RepId"].ToString() + "'");
                            if (drArray != null && drArray.Length > 0)
                            {
                                foreach (DataRow dr in drArray)
                                    TerritoryNum += dr["TerritoryNum"].ToString() + " : " + dr["TerritoryName"].ToString() + ", ";

                                if (TerritoryNum.Length > 0)
                                    TerritoryNum = TerritoryNum.Substring(0, TerritoryNum.Length - 2);

                                drUser["TerritoryNum"] = TerritoryNum;
                            }
                        }
                    }
                }

                DataTable dtRegionTerritoryMap = null, dtAccountTypeGroupList = null, dtAttributeOperationGroupList = null;
                if (RepId == null)
                {
                    dtRepTerritoryMap = null;

                    dtRegionTerritoryMap = GetRegionTerritoryMap(null, ref Message);
                    if (dtRegionTerritoryMap == null || dtRegionTerritoryMap.Rows.Count <= 0)
                    {
                        send_obj.responseObjectInfo.Status = 2;
                        send_obj.responseObjectInfo.Message = "Region Territory Map data not found." + Environment.NewLine + Message;
                        send_obj.responseObjectInfo.dt_ReturnedTables = null;
                        send_obj.responseObjectInfo.ObjRetArgs = null;
                        return send_obj;
                    }

                    dtAccountTypeGroupList = GetAccountTypeGroupMst(null, ref Message);
                    if (dtAccountTypeGroupList == null || dtAccountTypeGroupList.Rows.Count <= 0)
                    {
                        send_obj.responseObjectInfo.Status = 2;
                        send_obj.responseObjectInfo.Message = "AccountType Group list data not found." + Environment.NewLine + Message;
                        send_obj.responseObjectInfo.dt_ReturnedTables = null;
                        send_obj.responseObjectInfo.ObjRetArgs = null;
                        return send_obj;
                    }

                    dtAttributeOperationGroupList = GetAttributeOperationGroupMst(null, ref Message);
                    if (dtAttributeOperationGroupList == null || dtAttributeOperationGroupList.Rows.Count <= 0)
                    {
                        send_obj.responseObjectInfo.Status = 2;
                        send_obj.responseObjectInfo.Message = "AttributeOperation Group List data not found." + Environment.NewLine + Message;
                        send_obj.responseObjectInfo.dt_ReturnedTables = null;
                        send_obj.responseObjectInfo.ObjRetArgs = null;
                        return send_obj;
                    }
                }

                send_obj.responseObjectInfo.Status = 1;
                send_obj.responseObjectInfo.Message = "User List retrieved successfully.";
                send_obj.responseObjectInfo.dt_ReturnedTables = new dynamic[5];
                send_obj.responseObjectInfo.dt_ReturnedTables[0] = dtUserList;
                send_obj.responseObjectInfo.dt_ReturnedTables[1] = dtRepTerritoryMap;
                send_obj.responseObjectInfo.dt_ReturnedTables[2] = dtRegionTerritoryMap;
                send_obj.responseObjectInfo.dt_ReturnedTables[3] = dtAccountTypeGroupList;
                send_obj.responseObjectInfo.dt_ReturnedTables[4] = dtAttributeOperationGroupList;
                send_obj.responseObjectInfo.ObjRetArgs = null;
                return send_obj;
            }
            catch (Exception ex)
            {
                ServerLog.ExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                send_obj.responseObjectInfo.Status = 2;
                send_obj.responseObjectInfo.Message = ex.Message;
                send_obj.responseObjectInfo.dt_ReturnedTables = null;
                return send_obj;
            }
        }

        private SendReceiveJSon SaveUserData(SendReceiveJSon receive_obj)
        {
            SendReceiveJSon send_obj = new SendReceiveJSon();
            String Message = String.Empty;

            try
            {
                Dictionary<string, object> dicRecieveData = receive_obj.requestObjectInfo.ds_UploadData;
                String jsonTypedUserMst = null;
                DataSet dS_Config_UserMstClient = null;
                if (dicRecieveData.ContainsKey("New"))
                {
                    jsonTypedUserMst = JsonConvert.SerializeObject(dicRecieveData["New"]);
                    dS_Config_UserMstClient = JsonConvert.DeserializeObject<DataSet>(jsonTypedUserMst);
                }

                #region Primary Validation
                if (dS_Config_UserMstClient == null || dS_Config_UserMstClient.Tables.Count <= 0 || dS_Config_UserMstClient.Tables["Config_UserMst"] == null || dS_Config_UserMstClient.Tables["Config_UserMst"].Rows.Count <= 0 || dS_Config_UserMstClient.Tables["Config_RepTerritoryMap"] == null || dS_Config_UserMstClient.Tables["Config_RepTerritoryMap"].Rows.Count <= 0)
                {
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "No data available to Save User Master. Operation cancelled.";
                    return send_obj;
                }
                #endregion

                String database = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("database") && receive_obj.requestObjectInfo.OpArgs["database"] != null && receive_obj.requestObjectInfo.OpArgs["database"].ToString().Trim() != String.Empty)
                    database = receive_obj.requestObjectInfo.OpArgs["database"].ToString().Trim();

                String RepId = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("RepId") && receive_obj.requestObjectInfo.OpArgs["RepId"] != null && receive_obj.requestObjectInfo.OpArgs["RepId"].ToString().Trim() != String.Empty)
                    RepId = receive_obj.requestObjectInfo.OpArgs["RepId"].ToString().Trim();

                String NewEdit = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("NewEdit") && receive_obj.requestObjectInfo.OpArgs["NewEdit"] != null && receive_obj.requestObjectInfo.OpArgs["NewEdit"].ToString().Trim() != String.Empty)
                    NewEdit = receive_obj.requestObjectInfo.OpArgs["NewEdit"].ToString().Trim();

                ClientUtility objClientUtility = new ClientUtility();
                objClientUtility.SetDatabaseConnectionString(this, database);
                if (DBConnection.State == ConnectionState.Open)
                    DBConnection.Close();
                DBConnection.Open();
                DBCommand.Transaction = DBConnection.BeginTransaction();

                //Config_UserMst
                DataTable dtConfig_UserMstClient = dS_Config_UserMstClient.Tables["Config_UserMst"];
                DataTable dtConfig_UserMstServer = objClientUtility.GetTableSchema(ref DBDataAdpterObject, dtConfig_UserMstClient.TableName, ref Message);
                dtConfig_UserMstServer.PrimaryKey = new DataColumn[] { dtConfig_UserMstServer.Columns["RepId"] };
                dtConfig_UserMstServer.ImportRow(dtConfig_UserMstClient.Rows[0]);
                dtConfig_UserMstServer.Rows[0]["Designation"] = dtConfig_UserMstServer.Rows[0]["Designation"].ToString().ToUpper();
                if (NewEdit == "New")
                {

                    //user quickblox registration
                    string BlobId = DoUserRegistrationQuickBlox(dtConfig_UserMstServer.Rows[0]["RepName"].ToString(), dtConfig_UserMstServer.Rows[0]["Email"].ToString(), "admin@123");

                    clsEncryptionDecryption objclsEncryptionDecryption = new clsEncryptionDecryption();
                    String EncryptPassword = String.Empty;
                    if (dtConfig_UserMstServer.Rows[0]["Password"].ToString().Trim() != String.Empty)
                    {
                        EncryptPassword = objclsEncryptionDecryption.encryptPassword(dtConfig_UserMstServer.Rows[0]["Password"].ToString());
                        if (EncryptPassword != null && EncryptPassword.Trim() != String.Empty)
                            dtConfig_UserMstServer.Rows[0]["Password"] = EncryptPassword;
                    }
                    dtConfig_UserMstServer.Rows[0]["CreatedBy"] = "ADMIN";
                    dtConfig_UserMstServer.Rows[0]["CreatedDate"] = DateTime.Now;
                    dtConfig_UserMstServer.Rows[0]["CreatedHost"] = "10.0.0.0";

                    dtConfig_UserMstServer.Rows[0]["BlobId"] = BlobId;
                    dtConfig_UserMstServer.Rows[0].AcceptChanges();
                    dtConfig_UserMstServer.Rows[0].SetAdded();
                }
                else if (NewEdit == "Edit")
                {
                    dtConfig_UserMstServer.Rows[0]["LastModifiedBy"] = "ADMIN";
                    dtConfig_UserMstServer.Rows[0]["LastModifiedDate"] = DateTime.Now;
                    dtConfig_UserMstServer.Rows[0]["LastModifiedHost"] = "10.0.0.0";
                    dtConfig_UserMstServer.Rows[0].AcceptChanges();
                    dtConfig_UserMstServer.Rows[0].SetModified();
                }

                //Config_RepTerritoryMap
                int index;
                DataTable dtConfig_RepTerritoryMapClient = dS_Config_UserMstClient.Tables["Config_RepTerritoryMap"];
                DataTable dtConfig_RepTerritoryMapServer = objClientUtility.GetTableSchema(ref DBDataAdpterObject, dtConfig_RepTerritoryMapClient.TableName, ref Message);
                dtConfig_RepTerritoryMapServer.PrimaryKey = new DataColumn[] { dtConfig_RepTerritoryMapServer.Columns["RepId"], dtConfig_RepTerritoryMapServer.Columns["TerritoryNum"] };
                foreach (DataRow dr in dtConfig_RepTerritoryMapClient.Rows)
                {
                    dtConfig_RepTerritoryMapServer.ImportRow(dr);
                    index = dtConfig_RepTerritoryMapServer.Rows.Count - 1;
                    dtConfig_RepTerritoryMapServer.Rows[index]["RepId"] = RepId;
                }

                BLGeneralUtil.UpdateTableInfo utf = BLGeneralUtil.UpdateTable(ref DBCommand, dtConfig_UserMstServer, BLGeneralUtil.UpdateWhereMode.KeyColumnsOnly);
                if (!utf.Status || utf.TotalRowsAffected != dtConfig_UserMstServer.Rows.Count)
                {
                    DBCommand.Transaction.Rollback();
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "An error occurred while Save User data. " + utf.ErrorMessage;
                    send_obj.responseObjectInfo.dt_ReturnedTables = null;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                }
                else
                {
                    DBCommand.Parameters.Clear();
                    DBCommand.CommandText = "DELETE FROM Config_RepTerritoryMap WHERE RepId=@RepId";
                    DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@RepId", DbType.String, RepId));
                    int NoOfRowsDelete = DBCommand.ExecuteNonQuery();

                    utf = BLGeneralUtil.UpdateTable(ref DBCommand, dtConfig_RepTerritoryMapServer, BLGeneralUtil.UpdateWhereMode.KeyColumnsOnly);
                    if (!utf.Status || utf.TotalRowsAffected != dtConfig_RepTerritoryMapServer.Rows.Count)
                    {
                        DBCommand.Transaction.Rollback();
                        send_obj.responseObjectInfo.Status = 2;
                        send_obj.responseObjectInfo.Message = "An error occurred while Save Rep Territory Map data. " + utf.ErrorMessage;
                        send_obj.responseObjectInfo.dt_ReturnedTables = null;
                        send_obj.responseObjectInfo.ObjRetArgs = null;
                    }
                    else
                    {
                        DBCommand.Transaction.Commit();
                        DataTable dtUserList = GetUserDetail(null, ref Message);
                        DataTable dtRepTerritoryMap = GetRepTerritoryMap(null, ref Message);
                        if (dtUserList != null && dtUserList.Rows.Count > 0)
                        {
                            if (!dtUserList.Columns.Contains("TerritoryNum"))
                                dtUserList.Columns.Add("TerritoryNum");
                            if (dtRepTerritoryMap != null && dtRepTerritoryMap.Rows.Count > 0)
                            {
                                DataRow[] drArray = null;
                                String TerritoryNum = String.Empty;
                                foreach (DataRow drUser in dtUserList.Rows)
                                {
                                    TerritoryNum = String.Empty;
                                    drArray = dtRepTerritoryMap.Select("RepId='" + drUser["RepId"].ToString() + "'");
                                    if (drArray != null && drArray.Length > 0)
                                    {
                                        foreach (DataRow dr in drArray)
                                            TerritoryNum += dr["TerritoryNum"].ToString() + " : " + dr["TerritoryName"].ToString() + ", ";

                                        if (TerritoryNum.Length > 0)
                                            TerritoryNum = TerritoryNum.Substring(0, TerritoryNum.Length - 2);

                                        drUser["TerritoryNum"] = TerritoryNum;
                                    }
                                }
                            }
                        }
                        send_obj.responseObjectInfo.Status = 1;
                        send_obj.responseObjectInfo.Message = "User data saved successfully.";
                        send_obj.responseObjectInfo.dt_ReturnedTables = new dynamic[1];
                        send_obj.responseObjectInfo.dt_ReturnedTables[0] = dtUserList;
                        send_obj.responseObjectInfo.ObjRetArgs = null;

                    }
                }
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
            catch (Exception ex)
            {
                ServerLog.ExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                send_obj.responseObjectInfo.Status = 2;
                send_obj.responseObjectInfo.Message = ex.Message;
                send_obj.responseObjectInfo.dt_ReturnedTables = null;
            }

            return send_obj;
        }

        private DataTable GetUserDetail(String RepId, ref String Message)
        {
            try
            {
                DBDataAdpterObject.SelectCommand.Parameters.Clear();
                StringBuilder SQLSelect = new StringBuilder();
                SQLSelect.Append(@" SELECT   Config_UserMst.*, Config_AccountTypeGroupMst.AccountTypeGroupDesc, Config_AttributeOperationGroupMst.AttributeOperationGroupDesc  
                                    FROM     Config_UserMst 
                                    LEFT OUTER JOIN Config_AccountTypeGroupMst ON Config_UserMst.AccountTypeGroupId = Config_AccountTypeGroupMst.AccountTypeGroupId 
                                    LEFT OUTER JOIN Config_AttributeOperationGroupMst ON Config_UserMst.AttributeOperationGroupId = Config_AttributeOperationGroupMst.AttributeOperationGroupId 
                                    WHERE    1=1 ");
                if (RepId != null && RepId.Trim() != String.Empty)
                {
                    SQLSelect.Append(" AND    (RepId = @RepId) ");
                    DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@RepId", DbType.String, RepId));
                }
                DBDataAdpterObject.SelectCommand.CommandText = SQLSelect.ToString();
                DBDataAdpterObject.TableMappings.Clear();
                DBDataAdpterObject.TableMappings.Add("Table", "Config_UserMst");
                DataSet ds = new DataSet();
                DBDataAdpterObject.Fill(ds);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    clsEncryptionDecryption objclsEncryptionDecryption = new clsEncryptionDecryption();
                    String DecryptPassword = String.Empty;
                    if (!ds.Tables[0].Columns.Contains("DecryptPassword"))
                        ds.Tables[0].Columns.Add("DecryptPassword");
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        try
                        {
                            if (dr["Password"].ToString().Trim() != String.Empty)
                            {
                                DecryptPassword = objclsEncryptionDecryption.decryptPassword(dr["Password"].ToString().Trim());
                                if (DecryptPassword != null && DecryptPassword.Trim() != String.Empty)
                                    dr["DecryptPassword"] = DecryptPassword;
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }

                    Message = "Rep detail retrieved succesfully for Rep Id : " + RepId;
                    return ds.Tables[0];
                }
                else
                {
                    Message = "Rep detail not found for Rep Id : " + RepId;
                    return null;
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
            finally
            {
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
        }

        private DataTable GetRepTerritoryMap(String RepId, ref String Message)
        {
            try
            {
                DBDataAdpterObject.SelectCommand.Parameters.Clear();
                StringBuilder SQLSelect = new StringBuilder();
                SQLSelect.Append(@" SELECT   Config_RepTerritoryMap.*, Config_RegionTerritoryMap.TerritoryName 
                                    FROM     Config_RepTerritoryMap 
                                    LEFT OUTER JOIN Config_RegionTerritoryMap ON Config_RepTerritoryMap.TerritoryNum = Config_RegionTerritoryMap.TerritoryNum 
                                    WHERE    1=1 ");
                if (RepId != null && RepId.Trim() != String.Empty)
                {
                    SQLSelect.Append(" AND    (RepId = @RepId) ");
                    DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@RepId", DbType.String, RepId));
                }
                DBDataAdpterObject.SelectCommand.CommandText = SQLSelect.ToString();
                DBDataAdpterObject.TableMappings.Clear();
                DBDataAdpterObject.TableMappings.Add("Table", "Config_RepTerritoryMap");
                DataSet ds = new DataSet();
                DBDataAdpterObject.Fill(ds);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    Message = "RepTerritoryMap detail retrieved succesfully for Rep Id : " + RepId;
                    return ds.Tables[0];
                }
                else
                {
                    Message = "RepTerritoryMap detail not found for AccountType Rep Id : " + RepId;
                    return null;
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
            finally
            {
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
        }

        private DataTable GetRegionTerritoryMap(String TerritoryNum, ref String Message)
        {
            try
            {
                DBDataAdpterObject.SelectCommand.Parameters.Clear();
                StringBuilder SQLSelect = new StringBuilder();
                SQLSelect.Append(@" SELECT   DISTINCT TerritoryNum, TerritoryName 
                                    FROM     Config_RegionTerritoryMap 
                                    WHERE    1=1 ");
                if (TerritoryNum != null && TerritoryNum.Trim() != String.Empty)
                {
                    SQLSelect.Append(" AND    (TerritoryNum = @TerritoryNum) ");
                    DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@TerritoryNum", DbType.String, TerritoryNum));
                }
                DBDataAdpterObject.SelectCommand.CommandText = SQLSelect.ToString();
                DBDataAdpterObject.TableMappings.Clear();
                DBDataAdpterObject.TableMappings.Add("Table", "Config_RegionTerritoryMap");
                DataSet ds = new DataSet();
                DBDataAdpterObject.Fill(ds);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    Message = "RegionTerritoryMap detail retrieved succesfully for TerritoryNum : " + TerritoryNum;
                    return ds.Tables[0];
                }
                else
                {
                    Message = "RegionTerritoryMap detail not found for AccountType TerritoryNum : " + TerritoryNum;
                    return null;
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
            finally
            {
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
        }

        private DataTable GetAccountTypeGroupMst(String AccountTypeGroupId, ref String Message)
        {
            try
            {
                DBDataAdpterObject.SelectCommand.Parameters.Clear();
                StringBuilder SQLSelect = new StringBuilder();
                SQLSelect.Append(@" SELECT   *  
                                    FROM     Config_AccountTypeGroupMst 
                                    WHERE    1=1 ");
                if (AccountTypeGroupId != null && AccountTypeGroupId.Trim() != String.Empty)
                {
                    SQLSelect.Append(" AND    (AccountTypeGroupId = @AccountTypeGroupId) ");
                    DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@AccountTypeGroupId", DbType.String, AccountTypeGroupId));
                }
                DBDataAdpterObject.SelectCommand.CommandText = SQLSelect.ToString();
                DBDataAdpterObject.TableMappings.Clear();
                DBDataAdpterObject.TableMappings.Add("Table", "Config_AccountTypeGroupMst");
                DataSet ds = new DataSet();
                DBDataAdpterObject.Fill(ds);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    Message = "AccountType Group detail retrieved succesfully for AccountType Group Id : " + AccountTypeGroupId;
                    return ds.Tables[0];
                }
                else
                {
                    Message = "AccountType Group detail not found for AccountType Group Id : " + AccountTypeGroupId;
                    return null;
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
            finally
            {
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
        }

        private DataTable GetAttributeOperationGroupMst(String AttributeOperationGroupId, ref String Message)
        {
            try
            {
                DBDataAdpterObject.SelectCommand.Parameters.Clear();
                StringBuilder SQLSelect = new StringBuilder();
                SQLSelect.Append(@" SELECT   *  
                                    FROM     Config_AttributeOperationGroupMst 
                                    WHERE    1=1 ");
                if (AttributeOperationGroupId != null && AttributeOperationGroupId.Trim() != String.Empty)
                {
                    SQLSelect.Append(" AND    (AttributeOperationGroupId = @AttributeOperationGroupId) ");
                    DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@AttributeOperationGroupId", DbType.String, AttributeOperationGroupId));
                }
                DBDataAdpterObject.SelectCommand.CommandText = SQLSelect.ToString();
                DBDataAdpterObject.TableMappings.Clear();
                DBDataAdpterObject.TableMappings.Add("Table", "Config_AttributeOperationGroupMst");
                DataSet ds = new DataSet();
                DBDataAdpterObject.Fill(ds);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].NewRow(); //Create New Row
                    dr["AttributeOperationGroupId"] = "Superadmin";              // Set Column Value
                    dr["AttributeOperationGroupDesc"] = "SuperAdmin";              // Set Column Value
                    ds.Tables[0].Rows.InsertAt(dr, 0);

                    Message = "AttributeOperation Group detail retrieved succesfully for AttributeOperation Group Id : " + AttributeOperationGroupId;
                    return ds.Tables[0];
                }
                else
                {
                    Message = "AttributeOperation Group detail not found for AttributeOperation Group Id : " + AttributeOperationGroupId;
                    return null;
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
            finally
            {
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
        }

        #region QuickBlox

        public string Timestamp()
        {
            long ticks = DateTime.UtcNow.Ticks - DateTime.Parse("01/01/1970 00:00:00").Ticks;
            ticks /= 10000000;
            return ticks.ToString();
        }

        public string DoUserRegistrationQuickBlox(string name, string email, string password)
        {
            try
            {
                //register user

                ResponseObjectInfo objResponseObjectInfo = new ResponseObjectInfo();

                string application_id = "41097";
                string auth_key = "zKpcr4Xzdzevpr7";
                string timestamp = Timestamp();
                string nonce = new Random().Next().ToString();
                string auth_secret = "47-FnCPYP7rz9CH";
                //string setAccountKey = "ckY9GmG3pkzKZr76RKh1";


                string result = null;
                var apiCredentials = new ApiCredential(application_id, auth_key, auth_secret);
                var adminCredentials = new UserCredential() { UserLogin = "quotes4transport", Password = "Matt2105*" };
                QuickbloxRestApi client = new QuickbloxRestApi(apiCredentials, adminCredentials);
                UserParameters UserParam = new UserParameters();

                
                UserParam.Email = email;
                UserParam.Password = password;
                UserParam.FullName = name;
                UserParam.Login = email;

                var users = client.RegisterAsync(UserParam);

                //objResponseObjectInfo.Status = 1;
                //objResponseObjectInfo.Message = "Success";
                //objResponseObjectInfo.dt_ReturnedTables = new dynamic[1];
                //objResponseObjectInfo.dt_ReturnedTables[0] = users;

                return users;
            }
            catch (Exception ex)
            {
                return null;

            }

        }

        #endregion
    }
}
