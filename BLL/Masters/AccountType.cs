﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.Utilities;
using System.Data;
using System.Configuration;
using BLL.ExtraUtilities;
using Newtonsoft.Json;

namespace BLL.Masters
{
    public class AccountType : ServerBase
    {
        public override SendReceiveJSon ProcessRequest(SendReceiveJSon receive_obj)
        {
            switch (receive_obj.requestObjectInfo.ObjectProfile.MethodName)
            {
                case "GetInitialData":
                    return GetInitialData(receive_obj);
                case "GetAccountDetails":
                    return GetAccountDetails(receive_obj);
                case "SaveAccountData":
                    return SaveAccountData(receive_obj);
                default:
                    SendReceiveJSon send_obj = new SendReceiveJSon();
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "Called method not found " + receive_obj.requestObjectInfo.ObjectProfile.MethodName + ". Please contact to administrator.";
                    return send_obj;
            }
        }

        private SendReceiveJSon GetInitialData(SendReceiveJSon receive_obj)
        {
            SendReceiveJSon send_obj = new SendReceiveJSon();
            String Message = String.Empty;

            try
            {
                IDbConnection connection = DBObjectFactory.GetConnectionObject();
                DataTable dtDatabase = new DataTable("tDatabase");
                dtDatabase.Columns.Add("code");
                dtDatabase.Columns.Add("desc");
                connection.ConnectionString = ConfigurationManager.ConnectionStrings["DeGrToolWorkingConnectionString"].ConnectionString;
                dtDatabase.Rows.Add(new Object[] { connection.Database, connection.Database });
                connection.ConnectionString = ConfigurationManager.ConnectionStrings["DeGrToolLiveConnectionString"].ConnectionString;
                dtDatabase.Rows.Add(new Object[] { connection.Database, connection.Database });
                connection = null;
                if (dtDatabase != null && dtDatabase.Rows.Count > 0)
                {
                    send_obj.responseObjectInfo.Status = 1;
                    send_obj.responseObjectInfo.Message = "Initial data retrieved successfully.";
                    send_obj.responseObjectInfo.dt_ReturnedTables = new dynamic[1];
                    send_obj.responseObjectInfo.dt_ReturnedTables[0] = dtDatabase;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                }
                else
                {
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "Initial data not found.";
                    send_obj.responseObjectInfo.dt_ReturnedTables = null;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                }
            }
            catch (Exception ex)
            {
                ServerLog.ExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                send_obj.responseObjectInfo.Status = 2;
                send_obj.responseObjectInfo.Message = ex.Message;
                send_obj.responseObjectInfo.dt_ReturnedTables = null;
            }

            return send_obj;
        }

        private SendReceiveJSon GetAccountDetails(SendReceiveJSon receive_obj)
        {
            SendReceiveJSon send_obj = new SendReceiveJSon();
            String Message = String.Empty;

            try
            {
                String database = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("database") && receive_obj.requestObjectInfo.OpArgs["database"] != null && receive_obj.requestObjectInfo.OpArgs["database"].ToString().Trim() != String.Empty)
                    database = receive_obj.requestObjectInfo.OpArgs["database"].ToString().Trim();

                String AccountType = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("AccountType") && receive_obj.requestObjectInfo.OpArgs["AccountType"] != null && receive_obj.requestObjectInfo.OpArgs["AccountType"].ToString().Trim() != String.Empty)
                    AccountType = receive_obj.requestObjectInfo.OpArgs["AccountType"].ToString().Trim();

                ClientUtility objClientUtility = new ClientUtility();
                objClientUtility.SetDatabaseConnectionString(this, database);

                DataTable dtAccountList = GetAccountDetail(AccountType, ref Message);
                if (dtAccountList == null || dtAccountList.Rows.Count <= 0)
                {
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "Account list data not found." + Environment.NewLine + Message;
                    send_obj.responseObjectInfo.dt_ReturnedTables = null;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                    return send_obj;
                }

                DataTable dtCategory = GetAccountCategoryMap(AccountType, ref Message);
                if (dtCategory == null || dtCategory.Rows.Count <= 0)
                {
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "RepTerritoryMap data not found." + Environment.NewLine + Message;
                    send_obj.responseObjectInfo.dt_ReturnedTables = null;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                    return send_obj;
                }

                DataTable dtAccountTypeRights = GetAccountTypeGroupMst(null, ref Message);
                if (dtAccountTypeRights == null || dtAccountTypeRights.Rows.Count <= 0)
                {
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "Account Type rights not found." + Environment.NewLine + Message;
                    send_obj.responseObjectInfo.dt_ReturnedTables = null;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                    return send_obj;
                }
                //if (dtAccountList != null && dtAccountList.Rows.Count > 0)
                //{
                //    if (!dtAccountList.Columns.Contains("TerritoryNum"))
                //        dtAccountList.Columns.Add("TerritoryNum");
                //    if (dtRepTerritoryMap != null && dtRepTerritoryMap.Rows.Count > 0)
                //    {
                //        DataRow[] drArray = null;
                //        String TerritoryNum = String.Empty;
                //        foreach (DataRow drAccount in dtAccountList.Rows)
                //        {
                //            TerritoryNum = String.Empty;
                //            drArray = dtRepTerritoryMap.Select("RepId='" + drAccount["RepId"].ToString() + "'");
                //            if (drArray != null && drArray.Length > 0)
                //            {
                //                foreach (DataRow dr in drArray)
                //                    TerritoryNum += dr["TerritoryNum"].ToString() + " : " + dr["TerritoryName"].ToString() + ", ";

                //                if (TerritoryNum.Length > 0)
                //                    TerritoryNum = TerritoryNum.Substring(0, TerritoryNum.Length - 2);

                //                drAccount["TerritoryNum"] = TerritoryNum;
                //            }
                //        }
                //    }
                //}

                //DataTable dtRegionTerritoryMap = null, dtAccountTypeGroupList = null, dtAttributeOperationGroupList = null;
                //if (AccountType == null)
                //{
                //    dtRepTerritoryMap = null;

                //    dtRegionTerritoryMap = GetRegionTerritoryMap(null, ref Message);
                //    if (dtRegionTerritoryMap == null || dtRegionTerritoryMap.Rows.Count <= 0)
                //    {
                //        send_obj.responseObjectInfo.Status = 2;
                //        send_obj.responseObjectInfo.Message = "Region Territory Map data not found." + Environment.NewLine + Message;
                //        send_obj.responseObjectInfo.dt_ReturnedTables = null;
                //        send_obj.responseObjectInfo.ObjRetArgs = null;
                //        return send_obj;
                //    }

                //    dtAccountTypeGroupList = GetAccountTypeGroupMst(null, ref Message);
                //    if (dtAccountTypeGroupList == null || dtAccountTypeGroupList.Rows.Count <= 0)
                //    {
                //        send_obj.responseObjectInfo.Status = 2;
                //        send_obj.responseObjectInfo.Message = "AccountType Group list data not found." + Environment.NewLine + Message;
                //        send_obj.responseObjectInfo.dt_ReturnedTables = null;
                //        send_obj.responseObjectInfo.ObjRetArgs = null;
                //        return send_obj;
                //    }

                //    dtAttributeOperationGroupList = GetAttributeOperationGroupMst(null, ref Message);
                //    if (dtAttributeOperationGroupList == null || dtAttributeOperationGroupList.Rows.Count <= 0)
                //    {
                //        send_obj.responseObjectInfo.Status = 2;
                //        send_obj.responseObjectInfo.Message = "AttributeOperation Group List data not found." + Environment.NewLine + Message;
                //        send_obj.responseObjectInfo.dt_ReturnedTables = null;
                //        send_obj.responseObjectInfo.ObjRetArgs = null;
                //        return send_obj;
                //    }
                //}

                send_obj.responseObjectInfo.Status = 1;
                send_obj.responseObjectInfo.Message = "Account List retrieved successfully.";
                send_obj.responseObjectInfo.dt_ReturnedTables = new dynamic[5];
                send_obj.responseObjectInfo.dt_ReturnedTables[0] = dtAccountList;
                send_obj.responseObjectInfo.dt_ReturnedTables[1] = dtCategory;
                send_obj.responseObjectInfo.dt_ReturnedTables[2] = dtAccountTypeRights;
                //send_obj.responseObjectInfo.dt_ReturnedTables[3] = dtAccountTypeGroupList;
                //send_obj.responseObjectInfo.dt_ReturnedTables[4] = dtAttributeOperationGroupList;
                send_obj.responseObjectInfo.ObjRetArgs = null;
                return send_obj;
            }
            catch (Exception ex)
            {
                ServerLog.ExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                send_obj.responseObjectInfo.Status = 2;
                send_obj.responseObjectInfo.Message = ex.Message;
                send_obj.responseObjectInfo.dt_ReturnedTables = null;
                return send_obj;
            }
        }

        private SendReceiveJSon SaveAccountData(SendReceiveJSon receive_obj)
        {
            SendReceiveJSon send_obj = new SendReceiveJSon();
            String Message = String.Empty;

            try
            {
                Dictionary<string, object> dicRecieveData = receive_obj.requestObjectInfo.ds_UploadData;
                String jsonTypedUserMst = null;
                DataSet dS_Config_AccountTypeMstClient = null;
                if (dicRecieveData.ContainsKey("New"))
                {
                    jsonTypedUserMst = JsonConvert.SerializeObject(dicRecieveData["New"]);
                    dS_Config_AccountTypeMstClient = JsonConvert.DeserializeObject<DataSet>(jsonTypedUserMst);
                }

                #region Primary Validation
                if (dS_Config_AccountTypeMstClient == null || dS_Config_AccountTypeMstClient.Tables.Count <= 0 || dS_Config_AccountTypeMstClient.Tables["Config_AccountTypeMst"] == null || dS_Config_AccountTypeMstClient.Tables["Config_AccountTypeMst"].Rows.Count <= 0)
                {
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "No data available to Save User Master. Operation cancelled.";
                    return send_obj;
                }

                #endregion

                String database = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("database") && receive_obj.requestObjectInfo.OpArgs["database"] != null && receive_obj.requestObjectInfo.OpArgs["database"].ToString().Trim() != String.Empty)
                    database = receive_obj.requestObjectInfo.OpArgs["database"].ToString().Trim();

                String AccountType = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("AccountType") && receive_obj.requestObjectInfo.OpArgs["AccountType"] != null && receive_obj.requestObjectInfo.OpArgs["AccountType"].ToString().Trim() != String.Empty)
                    AccountType = receive_obj.requestObjectInfo.OpArgs["AccountType"].ToString().Trim();

                String NewEdit = null;
                if (receive_obj.requestObjectInfo.OpArgs.ContainsKey("NewEdit") && receive_obj.requestObjectInfo.OpArgs["NewEdit"] != null && receive_obj.requestObjectInfo.OpArgs["NewEdit"].ToString().Trim() != String.Empty)
                    NewEdit = receive_obj.requestObjectInfo.OpArgs["NewEdit"].ToString().Trim();

                ClientUtility objClientUtility = new ClientUtility();
                objClientUtility.SetDatabaseConnectionString(this, database);
                if (DBConnection.State == ConnectionState.Open)
                    DBConnection.Close();
                DBConnection.Open();
                DBCommand.Transaction = DBConnection.BeginTransaction();

                //Config_UserMst
                DataTable dtConfig_AccountTypeMstClient = dS_Config_AccountTypeMstClient.Tables["Config_AccountTypeMst"];
                DataTable dtConfig_AccountTypeMstServer = objClientUtility.GetTableSchema(ref DBDataAdpterObject, dtConfig_AccountTypeMstClient.TableName, ref Message);
                dtConfig_AccountTypeMstServer.PrimaryKey = new DataColumn[] { dtConfig_AccountTypeMstServer.Columns["AccountType"] };
                dtConfig_AccountTypeMstServer.ImportRow(dtConfig_AccountTypeMstClient.Rows[0]);
                //dtConfig_AccountTypeMstServer.Rows[0]["Designation"] = dtConfig_AccountTypeMstServer.Rows[0]["Designation"].ToString().ToUpper();
                if (NewEdit == "New")
                {
                    //user quickblox registration
                    //string BlobId = DoUserRegistrationQuickBlox(dtConfig_AccountTypeMstServer.Rows[0]["RepName"].ToString(), dtConfig_AccountTypeMstServer.Rows[0]["Email"].ToString(), "admin@123");

                    //clsEncryptionDecryption objclsEncryptionDecryption = new clsEncryptionDecryption();
                    //String EncryptPassword = String.Empty;
                    //if (dtConfig_AccountTypeMstServer.Rows[0]["Password"].ToString().Trim() != String.Empty)
                    //{
                    //    EncryptPassword = objclsEncryptionDecryption.encryptPassword(dtConfig_AccountTypeMstServer.Rows[0]["Password"].ToString());
                    //    if (EncryptPassword != null && EncryptPassword.Trim() != String.Empty)
                    //        dtConfig_AccountTypeMstServer.Rows[0]["Password"] = EncryptPassword;
                    //}
                    // dtConfig_AccountTypeMstServer.Rows[0]["CreatedBy"] = "ADMIN";
                    dtConfig_AccountTypeMstServer.Rows[0]["LastUpdatedDateTime"] = DateTime.Now;
                    //dtConfig_AccountTypeMstServer.Rows[0]["CreatedHost"] = "10.0.0.0";

                    //dtConfig_AccountTypeMstServer.Rows[0]["BlobId"] = null;
                    dtConfig_AccountTypeMstServer.Rows[0].AcceptChanges();
                    dtConfig_AccountTypeMstServer.Rows[0].SetAdded();
                }
                else if (NewEdit == "Edit")
                {
                    // dtConfig_AccountTypeMstServer.Rows[0]["LastModifiedBy"] = "ADMIN";
                    dtConfig_AccountTypeMstServer.Rows[0]["LastUpdatedDateTime"] = DateTime.Now;
                    // dtConfig_AccountTypeMstServer.Rows[0]["LastModifiedHost"] = "10.0.0.0";
                    dtConfig_AccountTypeMstServer.Rows[0].AcceptChanges();
                    dtConfig_AccountTypeMstServer.Rows[0].SetModified();
                }

                ////Config_RepTerritoryMap
                //int index;
                //DataTable dtConfig_RepTerritoryMapClient = dS_Config_AccountTypeMstClient.Tables["Config_RepTerritoryMap"];
                //DataTable dtConfig_RepTerritoryMapServer = objClientUtility.GetTableSchema(ref DBDataAdpterObject, dtConfig_RepTerritoryMapClient.TableName, ref Message);
                //dtConfig_RepTerritoryMapServer.PrimaryKey = new DataColumn[] { dtConfig_RepTerritoryMapServer.Columns["RepId"], dtConfig_RepTerritoryMapServer.Columns["TerritoryNum"] };
                //foreach (DataRow dr in dtConfig_RepTerritoryMapClient.Rows)
                //{
                //    dtConfig_RepTerritoryMapServer.ImportRow(dr);
                //    index = dtConfig_RepTerritoryMapServer.Rows.Count - 1;
                //    dtConfig_RepTerritoryMapServer.Rows[index]["RepId"] = RepId;
                //}

                BLGeneralUtil.UpdateTableInfo utf = BLGeneralUtil.UpdateTable(ref DBCommand, dtConfig_AccountTypeMstServer, BLGeneralUtil.UpdateWhereMode.KeyColumnsOnly);
                if (!utf.Status || utf.TotalRowsAffected != dtConfig_AccountTypeMstServer.Rows.Count)
                {
                    DBCommand.Transaction.Rollback();
                    send_obj.responseObjectInfo.Status = 2;
                    send_obj.responseObjectInfo.Message = "An error occurred while Save User data. " + utf.ErrorMessage;
                    send_obj.responseObjectInfo.dt_ReturnedTables = null;
                    send_obj.responseObjectInfo.ObjRetArgs = null;
                }
                //else
                //{
                //    DBCommand.Parameters.Clear();
                //    DBCommand.CommandText = "DELETE FROM Config_RepTerritoryMap WHERE RepId=@RepId";
                //    DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@RepId", DbType.String, RepId));
                //    int NoOfRowsDelete = DBCommand.ExecuteNonQuery();

                //    utf = BLGeneralUtil.UpdateTable(ref DBCommand, dtConfig_RepTerritoryMapServer, BLGeneralUtil.UpdateWhereMode.KeyColumnsOnly);
                //    if (!utf.Status || utf.TotalRowsAffected != dtConfig_RepTerritoryMapServer.Rows.Count)
                //    {
                //        DBCommand.Transaction.Rollback();
                //        send_obj.responseObjectInfo.Status = 2;
                //        send_obj.responseObjectInfo.Message = "An error occurred while Save Rep Territory Map data. " + utf.ErrorMessage;
                //        send_obj.responseObjectInfo.dt_ReturnedTables = null;
                //        send_obj.responseObjectInfo.ObjRetArgs = null;
                //    }
                else
                {
                    DBCommand.Transaction.Commit();
                    DataTable dtAccountList = GetAccountDetail(null, ref Message);
                    //DataTable dtRepTerritoryMap = GetRepTerritoryMap(null, ref Message);
                    //if (dtAccountList != null && dtAccountList.Rows.Count > 0)
                    //{
                    //    if (!dtAccountList.Columns.Contains("TerritoryNum"))
                    //        dtAccountList.Columns.Add("TerritoryNum");
                    //    if (dtRepTerritoryMap != null && dtRepTerritoryMap.Rows.Count > 0)
                    //    {
                    //        DataRow[] drArray = null;
                    //        String TerritoryNum = String.Empty;
                    //        foreach (DataRow drUser in dtAccountList.Rows)
                    //        {
                    //            TerritoryNum = String.Empty;
                    //            drArray = dtRepTerritoryMap.Select("RepId='" + drUser["RepId"].ToString() + "'");
                    //            if (drArray != null && drArray.Length > 0)
                    //            {
                    //                foreach (DataRow dr in drArray)
                    //                    TerritoryNum += dr["TerritoryNum"].ToString() + " : " + dr["TerritoryName"].ToString() + ", ";

                    //                if (TerritoryNum.Length > 0)
                    //                    TerritoryNum = TerritoryNum.Substring(0, TerritoryNum.Length - 2);

                    //                drUser["TerritoryNum"] = TerritoryNum;
                    //            }
                    //        }
                    //    }
                    //}

                    send_obj.responseObjectInfo.Status = 1;
                    send_obj.responseObjectInfo.Message = "Account data saved successfully.";
                    send_obj.responseObjectInfo.dt_ReturnedTables = new dynamic[1];
                    send_obj.responseObjectInfo.dt_ReturnedTables[0] = dtAccountList;
                    send_obj.responseObjectInfo.ObjRetArgs = null;

                }
                //}
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
            catch (Exception ex)
            {
                ServerLog.ExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                send_obj.responseObjectInfo.Status = 2;
                send_obj.responseObjectInfo.Message = ex.Message;
                send_obj.responseObjectInfo.dt_ReturnedTables = null;
            }

            return send_obj;
        }

        private DataTable GetAccountDetail(String AccountType, ref String Message)
        {
            try
            {
                DBDataAdpterObject.SelectCommand.Parameters.Clear();
                StringBuilder SQLSelect = new StringBuilder();
                SQLSelect.Append(@" SELECT  * 
                                    FROM     Config_AccountTypeMst
                                    WHERE    1=1 ");
                if (AccountType != null && AccountType.Trim() != String.Empty)
                {
                    SQLSelect.Append(" AND    (AccountType = @AccountType) ");
                    DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@AccountType", DbType.String, AccountType));
                }
                DBDataAdpterObject.SelectCommand.CommandText = SQLSelect.ToString();
                DBDataAdpterObject.TableMappings.Clear();
                DBDataAdpterObject.TableMappings.Add("Table", "Config_AccountTypeMst");
                DataSet ds = new DataSet();
                DBDataAdpterObject.Fill(ds);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    //clsEncryptionDecryption objclsEncryptionDecryption = new clsEncryptionDecryption();
                    //String DecryptPassword = String.Empty;
                    //if (!ds.Tables[0].Columns.Contains("DecryptPassword"))
                    //    ds.Tables[0].Columns.Add("DecryptPassword");
                    //foreach (DataRow dr in ds.Tables[0].Rows)
                    //{
                    //    try
                    //    {
                    //        if (dr["Password"].ToString().Trim() != String.Empty)
                    //        {
                    //            DecryptPassword = objclsEncryptionDecryption.decryptPassword(dr["Password"].ToString().Trim());
                    //            if (DecryptPassword != null && DecryptPassword.Trim() != String.Empty)
                    //                dr["DecryptPassword"] = DecryptPassword;
                    //        }
                    //    }
                    //    catch
                    //    {
                    //        continue;
                    //    }
                    //}

                    Message = "Account detail retrieved succesfully for Account Type : " + AccountType;
                    return ds.Tables[0];
                }
                else
                {
                    Message = "Account detail not found for Account Type : " + AccountType;
                    return null;
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
            finally
            {
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
        }

        private DataTable GetAccountCategoryMap(String AccountType, ref String Message)
        {
            try
            {
                DBDataAdpterObject.SelectCommand.Parameters.Clear();
                StringBuilder SQLSelect = new StringBuilder();
                SQLSelect.Append(@" SELECT   Config_CategoryMst.*
                                    FROM     Config_AccountTypeMst 
                                    inner JOIN Config_CategoryMst ON Config_CategoryMst.AccountType = Config_AccountTypeMst.AccountType 
                                    WHERE    1=1 ");
                if (AccountType != null && AccountType.Trim() != String.Empty)
                {
                    SQLSelect.Append(" AND    (Config_CategoryMst.AccountType = @AccountType) ");
                    DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@AccountType", DbType.String, AccountType));
                }
                SQLSelect.Append(" Order by AccountType,CategorySeqNo");
                DBDataAdpterObject.SelectCommand.CommandText = SQLSelect.ToString();
                DBDataAdpterObject.TableMappings.Clear();
                DBDataAdpterObject.TableMappings.Add("Table", "Config_CategoryMst");
                DataSet ds = new DataSet();
                DBDataAdpterObject.Fill(ds);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    Message = "GetAccountCategoryMap detail retrieved succesfully for Account Type : " + AccountType;
                    return ds.Tables[0];
                }
                else
                {
                    Message = "GetAccountCategoryMap detail not found for AccountType Account Type : " + AccountType;
                    return null;
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
            finally
            {
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
        }

        private DataTable GetRegionTerritoryMap(String TerritoryNum, ref String Message)
        {
            try
            {
                DBDataAdpterObject.SelectCommand.Parameters.Clear();
                StringBuilder SQLSelect = new StringBuilder();
                SQLSelect.Append(@" SELECT   DISTINCT TerritoryNum, TerritoryName 
                                    FROM     Config_RegionTerritoryMap 
                                    WHERE    1=1 ");
                if (TerritoryNum != null && TerritoryNum.Trim() != String.Empty)
                {
                    SQLSelect.Append(" AND    (TerritoryNum = @TerritoryNum) ");
                    DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@TerritoryNum", DbType.String, TerritoryNum));
                }
                DBDataAdpterObject.SelectCommand.CommandText = SQLSelect.ToString();
                DBDataAdpterObject.TableMappings.Clear();
                DBDataAdpterObject.TableMappings.Add("Table", "Config_RegionTerritoryMap");
                DataSet ds = new DataSet();
                DBDataAdpterObject.Fill(ds);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    Message = "RegionTerritoryMap detail retrieved succesfully for TerritoryNum : " + TerritoryNum;
                    return ds.Tables[0];
                }
                else
                {
                    Message = "RegionTerritoryMap detail not found for AccountType TerritoryNum : " + TerritoryNum;
                    return null;
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
            finally
            {
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
        }

        private DataTable GetAccountTypeGroupMst(String AccountTypeGroupId, ref String Message)
        {
            try
            {
                DBDataAdpterObject.SelectCommand.Parameters.Clear();
                StringBuilder SQLSelect = new StringBuilder();
                SQLSelect.Append(@" SELECT   *  
                                    FROM     Config_AccountTypeGroupMst 
                                    WHERE    1=1 ");
                if (AccountTypeGroupId != null && AccountTypeGroupId.Trim() != String.Empty)
                {
                    SQLSelect.Append(" AND    (AccountTypeGroupId = @AccountTypeGroupId) ");
                    DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@AccountTypeGroupId", DbType.String, AccountTypeGroupId));
                }
                DBDataAdpterObject.SelectCommand.CommandText = SQLSelect.ToString();
                DBDataAdpterObject.TableMappings.Clear();
                DBDataAdpterObject.TableMappings.Add("Table", "Config_AccountTypeGroupMst");
                DataSet ds = new DataSet();
                DBDataAdpterObject.Fill(ds);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    Message = "AccountType Group detail retrieved succesfully for AccountType Group Id : " + AccountTypeGroupId;
                    return ds.Tables[0];
                }
                else
                {
                    Message = "AccountType Group detail not found for AccountType Group Id : " + AccountTypeGroupId;
                    return null;
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
            finally
            {
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
        }

        private DataTable GetAttributeOperationGroupMst(String AttributeOperationGroupId, ref String Message)
        {
            try
            {
                DBDataAdpterObject.SelectCommand.Parameters.Clear();
                StringBuilder SQLSelect = new StringBuilder();
                SQLSelect.Append(@" SELECT   *  
                                    FROM     Config_AttributeOperationGroupMst 
                                    WHERE    1=1 ");
                if (AttributeOperationGroupId != null && AttributeOperationGroupId.Trim() != String.Empty)
                {
                    SQLSelect.Append(" AND    (AttributeOperationGroupId = @AttributeOperationGroupId) ");
                    DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@AttributeOperationGroupId", DbType.String, AttributeOperationGroupId));
                }
                DBDataAdpterObject.SelectCommand.CommandText = SQLSelect.ToString();
                DBDataAdpterObject.TableMappings.Clear();
                DBDataAdpterObject.TableMappings.Add("Table", "Config_AttributeOperationGroupMst");
                DataSet ds = new DataSet();
                DBDataAdpterObject.Fill(ds);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].NewRow(); //Create New Row
                    dr["AttributeOperationGroupId"] = "Superadmin";              // Set Column Value
                    dr["AttributeOperationGroupDesc"] = "SuperAdmin";              // Set Column Value
                    ds.Tables[0].Rows.InsertAt(dr, 0);

                    Message = "AttributeOperation Group detail retrieved succesfully for AttributeOperation Group Id : " + AttributeOperationGroupId;
                    return ds.Tables[0];
                }
                else
                {
                    Message = "AttributeOperation Group detail not found for AttributeOperation Group Id : " + AttributeOperationGroupId;
                    return null;
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
            finally
            {
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
        }

    }
}
