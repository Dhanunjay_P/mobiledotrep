﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.Utilities;
using System.Configuration;
using System.Data;
using XSD.General;

namespace BLL.Masters
{
    public class MgmtInfo : ServerBase
    {
        public MgmtInfo()
        {
            DBConnection = DBObjectFactory.GetConnectionObject();
            DBConnection.ConnectionString = ConfigurationManager.ConnectionStrings["MgmtAppInfoConnectionString"].ToString();
            DBCommand = DBObjectFactory.GetCommandObject();
            DBCommand.Connection = DBConnection;
            DBDataAdpterObject = DBObjectFactory.GetDataAdapterObject(DBCommand);
            DBDataAdpterObject.SelectCommand.CommandTimeout = 60;
        }

        public DataTable GetAppVersionInfo(String AppName, ref String Message)
        {
            try
            {
                StringBuilder SQLSelect = new StringBuilder();
                SQLSelect.Append("SELECT    * ");
                SQLSelect.Append("FROM     AppVersionInfo ");
                SQLSelect.Append("WHERE    (AppName = @AppName) ");
                DBDataAdpterObject.SelectCommand.CommandText = SQLSelect.ToString();
                DBDataAdpterObject.SelectCommand.Parameters.Clear();
                DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@AppName", DbType.String, AppName));
                DBDataAdpterObject.TableMappings.Clear();
                DBDataAdpterObject.TableMappings.Add("Table", "AppVersionInfo");
                DataSet ds = new DataSet();
                DBDataAdpterObject.Fill(ds);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    Message = "AppVersionInfo retrieved succesfully for AppName : " + AppName;
                    return ds.Tables[0];
                }
                else
                {
                    Message = "AppVersionInfo not found for AppName : " + AppName;
                    return null;
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
            finally
            {
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
        }

        public Byte UpdateAppLogInfo(String AppName, String RepId, DateTime LogDateTime, String LogType, ref String Message)
        {
            try
            {
                if (LogType == "L")
                {
                    DBDataAdpterObject.SelectCommand.Parameters.Clear();
                    DBDataAdpterObject.SelectCommand.CommandText = @"SELECT TOP(1) * 
                                                                     FROM AppLogInfo 
                                                                     WHERE AppName=@AppName 
                                                                     AND  RepId=@RepId 
                                                                     AND  LogType=@LogType 
                                                                     ORDER BY LogDateTime DESC";
                    DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@AppName", DbType.String, AppName));
                    DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@RepId", DbType.String, RepId));
                    DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@LogType", DbType.String, LogType));
                    DBDataAdpterObject.TableMappings.Clear();
                    DBDataAdpterObject.TableMappings.Add("Table", "AppLogInfo");
                    DataSet ds = new DataSet();
                    DBDataAdpterObject.Fill(ds);
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["LogDateTime"].ToString().Trim() != String.Empty)
                    {
                        DateTime LastLogDateTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["LogDateTime"]);
                        TimeSpan ts = LogDateTime.Subtract(LastLogDateTime);
                        if (ts.TotalMinutes < 20)
                        {
                            Message = "AppLog Info data already updated before 20 mintues ago.";
                            return 1;
                        }
                    }
                }

                DS_AppLogInfo dS_AppLogInfo = new DS_AppLogInfo();
                dS_AppLogInfo.EnforceConstraints = false;
                DS_AppLogInfo.AppLogInfoRow rows = dS_AppLogInfo.AppLogInfo.NewAppLogInfoRow();
                rows.AppName = AppName;
                rows.RepId = RepId;
                rows.LogDateTime = LogDateTime;
                rows.LogType = LogType;
                dS_AppLogInfo.AppLogInfo.Rows.Add(rows);

                try
                {
                    dS_AppLogInfo.EnforceConstraints = true;
                }
                catch (ConstraintException ce)
                {
                    Message = ce.Message;
                    ServerLog.MgmtExceptionLog(ce.Message + Environment.NewLine + ce.StackTrace);
                    return 2;
                }
                catch (Exception ex)
                {
                    Message = ex.Message;
                    ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                    return 2;
                }

                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                //Establish DataBase Connection.
                DBConnection.Open();
                //Begin Transaction.
                DBCommand.Transaction = DBConnection.BeginTransaction();

                BLGeneralUtil.UpdateTableInfo objUpdateTableInfo = BLGeneralUtil.UpdateTable(ref DBCommand, dS_AppLogInfo.AppLogInfo, BLGeneralUtil.UpdateWhereMode.KeyColumnsOnly);
                if (!objUpdateTableInfo.Status || objUpdateTableInfo.TotalRowsAffected != dS_AppLogInfo.AppLogInfo.Rows.Count)
                {
                    DBCommand.Transaction.Rollback();
                    if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                    Message = objUpdateTableInfo.ErrorMessage;
                    return 2;
                }

                DBCommand.Transaction.Commit();
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                Message = "AppLog Info data updated successfully.";
                return 1;
            }
            catch (Exception ex)
            {
                if (DBCommand.Transaction != null)
                    DBCommand.Transaction.Rollback();
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return 2;
            }
        }

        public Byte UpdateAppLogInfoForLaunch(String AppName, String RepId, DateTime LogDateTime, String LogType, ref String Message)
        {
            try
            {
//                if (LogType == "L")
//                {
//                    DBDataAdpterObject.SelectCommand.Parameters.Clear();
//                    DBDataAdpterObject.SelectCommand.CommandText = @"SELECT TOP(1) * 
//                                                                     FROM AppLogInfo 
//                                                                     WHERE AppName=@AppName 
//                                                                     AND  RepId=@RepId 
//                                                                     AND  LogType=@LogType 
//                                                                     ORDER BY LogDateTime DESC";
//                    DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@AppName", DbType.String, AppName));
//                    DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@RepId", DbType.String, RepId));
//                    DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@LogType", DbType.String, LogType));
//                    DBDataAdpterObject.TableMappings.Clear();
//                    DBDataAdpterObject.TableMappings.Add("Table", "AppLogInfo");
//                    DataSet ds = new DataSet();
//                    DBDataAdpterObject.Fill(ds);
//                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["LogDateTime"].ToString().Trim() != String.Empty)
//                    {
//                        DateTime LastLogDateTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["LogDateTime"]);
//                        TimeSpan ts = LogDateTime.Subtract(LastLogDateTime);
//                        if (ts.TotalMinutes < 20)
//                        {
//                            Message = "AppLog Info data already updated before 20 mintues ago.";
//                            return 1;
//                        }
//                    }
//                }

                DS_AppLogInfo dS_AppLogInfo = new DS_AppLogInfo();
                dS_AppLogInfo.EnforceConstraints = false;
                DS_AppLogInfo.AppLogInfoRow rows = dS_AppLogInfo.AppLogInfo.NewAppLogInfoRow();
                rows.AppName = AppName;
                rows.RepId = RepId;
                rows.LogDateTime = LogDateTime;
                rows.LogType = LogType;
                dS_AppLogInfo.AppLogInfo.Rows.Add(rows);

                try
                {
                    dS_AppLogInfo.EnforceConstraints = true;
                }
                catch (ConstraintException ce)
                {
                    Message = ce.Message;
                    ServerLog.MgmtExceptionLog(ce.Message + Environment.NewLine + ce.StackTrace);
                    return 2;
                }
                catch (Exception ex)
                {
                    Message = ex.Message;
                    ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                    return 2;
                }

                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                //Establish DataBase Connection.
                DBConnection.Open();
                //Begin Transaction.
                DBCommand.Transaction = DBConnection.BeginTransaction();

                BLGeneralUtil.UpdateTableInfo objUpdateTableInfo = BLGeneralUtil.UpdateTable(ref DBCommand, dS_AppLogInfo.AppLogInfo, BLGeneralUtil.UpdateWhereMode.KeyColumnsOnly);
                if (!objUpdateTableInfo.Status || objUpdateTableInfo.TotalRowsAffected != dS_AppLogInfo.AppLogInfo.Rows.Count)
                {
                    DBCommand.Transaction.Rollback();
                    if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                    Message = objUpdateTableInfo.ErrorMessage;
                    return 2;
                }

                DBCommand.Transaction.Commit();
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                Message = "AppLog Info data updated successfully.";
                return 1;
            }
            catch (Exception ex)
            {
                if (DBCommand.Transaction != null)
                    DBCommand.Transaction.Rollback();
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return 2;
            }
        }

        public DataTable GetAppInfo(String AppName, ref String Message)
        {
            try
            {
                DBDataAdpterObject.SelectCommand.Parameters.Clear();
                StringBuilder SQLSelect = new StringBuilder();
                SQLSelect.Append("SELECT    * ");
                SQLSelect.Append("FROM     AppInfo ");
                if (AppName != null && AppName.Trim() != String.Empty)
                {
                    SQLSelect.Append("WHERE    (AppName = @AppName) ");
                    DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@AppName", DbType.String, AppName));
                }
                DBDataAdpterObject.SelectCommand.CommandText = SQLSelect.ToString();
                DBDataAdpterObject.TableMappings.Clear();
                DBDataAdpterObject.TableMappings.Add("Table", "AppInfo");
                DataSet ds = new DataSet();
                DBDataAdpterObject.Fill(ds);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    Message = "AppInfo retrieved succesfully for AppName : " + AppName;
                    return ds.Tables[0];
                }
                else
                {
                    Message = "AppInfo not found for AppName : " + AppName;
                    return null;
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
            finally
            {
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
        }       

        public DateTime? GetLastUpdatedDataDate(String AppName, ref String Message)
        {
            try
            {
                StringBuilder SQLSelect = new StringBuilder();
                SQLSelect.Append("SELECT    LastUpdatedDateTime ");
                SQLSelect.Append("FROM     AppDataUpdateInfo ");
                SQLSelect.Append("WHERE     AppName = @AppName ");
                DBCommand.CommandText = SQLSelect.ToString();
                DBCommand.Parameters.Clear();
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@AppName", DbType.String, AppName));
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                DBConnection.Open();
                Object objLastUpdatedDateTime = DBCommand.ExecuteScalar();
                if (objLastUpdatedDateTime != null)
                {
                    Message = "Last Updated Data Date retrieve successfully.";
                    return Convert.ToDateTime(objLastUpdatedDateTime);
                }
                else
                {
                    Message = "Last Updated Data Date not found.";
                    return null;
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
            finally
            {
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
        }

        public DataTable GetAppList(ref String Message)
        {
            try
            {
                DBDataAdpterObject.SelectCommand.Parameters.Clear();
                StringBuilder SQLSelect = new StringBuilder();
                SQLSelect.Append("SELECT    AppName ");
                SQLSelect.Append("FROM     AppInfo ");
                SQLSelect.Append("WHERE    (ShowInList = @ShowInList) ");
                DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@ShowInList", DbType.String, Constant.FLAG_Y));
                DBDataAdpterObject.SelectCommand.CommandText = SQLSelect.ToString();
                DBDataAdpterObject.TableMappings.Clear();
                DBDataAdpterObject.TableMappings.Add("Table", "AppInfo");
                DataSet ds = new DataSet();
                DBDataAdpterObject.Fill(ds);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    Message = "App List retrieved succesfully.";
                    return ds.Tables[0];
                }
                else
                {
                    Message = "App List not found.";
                    return null;
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
            finally
            {
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
        }

        public Byte SaveAppUpgradeDetail(String AppName, String Territory, String RepId, String RepName, String DeviceTime, String Version, String DeviceType, ref String Message)
        {
            try
            {
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                //Establish DataBase Connection.
                DBConnection.Open();
                //Begin Transaction.
                DBCommand.Transaction = DBConnection.BeginTransaction();
                StringBuilder SQLUpdate = new StringBuilder();
                SQLUpdate.Append("INSERT Into AppVersionUpgradeInfo ");
                SQLUpdate.Append("Values(@AppName,@Territory,@RepId,@RepName,@UpgradeDateTime,@Version,@DeviceType,@DeviceTime)");

                DBCommand.Parameters.Clear();
                DBCommand.CommandText = SQLUpdate.ToString();
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@AppName", DbType.String, AppName));
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@Territory", DbType.String, Territory));
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@RepId", DbType.String, RepId));
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@RepName", DbType.String, RepName));
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@Version", DbType.String, Version));
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@DeviceType", DbType.String, DeviceType));
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@DeviceTime", DbType.String, DeviceTime));
                DBCommand.Parameters.Add(DBObjectFactory.MakeParameter("@UpgradeDateTime", DbType.DateTime, DateTime.Now));
                int NoOfRowsUpdate = DBCommand.ExecuteNonQuery();
                if (NoOfRowsUpdate <= 0)
                {
                    DBCommand.Transaction.Rollback();
                    if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                    Message = "Fail to Save Upgrade Details";
                    return 2;
                }

                DBCommand.Transaction.Commit();
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                Message = "Save successfully Upgrade Details";
                return 1;
            }
            catch (Exception ex)
            {
                if (DBCommand.Transaction != null)
                    DBCommand.Transaction.Rollback();
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
                Message = ex.Message;
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return 2;
            }
        }

        public DataTable GetAppUpgradeInfo(String FromDate, String ToDate)
        {
            try
            {
                DBDataAdpterObject.SelectCommand.Parameters.Clear();
                StringBuilder SQLSelect = new StringBuilder();
                SQLSelect.Append("SELECT    Territory, RepName AS [Rep Name], CONVERT(Varchar(10), UpgradeDate, 101) AS [Upgrade Date], Version, DeviceType AS [Device Type]");
                SQLSelect.Append("FROM     AppVersionUpgradeInfo WHERE Repid<>'Kt'");
                if (FromDate != null && FromDate != String.Empty)
                    SQLSelect.Append(" AND Convert(Varchar(10), UpgradeDate, 101) >= @FromDate");
                if (ToDate != null && ToDate != String.Empty)
                    SQLSelect.Append(" AND Convert(Varchar(10), UpgradeDate, 101) <= @ToDate");
                SQLSelect.Append(" Order By UpgradeDate desc");
                DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@FromDate", DbType.String, FromDate));
                DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@ToDate", DbType.String, ToDate));
                DBDataAdpterObject.SelectCommand.CommandText = SQLSelect.ToString();
                DBDataAdpterObject.TableMappings.Clear();
                DBDataAdpterObject.TableMappings.Add("Table", "AppUpgradeInfo");
                DataSet ds = new DataSet();
                DBDataAdpterObject.Fill(ds);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0];
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
            finally
            {
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
        }
    }
}
