﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.Utilities;
using System.Configuration;
using System.Data;

namespace BLL.Masters
{
    public class Report : ServerBase
    {
        public Report()
        {
            DBConnection = DBObjectFactory.GetConnectionObject();
            DBConnection.ConnectionString = ConfigurationManager.ConnectionStrings["MgmtAppInfoConnectionString"].ToString();
            DBCommand = DBObjectFactory.GetCommandObject();
            DBCommand.Connection = DBConnection;
            DBDataAdpterObject = DBObjectFactory.GetDataAdapterObject(DBCommand);
            DBDataAdpterObject.SelectCommand.CommandTimeout = 60;
        }

        public DataTable getAppName()
        {
            DataTable dtgetAppName = new DataTable();
            String Message = String.Empty;
            try
            {
                DataSet ds_temp = new DataSet();
                String SqlSelect = "SELECT        * " +
                                    "FROM            AppVersionInfo ";


                DBDataAdpterObject.SelectCommand.CommandText = SqlSelect;
                DBDataAdpterObject.SelectCommand.Parameters.Clear();

                DBDataAdpterObject.Fill(ds_temp);
                if (ds_temp != null && ds_temp.Tables[0] != null && ds_temp.Tables[0].Rows.Count > 0)
                {
                    dtgetAppName = ds_temp.Tables[0].Copy();
                }
                return dtgetAppName;
            }
            catch (Exception ex)
            {
                ServerLog.ExceptionLog(ex.Message.ToString() + Environment.NewLine + ex.StackTrace);
                Message = ex.Message.ToString();
                return null;
            }
            finally
            {
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
        }


        public DataTable GetSelectedAppData(String AppName)
        {
            DataTable dtgetAppName = new DataTable();
            String Message = String.Empty;
            try
            {
                DataSet ds_temp = new DataSet();
                String SqlSelect = "SELECT        distinct RepId " +
                                    "FROM            AppLogInfo where  AppName=@AppName order by RepId";


                DBDataAdpterObject.SelectCommand.Parameters.Add(DBObjectFactory.MakeParameter("@AppName", DbType.String, AppName));
                DBDataAdpterObject.SelectCommand.CommandText = SqlSelect;
                DBDataAdpterObject.SelectCommand.Parameters.Clear();

                DBDataAdpterObject.Fill(ds_temp);
                if (ds_temp != null && ds_temp.Tables[0] != null && ds_temp.Tables[0].Rows.Count > 0)
                {
                    dtgetAppName = ds_temp.Tables[0].Copy();
                }
                return dtgetAppName;
            }
            catch (Exception ex)
            {
                ServerLog.ExceptionLog(ex.Message.ToString() + Environment.NewLine + ex.StackTrace);
                Message = ex.Message.ToString();
                return null;
            }
            finally
            {
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
        }

        public DataTable GetReport(String AppName, String RepId, String LogType, String txtFrom, String txtTo)
        {
            DataTable dtgetReport = new DataTable();
            String Message = String.Empty;
            try
            {
                DataSet ds_temp = new DataSet();
                String SqlSelect = "SELECT       ROW_NUMBER() OVER(ORDER BY RepId) AS No, * " +
                                    "FROM            AppLogInfo where  AppName='" + AppName + "'";

                

                if (RepId != "")
                {
                    SqlSelect += "and RepId ='" + RepId+"'";
                }
                if (LogType != "")
                {
                    SqlSelect += "and LogType ='" + LogType + "'";
                }
                if (txtFrom != "" && txtTo != "")
                {
                   // SqlSelect += "and LogDateTime >='" + DateTime.Parse(txtFrom) + "'and LogDateTime <= '" + DateTime.Parse(txtTo) + "'";
                    //added
                    DateTime txtfrom = DateTime.Parse(txtFrom);
                    DateTime dttxtfrom = txtfrom.ToUniversalTime();
                    var newdttxtfrom = Convert.ToDateTime((dttxtfrom));
                    var offsettxtfrom = new DateTimeOffset(newdttxtfrom);
                    DateTime from = offsettxtfrom.LocalDateTime;

                    DateTime txtto = DateTime.Parse(txtTo);
                    DateTime dttxtto = txtto.ToUniversalTime();
                    var newdttxtto = Convert.ToDateTime((dttxtto));
                    var offsettxtto = new DateTimeOffset(newdttxtto);
                    DateTime to = offsettxtto.LocalDateTime;
                    //end
                    SqlSelect += "and LogDateTime >='" + from + "'and LogDateTime <= '" + to + "'";

                    ServerLog.ExceptionLog("Inside GetReport()::Converted DatetimeFrom Depohub was : " + from);//original RequesteddatetimeFrom
                    ServerLog.ExceptionLog("Inside GetReport()::Converted DatetimeTo Depohub was : " + to);//original RequesteddatetimeFrom
                    //ServerLog.ExceptionLog("Inside GetReport()::Requested DatetimeFrom Depohub was : " + txtFrom);//original RequesteddatetimeFrom
                    //ServerLog.ExceptionLog("Inside GetReport()::after conversion,Requested Universal Datetimefrom  at amzaon becomes : " + dttxtfrom);//UniversalDatetimeFrom
                    //ServerLog.ExceptionLog("Inside GetReport()::after conversion,Requested from Datetime at amzaon becomes : " + from);//final Conversion Datetime
                    //ServerLog.ExceptionLog("Inside GetReport()::Requested DatetimeTo Depohub was : " + txtTo);//original RequesteddatetimeTo
                    //ServerLog.ExceptionLog("Inside GetReport()::after conversion,Requested Universal DatetimeTo at amzaon becomes : " + dttxtto);//UniversalDatetimeTo
                    //ServerLog.ExceptionLog("Inside GetReport()::after conversion,Requested To Datetime at amzaon becomes : " + to);//final Conversion Datetime
                 
                }

                SqlSelect += "order by LogDateTime desc";
                DBDataAdpterObject.SelectCommand.CommandText = SqlSelect;
                DBDataAdpterObject.SelectCommand.Parameters.Clear();

                DBDataAdpterObject.Fill(ds_temp);
                if (ds_temp != null && ds_temp.Tables[0] != null && ds_temp.Tables[0].Rows.Count > 0)
                {
                    dtgetReport = ds_temp.Tables[0].Copy();
                }
                return dtgetReport;
            }
            catch (Exception ex)
            {
                ServerLog.ExceptionLog(ex.Message.ToString() + Environment.NewLine + ex.StackTrace);
                Message = ex.Message.ToString();
                return null;
            }
            finally
            {
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
        }


        public DataTable GetSumaryReport(String AppName, String RepId, String LogType, String txtFrom, String txtTo)
        {
            DataTable dtgetReport = new DataTable();
            String Message = String.Empty;
            try
            {
                DataSet ds_temp = new DataSet();
                String SqlSelect = " select ROW_NUMBER() OVER(ORDER BY RepId) AS No, RepId,sum(case when LogType='S' THEN 1 ELSE 0 END)as Stype,sum(case when LogType='L' THEN 1 ELSE 0 END)as Ltype,MAX(LogDateTime)  as LastActTime " +
                                    "FROM            AppLogInfo where  AppName='" + AppName + "' ";


                

                if (RepId != "")
                {
                    SqlSelect += "and RepId ='" + RepId + "'";
                }
                if (LogType != "")
                {
                    SqlSelect += "and LogType ='" + LogType + "'";
                }
                if (txtFrom != "" && txtTo != "")
                {
                   // SqlSelect += "and LogDateTime >='" + DateTime.Parse(txtFrom) + "'and LogDateTime <= '" + DateTime.Parse(txtTo) + "'";
                    DateTime txtfrom = DateTime.Parse(txtFrom);
                    DateTime dttxtfrom = txtfrom.ToUniversalTime();
                    var newdttxtfrom = Convert.ToDateTime((dttxtfrom));
                    var offsettxtfrom = new DateTimeOffset(newdttxtfrom);
                    DateTime from = offsettxtfrom.LocalDateTime;

                    DateTime txtto = DateTime.Parse(txtTo);
                    DateTime dttxtto = txtto.ToUniversalTime();
                    var newdttxtto = Convert.ToDateTime((dttxtto));
                    var offsettxtto = new DateTimeOffset(newdttxtto);
                    DateTime to = offsettxtto.LocalDateTime;
                    SqlSelect += "and LogDateTime >='" + from + "'and LogDateTime <= '" + to + "'";
                    ServerLog.ExceptionLog("Inside GetSumaryReport()::Converted DatetimeFrom Depohub was : " + from);//original RequesteddatetimeFrom
                    ServerLog.ExceptionLog("Inside GetSumaryReport()::Converted DatetimeTo Depohub was : " + to);//original RequesteddatetimeFrom
                    //ServerLog.ExceptionLog("Inside GetSumaryReport()::after conversion,Requested Universal Datetimefrom  at amzaon becomes : " + dttxtfrom);//UniversalDatetimeFrom
                    //ServerLog.ExceptionLog("Inside GetSumaryReport()::after conversion,Requested from Datetime at amzaon becomes : " + from);//final Conversion Datetime
                    //ServerLog.ExceptionLog("Inside GetSumaryReport()::Requested DatetimeTo Depohub was : " + txtTo);//original RequesteddatetimeTo
                    //ServerLog.ExceptionLog("Inside GetSumaryReport()::after conversion,Requested Universal DatetimeTo at amzaon becomes : " + dttxtto);//UniversalDatetimeTo
                    //ServerLog.ExceptionLog("Inside GetSumaryReport()::after conversion,Requested To Datetime at amzaon becomes : " + to);//final Conversion Datetime

                }

                //SqlSelect += "group by RepId";
                SqlSelect += " group by RepId order by RepId ";
                DBDataAdpterObject.SelectCommand.CommandText = SqlSelect;
                DBDataAdpterObject.SelectCommand.Parameters.Clear();

                DBDataAdpterObject.Fill(ds_temp);
                if (ds_temp != null && ds_temp.Tables[0] != null && ds_temp.Tables[0].Rows.Count > 0)
                {
                    dtgetReport = ds_temp.Tables[0].Copy();
                }
                return dtgetReport;
            }
            catch (Exception ex)
            {
                ServerLog.ExceptionLog(ex.Message.ToString() + Environment.NewLine + ex.StackTrace);
                Message = ex.Message.ToString();
                return null;
            }
            finally
            {
               
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
        }

    }
}
