﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Utilities
{
    /**
     * class name : Tree
     * purpose: Work as data structure for menu and access right tree structure.
     *          - It contains list of base tree nodes
     * **/
    public class Tree
    {
        public List<TreeNode> nodeList { get; set; } // list of base nodes of tree structure
    }

    /**
     * class name: TreeNode
     * Purpose : data structure  that defines tree node.
     *           - it contains child <TreeNodes> if any.
     * **/
    public class TreeNode
    {
        public TreeNode()
        {
            isFolder = false;
            hideCheckbox = false;
            unselectable = false;
            children = new List<TreeNode>();
        }
        public string title { get; set; } // title of node
        public object icon { get; set; } // Use a custom image (filename relative to tree.options.imagePath). 'null' for default icon, 'false' for no icon.
        public string tooltip { get; set; } // Show this popup text.
        public string key { get; set; } // key work as id in tree structure.. one can access tree node with key
        //  key string format --> {id:"",itemType:""} -- itemType = MenuItem/AccessItem/SectionItem/FolderItem
        public List<TreeNode> children { get; set; } // list of child nodes (TreeNodes)
        public bool select { get; set; } // flag : true -- node checkbox selected ; false --  node checkbox not selected
        public bool isFolder { get; set; } // flag : node is folder or not {true/false} ; 
        public bool hideCheckbox { get; set; } // flag: {true/false}  to hide / show checkbox
        public bool unselectable { get; set; } // flag : true -- disable selection of checkbox for node
    }
}
