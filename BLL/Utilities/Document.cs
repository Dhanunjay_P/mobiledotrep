using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Data;
using System.Configuration;
using BLL.Utilities;

namespace BLL.Utilities
{
    /// <summary>
    /// Summary description for Document.
    /// </summary>
    public class Document : ServerBase
    {
        #region Constructor & Destructor.
        public Document()
        {
        }
        ~Document()
        {
            if (DBConnection != null)
            {
                if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
            }
        }
        #endregion

        private const int DOCUMENTNOPORTIONLENGTH = 6;
        #region GetNextDocNo
        public Boolean W_GetNextDocumentNo(ref IDbDataAdapter adapter, String sa_id, String DocumentType, String UserID, String HostName, ref String NextDocumentNo, ref String Message)
        {
            adapter.SelectCommand.CommandText = " SELECT next_doc_no, length, year_prefix, doc_prefix  FROM w_next_doc_no WHERE " +
                                " sa_id = @sa_id AND " +
                                " doc_type = @document_type";

            adapter.SelectCommand.Parameters.Clear();
            adapter.SelectCommand.Parameters.Add(BLGeneralUtil.MakeParameter("@sa_id", DbType.String, sa_id));
            adapter.SelectCommand.Parameters.Add(BLGeneralUtil.MakeParameter("@document_type", DbType.String, DocumentType));

            Decimal doc_no;
            int length = 6;
            String year_prefix = String.Empty, doc_prefix = String.Empty;
            DataSet ds = new DataSet();
            //MessageBox.Show("Before getting the next document no");
            adapter.Fill(ds);
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["next_doc_no"].ToString() != String.Empty)
                doc_no = Convert.ToDecimal(ds.Tables[0].Rows[0]["next_doc_no"].ToString());
            else
                doc_no = 0;

            if (doc_no <= 0)
            {
                Message = "An error occured while getting next document number for Document Type " + DocumentType + ".";
                return false;
            }

            if (ds.Tables[0].Rows[0]["length"].ToString() != String.Empty)
                length = Convert.ToInt32(ds.Tables[0].Rows[0]["length"].ToString());
            if (ds.Tables[0].Rows[0]["doc_prefix"].ToString() != String.Empty)
                doc_prefix = ds.Tables[0].Rows[0]["doc_prefix"].ToString();
            if (ds.Tables[0].Rows[0]["year_prefix"].ToString() != String.Empty)
                year_prefix = ds.Tables[0].Rows[0]["year_prefix"].ToString();

            adapter.SelectCommand.CommandText = " UPDATE w_next_doc_no SET next_doc_no = next_doc_no + 1, " +
                                " last_modified_by = @UserId, last_modified_date = @AsOn," +
                                " last_modified_host = @HostName WHERE " +
                                " sa_id = @sa_id AND " +
                                " doc_type = @document_type AND next_doc_no = @doc_no";

            adapter.SelectCommand.Parameters.Add(BLGeneralUtil.MakeParameter("@doc_no", DbType.Decimal, doc_no));
            adapter.SelectCommand.Parameters.Add(BLGeneralUtil.MakeParameter("@UserId", DbType.String, UserID));
            adapter.SelectCommand.Parameters.Add(BLGeneralUtil.MakeParameter("@AsOn", DbType.DateTime, DateTime.Now));
            adapter.SelectCommand.Parameters.Add(BLGeneralUtil.MakeParameter("@HostName", DbType.String, HostName));

            //MessageBox.Show("Before updating the document no");
            if (adapter.SelectCommand.ExecuteNonQuery() <= 0)
            {
                Message = "An error occured while Updating next document number for Document Type " + DocumentType + ".";
                return false;
            }

            int doc_length = doc_no.ToString().Length;
            int calc_length = length - doc_length;
            String Zeros = String.Empty;
            for (int i = 1; i <= calc_length; i++)
                Zeros += "0";

            NextDocumentNo = year_prefix + doc_prefix + Zeros + doc_no.ToString();
            return true;
        }

        public Boolean W_GetNextDocumentNo(ref IDbCommand comm, String sa_id, String DocumentType, String UserID, String HostName, ref String NextDocumentNo, ref String Message)
        {
            comm.CommandText = " SELECT next_doc_no FROM w_next_doc_no WHERE " +
                                " sa_id = @sa_id AND " +
                                " doc_type = @document_type";

            comm.Parameters.Clear();

            IDataParameter para1 = DBObjectFactory.GetParameterObject();
            para1.ParameterName = "@sa_id";
            para1.DbType = DbType.String;
            para1.Value = sa_id; ;
            comm.Parameters.Add(para1);

            IDataParameter para2 = DBObjectFactory.GetParameterObject();
            para2.ParameterName = "@document_type";
            para2.DbType = DbType.String;
            para2.Value = DocumentType;
            comm.Parameters.Add(para2);

            Decimal doc_no;
            Object objRetVal = null;
            //MessageBox.Show("Before getting the next document no");
            objRetVal = comm.ExecuteScalar();
            if (objRetVal != null)
                doc_no = Convert.ToDecimal(objRetVal);
            else
                doc_no = 0;

            if (doc_no <= 0)
            {
                Message = "An error occured while getting next document number for Document Type " + DocumentType + ".";
                return false;
            }

            comm.CommandText = " UPDATE w_next_doc_no SET next_doc_no = next_doc_no + 1, " +
                                " last_modified_by = @UserId, last_modified_date = @AsOn," +
                                " last_modified_host = @HostName WHERE " +
                                " sa_id = @sa_id AND " +
                                " doc_type = @document_type AND next_doc_no = @doc_no";

            IDataParameter para3 = DBObjectFactory.GetParameterObject();
            para3.ParameterName = "@doc_no";
            para3.DbType = DbType.Decimal;
            para3.Value = doc_no;
            comm.Parameters.Add(para3);

            IDataParameter para4 = DBObjectFactory.GetParameterObject();
            para4.ParameterName = "@UserId";
            para4.DbType = DbType.String;
            para4.Value = UserID;
            comm.Parameters.Add(para4);

            IDataParameter para5 = DBObjectFactory.GetParameterObject();
            para5.ParameterName = "@AsOn";
            para5.DbType = DbType.DateTime;
            para5.Value = DateTime.Now;
            comm.Parameters.Add(para5);

            IDataParameter para6 = DBObjectFactory.GetParameterObject();
            para6.ParameterName = "@HostName";
            para6.DbType = DbType.String;
            para6.Value = HostName;
            comm.Parameters.Add(para6);

            //MessageBox.Show("Before updating the document no");
            if (comm.ExecuteNonQuery() <= 0)
            {
                Message = "An error occured while Updating next document number for Document Type " + DocumentType + ".";
                return false;
            }

            NextDocumentNo = FormatDocumentNo(DocumentType, doc_no);
            //MessageBox.Show("Document no is " + NextDocumentNo);
            return true;
        }

        private String FormatDocumentNo(String DocumentType, Decimal doc_no)
        {
            //temporarily we have applied following logic to generate ipd,opd and private services numbers
            //in future it will be customezed and for that we need one table for this parameters.

            //right now it will work as follow.
            //e.g. for ipd it will be I050600001
            //here I indicates this number is for Ipd
            //next 4 digits 0506 indicates year duration
            //and last 6 digits indicates sr.no.            
            String FormattedNo = "";
            //Document Type 1 char
            FormattedNo = DocumentType.Substring(0, 1);
            //Current Year 2 chars
            String year = System.DateTime.Today.Year.ToString();
            FormattedNo += year.Substring(2);
            year = System.DateTime.Today.AddYears(1).Year.ToString().Substring(2);
            //next year 2 chars
            FormattedNo += year;

            int LengthOfCurrentDocNo = doc_no.ToString().Length;
            for (int i = 1; i <= DOCUMENTNOPORTIONLENGTH - LengthOfCurrentDocNo; i++)
                FormattedNo += "0";
            FormattedNo += doc_no.ToString();

            return FormattedNo;
        }

        public Boolean GetNextDocumentNo(ref IDbCommand comm, String TransactionType, String UserID, String HostName, ref String NextDocumentNo, ref String Message)
        {
            //IDbDataAdapter objIDbDataAdapter = new SqlDataAdapter((SqlCommand)comm);
            /**********************************************/
            //ADDED BY SUNDEEP ON 25/03/2009.
            //this will help to get the year prefix and doctype prefix in the document number generation.
            //this year prefix will be coming from year mst
            //this doctype prefix will be coming from doc_prefix_map
            //*********************************************************************/
            if (DBConnection.State == ConnectionState.Closed)
            {
                DBConnection.Open();
            }
            //....................................
            DBCommand.Transaction = DBConnection.BeginTransaction();
            
            //....................................  
            String YearCode = String.Empty;
            String YearPrefix = String.Empty;
            String DocTypePrefix = String.Empty;

            /**********************************************/
            DBCommand.CommandText = " SELECT next_doc_no FROM next_doc_no WHERE " +
                                " trans_type = @trans_type ";

            DBCommand.Parameters.Clear();


            IDataParameter para2 = DBObjectFactory.GetParameterObject();
            para2.ParameterName = "@trans_type";
            para2.DbType = DbType.String;
            para2.Value = TransactionType;
            DBCommand.Parameters.Add(para2);


            Object objRetVal = null;
            Decimal doc_no;
            //MessageBox.Show("Before getting the next document no");
            objRetVal = DBCommand.ExecuteScalar();
            if (objRetVal != null)
                doc_no = Convert.ToDecimal(objRetVal);
            else
                doc_no = 0;

            if (doc_no <= 0)
            {
                Message = "An error occured while getting next document number for Transaction Type " + TransactionType + ".";
                DBCommand.Transaction.Rollback();
                if (DBConnection.State == ConnectionState.Open)
                    DBConnection.Close();
                return false;
            }

            DBCommand.CommandText = " UPDATE next_doc_no SET next_doc_no = next_doc_no + 1, " +
                                " last_modified_by = @UserId, last_modified_date = @AsOn," +
                                " last_modified_host = @HostName WHERE " +
                                " trans_type = @trans_type AND next_doc_no = @doc_no";

            IDataParameter para3 = DBObjectFactory.GetParameterObject();
            para3.ParameterName = "@doc_no";
            para3.DbType = DbType.Decimal;
            para3.Value = doc_no;
            DBCommand.Parameters.Add(para3);

            IDataParameter para4 = DBObjectFactory.GetParameterObject();
            para4.ParameterName = "@UserId";
            para4.DbType = DbType.String;
            para4.Value = UserID;
            DBCommand.Parameters.Add(para4);

            IDataParameter para5 = DBObjectFactory.GetParameterObject();
            para5.ParameterName = "@AsOn";
            para5.DbType = DbType.DateTime;
            para5.Value = DateTime.Now;
            DBCommand.Parameters.Add(para5);

            IDataParameter para6 = DBObjectFactory.GetParameterObject();
            para6.ParameterName = "@HostName";
            para6.DbType = DbType.String;
            para6.Value = HostName;
            DBCommand.Parameters.Add(para6);

            //MessageBox.Show("Before updating the document no");
            if (DBCommand.ExecuteNonQuery() <= 0)
            {
                Message = "An error occured while Updating next document number for Transaction Type " + TransactionType + ".";
                DBCommand.Transaction.Rollback();
                if (DBConnection.State == ConnectionState.Open)
                    DBConnection.Close();
                return false;
            }
            DBCommand.Transaction.Commit();
            if (DBConnection.State == ConnectionState.Open)
                DBConnection.Close();
            NextDocumentNo = Convert.ToString(doc_no);
            return true;
        }

        #endregion

        #region Document Types
        public static string GetAttachmentDocType()
        {
            return "Attach";
        }        
        #endregion
    }
}
