﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Data;
using System.Configuration;

namespace BLL.Utilities
{
    public class EmailSender : ServerBase
    {
        public DataTable GetEmailAcountsDetail(ref String Message)
        {
            try
            {
                StringBuilder SQLSelect = new StringBuilder();
                SQLSelect.Append("SELECT * FROM Config_EmailAccounts");
                DBDataAdpterObject.SelectCommand.CommandText = SQLSelect.ToString();
                DBDataAdpterObject.TableMappings.Clear();
                DBDataAdpterObject.TableMappings.Add("Table", "Config_EmailAccounts");
                DataSet ds = new DataSet();
                DBDataAdpterObject.Fill(ds);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    Message = "Email Acounts retrieved successfully.";
                    return ds.Tables[0];
                }
                else
                {
                    Message = "Email Acounts not found.";
                    return null;
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                ServerLog.Log(ex.Message + Environment.NewLine + ex.StackTrace);
                return null;
            }
        }

        public Byte SendMail(string fromEmail, string SenderDisplayName, string fromPassword, string[] toEmail, string sendServerName, int PortNum, string isSSlEnabled, string isMailtype, string subject, string body, Boolean IsBodyHtml, ref String Message)
        {
            string tomailList = string.Empty;
            try
            {
                SmtpClient SmtpServer = new SmtpClient();
                SmtpServer.Credentials = new System.Net.NetworkCredential(fromEmail, fromPassword);

                SmtpServer.Port = PortNum;
                SmtpServer.Host = sendServerName;
                if (isSSlEnabled == "N")
                {
                    SmtpServer.EnableSsl = false;
                }
                else
                {
                    SmtpServer.EnableSsl = true;
                }

                if (toEmail.Length > 0)
                {
                    for (int i = 0; i < toEmail.Length; i++)
                    {
                        if (toEmail[i] != null)
                        {
                            if (i != toEmail.Length - 1)
                            {
                                tomailList += toEmail[i].ToString() + ",";
                            }
                            else
                            {
                                tomailList += toEmail[i].ToString();
                            }
                        }
                    }
                    //string commacheck=
                    //tomailList=tomailList
                }
                MailMessage mail = new MailMessage();

                mail.From = new MailAddress(fromEmail, (SenderDisplayName != null && SenderDisplayName.Trim() != String.Empty ? SenderDisplayName : fromEmail), System.Text.Encoding.UTF8);
                mail.IsBodyHtml = IsBodyHtml;

                if (toEmail.Length > 0 && !string.IsNullOrEmpty(tomailList))
                {
                    for (int i = 0; i < toEmail.Length; i++)
                    {
                        if (toEmail[i] != null && toEmail[i].ToString() != "")
                        {
                            mail.To.Add(toEmail[i].ToString());
                        }
                    }
                    if (isMailtype == "Reminder")
                    {
                        mail.Subject = "We Remind you.";
                        mail.Body = "Reminders <br />" +
                                    "Regards";
                        //"To change the reminder settings, <a href=\"" + ConfigurationManager.AppSettings.Get("UserSettingLink") + "\">click here</a>.";
                    }
                    else if (isMailtype == "Late Notice")
                    {
                        mail.Subject = "WARNING! update data.";
                        mail.Body = "Update data as soon as possible.\n" +
                                    "Regards";

                    }
                    else if (isMailtype == "Escalation Notice")
                    {
                        if (subject != string.Empty)
                        {
                            mail.Subject = subject;
                        }
                        else
                        {
                            mail.Subject = "URGENT! update data.";
                        }
                        if (subject != string.Empty)
                        {
                            mail.Body = body.Replace('|', '\n');
                        }
                        else
                        {
                            mail.Body = "Update data URGENT.\n" +
                                        "Regards";
                        }
                    }
                    else
                    {
                        mail.Subject = subject;
                        mail.Body = body;
                    }
                    SmtpServer.Send(mail);
                }
                return 1;
            }
            catch (Exception ex)
            {
                ServerLog.Log(ex.Message.ToString() + Environment.NewLine + ex.StackTrace);
                Message = ex.Message;
                return 2;
            }
        }

        public Byte UpdateEmailAcountsLastMailSendDateTime(ref IDbCommand command, int SeqNo, int NoOfMailSend, DateTime LastSendDateTime, ref String Message)
        {
            try
            {
                command.CommandText = "UPDATE Config_EmailAccounts SET LastSendMailIndex=LastSendMailIndex + @LastSendMailIndex, LastSendDateTime=@LastSendDateTime WHERE SeqNo=@SeqNo";
                command.Parameters.Clear();
                command.Parameters.Add(DBObjectFactory.MakeParameter("@LastSendMailIndex", DbType.Int32, NoOfMailSend));
                command.Parameters.Add(DBObjectFactory.MakeParameter("@LastSendDateTime", DbType.DateTime, LastSendDateTime));
                command.Parameters.Add(DBObjectFactory.MakeParameter("@SeqNo", DbType.Int32, SeqNo));

                if (command.ExecuteNonQuery() > 0)
                {
                    Message = "Last Mail Send DateTime updated successfully.";
                    return 1;
                }
                else
                {
                    Message = "Fail to update Last Mail Send DateTime.";
                    return 2;
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                ServerLog.Log(ex.Message + Environment.NewLine + ex.StackTrace);
                return 2;
            }
        }

        //Dhanunjay 07-01-2016
        public Byte SendMail_Air(String fromEmail, string SenderDisplayName, String fromPassword, String toEmail, String sendServerName, int PortNum, Boolean isSSlEnabled, string isMailtype, String subject, Boolean IsBodyHtml, String body, String[] AttachmentFilePath, ref String Message)
        {
            try
            {
                SmtpClient SmtpServer = new SmtpClient();
                SmtpServer.Credentials = new System.Net.NetworkCredential(fromEmail, fromPassword);
                SmtpServer.Port = PortNum;
                SmtpServer.Host = sendServerName;
                SmtpServer.EnableSsl = isSSlEnabled;

                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(fromEmail, (SenderDisplayName != null && SenderDisplayName.Trim() != String.Empty ? SenderDisplayName : fromEmail), System.Text.Encoding.UTF8);
                if (toEmail != null && toEmail.Length > 0)
                {
                    // for (int i = 0; i < toEmail.Length; i++)
                    //{
                    // if (toEmail != null && toEmail.ToString().Trim().Length > 0)
                    mail.To.Add(toEmail.ToString());
                    //}
                }
                mail.Subject = subject;
                mail.IsBodyHtml = IsBodyHtml;
                mail.Body = body;
                if (AttachmentFilePath != null && AttachmentFilePath.Length > 0)
                {
                    Attachment attachment = null;
                    foreach (String filePath in AttachmentFilePath)
                    {
                        attachment = new Attachment(filePath);
                        mail.Attachments.Add(attachment);
                    }
                }
                SmtpServer.Timeout = int.MaxValue;
                SmtpServer.Send(mail);

                Message = "E-Mail sent successfully to " + String.Join(",", toEmail);
                return 1;
            }
            catch (Exception ex)
            {
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                Message = ex.Message;
                return 2;
            }
        }

        //By Vatsal on 17-08-2017
        public Byte SendMail_Roster(String fromEmail, String FromName, String fromPassword, String toEmail, String sendServerName, int PortNum, Boolean isSSlEnabled, string isMailtype, String subject, Boolean IsBodyHtml, String body, String[] AttachmentFilePath, ref String Message)
        {
            try
            {
                SmtpClient SmtpServer = new SmtpClient();
                SmtpServer.Credentials = new System.Net.NetworkCredential(fromEmail, fromPassword);
                SmtpServer.Port = PortNum;
                SmtpServer.Host = sendServerName;
                SmtpServer.EnableSsl = isSSlEnabled;
                if (String.IsNullOrEmpty(FromName))
                {
                    FromName = fromEmail;
                }
                MailMessage mail = new MailMessage();
                //mail.From = new MailAddress(fromEmail, fromEmail, System.Text.Encoding.UTF8);
                mail.From = new MailAddress(fromEmail, FromName, System.Text.Encoding.UTF8);
                if (toEmail != null && toEmail.Length > 0)
                {
                    // for (int i = 0; i < toEmail.Length; i++)
                    //{
                    // if (toEmail != null && toEmail.ToString().Trim().Length > 0)
                    mail.To.Add(toEmail.ToString());
                    //}
                }
                mail.Subject = subject;
                mail.IsBodyHtml = IsBodyHtml;
                mail.Body = body;
                if (AttachmentFilePath != null && AttachmentFilePath.Length > 0)
                {
                    Attachment attachment = null;
                    foreach (String filePath in AttachmentFilePath)
                    {
                        attachment = new Attachment(filePath);
                        mail.Attachments.Add(attachment);
                    }
                }
                SmtpServer.Timeout = int.MaxValue;
                SmtpServer.Send(mail);

                Message = "E-Mail sent successfully to " + String.Join(",", toEmail);
                return 1;
            }
            catch (Exception ex)
            {
                ServerLog.MgmtExceptionLog(ex.Message + Environment.NewLine + ex.StackTrace);
                Message = ex.Message;
                return 2;
            }
        }

    }
}
