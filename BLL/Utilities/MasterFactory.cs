﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Utilities
{
    public class MasterFactory
    {
        public static ServerBase GenerateMasterObject(String ModuleName, String TransactionType)
        {
            switch (ModuleName)
            {
                case "S"://Generate Sales Master Object
                    return GenerateSalesMasterObject(TransactionType);
                case "F"://Generate Finance Master Object
                    return GenerateFinanceMasterObject(TransactionType);
                default:
                    return null;
            }
        }

        private static ServerBase GenerateSalesMasterObject(String TransactionType)
        {
            switch (TransactionType)
            {
                default:
                    return null;
            }
        }

        private static ServerBase GenerateFinanceMasterObject(String TransactionType)
        {
            switch (TransactionType)
            {
                default:
                    return null;
            }
        }
    }
}
