using System;
using System.IO;
using System.Configuration;
using System.Text;

namespace BLL.Utilities
{
    /// <summary>
    /// Summary description for LogFunction.
    /// </summary>
    public class ServerLog
    {
        //private static String DirectoryPath = System.Reflection.Assembly.LoadFrom(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location)) + @"\Log";
        private static String DirectoryPath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "Log";
        private static String ServerLogFile = ConfigurationManager.AppSettings.Get("ServerLogFile");
        private static String InvalidLoginLogFile = ConfigurationManager.AppSettings.Get("InvalidLoginLogFile");
        private static String ExceptionLogFile = ConfigurationManager.AppSettings.Get("ExceptionLogFile");
        private static String ThemeLogFile = ConfigurationManager.AppSettings.Get("ThemeLogFile");
        private static String MgmtExceptionLogFile = ConfigurationManager.AppSettings.Get("MgmtExceptionLogFile");
        private static String DateTimeLogFormat = "dd-MMM-yyyy hh:mm:ss:fffffff tt";

        public static void Log(String message)
        {
            WriteLog(ServerLogFile, message);

            //StreamWriter sw = null;
            //if (File.Exists(ServerLogFile))
            //    sw = File.AppendText(ServerLogFile);
            //else
            //    sw = File.CreateText(ServerLogFile);
            //sw.WriteLine(DateTime.Now.ToString() + "  " + message);
            //sw.Close();
        }

        public static void InvalidLoginLog(String message)
        {
            //FileStream fs = new FileStream(InvalidLoginLogFile, FileMode.Append, FileAccess.Write, FileShare.Write);
            //fs.Close();
            //StreamWriter sw = new StreamWriter(InvalidLoginLogFile, true, Encoding.ASCII);
            //sw.WriteLine(message);
            //sw.Close();
        }

        public static void ExceptionLog(String message)
        {
            WriteLog(ExceptionLogFile, message);

            //Log(message);

            //StreamWriter sw = null;
            //if (File.Exists(ExceptionLogFile))
            //    sw = File.AppendText(ExceptionLogFile);
            //else
            //    sw = File.CreateText(ExceptionLogFile);
            //sw.WriteLine(DateTime.Now.ToString() + "  " + message);
            //sw.Close();
        }

        public static void MgmtExceptionLog(String message)
        {
            WriteLog(MgmtExceptionLogFile, message);

            //StreamWriter sw = null;
            //if (File.Exists(MgmtExceptionLogFile))
            //    sw = File.AppendText(MgmtExceptionLogFile);
            //else
            //    sw = File.CreateText(MgmtExceptionLogFile);
            //sw.WriteLine(DateTime.Now.ToString() + "  " + message);
            //sw.Close();
        }

        public static void ThemeLog(String message)
        {
            //FileStream fs = new FileStream(ThemeLogFile, FileMode.Append, FileAccess.Write, FileShare.Write);
            //fs.Close();
            //StreamWriter sw = new StreamWriter(ThemeLogFile, true, Encoding.ASCII);
            //sw.WriteLine(message);
            //sw.Close();
        }

        private static void WriteLog(String LogFile, String message)
        {
            //Application.StartupPath
            //System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath

            String AutoFlushServerLogFile = "Y";
            long MaxSizeToAutoFlushServerLogFile = 5000000; //in bytes.

            try
            {
                AutoFlushServerLogFile = ConfigurationManager.AppSettings.Get("AutoFlushServerLogFile");
                if (AutoFlushServerLogFile == null || AutoFlushServerLogFile.Trim() == String.Empty)
                    AutoFlushServerLogFile = "Y";

                String LogFileSize = ConfigurationManager.AppSettings.Get("MaxSizeToAutoFlushServerLogFile");
                if (LogFileSize == null || LogFileSize.Trim() == String.Empty)
                    LogFileSize = "5000000";
                MaxSizeToAutoFlushServerLogFile = Convert.ToInt64(LogFileSize);
            }
            catch
            {
                AutoFlushServerLogFile = "Y";
                MaxSizeToAutoFlushServerLogFile = 5000000;
            }

            try
            {
                if (!Directory.Exists(DirectoryPath))
                    Directory.CreateDirectory(DirectoryPath);
                String LogFilePath = DirectoryPath + @"\" + LogFile;

                StreamWriter sw = null;
                if (AutoFlushServerLogFile == "Y")
                {
                    if (File.Exists(LogFilePath))
                    {
                        FileInfo objFileInfo = new FileInfo(LogFilePath);
                        if (objFileInfo.Length >= MaxSizeToAutoFlushServerLogFile)
                        {
                            File.Delete(LogFilePath);
                            sw = File.CreateText(LogFilePath);
                            sw.WriteLine("Auto Clear Log file on " + DateTime.Now.ToString(DateTimeLogFormat));
                            sw.WriteLine(DateTime.Now.ToString() + "  " + message);
                            sw.Close();
                            return;
                        }
                    }
                }

                if (!File.Exists(LogFilePath))
                    sw = File.CreateText(LogFilePath);
                else
                    sw = File.AppendText(LogFilePath);
                sw.WriteLine(DateTime.Now.ToString(DateTimeLogFormat) + "  " + message);
                sw.Close();
            }
            catch { }
        }

        //private static void WriteLog(String LogFile, String message)
        //{
        //    try
        //    {
        //        StreamWriter sw = null;
        //        if (ConfigurationManager.AppSettings.Get("AutoFlushServerLogFile") == "Y")
        //        {
        //            if (File.Exists(LogFile))
        //            {
        //                FileInfo objFileInfo = new FileInfo(LogFile);
        //                if (objFileInfo.Length >= Convert.ToInt64(ConfigurationManager.AppSettings.Get("MaxSizeToAutoFlushServerLogFile")))
        //                {
        //                    File.Delete(LogFile);
        //                    sw = File.CreateText(LogFile);
        //                    sw.WriteLine("Auto Clear Log file on " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
        //                    sw.WriteLine(DateTime.Now.ToString() + "  " + message);
        //                    sw.Close();
        //                    return;
        //                }
        //            }
        //        }

        //        if (File.Exists(LogFile))
        //            sw = File.AppendText(LogFile);
        //        else
        //            sw = File.CreateText(LogFile);

        //        sw.WriteLine(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss:ffffff tt") + "    " + message);
        //        sw.Close();
        //    }
        //    catch { }
        //}

        //public static void Flush()
        //{		
        //    File.Delete(ServerLogFile);			
        //}

        private static void Flush(String LogFile)
        {
            try
            {
                if (!Directory.Exists(DirectoryPath))
                    Directory.CreateDirectory(DirectoryPath);
                String LogFilePath = DirectoryPath + @"\" + LogFile;

                if (File.Exists(LogFilePath))
                    File.Delete(LogFilePath);
                StreamWriter sw = File.CreateText(LogFilePath);
                sw.WriteLine("Clear Log file on " + DateTime.Now.ToString(DateTimeLogFormat));
                sw.Close();
            }
            catch { }
        }

        public static void FlushServerLogFile()
        {
            Flush(ServerLogFile);
        }

        public static void FlushExceptionLog()
        {
            Flush(ExceptionLogFile);
        }

        public static void FlushALL()
        {
            Flush(ServerLogFile);
            Flush(ExceptionLogFile);
        }
    }
}
