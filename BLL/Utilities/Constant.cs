using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Utilities
{
    public static class Constant
    {
        public static int TimeZoneDifferenceInMinute = -420;

        public static String OS_iOS = "iOS";
        public static String OS_Android = "Android";

        public static String TypeCode_Prof = "Prof";
        public static String TypeCode_Institution = "Institution";
        public static String TypeCode_KOL = "KOL";

        public const String Provider_MSSQL = "System.Data.SqlClient";
        public const String Provider_Oracle = "System.Data.OracleClient"; //"ORAOLEDB.ORACLE";
        public const String Provider_MYSQL = "MySql.Data.MySqlClient";

        public const String DateTimeFormat = "dd-MMM-yyyy";
        
        public const String NO = "N";
        public const String YES = "Y";

        public const String ACTIVE = "A";
        public const String DEACTIVE = "D";
        public const String FLAG_Y = "Y";
        public const String FLAG_N = "N";
       
    }
}

