﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login_Roster.aspx.cs" Inherits="RosterLogin" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head runat="server">
<link rel="shortcut icon" href="roster/image/icon.png">
    <title>Roster</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="Roster/CSS/bootstrap-multiselect.css" rel="stylesheet" />
    
    <link rel="stylesheet" href="Roster/CSS/jquery.dataTables.min.css" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
    <link href="Roster/CSS/style.css" rel="stylesheet" />
    <link href="Roster/CSS/font-awesome.min.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="Roster/Scripts/bootstrap-multiselect.js"></script>
    <script src="Roster/Scripts/jquery.dataTables.min.js"></script>
    <style>
       


    #card {
      width: 100%;
      height: 100%;
       
      -webkit-transition: -webkit-transform 1s;
         -moz-transition: -moz-transform 1s;
           -o-transition: -o-transform 1s;
              transition: transform 1s;
      -webkit-transform-style: preserve-3d;
         -moz-transform-style: preserve-3d;
           -o-transform-style: preserve-3d;
              transform-style: preserve-3d;
    }

    #card.flipped {
      -webkit-transform: rotateY( 180deg );
         -moz-transform: rotateY( 180deg );
           -o-transform: rotateY( 180deg );
              transform: rotateY( 180deg );
    }

    #card .rotatemodal {
      /*display: block;*/
      height: 100%;
      width: 100%;
      color: white;
      position:absolute;
      -webkit-backface-visibility: hidden;
         -moz-backface-visibility: hidden;
           -o-backface-visibility: hidden;
              backface-visibility: hidden;
    }
    #card .back {
    
      -webkit-transform: rotateY( 180deg );
         -moz-transform: rotateY( 180deg );
           -o-transform: rotateY( 180deg );
              transform: rotateY( 180deg );
    }
     .right-bottom-cornar{    
            background-color: transparent;
    border: none;
    position: absolute;
    bottom: 10px;
    right: 0;
    border-radius: 1em;
    padding: 10px;
    height: 20px;
    padding-top: 0px;
    border-top-left-radius: 0px;
    border-bottom-right-radius: 0px;
    cursor: pointer;
    text-decoration:none !important;
    font-size:25px;
}
        .login-content {
        min-height:320px
        }

         .login-modal .login-content label {
            margin: 0 -15px 0 -15px;
            padding: 0 15px;
            
            color: #333;
            margin-bottom: 10px;
            font-size:13px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
            <Services>
                <asp:ServiceReference Path="~/DeGrToolWebService.asmx" />
            </Services>
        </asp:ScriptManager>
        
            <div class="login">
                <div id="card">
                    <div class="front rotatemodal">
                        <div class="login-modal" id="ssomodal" style="border-radius: 10px">
                            <div class="login-header" style="border-radius: 9px; border-bottom-left-radius: 0px; border-bottom-right-radius: 0px;">
                                <%--<img src="Images_air/zs_phardma_logo.png" />--%>    
                                <img src="Images_air/NeuroLogo.png" class="logo" />    
                            </div>
                            <div class="login-content" id="ssoouter" style="padding:0px">
                                <div id="ssoinner" style="text-align:center">
                               <img src="Roster/Images/logo.png" />
                                
                               <div class="btn btn-primary btn-block" id="log_cor" style="width: 100px;margin:0 auto;margin-top:25px;">Sign On</div> 
                              </div>
                                   <div><a class="change right-bottom-cornar" style="">⇛  &nbsp;</a></div>
                                <%-- <a class="btn btn-primary btn-block" id="log_cor">Login with Corporate User</a>  
                                --%>
                            </div>
                            <%--<div class="login-footer">
                
            </div>--%>
                        </div>
                    </div>
                    <div class="back rotatemodal">
                        <div class="login-modal" style="border-radius: 10px">
                            <div class="login-header" style="border-radius: 9px; border-bottom-left-radius: 0px; border-bottom-right-radius: 0px;">
                                <img src="Roster/Images/logo.png" />
                            </div>
                            <div class="login-content">
                                <h1>Login</h1>
                                <div class="form-group">
                                    <label>Username:</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input id="username" type="text" class="form-control" name="username" placeholder="Username" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Password:</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input id="password" type="password" class="form-control" name="password" placeholder="Password" />
                                    </div>
                                </div>
                                <a class="btn btn-primary btn-block" id="login">LOGIN</a>
                               <div><a class="change right-bottom-cornar" style="">⇛  &nbsp;</a></div>
                                <%-- <a class="btn btn-primary btn-block" id="log_cor">Login with Corporate User</a>  
                                --%>
                            </div>
                            <%--<div class="login-footer">
                
            </div>--%>
                        </div>
                    </div>

                    

                </div>
            </div>
        
    </form>
     <script type="text/javascript" language="javascript">

         $(document).ready(function () {
             $("#ssoinner").css('top', ($("#ssomodal .login-content").outerHeight() - $("#ssoinner").height()) / 2);
             $("#ssoinner").css('position', 'relative');
             var card = document.getElementById('card');
             $('.change').click(function (e) {
                 //$("#ssoouter").show();
                 //$(card).toggleClass('flipped');
                 
                 if ($('.change').data('simpleLogin') != true) {
                     $('.change').data('simpleLogin', true);
                     $(card).toggleClass('flipped');
                     setTimeout(function () {
                         $("#ssoouter").hide();
                     }, 300);
                 }
                 else {
                     $("#ssoouter").show();
                     $(card).toggleClass('flipped');
                     $('.change').data('simpleLogin', false);
                     
                 }
             });


             $("#login").click(function (event) {
                 if (document.getElementById("username").value == "") {
                     alert("Please enter username.");
                     document.getElementById("username").style.borderColor = 'red';
                     document.getElementById("username").focus();
                     return false;
                 } else {
                     UserName = document.getElementById("username").value;
                     document.getElementById("username").style.borderColor = '';
                 }
                 if (document.getElementById("password").value == "") {
                     alert("Please enter password.");
                     document.getElementById("password").style.borderColor = 'red';
                     document.getElementById("password").focus();
                     return false;
                 }
                 else {
                     Password = document.getElementById("password").value;
                     document.getElementById("password").style.borderColor = '';
                 }

                 DeGrToolWebService.LoginWebSite_Roaster(UserName, Password, onSuccess, onFail);
             });
             $("#log_cor").click(function () {
                 //FOR OKTA LOGIN IN ZS
                 //window.location.href = "https://zspharma.okta.com/app/zspharma_roster_1/exkavzszxjY5QC1Gs0x7/sso/saml";

                 //For AstraZeneca in ZS
                 window.location.href = "https://ping.astrazeneca.com/idp/startSSO.ping?PartnerSpId=az:sp:Roster";

                 //FOR OKTA LOGIN IN Neurocrine
                 //window.location.href = "https://neurocrine.okta.com/app/neurocrine_neuroroster_1/exkbojo0eeaWCPR2N0x7/sso/saml"
             });


         });
     
         function onSuccess(data) {
             if (data == "1") {
                 sessionStorage["username"] = document.getElementById("username").value;
                 location.href = "RosterDetails.aspx";

             }
             else if (data == "2") {
                 alert("User id is inactive.");
             }
             else if (data == "4") {

                 alert("You have no rights.")
             }
             else {
                 alert("User id or password not matched");
             }
         }

         function onFail(error) {
             //  alert("Ajax Fail");
         }


    </script>
</body>
</html>

