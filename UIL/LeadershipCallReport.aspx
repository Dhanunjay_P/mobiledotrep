﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="LeadershipCallReport.aspx.cs" Inherits="LeadershipCallReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderTitle" Runat="Server">
Leadership Call Report
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script language="javascript" type="text/javascript">

        var OriginalData = new Object();
        var NewUploadData = new Object();
        var OldUploadData = new Object();
        var oTableUser = null;
        var validator;
        var NoOfRow = 0;

        $("legend").live('click', function () {
            $(this).siblings().slideToggle("slow");
        });

        function pageLoad() {

            //SessionData = JSON.parse(document.getElementById("ctl00_ContentPlaceHolder1_hidSession").value);
            $("#UserListContent").hide();

            PrepareBasicObjects("LeadershipCallReport", "GetReportData", "M", "S", "M001", "M003N");

            AutoShowMessage = false;
            CallWebService(OnPageLoadSuccess);
        }

        function OnPageLoadSuccess() {

            if (ResultObject != null) {
                if (ResultObject.responseObjectInfo.Status == 1) {

                    DisplayUserData(ResultObject.responseObjectInfo.dt_ReturnedTables[0]);
                    $("#LeadershipCallListContent").show();
                }
                else
                    $("#LeadershipCallListContent").hide();
            }
            else
                $("#LeadershipCallListContent").hide();
        }

        function DisplayUserData(data) {

            try {
                if (oTableUser != null) {
                    oTableUser.fnDestroy();
                    $("#LeadershipCallList").html(' <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped" id="LeadershipCallTbl"><thead></thead><tbody> </tbody><tfoot> </tfoot></table>');
                }
                oTableUser = $("#LeadershipCallTbl").dataTable({
                    "iDisplayLength": -1,
                    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                    "aaData": data,
                    "aoColumns": [                            
                            { "sTitle": "SPORID", "mData": "SPORID", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "CustomerName", "mData": "CustomerName", "sClass": "left", "sWidth": "150px" },
                            { "sTitle": "Address", "mData": "Address", "sClass": "left", "sWidth": "250px" },
                            { "sTitle": "City", "mData": "City", "sClass": "left", "sWidth": "70px" },
                            { "sTitle": "State", "mData": "State", "sClass": "left", "sWidth": "40px" },
                            { "sTitle": "CallDate", "mData": "callDate", "sClass": "left", "sWidth": "100px" },
                            { "sTitle": "CalledBy", "mData": "calledBy", "sClass": "left", "sWidth": "50px" }
                          ]
                });

                //$('#LeadershipCallList').css('width', screen.width - 100);
                $('#LeadershipCallList').css('width', $(window).width() - 100);
            }
            catch (err) {
                alert(err.message);
            }
        }                       
    </script>    
    <div class="row-fluid">
        <div id="LeadershipCallListContent" class="span12" style="display:none;">
            <fieldset>
                <legend>Leadership Call</legend>
                <div align="left">
                    <table>
                        <tr>
                            <td align="left">
                                Export to : <asp:LinkButton ID="lnkExportToExcel" runat="server" onclick="lnkExportToExcel_Click">Excel</asp:LinkButton>&nbsp;|&nbsp;<asp:LinkButton ID="lnkExportToPdf" runat="server" onclick="lnkExportToPdf_Click">PDF</asp:LinkButton>&nbsp;|&nbsp;<asp:LinkButton ID="lnkExportToCSV" runat="server" onclick="lnkExportToCSV_Click">CSV</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="LeadershipCallList" style="margin-top: 20px;overflow:auto;">
                    <table style=" width:100%" cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped" id="LeadershipCallTbl">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>            
                    </table>
                </div>         
            </fieldset>
        </div>        
    </div>    
</asp:Content>