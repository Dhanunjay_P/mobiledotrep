﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;
using System.IO;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Text;
using ClosedXML.Excel;

public partial class LeadershipCallReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void lnkExportToExcel_Click(object sender, EventArgs e)
    {
        String Message = String.Empty;
        BLL.Masters.LeadershipCallReport objLeadershipCallReport = new BLL.Masters.LeadershipCallReport();
        DataTable dtLeadershipCall = objLeadershipCallReport.GetLeadershipCallData(ref Message);
        if (dtLeadershipCall != null && dtLeadershipCall.Rows.Count > 0)
        {            
            String fileName = "LeadershipCall" + "_" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xlsx";
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dtLeadershipCall, dtLeadershipCall.TableName);

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }

            //String filePath = Server.MapPath("~");
            //if (ExcelFunction.GenerateExcel(dtLeadershipCall, filePath + "\\" + fileName, ref Message))
            //{
            //    Response.Clear();
            //    Response.Buffer = true;
            //    Response.Charset = "";
            //    //Response.ContentType = "application/octet-stream"; //Response.ContentType = "application/vnd.ms-excel"; //Response.ContentType = "application/octet-stream";
            //    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //    Response.AppendHeader("content-disposition", "attachment; filename=" + fileName);
            //    Response.TransmitFile(filePath + "\\" + fileName);  //Response.WriteFile(filePath + "\\" + fileName); //Response.TransmitFile(filePath + "\\" + fileName + ".xls");
            //    Response.Flush();
            //    Response.Close();
            //    Response.End();
            //    //HttpContext.Current.ApplicationInstance.CompleteRequest();                
            //}

            //String FileName = "LeadershipCall";
            //HtmlForm form = new HtmlForm();
            //Response.Clear();
            //Response.Buffer = true;
            //Response.Charset = "";
            //Response.AddHeader("content-disposition", string.Format("attachment;filename={0}", FileName + "_" + DateTime.Today.ToString("dd-MMM-yyyy") + ".xls"));
            //Response.ContentType = "application/ms-excel";
            ////Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //StringWriter sw = new StringWriter();
            //HtmlTextWriter hw = new HtmlTextWriter(sw);

            //// Create a dynamic control, populate and render it
            //GridView excel = new GridView();
            //excel.AllowPaging = false;
            //excel.DataSource = dtLeadershipCall;
            //excel.DataBind();

            //form.Attributes["runat"] = "server";
            //form.Controls.Add(excel);
            //this.Controls.Add(form);
            //form.RenderControl(hw);
            //string style = @"<style> .textmode { mso-number-format:\@;}</style>";
            //Response.Write(style);
            //Response.Output.Write(sw.ToString());
            //Response.Flush();
            //Response.Close();
            ////Response.End();
        }
    }

    protected void lnkExportToPdf_Click(object sender, EventArgs e)
    {
        String Message = String.Empty;
        BLL.Masters.LeadershipCallReport objLeadershipCallReport = new BLL.Masters.LeadershipCallReport();
        DataTable dtLeadershipCall = objLeadershipCallReport.GetLeadershipCallData(ref Message);
        if (dtLeadershipCall != null && dtLeadershipCall.Rows.Count > 0)
        {
            String FileName = "LeadershipCall";

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + FileName + "_" + DateTime.Today.ToString("dd-MMM-yyyy") + ".pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            // Create a dynamic control, populate and render it
            GridView pdf = new GridView();
            pdf.AllowPaging = false;
            pdf.DataSource = dtLeadershipCall;
            pdf.DataBind();

            pdf.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4.Rotate(), 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();

            //HtmlForm form = new HtmlForm();
            //form.Attributes["runat"] = "server";
            //form.Controls.Add(pdf);
            //this.Controls.Add(form);
            //form.RenderControl(hw);
            //StringReader sr = new StringReader(sw.ToString());
            //iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(PageSize.A4, 10f, 10f, 10f, 0f);
            //HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            //PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            //pdfDoc.Open();
            //htmlparser.Parse(sr);
            //pdfDoc.Close();
            //Response.Write(pdfDoc);
            //Response.End();
        }
    }

    protected void lnkExportToCSV_Click(object sender, EventArgs e)
    {
        String Message = String.Empty;
        BLL.Masters.LeadershipCallReport objLeadershipCallReport = new BLL.Masters.LeadershipCallReport();
        DataTable dtLeadershipCall = objLeadershipCallReport.GetLeadershipCallData(ref Message);
        if (dtLeadershipCall != null && dtLeadershipCall.Rows.Count > 0)
        {
            String FileName = "LeadershipCall";
            HtmlForm form = new HtmlForm();
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.AddHeader("content-disposition", string.Format("attachment;filename={0}", FileName + "_" + DateTime.Today.ToString("dd-MMM-yyyy") + ".csv"));
            Response.ContentType = "application/text";

            StringBuilder sb = new StringBuilder();
            for (int k = 0; k < dtLeadershipCall.Columns.Count; k++)
            {
                //add separator
                sb.Append(dtLeadershipCall.Columns[k].ColumnName.Replace(",", "") + ',');
            }
            //append new line
            sb.Append("\r\n");
            for (int i = 0; i < dtLeadershipCall.Rows.Count; i++)
            {
                for (int k = 0; k < dtLeadershipCall.Columns.Count; k++)
                {
                    //add separator
                    sb.Append(dtLeadershipCall.Rows[i][k].ToString().Replace(",", "") + ',');
                }
                //append new line
                sb.Append("\r\n");
            }

            Response.Output.Write(sb.ToString());
            Response.Flush();
            Response.End();
        }
    }
}