﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login_Air.aspx.cs" Inherits="Login_Air" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AIR Portal</title>
    <link href="https://fonts.google.com/specimen/Roboto" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="~/css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="~/Scripts/jquery-1.11.1.min.js"></script>
    <script src="Scripts/bootbox.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $("#login").click(function (event) {
                if (document.getElementById("username").value == "") {
                    alert("Please enter username.");
                    document.getElementById("username").style.borderColor = 'red';
                    document.getElementById("username").focus();
                    return false;
                } else {
                    UserName = document.getElementById("username").value;
                    document.getElementById("username").style.borderColor = '';
                }
                if (document.getElementById("password").value == "") {
                    alert("Please enter password.");
                    document.getElementById("password").style.borderColor = 'red';
                    document.getElementById("password").focus();
                    return false;
                }
                else {
                    Password = document.getElementById("password").value;
                    document.getElementById("password").style.borderColor = '';
                }
                DeGrToolWebService.LoginWebSite_Air(UserName, Password, onSuccess, onFail);
            });
            $(document).keypress(function (e) {

                if (e.keyCode == 13 && $(document).find('#login').length == 1) {
                    $('#login').trigger('click');
                }
            });
            $("#log_cor").click(function () {

                //window.location.href = "https://zspharma.okta.com/app/zspharma_air_1/exkaqhs28836jh6vA0x7/sso/saml";
                window.location.href = "https://ping.astrazeneca.com/idp/startSSO.ping?PartnerSpId=az:sp:AIR";


            });
        });

        //$(Document).on("click", "#login", function () {

        //    if (document.getElementById("username").value == "") {
        //        alert("Please enter username.");
        //        document.getElementById("username").style.borderColor = 'red';
        //        document.getElementById("username").focus();
        //        return false;
        //    } else {
        //        UserName = document.getElementById("username").value;
        //        document.getElementById("username").style.borderColor = '';
        //    }
        //    if (document.getElementById("password").value == "") {
        //        alert("Please enter password.");
        //        document.getElementById("password").style.borderColor = 'red';
        //        document.getElementById("password").focus();
        //        return false;
        //    }
        //    else {
        //        Password = document.getElementById("password").value;
        //        document.getElementById("password").style.borderColor = '';
        //    }
        //    DeGrToolWebService.LoginWebSite_Air(UserName, Password, onSuccess, onFail);
        //});

        function onSuccess(data) {
            if (data == "1") {
                sessionStorage["username"] = document.getElementById("username").value;
                sessionStorage["is_single"] = "N";
                location.href = "Issue_detail.aspx";
                //var name = document.getElementById("username").value;
            }
            else if (data == "2") {
                alert("User id is inactive.");
            }
            else {

                alert("Wrong password. Try again.");
            }
        }

        function onFail(error) {
            //  alert("Ajax Fail");
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
            <Services>
                <asp:ServiceReference Path="~/DeGrToolWebService.asmx" />
            </Services>
        </asp:ScriptManager>
        <div id="site-content">

            <header class="site-header">

                <div class="bottom-header">
                    <div class="container">
                        <a class="branding pull-left">

                            <div class="col-xs-11">
                                <div class="col-xs-6">
                                    <img src="Images_air/zs_pharma_logo.png" alt="Site title" class="logo-icon" style="margin-top: 10px;">
                                </div>

                                <div class="col-xs-4">
                                    <div class="header_width" align="center">

                                        <img src="Images_air/AIR.png" alt="Site title" class="logo-icon" style="margin-top: 3px; width: 100px;">
                                    </div>
                                </div>
                            </div>


                        </a>
                        <!-- #branding -->

                        <!-- .main-navigation -->
                    </div>
                    <!-- .container -->
                </div>
            </header>
            <!-- .site-header -->

            <main class="content">
				
				<div class="page-content myfont_regular">
					
                   <body>

                       <div class="fullwidth-block">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-offset-3 myfont_regular">
              
                <form id="frm_ci_info">
                    <div class="row bod" align="center" >
                       
                        <div class="col-lg-7 col-md-8" style="margin-bottom: 35px;"  >
                          <h1 class="myfont_regular" style="margin-bottom: 28px;margin-left: 15px;">Login</h1>
                            <div class="row" style="margin-left: -68px; min-height: 34px;">
                                <div class="col-lg-6" >
                             <label>Username</label></div><div class="col-lg-6">
                             <input type="text" placeholder="Enter Username" name="uname" id="username" required></div>
                             <div class="col-lg-6" >
                             <label >Password</label></div> <div class="col-lg-6">
                             <input type="password" placeholder="Enter Password" name="psw" id="password" required></div>
                       </div>
                                 </div>
                       
                        <div class="col-lg-7 col-md-8" >                           
                            
                            <input type="button" class="button wow fadeInRight" style="margin-left:20px;font-size: 13px;" value="Submit" id="login">
                             <input type="button" class="button wow fadeInRight" style="margin-left:-31px;font-size: 13px;" value="Login with Corporate User" id="log_cor">
                        </div>
                           
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
                   </body>
					
				</div> <!-- .inner-content -->
			</main>
            <!-- .content -->



        </div>
    </form>
</body>
</html>

<style type="text/css">
    body {
        background: #fff url(Images_air/bg-bg.png) -15px 37% no-repeat;
        /*background: #fff url(/CIPortal/Images/bg-bg.png) -15px 37% no-repeat;*/
    }

    .bod {
        background: #fff url(Images_air/bg-bg.png) -100px 53% no-repeat;
        height: 248px;
        border: 1px double black;
        width: 42%;
        /* HEIGHT: 25%; */
        padding-top: 1%;
        padding-left: 10%;
        margin-left: 124px;
        margin-right: 53px;
        margin-top: 58px;
        /* HEIGHT: 179%; */
    }

    }

    .modal-backdrop {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 1040;
        background-color: #000;
    }

        .modal-backdrop.fade {
            opacity: 0;
        }

            .modal-backdrop, .modal-backdrop.fade.in {
                opacity: .8;
                filter: alpha(opacity=80);
            }

    .modal {
        position: fixed;
        top: 10%;
        left: 50%;
        z-index: 1050;
        width: 560px;
        margin-left: -280px;
        background-color: #fff;
        border: 1px solid #999;
        border: 1px solid rgba(0,0,0,0.3);
        *border: 1px solid #999;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        border-radius: 6px;
        outline: 0;
        -webkit-box-shadow: 0 3px 7px rgba(0,0,0,0.3);
        -moz-box-shadow: 0 3px 7px rgba(0,0,0,0.3);
        box-shadow: 0 3px 7px rgba(0,0,0,0.3);
        -webkit-background-clip: padding-box;
        -moz-background-clip: padding-box;
        background-clip: padding-box;
    }

        .modal.fade {
            top: -25%;
            -webkit-transition: opacity .3s linear,top .3s ease-out;
            -moz-transition: opacity .3s linear,top .3s ease-out;
            -o-transition: opacity .3s linear,top .3s ease-out;
            transition: opacity .3s linear,top .3s ease-out;
        }

            .modal.fade.in {
                top: 10%;
            }

    .modal-header {
        padding: 9px 15px;
        border-bottom: 1px solid #eee;
    }

        .modal-header .close {
            margin-top: 2px;
        }

        .modal-header h3 {
            margin: 0;
            line-height: 30px;
        }

    .modal-body {
        position: relative;
        max-height: 500px;
        padding: 15px;
        overflow-y: auto;
    }

    .modal-form {
        margin-bottom: 0;
    }

    .modal-footer {
        padding: 14px 15px 15px;
        margin-bottom: 0;
        text-align: right;
        background-color: #f5f5f5;
        border-top: 1px solid #ddd;
        -webkit-border-radius: 0 0 6px 6px;
        -moz-border-radius: 0 0 6px 6px;
        border-radius: 0 0 6px 6px;
        *zoom: 1;
        -webkit-box-shadow: inset 0 1px 0 #fff;
        -moz-box-shadow: inset 0 1px 0 #fff;
        box-shadow: inset 0 1px 0 #fff;
    }

        .modal-footer:before, .modal-footer:after {
            display: table;
            line-height: 0;
            content: "";
        }

        .modal-footer:after {
            clear: both;
        }

        .modal-footer .btn + .btn {
            margin-bottom: 0;
            margin-left: 5px;
        }

        .modal-footer .btn-group .btn + .btn {
            margin-left: -1px;
        }

    .fade {
        opacity: 0;
        -webkit-transition: opacity .15s linear;
        -moz-transition: opacity .15s linear;
        -o-transition: opacity .15s linear;
        transition: opacity .15s linear;
    }

        .fade.in {
            opacity: 1;
        }

    .modal-backdrop.fade {
        opacity: 0;
    }

        .modal-backdrop, .modal-backdrop.fade.in {
            opacity: .8;
            filter: alpha(opacity=80);
        }

    .modal {
        position: fixed;
        top: 10%;
        left: 50%;
        z-index: 1050;
        width: 560px;
        margin-left: -280px;
        background-color: #fff;
        border: 1px solid #999;
        border: 1px solid rgba(0,0,0,0.3);
        *border: 1px solid #999;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        border-radius: 6px;
        outline: 0;
        -webkit-box-shadow: 0 3px 7px rgba(0,0,0,0.3);
        -moz-box-shadow: 0 3px 7px rgba(0,0,0,0.3);
        box-shadow: 0 3px 7px rgba(0,0,0,0.3);
        -webkit-background-clip: padding-box;
        -moz-background-clip: padding-box;
        background-clip: padding-box;
    }

        .modal.fade {
            top: -25%;
            -webkit-transition: opacity .3s linear,top .3s ease-out;
            -moz-transition: opacity .3s linear,top .3s ease-out;
            -o-transition: opacity .3s linear,top .3s ease-out;
            transition: opacity .3s linear,top .3s ease-out;
        }

            .modal.fade.in {
                top: 10%;
            }


            .modal.fade.in {
                top: 10%;
            }

        .modal.fade {
            top: -25%;
            -webkit-transition: opacity .3s linear,top .3s ease-out;
            -moz-transition: opacity .3s linear,top .3s ease-out;
            -o-transition: opacity .3s linear,top .3s ease-out;
            transition: opacity .3s linear,top .3s ease-out;
        }

    .fade.in {
        opacity: 1;
    }

    .modal {
        border-radius: 0;
    }

    .modal {
        position: fixed;
        top: 10%;
        left: 50%;
        z-index: 1050;
        width: 560px;
        margin-left: -280px;
        background-color: #fff;
        border: 1px solid #999;
        border: 1px solid rgba(0,0,0,0.3);
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        border-radius: 6px;
        outline: 0;
        -webkit-box-shadow: 0 3px 7px rgba(0,0,0,0.3);
        -moz-box-shadow: 0 3px 7px rgba(0,0,0,0.3);
        box-shadow: 0 3px 7px rgba(0,0,0,0.3);
        -webkit-background-clip: padding-box;
        -moz-background-clip: padding-box;
        background-clip: padding-box;
    }

    .fade {
        opacity: 0;
        -webkit-transition: opacity .15s linear;
        -moz-transition: opacity .15s linear;
        -o-transition: opacity .15s linear;
        transition: opacity .15s linear;
    }

    .modal-body {
        position: relative;
        max-height: 500px;
        padding: 15px;
        overflow-y: auto;
        color: black;
    }

    button.close {
        padding: 0;
        cursor: pointer;
        background: transparent;
        border: 0;
        -webkit-appearance: none;
    }

    .close {
        float: right;
        font-size: 20px;
        font-weight: bold;
        line-height: 20px;
        color: #000;
        text-shadow: 0 1px 0 #fff;
        opacity: .2;
        filter: alpha(opacity=20);
    }

    .modal-footer {
        border-top-color: #e4e9ee;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
        background-color: #eff3f8;
    }

    .modal-footer {
        padding: 14px 15px 15px;
        margin-bottom: 0;
        text-align: right;
        background-color: #f5f5f5;
        border-top: 1px solid #ddd;
        -webkit-border-radius: 0 0 6px 6px;
        -moz-border-radius: 0 0 6px 6px;
        border-radius: 0 0 6px 6px;
        -webkit-box-shadow: inset 0 1px 0 #fff;
        -moz-box-shadow: inset 0 1px 0 #fff;
        box-shadow: inset 0 1px 0 #fff;
    }

    /*.btn-primary {
    background-color: #5B9BD5!important;
    border-color: #5B9BD5;
}

.btn, .btn-default {
    background-color: #abbac3!important;
    border-color: #abbac3;
}

.btn {
    display: inline-block;
    color: #FFF!important;
    text-shadow: 0 -1px 0 rgba(0,0,0,0.25)!important;
    background-image: none!important;
    border: 5px solid;
    border-radius: 0;
    box-shadow: none!important;
    -webkit-transition: all .1s ease-in-out;
    -moz-transition: all .1s ease-in-out;
    -o-transition: all .1s ease-in-out;
    transition: all .1s ease-in-out;
    cursor: pointer;
    vertical-align: middle;
    margin: 0;
    position: relative;
    padding: 0 12px 1px;
    line-height: 32px;
    font-size: 14px;
}

.btn-primary {
    color: #fff;
    text-shadow: 0 -1px 0 rgba(0,0,0,0.25);
    background-color: #006dcc;
    background-repeat: repeat-x;
    background-image: linear-gradient(to bottom,#08c,#04c);
    border-left-color: #04c;
    border-right-color: #04c;
    border-top-color: #04c;
    border-bottom-color: #002a80;
}

.btn {
    display: inline-block;
    border-right: 0 none #e6e6e6;
    border-top: 0 none #e6e6e6;
    border-bottom: 0 none #b3b3b3;
    display: inline;
    padding: 4px 12px;
    margin-bottom: 0;
    font-size: 14px;
    line-height: 20px;
    color: #333;
    text-align: center;
    text-shadow: 0 1px 1px rgba(255,255,255,0.75);
    vertical-align: middle;
    cursor: pointer;
    background-color: #f5f5f5;
    background-repeat: repeat-x;
    -moz-border-radius: 4px;
    border-radius: 4px;
    zoom: 1;
    -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
    -moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
    box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
    background-image: linear-gradient(to bottom,#fff,#e6e6e6);
}*/
</style>
