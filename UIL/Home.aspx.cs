﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Home : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        HttpCookie groupType = Request.Cookies["GroupType"];
        if (groupType != null)
        {
            if (groupType.Value != null && groupType.Value != "")
            {
                DeGrToolWebService obj = new DeGrToolWebService();
                menu.InnerHtml = obj.GetGroupRights(groupType.Value);
            }
        }        
    }
}