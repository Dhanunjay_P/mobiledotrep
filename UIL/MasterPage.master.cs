﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.Now.AddYears(-60));

        HttpCookie groupType = Request.Cookies["GroupType"];
        if (groupType != null)
        {
            if (groupType.Value != null && groupType.Value != "")
            {
                DeGrToolWebService obj = new DeGrToolWebService();
                if (menu != null)
                    menu.InnerHtml = obj.GetGroupRights(groupType.Value);

            }
        }

        //Dictionary<string, object> SessionData = new Dictionary<string, object>();
        //if (SessionData.ContainsKey("SessionId"))
        //    SessionData["SessionId"] = Session["SessionId"];
        //else
        //    SessionData.Add("SessionId", Session["SessionId"]);
        //if (SessionData.ContainsKey("HostIP"))
        //    SessionData["HostIP"] = Session["HostIP"];
        //else
        //    SessionData.Add("HostIP", Session["HostIP"]);
        //if (SessionData.ContainsKey("HostName"))
        //    SessionData["HostName"] = Session["HostName"];
        //else
        //    SessionData.Add("HostName", Session["HostName"]);
        //if (SessionData.ContainsKey("UserId"))
        //    SessionData["UserId"] = Session["UserId"];
        //else
        //    SessionData.Add("UserId", Session["UserId"]);
        //if (SessionData.ContainsKey("UserName"))
        //    SessionData["UserName"] = Session["UserName"];
        //else
        //    SessionData.Add("UserName", Session["UserName"]);
        //if (SessionData.ContainsKey("SaId"))
        //    SessionData["SaId"] = Session["sa_id"];
        //else
        //    SessionData.Add("SaId", Session["sa_id"]);
        //if (SessionData.ContainsKey("CompanyId"))
        //    SessionData["CompanyId"] = Session["company_code"];
        //else
        //    SessionData.Add("CompanyId", Session["company_code"]);
        //JavaScriptSerializer ser = new JavaScriptSerializer();
        //hidSession.Value = ser.Serialize(SessionData);
    }
}
