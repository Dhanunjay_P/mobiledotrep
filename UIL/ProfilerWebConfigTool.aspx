﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ProfilerWebConfigTool.aspx.cs" Inherits="ProfilerWebConfigTool" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderTitle" runat="Server">
    Profiler WebConfig Tool
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="ClientScripts/dist/css/bootstrap-multiselect.css" type="text/css">
    <script type="text/javascript" src="ClientScripts/dist/js/bootstrap-multiselect.js"></script>
    <style type="text/css">
        ul.multiselect-container.dropdown-menu
        {
            height: 500px;
            overflow-y: auto;
        }
    </style>
    <script src="Scripts/jscolor.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        var OriginalData = new Object();
        var NewUploadData = new Object();
        var OldUploadData = new Object();
        var oTableAccountType = null;
        var oTableCategory = null;
        var oTableAttribute = null;
        var validator;

        $("legend").live('click', function () {
            $(this).siblings().slideToggle("slow");
        });

        function pageLoad() {

            //----- Restrict input field to Decimal ----- 
            $(".numeric").keypress(function (e) {

                //if the letter is not digit then display error and don't type anything
                if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
                else if (e.which == 46 && $(this).val().indexOf('.') != -1) {
                    e.preventDefault();
                } // prevent if already dot
            });

            //----- Restrict Cut, Copy and Paste to Decimal ----- 
            $('.numeric').bind("cut copy paste", function (e) {
                e.preventDefault();
            });

            //SessionData = JSON.parse(document.getElementById("ctl00_ContentPlaceHolder1_hidSession").value);

            $("#ddlAccountType").empty();
            $("#ddlAccountTypeCategory").empty();
            $("#ddlAccountTypeAttribute").empty();

            PrepareBasicObjects("ProfilerWebConfigTool", "GetInitialData", "M", "S", "M001", "M003N");

            AutoShowMessage = false;
            CallWebService(OnPageLoadSuccess);
        }

        function OnPageLoadSuccess() {

            if (ResultObject != null) {
                if (ResultObject.responseObjectInfo.Status == 1) {

                    BindDropDown(ResultObject.responseObjectInfo.dt_ReturnedTables[0], "ddlDatabase", "code", "desc", FirstIndexAS.Select);
                }
                else
                    $("#ddlDatabase").empty();
            }
        }

        function GetAccountTypeAndCategoryAndAttributeList() {

            $("#NewEntryAndModifyAccountType").hide();
            $("#NewEntryAndModifyCategory").hide();
            $("#NewEntryAndModifyAttribute").hide();
            $("#AccountTypeListContent").hide();
            $("#CategoryListContent").hide();
            $("#AttributeListContent").hide();
            $("#ddlAccountType").empty();
            $("#ddlAccountTypeCategory").empty();
            $("#ddlAccountTypeAttribute").empty();
            $("#ddlCategoryCode").empty();

            PrepareBasicObjects("ProfilerWebConfigTool", "GetAccountTypeAndCategoryAndAttributeList", "M", "S", "M001", "M003N");

            var database = $("#ddlDatabase").val();

            OpArgsObject["database"] = database;

            AutoShowMessage = false;
            CallWebService(OnGetAccountTypeAndCategoryAndAttributeListSuccess);
        }

        function OnGetAccountTypeAndCategoryAndAttributeListSuccess() {
            debugger;
            if (ResultObject != null) {

                if (ResultObject.responseObjectInfo.Status == 1) {

                    if (ResultObject.responseObjectInfo.dt_ReturnedTables[0] != null) {
                        BindDropDown(ResultObject.responseObjectInfo.dt_ReturnedTables[0], "ddlAccountType", "AccountType", "AccountTypeDescription", FirstIndexAS.Select);
                        BindDropDown(ResultObject.responseObjectInfo.dt_ReturnedTables[0], "ddlAccountTypeCategoryFilter", "AccountType", "AccountTypeDescription", FirstIndexAS.ALL);
                        BindDropDown(ResultObject.responseObjectInfo.dt_ReturnedTables[0], "ddlAccountTypeAttributeFilter", "AccountType", "AccountTypeDescription", FirstIndexAS.ALL);
                        BindDropDown(ResultObject.responseObjectInfo.dt_ReturnedTables[0], "ddlAccountTypeCategory", "AccountType", "AccountTypeDescription", FirstIndexAS.Select);
                        BindDropDown(ResultObject.responseObjectInfo.dt_ReturnedTables[0], "ddlAccountTypeAttribute", "AccountType", "AccountTypeDescription", FirstIndexAS.Select);
                        DisplayAccountTypeData(ResultObject.responseObjectInfo.dt_ReturnedTables[0]);
                        $("#AccountTypeListContent").show();
                    }
                    else
                        $("#AccountTypeListContent").hide();

                    if (ResultObject.responseObjectInfo.dt_ReturnedTables[1] != null) {

                        BindDropDown(ResultObject.responseObjectInfo.dt_ReturnedTables[1], "ddlCategoryCode", "CategoryCode", "CategoryDescription", FirstIndexAS.Select);
                        BindDropDown(ResultObject.responseObjectInfo.dt_ReturnedTables[1], "ddlCategoryAttributeFilter", "CategoryCode", "CategoryDescription", FirstIndexAS.ALL);
                        DisplayCategoryData(ResultObject.responseObjectInfo.dt_ReturnedTables[1]);
                        $("#CategoryListContent").show();
                    }
                    else
                        $("#CategoryListContent").hide();

                    if (ResultObject.responseObjectInfo.dt_ReturnedTables[2] != null) {

                        DisplayAttributeData(ResultObject.responseObjectInfo.dt_ReturnedTables[2]);
                        $("#AttributeListContent").show();
                    }
                    else
                        $("#AttributeListContent").hide();

                }
                else {

                    $("#ddlAccountType").empty();
                    $("#ddlAccountTypeCategory").empty();
                    $("#ddlAccountTypeAttribute").empty();
                    $("#AccountTypeListContent").hide();
                    $("#CategoryListContent").hide();
                    $("#AttributeListContent").hide();
                }
            }
        }

        function DisplayAccountTypeData(data) {

            try {
                if (oTableAccountType != null) {
                    oTableAccountType.fnDestroy();
                    $("#AccountTypeList").html(' <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped" id="AccountTypeTbl"><thead></thead><tbody> </tbody><tfoot> </tfoot></table>');
                }
                oTableAccountType = $("#AccountTypeTbl").dataTable({
                    "iDisplayLength": 25,
                    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                    "aaData": data,
                    "aoColumns": [
                            {
                                "sTitle": '<div style="width:40px;text-align:center;margin-bottom:2px;"><i class="icon-plus-sign" title="Add AccountType" id="btnAddAccountType" onclick="AddAccountType()" data-mode="ADD"></i></div>',
                                "mData": null,
                                "sClass": "center",
                                "sWidth": "20px",
                                "sDefaultContent": '<i id="imgEditCategory" title="Edit AccountType" class="icon-pencil"></i>&nbsp;<i id="imgDeleteAccountType" title="Delete AccountType" class="icon-trash"></i>',
                                "bSearchable": false,
                                "bSortable": false
                            },
                            { "sTitle": "AccountType", "mData": "AccountType", "sClass": "left", "sWidth": "80px" },
                            { "sTitle": "AccountTypeDescription", "mData": "AccountTypeDescription", "sClass": "left", "sWidth": "150px" },
                            { "sTitle": "TableName", "mData": "TableName", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "AccountIdColumn", "mData": "AccountIdColumn", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "AccountParentIdColumn", "mData": "AccountParentIdColumn", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "AccountNameColumn", "mData": "AccountNameColumn", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "AccountTypeColumn", "mData": "AccountTypeColumn", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "AccountAddressColumn", "mData": "AccountAddressColumn", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "AccountCityStateZipColumn", "mData": "AccountCityStateZipColumn", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "LatitudeColumn", "mData": "LatitudeColumn", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "LongitudeColumn", "mData": "LongitudeColumn", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "NoOfRecord", "mData": "NoOfRecord", "sClass": "right", "sWidth": "50px" },
                            { "sTitle": "Active", "mData": "IsActive", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "SeqNo", "mData": "SeqNo", "sClass": "right", "sWidth": "50px" },
                            { "sTitle": "FileName", "mData": "FileName", "sClass": "left", "sWidth": "100px" },
                            { "sTitle": "CallType", "mData": "CallType", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "ImageURL", "mData": "ImageURL", "sClass": "left", "sWidth": "150px" },
                            { "sTitle": "AccountListDependOn", "mData": "AccountListDependOn", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "AddressMstTableName", "mData": "AddressMstTableName", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "AddressMstIdColumn", "mData": "AddressMstIdColumn", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "PageFooterView", "mData": "PageFooterView", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "LastUpdatedDateTime", "mData": "LastUpdatedDateTime", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "AccountSelectionCriterion", "mData": "AccountSelectionCriterion", "sClass": "left", "sWidth": "50px" }
                          ]
                });

                //$('#AccountTypeList').css('width', screen.width - 100);
                $('#AccountTypeList').css('width', $(window).width() - 100);
            }
            catch (err) {
                alert(err.message);
            }
        }

        function DisplayCategoryData(data) {

            try {
                if (oTableCategory != null) {
                    oTableCategory.fnDestroy();
                    $("#CategoryList").html(' <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped" id="CategoryTbl"><thead></thead><tbody> </tbody><tfoot> </tfoot></table>');
                }
                oTableCategory = $("#CategoryTbl").dataTable({
                    "iDisplayLength": 25,
                    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                    "aaData": data,
                    "aoColumns": [
                            {
                                "sTitle": '<div style="width:40px;text-align:center;margin-bottom:2px;"><i class="icon-plus-sign" title="Add Category" id="btnAddCategory" onclick="AddCategory()" data-mode="ADD"></i></div>',
                                "mData": null,
                                "sClass": "center",
                                "sWidth": "20px",
                                "sDefaultContent": '<i id="imgEditCategory" title="Edit Category" class="icon-pencil"></i>&nbsp;<i id="imgDeleteCategory" title="Delete Category" class="icon-trash"></i>',
                                "bSearchable": false,
                                "bSortable": false
                            },
                            { "sTitle": "AccountType", "mData": "AccountType", "sClass": "left", "sWidth": "80px" },
                            { "sTitle": "CategoryCode", "mData": "CategoryCode", "sClass": "left", "sWidth": "80px" },
                            { "sTitle": "CategoryDescription", "mData": "CategoryDescription", "sClass": "left", "sWidth": "150px" },
                            { "sTitle": "CategorySeqNo", "mData": "CategorySeqNo", "sClass": "right", "sWidth": "50px" },
                            { "sTitle": "FileName", "mData": "FileName", "sClass": "left", "sWidth": "100px" },
                            { "sTitle": "ImageURL", "mData": "ImageURL", "sClass": "left", "sWidth": "150px" },
                            { "sTitle": "Active", "mData": "IsActive", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "AllowAdd", "mData": "AllowAdd", "sClass": "left", "sWidth": "50px" }
                          ]
                });

                //$('#CategoryList').css('width', screen.width - 100);
                $('#CategoryList').css('width', $(window).width() - 100);
            }
            catch (err) {
                alert(err.message);
            }
        }

        function DisplayAttributeData(data) {

            try {
                if (oTableAttribute != null) {
                    oTableAttribute.fnDestroy();
                    $("#AttributeList").html(' <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped" id="AttributeTbl"><thead></thead><tbody> </tbody><tfoot> </tfoot></table>');
                }
                oTableAttribute = $("#AttributeTbl").dataTable({
                    "iDisplayLength": 25,
                    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                    "aaData": data,
                    "aoColumns": [
                            {
                                "sTitle": '<div style="width:40px;text-align:center;margin-bottom:2px;"><i class="icon-plus-sign" title="Add Attribute" id="btnAddAttribute" onclick="AddAttribute()" data-mode="ADD"></i></div>',
                                "mData": null,
                                "sClass": "center",
                                "sWidth": "20px",
                                "sDefaultContent": '<i id="imgEditAttribute" title="Edit Attribute" class="icon-pencil"></i>&nbsp;<i id="imgDeleteAttribute" title="Delete Attribute" class="icon-trash"></i>',
                                "bSearchable": false,
                                "bSortable": false
                            },
                            { "sTitle": "AccountType", "mData": "AccountType", "sClass": "left", "sWidth": "80px" },
                            { "sTitle": "CategoryCode", "mData": "CategoryCode", "sClass": "left", "sWidth": "80px" },
                            { "sTitle": "AttributeCode", "mData": "AttributeCode", "sClass": "left", "sWidth": "80px" },
                            { "sTitle": "AttributeDescription", "mData": "AttributeDescription", "sClass": "left", "sWidth": "150px" },
                            { "sTitle": "AttributeSeqNo", "mData": "AttributeSeqNo", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "ShowWhenEmpty", "mData": "ShowWhenEmpty", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "ShowInFilter", "mData": "ShowInFilter", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "Active", "mData": "IsActive", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "TableName", "mData": "TableName", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "IdColumn", "mData": "IdColumn", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "DistinctIdColumn", "mData": "DistinctIdColumn", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "ColumnName", "mData": "ColumnName", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "IsEditable", "mData": "IsEditable", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "DisplayType", "mData": "DisplayType", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "EditType", "mData": "EditType", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "EditOptions", "mData": "EditOptions", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "IsAddressAttribute", "mData": "IsAddressAttribute", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "ImageLink", "mData": "ImageLink", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "IsNumeric", "mData": "IsNumeric", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "IsPhoneNumber", "mData": "IsPhoneNumber", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "Seperator", "mData": "Seperator", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "Format", "mData": "Format", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "NoOfLines", "mData": "NoOfLines", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "ColorValue", "mData": "ColorValue", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "ConditionalAttributeReq", "mData": "ConditionalAttributeReq", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "ConditionalAttribute", "mData": "ConditionalAttribute", "sClass": "left", "sWidth": "50px" }
                          ]
                });

                //$('#AttributeList').css('width', screen.width - 100);
                $('#AttributeList').css('width', $(window).width() - 100);
            }
            catch (err) {
                alert(err.message);
            }
        }

        function AddAccountType() {
            debugger;
            $("#txtNewEditAccountType").val("New");
            ClearAccountTypeData();
            $("#txtAccountType").prop("disabled", false);
            $("#NewEntryAndModifyAccountType").show();

            PrepareBasicObjects("AccountType", "GetAccountDetails", "M", "S", "M001", "M003N");

            BindDropDown(ResultObject.responseObjectInfo.dt_ReturnedTables[2], "example-multiple-selected", "AccountTypeGroupId", "AccountTypeGroupId", FirstIndexAS.Select);
            $('#example-multiple-selected').multiselect({
                maxheight: 200,
                includeSelectAllOption: true,
                includeSelectAllIfMoreThan: 0,
                selectAllText: ' Select all',
                numberDisplayed: 8

            });

            $("#AccountTypeList").hide();
        }

        //Click on Edit Image
        $('#AccountTypeTbl tbody tr td i.icon-pencil').live('click', function (e) {
            var row = $(this).closest("tr").get(0);
            var aData = oTableAccountType.fnGetData(row);

            //$.setFormData("Config_AccountTypeMst", aData);
            GetAccountTypeDetails(aData["AccountType"]);
            return false;
        });

        function GetAccountTypeDetails(AccountType) {

            PrepareBasicObjects("ProfilerWebConfigTool", "GetAccountTypeDetails", "M", "S", "M001", "M003N");

            var database = $("#ddlDatabase").val();

            OpArgsObject["database"] = database;
            OpArgsObject["account_type"] = AccountType;

            AutoShowMessage = false;
            CallWebService(OnGetAccountTypeDetailsSuccess);
        }

        function OnGetAccountTypeDetailsSuccess() {

            if (ResultObject != null) {
                if (ResultObject.responseObjectInfo.Status == 1) {

                    $("#NewEntryAndModifyAccountType").show();
                    $("#AccountTypeList").hide();
                    var aData = ResultObject.responseObjectInfo.dt_ReturnedTables[0][0];
                    $.setFormData("Config_AccountTypeMst", aData);

                    $("#txtNewEditAccountType").val("Edit");
                    $("#txtAccountType").prop("disabled", true);
                }
                else
                    ClearAccountTypeData();
            }
        }

        //Click on Delete Image
        $('#AccountTypeTbl tbody tr td i.icon-trash').live('click', function (e) {
            var row = $(this).closest("tr").get(0);
            var aData = oTableAccountType.fnGetData(row);
            //$.setFormData("Config_AccountTypeMst", aData);
            var answer = confirm("Are Sure You Want to Delete AccountType : " + aData["AccountTypeDescription"] + " ?");
            if (answer) {
                GetAccountTypeDetailForDelete(aData["AccountType"]);
            }
            return false;
        });

        function GetAccountTypeDetailForDelete(AccountType) {

            PrepareBasicObjects("ProfilerWebConfigTool", "GetAccountTypeDetails", "M", "S", "M001", "M003N");

            var database = $("#ddlDatabase").val();

            OpArgsObject["database"] = database;
            OpArgsObject["account_type"] = AccountType;

            AutoShowMessage = false;
            CallWebService(OnGetAccountTypeDetailForDeleteSuccess);
        }

        function OnGetAccountTypeDetailForDeleteSuccess() {

            if (ResultObject != null) {
                if (ResultObject.responseObjectInfo.Status == 1) {

                    PrepareBasicObjects("ProfilerWebConfigTool", "SaveAccountTypeData", "M", "S", "M001", "M003N");

                    var listofrecords = new Array();
                    var form_header = ResultObject.responseObjectInfo.dt_ReturnedTables[0][0];
                    form_header["IsActive"] = "N";

                    listofrecords[0] = form_header;

                    NewUploadData["Config_AccountTypeMst"] = listofrecords;

                    OldUploadData = {};

                    UploadDataObject["New"] = NewUploadData;
                    UploadDataObject["Old"] = null;

                    $("#txtNewEditAccountType").val("Edit");
                    var database = $("#ddlDatabase").val();
                    var NewEdit = $("#txtNewEditAccountType").val();

                    OpArgsObject["database"] = database;
                    OpArgsObject["NewEdit"] = NewEdit;

                    AutoShowMessage = true;
                    CallWebService(OnSaveAccountTypeSuccess);
                }
            }
        }

        function PrepareUploadAccountTypeDataObject() {
            var listofrecords = new Array();
            var form_header = new Object();

            form_header = $.getFormData("Config_AccountTypeMst");

            listofrecords[0] = form_header;

            NewUploadData["Config_AccountTypeMst"] = listofrecords;

            OldUploadData = {};

            UploadDataObject["New"] = NewUploadData;
            UploadDataObject["Old"] = null;

            var database = $("#ddlDatabase").val();
            var NewEdit = $("#txtNewEditAccountType").val();

            OpArgsObject["database"] = database;
            OpArgsObject["NewEdit"] = NewEdit;
        }

        function SaveAccountTypeData() {

            if ($("#txtAccountType").val() == null || $("#txtAccountType").val() == "") {
                alert("Please enter Account Type.");
                $("#txtAccountType").focus();
                return false;
            }
            else if ($("#txtAccountTypeDescription").val() == null || $("#txtAccountTypeDescription").val() == "") {
                alert("Please enter AccountType Description.");
                $("#txtAccountTypeDescription").focus();
                return false;
            }
            else if ($("#txtTableNameAccountType").val() == null || $("#txtTableNameAccountType").val() == "") {
                alert("Please enter TableName.");
                $("#txtTableNameAccountType").focus();
                return false;
            }
            else if ($("#txtAccountIdColumn").val() == null || $("#txtAccountIdColumn").val() == "") {
                alert("Please enter AccountId Column.");
                $("#txtAccountIdColumn").focus();
                return false;
            }
            else if ($("#txtAccountNameColumn").val() == null || $("#txtAccountNameColumn").val() == "") {
                alert("Please enter AccountName Column.");
                $("#txtAccountNameColumn").focus();
                return false;
            }
            else if ($("#txtAccountTypeColumn").val() == null || $("#txtAccountTypeColumn").val() == "") {
                alert("Please enter AccountType Column.");
                $("#txtAccountTypeColumn").focus();
                return false;
            }
            else if ($("#txtAccountAddressColumn").val() == null || $("#txtAccountAddressColumn").val() == "") {
                alert("Please enter AccountAddress Column.");
                $("#txtAccountAddressColumn").focus();
                return false;
            }
            else if ($("#txtAccountCityStateZipColumn").val() == null || $("#txtAccountCityStateZipColumn").val() == "") {
                alert("Please enter AccountCityStateZip Column.");
                $("#txtAccountCityStateZipColumn").focus();
                return false;
            }
            else if ($("#txtLatitudeColumn").val() == null || $("#txtLatitudeColumn").val() == "") {
                alert("Please enter Latitude Column.");
                $("#txtLatitudeColumn").focus();
                return false;
            }
            else if ($("#txtLongitudeColumn").val() == null || $("#txtLongitudeColumn").val() == "") {
                alert("Please enter Longitude Column.");
                $("#txtLongitudeColumn").focus();
                return false;
            }
            else if ($("#txtSeqNo").val() == null || $("#txtSeqNo").val() == "") {
                alert("Please enter SeqNo Column.");
                $("#txtSeqNo").focus();
                return false;
            }
            else if ($("#chkAccountListDependOn").val() == null) {
                alert("Please select AccountListDependOn.");
                $("#chkAccountListDependOn").focus();
                return false;
            }

            var answer = confirm("Are Sure You Want To Save Data?");
            if (answer) {

                PrepareBasicObjects("ProfilerWebConfigTool", "SaveAccountTypeData", "M", "S", "M001", "M003N");

                PrepareUploadAccountTypeDataObject();

                AutoShowMessage = true;
                CallWebService(OnSaveAccountTypeSuccess);
            }
        }

        function OnSaveAccountTypeSuccess() {

            if (ResultObject != null) {
                if (ResultObject.responseObjectInfo.Status == 1) {

                    ClearAccountTypeData();

                    $("#NewEntryAndModifyAccountType").hide();
                    $("#AccountTypeList").show();

                    if (ResultObject.responseObjectInfo.dt_ReturnedTables[0] != null)
                        DisplayAccountTypeData(ResultObject.responseObjectInfo.dt_ReturnedTables[0]);
                }
            }
        }

        function BackAccountTypeData() {

            $("#NewEntryAndModifyAccountType").hide();
            $("#AccountTypeList").show();
        }

        function ClearAccountTypeData() {

            if ($("#txtNewEditAccountType").val() == "New")
                $("#txtAccountType").val("");
            $("#txtAccountTypeDescription").val("");
            $("#txtTableNameAccountType").val("");
            $("#txtAccountIdColumn").val("");
            $("#txtAccountParentIdColumn").val("");
            $("#txtAccountNameColumn").val("");
            $("#txtAccountTypeColumn").val("");
            $("#txtAccountAddressColumn").val("");
            $("#txtAccountCityStateZipColumn").val("");
            $("#txtLatitudeColumn").val("");
            $("#txtLongitudeColumn").val("");
            $("#txtNoOfRecord").val("");
            $("#chkIsActiveAccountType").val("Y");
            $("#txtSeqNo").val("");
            $("#txtFileNameAccountType").val("");
            $("#txtCallType").val("");
            $("#txtImageURLAccountType").val("");
            $("#chkAccountListDependOn").val("");
            $("#txtAddressMstTableName").val("");
            $("#txtAddressMstIdColumn").val("");
            $("#txtPageFooterView").val("");
            $("#txtAccountSelectionCriterion").val("");
        }

        function GetFilterCategory() {

            PrepareBasicObjects("ProfilerWebConfigTool", "GetCategoryDetails", "M", "S", "M001", "M003N");

            var database = $("#ddlDatabase").val();
            var AccountType = $("#ddlAccountTypeCategoryFilter").val();

            OpArgsObject["database"] = database;
            OpArgsObject["account_type"] = AccountType;

            AutoShowMessage = false;
            CallWebService(OnGetFilterCategorySuccess);
        }

        function OnGetFilterCategorySuccess() {

            if (ResultObject != null) {
                if (ResultObject.responseObjectInfo.Status == 1) {

                    if (ResultObject.responseObjectInfo.dt_ReturnedTables[0] != null)
                        DisplayCategoryData(ResultObject.responseObjectInfo.dt_ReturnedTables[0]);
                }
            }
        }

        function AddCategory() {

            $("#txtNewEditCategory").val("New");
            ClearCategoryData();
            $("#txtCategoryCode").prop("disabled", false);
            $("#ddlAccountTypeCategory").prop("disabled", false);
            $("#NewEntryAndModifyCategory").show();
            $("#CategoryList").hide();
            $("#CategoryFilter").hide();
        }

        //Click on Edit Image
        $('#CategoryTbl tbody tr td i.icon-pencil').live('click', function (e) {
            var row = $(this).closest("tr").get(0);
            var aData = oTableCategory.fnGetData(row);

            //$.setFormData("Config_CategoryMst", aData);
            GetCategoryDetails(aData["AccountType"], aData["CategoryCode"]);
            return false;
        });

        function GetCategoryDetails(AccountType, CategoryCode) {

            PrepareBasicObjects("ProfilerWebConfigTool", "GetCategoryDetails", "M", "S", "M001", "M003N");

            var database = $("#ddlDatabase").val();

            OpArgsObject["database"] = database;
            OpArgsObject["account_type"] = AccountType;
            OpArgsObject["CategoryCode"] = CategoryCode;

            AutoShowMessage = false;
            CallWebService(OnGetCategoryDetailsSuccess);
        }

        function OnGetCategoryDetailsSuccess() {

            if (ResultObject != null) {
                if (ResultObject.responseObjectInfo.Status == 1) {

                    $("#NewEntryAndModifyCategory").show();
                    $("#CategoryList").hide();
                    $("#CategoryFilter").hide();
                    var aData = ResultObject.responseObjectInfo.dt_ReturnedTables[0][0];
                    $.setFormData("Config_CategoryMst", aData);

                    $("#txtNewEditCategory").val("Edit");
                    $("#txtCategoryCode").prop("disabled", true);
                    $("#ddlAccountTypeCategory").prop("disabled", true);
                }
                else
                    ClearCategoryData();
            }
        }

        //Click on Delete Image
        $('#CategoryTbl tbody tr td i.icon-trash').live('click', function (e) {
            var row = $(this).closest("tr").get(0);
            var aData = oTableCategory.fnGetData(row);
            //$.setFormData("Config_CategoryMst", aData);
            var answer = confirm("Are Sure You Want to Delete Category : " + aData["CategoryDescription"] + " ?");
            if (answer) {
                GetCategoryDetailForDelete(aData["AccountType"], aData["CategoryCode"]);
            }
            return false;
        });

        function GetCategoryDetailForDelete(AccountType, CategoryCode) {

            PrepareBasicObjects("ProfilerWebConfigTool", "GetCategoryDetails", "M", "S", "M001", "M003N");

            var database = $("#ddlDatabase").val();

            OpArgsObject["database"] = database;
            OpArgsObject["account_type"] = AccountType;
            OpArgsObject["CategoryCode"] = CategoryCode;

            AutoShowMessage = false;
            CallWebService(OnGetCategoryDetailForDeleteSuccess);
        }

        function OnGetCategoryDetailForDeleteSuccess() {

            if (ResultObject != null) {
                if (ResultObject.responseObjectInfo.Status == 1) {

                    PrepareBasicObjects("ProfilerWebConfigTool", "SaveCategoryData", "M", "S", "M001", "M003N");

                    var listofrecords = new Array();
                    var form_header = ResultObject.responseObjectInfo.dt_ReturnedTables[0][0];
                    form_header["IsActive"] = "N";

                    listofrecords[0] = form_header;

                    NewUploadData["Config_CategoryMst"] = listofrecords;

                    OldUploadData = {};

                    UploadDataObject["New"] = NewUploadData;
                    UploadDataObject["Old"] = null;

                    $("#txtNewEditCategory").val("Edit");
                    var database = $("#ddlDatabase").val();
                    var NewEdit = $("#txtNewEditCategory").val();

                    OpArgsObject["database"] = database;
                    OpArgsObject["NewEdit"] = NewEdit;

                    AutoShowMessage = true;
                    CallWebService(OnSaveCategoryDataSuccess);
                }
            }
        }

        function PrepareUploadCategoryDataObject() {
            var listofrecords = new Array();
            var form_header = new Object();

            form_header = $.getFormData("Config_CategoryMst");

            listofrecords[0] = form_header;

            NewUploadData["Config_CategoryMst"] = listofrecords;

            OldUploadData = {};

            UploadDataObject["New"] = NewUploadData;
            UploadDataObject["Old"] = null;

            var database = $("#ddlDatabase").val();
            var NewEdit = $("#txtNewEditCategory").val();

            OpArgsObject["database"] = database;
            OpArgsObject["NewEdit"] = NewEdit;
        }

        function SaveCategoryData() {

            if ($("#txtCategoryCode").val() == null || $("#txtCategoryCode").val() == "") {
                alert("Please enter Category Code.");
                $("#txtCategoryCode").focus();
                return false;
            }
            else if ($("#txtCategoryDescription").val() == null || $("#txtCategoryDescription").val() == "") {
                alert("Please enter Category Description.");
                $("#txtCategoryDescription").focus();
                return false;
            }
            else if ($("#ddlAccountTypeCategory").val() == null || $("#ddlAccountTypeCategory").val() == "") {
                alert("Please select Account Type.");
                $("#ddlAccountTypeCategory").focus();
                return false;
            }
            else if ($("#txtCategorySeqNo").val() == null || $("#txtCategorySeqNo").val() == "") {
                alert("Please enter Category SeqNo.");
                $("#txtCategorySeqNo").focus();
                return false;
            }

            var answer = confirm("Are Sure You Want To Save Data?");
            if (answer) {

                PrepareBasicObjects("ProfilerWebConfigTool", "SaveCategoryData", "M", "S", "M001", "M003N");

                PrepareUploadCategoryDataObject();

                AutoShowMessage = true;
                CallWebService(OnSaveCategoryDataSuccess);
            }
        }

        function OnSaveCategoryDataSuccess() {

            if (ResultObject != null) {
                if (ResultObject.responseObjectInfo.Status == 1) {

                    ClearCategoryData();

                    $("#NewEntryAndModifyCategory").hide();
                    $("#CategoryList").show();
                    $("#CategoryFilter").show();
                    $("#ddlAccountTypeCategoryFilter").val("");

                    if (ResultObject.responseObjectInfo.dt_ReturnedTables[0] != null)
                        DisplayCategoryData(ResultObject.responseObjectInfo.dt_ReturnedTables[0]);
                }
            }
        }

        function BackCategoryData() {

            $("#NewEntryAndModifyCategory").hide();
            $("#CategoryList").show();
            $("#CategoryFilter").show();
        }

        function ClearCategoryData() {

            if ($("#txtNewEditCategory").val() == "New") {

                $("#txtCategoryCode").val("");
                $("#ddlAccountTypeCategory").val("");
            }

            $("#txtCategoryDescription").val("");
            $("#txtCategorySeqNo").val("");
            $("#txtFileNameCategory").val("");
            $("#txtImageURLCategory").val("");
            $("#chkIsActiveCategory").val("Y");
            $("#chkAllowAdd").val("N");
        }

        function GetCategoryList() {

            $("#ddlCategoryCode").empty();

            PrepareBasicObjects("ProfilerWebConfigTool", "GetCategoryDetails", "M", "S", "M001", "M003N");

            var database = $("#ddlDatabase").val();
            var AccountType = $("#ddlAccountTypeAttribute").val();

            OpArgsObject["database"] = database;
            OpArgsObject["account_type"] = AccountType;

            AutoShowMessage = false;
            CallWebService(OnGetCategoryListSuccess);
        }

        function OnGetCategoryListSuccess() {

            if (ResultObject != null) {
                if (ResultObject.responseObjectInfo.Status == 1) {

                    BindDropDown(ResultObject.responseObjectInfo.dt_ReturnedTables[0], "ddlCategoryCode", "CategoryCode", "CategoryDescription", FirstIndexAS.Select);
                }
                else
                    $("#ddlCategoryCode").empty();
            }
        }

        function GetFilterAttribute(ddl) {

            PrepareBasicObjects("ProfilerWebConfigTool", "GetAttributeDetails", "M", "S", "M001", "M003N");

            if (ddl != null && ddl == "A")
                $("#ddlCategoryAttributeFilter").empty();

            var database = $("#ddlDatabase").val();
            var AccountType = $("#ddlAccountTypeAttributeFilter").val();
            var CategoryCode = $("#ddlCategoryAttributeFilter").val();

            OpArgsObject["database"] = database;
            OpArgsObject["account_type"] = AccountType;
            OpArgsObject["CategoryCode"] = CategoryCode;

            AutoShowMessage = false;
            CallWebService(OnGetFilterAttributeSuccess);
        }

        function OnGetFilterAttributeSuccess() {

            if (ResultObject != null) {
                if (ResultObject.responseObjectInfo.Status == 1) {

                    if (ResultObject.responseObjectInfo.dt_ReturnedTables[0] != null)
                        DisplayAttributeData(ResultObject.responseObjectInfo.dt_ReturnedTables[0]);

                    if (ResultObject.responseObjectInfo.dt_ReturnedTables[1] != null) {

                        var CategoryCode = $("#ddlCategoryAttributeFilter").val();
                        BindDropDown(ResultObject.responseObjectInfo.dt_ReturnedTables[1], "ddlCategoryAttributeFilter", "CategoryCode", "CategoryDescription", FirstIndexAS.ALL);
                        if (CategoryCode != null)
                            $("#ddlCategoryAttributeFilter").val(CategoryCode);
                    }
                }
            }
        }

        function AddAttribute() {

            $("#txtNewEditAttribute").val("New");
            ClearAttributeData();
            $("#ddlAccountTypeAttribute").prop("disabled", false);
            $("#ddlCategoryCode").prop("disabled", false);
            $("#txtAttributeCode").prop("disabled", false);
            $("#NewEntryAndModifyAttribute").show();
            $("#AttributeList").hide();
            $("#FilterAttribute").hide();
        }

        //Click on Edit Image
        $('#AttributeTbl tbody tr td i.icon-pencil').live('click', function (e) {
            var row = $(this).closest("tr").get(0);
            var aData = oTableAttribute.fnGetData(row);

            //$.setFormData("Config_AttributeCategoryMap", aData);
            GetAttributeDetails(aData["AccountType"], aData["CategoryCode"], aData["AttributeCode"]);
            return false;
        });

        function GetAttributeDetails(AccountType, CategoryCode, AttributeCode) {
            debugger;
            PrepareBasicObjects("ProfilerWebConfigTool", "GetAttributeDetails", "M", "S", "M001", "M003N");

            var database = $("#ddlDatabase").val();

            OpArgsObject["database"] = database;
            OpArgsObject["account_type"] = AccountType;
            OpArgsObject["CategoryCode"] = CategoryCode;
            OpArgsObject["AttributeCode"] = AttributeCode;

            AutoShowMessage = false;
            CallWebService(OnGetAttributeDetailsSuccess);
        }

        function OnGetAttributeDetailsSuccess() {
            debugger;
            if (ResultObject != null) {
                if (ResultObject.responseObjectInfo.Status == 1) {

                    $("#ddlCategoryCode").empty();
                    if (ResultObject.responseObjectInfo.dt_ReturnedTables[1] != null)
                        BindDropDown(ResultObject.responseObjectInfo.dt_ReturnedTables[1], "ddlCategoryCode", "CategoryCode", "CategoryDescription", FirstIndexAS.Select);

                    $("#NewEntryAndModifyAttribute").show();
                    $("#AttributeList").hide();
                    $("#FilterAttribute").hide();
                    var aData = ResultObject.responseObjectInfo.dt_ReturnedTables[0][0];
                    $.setFormData("Config_AttributeCategoryMap", aData);

                    $("#ddlDisplayType").trigger("change");
                    document.getElementById('ddlEditType').value = aData["EditType"];

                    $("#txtNewEditAttribute").val("Edit");
                    $("#ddlAccountTypeAttribute").prop("disabled", true);
                    $("#ddlCategoryCode").prop("disabled", true);
                    $("#txtAttributeCode").prop("disabled", true);
                }
                else
                    ClearAttributeData();
            }
        }

        //Click on Delete Image
        $('#AttributeTbl tbody tr td i.icon-trash').live('click', function (e) {
            var row = $(this).closest("tr").get(0);
            var aData = oTableAttribute.fnGetData(row);
            //$.setFormData("Config_AttributeCategoryMap", aData);
            var answer = confirm("Are Sure You Want to Delete Attribute : " + aData["AttributeDescription"] + " ?");
            if (answer) {
                GetAttributeDetailForDelete(aData["AccountType"], aData["CategoryCode"], aData["AttributeCode"]);
            }
            return false;
        });

        function GetAttributeDetailForDelete(AccountType, CategoryCode, AttributeCode) {

            PrepareBasicObjects("ProfilerWebConfigTool", "GetAttributeDetails", "M", "S", "M001", "M003N");

            var database = $("#ddlDatabase").val();

            OpArgsObject["database"] = database;
            OpArgsObject["account_type"] = AccountType;
            OpArgsObject["CategoryCode"] = CategoryCode;
            OpArgsObject["AttributeCode"] = AttributeCode;

            AutoShowMessage = false;
            CallWebService(OnGetAttributeDetailForDeleteSuccess);
        }

        function OnGetAttributeDetailForDeleteSuccess() {

            if (ResultObject != null) {
                if (ResultObject.responseObjectInfo.Status == 1) {

                    PrepareBasicObjects("ProfilerWebConfigTool", "SaveAttributeData", "M", "S", "M001", "M003N");

                    var listofrecords = new Array();
                    var form_header = ResultObject.responseObjectInfo.dt_ReturnedTables[0][0];
                    form_header["IsActive"] = "N";

                    listofrecords[0] = form_header;

                    NewUploadData["Config_AttributeCategoryMap"] = listofrecords;

                    OldUploadData = {};

                    UploadDataObject["New"] = NewUploadData;
                    UploadDataObject["Old"] = null;

                    $("#txtNewEditAttribute").val("Edit");
                    var database = $("#ddlDatabase").val();
                    var NewEdit = $("#txtNewEditAttribute").val();

                    OpArgsObject["database"] = database;
                    OpArgsObject["NewEdit"] = NewEdit;

                    AutoShowMessage = true;
                    CallWebService(OnSaveAttributeDataSuccess);
                }
            }
        }

        function PrepareUploadAttributeDataObject() {
            var listofrecords = new Array();
            var form_header = new Object();

            form_header = $.getFormData("Config_AttributeCategoryMap");

            listofrecords[0] = form_header;

            NewUploadData["Config_AttributeCategoryMap"] = listofrecords;

            OldUploadData = {};

            UploadDataObject["New"] = NewUploadData;
            UploadDataObject["Old"] = null;

            var database = $("#ddlDatabase").val();
            var NewEdit = $("#txtNewEditAttribute").val();

            OpArgsObject["database"] = database;
            OpArgsObject["NewEdit"] = NewEdit;
        }

        function SaveAttributeData() {

            if ($("#txtAttributeCode").val() == null || $("#txtAttributeCode").val() == "") {
                alert("Please enter Attribute Code.");
                $("#txtAttributeCode").focus();
                return false;
            }
            else if ($("#txtAttributeDescription").val() == null || $("#txtAttributeDescription").val() == "") {
                alert("Please enter Attribute Description.");
                $("#txtAttributeDescription").focus();
                return false;
            }
            else if ($("#ddlAccountTypeAttribute").val() == null || $("#ddlAccountTypeAttribute").val() == "") {
                alert("Please select Account Type.");
                $("#ddlAccountTypeAttribute").focus();
                return false;
            }
            else if ($("#ddlCategoryCode").val() == null || $("#ddlCategoryCode").val() == "") {
                alert("Please select Category.");
                $("#ddlCategoryCode").focus();
                return false;
            }
            else if ($("#txtAttributeSeqNo").val() == null || $("#txtAttributeSeqNo").val() == "") {
                alert("Please enter Attribute SeqNo.");
                $("#txtAttributeSeqNo").focus();
                return false;
            }

            var answer = confirm("Are Sure You Want To Save Data?");
            if (answer) {

                PrepareBasicObjects("ProfilerWebConfigTool", "SaveAttributeData", "M", "S", "M001", "M003N");

                PrepareUploadAttributeDataObject();

                AutoShowMessage = true;
                CallWebService(OnSaveAttributeDataSuccess);
            }
        }

        function OnSaveAttributeDataSuccess() {

            if (ResultObject != null) {
                if (ResultObject.responseObjectInfo.Status == 1) {

                    ClearCategoryData();

                    $("#NewEntryAndModifyAttribute").hide();
                    $("#AttributeList").show();
                    $("#FilterAttribute").show();
                    $("#ddlAccountTypeAttributeFilter").val("");
                    $("#ddlCategoryAttributeFilter").val("");

                    if (ResultObject.responseObjectInfo.dt_ReturnedTables[0] != null)
                        DisplayAttributeData(ResultObject.responseObjectInfo.dt_ReturnedTables[0]);
                }
            }
        }

        function BackAttributeData() {

            $("#NewEntryAndModifyAttribute").hide();
            $("#AttributeList").show();
            $("#FilterAttribute").show();
        }

        function ClearAttributeData() {

            if ($("#txtNewEditAttribute").val() == "New") {

                $("#txtAttributeCode").val("");
                $("#ddlAccountTypeAttribute").val("");
                $("#ddlCategoryCode").val("");
            }

            $("#txtAttributeDescription").val("");
            $("#txtAttributeSeqNo").val("");
            $("#chkIsActiveAttribute").val("Y");
            $("#chkShowWhenEmpty").val("N");
            $("#chkShowInFilter").val("N");
            $("#txtTableNameAttribute").val("");
            $("#txtIdColumn").val("");
            $("#txtDistinctIdColumn").val("");
            $("#txtColumnName").val("");
            $("#chkIsEditable").val("N");
            $("#txtEditType").val("");
            $("#txtEditOptions").val("");
            $("#txtDisplayType").val("");
            $("#chkIsAddressAttribute").val("N");
            $("#txtImageLink").val("");
            $("#chkIsNumeric").val("N");
            $("#chkIsPhoneNumber").val("N");
            $("#txtSeperator").val("");
            $("#txtFormat").val("");
            $("#txtNoOfLines").val("");
            $("#txtColorValue").val("");
            $("#chkConditionalAttributeReq").val("N");
            $("#txtConditionalAttribute").val("");
        }

        function ClearData() {

            ClearCategoryData();
            ClearAttributeData();
        }

        //17-09-2016 cascade dropdown edit and display type
        jQuery(function ($) {
            var EditType = {
                'K': ['T-Text', 'D-Dropdown', 'R-Color Indicator', 'I-Image', 'P-Datetime picker', 'C-Color Text', 'S-Sparkline Graph'],
                'Q': ['D-Dropdown', 'P-Datetime picker', 'T-Text'],
                'I': ['I-Image'],
                'B': [],
                'D': [],
                'G': [],
                'T': [],
                'M': [],
                'L': []
            }

            var $EditType = $('#ddlEditType');

            $('#ddlDisplayType').change(function () {
                debugger;
                var DisplayType = $(this).val(), lcns = EditType[DisplayType] || [];

                var defaultData = '<option value="0">---Select---</option>';
                var html = $.map(lcns, function (lcn) {
                    var splitString = lcn.split('-');
                    return '<option value="' + splitString[0] + '">' + lcn + '</option>'
                }).join('');



                $EditType.html(defaultData + html)
            });
        });


        $(function () {
            $(":file").change(function () {
                debugger;
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded;
                    reader.readAsDataURL(this.files[0]);

                    var data = saveFileOrImage("ImgUpload", "AttributeImageUpload.ashx");
                    //alert("data saved successfully on " + data);
                }
            });
        });

        function imageIsLoaded(e) {
            $('#myImg').attr('src', e.target.result);
        };

        function saveFileOrImage(fileUploadId, HandlerName) {
            debugger;
            var data = new FormData();
            var files = $("#" + fileUploadId).get(0).files;
            var saveMemPhoto = "";
            var flag = false;
            // Add the uploaded image content to the form data collection
            if (files.length > 0) {
                data.append("UploadedImage", files[0]);
                data.append("currentDateTime", $.now());
                var fileName = files[0]["name"];
                saveMemPhoto = $.now() + "___" + fileName;
                var Path = '<%=ConfigurationManager.AppSettings["AttachmentPath"].ToString() %>'
                document.getElementById("txtImageLinkAttribute").value = Path + "/AttributeImageUpload/" + saveMemPhoto;
                $("#uploadedUniPhoto").val(saveMemPhoto);
            }

            $.ajax({
                url: HandlerName,
                type: "POST",
                data: data,
                contentType: false,
                processData: false,
                async: false,
                success: function (result) {
                    debugger;
                    //alert(document.getElementById("hdnImagePath").value);
                },
                error: function (err) {
                    flag = false;
                    // alert(err.statusText);
                }
            });

        }

    </script>
    <div class="row-fluid">
        <div class="span12">
            <fieldset>
                <legend>Database</legend>
                <div id="Database" style="display: block;">
                    <table cellpadding="4" cellspacing="1">
                        <tr>
                            <td>
                                <table id="tblDataBaseAccountType" cellpadding="2" cellspacing="0">
                                    <tr>
                                        <td align="right">
                                            <span class="star">*</span><b>Database :</b>
                                        </td>
                                        <td align="left">
                                            <select id="ddlDatabase" onchange="javascript:return GetAccountTypeAndCategoryAndAttributeList();">
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="row-fluid">
        <div id="AccountTypeListContent" class="span12" style="display: none;">
            <fieldset>
                <legend>Account Type</legend>
                <div>
                    <div id="AccountTypeList" style="margin-top: 20px; overflow: auto;">
                        <table style="width: 100%" cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped"
                            id="AccountTypeTbl">
                            <thead>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                    <div id="NewEntryAndModifyAccountType" style="display: none;">
                        <table cellpadding="4" cellspacing="1">
                            <tr>
                                <td>
                                    <table id="tblAccountTypeDetail" cellpadding="2" cellspacing="0">
                                        <tr style="display: none;">
                                            <td align="right">
                                                <b>New/Edit :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtNewEditAccountType" data-field-name="NewEdit" maxlength="5"
                                                    disabled="disabled" class="Config_AccountTypeMst" />
                                            </td>
                                            <td align="right">
                                                <b>LastUpdatedDateTime :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtLastUpdatedDateTime" data-field-name="LastUpdatedDateTime"
                                                    maxlength="10" class="Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>AccountType :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAccountType" data-field-name="AccountType" maxlength="50"
                                                    disabled="disabled" class="Config_AccountTypeMst" />
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span><b>AccountTypeDescription :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAccountTypeDescription" data-field-name="AccountTypeDescription"
                                                    maxlength="255" class="Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>TableName :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtTableNameAccountType" data-field-name="TableName" maxlength="255"
                                                    class="Config_AccountTypeMst" />
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span><b>AccountIdColumn :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAccountIdColumn" data-field-name="AccountIdColumn" maxlength="255"
                                                    class="numeric Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <b>AccountParentIdColumn :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAccountParentIdColumn" data-field-name="AccountParentIdColumn"
                                                    maxlength="255" class="Config_AccountTypeMst" />
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span><b>AccountNameColumn :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAccountNameColumn" data-field-name="AccountNameColumn"
                                                    maxlength="255" class="Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>AccountTypeColumn :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAccountTypeColumn" data-field-name="AccountTypeColumn"
                                                    maxlength="255" class="Config_AccountTypeMst" />
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span><b>AccountAddressColumn :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAccountAddressColumn" data-field-name="AccountAddressColumn"
                                                    maxlength="255" class="Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>AccountCityStateZipColumn :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAccountCityStateZipColumn" data-field-name="AccountCityStateZipColumn"
                                                    maxlength="255" class="Config_AccountTypeMst" />
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span><b>LatitudeColumn :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtLatitudeColumn" data-field-name="LatitudeColumn" maxlength="255"
                                                    class="Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>LongitudeColumn :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtLongitudeColumn" data-field-name="LongitudeColumn" maxlength="255"
                                                    class="Config_AccountTypeMst" />
                                            </td>
                                            <td align="right">
                                                <b>NoOfRecord :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtNoOfRecord" data-field-name="NoOfRecord" maxlength="6"
                                                    class="numeric Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <b>Active :</b>
                                            </td>
                                            <td align="left">
                                                <select id="chkIsActiveAccountType" data-field-name="IsActive" class="Config_AccountTypeMst">
                                                    <option value="N">No</option>
                                                    <option value="Y" selected="true">Yes</option>
                                                </select>
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span><b>SeqNo :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtSeqNo" data-field-name="SeqNo" maxlength="4" class="numeric Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <b>FileName :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtFileNameAccountType" data-field-name="FileName" maxlength="255"
                                                    class="Config_AccountTypeMst" />
                                            </td>
                                            <td align="right">
                                                <b>CallType :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtCallType" data-field-name="CallType" maxlength="1" class="Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <b>ImageURL :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtImageURLAccountType" data-field-name="ImageURL" maxlength="255"
                                                    class="Config_AccountTypeMst" />
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span><b>AccountListDependOn :</b>
                                            </td>
                                            <td align="left">
                                                <select id="chkAccountListDependOn" data-field-name="AccountListDependOn" class="Config_AccountTypeMst">
                                                    <option value="">None</option>
                                                    <option value="Territory">Territory</option>
                                                    <option value="Zip" selected="true">Zip</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <b>AddressMstTableName :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAddressMstTableName" data-field-name="AddressMstTableName"
                                                    maxlength="50" class="Config_AccountTypeMst" />
                                            </td>
                                            <td align="right">
                                                <b>AddressMstIdColumn :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAddressMstIdColumn" data-field-name="AddressMstIdColumn"
                                                    maxlength="50" class="Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <b>PageFooterView :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtPageFooterView" data-field-name="PageFooterView" maxlength="10"
                                                    class="Config_AccountTypeMst" />
                                            </td>
                                            <td align="right">
                                                <b>AccountSelectionCriterion :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAccountSelectionCriterion" data-field-name="AccountSelectionCriterion"
                                                    maxlength="255" class="Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <b>Rights to Account Type:</b>
                                            </td>
                                            <td align="left">
                                                <select id="example-multiple-selected" multiple="multiple">
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4">
                                                <input type="button" id="btnSaveAccountType" value="Save" onclick="javascript:return SaveAccountTypeData();"
                                                    style="width: 90px; font-weight: bold;" class="btn" />
                                                <input type="button" id="btnClearAccountType" value="Clear" onclick="javascript:return ClearAccountTypeData();"
                                                    style="width: 90px; font-weight: bold;" class="btn" />
                                                <input type="button" id="btnBackAccountType" value="Back" onclick="javascript:return BackAccountTypeData();"
                                                    style="width: 90px; font-weight: bold;" class="btn" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="row-fluid">
        <div id="CategoryListContent" class="span12" style="display: none;">
            <fieldset>
                <legend>Category</legend>
                <div>
                    <div id="CategoryFilter">
                        <table cellpadding="2" cellspacing="0">
                            <tr>
                                <td align="right">
                                    <b>Filter By AccountType :</b>
                                </td>
                                <td align="left">
                                    <select id="ddlAccountTypeCategoryFilter" onchange="javascript:return GetFilterCategory();">
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="CategoryList" style="margin-top: 20px; overflow: auto;">
                        <table style="width: 100%" cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped"
                            id="CategoryTbl">
                            <thead>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                    <div id="NewEntryAndModifyCategory" style="display: none;">
                        <table cellpadding="4" cellspacing="1">
                            <tr>
                                <td>
                                    <table id="tblCategoryDetail" cellpadding="2" cellspacing="0">
                                        <tr style="display: none;">
                                            <td align="right">
                                                <b>New/Edit :</b>
                                            </td>
                                            <td align="left" colspan="3">
                                                <input type="text" id="txtNewEditCategory" data-field-name="NewEdit" maxlength="5"
                                                    disabled="disabled" class="Config_CategoryMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>AccountType :</b>
                                            </td>
                                            <td align="left">
                                                <select id="ddlAccountTypeCategory" data-field-name="AccountType" class="Config_CategoryMst">
                                                </select>
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span><b>CategoryCode :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtCategoryCode" data-field-name="CategoryCode" maxlength="100"
                                                    disabled="disabled" class="Config_CategoryMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>CategoryDescription :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtCategoryDescription" data-field-name="CategoryDescription"
                                                    maxlength="255" class="Config_CategoryMst" />
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span><b>CategorySeqNo :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtCategorySeqNo" data-field-name="CategorySeqNo" maxlength="4"
                                                    class="numeric Config_CategoryMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <b>Active :</b>
                                            </td>
                                            <td align="left">
                                                <select id="chkIsActiveCategory" data-field-name="IsActive" class="Config_CategoryMst">
                                                    <option value="N">No</option>
                                                    <option value="Y" selected="true">Yes</option>
                                                </select>
                                            </td>
                                            <td align="right">
                                                <b>FileName :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtFileNameCategory" data-field-name="FileName" maxlength="255"
                                                    class="Config_CategoryMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <b>Image URL :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtImageURLCategory" data-field-name="ImageURL" maxlength="255"
                                                    class="Config_CategoryMst" />
                                            </td>
                                            <td align="right">
                                                <b>Allow Add :</b>
                                            </td>
                                            <td align="left">
                                                <select id="chkAllowAdd" data-field-name="AllowAdd" class="Config_CategoryMst">
                                                    <option value="N" selected="true">No</option>
                                                    <option value="Y">Yes</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4">
                                                <input type="button" id="btnSaveCategory" value="Save" onclick="javascript:return SaveCategoryData();"
                                                    style="width: 90px; font-weight: bold;" class="btn" />
                                                <input type="button" id="btnClearCategory" value="Clear" onclick="javascript:return ClearCategoryData();"
                                                    style="width: 90px; font-weight: bold;" class="btn" />
                                                <input type="button" id="btnBackCategory" value="Back" onclick="javascript:return BackCategoryData();"
                                                    style="width: 90px; font-weight: bold;" class="btn" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="row-fluid">
        <div id="AttributeListContent" class="span12" style="display: none;">
            <fieldset>
                <legend>Attribute</legend>
                <div>
                    <div id="FilterAttribute">
                        <table cellpadding="2" cellspacing="0">
                            <tr>
                                <td align="right">
                                    <b>Filter By AccountType :</b>
                                </td>
                                <td align="left">
                                    <select id="ddlAccountTypeAttributeFilter" onchange="javascript:return GetFilterAttribute('A');">
                                    </select>
                                </td>
                                <td align="right">
                                    <b>Category :</b>
                                </td>
                                <td align="left">
                                    <select id="ddlCategoryAttributeFilter" onchange="javascript:return GetFilterAttribute('C');">
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="AttributeList" style="margin-top: 20px; overflow: auto;">
                        <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped"
                            id="AttributeTbl">
                            <thead>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                    <div id="NewEntryAndModifyAttribute" style="display: none;">
                        <table cellpadding="4" cellspacing="1">
                            <tr>
                                <td>
                                    <table id="Table1" cellpadding="2" cellspacing="0">
                                        <tr style="display: none;">
                                            <td align="right">
                                                <b>New/Edit :</b>
                                            </td>
                                            <td align="left" colspan="3">
                                                <input type="text" id="txtNewEditAttribute" data-field-name="NewEdit" maxlength="5"
                                                    disabled="disabled" class="Config_AttributeCategoryMap" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>AccountType :</b>
                                            </td>
                                            <td align="left">
                                                <select id="ddlAccountTypeAttribute" onchange="javascript:return GetCategoryList();"
                                                    data-field-name="AccountType" class="Config_AttributeCategoryMap">
                                                </select>
                                            </td>
                                            <td align="right">
                                                <b>DisplayType :</b>
                                            </td>
                                            <td align="left">
                                                <%--<input type="text" id="txtDisplayType" data-field-name="DisplayType" maxlength="1"
                                                    class="Config_AttributeCategoryMap" />--%>
                                                <select id="ddlDisplayType" data-field-name="DisplayType" class="Config_AttributeCategoryMap">
                                                    <option value="K" selected="true">K-Key Value</option>
                                                    <option value="Q">Q-Question</option>
                                                    <option value="I">I-Image</option>
                                                    <option value="B">B-Bubble</option>
                                                    <option value="D">D-Dropdown</option>
                                                    <option value="G">G-Graphs</option>
                                                    <option value="T">T-Tables</option>
                                                    <option value="M">M-Maps</option>
                                                    <option value="L">L-List Bullets</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>CategoryCode :</b>
                                            </td>
                                            <td align="left">
                                                <select id="ddlCategoryCode" data-field-name="CategoryCode" class="Config_AttributeCategoryMap">
                                                </select>
                                            </td>
                                            <td align="right">
                                                <b>EditType :</b>
                                            </td>
                                            <td align="left">
                                                <%--<input type="text" id="txtEditType" data-field-name="EditType" maxlength="1" class="Config_AttributeCategoryMap" />--%>
                                                <select id="ddlEditType" data-field-name="EditType" class="Config_AttributeCategoryMap">
                                                    <option value="0" selected="true">---Select---</option>
                                                    <%-- <option value="T" selected="true">T-Text</option>
                                                    <option value="D">D-Dropdown</option>
                                                    <option value="R">R-Color indicator</option>
                                                    <option value="I">I-Image</option>
                                                    <option value="P">P-Datetime picker</option>
                                                    <option value="C">C-Colored Text</option>
                                                    <option value="S">S-Sparkline Graph</option>--%>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>AttributeCode :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAttributeCode" data-field-name="AttributeCode" maxlength="100"
                                                    disabled="disabled" class="Config_AttributeCategoryMap" />
                                            </td>
                                            <td align="right">
                                                <b>EditOptions :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtEditOptions" data-field-name="EditOptions" maxlength="1024"
                                                    class="Config_AttributeCategoryMap" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>AttributeDescription :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAttributeDescription" data-field-name="AttributeDescription"
                                                    maxlength="255" class="Config_AttributeCategoryMap" />
                                            </td>
                                            <td align="right">
                                                <b>IsAddressAttribute :</b>
                                            </td>
                                            <td align="left">
                                                <select id="chkIsAddressAttribute" data-field-name="IsAddressAttribute" class="Config_AttributeCategoryMap">
                                                    <option value="N" selected="true">No</option>
                                                    <option value="Y">Yes</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>AttributeSeqNo :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAttributeSeqNo" data-field-name="AttributeSeqNo" maxlength="4"
                                                    class="numeric Config_AttributeCategoryMap" />
                                            </td>
                                            <td align="right">
                                                <b>ImageLink :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtImageLinkAttribute" data-field-name="ImageLink" maxlength="255"
                                                    class="Config_AttributeCategoryMap" style="display: none;" />
                                                <input type='file' id='ImgUpload' />
                                                <img id="myImg" src="#" alt="your image" height="100px" width="100px" src="ImageLink"
                                                    class="Config_AttributeCategoryMap" />
                                                <%--<input type="hidden" id="hdnImagePath" data-field-name="ImageLink" class="Config_AttributeCategoryMap" />--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <b>ShowWhenEmpty :</b>
                                            </td>
                                            <td align="left">
                                                <select id="chkShowWhenEmpty" data-field-name="ShowWhenEmpty" class="Config_AttributeCategoryMap">
                                                    <option value="N" selected="true">No</option>
                                                    <option value="Y">Yes</option>
                                                </select>
                                            </td>
                                            <td align="right">
                                                <b>IsNumeric :</b>
                                            </td>
                                            <td align="left">
                                                <select id="chkIsNumeric" data-field-name="IsNumeric" class="Config_AttributeCategoryMap">
                                                    <option value="N" selected="true">No</option>
                                                    <option value="Y">Yes</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <b>ShowInFilter :</b>
                                            </td>
                                            <td align="left">
                                                <select id="chkShowInFilter" data-field-name="ShowInFilter" class="Config_AttributeCategoryMap">
                                                    <option value="N" selected="true">No</option>
                                                    <option value="Y">Yes</option>
                                                </select>
                                            </td>
                                            <td align="right">
                                                <b>IsPhoneNumber :</b>
                                            </td>
                                            <td align="left">
                                                <select id="chkIsPhoneNumber" data-field-name="IsPhoneNumber" class="Config_AttributeCategoryMap">
                                                    <option value="N" selected="true">No</option>
                                                    <option value="Y">Yes</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <b>Active :</b>
                                            </td>
                                            <td align="left">
                                                <select id="chkIsActiveAttribute" data-field-name="IsActive" class="Config_AttributeCategoryMap">
                                                    <option value="N">No</option>
                                                    <option value="Y" selected="true">Yes</option>
                                                </select>
                                            </td>
                                            <td align="right">
                                                <b>Seperator :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtSeperator" data-field-name="Seperator" maxlength="10" class="Config_AttributeCategoryMap" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <b>TableName :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtTableNameAttribute" data-field-name="TableName" maxlength="255"
                                                    class="Config_AttributeCategoryMap" />
                                            </td>
                                            <td align="right">
                                                <b>Format :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtFormat" data-field-name="Format" maxlength="255" class="Config_AttributeCategoryMap" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <b>IdColumn :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtIdColumn" data-field-name="IdColumn" maxlength="255" class="Config_AttributeCategoryMap" />
                                            </td>
                                            <td align="right">
                                                <b>NoOfLines :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtNoOfLines" data-field-name="NoOfLines" maxlength="10" class="Config_AttributeCategoryMap" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <b>DistinctIdColumn :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtDistinctIdColumn" data-field-name="DistinctIdColumn" maxlength="255"
                                                    class="Config_AttributeCategoryMap" />
                                            </td>
                                            <td align="right">
                                                <b>ColorValue :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtColorValue" data-field-name="ColorValue" maxlength="15"
                                                    class="jscolor Config_AttributeCategoryMap" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <b>ColumnName :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtColumnName" data-field-name="ColumnName" maxlength="255"
                                                    class="Config_AttributeCategoryMap" />
                                            </td>
                                            <td align="right">
                                                <b>ConditionalAttributeReq :</b>
                                            </td>
                                            <td align="left">
                                                <select id="chkConditionalAttributeReq" data-field-name="ConditionalAttributeReq"
                                                    class="Config_AttributeCategoryMap">
                                                    <option value="N" selected="true">No</option>
                                                    <option value="Y">Yes</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <b>IsEditable :</b>
                                            </td>
                                            <td align="left">
                                                <select id="chkIsEditable" data-field-name="IsEditable" class="Config_AttributeCategoryMap">
                                                    <option value="N" selected="true">No</option>
                                                    <option value="Y">Yes</option>
                                                </select>
                                            </td>
                                            <td align="right">
                                                <b>ConditionalAttribute :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtConditionalAttribute" data-field-name="ConditionalAttribute"
                                                    maxlength="255" class="Config_AttributeCategoryMap" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4">
                                                <input type="button" id="btnSaveAttribute" value="Save" onclick="javascript:return SaveAttributeData();"
                                                    style="width: 90px; font-weight: bold;" class="btn" />
                                                <input type="button" id="btnClearAttribute" value="Clear" onclick="javascript:return ClearAttributeData();"
                                                    style="width: 90px; font-weight: bold;" class="btn" />
                                                <input type="button" id="btnBackAttribute" value="Back" onclick="javascript:return BackAttributeData();"
                                                    style="width: 90px; font-weight: bold;" class="btn" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</asp:Content>
