﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Home" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Home</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
    <link href="CSS%20Styles/coolMenu.css" rel="stylesheet" type="text/css" />
    <script src="ui/CookiesCreate.js" type="text/javascript"></script>
    <script src="Menu/jquery.js" type="text/javascript"></script>
    <link href="Menu/menu.css" rel="stylesheet" type="text/css" />
    <script src="Menu/menu.js" type="text/javascript"></script>
    <style type="text/css">
        div#menu
        {
            width: 98%;
            float: left;
            margin-left: 0.8%;
        }
        div#copyright
        {
            margin: 0 auto;
            width: 80%;
            font: 11px 'Trebuchet MS';
            color: #124a6f;
            text-indent: 20px;
            padding: 40px 0 0 0;
        }
        div#copyright a
        {
            color: #4682b4;
        }
        div#copyright a:hover
        {
            color: #124a6f;
        }
        .header
        {
            width: 100%;
            height: 100px;
            float: left; /*background: url(images/header.png) no-repeat;*/
        }
        .clogo
        {
            width: 100%;
            height: 86px;
            float: left;
            margin-top: 0.7%;
        }
        img, object
        {
            max-width: 100%;
        }
        object
        {
            margin: 0 auto 1em;
            max-width: 100%;
        }
        .headermidd
        {
            width: auto;
            float: left;
            font-size: 20px;
            color: #448fb9;
            padding: 35px;
        }
        .headerleft
        {
            height: 100px;
            float: left;
            width: 1.2%;
            background: url(Images/leftheader.png) no-repeat;
            z-index: 2;
        }
        .headermiddle
        {
            height: 100px;
            float: left;
            width: 97%;
            background: url(Images/header.png) repeat-x;
        }
        .headerright
        {
            height: 100px;
            float: left;
            width: 1.8%;
            background: url(Images/rigthheader.png) no-repeat;
            z-index: 2;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function CheckCookie() {
            var cookieName = getCookie("LoginUser");
            if (cookieName == null) {
                window.location.href = "Login.aspx";
            }
            //            var groupType = getCookie("GroupType");
            //            if (groupType != null && groupType != "") {
            //                GeneralService.GetGroupRights(groupType, SetMenuData);
            //            }
        }
        function SetMenuData(result) {
            //document.getElementById("menu").innerHTML = result;
        }
        function EraseCookieOnLogout() {
            eraseCookie("LoginUser");
            window.location.href = "Login.aspx";
        }
    </script>
</head>
<body onload="CheckCookie();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        <Services>
            <asp:ServiceReference Path="~/DeGrToolWebService.asmx" />
        </Services>
    </asp:ScriptManager>
    <div class="header">
        <div class="headerleft">
        </div>
        <div class="headermiddle">
            <div class="clogo">
                <img src="Images/Banner.png" alt="" width="100%" height="100%" />
            </div>
            <%--    <div id="DivHeaderMain" class="headermidd">
            </div>--%>
        </div>
        <div class="headerright">
        </div>
    </div>
    <div id="menu" runat="server">
        
    </div>
    </form>
</body>
</html>
