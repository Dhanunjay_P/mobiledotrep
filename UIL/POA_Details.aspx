﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="POA_Details.aspx.cs" Inherits="POA_Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderTitle" runat="Server">
    POA Details
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="ClientScripts/dist/css/bootstrap-multiselect.css" type="text/css">
    <script type="text/javascript" src="ClientScripts/dist/js/bootstrap-multiselect.js"></script>
    <style type="text/css">
        ul.multiselect-container.dropdown-menu
        {
            height: 500px;
            overflow-y: auto;
        }
    </style>
    <script src="Scripts/jscolor.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var OriginalData = new Object();
        var NewUploadData = new Object();
        var OldUploadData = new Object();
        var oTableAccountType = null;
        var oTableCategory = null;
        var oTableAttribute = null;
        var validator;

        function pageLoad() {

            //----- Restrict input field to Decimal ----- 
            $(".numeric").keypress(function (e) {

                //if the letter is not digit then display error and don't type anything
                if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
                else if (e.which == 46 && $(this).val().indexOf('.') != -1) {
                    e.preventDefault();
                } // prevent if already dot
            });

            //----- Restrict Cut, Copy and Paste to Decimal ----- 
            $('.numeric').bind("cut copy paste", function (e) {
                e.preventDefault();
            });

            //SessionData = JSON.parse(document.getElementById("ctl00_ContentPlaceHolder1_hidSession").value);

            $("#ddlAccountType").empty();
            $("#ddlAccountTypeCategory").empty();
            $("#ddlAccountTypeAttribute").empty();

            PrepareBasicObjects("ProfilerWebConfigTool", "GetInitialData", "M", "S", "M001", "M003N");

            AutoShowMessage = false;
            CallWebService(OnPageLoadSuccess);
        }

        function OnPageLoadSuccess() {

            if (ResultObject != null) {
                if (ResultObject.responseObjectInfo.Status == 1) {

                    BindDropDown(ResultObject.responseObjectInfo.dt_ReturnedTables[0], "ddlDatabase", "code", "desc", FirstIndexAS.Select);
                }
                else
                    $("#ddlDatabase").empty();
            }
        }


        function DisplayAccountTypeData(data) {

            try {
                if (oTableAccountType != null) {
                    oTableAccountType.fnDestroy();
                    $("#AccountTypeList").html(' <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped" id="Table1"><thead></thead><tbody> </tbody><tfoot> </tfoot></table>');
                }
                oTableAccountType = $("#AccountTypeTbl").dataTable({
                    "iDisplayLength": 25,
                    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                    "aaData": data,
                    "aoColumns": [
                            {
                                "sTitle": '<div style="width:40px;text-align:center;margin-bottom:2px;"><i class="icon-plus-sign" title="Add AccountType" id="btnAddAccountType" onclick="AddAccountType()" data-mode="ADD"></i></div>',
                                "mData": null,
                                "sClass": "center",
                                "sWidth": "20px",
                                "sDefaultContent": '<i id="imgEditCategory" title="Edit AccountType" class="icon-pencil"></i>&nbsp;<i id="imgDeleteAccountType" title="Delete AccountType" class="icon-trash"></i>',
                                "bSearchable": false,
                                "bSortable": false
                            },
                            { "sTitle": "AccountType", "mData": "AccountType", "sClass": "left", "sWidth": "80px" },
                            { "sTitle": "AccountTypeDescription", "mData": "AccountTypeDescription", "sClass": "left", "sWidth": "150px" },
                            { "sTitle": "TableName", "mData": "TableName", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "AccountIdColumn", "mData": "AccountIdColumn", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "AccountParentIdColumn", "mData": "AccountParentIdColumn", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "AccountNameColumn", "mData": "AccountNameColumn", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "AccountTypeColumn", "mData": "AccountTypeColumn", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "AccountAddressColumn", "mData": "AccountAddressColumn", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "AccountCityStateZipColumn", "mData": "AccountCityStateZipColumn", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "LatitudeColumn", "mData": "LatitudeColumn", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "LongitudeColumn", "mData": "LongitudeColumn", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "NoOfRecord", "mData": "NoOfRecord", "sClass": "right", "sWidth": "50px" },
                            { "sTitle": "Active", "mData": "IsActive", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "SeqNo", "mData": "SeqNo", "sClass": "right", "sWidth": "50px" },
                            { "sTitle": "FileName", "mData": "FileName", "sClass": "left", "sWidth": "100px" },
                            { "sTitle": "CallType", "mData": "CallType", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "ImageURL", "mData": "ImageURL", "sClass": "left", "sWidth": "150px" },
                            { "sTitle": "AccountListDependOn", "mData": "AccountListDependOn", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "AddressMstTableName", "mData": "AddressMstTableName", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "AddressMstIdColumn", "mData": "AddressMstIdColumn", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "PageFooterView", "mData": "PageFooterView", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "LastUpdatedDateTime", "mData": "LastUpdatedDateTime", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "AccountSelectionCriterion", "mData": "AccountSelectionCriterion", "sClass": "left", "sWidth": "50px" }
                          ]
                });

                //$('#AccountTypeList').css('width', screen.width - 100);
                $('#AccountTypeList').css('width', $(window).width() - 100);
            }
            catch (err) {
                alert(err.message);
            }
        }
    </script>
    <div class="row-fluid">
        <div class="span12">
            <fieldset>
                <legend>Database</legend>
                <div id="Database" style="display: block;">
                    <table cellpadding="4" cellspacing="1">
                        <tr>
                            <td>
                                <table id="tblDataBaseAccountType" cellpadding="2" cellspacing="0">
                                    <tr>
                                        <td align="right">
                                            <span class="star">*</span><b>Database :</b>
                                        </td>
                                        <td align="left">
                                            <select id="ddlDatabase" onchange="javascript:return GetAccountTypeAndCategoryAndAttributeList();">
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="row-fluid">
        <div id="AccountTypeListContent" class="span12" style="display: none;">
            <fieldset>
                <legend>Account Type</legend>
                <div>
                    <div id="AccountTypeList" style="margin-top: 20px; overflow: auto;">
                        <table style="width: 100%" cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped"
                            id="AccountTypeTbl">
                            <thead>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</asp:Content>
