﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Air.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="ChangePasswors" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style>
        div.ajax-loading {
            position: fixed;
            top: 0;
            left: 0;
            z-index: 9999999;
            background-color: white;
            opacity: 0.7;
            filter: alpha(opacity=70); /* ie */
            -moz-opacity: 0.7; /* mozilla */
            display: none;
        }

            div.ajax-loading * {
                background-color: white;
                background-position: center center;
                background-repeat: no-repeat;
                opacity: 1;
                filter: alpha(opacity=100); /* ie */
                -moz-opacity: 1; /* mozilla */
            }

        .divchange {
            width: 300px;
            margin-left: 500px;
            margin-top: 99px;
            border: 1px solid black;
        }

        .btn {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            /*touch-action: manipulation;*/
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
        }

        td {
            padding-left: 5px;
        }

        .bootstrap-dialog-footer-buttons {
            background-color: #808080;
        }

        .bootstrap-dialog.type-primary .modal-header {
            background-color: #808080;
        }

        .modal-footer {
            background-color: #808080 !important;
        }

        .bootstrap-dialog-title {
            padding-left: 12px !important;
        }

        .modal-dialog {
            background-color: rgb(218, 213, 213) !important;
            color: #181717 !important;
        }
    </style>
    <div class="divchange">
        <table>
            <tr>
                <th colspan="2">
                    <h1>Change Password</h1>
                </th>

            </tr>
            <tr>
                <td>
                    <label for="new_password" class="control-label">New Password</label>

                </td>
                <td>
                    <input type="password" name="new_password" id="old_pwd">
                </td>

            </tr>
            <tr>
                <td>
                    <label for="new_password" class="control-label">New Password</label>

                </td>
                <td>
                    <input type="password" name="new_password" id="new_pwd">
                </td>

            </tr>
            <tr>
                <td>
                    <label for="confirm_password" class="control-label">Confirm Password</label></td>
                <td>
                    <input type="password" name="confirm_password" id="conf_pwd">
                </td>

            </tr>
            <tr>
                <td></td>

                <td style="height: 46px;">

                    <button class="btn btn-primary" id="password_save">Save changes</button>
                </td>

            </tr>
        </table>
        <div id="loading" class="ajax-loading" style="height: 100%; width: 100%; vertical-align: middle; text-align: center">
            <img src="Images_air/gif-load.gif" style="margin-top: 25%" alt="Loading please wait......" />
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            if (sessionStorage["username"] != null) {

            } else {
                location.href = "Login_Air.aspx";

            };

            $("#new_pwd").change(function () {
                debugger;
                CheckPassword($('#new_pwd').val());
            });
            //function checkPassword(str) {

            //    return re.test(str);
            //}

            $("#password_save").click(function () {

                debugger;
                if ($('#old_pwd').val() == "") {

                    BootstrapDialog.show({

                        title: 'Message',
                        message: "Old Password Is not empty ",

                        buttons: [
                          {
                              label: 'Close',
                              cssClass: 'btn btn-rounded  btn-primary',
                              action: function (dialogItself) {
                                  dialogItself.close();
                                  $("#old_pwd").focus();
                                  return false;
                              }
                          }]
                    });
                }
                else if ($('#new_pwd').val() == "") {
                    BootstrapDialog.show({

                        title: 'Message',
                        message: "New Password Is not empty ",

                        buttons: [
                          {
                              label: 'Close',
                              cssClass: 'btn btn-rounded  btn-primary',
                              action: function (dialogItself) {
                                  dialogItself.close();
                                  $("#new_pwd").focus();
                                  return false;
                              }
                          }]
                    });

                }
                else {
                    DeGrToolWebService.ChangePassword_Air(sessionStorage["username"], $('#old_pwd').val(), $('#new_pwd').val(), result);
                }
                return false;

            });

            function result(data) {
                debugger;
                var jsontable = JSON.parse(data);
                if (jsontable["Status"] == 1) {

                    BootstrapDialog.show({

                        title: 'Message',
                        message: jsontable["Message"],

                        buttons: [
                          {
                              label: 'Close',
                              cssClass: 'btn btn-rounded  btn-primary',
                              action: function (dialogItself) {
                                  dialogItself.close();
                                  location.href = "Issue_detail.aspx";
                                  return false;
                              }
                          }]
                    });

                } else {
                    BootstrapDialog.show({

                        title: 'Message',
                        message: jsontable["Message"],

                        buttons: [
                          {
                              label: 'Close',
                              cssClass: 'btn btn-rounded  btn-primary',
                              action: function (dialogItself) {
                                  dialogItself.close();
                                  $("#new_pwd").focus();
                                  return false;
                              }
                          }]
                    });

                }

            }
            $("#conf_pwd").change(function () {
                if ($("#new_pwd").val() != "") {
                    matchconfpwd();
                } else {
                    BootstrapDialog.show({

                        title: 'Message',
                        message: 'please Enter Password',

                        buttons: [
                          {
                              label: 'Close',
                              cssClass: 'btn btn-rounded  btn-primary',
                              action: function (dialogItself) {
                                  dialogItself.close();
                                  $("#new_pwd").focus();
                                  return false;
                              }
                          }]
                    });

                }
            });
            function CheckPassword(inputtxt) {
                debugger
                var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/;
                var restest = passw.test(inputtxt);

                if (restest == true) {



                } else {
                    BootstrapDialog.show({

                        title: 'Message',
                        message: 'Password Should be atleast 6 characters long. It should contain atleast one Numeric, one Upper Case and one Lower Case Character.',

                        buttons: [
                          {
                              label: 'Close',
                              cssClass: 'btn btn-rounded  btn-primary',
                              action: function (dialogItself) {
                                  dialogItself.close();
                                  $("#new_pwd").focus();
                                  return false;
                              }
                          }]
                    });
                    //alert("password is string of six or more characters that contains at least one digit and at least one lowercase character  and at least one uppercase character");
                    //$("#new_pwd").focus()

                }


            }
        });
        function matchconfpwd() {
            var pwd = $("#new_pwd").val();
            var new_pwd = $("#conf_pwd").val();


            if (new_pwd == pwd) {


            } else {
                BootstrapDialog.show({

                    title: 'Message',
                    message: 'paswword not match',

                    buttons: [
                      {
                          label: 'Close',
                          cssClass: 'btn btn-rounded  btn-primary',
                          action: function (dialogItself) {
                              dialogItself.close();
                              $("#conf_pwd").focus();
                              return false;
                          }
                      }]
                });
                //alert("paswword not match");

            }
        }

    </script>
    <link href="css/bootstrap-dialog.min.css" rel="stylesheet" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.7/js/bootstrap-dialog.min.js"></script>
</asp:Content>

