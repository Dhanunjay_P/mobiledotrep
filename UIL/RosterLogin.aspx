﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RosterLogin.aspx.cs" Inherits="RosterLogin" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head runat="server">
    <title>Roster</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="Roster/CSS/bootstrap-multiselect.css" rel="stylesheet" />
    
    <link rel="stylesheet" href="Roster/CSS/jquery.dataTables.min.css" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
    <link href="Roster/CSS/style.css" rel="stylesheet" />
    <link href="Roster/CSS/font-awesome.min.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="Roster/Scripts/bootstrap-multiselect.js"></script>
    <script src="Roster/Scripts/jquery.dataTables.min.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
            <Services>
                <asp:ServiceReference Path="~/DeGrToolWebService.asmx" />
            </Services>
        </asp:ScriptManager>
    <div class="login">
        <div class="login-modal" style="border-radius:10px">
            <div class="login-header" style="border-radius: 9px;border-bottom-left-radius: 0px;border-bottom-right-radius: 0px;">
                <img src="Roster/Image/logo.png" />
            </div>
            <div class="login-content">
                <h1>Login</h1>
                <div class="form-group">
                    <label for="usr">Username:</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="username" type="text" class="form-control" name="username" placeholder="Username">
                    </div>
                </div>
                <div class="form-group">
                    <label for="usr">Password:</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="password" type="password" class="form-control" name="password" placeholder="Password">
                    </div>
                </div>
                <a class="btn btn-primary btn-block" id="login">LOGIN</a>  
                <%-- <a class="btn btn-primary btn-block" id="log_cor">Login with Corporate User</a>  
                --%>
            </div>
            <%--<div class="login-footer">
                
            </div>--%>
        </div>
    </div>
        </form>
     <script type="text/javascript" language="javascript">
         $(document).ready(function () {
             $("#login").click(function (event) {
                 if (document.getElementById("username").value == "") {
                     alert("Please enter username.");
                     document.getElementById("username").style.borderColor = 'red';
                     document.getElementById("username").focus();
                     return false;
                 } else {
                     UserName = document.getElementById("username").value;
                     document.getElementById("username").style.borderColor = '';
                 }
                 if (document.getElementById("password").value == "") {
                     alert("Please enter password.");
                     document.getElementById("password").style.borderColor = 'red';
                     document.getElementById("password").focus();
                     return false;
                 }
                 else {
                     Password = document.getElementById("password").value;
                     document.getElementById("password").style.borderColor = '';
                 }

                 DeGrToolWebService.LoginWebSite_Roaster(UserName, Password, onSuccess, onFail);
             });
             $("#log_cor").click(function () {

                 window.location.href = "https://zspharma.okta.com/app/zspharma_roster_1/exkavzszxjY5QC1Gs0x7/sso/saml";

             });


         });
         $(document).keypress(function (e) {

             if (e.keyCode == 13 && $(document).find('#login').length == 1) {
                 $('#login').trigger('click');
             }
         });
         function onSuccess(data) {
             if (data == "1") {
                 sessionStorage["username"] = document.getElementById("username").value;
                 location.href = "Roster.aspx";

             }
             else if (data == "2") {
                 alert("User id is inactive.");
             }
             else {

                 alert("User id or password not matched");
             }
         }

         function onFail(error) {
             //  alert("Ajax Fail");
         }


    </script>
</body>
</html>

