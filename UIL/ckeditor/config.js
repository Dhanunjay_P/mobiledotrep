﻿/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function (config) {
    config.toolbar =
    [      
        ['Cut', 'Copy', 'Paste', 'SelectAll'],
        ['Bold', 'Italic', 'Underline', '-', 'Subscript', 'Superscript'],
        ['-', 'Outdent', 'Indent'],
        ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
        ['Link', 'Unlink'],
        ['SpecialChar'],
        ['TextColor'],
        ['Font'], ['FontSize'],
    ];
    config.uiColor = '#D4D4D4';
    config.enterMode = CKEDITOR.ENTER_BR;
    config.removePlugins = 'contextmenu';
    config.forcePasteAsPlainText = true;
    config.toolbarCanCollapse = false;
};
