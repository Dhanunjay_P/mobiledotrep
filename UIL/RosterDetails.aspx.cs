﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class Roster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Download_Roster(object sender, EventArgs e)
    {


        string str_path = HttpContext.Current.Request.Url.Authority;

        for (int i = 0; i < (HttpContext.Current.Request.Url.Segments.Length - 1); i++)
        {
            str_path = str_path + HttpContext.Current.Request.Url.Segments[i];
        }


        ReportPrinter obReportPrinter = new ReportPrinter();

        obReportPrinter.PageFile = "https://" + str_path + "PrintRosterData.aspx?saleforce=" + hidSalesforce.Value + "&Terriotory=" + hidTerritory.Value + "&employee=" + hidEmployee.Value + "&userName=" + hiduserName.Value;
        //  obReportPrinter.PageFile = "http://" + str_path + "printData.aspx?caseid=" + hnd_caseid.Value + "&username=KT";
        //  BLL.Utilities.ServerLog.MgmtExceptionLog(obReportPrinter.PageFile);


        obReportPrinter.MarginTop = "10";
        
        obReportPrinter.GetPdf();


        if (obReportPrinter.FileContent != null && obReportPrinter.FileContent.Length > 0)
        {
            FileStream fs = new FileStream(Path.Combine(@"C:\abc.pdf"), FileMode.Create);
            fs.Write(obReportPrinter.FileContent, 0, obReportPrinter.FileContent.Length);
            fs.Dispose();
            fs.Close();

            HttpContext.Current.Response.ContentType = "application/octet-stream";
            if (hidTerritoryName.Value != String.Empty)
            {
                HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename=\"{0}\"", hidTerritoryName.Value + ".pdf"));
            }
            else
            {
                HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename=\"{0}\"", "Geography.pdf"));
            }
            HttpContext.Current.Response.BinaryWrite(obReportPrinter.FileContent);
        }

    }

    protected void Download_Employee(object sender, EventArgs e)
    {


        string str_path = HttpContext.Current.Request.Url.Authority;

        for (int i = 0; i < (HttpContext.Current.Request.Url.Segments.Length - 1); i++)
        {
            str_path = str_path + HttpContext.Current.Request.Url.Segments[i];
        }


        ReportPrinter obReportPrinter = new ReportPrinter();

        obReportPrinter.PageFile = "https://" + str_path + "PrintEmployeeData.aspx?employee=" + hidEmployee.Value + "&username=" + hiduserName.Value;
        //  obReportPrinter.PageFile = "http://" + str_path + "printData.aspx?caseid=" + hnd_caseid.Value + "&username=KT";
        //  BLL.Utilities.ServerLog.MgmtExceptionLog(obReportPrinter.PageFile);

        obReportPrinter.MarginTop = "10";
        
        
        obReportPrinter.GetPdf();


        if (obReportPrinter.FileContent != null && obReportPrinter.FileContent.Length > 0)
        {
            //FileStream fs = new FileStream(Path.Combine(@"D:\jaimindata\m_pradeep\DeGrToolNew\abc.pdf"), FileMode.Create);
            //fs.Write(obReportPrinter.FileContent, 0, obReportPrinter.FileContent.Length);
            //fs.Dispose();
            //fs.Close();

            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename=\"{0}\"", hidEmployeeName.Value + ".pdf"));
            HttpContext.Current.Response.BinaryWrite(obReportPrinter.FileContent);
        }

    }
}