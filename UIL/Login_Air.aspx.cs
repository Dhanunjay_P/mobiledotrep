﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using BLL.Masters;
using System.Data;

public partial class Login_Air : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.Form["SAMLResponse"] != null)
            {
                var responseDecoded = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(HttpUtility.HtmlDecode(Request.Form["SAMLResponse"])));
                XmlDocument xdoc = new XmlDocument();

                using (StringReader sr = new StringReader(responseDecoded))
                {
                    using (XmlReader reader = XmlReader.Create(sr))
                    {
                        xdoc.Load(reader);

                        try
                        {
                            string str_path = Server.MapPath("~/Styles/");
                            xdoc.Save(str_path + "SAML_Response.xml");
                        }
                        catch (Exception ex)
                        { }

                        //var elem_status_code = xdoc["saml2p:Response"]["saml2p:Status"]["saml2p:StatusCode"];
                        var elem_status_code = xdoc["samlp:Response"]["samlp:Status"]["samlp:StatusCode"];

                        var attr_value = false;

                        if (elem_status_code != null)
                        {
                            if (elem_status_code.Attributes["Value"] != null)
                            {
                                attr_value = elem_status_code.Attributes["Value"].Value.ToLower().Contains("status:success");
                            }
                        }

                        if (attr_value)
                        {
                            Session["user_id"] = "okta_user";
                            Session["login_type"] = "sso";

                            //var attr_user_first_name = xdoc["saml2p:Response"]["saml2:Assertion"]["saml2:AttributeStatement"].ChildNodes[0]["saml2:AttributeValue"].InnerText;
                            //var attr_user_last_name = xdoc["saml2p:Response"]["saml2:Assertion"]["saml2:AttributeStatement"].ChildNodes[1]["saml2:AttributeValue"].InnerText;
                            //var attr_user_email = xdoc["saml2p:Response"]["saml2:Assertion"]["saml2:AttributeStatement"].ChildNodes[2]["saml2:AttributeValue"].InnerText;
                            var attr_user_email = xdoc["samlp:Response"]["saml:Assertion"]["saml:Subject"]["saml:NameID"].InnerText;

                            //MgmtSales objsales = new MgmtSales();

                            //DataTable dt_user_datail = objsales.GetUserDetail_air_user(attr_user_email);

                            //string str_script = "sessionStorage['username']='" + attr_user_email + "';sessionStorage['is_single']='Y';sessionStorage['user_first_name']='" + attr_user_first_name + "';sessionStorage['user_last_name']='" + attr_user_last_name + "';sessionStorage['user_email']='" + attr_user_email + "';location.href = 'Issue_detail.aspx';";

                            string str_script = "sessionStorage['username']='" + attr_user_email + "';sessionStorage['is_single']='Y';sessionStorage['user_email']='" + attr_user_email + "';location.href = 'Issue_detail.aspx';";
                            ScriptManager.RegisterStartupScript(this, GetType(), "Message", str_script, true);
                        }
                    }
                }
            }
            else if (Request.Form["SAMLResponse"] == null)
            {
                if (Session["login_type"] != null && Session["login_type"].ToString() == "sso")
                {
                    //Response.Redirect(str_okta_url);
                }
                else if (Session["user_id"] != null)
                {
                    //Response.Redirect("Issue_detail.aspx");
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
}