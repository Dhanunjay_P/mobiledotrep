﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="UserMst.aspx.cs" Inherits="UserMst" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderTitle" runat="Server">
    User Master
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript" type="text/javascript">

        var OriginalData = new Object();
        var NewUploadData = new Object();
        var OldUploadData = new Object();
        var oTableUser = null;
        var validator;
        var NoOfRow = 0;

        $("legend").live('click', function () {
            $(this).siblings().slideToggle("slow");
        });

        function pageLoad() {

            //SessionData = JSON.parse(document.getElementById("ctl00_ContentPlaceHolder1_hidSession").value);

            PrepareBasicObjects("UserMst", "GetInitialData", "M", "S", "M001", "M003N");

            AutoShowMessage = false;
            CallWebService(OnPageLoadSuccess);
        }

        function OnPageLoadSuccess() {

            if (ResultObject != null) {
                if (ResultObject.responseObjectInfo.Status == 1) {

                    BindDropDown(ResultObject.responseObjectInfo.dt_ReturnedTables[0], "ddlDatabase", "code", "desc", FirstIndexAS.Select);
                }
                else
                    $("#ddlDatabase").empty();
            }
        }

        function GetUserList() {

            $("#DataList").hide();
            $("#NewEntryAndModifyUser").hide();
            $("#UserListContent").hide();
            $("#chkAccountTypeGroupId").empty();
            $("#chkAttributeOperationGroupId").empty();
            $("#ddlTerritoryNum").empty();

            PrepareBasicObjects("UserMst", "GetUserDetails", "M", "S", "M001", "M003N");

            var database = $("#ddlDatabase").val();

            OpArgsObject["database"] = database;

            AutoShowMessage = false;
            CallWebService(OnGetUserListSuccess);
        }

        function OnGetUserListSuccess() {

            if (ResultObject != null) {

                if (ResultObject.responseObjectInfo.Status == 1) {

                    if (ResultObject.responseObjectInfo.dt_ReturnedTables[0] != null) {
                        DisplayUserData(ResultObject.responseObjectInfo.dt_ReturnedTables[0]);
                        $("#UserListContent").show();
                    }
                    else
                        $("#UserListContent").hide();

                    if (ResultObject.responseObjectInfo.dt_ReturnedTables[2] != null)
                        BindDropDown(ResultObject.responseObjectInfo.dt_ReturnedTables[2], "ddlTerritoryNum", "TerritoryNum", "TerritoryName", FirstIndexAS.Select);
                    else
                        $("#ddlTerritoryNum").empty();

                    if (ResultObject.responseObjectInfo.dt_ReturnedTables[3] != null)
                        BindDropDown(ResultObject.responseObjectInfo.dt_ReturnedTables[3], "chkAccountTypeGroupId", "AccountTypeGroupId", "AccountTypeGroupDesc", FirstIndexAS.Select);
                    else
                        $("#chkAccountTypeGroupId").empty();

                    if (ResultObject.responseObjectInfo.dt_ReturnedTables[4] != null)
                        BindDropDown(ResultObject.responseObjectInfo.dt_ReturnedTables[4], "chkAttributeOperationGroupId", "AttributeOperationGroupId", "AttributeOperationGroupDesc", FirstIndexAS.Select);
                    else
                        $("#chkAttributeOperationGroupId").empty();
                }
                else {
                    $("#UserListContent").hide();
                    $("#chkAccountTypeGroupId").empty();
                    $("#chkAttributeOperationGroupId").empty();
                    $("#ddlTerritoryNum").empty();
                }
            }
        }

        function DisplayUserData(data) {

            try {
                if (oTableUser != null) {
                    oTableUser.fnDestroy();
                    $("#UserList").html(' <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped" id="UserTbl"><thead></thead><tbody> </tbody><tfoot> </tfoot></table>');
                }
                oTableUser = $("#UserTbl").dataTable({
                    "iDisplayLength": 25,
                    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                    "aaData": data,
                    "aoColumns": [
                            {
                                "sTitle": '<div style="width:40px;text-align:center;margin-bottom:2px;"><i class="icon-plus-sign" title="Add User" id="btnAddUser" onclick="AddUser()" data-mode="ADD"></i></div>',
                                "mData": null,
                                "sClass": "center",
                                "sWidth": "20px",
                                "sDefaultContent": '<i id="imgEditUser" title="Edit User" class="icon-pencil"></i>&nbsp;<i id="imgDeleteUser" title="Delete User" class="icon-trash"></i>',
                                "bSearchable": false,
                                "bSortable": false
                            },
                            { "sTitle": "RepId", "mData": "RepId", "sClass": "left", "sWidth": "80px" },
                            { "sTitle": "RepName", "mData": "RepName", "sClass": "left", "sWidth": "100px" },
                            { "sTitle": "Email", "mData": "Email", "sClass": "left", "sWidth": "80px" },
                            { "sTitle": "Designation", "mData": "Designation", "sClass": "left", "sWidth": "80px" },
                            { "sTitle": "Password", "mData": "DecryptPassword", "sClass": "left", "sWidth": "80px" },
                            { "sTitle": "AccountTypeGroup", "mData": "AccountTypeGroupDesc", "sClass": "left", "sWidth": "80px" },
                            { "sTitle": "AttributeOperationGroup", "mData": "AttributeOperationGroupDesc", "sClass": "left", "sWidth": "80px" },
                            { "sTitle": "SendDKPAlert", "mData": "SendDKPAlert", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "Active", "mData": "IsActive", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "UserStatusFlag", "mData": "UserStatusFlag", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "RepTerritoryMappedList", "mData": "TerritoryNum", "sClass": "left", "sWidth": "200px" }
                          ]
                });

                //$('#UserList').css('width', screen.width - 100);
                $('#UserList').css('width', $(window).width() - 100);
            }
            catch (err) {
                alert(err.message);
            }
        }

        function ChangeRepId() {

            $("#txtEmail").val($("#txtRepId").val());
        }

        function AddUser() {

            NoOfRow = 1;
            $("#txtNewEditUser").val("New");
            ClearUserData();
            $("#txtRepId").prop("disabled", false);
            $("#txtEmail").prop("disabled", true);
            $("#NewEntryAndModifyUser").show();
            $("#DataList").show();
            $("#PasswordRow").show();
            $("#UserList").hide();
        }

        //Click on Edit Image
        $('#UserTbl tbody tr td i.icon-pencil').live('click', function (e) {
            var row = $(this).closest("tr").get(0);
            var aData = oTableUser.fnGetData(row);

            //$.setFormData("Config_UserMst", aData);
            GetUserDetails(aData["RepId"]);
            return false;
        });

        function GetUserDetails(RepId) {

            NoOfRow = 1;
            $("#txtSrNo").val(NoOfRow);
            PrepareBasicObjects("UserMst", "GetUserDetails", "M", "S", "M001", "M003N");

            var database = $("#ddlDatabase").val();

            OpArgsObject["database"] = database;
            OpArgsObject["RepId"] = RepId;

            AutoShowMessage = false;
            CallWebService(OnGetUserDetailsSuccess);
        }

        function OnGetUserDetailsSuccess() {

            if (ResultObject != null) {
                if (ResultObject.responseObjectInfo.Status == 1) {

                    $("#NewEntryAndModifyUser").show();
                    $("#DataList").show();
                    $("#UserList").hide();
                    var aData = ResultObject.responseObjectInfo.dt_ReturnedTables[0][0];
                    $.setFormData("Config_UserMst", aData);

                    $("#txtNewEditUser").val("Edit");
                    $("#txtRepId").prop("disabled", true);
                    $("#txtEmail").prop("disabled", true);
                    $("#PasswordRow").hide();

                    if (ResultObject.responseObjectInfo.dt_ReturnedTables[1] != null) {

                        DisplayRepTerritoryMapData(ResultObject.responseObjectInfo.dt_ReturnedTables[1]);

                        NoOfRow = ResultObject.responseObjectInfo.dt_ReturnedTables[1].length;
                    }
                    NoOfRow++;
                    $("#txtSrNo").val(NoOfRow);
                }
                else
                    ClearUserData();
            }
        }

        function DisplayRepTerritoryMapData(data) {

            try {

                $("#tbodyDtls").html("");

                OriginalData["Config_RegionTerritoryMap"] = data;

                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {

                        var TerritoryNum = data[i]["TerritoryNum"];
                        var TerritoryName = data[i]["TerritoryName"];
                        var IsActive = data[i]["IsActive"];
                        var IsActiveDesc = "Yes";
                        if (IsActive == "N")
                            IsActiveDesc = "No";
                        var TerritoryOwner = data[i]["TerritoryOwner"];
                        var TerritoryOwnerDesc = "No";
                        if (TerritoryOwner == "Y")
                            TerritoryOwnerDesc = "Yes";
                        var sr_no = i + 1;
                        var rowToReplace = "";

                        rowToReplace += "<tr id='" + sr_no + "'>";

                        rowToReplace += "<td><i id='imgEdit' title='Edit' class='icon-pencil' onclick='editRow(\"" + sr_no + "\")'></i> ";

                        rowToReplace += "<i id='imgDelete' title='Delete' class='icon-trash' onclick='deleteRow(\"" + sr_no + "\")'></i></td>";

                        rowToReplace += "<td abbr='" + TerritoryNum + "'>" + TerritoryName + "</td>";

                        rowToReplace += "<td abbr='" + IsActive + "'>" + IsActiveDesc + "</td>";

                        rowToReplace += "<td abbr='" + TerritoryOwner + "'>" + TerritoryOwnerDesc + "</td>";

                        rowToReplace += "<td style='display:none'>" + sr_no + "</td>";

                        rowToReplace += "</tr>";

                        $("#example tbody").append(rowToReplace);
                    }
                }
            }
            catch (e) {
                alert("Exception : " + e.message);
            }
        }

        //Click on Delete Image
        $('#UserTbl tbody tr td i.icon-trash').live('click', function (e) {
            var row = $(this).closest("tr").get(0);
            var aData = oTableUser.fnGetData(row);
            //$.setFormData("Config_UserMst", aData);
            var answer = confirm("Are Sure You Want to Delete User : " + aData["RepName"] + " ?");
            if (answer) {
                GetUserDetailForDelete(aData["RepId"]);
            }
            return false;
        });

        function GetUserDetailForDelete(RepId) {

            NoOfRow = 1;
            $("#txtSrNo").val(NoOfRow);
            PrepareBasicObjects("UserMst", "GetUserDetails", "M", "S", "M001", "M003N");

            var database = $("#ddlDatabase").val();

            OpArgsObject["database"] = database;
            OpArgsObject["RepId"] = RepId;

            AutoShowMessage = false;
            CallWebService(OnGetUserDetailForDeleteSuccess);
        }

        function OnGetUserDetailForDeleteSuccess() {

            if (ResultObject != null) {
                if (ResultObject.responseObjectInfo.Status == 1) {

                    PrepareBasicObjects("UserMst", "SaveUserData", "M", "S", "M001", "M003N");

                    var listofrecords = new Array();
                    var form_header = ResultObject.responseObjectInfo.dt_ReturnedTables[0][0];
                    form_header["IsActive"] = "N";

                    listofrecords[0] = form_header;

                    NewUploadData["Config_UserMst"] = listofrecords;

                    var RepTerritoryMap = ResultObject.responseObjectInfo.dt_ReturnedTables[1];
                    for (var i = 0; i < RepTerritoryMap.length; i++) {

                        RepTerritoryMap[i]["IsActive"] = "N";
                    }

                    NewUploadData["Config_RepTerritoryMap"] = RepTerritoryMap;

                    OldUploadData = {};

                    UploadDataObject["New"] = NewUploadData;
                    UploadDataObject["Old"] = null;

                    $("#txtNewEditUser").val("Edit");
                    var database = $("#ddlDatabase").val();
                    var RepId = $("#txtRepId").val();
                    var NewEdit = $("#txtNewEditUser").val();

                    OpArgsObject["database"] = database;
                    OpArgsObject["RepId"] = RepId;
                    OpArgsObject["NewEdit"] = NewEdit;

                    AutoShowMessage = true;
                    CallWebService(OnSaveUserDataSuccess);
                }
            }
        }

        function addrow() {

            try {

                if ($("#ddlTerritoryNum").val() == null || $("#ddlTerritoryNum").val() == "") {

                    alert("Please select Territory.");
                    $("#ddlTerritoryNum").focus();
                    return false;
                }
                else if ($("#ddlIsActive").val() == null || $("#ddlIsActive").val() == "") {

                    alert("Please select Active/Deactive.");
                    $("#ddlIsActive").focus();
                    return false;
                }
                else if ($("#ddlTerritoryOwner").val() == null || $("#ddlTerritoryOwner").val() == "") {

                    alert("Please select TerritoryOwner (Yes/No).");
                    $("#ddlTerritoryOwner").focus();
                    return false;
                }
                else {

                    var TerritoryNum = $("#ddlTerritoryNum").val();
                    var TerritoryName = $("#ddlTerritoryNum option:selected").text();
                    var IsActive = $("#ddlIsActive").val();
                    var IsActiveDesc = $("#ddlIsActive option:selected").text();
                    var TerritoryOwner = $("#ddlTerritoryOwner").val();
                    var TerritoryOwnerDesc = $("#ddlTerritoryOwner option:selected").text();
                    var sr_no = $("#txtSrNo").val();
                    var rowToReplace = "";

                    rowToReplace += "<tr id='" + sr_no + "'>";

                    rowToReplace += "<td><i id='imgEdit' title='Edit' class='icon-pencil' onclick='editRow(\"" + sr_no + "\")'></i> ";

                    rowToReplace += "<i id='imgDelete' title='Delete' class='icon-trash' onclick='deleteRow(\"" + sr_no + "\")'></i></td>";

                    rowToReplace += "<td abbr='" + TerritoryNum + "'>" + TerritoryName + "</td>";

                    rowToReplace += "<td abbr='" + IsActive + "'>" + IsActiveDesc + "</td>";

                    rowToReplace += "<td abbr='" + TerritoryOwner + "'>" + TerritoryOwnerDesc + "</td>";

                    rowToReplace += "<td style='display:none'>" + sr_no + "</td>";

                    rowToReplace += "</tr>";

                    if ($("#btnAdd").data('mode') == "UPDATE") {

                        $("#" + sr_no).replaceWith(rowToReplace);

                        $("#btnAdd").data("mode", "ADD");
                    }
                    else {

                        if ($("#" + sr_no).length > 0) {
                            $("#" + sr_no).replaceWith(rowToReplace);
                        }
                        else {
                            $("#example tbody").append(rowToReplace);
                        }

                        NoOfRow++;
                    }

                    $("#txtSrNo").val(NoOfRow);
                    $("#ddlTerritoryNum").val("");
                    $("#ddlIsActive").val("");
                    $("#ddlTerritoryOwner").val("");
                }
            }
            catch (e) {

                alert("Exception : " + e.message);
                return false;
            }
        }

        //----- Edit Details Row ----- 
        function editRow(rowID) {
            debugger;
            try {

                $("#" + rowID + " td").each(function (i) {
                    if ($(this).attr("abbr") != undefined) {
                        if ($("#controlRow td:nth-child(" + (i + 1) + ")").children().length > 1) {
                            $("#controlRow td:nth-child(" + (i + 1) + ")").find("input[type='hidden']").val($(this).attr("abbr"));
                            $("#controlRow td:nth-child(" + (i + 1) + ")").find("input[type='text']").val($(this).html());
                            $("#controlRow td:nth-child(" + (i + 1) + ")").find("label").html($(this).html());
                        }
                        else {
                            $("#controlRow td:nth-child(" + (i + 1) + ")").children().val($(this).attr("abbr"));
                        }
                    }
                    else {
                        $("#controlRow td:nth-child(" + (i + 1) + ")").children().val($(this).html());
                        $("#controlRow td:nth-child(" + (i + 1) + ")").find("label").html($(this).html());
                    }
                });

                $("#btnAdd").data("mode", "UPDATE");

                return false;
            }
            catch (e) {
                alert("Exception : " + e.message);
                return false;
            }
        }

        //----- Delete Details Row ----- 
        function deleteRow(rowID) {

            try {

                var TerritoryNum = $("#" + rowID + " td")[1].firstChild.data;
                var answer = confirm("Are Sure You Want to Delete Row : " + TerritoryNum + " ?");
                if (answer) {

                    $("#" + rowID).remove();
                }
                return false;
            }
            catch (e) {
                alert("Exception : " + e.message);
            }
        }

        function PrepareUploadUserDataObject() {
            var listofrecords = new Array();
            var form_header = new Object();

            form_header = $.getFormData("Config_UserMst");

            listofrecords[0] = form_header;

            NewUploadData["Config_UserMst"] = listofrecords;

            var table = document.getElementById("example");
            var rowCollection = table.rows;
            var dtlList = [];
            var sr_no = 1;

            for (var i = 2; i < rowCollection.length; i++) {

                var dtl = {};

                dtl["RepId"] = form_header["RepId"];

                if (rowCollection[i].cells[1].abbr != null)
                    dtl["TerritoryNum"] = rowCollection[i].cells[1].abbr;
                else
                    dtl["TerritoryNum"] = null;
                if (rowCollection[i].cells[2].abbr != null)
                    dtl["IsActive"] = rowCollection[i].cells[2].abbr;
                else
                    dtl["IsActive"] = null;
                if (rowCollection[i].cells[3].abbr != "")
                    dtl["TerritoryOwner"] = rowCollection[i].cells[3].abbr;
                else
                    dtl["TerritoryOwner"] = null;

                dtl["sr_no"] = sr_no++;

                dtlList.push(dtl);
            }

            NewUploadData["Config_RepTerritoryMap"] = dtlList;

            OldUploadData = {};

            UploadDataObject["New"] = NewUploadData;
            UploadDataObject["Old"] = null;

            var database = $("#ddlDatabase").val();
            var RepId = $("#txtRepId").val();
            var NewEdit = $("#txtNewEditUser").val();

            OpArgsObject["database"] = database;
            OpArgsObject["RepId"] = RepId;
            OpArgsObject["NewEdit"] = NewEdit;
        }

        function SaveUserData() {

            if ($("#txtRepId").val() == null || $("#txtRepId").val() == "") {
                alert("Please enter RepId.");
                $("#txtRepId").focus();
                return false;
            }
            else if ($("#txtRepName").val() == null || $("#txtRepName").val() == "") {
                alert("Please enter RepName.");
                $("#txtRepName").focus();
                return false;
            }
            else if ($("#txtDesignation").val() == null || $("#txtDesignation").val() == "") {
                alert("Please enter Designation.");
                $("#txtDesignation").focus();
                return false;
            }
            else if ($("#txtPassword").val() == null || $("#txtPassword").val() == "") {
                alert("Please enter Password.");
                $("#txtPassword").focus();
                return false;
            }
            else if ($("#txtConformPassword").val() == null || $("#txtConformPassword").val() == "") {
                alert("Please enter Conform Password.");
                $("#txtConformPassword").focus();
                return false;
            }
            else if ($("#txtPassword").val() != $("#txtConformPassword").val()) {
                alert("Password & Conform Password must be equal.");
                $("#txtPassword").val("");
                $("#txtConformPassword").val("");
                $("#txtPassword").focus();
                return false;
            }
            else if ($("#chkAccountTypeGroupId").val() == null || $("#chkAccountTypeGroupId").val() == "") {
                alert("Please select AccountType GroupId.");
                $("#chkAccountTypeGroupId").focus();
                return false;
            }
            else if ($("#chkAttributeOperationGroupId").val() == null || $("#chkAttributeOperationGroupId").val() == "") {
                alert("Please enter AttributeOperation GroupId.");
                $("#chkAttributeOperationGroupId").focus();
                return false;
            }
            var table = document.getElementById("example");
            if (table.rows.length <= 2) {
                alert("Please enter at least one Rep Territory Map row to Save Data.");
                return false;
            }

            var answer = confirm("Are Sure You Want To Save Data?");
            if (answer) {

                PrepareBasicObjects("UserMst", "SaveUserData", "M", "S", "M001", "M003N");

                PrepareUploadUserDataObject();

                AutoShowMessage = true;
                CallWebService(OnSaveUserDataSuccess);
            }
        }

        function OnSaveUserDataSuccess() {

            if (ResultObject != null) {
                if (ResultObject.responseObjectInfo.Status == 1) {

                    ClearUserData();

                    $("#NewEntryAndModifyUser").hide();
                    $("#DataList").hide();
                    $("#UserList").show();

                    if (ResultObject.responseObjectInfo.dt_ReturnedTables[0] != null)
                        DisplayUserData(ResultObject.responseObjectInfo.dt_ReturnedTables[0]);
                }
            }
        }

        function BackUserData() {

            $("#NewEntryAndModifyUser").hide();
            $("#DataList").hide();
            $("#UserList").show();
        }

        function ClearUserData() {

            if ($("#txtNewEditUser").val() == "New") {

                $("#txtRepId").val("");
                $("#txtEmail").val("");
                $("#txtPassword").val("");
                $("#txtConformPassword").val("");
            }

            $("#txtRepName").val("");
            $("#txtDesignation").val("");
            $("#chkAccountTypeGroupId").val("");
            $("#chkAttributeOperationGroupId").val("");
            $("#chkSendDKPAlert").val("N");
            $("#chkIsActiveUser").val("Y");
            $("#chkUserStatusFlag").val("A");

            NoOfRow = 1;
            $("#txtSrNo").val(NoOfRow);
            $("#ddlTerritoryNum").val("");
            $("#ddlIsActive").val("");
            $("#ddlTerritoryOwner").val("");
            $("#tbodyDtls").html("");
        }                
    </script>
    <div class="row-fluid">
        <div class="span12">
            <fieldset>
                <legend>Database</legend>
                <div id="Database" style="display: block;">
                    <table cellpadding="4" cellspacing="1">
                        <tr>
                            <td>
                                <table id="tblDataBaseAccountType" cellpadding="2" cellspacing="0">
                                    <tr>
                                        <td align="right">
                                            <span class="star">*</span><b>Database :</b>
                                        </td>
                                        <td align="left">
                                            <select id="ddlDatabase" onchange="javascript:return GetUserList();">
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="row-fluid">
        <div id="UserListContent" class="span12" style="display: none;">
            <fieldset>
                <legend>User</legend>
                <div>
                    <div id="UserList" style="margin-top: 20px; overflow: auto;">
                        <table style="width: 100%" cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped"
                            id="UserTbl">
                            <thead>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                    <div id="NewEntryAndModifyUser" style="display: none;">
                        <table cellpadding="4" cellspacing="1">
                            <tr>
                                <td>
                                    <table id="tblUserDetail" cellpadding="2" cellspacing="0">
                                        <tr style="display: none;">
                                            <td align="right">
                                                <b>New/Edit :</b>
                                            </td>
                                            <td align="left" colspan="3">
                                                <input type="text" id="txtNewEditUser" data-field-name="NewEdit" maxlength="5" disabled="disabled"
                                                    class="Config_UserMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>RepId :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtRepId" data-field-name="RepId" maxlength="100" onchange="javascript:return ChangeRepId();"
                                                    class="Config_UserMst" />
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span><b>RepName :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtRepName" data-field-name="RepName" maxlength="100" class="Config_UserMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>Email :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtEmail" data-field-name="Email" maxlength="100" disabled="disabled"
                                                    class="Config_UserMst" />
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span><b>Designation :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtDesignation" data-field-name="Designation" style="text-transform: uppercase"
                                                    maxlength="15" class="Config_UserMst" />
                                            </td>
                                        </tr>
                                        <tr id="PasswordRow">
                                            <td align="right">
                                                <span class="star">*</span><b>Password :</b>
                                            </td>
                                            <td align="left">
                                                <input type="password" id="txtPassword" data-field-name="Password" maxlength="30"
                                                    class="Config_UserMst" />
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span><b>Conform Password :</b>
                                            </td>
                                            <td align="left">
                                                <input type="password" id="txtConformPassword" data-field-name="Password" maxlength="30"
                                                    class="Config_UserMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>AccountTypeGroupId :</b>
                                            </td>
                                            <td align="left">
                                                <select id="chkAccountTypeGroupId" data-field-name="AccountTypeGroupId" class="Config_UserMst">
                                                </select>
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span><b>AttributeOperationGroupId :</b>
                                            </td>
                                            <td align="left">
                                                <select id="chkAttributeOperationGroupId" data-field-name="AttributeOperationGroupId"
                                                    class="Config_UserMst">
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <b>SendDKPAlert :</b>
                                            </td>
                                            <td align="left">
                                                <select id="chkSendDKPAlert" data-field-name="SendDKPAlert" class="Config_UserMst">
                                                    <option value="N" selected="true">No</option>
                                                    <option value="Y">Yes</option>
                                                </select>
                                            </td>
                                            <td align="right">
                                                <b>Active :</b>
                                            </td>
                                            <td align="left">
                                                <select id="chkIsActiveUser" data-field-name="IsActive" class="Config_UserMst">
                                                    <option value="N">No</option>
                                                    <option value="Y" selected="true">Yes</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <b>UserStatus :</b>
                                            </td>
                                            <td align="left">
                                                <select id="chkUserStatusFlag" data-field-name="UserStatusFlag" class="Config_UserMst">
                                                    <option value="A" selected="true">Active</option>
                                                    <option value="D">Deactive</option>
                                                </select>
                                            </td>
                                            <td align="right" style="display: none">
                                                <b>CreatedBy :</b>
                                            </td>
                                            <td align="left" style="display: none">
                                                <input type="text" id="txtCreatedBy" data-field-name="CreatedBy" disabled="disabled"
                                                    maxlength="30" class="Config_UserMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="display: none">
                                                <b>CreatedDate :</b>
                                            </td>
                                            <td align="left" style="display: none">
                                                <input type="text" id="txtCreatedDate" data-field-name="CreatedDate" disabled="disabled"
                                                    maxlength="30" class="Config_UserMst" />
                                            </td>
                                            <td align="right" style="display: none">
                                                <b>CreatedHost :</b>
                                            </td>
                                            <td align="left" style="display: none">
                                                <input type="text" id="txtCreatedHost" data-field-name="CreatedHost" disabled="disabled"
                                                    maxlength="30" class="Config_UserMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4">
                                                <input type="button" id="btnSaveCategory" value="Save" onclick="javascript:return SaveUserData();"
                                                    style="width: 90px; font-weight: bold;" class="btn" />
                                                <input type="button" id="btnClearCategory" value="Clear" onclick="javascript:return ClearUserData();"
                                                    style="width: 90px; font-weight: bold;" class="btn" />
                                                <input type="button" id="btnBackCategory" value="Back" onclick="javascript:return BackUserData();"
                                                    style="width: 90px; font-weight: bold;" class="btn" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="DataList" style="margin-top: 20px; display: none">
                        <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped"
                            id="example">
                            <thead>
                                <tr class="header">
                                    <th style="text-align: left; width: 40px;">
                                    </th>
                                    <th style="text-align: left; max-width: 150px;">
                                        <b>TerritoryNum</b>
                                    </th>
                                    <th style="text-align: left; max-width: 100px;">
                                        <b>Active</b>
                                    </th>
                                    <th style="text-align: left; max-width: 100px;">
                                        <b>TerritoryOwner</b>
                                    </th>
                                    <th style="text-align: left; width: 40px; display: none">
                                        SrNo
                                    </th>
                                </tr>
                                <tr id="controlRow">
                                    <td>
                                        <i class="icon-plus-sign" title="Add" id="btnAdd" onclick="addrow()" data-mode="ADD">
                                        </i>
                                    </td>
                                    <td>
                                        <select id="ddlTerritoryNum" style="width: 150px;" data-field-name="TerritoryNum"
                                            class="Config_RepTerritoryMap">
                                        </select>
                                    </td>
                                    <td>
                                        <select id="ddlIsActive" style="width: 100px;" data-field-name="IsActive" class="Config_RepTerritoryMap">
                                            <option value="Y" selected="true">Yes</option>
                                            <option value="N">No</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="ddlTerritoryOwner" style="width: 100px;" data-field-name="TerritoryOwner"
                                            class="Config_RepTerritoryMap">
                                            <option value="N" selected="true">No</option>
                                            <option value="Y">Yes</option>
                                        </select>
                                    </td>
                                    <td style="display: none;">
                                        <input type="text" id="txtSrNo" style="width: 40px; display: none" disabled="disabled"
                                            data-field-name="SrNo" class="Config_RepTerritoryMap" />
                                    </td>
                                </tr>
                            </thead>
                            <tbody id="tbodyDtls">
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</asp:Content>
