﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ManualNotification.aspx.cs"
    Inherits="ManualNotification" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Manual Notification</title>
    <script src="Javascript/jquery_1.9.0.min.js" type="text/javascript"></script>
    <link href="Thickbox/thickbox.css" rel="stylesheet" type="text/css" />  
      <script src="Thickbox/thickbox-compressed.js" type="text/javascript"></script>
      <script src="Thickbox/thickbox.js" type="text/javascript"></script>  
    <script src="Javascript/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
    <link href="css/smoothness/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>--%>
    <link href="CSS%20Styles/coolMenu.css" rel="stylesheet" type="text/css" />
    <script src="ui/CookiesCreate.js" type="text/javascript"></script>
    <%-- <script src="Menu/jquery.js" type="text/javascript"></script>--%>
    <link href="Menu/menu.css" rel="stylesheet" type="text/css" />
    <%--<script src="Menu/menu.js" type="text/javascript"></script>--%>
    <link type="text/css" rel="stylesheet" href="calender/jscal2.css" />
    <link href="calender/border-radius.css" rel="stylesheet" type="text/css" />
    <link title="Win 2K" type="text/css" rel="alternate stylesheet" href="calender/win2k.css" />
    <%-- <script src="calender/jscal2.js" type="text/javascript"></script>
    <script src="calender/en.js" type="text/javascript"></script>--%>
    <style type="text/css">
        div#menu
        {
            width: 98%;
            float: left;
            margin-left: 0.8%;
        }
        div#copyright
        {
            margin: 0 auto;
            width: 80%;
            font: 11px 'Trebuchet MS';
            color: #124a6f;
            text-indent: 20px;
            padding: 40px 0 0 0;
        }
        div#copyright a
        {
            color: #4682b4;
        }
        div#copyright a:hover
        {
            color: #124a6f;
        }
        .header
        {
            width: 100%;
            height: 100px;
            float: left; /*background: url(images/header.png) no-repeat;*/
        }
        .clogo
        {
            width: 100%;
            height: 86px;
            float: left;
            margin-top: 0.7%;
        }
        img, object
        {
            max-width: 100%;
        }
        object
        {
            margin: 0 auto 1em;
            max-width: 100%;
        }
        .headermidd
        {
            width: auto;
            float: left;
            font-size: 20px;
            color: #448fb9;
            padding: 35px;
        }
        .headerleft
        {
            height: 100px;
            float: left;
            width: 1.2%;
            background: url(Images/leftheader.png) no-repeat;
            z-index: 2;
        }
        .headermiddle
        {
            height: 100px;
            float: left;
            width: 97%;
            background: url(Images/header.png) repeat-x;
        }
        .headerright
        {
            height: 100px;
            float: left;
            width: 1.8%;
            background: url(Images/rigthheader.png) no-repeat;
            z-index: 2;
        }
        .font
        {
            font-family: Calibri;
            font-size: 13px;
        }
        .button
        {
            width: 90px;
            height: 24px;
            background: #5486a6;
            cursor: pointer;
            color: #fff;
            padding: 3px;
            border: 1px Solid #5486a6;
            border-radius: 3px;
        }
        .button:hover
        {
            width: 90px;
            height: 24px;
            background: #175279;
            cursor: pointer;
            color: #fff;
            padding: 3px;
            border: 1px Solid #175279;
            border-radius: 3px;
        }
        .buttonNavigate
        {
            font-size: 15px;
            height: 25px;
            background: Silver;
            cursor: pointer;
            color: Black;
            font-weight: bold;
            cursor: pointer;
            border: 2px Solid #CCCCCC;
        }
        .buttonNavigate:hover
        {
            font-size: 15px;
            height: 25px;
            background: #CCCCCC;
            cursor: pointer;
            color: Black;
            font-weight: bold;
            cursor: pointer;
            border: 2px Solid Silver;
        }
        .buttoncommand
        {
            float: left;
            width: 50px;
            height: 20px;
            border: none;
            background: #448fb9;
            cursor: pointer;
            color: #fff;
            z-index: 22;
            border: 2px Solid #419cce;
        }
        .SiteMapBack
        {
            border-top: 1px solid #93B2CC;
            border-bottom: 1px solid #93B2CC;
            border-left: 1px solid #93B2CC;
            border-right: 1px solid #93B2CC;
            background-position-y: 100%;
            background-position: center;
            background-attachment: scroll;
            background-repeat: repeat-x;
            text-transform: capitalize;
            font-family: Calibri;
            color: Black;
            font-size: 13px;
        }
        #tblMessageEntry
        {
            border-collapse: collapse;
            width: -moz-max-content;
        }
        
        #tblMessageEntry tr
        {
        }
        
        #tblMessageEntry td
        {
            font-family: Calibri;
            font-size: 14px;
            text-align: left;
        }
    </style>       
    <link href="Styles/ui.dynatree.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/jquery.dynatree.js" type="text/javascript"></script>
    <script type="text/javascript">
        var RepTerritoryMap;
        var ManualCheckUncheck = false;
        //#region populateTree()
        // Function Name : populateTree
        // Purpose : destroy existing tree structure if present and 
        //           create new one with tree structure data present in <treeData> variable
        function PopulateRepTerritoryHierarchy() {

            var treee = null;
            try {
                $("#divRepHierarchy").dynatree('destroy');
                $('#divRepHierarchy').empty();
            } 
            catch (ex) { 
            }

            treee = $("#divRepHierarchy").dynatree({
                checkbox: true,
                selectMode: 3,
                children: RepTerritoryMap["nodeList"],
                //minExpandLevel: 2,
                onSelect: function (select, node) {

                    if (node.tree.getSelectedNodes().length > 0) {
                        $("#btnCheckUncheckAll").html("Uncheck All")
                    }
                    else {
                        $("#btnCheckUncheckAll").html("Check All")
                        if (document.getElementById("chkRM") !== null)
                            document.getElementById("chkRM").checked = false;
                        if (document.getElementById("chkZD") !== null)
                            document.getElementById("chkZD").checked = false;
                        if (document.getElementById("chkNSD") !== null)
                            document.getElementById("chkNSD").checked = false;
                        if (document.getElementById("chkMSLD") !== null)
                            document.getElementById("chkMSLD").checked = false;
                        if (document.getElementById("chkNAED") !== null)
                            document.getElementById("chkNAED").checked = false;
                        if (document.getElementById("chkOAMRegion") !== null)
                            document.getElementById("chkOAMRegion").checked = false;
                        if (document.getElementById("chkCAMRegion") !== null)
                            document.getElementById("chkCAMRegion").checked = false;
                        if (document.getElementById("chkOAMDirector") !== null)
                            document.getElementById("chkOAMDirector").checked = false;
                        if (document.getElementById("chkCAMDirector") !== null)
                            document.getElementById("chkCAMDirector").checked = false;
                    }

                    // Get a list of all selected nodes, and convert to a key array:
                    var selKeys = $.map(node.tree.getSelectedNodes(), function (node) {
                        return node.data.key;
                    });
                    // $("#echoSelection3").text(selKeys.join(", "));

                    // Get a list of all selected TOP nodes
                    var selRootNodes = node.tree.getSelectedNodes(true);
                    // ... and convert to a key array:
                    var selRootKeys = $.map(selRootNodes, function (node) {
                        return node.data.key;
                    });
                    //$("#echoSelectionRootKeys3").text(selRootKeys.join(", "));
                    // $("#echoSelectionRoots3").text(selRootNodes.join(", "));                    
                },
                onDblClick: function (node, event) {
                    node.toggleSelect();
                },
                onKeydown: function (node, event) {
                    if (event.which == 32) {
                        node.toggleSelect();
                        return false;
                    }
                },                
                // The following options are only required, if we have more than one tree on one page:
                //        initId: "treeData",
                cookieId: "dynatree-Cb3",
                idPrefix: "dynatree-Cb3-"
            });
            $("#btnExpandCollapseAll").click(function () {

                var ExpandCollapse = true;
                if ($("#btnExpandCollapseAll").html() == "Expand All") {

                    $("#btnExpandCollapseAll").html("Collapse All")
                    ExpandCollapse = true;
                }
                else {

                    $("#btnExpandCollapseAll").html("Expand All")
                    ExpandCollapse = false;
                }

                $("#divRepHierarchy").dynatree("getRoot").visit(function (node) {
                    node.expand(ExpandCollapse);
                });
                return false;
            });
            $("#btnCheckUncheckAll").click(function () {

                var CheckUncheck = true;
                if ($("#btnCheckUncheckAll").html() == "Check All") {

                    $("#btnCheckUncheckAll").html("Uncheck All")
                    CheckUncheck = true;
                }
                else {

                    $("#btnCheckUncheckAll").html("Check All")
                    CheckUncheck = false;
                }

                $("#divRepHierarchy").dynatree("getRoot").visit(function (node) {
                    node.select(CheckUncheck);
                });

                if (document.getElementById("chkRM") !== null)
                    document.getElementById("chkRM").checked = CheckUncheck;
                if (document.getElementById("chkZD") !== null)
                    document.getElementById("chkZD").checked = CheckUncheck;
                if (document.getElementById("chkNSD") !== null)
                    document.getElementById("chkNSD").checked = CheckUncheck;
                if (document.getElementById("chkMSLD") !== null)
                    document.getElementById("chkMSLD").checked = CheckUncheck;
                if (document.getElementById("chkNAED") !== null)
                    document.getElementById("chkNAED").checked = CheckUncheck;
                if (document.getElementById("chkOAMRegion") !== null)
                    document.getElementById("chkOAMRegion").checked = CheckUncheck;
                if (document.getElementById("chkCAMRegion") !== null)
                    document.getElementById("chkCAMRegion").checked = CheckUncheck;
                if (document.getElementById("chkOAMDirector") !== null)
                    document.getElementById("chkOAMDirector").checked = CheckUncheck;
                if (document.getElementById("chkCAMDirector") !== null)
                    document.getElementById("chkCAMDirector").checked = CheckUncheck;

                return false;
            });
            //#endregion
        }        
    </script>    
    <script src="ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="ckeditor/ckeditor_basic.js" type="text/javascript"></script>
    <script src="ckeditor/config.js" type="text/javascript"></script>    
    <link href="Scripts/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/jquery.datetimepicker.js" type="text/javascript"></script> 
    <script type="text/javascript">

        $(document).ready(function () {
            var descID = document.getElementById("txtMessage");
            CKEDITOR.replace(descID,
            {
                width: 600,
                height: 130
            });

            $('#txtSendDateTime').datetimepicker({
                datepicker: true,
                timepicker: true,
                format: 'm/d/Y H:i'
            });
            $('#txtSendDateTime').datetimepicker('reset');
        });
    </script>    
    <script language="javascript" type="text/javascript">
        var ResultObject = new Object();
        
        function GetInitialData() {
            
            var cookieName = getCookie("LoginUser");
            if (cookieName == null) {

                window.location.href = "Login.aspx";
            }
            else {
            
                DeGrToolWebService.GetInitialDataForNotificationNew(SetInitialData);
            }
        }

        function SetInitialData(result) {
            ResultObject = JSON.parse(result);
            if (ResultObject.Status == 1) {
                var objdata = ResultObject.dt_ReturnedTables[0];
                if (objdata != null) {
                    $.each(objdata, function (i, val) {
                        $('#ddlTargetApp').append('<option value="' + val["AppName"] + '">' + val["AppName"] + '</option>');
                    });
                }

                RepTerritoryMap = ResultObject.dt_ReturnedTables[1];
                if (RepTerritoryMap != null)
                    PopulateRepTerritoryHierarchy();                   
            }
        }

        function CheckUncheckRegion(Designation, Role, TerritoryType, Checked) {

            ManualCheckUncheck = true;

            if (Checked) {
                $("#btnCheckUncheckAll").html("Uncheck All")
            }
            else {
                $("#btnCheckUncheckAll").html("Check All")
            }

            $("#divRepHierarchy").dynatree("getRoot").visit(function (node) {

                var KeyOb = JSON.parse(node.data.key);
                if (Designation != null && KeyOb["Designation"] == Designation) {
                    node.select(Checked);
                }
                else if (TerritoryType != null && Role != null && KeyOb["TerritoryType"] == TerritoryType && KeyOb["Role"] == Role) {
                    node.select(Checked);
                }
            });

            ManualCheckUncheck = false;
        }

        function SaveManualNotification() {
            var RepIdList = GetSelectedRepId();
            if (RepIdList == null || RepIdList == '' || RepIdList.length <= 0) {
                alert("Please select at least one Rep Id.");
                return false;
            }

            var SendDateTime = document.getElementById("txtSendDateTime").value;
//            if (SendDateTime == null || SendDateTime == '' || SendDateTime.length <= 0) {
//                alert("Please select Send DateTime.");
//                document.getElementById("txtSendDateTime").focus();
//                return false;
//            }
            var strUtcSendDateTime = null;
            if (SendDateTime != null && SendDateTime != '') {
                var dSendDateTime = new Date(SendDateTime);
                var utc = dSendDateTime.getTime() + (dSendDateTime.getTimezoneOffset() * 60000);
                var utcSendDateTime = new Date(utc);
                strUtcSendDateTime = (utcSendDateTime.getMonth() + 1) + '/' + utcSendDateTime.getDate() + '/' + utcSendDateTime.getFullYear() + ' ' + utcSendDateTime.getHours() + ':' + utcSendDateTime.getMinutes() + ':' + utcSendDateTime.getSeconds();
                //alert(strUtcSendDateTime);
            }

            var TargetApp = document.getElementById("ddlTargetApp").value;
            if (TargetApp == null || TargetApp == '' || TargetApp.length <= 0) {
                alert("Please select Target App.");
                document.getElementById("ddlTargetApp").focus();
                return false;
            }

            var Sender = document.getElementById("txtSender").value;
            if (Sender == null || Sender == '' || Sender.length <= 0) {
                alert("Please enter Sender.");
                document.getElementById("txtSender").focus();
                return false;
            }

            var Subject = document.getElementById("txtSubject").value;
            if (Subject == null || Subject == '' || Subject.length <= 0) {
                alert("Please enter Subject.");
                document.getElementById("txtSubject").focus();
                return false;
            }

            var Message = CKEDITOR.instances.txtMessage.getData();
            if (Message == null || Message == '' || Message.length <= 0) {
                alert("Please Enter Message.");
                CKEDITOR.instances.txtMessage.focus();
                return false;
            }

            var AttachmentName = "";
            var AttachmentType = "";
            var AttachmentPath = "";
            var table = document.getElementById("tblAttachment");
            var rowCount = table.rows.length;
            var row = null;
            for (var i = 0; i < rowCount; i++) {
                row = table.rows[i];
                AttachmentName += $("#AttachmentName" + row.id).html() + "|";
                AttachmentType += document.getElementById("AttachmentType" + row.id).value + "|";
                AttachmentPath += document.getElementById("AttachmentPath" + row.id).value + "|";
            }

            //Call WebService.
            DeGrToolWebService.SaveMessageNew(RepIdList, strUtcSendDateTime, TargetApp, Sender, Subject, Message, AttachmentName, AttachmentType, AttachmentPath, SaveMessageSuccess);
        }

        function SaveMessageSuccess(result) {
            ResultObject = JSON.parse(result);
            if (ResultObject != null) {
                alert(ResultObject.Message);
                if (ResultObject.Status == 1) {
                    ClearData();
                }
            }
            return true;
        }

        function ClearData() {
            AttachmentRowId = 0;
            document.getElementById("txtSender").value = "";
            document.getElementById("txtSubject").value = "";
            CKEDITOR.instances.txtMessage.setData("");
            if (document.getElementById("chkRM") !== null)
                document.getElementById("chkRM").checked = false;
            if (document.getElementById("chkZD") !== null)
                document.getElementById("chkZD").checked = false;
            if (document.getElementById("chkNSD") !== null)
                document.getElementById("chkNSD").checked = false;
            if (document.getElementById("chkMSLD") !== null)
                document.getElementById("chkMSLD").checked = false;
            if (document.getElementById("chkNAED") !== null)
                document.getElementById("chkNAED").checked = false;
            if (document.getElementById("chkOAMRegion") !== null)
                document.getElementById("chkOAMRegion").checked = false;
            if (document.getElementById("chkCAMRegion") !== null)
                document.getElementById("chkCAMRegion").checked = false;
            if (document.getElementById("chkOAMDirector") !== null)
                document.getElementById("chkOAMDirector").checked = false;
            if (document.getElementById("chkCAMDirector") !== null)
                document.getElementById("chkCAMDirector").checked = false;
            $("#divRepHierarchy").dynatree("getRoot").visit(function (node) { node.select(false); });
            $("#btnCheckUncheckAll").html("Check All");
            //Clear Attachment
            var table = document.getElementById("tblAttachment");
            var rowCount = table.rows.length;
            for (var i = rowCount - 1; i >= 0; i--) {
                table.deleteRow(i);
            }
            $('#txtSendDateTime').datetimepicker('reset');
            return true;
        }

        function GetSelectedRepId() {
            var selNodes = $("#divRepHierarchy").dynatree("getSelectedNodes");
            var selKeys = $.map(selNodes, function (node) {
                return node.data.key;
            });

            // convert to title/key array
            var RepIdList = "";

            $.each(selNodes, function (i, node) {

                var KeyOb = JSON.parse(node.data.key);
                if (KeyOb["id"] != "" && KeyOb["id"] != "NA" && KeyOb["id"] != "OAM" && KeyOb["id"] != "CAM" && KeyOb["id"] != "NATION")
                    RepIdList += KeyOb["id"] + ",";
            });

            if (RepIdList != null && RepIdList != "" && RepIdList.length > 0)
                RepIdList = RepIdList.substring(0, RepIdList.length - 1);

            return RepIdList;
        }        
    </script>
    <script type="text/javascript" language="javascript">

        function RemoveAttachment(rowid) {

            var table = document.getElementById("tblAttachment");
            var rowCount = table.rows.length;
            var row = null;
            for (var i = rowCount - 1; i >= 0; i--) {
                row = table.rows[i];
                if (row.id == rowid) {
                    table.deleteRow(i);
                    break;
                }
            }
            return false;
        }

        function CreateCookie(nameP, valueP, daysP) {
            if (daysP) {
                var date = new Date();
                date.setTime(date.getTime() + (daysP * 24 * 60 * 60 * 1000));
                var expires = "; expires=" + date.toGMTString();
            }

            else
                var expires = "";

            document.cookie = nameP + "=" + valueP + expires + "; path=/";
        }

        function ReadCookie(nameP) {
            var nameEQ = nameP + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ')
                    c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        }

        function EraseCookie(nameP) {
            CreateCookie(nameP, "", -1);
        }

        function BrowseFolderForAttachment() {
            EraseCookie("AttachmentName");
            opener = $("#TB_window");


            opener = tb_show("<b>Upload File</b>", "frmAttachmentUpload.aspx?&TB_iframe=true&height=50&width=400", "");

            setTimeout("WaitForUserSubmitAttachment()", 300);
            opener = $("#TB_window");

            return false;
        }

        var AttachmentRowId = 0;
        function WaitForUserSubmitAttachment() {
            var gName = ReadCookie("AttachmentName");
            var gType = ReadCookie("AttachmentType");
            var gPath = ReadCookie("AttachmentPath");
            if (opener.length == "0" && gName != null && gType != null && gPath != null) {

                var table = document.getElementById("tblAttachment");
                var rowCount = table.rows.length;
                var row = table.insertRow(rowCount);
                row.style.backgroundColor = "White";
                row.id = AttachmentRowId;
                AttachmentRowId++;
                var cell1;
                var element1;

                //1 Attachment Name
                cell1 = row.insertCell(0);
                element1 = document.createElement("label");
                element1.id = "AttachmentName" + row.id;
                //element1.readOnly = true;
                if (gName && gName.length > 0 && gName != "*") {
                    element1.innerHTML = gName;
                }
                cell1.appendChild(element1);

                //2 Remove Attachment Image
                cell1 = row.insertCell(1);
                cell1.align = "Center"
                element1 = document.createElement("input");
                element1.id = "RemoveAttachment" + row.id;
                element1.type = "image";
                element1.src = "Images/delete2.jpg";
                element1.alt = "Remove";
                element1.style.width = "25px";
                element1.style.height = "24px";
                element1.setAttribute("onclick", "return RemoveAttachment(" + row.id + ");");
                cell1.appendChild(element1);

                //3 Attachment Type
                cell1 = row.insertCell(2);
                element1 = document.createElement("input");
                element1.id = "AttachmentType" + row.id;
                element1.readOnly = true;
                element1.type = "hidden";
                if (gType && gType.length > 0 && gType != "*") {
                    element1.value = gType;
                }
                cell1.appendChild(element1);

                //3 Attachment Path
                cell1 = row.insertCell(3);
                element1 = document.createElement("input");
                element1.id = "AttachmentPath" + row.id;
                element1.readOnly = true;
                element1.type = "hidden";
                if (gPath && gPath.length > 0 && gPath != "*") {
                    element1.value = gPath;
                }
                cell1.appendChild(element1);

                clearTimeout(timer);
                tb_remove();
            }
            else {
                opener = $("#TB_window");
                timer = setTimeout("WaitForUserSubmitAttachment()", 300);
            }
            return false;
        }

        function updated() {
            tb_remove();
            return false;
        }    
    </script>
    <script type="text/javascript" language="javascript">
        function CheckCookie() {
            var cookieName = getCookie("LoginUser");
            if (cookieName == null) {
                window.location.href = "Login.aspx";
            }
        }
        function EraseCookieOnLogout() {
            eraseCookie("LoginUser");
            window.location.href = "Login.aspx";
        }
    </script>
</head>
<body onload="Javascript:GetInitialData();">
    <form id="form1" runat="server">
    <input type="hidden" id="hidOPerationPre" />
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        <Services>
            <asp:ServiceReference Path="~/DeGrToolWebService.asmx" />
        </Services>
    </asp:ScriptManager>
    <div class="header">
        <div class="headerleft">
        </div>
        <div class="headermiddle">
            <div class="clogo">
                <img src="Images/Banner.png" alt="" width="100%" height="100%" />
            </div>
            <%--   <div id="DivHeaderMain" class="headermidd">
            </div>--%>
        </div>
        <div class="headerright">
        </div>
    </div>
    <div>
        <div id="menu" runat="server">
        </div>
    </div>
    <div style="width: 98%; margin-left: 1%; margin-top: 10px; float: left;">
        <table style="width: 100%; border: 1px Solid #E5E5E5; height: 425px;">
            <tr style="background-color: White;">
                <td colspan="2" align="center" style="border-bottom: 1px Solid #E5E5E5;">
                    <h1 style="color: #1f497d; font-family: Calibri; font-weight: bold; font-size: 24px;">
                        Manual Notification</h1>
                </td>
            </tr>
            <tr>
                <td style="width:25%;height:420px" class="SiteMapBack" valign="top">
                    <table style="width:100%">
                        <tr>
                            <td valign="top" style="width:100%;height:17px;" class="SiteMapBack" align="center" >
                                <a href="#" id="btnExpandCollapseAll">Expand All</a>&nbsp;&nbsp;                    
                                <span style="font-size: 13px; font-family: Calibri"><strong>Rep Hierarchy</strong></span>&nbsp;&nbsp;
                                <a href="#" id="btnCheckUncheckAll">Check All</a>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="width:100%;height:420px;" class="SiteMapBack">
                                <div style="width:100%; height:410px;" id="divRepHierarchy">
                                </div>
                            </td>
                        </tr>
                    </table>                    
                </td>
                <td style="width:75%;height:420px" class="SiteMapBack" valign="top">
                    <table id="tblMessageEntry" style="width:100%">
                        <tr>
                            <td style="width:12%">
                                Send DateTime:
                            </td>
                            <td> <%--style="width:30%"--%>
                                <input name="txtSendDateTime" id="txtSendDateTime" type="text" style="width:150px;"/>
                            </td>
                            <td align="left">
                                <input id="chkRM" type="checkbox" onchange="return CheckUncheckRegion('RSM', null, null, this.checked);" />RM
                            </td>
                            <%--<td align="left">
                                <input id="chkOAMDirector" type="checkbox" onchange="return CheckUncheckRegion(null, 'NSD', 'OAM', this.checked);" />OAM Director
                            </td> --%>
                        </tr>
                        <tr>
                            <td style="width:12%">
                                Target App :
                            </td>
                            <td> <%--style="width:30%"--%>
                                <select name="ddlTargetApp" id="ddlTargetApp" style="width:155px;">
                                </select>
                            </td>
                            <td align="left">
                                <input id="chkZD" type="checkbox" onchange="return CheckUncheckRegion('ZD', null, null, this.checked);" />ZD
                            </td>
                            <%--<td align="left">
                                <input id="chkCAMDirector" type="checkbox" onchange="return CheckUncheckRegion(null, 'NSD', 'CAM', this.checked);" />CAM Director
                            </td>--%>
                        </tr>
                        <tr>
                            <td style="width:12%;">
                                Sender :
                            </td>
                            <td><%--style="width:63%;"--%>
                                <input name="txtSender" type="text" id="txtSender" style="width:595px;" />
                            </td>
                            <td align="left">
                                <input id="chkNSD" type="checkbox" onchange="return CheckUncheckRegion('NSD', null, null, this.checked);" />NSD
                            </td>
                            <%--<td align="left">
                                <input id="chkOAMRegion" type="checkbox" onchange="return CheckUncheckRegion('RSD', null, null, this.checked);" />OAM Managers
                            </td>--%>
                        </tr>
                        <tr>
                            <td style="width:12%;">
                                Subject :
                            </td>
                            <td><%--style="width:63%;"--%>
                                <input name="txtSubject" type="text" id="txtSubject" style="width:595px;" />
                            </td>
                            <td align="left">
                                <input id="chkMSLD" type="checkbox" onchange="return CheckUncheckRegion('MSLD', null, null, this.checked);" />MSLD
                            </td>                            
                            <%--<td align="left">
                                <input id="chkCAMRegion" type="checkbox" onchange="return CheckUncheckRegion('ADCA', null, null, this.checked);" />CAM Managers
                            </td>--%>
                        </tr>
                        <tr>
                            <td valign="top" style="width:12%; height:130px;">
                                Message :
                            </td>
                            <td style="width:80%;">
                                <textarea name="txtMessage" rows="2" cols="20" id="txtMessage" style="height:130px;width:595px;"></textarea>
                            </td>
                            <td align="left" valign="top">
                                <input id="chkNAED" type="checkbox" onchange="return CheckUncheckRegion('NAED', null, null, this.checked);" />NAED
                            </td>                            
                        </tr>
                        <tr>                            
                            <td valign="top" style="width:12%">
                                Attachment :
                            </td>
                            <td valign="top" colspan="2" style="width:88%;">
                                <table>
                                    <tr>
                                        <td valign="top">
                                            <input id="AddAttachment" type="image" src="images/Attachement2.jpg" alt="Add" style="width:25px; height:24px" onclick="javascript:return BrowseFolderForAttachment();" />
                                        </td>
                                        <td valign="top">
                                            <table id="tblAttachment">                                    
                                            </table>
                                        </td>                            
                                    </tr>
                                </table>                                
                            </td>                            
                        </tr>
                        <tr>
                            <td style="width:12%">
                            </td>
                            <td colspan="2" style="width:88%" align="left">
                                <div>
                                    <input type="button" id="btnSave" class="button" name="btnSave" value="Save" onclick="return SaveManualNotification();" />
                                    <input type="button" id="btnClear" class="button" name="btnClear" value="Clear" onclick="return ClearData();" />                                    
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>            
        </table>
    </div>
    </form>
</body>
</html>
<script type="text/javascript">

    //Start calendar setup code
    function calendar(input, start, showtime) {
        if (showtime == "") {
            showtime = false;
            ifformats = "%m/%d/%Y"
        }
        else {
            showtime = "12"
            ifformats = "%m/%d/%Y %H:%M"
        }
        if (start == "")
            start = 20000101;
        else
            start = start + "0101";
        if (document.getElementById(input)) {
            new Calendar({
                inputField: input,
                dateFormat: ifformats,
                showTime: showtime,
                trigger: input,
                minuteStep: 1,
                min: parseInt(start),
                onSelect: function () {
                    if (showtime != "") {
                        document.getElementById(input).value = document.getElementById(input).value.substr(0, 11);
                        if (this.getHours() < 10)
                            document.getElementById(input).value = document.getElementById(input).value + "0";
                        document.getElementById(input).value = document.getElementById(input).value + this.getHours() + ":";
                        if (this.getMinutes() < 10)
                            document.getElementById(input).value = document.getElementById(input).value + "0";
                        document.getElementById(input).value = document.getElementById(input).value + this.getMinutes();
                    }
                    this.hide();
                }
            });
            new Calendar({
                inputField: input,
                dateFormat: ifformats,
                showTime: showtime,
                trigger: input,
                minuteStep: 1,
                min: parseInt(start),
                onSelect: function () {
                    if (showtime != "") {
                        document.getElementById(input).value = document.getElementById(input).value.substr(0, 11);
                        if (this.getHours() < 10)
                            document.getElementById(input).value = document.getElementById(input).value + "0";
                        document.getElementById(input).value = document.getElementById(input).value + this.getHours() + ":";
                        if (this.getMinutes() < 10)
                            document.getElementById(input).value = document.getElementById(input).value + "0";
                        document.getElementById(input).value = document.getElementById(input).value + this.getMinutes();
                    }
                    this.hide();
                }
            });
        }
    }
    //End calendar setup code
</script>
<script type="text/javascript">
    //    calendar('txtStartDate', '', '');
    //    calendar('txtEndDate', '', '');
</script>
