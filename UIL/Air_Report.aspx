﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Air.master" AutoEventWireup="true"
    CodeFile="Air_Report.aspx.cs" Inherits="Air_Report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/fixedcolumns/3.2.2/css/fixedColumns.dataTables.min.css"
        rel="stylesheet" />
      <link href="css/bootstrap-dialog.min.css" rel="stylesheet" />
    <script src="css/jquery-1.12.3.js"></script>
    <script src="css/jquery.dataTables.min.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900'
        rel='stylesheet' type='text/css' />
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <%--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>
    <script src=" https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
    <script src="https://cdn.datatables.net/fixedcolumns/3.2.2/js/dataTables.fixedColumns.min.js"></script>
    <link href="ClientScripts/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="ClientScripts/jquery-ui.1.9.1min.js" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.7/js/bootstrap-dialog.min.js"></script>
    <script src="Scripts/multiselect.js"></script>
    <link href="css/multiselect.css" rel="stylesheet" />
    <%--<link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css"
    rel="stylesheet" type="text/css" />
  
<link href="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css"
    rel="stylesheet" type="text/css" />
<script src="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"
    type="text/javascript"></script>--%>
    <script>

        $(document).ready(function () {

            //sessionStorage["username"] = 'kt';
            if (sessionStorage["username"] != null) {
                $("#username").val(sessionStorage["username"]);
                DeGrToolWebService.GetUserdata_air(sessionStorage["username"], onSuccess, onFail);

            } else {
                location.href = "Login_Air.aspx";

            }

            $(".datepicker").datepicker({
                dateFormat: 'mm/dd/yy'
            });

            $("#search").click(function (e) {
                var object = getDataForFilter();
            });

            $("#select_air_type").on('change', function (e) {
                if ($("#select_air_type").val() != null && $("#select_air_type").val().length > 0) {
                    $('#team_User').parent().find('.ms-options-wrap').css('display', 'none');
                    $('#team_User').removeAttr('multiple');
                    $('#team_User').attr('disabled', 'disabled');
                    $('#team_User').val('');
                    $('#team_User').css('display', '');
                }
                else {
                    $('#team_User').css('display', 'none');
                    $('#team_User').attr('multiple', 'multiple');
                    $('#team_User').parent().find('.ms-options-wrap').css('display', '');
                    $('#team_User').val("");
                }
            });
        });//ready end

        function ConvertArrayToString(array) {
            var str = "";
            if (array != null) {
                if (array.length != 0) {
                    for (var i = 0 ; i < array.length; i++) {
                        if (i < array.length - 1) {
                            if (array[i] != "") {
                                str += '\'' + array[i] + '\',';
                            }
                        }
                        else {
                            if (array[i] != "") {
                                str += '\'' + array[i] + '\'';
                            }
                        }
                    }
                }
            }
            return str;
        }

        function getDataForFilter() {
            var selcetedairStatus = $("#select_air_type").val();
            var Status = $("#airStatus").val();
            var fromDate = $("#fromDate").val();
            var toDate = $("#toDate").val();
            var selectUser = $("#Drop_user").val();
            var selectteam = $("#team_User").val();

            //console.log(ConvertArrayToString(selcetedairStatus));
            //console.log(ConvertArrayToString(Status));
            //console.log(fromDate);
            //console.log(toDate);
            $("#loading").show();
            DeGrToolWebService.GetFilteredAirRpts($("#username").val(), ConvertArrayToString(selcetedairStatus), ConvertArrayToString(Status), fromDate, toDate, ConvertArrayToString(selectUser), ConvertArrayToString(selectteam), FillResult, onFail);
        }
        function FillResult(data) {
            debugger;
            var jsontable = $.parseJSON(data);
            if (jsontable["Status"] == 1) {
                console.log(" sucessfull");
                console.log(jsontable);
                if (jsontable["dt_ReturnedTables"][0].length != 0) {

                    var table = "<table class='table table-hover cust_table' id='tbl_air' ><thead> <tr class='cust_table_header'><th>Status </th> <th>Case No</th><th>Account ID </th><th>Account Name</th><th id='tbl_issue_type'>Issue Type </th><th>Created By </th><th>Created Date</th><th>Assigned To</th> <th>Territory Details</th><th>Region Details</th>  </thead>  <tbody>";
                    for (i = 0; i < jsontable["dt_ReturnedTables"][0].length; i++) {

                        var row = "<tr>";
                        //row += '<td>' + jsontable["dt_ReturnedTables"][0][i]["CaseNum"] + '</td>' + '<td>' + jsontable["dt_ReturnedTables"][0][i]["ZSPID"] + '</td>' + '<td>' + jsontable["dt_ReturnedTables"][0][i]["AccountName"] + '</td>' + '<td>' + jsontable["dt_ReturnedTables"][0][i]["AIRType"] + '</td>' + '<td>' + jsontable["dt_ReturnedTables"][0][i]["RepId"] + '</td>' + '<td>' + jsontable["dt_ReturnedTables"][0][i]["name"] + '</td>' + '<td>' + jsontable["dt_ReturnedTables"][0][i]["Phone_number"] + '</td>' + '<td><select class="cls_status_air">' + stutusbind(); + '</select></td>';

                        //row += '<td> <span class= "lbl" style="';
                        row += '<td>';

                        //if (jsontable["dt_ReturnedTables"][0][i]["Status"] == "Closed") {
                        //    row += ' background-color: #337ab7;padding: 0.2em 2.5em .3em;';
                        //}

                        //else if (jsontable["dt_ReturnedTables"][0][i]["Status"] == "InProgress") {
                        //    row += 'background-color: #5cb85c;padding: 0.2em 1.6em .2em;';
                        //}
                        //else if (jsontable["dt_ReturnedTables"][0][i]["Status"] == "Open") {
                        //    row += 'background-color: #f0ad4e;padding: 0.2em 3.0em .3em;';
                        //}

                        //row += '">' + jsontable["dt_ReturnedTables"][0][i]["Status"] + '</span></td>';
                        row += jsontable["dt_ReturnedTables"][0][i]["ModifiedStatus"] + '</td>';

                        //'<td><button type="button" class="cls_view_data btn btn-info" onclick="view_popup(this)" data-university-id="' + jsontable["dt_ReturnedTables"][0][i]["CaseNum"] + '" style="cursor: pointer;" id=' + jsontable["dt_ReturnedTables"][0][i]["CaseNum"] + '>' + "View" + '</button></td>'


                        row += '<td class="">' + jsontable["dt_ReturnedTables"][0][i]["CaseNum"] + '</td>' + '<td>' + jsontable["dt_ReturnedTables"][0][i]["ZSPID"] + '</td>' +
                            '<td>' + jsontable["dt_ReturnedTables"][0][i]["AccountName"] + '</td>' + '<td>' + jsontable["dt_ReturnedTables"][0][i]["Description"] + '</td>' +
                            '<td>' + jsontable["dt_ReturnedTables"][0][i]["RepId"] + '</td>'
                        row += '<td>' + jsontable["dt_ReturnedTables"][0][i]["ModifiedCreatedDate"] + ' </td ><td>';
                        if ((jsontable["dt_ReturnedTables"][0][i]["Assinged_To"]) == null) {
                            row += "-";
                        } else {
                            row += jsontable["dt_ReturnedTables"][0][i]["Assinged_To"];
                        }
                        row += '</td>';
                        row += '<td>' + jsontable["dt_ReturnedTables"][0][i]["TerritoryName"] + '</td>';
                        row += '<td>' + jsontable["dt_ReturnedTables"][0][i]["RegionName"] + '</td>';

                        row += '</tr>';
                        table += row;
                    }
                    table += '</tbody></table>';
                    $('#airtable').html(table);
                    if (!$.fn.DataTable.isDataTable("#tbl_air")) {
                        oTable = $('#tbl_air').DataTable({


                            "scrollX": true,
                            "bLengthChange": false,
                            "pageLength": 10,
                            "bFilter": true,
                            //"fixedColumns": {
                            //    leftColumns: 6
                            //},
                            "columnDefs": {

                                className: "dt-right"
                                //className: "dt-body-right"
                            },
                            "dom": 'Bfrtip',
                            buttons: [
                                  'excel', {
                                      extend: 'pdf',
                                      orientation: 'landscape',
                                      pageSize: 'LEGAL',

                                  }
                            ]
                        });
                    }

                    //var width1 = $(".dataTables_scrollHeadInner").outerWidth();

                    //$(".dataTables_scrollBody").css({ 'width': width1 });

                    $("#tbl_issue_type").trigger('click');

                    $('#tbl_air thead th').css('text-align', 'left');
                    $('#tbl_air thead th').css('padding-left', '2px');
                    $('#tbl_air tbody tr').each(function () {
                        var hdn_status = $(this).find('.cls_hdn_status_air').val();
                        $(this).find('.cls_drp_status_air').val(hdn_status);



                        //if (hdn_status == "Closed") {
                        //    $('.cls_drp_status_air').attr("disabled", true);
                        //    $('.cls_drp_assingby').attr("disabled", true);
                        //}

                        var hdn_assing = $(this).find('.cls_hdn_assign').val();
                        if (hdn_assing == "null") {
                            $(this).find('.cls_drp_assingby').val("*");

                        } else {
                            $(this).find('.cls_drp_assingby').val(hdn_assing);
                        }
                        //if (jsontable["dt_ReturnedTables"][0][0]["Assinged_to"] != "undefined") {
                        //    $(this).find('.cls_drp_assingby').val(hdn_ass_to);
                        //} 
                        //if (dropdowdisbale == 'N') {
                        //    $('.cls_drp_status_air').attr("disabled", true);
                        //    $('.cls_drp_assingby').attr("disabled", true);
                        //} else {
                        //    $('.cls_drp_status_air').attr("disabled", false);
                        //    $('.cls_drp_assingby').attr("disabled", false);
                        //}

                    });
                }
            }
            else {
                $('#airtable').html("<p style='padding-left: 10px;align-content: center;'><b><align:center>No Data Found </p>");
                $("#loading").hide();
            }

            $("#loading").hide();

        }



        function onSuccess(data) {
            debugger;
            var json = $.parseJSON(data);
            if (json["Status"] == 1) {
                //$('#username').val(json["dt_ReturnedTables"][0][0]["user_id"]);
                //$('#type').val(json["dt_ReturnedTables"][0][0]["AccessIssuetypeid"]);


                DeGrToolWebService.getAir(sessionStorage["username"], $('#select_air_type').val(), dropdown);
                //DeGrToolWebService.getAir('', $('#select_air_type').val(), dropdown);

                //$(".sh").hide();

                // DeGrToolWebService.GetAirwisedata($('#username').val(), $('#type').val(), datatableset);

            } else {

                $("#loading").hide();
                BootstrapDialog.show({

                    title: 'Message',
                    message: "You don't have rights to any Access Issue Type",

                    buttons: [
                      {
                          label: 'Close',
                          cssClass: 'btn btn-rounded  btn-primary',
                          action: function (dialogItself) {
                              dialogItself.close();

                              return false;
                          }
                      }]
                });
                //location.href = "Login_Air.aspx";
            }
        }

        function onFail(error) {

        }

        function dropdown(data) {

            var result_course = JSON.parse(data);
            //$("label[for='open']").text(result_course["dt_ReturnedTables"][1][0]["total_open"]);
            ////$('#open').val(result_course["dt_ReturnedTables"][1][0]["total_open"]);
            //$('#pro').text(result_course["dt_ReturnedTables"][1][0]["total_Progress"]);

            //$('#closed').text(result_course["dt_ReturnedTables"][1][0]["total_Closed"]);


            //$("#select_air_type").append($('<option></option>', {
            //    value: "",
            //    text: "Select Issue Type",
            //}));
            for (var i = 0; i < result_course["dt_ReturnedTables"][0].length; i++) {

                $("#select_air_type").append($('<option></option>', {
                    //value: result_course["dt_ReturnedTables"][0][i]["AccessIssueTypeId"],
                    //text: result_course["dt_ReturnedTables"][0][i]["Description"],
                    value: result_course["dt_ReturnedTables"][0][i]["AccessIssuetypeid"],
                    text: result_course["dt_ReturnedTables"][0][i]["Description"],
                }));
            }

            //$("#select_air_type").multiselect({
            //    enableFiltering: true,
            //    filterBehavior: 'value'
            //});

            $("#select_air_type").val('');
            $("#select_air_type").attr('multiple', 'multiple');
            $("#select_air_type").multiselect({
                columns: 1,
                placeholder: 'Select Type',
                selectAll: true

            });

            ////split data
            var arr = [];
            var res;
            for (var i = 0; i < result_course["dt_ReturnedTables"][2].length; i++) {
                res = result_course["dt_ReturnedTables"][2][i]["PersonAssignedTo"].split("~");
                for (var j = 0; j < res.length; j++) {
                    arr.push(res[j]);

                }

            }

            //$("#Drop_user").append($('<option></option>', {

            //    value: "",
            //    text: "Select User",
            //}));

            for (var i = 0; i < arr.length; i++) {

                $("#Drop_user").append($('<option></option>', {

                    value: arr[i],
                    text: arr[i],
                }));
            }
            $("#Drop_user").val('');
            $("#Drop_user").attr('multiple', 'multiple');
            $("#Drop_user").multiselect({
                columns: 1,
                placeholder: 'Select User',
                selectAll: true
            });
            //$("#team_User").append($('<option></option>', {

            //    value: "",
            //    text: "Select Team",
            //}));

            for (var i = 0; i < result_course["dt_ReturnedTables"][3].length; i++) {

                $("#team_User").append($('<option></option>', {

                    value: result_course["dt_ReturnedTables"][3][i]["AccessIssueTeam"],
                    text: result_course["dt_ReturnedTables"][3][i]["AccessIssueTeam"],
                }));
            }

            $("#team_User").val('');
            $("#team_User").attr('multiple', 'multiple');
            $("#team_User").multiselect({
                columns: 1,
                placeholder: 'Select Team',
                selectAll: true
            });

           // $("#team_User").attr("disabled", true);

            //$("airStatus").append($('<option></option>', {
            //    value: "admin",
            //    text: "ALL",
            //}));
            var array = new Object();
            array = [{ value: 'Inprogress', text: 'In Progress' }, { value: 'Closed', text: 'Closed' }, { value: 'Open', text: 'Open' }];
            for (var i = 0; i < array.length; i++) {

                $(".airStatus").append($('<option></option>', {
                    value: array[i]["value"],
                    text: array[i]["text"]
                }));
            }
            $(".airStatus").val('');
            $(".airStatus").attr('multiple', 'multiple');
            $(".airStatus").multiselect({
                //columns: 1,
                placeholder: 'Select Status',
                selectAll: true

            });

            //DeGrToolWebService.GetAirwisedata($('#username').val(), 'admin', datatableset);

            // $("#select_air_type").change();

        }

    </script>
    <div class="container  custom">
        <div class="form-grids row widget-shadow" data-example-id="basic-forms">
            <div class="form-title">
                <h4>Filter Option:</h4>
            </div>
            <div class="form-body">
                <div class="row">
                    <div class="form-group col-lg-2">
                        <select id="select_air_type" style="width: 100%">
                            <%--   <option>1</option>
                            <option>2</option>
                            <option>3</option>--%>
                        </select>
                    </div>
                    <div class="form-group col-lg-2">
                        <input type="text" class="inputform datepicker" style="width: 100%" id="fromDate"
                            placeholder="From Date" />
                    </div>
                    <div class="form-group col-lg-2">
                        <input type="text" class="inputform datepicker" style="width: 100%" id="toDate" placeholder="To Date" />
                    </div>
                    <div class="form-group col-lg-2">
                        <select class="inputform airStatus" id="airStatus">
                            <%--<option value="">Type</option>--%>
                        </select>
                    </div>
                    <div class="form-group col-lg-2">
                        <select id="Drop_user" style="width: 100%">
                        </select>
                    </div>
                    <div class="form-group col-lg-2">
                        <select id="team_User" style="width: 100%">
                        </select>
                    </div>
                    <div class="form-group col-lg-2">
                        <input type="button" class="button  fadeInRight cust_btn1" value="Filter" id="search" />
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="container  custom">
        <div class="form-grids row widget-shadow" data-example-id="basic-forms">
            <div class="form-title">
                <h4>Result:</h4>
            </div>
            <div class="form-body">
                <div id="airtable">
                </div>
            </div>
        </div>
    </div>
    <div id="loading" class="ajax-loading" style="height: 100%; width: 100%; vertical-align: middle; text-align: center">
        <img src="Images_air/gif-load.gif" style="margin-top: 25%" alt="Loading please wait......" />
    </div>
    <input type="hidden" id="hnd_caseid" runat="server" clientidmode="Static" />
    <input type="hidden" id="username" />
    <input type="hidden" id="type" />
    <style>
        div.ajax-loading {
            position: fixed;
            top: 0;
            left: 0;
            z-index: 9999999;
            background-color: white;
            opacity: 0.7;
            filter: alpha(opacity=70); /* ie */
            -moz-opacity: 0.7; /* mozilla */
            display: none;
        }

            div.ajax-loading * {
                background-color: white;
                background-position: center center;
                background-repeat: no-repeat;
                opacity: 1;
                filter: alpha(opacity=100); /* ie */
                -moz-opacity: 1; /* mozilla */
            }

        .form-group {
            margin-bottom: 15px;
        }

        .forms h4 {
            font-size: 1.3em;
            color: #6F6F6F;
        }

        .form-title {
            padding: 1em 2em;
            background-color: #EAEAEA;
            border-bottom: 1px solid #D6D5D5;
        }

        .widget-shadow {
            background-color: #fff;
            box-shadow: 0 -1px 3px rgba(0,0,0,.12), 0 1px 2px rgba(0,0,0,.24);
            -webkit-box-shadow: 0 -1px 3px rgba(0,0,0,.12), 0 1px 2px rgba(0,0,0,.24);
            -moz-box-shadow: 0 -1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);
        }

        .inputform {
            display: inline-block;
            padding: 10px 20px; /* border-radius: 20px; */
            border: 2px solid #dedfe2; /* font-family: inherit; */ /* font-family: Verdana; */ /* font-size: 14px; */
            font-size: 1em;
            font-weight: 300;
            width: 100%;
            outline: none;
            -webkit-transition: .3s ease;
            transition: .3s ease;
            line-height: normal;
            margin: 0;
            vertical-align: baseline;
            box-sizing: border-box;
            border-top: 0px;
            border-left: 0px;
            border-right: 0px;
        }




        @media screen and (min-width: 480px) {
            body {
                font-family: "Roboto", sans-serif, Helvetica, Arial, sans-serif;
            }
        }

        /*07*/
        .t1 {
            border: 1px solid black;
        }

        .leftside {
            margin-top: 10PX; /*margin-left: 100px;*/
        }

        .headerbar {
            background-color: gray;
            padding-left: 0px;
            padding-right: 0px;
            color: white; /*margin-top: 25px;*/
            padding: 10px;
            border-bottom: 10px solid #66a634; /*nirav*/
        }

        .header_modal {
            background-color: gray;
            text-align: center;
        }


        .Que {
            /*font-weight: bold;*/
            font-size: 13px;
            color: #0c0c0c;
            width: 60%;
            vertical-align: top;
        }

        .ans {
            padding-left: 10px;
            width: 40%;
        }

        .he {
            font-size: 15px;
        }

        .abc {
            font-size: 15px;
        }

        .custom {
            margin-top: 10px;
            border: 1px solid rgba(0, 0, 0, 0.48);
        }

        .cust_table {
            margin-left: 24px !important;
            text-align: center;
        }

            .cust_table .cust_table_header {
                width: 99% !important;
                padding: 0px 14px 3px 0px;
            }

        .cust_table_header {
            font-family: "Roboto", sans-serif, Helvetica, Arial, sans-serif;
        }

        .cust_td {
            text-align: center;
            padding-right: 50px !important;
        }

        .cust_search {
            line-height: 29px;
            border-right: 0 !important;
            border-top: 0 !important;
            border-left: 0 !important;
        }

        .dt-buttons {
            padding: 0px !important; /*margin: -60px;*/
        }

        #tbl_air_filter {
            margin: -28px 15px 0px 0px !important;
        }

            #tbl_air_filter > label {
                /*margin: -28px 15px 0px 0px !important;*/
                font-size: 15px;
            }

        .modal-header {
            padding: 0px 0px !important;
        }

        .cust_btn1 {
            display: inline-block;
            font-weight: 400;
            line-height: 0.7;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .5rem 1rem;
            font-size: 1rem;
            border-radius: .25rem;
            color: #fff;
            background-color: #5cb85c;
            border-color: #5cb85c;
            margin-top: 8px;
            margin-left: 10px;
        }

        .btn-info {
            color: #fff;
            background-color: hsla(49, 17%, 16%, 0.53);
            border-color: #5bc0de;
            display: inline-block;
            font-weight: 400;
            line-height: 1.25;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .5rem 1rem;
            font-size: 1rem;
            border-radius: .25rem;
            padding: .25rem .5rem;
            font-size: .875rem;
            border-radius: .2rem;
        }

        .cust_btn {
            color: #fff;
            background-color: #5cb85c;
            border-color: #5cb85c;
            display: inline-block;
            font-weight: 400;
            line-height: 1.25;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .5rem 1rem;
            font-size: 1rem;
            border-radius: .25rem;
            padding: .25rem .5rem;
            font-size: .875rem;
            border-radius: .2rem;
        }

        .cust_select {
            padding-bottom: 6px;
            padding-top: 4px;
            width: 83%;
        }

        .status_text {
            font-family: "Roboto", sans-serif, Helvetica, Arial, sans-serif;
            font-size: 16px;
            font-weight: bold;
            margin-right: 10px;
        }

        .status_circle_text {
            font-family: "Roboto", sans-serif, Helvetica, Arial, sans-serif;
            font-size: 16px;
            color: white;
            display: inline-block;
            min-width: 10px;
            padding: 3px 16px;
            font-size: 12px;
            font-weight: 700;
            line-height: 1;
            color: #000;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            background-color: hsl(0, 0%, 100%);
            border-radius: 3px;
        }

        .closed_text {
            margin-top: 10px;
            position: absolute;
            top: 0px;
            margin-left: 402px;
        }

        .model_font {
            font-family: "Roboto", sans-serif, Helvetica, Arial, sans-serif;
            line-height: 24px;
        }

        .modal_save {
            display: inline-block;
            font-weight: 400;
            line-height: 0.7;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .5rem 1rem;
            font-size: 1rem;
            border-radius: .25rem;
            color: #fff;
            background-color: #5cb85c;
            border-color: #5cb85c;
            margin: 8px 0px 0px -93px;
        }

        .modal_close {
            display: inline-block;
            font-weight: 400;
            line-height: 0.7;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .5rem 1rem;
            font-size: 1rem;
            border-radius: .25rem;
            color: #fff;
            background-color: #d9534f;
            border-color: #d43f3a;
            margin: 8px 0px 0px -93px;
        }

        #logout {
            color: white;
            background-color: rgba(0,0,0,0.3); /* border-color: #5cb85c; */
            width: 85px;
            padding: 7px !important;
            margin-top: 18px;
            border-radius: 7px;
        }

        #tbl_air_info {
            padding-left: 11px;
        }

        .buttons-html5 {
            color: #fff;
            background-color: #286090;
            border-color: #204d74;
            text-decoration: none;
            display: inline-block;
            font-weight: 400;
            line-height: 0.7;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .5rem 1rem;
            font-size: 1rem;
            border-radius: .25rem;
            color: #fff;
            background-color: #4f81bd;
            border-color: #4f81bd; /*margin-left: 329px;
            margin-top: -126px;*/
        }

        .lbl {
            display: inline;
            font-size: 75%;
            font-weight: 700;
            line-height: 1;
            color: #fff;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: .25em;
        }

        th, td {
            white-space: nowrap;
        }

        .table.dataTable tr.odd {
            background-color: #f2f2f2 !important;
        }

        table.dataTable tr.even {
            background-color: #E0DFDD !important;
        }

        h4 {
            font-size: 1rem;
            -webkit-margin-before: 0px;
            -webkit-margin-after: 0px;
            margin: 0 !important;
        }

        .ms-options > ul {
            list-style: none;
            padding: 0px;
            margin: 0px !important;
            background-color: white;
        }

        .ms-option > lable {
            padding-top: 0px !important;
            padding-bottom: 0px !important;
            padding-right: 0px !important;
        }

        .ms-options-wrap > button .ms-options-wrap > button:focus {
            display: inline-block;
            padding: 10px 20px; /* border-radius: 20px; */
            border: 2px solid #dedfe2;
            font-size: 1em;
            font-weight: 300;
            width: 70%;
            outline: none;
            -webkit-transition: .3s ease;
            transition: .3s ease;
            line-height: normal;
            margin: 0;
            vertical-align: baseline;
            box-sizing: border-box;
            border-top: 0px;
            border-left: 0px;
            border-right: 0px;
        }

        .ms-options-wrap > .ms-options > ul > li {
            float: none;
        }

        .ms-options-wrap > .ms-options > ul label {
            padding-bottom: 0px;
            padding-right: 0px;
            padding-top: 0px;
        }

        .ms-options-wrap > button:focus, .ms-options-wrap > button {
            display: inline-block;
            padding: 10px 20px; /* border-radius: 20px; */
            border: 2px solid #dedfe2; /* font-family: inherit; */ /* font-family: Verdana; */ /* font-size: 14px; */
            font-size: 1em;
            font-weight: 300;
            width: 100%;
            outline: none;
            -webkit-transition: .3s ease;
            transition: .3s ease;
            line-height: normal;
            margin: 0;
            vertical-align: baseline;
            box-sizing: border-box;
            border-top: 0px;
            border-left: 0px;
            border-right: 0px;
        }

        .form-title {
            background-color: gray !important;
        }

        h4 {
            color: white !important;
        }

        .buttons-html5 {
            margin-left: 10px !important;
        }
         .modal-dialog {
            background-color: rgb(218, 213, 213) !important;
            color: #181717 !important;
        }

        .bootstrap-dialog-footer-buttons {
            background-color: #808080;
        }

        .bootstrap-dialog.type-primary .modal-header {
            background-color: #808080;
        }

        .modal-footer {
            background-color: #808080 !important;
        }

        .bootstrap-dialog-title {
            padding-left: 12px !important;
        }
    </style>
</asp:Content>
