﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="printData.aspx.cs" Inherits="printData" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="css/jquery-1.12.3.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.12.0/semantic.min.css" />

    <script type="text/javascript" src="//cdn.rawgit.com/MrRio/jsPDF/master/dist/jspdf.min.js">
    </script>
    <script type="text/javascript" src="//cdn.rawgit.com/niklasvh/html2canvas/0.5.0-alpha2/dist/html2canvas.min.js">
    </script>

    <script type="text/javascript">


        $(document).ready(function () {
            debugger;

            ///  var username = getParameterByName('username');
            var caseid = getParameterByName('caseid');

            DeGrToolWebService.GetAirPortalDataPrint(caseid, databindview);


        });

        function databindview(data) {

            debugger;
            var str = "";
            var pe = "";
            var desc = "";

            var jsontable = $.parseJSON(data);
            if (jsontable["Status"] == "1") {

                $('#CaseNo').text(jsontable["dt_ReturnedTables"][0][0]["CaseNum"]);
                $('#accid').text(jsontable["dt_ReturnedTables"][0][0]["ZSPID"]);
                $('#Accname').text(jsontable["dt_ReturnedTables"][0][0]["AccountName"]);
                $('#open').text(jsontable["dt_ReturnedTables"][0][0]["OpenDate"]);
                if (jsontable["dt_ReturnedTables"][0][0]["CompleteBy"] == null) {
                    $('#cre_By').text("-");
                 } else {
                    $('#cre_By').text(jsontable["dt_ReturnedTables"][0][0]["CompleteBy"]);
                }

                if (jsontable["dt_ReturnedTables"][0][0]["InternalMessage"] == null || jsontable["dt_ReturnedTables"][0][0]["InternalMessage"] == "") { } else {
                    //$('#comment').text(jsontable["dt_ReturnedTables"][0][0]["InternalMessage"]);
                    var str = jsontable["dt_ReturnedTables"][0][0]["InternalMessage"];
                    str = str.replace(/<br\/>/g, "</div><div>");
                    str = '<div>' + str;
                    str = str.substring(0, str.length - 5);
                    $('#comment').html(str);

                }
                if (jsontable["dt_ReturnedTables"][0][0]["Message"] == null || jsontable["dt_ReturnedTables"][0][0]["Message"] == "") {
                } else {
                    //$('#message').text(jsontable["dt_ReturnedTables"][0][0]["Message"]);
                    $('#message').html("<p id=select_pic>" + jsontable["dt_ReturnedTables"][0][0]["Message"] + "</p>");
                }


                $('#rem').html(jsontable["dt_ReturnedTables"][0][0]["ActionItem_Desp"]);
                $('#title_issue').html("Issue Type: " + jsontable["dt_ReturnedTables"][0][0]["AIRType"]);
                if (jsontable["dt_ReturnedTables"][0][0]["QuestionId"] == null || jsontable["dt_ReturnedTables"][0][0]["QuestionId"] == "") {

                    $('#issue').css("display", "none");
                } else {

                    $('#issue').css("display", "block");
                    var strtable = '<table  width=100%>';
                    for (var i = 0; i < jsontable["dt_ReturnedTables"][1].length; i++) {

                        strtable += '<ol>';
                        strtable += '<tr>';

                        strtable += '<td style="vertical-align: top;"><li></li></td><td class="Que">' + jsontable["dt_ReturnedTables"][1][i]["QuestionDescription"] + ":" + '</td>';




                        if ((jsontable["dt_ReturnedTables"][1][i]["Value"]).trim().length == 0) { strtable += '<td class="ans" style="vertical-align: top;">' + "-" + '</td>'; } else {
                            strtable += '<td class="ans" style="vertical-align: top;">' + jsontable["dt_ReturnedTables"][1][i]["Value"] + '</td>';
                        }

                        strtable += '</tr>';


                    }
                    strtable += '</table >';

                    $('#issue_put').html(strtable);
                }

                //for (var j = 0; j < 3; j++) {
                //    str += '<li>' + "Recommend the office perform a manual product addition if possible using the ZS-9 PI" + '</li>';


                //}
                var repstr = jsontable["dt_ReturnedTables"][0][0]["RecomemededAction_Desp"];

                var res = repstr.replace(/~/g, "<br/>");

                // alert(res);
                $("#uldata").html(res);


                if (jsontable["dt_ReturnedTables"][0][0]["EscalationId"] == null) {
                    $('#escalation').css("display", "none");
                } else {
                    $('#escalation').css("display", "block");
                    desc += '<table  width=100%>';
                    for (var j = 0; j < jsontable["dt_ReturnedTables"][2].length; j++) {
                        desc += '<ol>';
                        desc += '<tr>';
                        desc += '<td style="vertical-align: top;"><li></li></td><td class="Que">' + jsontable["dt_ReturnedTables"][2][j]["QuestionDescription"] + ":" + '</td>';
                        if ((jsontable["dt_ReturnedTables"][2][j]["Value"]).trim().length == 0) { desc += '<td class="ans">' + "-" + '</td>'; } else {
                            desc += '<td class="ans" style="vertical-align: top;">' + jsontable["dt_ReturnedTables"][2][j]["Value"] + '</td>';
                        }
                        desc += '</tr>';
                    }
                    desc += '</table>';
                    $('#els').html(desc);

                }

                if (jsontable["dt_ReturnedTables"][0][0]["Assinged_To"] == null) { } else {

                    $("#id_assing").text(jsontable["dt_ReturnedTables"][0][0]["Assinged_To"]);
                }

                $("#id_status").text(jsontable["dt_ReturnedTables"][0][0]["Status"]);

                if (jsontable["dt_ReturnedTables"][0][0]["CompletedDateFormated"] == null || jsontable["dt_ReturnedTables"][0][0]["CompletedDateFormated"] == "") { } else {
                    $('#comdate').text(jsontable["dt_ReturnedTables"][0][0]["CompletedDateFormated"]);
                }


            }
        }

        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    </script>
    <style>
        @media screen and (min-width: 480px) {
            body {
                font-family: "Roboto", sans-serif, Helvetica, Arial, sans-serif;
            }
        }

        .t1 {
            border: 1px solid black;
        }

        .leftside {
            margin-top: 10PX; /*margin-left: 100px;*/
        }

        .headerbar {
            background-color: gray;
            padding-left: 0px;
            padding-right: 0px;
            color: white; /*margin-top: 25px;*/
            padding: 10px;
            border-bottom: 10px solid #66a634; /*nirav*/
        }

        .header_modal {
            background-color: gray;
            text-align: center;
        }

        #baseinfo {
            line-height: 12px;
            font-weight: bold;
        }

        .Que {
            /*font-weight: bold;*/
            font-size: 13px;
            color: #0c0c0c;
            width: 60%;
            vertical-align: top;
        }

        .ans {
            padding-left: 10px;
            width: 40%;
            font-size: 13px;
        }

        .he {
            font-size: 15px;
        }

        .abc {
            font-size: 15px;
        }

        .custom {
            margin-top: 27px;
            border: 1px solid rgba(0, 0, 0, 0.48);
        }

        .cust_table {
            padding: 0px 14px 3px 9px;
        }

        .cust_table_header {
            font-family: "Roboto", sans-serif, Helvetica, Arial, sans-serif;
        }

        .cust_td {
            text-align: center;
            padding-right: 50px !important;
        }

        .cust_search {
            border-right: 0 !important;
            border-top: 0 !important;
            border-left: 0 !important;
        }

        #tbl_air_filter {
            margin: -26px 15px 0px 0px !important;
        }

        .cust_btn1 {
            display: inline-block;
            font-weight: 400;
            line-height: 0.7;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .5rem 1rem;
            font-size: 1rem;
            border-radius: .25rem;
            color: #fff;
            background-color: #5cb85c;
            border-color: #5cb85c;
            margin: 8px 0px 0px -93px;
        }

        .btn-info {
            color: #fff;
            background-color: hsla(49, 17%, 16%, 0.53);
            border-color: #5bc0de;
            display: inline-block;
            font-weight: 400;
            line-height: 1.25;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .5rem 1rem;
            font-size: 1rem;
            border-radius: .25rem;
            padding: .25rem .5rem;
            font-size: .875rem;
            border-radius: .2rem;
        }

        .cust_btn {
            color: #fff;
            background-color: #5cb85c;
            border-color: #5cb85c;
            display: inline-block;
            font-weight: 400;
            line-height: 1.25;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .5rem 1rem;
            font-size: 1rem;
            border-radius: .25rem;
            padding: .25rem .5rem;
            font-size: .875rem;
            border-radius: .2rem;
        }

        .cust_select {
            padding-bottom: 6px;
            padding-top: 4px;
            width: 83%;
        }

        .status_text {
            font-family: "Roboto", sans-serif, Helvetica, Arial, sans-serif;
            font-size: 16px;
            font-weight: bold;
        }

        .status_circle_text {
            font-family: "Roboto", sans-serif, Helvetica, Arial, sans-serif;
            font-size: 16px;
            color: white;
            display: inline-block;
            min-width: 10px;
            padding: 3px 7px;
            font-size: 12px;
            font-weight: 700;
            line-height: 1;
            color: #000;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            background-color: hsl(0, 0%, 100%);
            border-radius: 3px;
        }

        .modal-body {
            max-height: 100% !important;
        }

        .closed_text {
            margin-top: 10px;
            position: absolute;
            top: 0px;
            margin-left: 402px;
        }

        .model_font {
            font-family: "Roboto", sans-serif, Helvetica, Arial, sans-serif;
            line-height: 24px;
        }

        .modal_save {
            display: inline-block;
            font-weight: 400;
            line-height: 0.7;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .5rem 1rem;
            font-size: 1rem;
            border-radius: .25rem;
            color: #fff;
            background-color: #5cb85c;
            border-color: #5cb85c;
            margin: 8px 0px 0px -93px;
        }

        .modal_close {
            display: inline-block;
            font-weight: 400;
            line-height: 0.7;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .5rem 1rem;
            font-size: 1rem;
            border-radius: .25rem;
            color: #fff;
            background-color: #d9534f;
            border-color: #d43f3a;
            margin: 8px 0px 0px -93px;
        }

        #logout {
            color: white;
            background-color: rgba(0,0,0,0.3); /* border-color: #5cb85c; */
            width: 85px;
            padding: 7px !important;
            margin-top: 18px;
            border-radius: 7px;
        }

        #tbl_air_info {
            padding-left: 11px;
        }

        .buttons-html5 {
            color: #fff;
            background-color: #286090;
            border-color: #204d74; /*display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;*/
            text-decoration: none;
            display: inline-block;
            font-weight: 400;
            line-height: 0.7;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .5rem 1rem;
            font-size: 1rem;
            border-radius: .25rem;
            color: #fff;
            background-color: #4f81bd;
            border-color: #4f81bd;
            margin-left: 410px;
            margin-top: -65px;
        }

        .DivPrint {
            padding: 71px;
            font-size: 16px;
        }

        .helper {
            display: inline-block;
            height: 100%;
            vertical-align: middle;
        }

        #web_logo {
            vertical-align: middle;
            /*height: 100%;*/
            padding: 14px;
            float: left;
        }

        #cat_logo {
            vertical-align: middle;
            margin-right: 125px;
            padding: 5px;
        }

        .frame {
            display: inline;
            white-space: nowrap;
            text-align: center;
            margin: 1em 0;
        }
    </style>
</head>
<body>
    <form runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
            <Services>
                <asp:ServiceReference Path="~/DeGrToolWebService.asmx" />
            </Services>
        </asp:ScriptManager>
    </form>
    <div class="DivPrint">
        <div class="modal-header header_modal " style="margin-bottom: 22px; height: 75px;">
            <%--<div style="margin-right: 100%;">
                <img src="Images_air/zs_pharma_logo.png" alt="Site title" class="" style="">
            </div>--%>
            <div class="frame"><span class="helper"></span>
                <img id="web_logo" src="Images_air/zs_pharma_logo.png" /></div>
            <div class="frame"><span class="helper"></span>
                <img id="cat_logo" src="Images_air/AIR.png" /></div>
        </div>
        <div class="modal-body">
            <div id="baseinfo">
                <div>
                    <label class="abc">
                        Case Number :</label>
                    <label for="CaseNo" id="CaseNo" class="abc" style="margin-right: 10%;">
                    </label>
                    <label class="abc">
                        Account Name :
                    </label>
                    <label for="Accname" class="abc" id="Accname" style="margin-right: 10%;">
                    </label>
                    <label class="abc">
                        Account ID :
                    </label>
                    <label for="accid" id="accid" class="abc">
                    </label>
                </div>

                <div style="padding-top: 10px">
                    <label class="abc" style="padding-left: 152px;">
                        Open Date :</label>
                    <label for="CaseNo" id="open" class="abc" style="margin-right: 10%;">
                    </label>
                    <label class="abc" style="padding-left: 100px;">
                        Created BY:</label>
                    <label for="Accname" class="abc" id="cre_By" style="margin-right: 10%;">
                    </label>
                </div>
            </div>
            <hr />
            <div id="textdata" style="display: none">
                <h5 style="margin-top: 10px;" class="he">
                    <b>REMINDERS</b></h5>
                <%-- <ul>
                            <li>Have the ZS-9 e-Prescribing Time Table available to see if ZS-9 should be in the office’s e-prescribing system already
                            </li>
                            <li>Confirm  with the office that ZS-9 5 g and 10 g are still NOT in system (the system may have been updated since the last time the office tried prescribing ZS-9)
                            </li>
                            <li>Ensure the office is looking for a 5 g or 10 g dose (these are the only strengths available). If needed, a dose strength override might be possible in the system.
                            </li>
                        </ul>--%>
                <p id="rem">
                </p>
            </div>
            <div id="issue">
                <h5 style="margin-top: 10px;" class="he" id="title_issue"></h5>
                <div id="issue_put">
                </div>
            </div>
            <div id="actionitem">
                <h5 style="margin-top: 10px;" class="he">
                    <b>Recommended Action Item </b>
                </h5>
                <div>
                    <p id="uldata" style="font-size: 13px;">
                    </p>
                </div>
            </div>
            <div id="escalation">
                <h5 style="margin-top: 10px;" class="he">
                    <b>Sales Representantive's Input </b>
                </h5>
                <div id="els">
                </div>
            </div>
            <div id="dropdown">
                <div style="padding-top: 15px; padding-bottom: 10px;">
                    <label for="ASSINGTO" style="padding-right: 59px;">
                        <b>Assign To:</b></label>
                    <span id="id_assing"></span>
                </div>
                <div>
                    <label for="Status" style="padding-right: 82px;">
                        <b>Status:</b></label>
                    <span id="id_status"></span>
                </div>
                <div style="padding-top: 10px;">
                    <label for="datecom" style="padding-right: 20px;"><b>Completed Date:</b></label>
                    <label for="comdate" id="comdate"></label>
                </div>
            </div>
            <div class="form-group" style="padding-top: 15px;">
                <label for="comment">
                    <b>INTERNAL NOTES : </b>
                </label>
                <br />
                <div id="comment"></div>
            </div>
            <div class="form-group" style="padding-top: 15px;">
                <label>
                    <b>NOTES : </b>
                </label>
                <%--<textarea class="form-control" id="message" style="min-height: 130px;"></textarea>--%>
                <br />
                <div id="message"></div>
            </div>
        </div>
    </div>
</body>
</html>
