﻿

function getCompanyType() {
    var Str = "";
    //for ZS
    Str = "ZS"
    //for Nuerocrine
    //Str = "Neurocrine"
    return Str;
}

function GetReportFields() {
    var ReportFields = new Array();
    switch (getCompanyType()) {
        case "ZS":
            ReportFields = JSON.parse('[{"value":0,"header":"Fieldforce ID"},{"value":1,"header":"Geography # "},{"value":2,"header":"Geography Name"},{"value":3,"header":"Role"},{"value":4,"header":"Geography Type"},{"value":5,"header":"User ID"},{"value":6,"header":"Full Name"},{"value":7,"header":"Employee Title"},{"value":8,"header":"Status"},{"value":9,"header":"Manager Name"},{"value":10,"header":"First Name "},{"value":11,"header":"Middle Name "},{"value":12,"header":"Last Name "},{"value":13,"header":"Employee ID"},{"value":14,"header":"Suffix"},{"value":15,"header":"Manager ID"},{"value":16,"header":"Shipping Address"},{"value":17,"header":"Shipping Address2"},{"value":18,"header":"Shipping City"},{"value":19,"header":"Shipping State"},{"value":20,"header":"Shipping Zip"},{"value":21,"header":"Mailing Address"},{"value":22,"header":"Mailing Address2"},{"value":23,"header":"Mailing City"},{"value":24,"header":"Mailing State"},{"value":25,"header":"Mailing Zip"},{"value":26,"header":"Office Phone"},{"value":27,"header":"Company Email"},{"value":28,"header":"Home Phone"},{"value":29,"header":"Cell Phone"},{"value":30,"header":"Personal Email"},{"value":31,"header":"Hire Date"},{"value":32,"header":"Position Start Date"},{"value":33,"header":"Position End Date"},{"value":34,"header":"Termination Date"},{"value":35,"header":"Field Start Date"},{"value":36,"header":"Employee Type"},{"value":37,"header":"Employee Status"},{"value":38,"header":"Fieldforce Name"},{"value":39,"header":"Fieldforce Start Date"},{"value":40,"header":"Fieldforce End Date"},{"value":41,"header":"Geography Start Date"},{"value":42,"header":"Geography End Date"},{"value":43,"header":"Assignment Type"},{"value":44,"header":"Sample Shipment Address "},{"value":45,"header":"Sample Shipment Address 2"},{"value":46,"header":"Sample Shipment City"},{"value":47,"header":"Sample Shipment State"},{"value":48,"header":"Sample Shipment Zip"},{"value":49,"header":"Parent Geography #"},{"value":50,"header":"Parent Geography Name"},{"value":51,"header":"2nd Level Parent Geography #"},{"value":52,"header":"2nd Level Parent Geography Name"},{"value":53,"header":"AZ Territory Number"},{"value":54,"header":"PRID"},{"value":55,"header":"AZ Employee ID"},{"value":56,"header":"AZ Email ID"}]');
            break;
        case "Neurocrine":
            ReportFields = JSON.parse('[{"value":0,"header":"Fieldforce ID"},{"value":1,"header":"Geography # "},{"value":2,"header":"Geography Name"},{"value":3,"header":"Role"},{"value":4,"header":"Geography Type"},{"value":5,"header":"User ID"},{"value":6,"header":"Full Name"},{"value":7,"header":"Employee Title"},{"value":8,"header":"Status"},{"value":9,"header":"Manager Name"},{"value":10,"header":"First Name "},{"value":11,"header":"Middle Name "},{"value":12,"header":"Last Name "},{"value":13,"header":"Employee ID"},{"value":14,"header":"Suffix"},{"value":15,"header":"Manager ID"},{"value":16,"header":"Shipping Address"},{"value":17,"header":"Shipping Address2"},{"value":18,"header":"Shipping City"},{"value":19,"header":"Shipping State"},{"value":20,"header":"Shipping Zip"},{"value":21,"header":"Mailing Address"},{"value":22,"header":"Mailing Address2"},{"value":23,"header":"Mailing City"},{"value":24,"header":"Mailing State"},{"value":25,"header":"Mailing Zip"},{"value":26,"header":"Office Phone"},{"value":27,"header":"Company Email"},{"value":28,"header":"Home Phone"},{"value":29,"header":"Cell Phone"},{"value":30,"header":"Personal Email"},{"value":31,"header":"Hire Date"},{"value":32,"header":"Position Start Date"},{"value":33,"header":"Position End Date"},{"value":34,"header":"Termination Date"},{"value":35,"header":"Field Start Date"},{"value":36,"header":"Employee Type"},{"value":37,"header":"Employee Status"},{"value":38,"header":"Fieldforce Name"},{"value":39,"header":"Fieldforce Start Date"},{"value":40,"header":"Fieldforce End Date"},{"value":41,"header":"Geography Start Date"},{"value":42,"header":"Geography End Date"},{"value":43,"header":"Assignment Type"},{"value":44,"header":"Sample Shipment Address "},{"value":45,"header":"Sample Shipment Address 2"},{"value":46,"header":"Sample Shipment City"},{"value":47,"header":"Sample Shipment State"},{"value":48,"header":"Sample Shipment Zip"},{"value":49,"header":"Parent Geography #"},{"value":50,"header":"Parent Geography Name"},{"value":51,"header":"2nd Level Parent Geography #"},{"value":52,"header":"2nd Level Parent Geography Name"}]');
            break;
    }
    return ReportFields;
}

function GetHidenFieldsForTerritoryEdit() {
    var ReportFields = new Array();
    switch (getCompanyType()) {
        case "ZS":
            ReportFields = ['terr_salesforece_row', 'terr_accc_crtd_row', 'terr_acc_crtd_longitude_row', 'terr_rap_addr_longitude_row', 'terr_rap_addr_latitude_row', 'terr_zip_longitude_row', 'terr_zip_latitude_row'];;
            break;
        case "Neurocrine":
            ReportFields = ['terr_salesforece_row', 'terr_accc_crtd_row', 'terr_acc_crtd_longitude_row', 'terr_rap_addr_longitude_row', 'terr_rap_addr_latitude_row', 'terr_zip_longitude_row', 'terr_zip_latitude_row', 'terr_az_territory_no_row'];
            break;
    }
    return ReportFields;
}

function GetHidenFieldsForTerritoryAdd() {
    var ReportFields = new Array();
    switch (getCompanyType()) {
        case "ZS":
            ReportFields = ['terr_accc_crtd_row', 'terr_acc_crtd_longitude_row', 'terr_rap_addr_longitude_row', 'terr_rap_addr_latitude_row', 'terr_zip_longitude_row', 'terr_zip_latitude_row'];
            break;
        case "Neurocrine":
            ReportFields = ['terr_accc_crtd_row', 'terr_acc_crtd_longitude_row', 'terr_rap_addr_longitude_row', 'terr_rap_addr_latitude_row', 'terr_zip_longitude_row', 'terr_zip_latitude_row', 'terr_az_territory_no_row  '];
            break;
    }
    return ReportFields;
}



function GetHidenFieldsForEmployeeEdit() {
    var ReportFields = new Array();
    switch (getCompanyType()) {
        case "ZS":
            ReportFields = ['FullNamePrefered', 'FullName'];;
            break;
        case "Neurocrine":
            ReportFields = ['FullNamePrefered', 'FullName','emp_prid_row','emp_az_employee_id_row','emp_az_email_id_row'];
            break;
    }
    return ReportFields;
}

function GetHidenFieldsForEmployeeAdd() {
    var ReportFields = new Array();
    switch (getCompanyType()) {
        case "ZS":
            ReportFields = [];;
            break;
        case "Neurocrine":
            ReportFields = ['emp_prid_row', , 'emp_az_employee_id_row', 'emp_az_email_id_row'];
            break;
    }
    return ReportFields;
}