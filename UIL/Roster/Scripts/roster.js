﻿// variable declaration
var MasterData = new Object();
var ReportParameter = new Object();
var oTable;
var oTableEmployee;
var PrevieFlag = true;
var today = new Date();
//var today = Date.now();
var dd = today.getDate();
var mm = today.getMonth() + 1; //January is 0!
var visibleColumnsOrder = new Array();
var yyyy = today.getFullYear();
if (dd < 10) {
    dd = '0' + dd;
}
if (mm < 10) {
    mm = '0' + mm;
}
var RemoveEmployeeRightsFlag = true;
today = mm + '/' + dd + '/' + yyyy;
var PrimaryVacancyCode = 'permanent';
var NotAssignAsperment = false;
var CmpName = "";
var domain = "";
var EmployeeEditRights = false;
var TerritoryEditRights = false;
//varible declaration End

//====================================================== ready start ============================================================
$(document).ready(function () {

    companyType = getCompanyType();
    if (companyType == "ZS") {
        domain = "@zspharma.com";
    }
    else if (companyType == "Neurocrine") {
        domain = "@neurocrine.com"
    }
    //$('.modal-dialog .modal-body').css('overflow-y', 'auto');
    $('.modal-dialog .modal-body').css('max-height', $(window).height() * 0.8);
    $('.modal-dialog .modal-body').css('overflow', 'auto');

    $('.modal-dialog .panel-body.max-height ').css('max-height', ($(window).height() * 0.8) - 50);

    $('#ExcelPreview .modal-dialog .modal-body').css('height', $(window).height() * 0.8);
    $('#ExcelPreview.modal-dialog .panel-body.max-height ').css('height', ($(window).height() * 0.8) - 50);

    
    $('#Report').css('height', ($(window).height() - $('#Report').position()["top"]));
 
    $("input[name=address]").css('display', 'none');
    $(".modal-dialog").css('margin-top', '1%');
    $(".modal-dialog").css('margin-bottom', '1%');
    if (sessionStorage["username"] != null && sessionStorage["username"] != undefined) {
        if (sessionStorage["username"] != "") {
            sessionStorage["username"] = sessionStorage["username"].toUpperCase();
            $("#username").val(sessionStorage["username"]);
            $("#hiduserName").val(sessionStorage["username"]);
            
            $("#loading").show();
            DeGrToolWebService.GetRoasterHomePageData(sessionStorage["username"],"First", onSuccess, onFail);
            //FillStatusDropdown();
        }
        else {
            location.href = "login_roster.aspx";
        }
    }
    else {
        location.href = "login_roster.aspx";
    }
    jQuery(function () {
        jQuery('#ReportSelectedDate').datetimepicker({
            format: 'm/d/Y',
            scrollInput: false,
            closeOnDateSelect: true,
            onShow: function (ct) {
                this.setOptions({
                    maxDate: jQuery('#ReportSelectEndDate').val() ? jQuery('#ReportSelectEndDate').val() : false, formatDate: 'm/d/Y',
                })
            },
            timepicker: false
        });
        jQuery('#ReportSelectEndDate').datetimepicker({
            format: 'm/d/Y',
            scrollInput: false,
            closeOnDateSelect: true,
            onShow: function (ct) {
                this.datetimepicker('destroy');

                this.setOptions({
                    minDate: jQuery('#ReportSelectedDate').val() ? jQuery('#ReportSelectedDate').val() : false,  formatDate:  'm/d/Y',
                })
            },
            timepicker: false
        });
    });
    $(".phoneMask").mask("(999) 999-9999");

    $("#ReportSelectedDate").val(today);

    jQuery('.datepicker').datetimepicker({
        timepicker: false,
        format: 'm/d/Y',
        scrollInput: false,
        closeOnDateSelect: true
    });

    $('select').change(function (e) {
        if ($("#" + e.target.id).val() == "") {
            MakeFistSelcetSameAsPlaceHolder(e.target.id)
        }
        else {
            MakeSelcetnormal(e.target.id);
        }
    });

    $(".showdetails").click(function (e) {
        var id = $(this).data('detail');

        if ($(this).data('attr') == 'show') {
            $("#" + id).show();
            $(this).data('attr', 'hide');
            $(this).text("Show Less");
        }
        else {
            $("#" + id).hide();
            $(this).data('attr', 'show');
            $(this).text("Show More");
        }
    });

    $(".Editfield").click(function (e) {
        //debugger;
        sessionStorage["ModalName"] = $(this).data('tab');
        var modalName = $(this).data('tab');;
        if (modalName == "myModal") {
            //$("#myModalclose").trigger('click');
            //setTimeout(function () {
                //$("#myModal").hide();
                $("#myModal .close").trigger('click');
            //}, 0);
            //$("#myModal").hide();
        }
        else if (modalName == "MyEmployeeModal") {
            //$("#myEmployeeModalClose").trigger('click');
            $("#MyEmployeeModal .close").trigger('click');
        }

        var opmode = $(this).data('opmode');
        var editfieldId = $(this).data('edit-field');
        var displaypopup = $(this).data('modal');
        var id = $(this).data('add-field');
        var modaltitle = $(this).data('modaltitle');
        if (opmode == 'EditData') {
            ConvertToEditMode(editfieldId, displaypopup, this);
        }
        else {
            CustomAlert("Something goes wrong");
        }
        $("#" + id + "Title").text(modaltitle);
        
        setTimeout(function () {
            $("#" + id).modal({
                backdrop: 'static',
                keyboard: false
            });
        }, 0);

    });

    $(".close2").click(function (e) {

        if (sessionStorage["ModalName"] != '' && sessionStorage["ModalName"] != undefined) {
            var modalName = sessionStorage["ModalName"];
             $("#" + modalName).show()
            //$("#" + modalName + ' .close').trigger('click');
            //setTimeout(function () {
            //    $("#" + modalName).modal({
            //        backdrop: 'static',
            //        keyboard: false
            //    });
            //}, 0);

            
            if (modalName == "myModal") {

                DeGrToolWebService.GetRosterEmployeeData($('#username').val(), $('#current_emp_id').val(), $('#current_emp_terr').val(), $('#current_emp_salesforce').val(), SetPopupData);
            }
            else if (modalName == "MyEmployeeModal") {
                var IncludeInvalid = "";
                if ($("#IncludeInactiveEmployee").prop('checked')) {
                    IncludeInvalid = "Yes";
                }
                DeGrToolWebService.GetAllEmployeeDetails($('#username').val(), ConvertArrayToString($('#Employee_Salesforce_Id').val()), $("#Employee_Geography_ID").val(), ConvertArrayToString($("#Employee_Role").val()), ConvertArrayToString($("#Employee_Territory_Type").val()), IncludeInvalid, employeeTableSet, onFail);
                DeGrToolWebService.GetRosterEmployeeDataForEmployeePopup($('#username').val(), $('#current_emp_id').val(), SetEmployeeTabPopupData);
                DeGrToolWebService.GetRosterEmployeeType(sessionStorage["username"], FillEmployeeDropdown);
                DeGrToolWebService.GetRosterEmployeeTitle(sessionStorage["username"], FillEmployeeTitleDropdown);
                DeGrToolWebService.GetRosterTerritoryEmployeeStaus(sessionStorage["username"], FillTerritoryEmployeeStausDropdown);
            }
            sessionStorage["ModalName"] = '';
        }
        else {
        }

    });

    $("#EmployeeTab").click(function (e) {
      
        $("#tbl_employee").show();
        $("#tbl_roster").hide();
        if (!$.fn.DataTable.isDataTable("#tbl_roster")) { oTable.destroy(); }

        $("#loading").show();
        $("#CountingSection").show();
        var IncludeInvalid = "";
        if ($("#IncludeInactiveEmployee").prop('checked')) {
            IncludeInvalid = "Yes";
        }
        DeGrToolWebService.GetAllEmployeeDetails($('#username').val(), ConvertArrayToString($('#Employee_Salesforce_Id').val()), $("#Employee_Geography_ID").val(), ConvertArrayToString($("#Employee_Role").val()), ConvertArrayToString($("#Employee_Territory_Type").val()), IncludeInvalid, employeeTableSet, onFail);
    });

    $("#RosterTab").click(function (e) {
        $("#tbl_roster").show();
        $("#tbl_employee").hide();
        if ($("#tbl_employee").length > 0) {
            if (!$.fn.DataTable.isDataTable("#tbl_employee")) { oTableEmployee.destroy(); }
        }
        $("#loading").show();
        $("#CountingSection").show();
        var IncludeInvalid = "";
        if ($("#IncludeInactiveRoster").prop('checked')) {
            IncludeInvalid = "Yes";
        }
        DeGrToolWebService.GetRoasterTabelData($('#username').val(), ConvertArrayToString($('#select_Salesforce_id').val()), $("#Geography_input").val(), ConvertArrayToString($("#Territory_Type").val()), ConvertArrayToString($("#Role_filter_select").val()), today,IncludeInvalid, datatableset);
    });

    $("#ReportTab").click(function (e) {
        $("#AsOnDateRoster").trigger('click');
        $("#CountingSection").hide();
        setTimeout(function () {
            $("#report_geographyID").css('height', $("#div_report_Salesforce_ID").height());
        }, 300);

    });

    $("#sameAsHome").click(function (e) {
        if ($("#sameAsHome").prop('checked')) {
            ClearMailPopup();
            setMailSameAsAddressPopup(MasterData["EmpData"]);
        }
        else {
            ClearMailPopup();
        }
    });

    $("#AsOnDateRoster").click(function (e) {
        if ($("#AsOnDateRoster").prop('checked')) {
            
            var disabled_terr_field = ['ReportSelectEndDate']
            MakeFieldNonEditable(disabled_terr_field);
            $('#FromDateLable').html('Report as of:');
            $("#ReportSelectedDate").val(today);
            $("#ReportSelectEndDate").val('');
            $("#ToDateLabel").css('padding-right', '36px');
        }
        else {
            $('#FromDateLable').html('From Date:');
            var disabled_terr_field = ['ReportSelectEndDate']
            $("#ReportSelectedDate").val('01/01/2015');
            $("#ReportSelectEndDate").val(today);
            MakeFieldEditable(disabled_terr_field);
            $("#ToDateLabel").css('padding-right','27px');
        }
    });

    $("#SameAsMaling").click(function (e) {
        if ($("#SameAsMaling").prop('checked')) {
            ClearShippingPopup();
            setShippingSameAsAddressPopup(MasterData["EmpData"]);
        }
        else {
            ClearShippingPopup();
        }
    });

    $("#saleforce_save").click(function (e) {
        var mode = $(this).data('data-savemode');
        var SalesForceDetails = GetSalesForcePopupData(mode);
        if (SalesForceDetails == null) {
            return false;
        }
        if ($("#salesforce_end_date_add").val() != "") {
            if (sessionStorage["SalesforceEndDate"] != $("#salesforce_end_date_add").val()) {

                var newline = '\r\n';

                var Message = "Deleting this fieldforce will also remove all its geographies. " + newline +
                    " Any employee assignments to these geographies will be end-dated as of the specified date. " + newline +
                    "Do you want to proceed?"

                BootstrapDialog.show({
                    message: Message,
                    title: 'Warning!',
                    closable: false,
                    type: [BootstrapDialog.TYPE_WARNING],
                    buttons: [{
                        label: 'Proceed',
                        cssClass: 'btn-primary',
                        action: function (dialogItself) {
                            SaveSalesforceData(mode, SalesForceDetails);

                            $("#salesforceAddModal").modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                            dialogItself.close();
                        }
                    }, {
                        label: 'Cancel',
                        action: function (dialogItself) {
                            $("#salesforceAddModal").modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                            dialogItself.close();
                        }
                    }]
                });
            }
            else {
                SaveSalesforceData(mode, SalesForceDetails);
            }
        }
        else {
            SaveSalesforceData(mode, SalesForceDetails);
        }

    });

    $("#terr_save").click(function (e) {

        var TerritoryObject = new Object();
        var mode = $(this).data('data-savemode');
        TerritoryObject = GetTerritoryPopupData(mode);
        if (TerritoryObject == null) {
            return false;
        }
        if (mode != "Add") {
            if ($("#new_terr_end_date").val() != "") {
                if (sessionStorage["TerritoriedData"] != $("#new_terr_end_date").val()) {

                    var mode = $(this).data('data-savemode');
                    var newline = '\r\n';

                    var Message = "Deleting this geography will also remove all its subordinate geographies. " + newline +
                        " Any employee assignments to these geographies will be end-dated as of the specified date." + newline +
                        " Do you want to proceed?"

                    BootstrapDialog.show({
                        message: Message,
                        title: 'Warning!',
                        closable: false,
                        type: [BootstrapDialog.TYPE_WARNING],
                        buttons: [{
                            label: 'Proceed',
                            cssClass: 'btn-primary',
                            action: function (dialogItself) {
                                SaveTerritoriesDetails(mode, TerritoryObject);

                                $("#NewTerritoryAdd").modal({
                                    backdrop: 'static',
                                    keyboard: false
                                });
                                dialogItself.close();
                            }
                        }, {
                            label: 'Cancel',
                            action: function (dialogItself) {
                                $("#NewTerritoryAdd").modal({
                                    backdrop: 'static',
                                    keyboard: false
                                });
                                dialogItself.close();
                            }
                        }]
                    });
                }
                else {
                    SaveTerritoriesDetails(mode, TerritoryObject);
                }
            }
            else {
                SaveTerritoriesDetails(mode, TerritoryObject);
            }
        }
        else {
            SaveTerritoriesDetails(mode, TerritoryObject);
        }
    });

    $("#employee_save").click(function (e) {
        var EmployeeObject = new Object();
        var mode = $(this).data('data-savemode');
        var mode = $(this).data('data-savemode');
        EmployeeObject = GetEmployeePopupData(mode);
        if (EmployeeObject == null) {
            return false;
        }

        if ($("#new_emp_termination_date").val() != "") {
            if (sessionStorage["EmpEndDate"] != $("#new_emp_termination_date").val()) {

                var SalesForceDetails = new Object();

                var newline = '\r\n';

                var Message = 'All the geographies the employee is assigned to will become Vacant  effective on the ‘Termination Date’ without another employee assignment. ' +
 newline + newline +

'Are you sure you want to do this?';
                //                            
                BootstrapDialog.show({
                    message: Message,
                    title: 'Warning!',
                    closable: false,
                    type: [BootstrapDialog.TYPE_WARNING],
                    buttons: [{
                        label: 'Proceed',
                        cssClass: 'btn-primary',
                        action: function (dialogItself) {
                            EmployeeSave(mode, EmployeeObject);

                            $("#empAddModal").modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                            dialogItself.close();
                        }
                    }, {
                        label: 'Cancel',
                        action: function (dialogItself) {
                            $("#empAddModal").modal();
                            dialogItself.close();
                        }
                    }]
                });
            }
            else {
                EmployeeSave(mode, EmployeeObject);
            }
        }
        else {
            EmployeeSave(mode, EmployeeObject);
        }

    });
   
    $("#home_add_save").click(function (e) {
        var SalesForceDetails = new Object();
        var mode = 'Edit';
        SalesForceDetails = GetHomeAddressPopupData(mode);
        if (SalesForceDetails == null) {
            return false;
        }

        $("#loading").show();
        DeGrToolWebService.UpdateEmplyoeeHomeDetailsWeb($('#username').val(), JSON.stringify(SalesForceDetails), popusaveSalesForce);

    });

    $("#m_address_save").click(function (e) {
        var SalesForceDetails = new Object();
        var mode = 'Edit';
        SalesForceDetails = GetMailAddressPopupData(mode);
        if (SalesForceDetails == null) {
            return false;
        }
        $("#loading").show();
        DeGrToolWebService.UpdateEmployeeMailAddrDetailsWeb($('#username').val(), JSON.stringify(SalesForceDetails), popusaveSalesForce);

    });

    $("#m_shippin_address_save").click(function (e) {
        var SalesForceDetails = new Object();
        var mode = 'Edit';
        SalesForceDetails = GetShipingAddressPopupDate(mode);
        if (SalesForceDetails == null) {
            return false;
        }
        $("#loading").show();
        DeGrToolWebService.UpdateEmployeeShipingAddrDetailsWeb($('#username').val(), JSON.stringify(SalesForceDetails), popusaveSalesForce);
    });

    $("#email_save").click(function (e) {
        var SalesForceDetails = new Object();
        var mode = 'Edit';

        SalesForceDetails = GetEmailPopupData(mode);
        if (SalesForceDetails == null) {
            return false;
        }
        $("#loading").show();
        DeGrToolWebService.UpdateEmplyoeeEmailDetailsWeb($('#username').val(), JSON.stringify(SalesForceDetails), popusaveSalesForce);
    });

    $("#phone_save").click(function (e) {
        var SalesForceDetails = new Object();
        var mode = 'Edit';

        SalesForceDetails = GetPhonePopupData(mode);
        if (SalesForceDetails == null) {
            return false;
        }
        $("#loading").show();
        DeGrToolWebService.UpdateEmployeePhoneDetailsWeb($('#username').val(), JSON.stringify(SalesForceDetails), popusaveSalesForce);
    });

    $("#emp_assign_salesforce").change(function (e) {
        DeGrToolWebService.GetRoasterTerritoryDropdownData(sessionStorage["username"], $("#emp_assign_salesforce").val(), FillEmployeeAssignTerritory);
    });

    $("#emp_assign").click(function (e) {
        sessionStorage["ModalName"] = "MyEmployeeModal";
        if (AssignEmployeeValidation()) {
            var NewAssignFieldforce = $("#emp_assign_salesforce").val();
            var NewAssignTerritory = $("#emp_assign_territory").val();
            var NewAssignEmployeeTerritoryStatusDate = $("#emp_assign_emp_terr_Status").val();
            var NewAssignStartDate = $("#emp_assign_Start_date").val();
            var NewAssignEmpEndDate = $("#emp_assign_end_date").val();
            $("#loading").show();
            DeGrToolWebService.RosterEmployeeAssign($('#username').val(), $("#current_emp_id").val(), NewAssignFieldforce, NewAssignTerritory, NewAssignEmployeeTerritoryStatusDate, NewAssignStartDate, NewAssignEmpEndDate, "N", DisplayPromtMessage);
        }
        else {
            return false;
        }

    });

    $("#remove_employee").click(function (e) {

       
        var EmployeeEndDate = $("#EmployeeRemoveDate").val();
        if (!(EmployeeEndDate != null && EmployeeEndDate != "" && EmployeeEndDate != undefined)) {
            isValid = false;
        }
        else {
            isValid = true;
        }
        if (isValid) {
            BootstrapDialog.show({
                message: "Are you sure you want to proceed?",
                title: 'Warning!',
                closable: false,
                type: [BootstrapDialog.TYPE_WARNING],
                buttons: [{
                    label: 'OK',
                    cssClass: 'btn-primary',
                    action: function (dialogItself) {
                        $("#loading").show();

                        DeGrToolWebService.RosterEmployeeRemoveAssignedEmp($('#username').val(), $("#current_emp_id").val(), sessionStorage['salseforceId'], sessionStorage['Territory_id'], EmployeeEndDate, $("#EmployeeRemoveDate").data('pk'), popusaveSalesForce);
                        //sessionStorage['EmployeeID'] = "";
                        sessionStorage['Territory_id'] = "";
                        sessionStorage['salseforceId'] = "";
                        dialogItself.close();
                    }
                }]
            });
        }
        else {
            CustomAlert("Please enter position end date.");
            return false;
        }
    });

    $("#previewExcelID").click(function (e) {
        var tempVisibleColumns = new Array();
        $('#FieldsToShow > div').each(function () {
            tempVisibleColumns.push($(this).attr('value'));
        });
        if (tempVisibleColumns.length <= 0) {
            CustomAlert("Please select columns.");
            return false;
        }
        var AsonRoster = "";
        if ($("#AsOnDateRoster").prop('checked')) {
            
            var EndDate = $("#ReportSelectedDate").val();
            AsonRoster = "Yes";
        }
        else {
            if ($("#ReportSelectEndDate").val() == "") {
                CustomAlert("Please enter to date.");
                return false;
            }
            var EndDate = $("#ReportSelectEndDate").val();
            AsonRoster = "No";
        }
        $("#loading").show();
        if ($("#includeInactiveInreport").prop('checked')) {
            var IncludeInactiveEmployee = "Yes";
        } else {
            var IncludeInactiveEmployee = "No";
        }
        DeGrToolWebService.GetRoasterExcelData($('#username').val(), ConvertArrayToString($('#report_Salesforce_ID').val()), $("#report_geographyID").val(), ConvertArrayToString($("#report_TerritoryTyep").val()), ConvertArrayToString($("#report_role_Name").val()), $("#ReportSelectedDate").val(), ConvertArrayToString($("#EmployeeAssignmentType").val()), EndDate, AsonRoster, IncludeInactiveEmployee, datatablesetExcel);
    });

    $("#logout").click(function (event) {
          //alert("logut click");
          sessionStorage["username"] = "";

          location.href = "login_roster.aspx";
    });

    $("#new_terr_salesforce_id").change(function (e) {
        salesforceName = $("#new_terr_salesforce_id").val();
        DeGrToolWebService.GetRoleMasterValues($('#username').val(), "'" + salesforceName + "'", FillRoleDropDown);

        DeGrToolWebService.GetTerritoryTypeValues($('#username').val(), ("'" + salesforceName + "'"), FillGeographyTypeDropDown);

        DeGrToolWebService.GetRoasterTerritoryDropdownData(sessionStorage["username"], salesforceName, FillTerritoryDropdown);
        MakeFistSelcetSameAsPlaceHolder('new_terr_parent_territory');
        MakeFistSelcetSameAsPlaceHolder('new_terr_terr_type');
        MakeFistSelcetSameAsPlaceHolder('new_terr_role_code');
    });

});
//======================================================= ready end =============================================================

//==================================================== function Starts ==========================================================

function ApplyRights(data) {
    
    if (data["Salesforce_Edit_Flag"] !== "Y") {
        $("#fieldforceEdit").remove();
    }
    if (data["Employee_Edit_Flag"] !== "Y") {
        EmployeeEditRights = false;
        $("#EmployeeEditSection").remove();
        $("#EmployeeAddressEditSection").remove();
        $("#employeeEmailEditSection").remove();
        $("#EmployeePhoneEditSection").remove();
    }
    else {
        EmployeeEditRights = true;
    }
    if (data["Territory_Edit_Flag"] !== "Y") {
        TerritoryEditRights = false;
        $("#geographyEditSection").remove();
    }
    else {
        TerritoryEditRights = true;
    }

     if (data["Add_Assignment_Flag"] !== "Y") {
        $("#EmployeeAssignmentAddSection").remove();
    }
     if (data["Remove_Assignment_Flag"] !== "Y") {
         $("#fieldforceEdit").remove();
         RemoveEmployeeRightsFlag = false;
     }
     else {
          RemoveEmployeeRightsFlag = true;

     }



    return true;
}

function onSuccess(data) {
    //$("#loading").hide();
    //debugger;
    var result_course = JSON.parse(data);
    if (result_course.Status == 1) {
        if (result_course["dt_ReturnedTables"][5] != null) {
            sessionStorage['username'] = result_course["dt_ReturnedTables"][5][0]['User_id'];
           
            sessionStorage["username"] = sessionStorage["username"].toUpperCase();
            $("#username").val(sessionStorage["username"]);
            $("#hiduserName").val(sessionStorage["username"]);
            if (result_course["dt_ReturnedTables"][4] != null) {
                if (ApplyRights(result_course["dt_ReturnedTables"][4][0])) {

                    MasterData["SalesForceData"] = result_course["dt_ReturnedTables"][0];

                    FillFilterSalesForceData(result_course["dt_ReturnedTables"][0]);

                    FillFilterRoleData(result_course["dt_ReturnedTables"][1]);

                    FillFilterTerritoryTypeDropdown(result_course["dt_ReturnedTables"][2]);

                    FillStateDropdown(result_course["dt_ReturnedTables"][3]);

                    $(".salesforce").html('');
                    $(".salesforce").append($('<option></option>', {
                        value: "",
                        text: "Please Select Fieldforce",
                    }));

                    for (var i = 0; i < result_course["dt_ReturnedTables"][0].length; i++) {
                        $(".salesforce").append($('<option></option>', {
                            value: result_course["dt_ReturnedTables"][0][i]["Salesforce_ID"],
                            text: result_course["dt_ReturnedTables"][0][i]["Salesforce_Name"],
                        }));
                    }
                    var IncludeInvalid = "";
                    if ($("#IncludeInactiveRoster").prop('checked')) {
                        IncludeInvalid = "Yes";
                    }
                    DeGrToolWebService.GetRoasterTabelData($('#username').val(), ConvertArrayToString($('#select_Salesforce_id').val()), $("#Geography_input").val(), ConvertArrayToString($("#Territory_Type").val()), ConvertArrayToString($("#Role_filter_select").val()), today, IncludeInvalid, datatableset);

                    DeGrToolWebService.GetRosterReports(sessionStorage["username"], RosterReportData, onFail);

                    DeGrToolWebService.GetRosterTerritoryEmployeeStaus(sessionStorage["username"], FillTerritoryEmployeeStausDropdown);
                }
                else {
                    CustomAlert('')
                }
            }
            else {
                $("#loading").hide();
                CustomAlert('You have no rights.');

                location.href = "login_roster.aspx";
            }
            
        }
        else {
            $("#loading").hide();
            CustomAlert('You have no rights.');
            location.href = "login_roster.aspx";
        }
    }
    else {
        CustomAlert(result_course.Message);
        location.href = "login_roster.aspx";
    }
}

function onFail(error) {

    $("#loading").hide();
}

//=================================================== Filter Row function Starts ================================================
function FillFilterSalesForceData(data) {

    $(".SalesforceMul").html('');
    $(".SalesforceMul").multiselect('destroy');
    for (var i = 0; i < data.length; i++) {
        $(".SalesforceMul").append($('<option></option>', {
            value: data[i]["Salesforce_ID"],
            text: data[i]["Salesforce_Name"],
        }));
    }
    $(".SalesforceMul").val('');
    $('.SalesforceMul').attr('multiple', 'multiple');

    $('#select_Salesforce_id').multiselect({
        nonSelectedText: 'Select Fieldforce',
        includeSelectAllOption: true,
        numberDisplayed: 0,
        nSelectedText: ' - Fieldforce selected',
        allSelectedText: 'All Fieldforce Selected',
        onChange: function (option, checked, select) {
            var salesforceName = $("#select_Salesforce_id").val();
            DeGrToolWebService.GetRoleMasterValues($('#username').val(), ConvertArrayToString(salesforceName), FillFilterRoleDropDown);
            DeGrToolWebService.GetTerritoryTypeValues($('#username').val(), ConvertArrayToString(salesforceName), FillFilterGeographyTypeDropDown);
        },
        onSelectAll: function () {
            var salesforceName = $("#select_Salesforce_id").val();
            DeGrToolWebService.GetRoleMasterValues($('#username').val(), ConvertArrayToString(salesforceName), FillFilterRoleDropDown);
            DeGrToolWebService.GetTerritoryTypeValues($('#username').val(), ConvertArrayToString(salesforceName), FillFilterGeographyTypeDropDown);
        }
    });

    $('#report_Salesforce_ID').multiselect({
        nonSelectedText: 'Select Fieldforce',
        buttonWidth: '100%',
        includeSelectAllOption: true,
        nSelectedText: ' - Fieldforce selected',
        allSelectedText: 'All Fieldforce Selected',
        numberDisplayed: 0,
        maxHeight: 200,
        onChange: function (option, checked, select) {
            var salesforceName = $("#report_Salesforce_ID").val();
            DeGrToolWebService.GetRoleMasterValues($('#username').val(), ConvertArrayToString(salesforceName), FillFilterRoleDropDown);
            DeGrToolWebService.GetTerritoryTypeValues($('#username').val(), ConvertArrayToString(salesforceName), FillFilterGeographyTypeDropDown);
        },
        onSelectAll: function () {
            var salesforceName = $("#report_Salesforce_ID").val();
            DeGrToolWebService.GetRoleMasterValues($('#username').val(), ConvertArrayToString(salesforceName), FillFilterRoleDropDown);
            DeGrToolWebService.GetTerritoryTypeValues($('#username').val(), ConvertArrayToString(salesforceName), FillFilterGeographyTypeDropDown);
        }

    });

    $('#Employee_Salesforce_Id').multiselect({
        nonSelectedText: 'Select Fieldforce',
        buttonWidth: '100%',
        includeSelectAllOption: true,
        nSelectedText: ' - Fieldforce selected',
        allSelectedText: 'All Fieldforce Selected',
        numberDisplayed: 0,
        maxHeight: 200,
        onChange: function (option, checked, select) {
            var salesforceName = $("#Employee_Salesforce_Id").val();
            DeGrToolWebService.GetRoleMasterValues($('#username').val(), ConvertArrayToString(salesforceName), FillFilterRoleDropDown);
            DeGrToolWebService.GetTerritoryTypeValues($('#username').val(), ConvertArrayToString(salesforceName), FillFilterGeographyTypeDropDown);
        },
        onSelectAll: function () {
            var salesforceName = $("#Employee_Salesforce_Id").val();
            DeGrToolWebService.GetRoleMasterValues($('#username').val(), ConvertArrayToString(salesforceName), FillFilterRoleDropDown);
            DeGrToolWebService.GetTerritoryTypeValues($('#username').val(), ConvertArrayToString(salesforceName), FillFilterGeographyTypeDropDown);
        }

    });
}

function FillFilterRoleData(data) {
    $(".roleMul").html('');
    $(".roleMul").multiselect('destroy');
    for (var i = 0; i < data.length; i++) {
        $(".roleMul").append($('<option></option>', {
            value: data[i]["Role_Code"],
            text: data[i]["Role_Name"],
        }));
    }
    $(".roleMul").val('');
    $('.roleMul').attr('multiple', 'multiple');
    $('.roleMul').multiselect({
        nonSelectedText: 'Select Role',
        includeSelectAllOption: true,
        numberDisplayed: 0,
        nSelectedText: ' - Role selected',
        allSelectedText: 'All Role Selected',
        maxHeight: 200
    });
}

function FillFilterTerritoryTypeDropdown(data) {
    $(".terrTypeMul").html('');
    $(".terrTypeMul").multiselect('destroy');
    for (var i = 0; i < data.length; i++) {

        $(".terrTypeMul").append($('<option></option>', {
            value: data[i]["TerriTory_Type_Code"],
            text: data[i]["Territory_Type_Name"],
        }));
    }
    $('.terrTypeMul').attr('multiple', 'multiple');
    $(".terrTypeMul").val('')
    $('.terrTypeMul').multiselect({
        nonSelectedText: 'Select Geography Type',
        includeSelectAllOption: true,
        numberDisplayed: 0,
        nSelectedText: ' - Geography Type selected',
        allSelectedText: 'All Geography Type Selected',
        maxHeight: 200
    });
}

function FillFilterGeographyTypeDropDown(data) {
    var result_course = JSON.parse(data);
    FillFilterTerritoryTypeDropdown(result_course["dt_ReturnedTables"][0]);
}

function FillFilterRoleDropDown(data) {
    var result_course = JSON.parse(data);
    FillFilterRoleData(result_course["dt_ReturnedTables"][0])
}

function ClearFiltersRecord() {

    $('.SalesforceMul').val('');
    $('.SalesforceMul').multiselect('refresh');
    $("#Geography_input").val('');
    $('#Territory_Type').val('');
    $("#Territory_Type").multiselect('refresh');
    $('#Role_filter_select').val('');
    $("#Role_filter_select").multiselect('refresh');
    $("#tbl_roster_filter > label > input[type='search']").val('');
    $("#loading").show();

    DeGrToolWebService.GetRoasterHomePageData(sessionStorage["username"], "Firsts", onSuccess, onFail);


}
//==================================================== Filter Row function ends =================================================

//================================================== Roster Tab Fucntions Starts ================================================
function SearchFoRecord() {
    $("#loading").show();
    var IncludeInvalid = "";
    if ($("#IncludeInactiveRoster").prop('checked')) {
        IncludeInvalid = "Yes";
    }
    DeGrToolWebService.GetRoasterTabelData($('#username').val(), ConvertArrayToString($('#select_Salesforce_id').val()), $("#Geography_input").val(), ConvertArrayToString($("#Territory_Type").val()), ConvertArrayToString($("#Role_filter_select").val()), today, IncludeInvalid, datatableset);
}

function datatableset(data) {
    if (data != "" && data != undefined) {
        var rosterTblData = JSON.parse(data);
        if (rosterTblData.Status == 1) {
            RosterTabel = rosterTblData["dt_ReturnedTables"][0];

            if (RosterTabel == null) {
                $("#loading").hide();
                CustomAlert("Not able to retrive data please refresh page!");
            }

            var total_vacant = $.grep(RosterTabel, function (item) {
                return item["Employee_ID"] == "vacant";
            });
            $("#RosterCounts").show();
            $("#EmployeeCounts").hide();
              

            $("#RosterTerritoryVacantCount").text(total_vacant.length);
            $("#RosterTerritoryCount").text(rosterTblData["dt_ReturnedTables"][1][0]["total_territories"]);
            $("#RosterTerritoryActiveCount").text(rosterTblData["dt_ReturnedTables"][1][0]["total_active"]);
            var table = "<table class='table stripe cust_table' id='tbl_roster' ><thead> " +
                                 "<tr class='cust_table_header'>" +
                                 "<th style='text-align:center' id='btn_view'>View </th> " +
                                 "<th style='text-align:center'>Fieldforce ID</th> <th style='text-align:center'>Geography ID</th><th style='text-align:center'>Geography Name</th>" +
                                 "<th style='text-align:center'>Role</th> <th style='text-align:center'>Type</th> " +
                                 "<th style='text-align:center'>Full Name</th><th style='text-align:center'>Status</th> " +
                                   "<th style='text-align:center'>Manager Name</th>" +
                                     "<th style='text-align:center'>Parent Geography</th>" +
                                       "<th style='text-align:center'>2nd Level Parent</th>" +

                           //      //hidden columns
                           //      "<th style='text-align:center'>User ID</th><th>Employee Title</th> <th style='text-align:center'>First Name </th>" +
                           //"<th style='text-align:center'>Middle Name </th>" +
                           //"<th style='text-align:center'>Last Name </th>" +
                           //"<th style='text-align:center'>Employee ID</th>" +
                           //"<th style='text-align:center'>Suffix</th>" +
                           //"<th style='text-align:center'>Manager ID</th>" +
                           //"<th style='text-align:center'>Shipping Address</th>" +
                           //"<th style='text-align:center'>Shipping Address2</th>" +
                           //"<th style='text-align:center'>Shipping City</th>" +
                           //"<th style='text-align:center'>Shipping State</th>" +
                           //"<th style='text-align:center'>Shipping Zip</th>" +
                           //"<th style='text-align:center'>Mailing Address</th>" +
                           //"<th style='text-align:center'>Mailing Address2</th>" +
                           //"<th style='text-align:center'>Mailing City</th>" +
                           //"<th style='text-align:center'>Mailing State</th>" +
                           //"<th style='text-align:center'>Mailing Zip</th>" +
                           //"<th style='text-align:center'>Office Phone</th>" +
                           //"<th style='text-align:center'>Company Email</th>" +
                           //"<th style='text-align:center'>Home Phone</th>" +
                           //"<th style='text-align:center'>CellPhone</th>" +
                           //"<th style='text-align:center'>Personal Email</th>" +
                           //"<th style='text-align:center'>Hire Date</th>" +
                           //"<th style='text-align:center'>Position Start Date</th>" +
                           //"<th style='text-align:center'>Position End Date</th>" +
                           //"<th style='text-align:center'>Termination Date</th>" +
                           //"<th style='text-align:center'>Field Start Date</th>" +
                           //"<th style='text-align:center'>Employee Type</th>" +
                           //"<th style='text-align:center'>Employee Status</th>" +
                           //"<th style='text-align:center'>Fieldforce Name</th>" +
                           //"<th style='text-align:center'>Fieldforce Start Date</th>" +
                           //"<th style='text-align:center'>Fieldforce End Date</th>" +
                           //"<th style='text-align:center'>Geography Start Date</th>" +
                           //"<th style='text-align:center'>Geography End Date</th>" +
                           //"<th style='text-align:center'>Default Geography ZIP</th>" +
                           //"<th style='text-align:center'>Geography ZIP Latitude</th>" +
                           //"<th style='text-align:center'>Geography ZIP Longitude</th>" +
                           //"<th style='text-align:center'>Rep Address Latitude</th>" +
                           //"<th style='text-align:center'>Rep Address Longitude</th>" +
                           //"<th style='text-align:center'>Account Centroid Latitude</th>" +
                           //"<th style='text-align:center'>Account Centroid Longitude</th>" +
                           // "<th style='text-align:center'>Employee Geography Status</th>" +
                           "  </thead>  <tbody>";
            var rowsData = "";
            for (var i = 0 ; i < RosterTabel.length; i++) {
                var row = RosterTabel[i];
                var row = replaceNull(row);
                var fullName = '';
                if (row["Last_Name"] != '') fullName += row["Last_Name"] + ', ';
                if (row["First_Name"] != '') fullName += row["First_Name"] + ' ';
                if (row["Middle_Name"] != '') fullName += row["Middle_Name"];
                if (row["Suffix"] != '' && row["Suffix"] != null) fullName += ', ' + row["Suffix"];
                var fullManagerName = '';
                if (row["manager_last_name"] != '') fullManagerName += row["manager_last_name"] + ', ';
                if (row["manager_first_name"] != '') fullManagerName += row["manager_first_name"] + ' ';
                if (row["manager_middle_name"] != '') fullManagerName += row["manager_middle_name"];
                if (row["manager_suffix"] != '' && row["manager_suffix"] != null) fullManagerName += ', ' + row["manager_suffix"] + ' ';
                rowsData += "<tr style='text-align:left'>" +
                     '<td class="text-center"><button class="btn btn-sm btn-link" title="View"  onclick="return  view_popup(this,\'' + row["Employee_ID"] + '\',\'' + row['Territory_ID'] + '\',\'' + row['Salesforce_ID'] + '\')"  ><i class="fa fa-expand" aria-hidden="true"></i></button></td>' +
                    "<td >" + row["Salesforce_ID"] + "</td><td style='text-align:left'>" + row["Territory_ID"] + "</td><td style='text-align:left'>" + row["Territory_Name"].toUpperCase() + "</td>" +
                    "<td>" + row["role_code"] + "</td><td>" + row["Territory_Type_Name"] + "</td><td>" + fullName + "</td>";
                var terrStatus = "";
                if (row["terr_end_date"] != "" && row["terr_end_date"] != null) {
                    var CompareDatetime = new Date(row["terr_end_date"]);
                    var todayDateTime = new Date(today);
                    if (todayDateTime <= CompareDatetime) { terrStatus = 'Active'; }
                    else { terrStatus = 'Inactive'; }
                }
                else { terrStatus = 'Active'; }

                rowsData += "<td>" + terrStatus + "</td>";
                rowsData += "<td style='text-align:left'>" + fullManagerName + "</td>";
                if (row["pID"] != null && row["pID"] != "") {rowsData += "<td style='text-align:left'>" + row["pID"] + ' - ' + row["p_name"].toUpperCase() + "</td>"; }
                else { rowsData += "<td style='text-align:left'> N/A </td>"; }
                if (row["gID"] != null && row["gID"] != "") { rowsData += "<td style='text-align:left'>" + row["gID"] + ' - ' + row["g_name"].toUpperCase() + "</td>";  }
                else { rowsData += "<td style='text-align:left'> N/A </td>"; }

       //         //"<td style='text-align:center'>First Name </td>" +
       //         rowsData += "<td>" + row["User_ID"] + "</td><td>" + row["Employee_title_desc"] + "</td><td style='text-align:center'>" + row['First_Name'] + "  </td>" +
       ////"<td style='text-align:center'>Middle Name </td>" +
       //"<td style='text-align:center'>" + row['Middle_Name'] + " </td>" +
       ////"<td style='text-align:center'>Last Name </td>" +
       //"<td style='text-align:center'>" + row['Last_Name'] + " </td>" +
       ////"<td style='text-align:center'>Employee ID</td>" +
       //"<td style='text-align:center'>" + row['Employee_ID'] + "</td>" +
       ////"<td style='text-align:center'>Suffix</td>" +
       //"<td style='text-align:center'>" + row['Suffix'] + "</td>" +
       ////"<td style='text-align:center'>Manager ID</td>" +
       //"<td style='text-align:center'>" + row['manager_id'] + "</td>" +
       ////"<td style='text-align:center'>Shiping Address</td>" +
       //"<td style='text-align:center'>" + row['M_Address1'] + "</td>" +
       ////"<td style='text-align:center'>Shiping Address2</td>" +
       //"<td style='text-align:center'>" + row['M_Address2'] + "</td>" +
       ////"<td style='text-align:center'>Shiping city</td>" +
       //"<td style='text-align:center'>" + row['M_City'] + "</td>" +
       ////"<td style='text-align:center'>Shiping state</td>" +
       //"<td style='text-align:center'>" + row['M_State'] + "</td>" +
       ////"<td style='text-align:center'>Shiping zip</td>" +
       //"<td style='text-align:center'>" + row['M_Zip'] + "</td>" +
       ////"<td style='text-align:center'>Mailing Address</td>" +
       //"<td style='text-align:center'>" + row['Home_Address1'] + "</td>" +
       ////"<td style='text-align:center'>Mailing address2</td>" +
       //"<td style='text-align:center'>" + row['Home_Address2'] + "</td>" +
       ////"<td style='text-align:center'>Mailing City</td>" +
       //"<td style='text-align:center'>" + row['Home_City'] + "</td>" +
       ////"<td style='text-align:center'>Home State</td>" +
       //"<td style='text-align:center'>" + row['Home_State'] + "</td>" +
       ////"<td style='text-align:center'>Mailing Zip</td>" +
       //"<td style='text-align:center'>" + row['Home_Zip'] + "</td>" +
       ////"<td style='text-align:center'>Office Phone</td>" +
       //"<td style='text-align:center'>" + row['Office_Phone'] + "</td>" +
       ////"<td style='text-align:center'>Company Email</td>" +
       //"<td style='text-align:center'>" + row['Company_Email'] + "</td>" +
       ////"<td style='text-align:center'>Home Phone</td>" +
       //"<td style='text-align:center'>" + row['Home_Phone'] + "</td>" +
       ////"<td style='text-align:center'>CellPhone</td>" +
       //"<td style='text-align:center'>" + row['Mobile_Phone'] + "</td>" +
       ////"<td style='text-align:center'>Personal Email</td>" +
       //"<td style='text-align:center'>" + row['Personal_Email'] + "</td>" +
       ////"<td style='text-align:center'>Hire Date</td>" +
       //"<td style='text-align:center'>" + row['Employee_hire_date'] + "</td>" +
       ////"<td style='text-align:center'>Position Start Date</td>" +
       //"<td style='text-align:center'>" + row['emp_position_start_date'] + "</td>" +
       ////"<td style='text-align:center'>Position End Date</td>" +
       //"<td style='text-align:center'>" + row['emp_position_end_date'] + "</td>" +
       ////"<td style='text-align:center'>Termination Date</td>" +
       //"<td style='text-align:center'>" + row['Employee_termination_date'] + "</td>" +
       ////"<td style='text-align:center'>Field Start Date</td>" +
       //"<td style='text-align:center'>" + row['Employee_field_Start_date'] + "</td>" +
       //////"<td style='text-align:center'>Position Name</td>" +
       ////"<td style='text-align:center'>" + row['emp_rol_name'] + "</td>" +
       //////"<td style='text-align:center'>Position Code</td>" +
       ////"<td style='text-align:center'>" + row['role_code'] + "</td>" +
       ////"<td style='text-align:center'>Employee Type</td>" +
       //"<td style='text-align:center'>" + row['Employee_type_desc'] + "</td>" +
       ////"<td style='text-align:center'>Employee Status</td>" +
       //"<td style='text-align:center'>" + row['Employee_Status'] + "</td>"+
       //         //"<th style='text-align:center'>Fieldforce Name</th>" +
       //         "<td style='text-align:center'>" + row['Salesforce_Name'] + "</td>" +
       //         //"<th style='text-align:center'>Fieldforce Start Date</th>" +
       //         "<td style='text-align:center'>" + row['fieldforce_start_date'] + "</td>" +
       //         //"<th style='text-align:center'>Fieldforce End Date</th>" +
       //         "<td style='text-align:center'>" + row['fieldforce_end_date'] + "</td>" +
       //         //"<th style='text-align:center'>Geography Start Date</th>" +
       //         "<td style='text-align:center'>" + row['terr_start_date'] + "</td>" +
       //         //"<th style='text-align:center'>Geography End Date</th>" +
       //         "<td style='text-align:center'>" + row['terr_end_date'] + "</td>" +
       //         //"<th style='text-align:center'>Default Geography ZIP</th>" +
       //         "<td style='text-align:center'>" + row['Default_Terr_ZIP'] + "</td>" +
       //         //"<th style='text-align:center'>Geography ZIP Latitude</th>" +
       //         "<td style='text-align:center'>" + row['Territory_ZIP_latitude'] + "</td>" +
       //         //"<th style='text-align:center'>Geography ZIP Longitude</th>" +
       //         "<td style='text-align:center'>" + row['Territory_ZIP_longitude'] + "</td>" +
       //         //"<th style='text-align:center'>Rep Address Latitude</th>" +
       //         "<td style='text-align:center'>" + row['Rep_Addr_Latitude'] + "</td>" +
       //         //"<th style='text-align:center'>Rep Address Longitude</th>" +
       //              "<td style='text-align:center'>" + row['Rep_Addr_Longitude'] + "</td>" +
       //         //"<th style='text-align:center'>Account Centroid Latitude</th>" +
       //              "<td style='text-align:center'>" + row['Acct_Cntrd_Latitude'] + "</td>" +
       //         //"<th style='text-align:center'>Account Centroid Longitude</th>" +
       //              "<td style='text-align:center'>" + row['Acct_Cntrd_Longitude'] + "</td>" +
       //         //"<th style='text-align:center'>Employee Geography Status</th>" +
       //              "<td style='text-align:center'>" + row['Status_Desc'] + "</td>";
                rowsData += '</tr>';
            }
            rowsData += "</table>";
            table += rowsData;
            headerData = new Array();
            var IncludeInvalidFlg = "";
            if ($("#IncludeInactiveRoster").prop('checked')) {
                IncludeInvalidFlg = true;
            }
            $('#rosterTable').html(table);

            var i = -1;
            headerData = GetReportFields();
            //headerData = JSON.parse('[{"value":0,"header":"Fieldforce ID"},{"value":1,"header":"Geography # "},{"value":2,"header":"Geography Name"},{"value":3,"header":"Role"},{"value":4,"header":"Geography Type"},{"value":5,"header":"User ID"},{"value":6,"header":"Full Name"},{"value":7,"header":"Employee Title"},{"value":8,"header":"Status"},{"value":9,"header":"Manager Name"},{"value":10,"header":"First Name "},{"value":11,"header":"Middle Name "},{"value":12,"header":"Last Name "},{"value":13,"header":"Employee ID"},{"value":14,"header":"Suffix"},{"value":15,"header":"Manager ID"},{"value":16,"header":"Shipping Address"},{"value":17,"header":"Shipping Address2"},{"value":18,"header":"Shipping City"},{"value":19,"header":"Shipping State"},{"value":20,"header":"Shipping Zip"},{"value":21,"header":"Mailing Address"},{"value":22,"header":"Mailing Address2"},{"value":23,"header":"Mailing City"},{"value":24,"header":"Mailing State"},{"value":25,"header":"Mailing Zip"},{"value":26,"header":"Office Phone"},{"value":27,"header":"Company Email"},{"value":28,"header":"Home Phone"},{"value":29,"header":"Cell Phone"},{"value":30,"header":"Personal Email"},{"value":31,"header":"Hire Date"},{"value":32,"header":"Position Start Date"},{"value":33,"header":"Position End Date"},{"value":34,"header":"Termination Date"},{"value":35,"header":"Field Start Date"},{"value":36,"header":"Employee Type"},{"value":37,"header":"Employee Status"},{"value":38,"header":"Fieldforce Name"},{"value":39,"header":"Fieldforce Start Date"},{"value":40,"header":"Fieldforce End Date"},{"value":41,"header":"Geography Start Date"},{"value":42,"header":"Geography End Date"},{"value":43,"header":"Assignment Type"},{"value":44,"header":"Sample Shipment Address "},{"value":45,"header":"Sample Shipment Address 2"},{"value":46,"header":"Sample Shipment City"},{"value":47,"header":"Sample Shipment State"},{"value":48,"header":"Sample Shipment Zip"},{"value":49,"header":"Parent Geography #"},{"value":50,"header":"Parent Geography Name"},{"value":51,"header":"2nd Level Parent Geography #"},{"value":52,"header":"2nd Level Parent Geography Name"}]');
            //headerData = JSON.parse('[{"value":0,"header":"Fieldforce ID"},{"value":1,"header":"Geography # "},{"value":2,"header":"Geography Name"},{"value":3,"header":"Role"},{"value":4,"header":"Geography Type"},{"value":5,"header":"User ID"},{"value":6,"header":"Full Name"},{"value":7,"header":"Employee Title"},{"value":8,"header":"Status"},{"value":9,"header":"Manager Name"},{"value":10,"header":"First Name "},{"value":11,"header":"Middle Name "},{"value":12,"header":"Last Name "},{"value":13,"header":"Employee ID"},{"value":14,"header":"Suffix"},{"value":15,"header":"Manager ID"},{"value":16,"header":"Shipping Address"},{"value":17,"header":"Shipping Address2"},{"value":18,"header":"Shipping City"},{"value":19,"header":"Shipping State"},{"value":20,"header":"Shipping Zip"},{"value":21,"header":"Mailing Address"},{"value":22,"header":"Mailing Address2"},{"value":23,"header":"Mailing City"},{"value":24,"header":"Mailing State"},{"value":25,"header":"Mailing Zip"},{"value":26,"header":"Office Phone"},{"value":27,"header":"Company Email"},{"value":28,"header":"Home Phone"},{"value":29,"header":"Cell Phone"},{"value":30,"header":"Personal Email"},{"value":31,"header":"Hire Date"},{"value":32,"header":"Position Start Date"},{"value":33,"header":"Position End Date"},{"value":34,"header":"Termination Date"},{"value":35,"header":"Field Start Date"},{"value":36,"header":"Employee Type"},{"value":37,"header":"Employee Status"},{"value":38,"header":"Fieldforce Name"},{"value":39,"header":"Fieldforce Start Date"},{"value":40,"header":"Fieldforce End Date"},{"value":41,"header":"Geography Start Date"},{"value":42,"header":"Geography End Date"},{"value":43,"header":"Assignment Type"},{"value":44,"header":"Sample Shipment Address "},{"value":45,"header":"Sample Shipment Address 2"},{"value":46,"header":"Sample Shipment City"},{"value":47,"header":"Sample Shipment State"},{"value":48,"header":"Sample Shipment Zip"},{"value":49,"header":"Parent Geography #"},{"value":50,"header":"Parent Geography Name"},{"value":51,"header":"2nd Level Parent Geography #"},{"value":52,"header":"2nd Level Parent Geography Name"}]');
            //headerData = JSON.parse('[{ "value": -1, "header": "View " }, { "value": 0, "header": "Fieldforce ID" }, { "value": 1, "header": "Geography ID " }, { "value": 2, "header": "Geography Name" }, { "value": 3, "header": "Role" }, { "value": 4, "header": "Type" }, { "value": 6, "header": "Full Name" }, { "value": 9, "header": "Status" }, { "value": 7, "header": "Manager Name" }, { "value": 5, "header": "User ID" }, { "value": 8, "header": "Employee Title" }, { "value": 10, "header": "First Name " }, { "value": 11, "header": "Middle Name " }, { "value": 12, "header": "Last Name " }, { "value": 13, "header": "Employee ID" }, { "value": 14, "header": "Suffix" }, { "value": 15, "header": "Manager ID" }, { "value": 16, "header": "Shipping Address" }, { "value": 17, "header": "Shipping Address2" }, { "value": 18, "header": "Shipping City" }, { "value": 19, "header": "Shipping State" }, { "value": 20, "header": "Shipping Zip" }, { "value": 21, "header": "Mailing Address" }, { "value": 22, "header": "Mailing Address2" }, { "value": 23, "header": "Mailing City" }, { "value": 24, "header": "Mailing State" }, { "value": 25, "header": "Mailing Zip" }, { "value": 26, "header": "Office Phone" }, { "value": 27, "header": "Company Email" }, { "value": 28, "header": "Home Phone" }, { "value": 29, "header": "Cell Phone" }, { "value": 30, "header": "Personal Email" }, { "value": 31, "header": "Hire Date" }, { "value": 32, "header": "Position Start Date" }, { "value": 33, "header": "Position End Date" }, { "value": 34, "header": "Termination Date" }, { "value": 35, "header": "Field Start Date" }, { "value": 36, "header": "Employee Type" }, { "value": 37, "header": "Employee Status" }, { "value": 38, "header": "Fieldforce Name" }, { "value": 39, "header": "Fieldforce Start Date" }, { "value": 40, "header": "Fieldforce End Date" }, { "value": 41, "header": "Geography Start Date" }, { "value": 42, "header": "Geography End Date" }, { "value": 43, "header": "Assignment Type" },{ "value": 44, "header": "Sample Shipment Address" },{ "value": 45, "header": "Sample Shipment Address 2" },{ "value": 46, "header": "Sample Shipment City" },{ "value": 47, "header": "Sample Shipment State" },{ "value": 48, "header": "Sample Shipment Zip" }]')
            //headerData = JSON.parse('[{ "value": -1, "header": "View " }, { "value": 0, "header": "Fieldforce ID" }, { "value": 1, "header": "Geography ID " }, { "value": 2, "header": "Geography Name" }, { "value": 3, "header": "Role" }, { "value": 4, "header": "Type" }, { "value": 6, "header": "Full Name" }, { "value": 9, "header": "Status" }, { "value": 7, "header": "Manager Name" }, { "value": 5, "header": "User ID" }, { "value": 8, "header": "Employee Title" }, { "value": 10, "header": "First Name " }, { "value": 11, "header": "Middle Name " }, { "value": 12, "header": "Last Name " }, { "value": 13, "header": "Employee ID" }, { "value": 14, "header": "Suffix" }, { "value": 15, "header": "Manager ID" }, { "value": 16, "header": "Shipping Address" }, { "value": 17, "header": "Shipping Address2" }, { "value": 18, "header": "Shipping City" }, { "value": 19, "header": "Shipping State" }, { "value": 20, "header": "Shipping Zip" }, { "value": 21, "header": "Mailing Address" }, { "value": 22, "header": "Mailing Address2" }, { "value": 23, "header": "Mailing City" }, { "value": 24, "header": "Mailing State" }, { "value": 25, "header": "Mailing Zip" }, { "value": 26, "header": "Office Phone" }, { "value": 27, "header": "Company Email" }, { "value": 28, "header": "Home Phone" }, { "value": 29, "header": "Cell Phone" }, { "value": 30, "header": "Personal Email" }, { "value": 31, "header": "Hire Date" }, { "value": 32, "header": "Position Start Date" }, { "value": 33, "header": "Position End Date" }, { "value": 34, "header": "Termination Date" }, { "value": 35, "header": "Field Start Date" }, { "value": 36, "header": "Employee Type" }, { "value": 37, "header": "Employee Status" }, { "value": 38, "header": "Fieldforce Name" }, { "value": 39, "header": "Fieldforce Start Date" }, { "value": 40, "header": "Fieldforce End Date" }, { "value": 41, "header": "Geography Start Date" }, { "value": 42, "header": "Geography End Date" }, { "value": 43, "header": "Default Geography ZIP" }, { "value": 44, "header": "Geography ZIP Latitude" }, { "value": 45, "header": "Geography ZIP Longitude" }, { "value": 46, "header": "Rep Address Latitude" }, { "value": 47, "header": "Rep Address Longitude" }, { "value": 48, "header": "Account Centroid Latitude" }, { "value": 49, "header": "Account Centroid Longitude" }, { "value": 50, "header": "Assignment Type" },{ "value": 51, "header": "Sample Shipment Address" },{ "value": 52, "header": "Sample Shipment Address 2" },{ "value": 53, "header": "Sample Shipment City" },{ "value": 54, "header": "Sample Shipment State" },{ "value": 55, "header": "Sample Shipment Zip" }]')
            if (!$.fn.DataTable.isDataTable("#tbl_roster")) {
                $('#rosterFilter').remove();
                oTable = $('#tbl_roster').DataTable({
                    "dom": '<<"#Txt123.top"lf>>rtip',
                    "bInfo": false,
                    "autoWidth": false,
                    "lengthMenu": [[10, 12, 15, 20, 25, 50, -1], [10, 12, 15, 20, 25, 50, "All"]],
                    "pageLength": 15,
                    "bFilter": true,
                    "scrollX": true,
                    stateSave: true,
                    "fixedColumns": { leftColumns: 3 },
                    "order": [[1, "asc"]],
                    "aoColumnDefs": [
                          { "aDataSort": [1, 2], "aTargets": [1] },
                          { "asSorting": ["asc", "desc"], "aTargets": [1] },
                          { "asSorting": ["desc", "asc"], "aTargets": [2] }
                    ],
                });
            }
            $('#rosterFilter').insertAfter('#searchRosterTable');
            $("#rosterFilter").css({ 'float': 'right' });
            fillColumnselectDropdown(headerData);
            var str = "";
            if (TerritoryEditRights) {
                str = '<div style="margin: 0 auto;width:70%;position: relative;text-align: center;font-size: 14px;"><label>Include Inactive</label>  <label class="switch">' +
                  '<input type="checkbox" onchange="RosterIncludeInactiveRecored(this);" id="IncludeInactiveRoster">' +
                  '<div class="slider round"></div>' +
                  '</label><div style="display: inline-table;float: right;"><input type="button" class="btn btn-success fadeInRight cust_btn1" title="Add New Geography" style="margin-right:10px;height: 26px;vertical-align: middle;padding: 0px 15px;" value="Add New Geography" onclick="OpenGeographyAddModal()"></div></div>';
            }
            else {
                str = '<div style="margin: 0 auto;width:70%;position: relative;text-align: center;font-size: 14px;"><label>Include Inactive</label>  <label class="switch">' +
                  '<input type="checkbox" onchange="RosterIncludeInactiveRecored(this);" id="IncludeInactiveRoster">' +
                  '<div class="slider round"></div>' +
                  '</label><div style="display: inline-table;float: right;"></div></div>';
            }
            $("#Txt123").append(str);
            if (IncludeInvalidFlg) {
                $("#IncludeInactiveRoster").prop('checked',true)
            }
        }
        else {
            CustomAlert(rosterTblData["Message"]);
        }
    }
    else {
        CustomAlert("No data found.");
    }
    $("#loading").hide();
    $("#Geography_input").css('height', $("#div_select_Salesforce_id .multiselect.dropdown-toggle.ca").outerHeight());
}

function view_popup(id, emp_id, terr_id, salesforceName) {
    $("#loading").show();
    DeGrToolWebService.GetRosterEmployeeData($('#username').val(), emp_id, terr_id, salesforceName, SetPopupData);

    DeGrToolWebService.GetRoleMasterValues($('#username').val(), "'" + salesforceName + "'", FillRoleDropDown);

    DeGrToolWebService.GetTerritoryTypeValues($('#username').val(), ("'" + salesforceName + "'"), FillGeographyTypeDropDown);

    DeGrToolWebService.GetRoasterTerritoryDropdownData(sessionStorage["username"], salesforceName, FillTerritoryDropdown);

    return false;

}

function clearPopup() {

    $("#p_salesforceID").html('');
    $("#p_terr").html('');
    $("#p_terr_type").html('');
    $("#p_role").html('');
    $("#p_parent_terr").html('');
    $("#p_terr_manager_id").html('');
    $("#p_terr_manager_name").html('');
    $("#p_terr_status").html('');
    $("#p_terr_start_date").html('');
    $("#p_terr_end_date").html('');
    $("#p_terr_default_zip").html('');
    $("#p_az_terr_number").html('');
    $("#p_emp_id").html('');
    $("#p_emp_department").html('');
    $("#p_user_id").html('');
    $("#p_emp_title").html('');
    $("#p_emp_name").html('');
    $("#p_preferredName").html('');
    $("#p_emp_hire_date").html('');
    $("#p_emp_field_start_date").html('');
    $("#p_emp_termination_date").html('');
    $("#p_emp_position_start_date").html('');
    //$("#p_emp_position_end_date").html('');
    $("#p_emp_terr_status").html('');
    $("#p_emp_type").html('');
    $("#p_emp_staus").html('');
    $("#p_emp_prid").html('');
    $("#p_az_emp_id").html('');
    $("#p_az_email_id").html('');
    $("#emp_home_address").html('');
    $("#p_mailing_address").html('');
    $("#p_company_emial").html('');
    $("#p_shipping_to_address").html('');
    $("#p_personal_email").html('');
    $("#p_emp_buss_phone").html('');
    $("#p_emp_mobie_no").html('');
    $("#p_emp_home_no").html('');
    return false;
}

function SetPopupData(data) {
    clearPopup()
    if (data != null && data != undefined) {
        var Result = JSON.parse(data)
        if (Result.Status == 1) {
            FillPopupData(Result["dt_ReturnedTables"]);

            $('#myModal').modal({
                backdrop: 'static',
                keyboard: false
            });
             
            $("#loading").hide();

        }
        else {
            CustomAlert(Result.Message);
        }
    }
    else {
    }
    return false;
}

function FillPopupData(data) {
    $("#RecentChangeData").html('');
    $("#TerritoryEmployeeRelationBody").html('');
    if (data[0] != null)
        FillEmployeeSection(data[0][0]);

    if (data[1] != null)
        FillTerritorySection(data[1][0]);

    if (data[2] != null)
        FillSalesforceSection(data[2][0]);

    if (data[3] != null) {
        FillRosterLastEditSection(data[3]);
    }

    if (data[4] != null) {
        FillRosterTerritoryEmployeeRelations(data[4]);
        
    }

    return false;
}

function FillRosterTerritoryEmployeeRelations(data) {
    var str = '';
    var startdate = "";
    var endDate = "";

    for (var i = 0 ; i < data.length; i++) {
        if (data[i]['Position_Start_Date'] == null) { startdate = ""; }
        else {
            startdate = data[i]['Position_Start_Date']
        }
        if (data[i]['Position_End_Date'] == null) { endDate = ""; }
        else {
            endDate = data[i]['Position_End_Date']
        }

        var fullName = '';
        if (data[i]["Last_Name"] != '') fullName += data[i]["Last_Name"] + ', ';
        if (data[i]["First_Name"] != '') fullName += data[i]["First_Name"] + ' ';
        if (data[i]["Middle_Name"] != '') fullName += data[i]["Middle_Name"];
        if (data[i]["Suffix"] != '') fullName += ', ' + data[i]["Suffix"] + '';
        var RoleCode = "";
        if (data[i]['Role_Code'] != '' && data[i]['Role_Code'] != null) RoleCode = data[i]['Role_Code'];
        str += '  <tr > <td>' + data[i]['Salesforce_ID'] + ' - ' + data[i]['Salesforce_Name'] + '</td> <td> ' + fullName + '</td> <td> ' + RoleCode + ' </td> <td> ' + startdate + '</td> <td> ' + endDate + '</td> <td> ' + data[i]['Status_Desc'] + '</td>'
        //+' <td style="float: right;"> ';
        //str += '<div style="cursor: pointer;color:#1e88e5;font-weight:normal" data-emppostitonenddate="' + endDate + '" data-empname="' + fullName + '" data-terrname="' + data[i]['Territory_ID'] + ' - ' + data[i]['Territory_Name'] + '"  onClick="ClearEndDatePopup(this,\'' + data[i]['Territory_ID'] + '\',\'' + data[i]['Salesforce_ID'] + '\',\'' + data[i]['sr_no'] + '\')">Edit</div>'
            + '</tr>';



    }
    $("#TerritoryEmployeeRelationBody").html(str);

}

function FillRosterLastEditSection(data) {
    $("#RecentChangeData").html('');
    var newRecet = "";
    for (var i = 0; i < data.length; i++) {
        if (data[i]["Edited_table"] == "Fieldforce Data") {
            var resultObject = FillSaleforceHistory2(data[i]);
        }
        else if (data[i]["Edited_table"] == "Geography Data") {
            var resultObject = FillTerritoryHistory2(data[i]);
        }
        else if (data[i]["Edited_table"] == "Employee Data") {
            sessionStorage["EditSectionName"] =data[i]["Edited_Section"];
            if (data[i]["Edited_Section"] == "Shipping Address") {
                var resultObject = FillEmployeeAdddresHistory2(data[i]);
            }
            else if (data[i]["Edited_Section"] == "Mailing Address") {
                var resultObject = FillEmployeeAdddresHistory2(data[i]);
            }
            else if (data[i]["Edited_Section"] == "Sample Shipment Locker") {
                var resultObject = FillEmployeeAdddresHistory2(data[i]);
            }
            else {
                var resultObject = FillEmployeeHistory2(data[i]);
            }
        }
        if (resultObject.NewValue.length > 0) {
            for (var j = 0; j < resultObject.NewValue.length; j++) {
                newRecet += '   <div > ' +
     '<div style="display: inline-table; width: 12%"> ' +
      data[i]["LastEditedDate"] + '</div> ' +
    '<div style="display: inline-table; width: 15%"> '
   + data[i]["Edited_table"] +

   '</div> ' +

                '<div style="display: inline-table; width: 20%"> ' +
                 resultObject.NewValue[j]["name"] + '</div> ' +

                '<div style="display: inline-table; width: 20%"> ' +
                 resultObject.OldValue[j]["value"] + '</div> ' +

                '<div style="display: inline-table; width: 20%"> ' +
                 resultObject.NewValue[j]["value"] + '</div> ' +
                '<div style="display:inline-table ;text-align:center;width: 10%;"> ' +
             '' + data[i]["Edited_By"] + ' </div>  <div class="lastcol" style="float: right;"> ' +
'</div>';
            }
        }
    }
    if (data.length == 5) {
        newRecet += '<div id="Div_LoadRosterMoreData" style="text-align:center"><a href="javascript:void(0)" onclick="LoadMoreRosterDetails(this,6)"  data-detail="RecentChangeData" data-attr="show">Load More Details</a></div>';
    }
    $("#RecentChangeData").append(newRecet);
}

function FillEmployeeSection(data) {
    if (data["Employee_ID"] != null) {
        $("#p_emp_id").html(data["Employee_ID"]);
        $("#current_emp_id").val(data["Employee_ID"]);
        $("#hidEmployee").val(data["Employee_ID"]);
    }
    else {
        $("#current_emp_id").val('');
        $("#hidEmployee").val('');
    }
    if (data["Department"] != null) $("#p_emp_department").html(data["Department"]);
    if (data["User_ID"] != null) $("#p_user_id").html(data["User_ID"]);
    if (data["Employee_title_desc"]) $("#p_emp_title").html(data["Employee_title_desc"]);
    var fullName = '';
    if (data["Last_Name"] != '') fullName += data["Last_Name"] + ', ';
    if (data["First_Name"] != '') {
        fullName += data["First_Name"] + ' ';
        $("#hidEmployeeName").val(data["First_Name"]);
    }
    if (data["Middle_Name"] != '') fullName += data["Middle_Name"];
    if (data["Suffix"] != '') fullName += ', ' + data["Suffix"];
    if (data["First_Name"] != null) $("#p_emp_name").html(fullName);
    if (fullName != null && fullName != "")
    { $("#TerritoryManager").html(fullName); }
    else {
        //$("#TerritoryManager").html("VACANT");
    }
    if (data["Preferred_Name"] != null) {
        var fullName = '';
        if (data["Last_Name"] != '') fullName += data["Last_Name"] + ', ';
        if (data["Preferred_Name"] != '') fullName += data["Preferred_Name"] + ' ';
        else {
            if (data["First_Name"] != '') fullName += data["First_Name"] + ' ';
            
        }
        if (data["Middle_Name"] != '') fullName += data["Middle_Name"];
        if (data["Suffix"] != '') fullName += ', ' + data["Suffix"];
    }
    if (data["Preferred_Name"] != null) $("#p_preferredName").html(data["Preferred_Name"]);
    //if (data["Preferred_Name"] != null) $("#p_preferredName").html(fullName)
    if (data["hire_date"] != null) $("#p_emp_hire_date").html(data["hire_date"]);
    if (data["field_start_date"] != null) $("#p_emp_field_start_date").html(data["field_start_date"]);
    if (data["termination_date"] != null) $("#p_emp_termination_date").html(data["termination_date"]);
    if (data["emp_terr_status"] != null) $("#p_emp_terr_status").html(data["emp_terr_status"]);
    if (data["Employee_type_desc"] != null) $("#p_emp_type").html(data["Employee_type_desc"]);
    if (data["Employee_Status"] != null) $("#p_emp_staus").html(data["Employee_Status"]);
    if (data["PRID"] != null) $("#p_emp_prid").html(data["PRID"]);
    if (data["AZ_Employee_ID"] != null) $("#p_az_emp_id").html(data["AZ_Employee_ID"]);
    if (data["AZ_Email_ID"] != null) $("#p_az_email_id").html(data["AZ_Email_ID"]);
 
    var homeaddrstr = '';
    if (data["Home_Address1"] != null) {
        homeaddrstr += data["Home_Address1"];
        if (data["Home_Address2"] != null && data["Home_Address2"] != "") homeaddrstr += ', ' + data["Home_Address2"];
        if (data["Home_City"] != null && data["Home_City"] != "") homeaddrstr += ', ' + data["Home_City"];
        if (data["Home_State"] != null && data["Home_State"] != "") homeaddrstr += ', ' + data["Home_State"];
        if (data["Home_Zip"] != null && data["Home_Zip"] != "") homeaddrstr += ' ' + data["Home_Zip"];
        $("#emp_home_address").html(homeaddrstr);
    }
    var mobileAddr = '';
    if (data["M_Address1"] != null) {
        mobileAddr += data["M_Address1"];
        if (data["M_Address2"] != null && data["M_Address2"] != "") mobileAddr += ', ' + data["M_Address2"];
        if (data["M_City"] != null && data["M_City"] != "") mobileAddr += ', ' + data["M_City"];
        if (data["M_State"] != null && data["M_State"] != "") mobileAddr += ', ' + data["M_State"];
        if (data["M_Zip"] != null && data["M_Zip"] != "") mobileAddr += ' ' + data["M_Zip"];
        $("#p_mailing_address").html(mobileAddr);
    }
    var ShipingAddr = '';
    if (data["Ship_to_Address"] != null) {
        ShipingAddr += data["Ship_to_Address"];
        if (data["Ship_to_Address2"] != null && data["Ship_to_Address2"] != "") ShipingAddr += ', ' + data["Ship_to_Address2"];
        if (data["Ship_to_city"] != null && data["Ship_to_city"] != "") ShipingAddr += ', ' + data["Ship_to_city"];
        if (data["Ship_to_State"] != null && data["Ship_to_State"] != "") ShipingAddr += ', ' + data["Ship_to_State"];
        if (data["Ship_to_Zip"] != null && data["Ship_to_Zip"] != "") ShipingAddr += ' ' + data["Ship_to_Zip"];
        $("#p_shipping_to_address").html(ShipingAddr);
    }
    if (data["Company_Email"] != null) $("#p_company_emial").html(data["Company_Email"]);
    if (data["Personal_Email"] != null) $("#p_personal_email").html(data["Personal_Email"]);
    if (data["Office_Phone"] != null) $("#p_emp_buss_phone").html(data["Office_Phone"]);
    if (data["Mobile_Phone"] != null) $("#p_emp_mobie_no").html(data["Mobile_Phone"]);
    if (data["Home_Phone"] != null) $("#p_emp_home_no").html(data["Home_Phone"]);
}

function FillTerritorySection(data) {
    if (data["Territory_ID"] != null) {
        $("#p_terr").html(data["Territory_ID"] + " - " + data["Territory_Name"]);
        $("#p_terr").data('value', data["Territory_ID"])
        $("#current_emp_terr").val(data["Territory_ID"]);
        $("#hidTerritory").val(data["Territory_ID"]);
        $("#hidTerritoryName").val(data["Territory_Name"]);
        

    }
    else {
        $("#p_terr").data('value', data["Territory_ID"])
        $("#current_emp_terr").val('');
        $("#hidTerritory").val('');
    }
    if (data["Territory_Type"] != null) $("#p_terr_type").html(data["Territory_Type_Name"]);
    if (data["Role_Name"] != null) $("#p_role").html(data["role_code"] + ' - ' + data["Role_Name"]);
    if (data["Parent_Terr_ID"] != null && data["terr_name"] != null) $("#p_parent_terr").html(data["Parent_Terr_ID"] + ' - ' + data["terr_name"]);
    if (data["manager_id"] != null) $("#p_terr_manager_id").html(data["manager_id"]);
    var fullName = '';

    if (data["manager_last_name"] != '' && data["manager_last_name"] != null) fullName += data["manager_last_name"] + ', ';
    if (data["manager_first_name"] != '' && data["manager_first_name"] != null) fullName += data["manager_first_name"] + ' ';
    if (data["manager_middle_name"] != '' && data["manager_middle_name"] != null) fullName += data["manager_middle_name"];
    if (data["manager_suffix"] != '' && data["manager_suffix"] != null) fullName += ', ' + data["manager_suffix"];
    if (fullName != null && fullName != "") { $("#p_terr_manager_name").html(fullName); }
    //else { $("#p_terr_manager_name").html("VACANT"); }
    if (data["Territory_End_date_New"] != "" && data["Territory_End_date_New"] != null) {
        var SalesforceEndDateTime = new Date(data["Territory_End_date_New"]);
        var todayDateTime = new Date(today);
        if (todayDateTime <= SalesforceEndDateTime) {
            $("#p_terr_status").html('Active');
            $("#Status").html('Active');
        }
        else {
            $("#p_terr_status").html('Inactive');
            $("#Status").html('Inactive');
        }
    } else {
        $("#p_terr_status").html('Active');
        $("#Status").html('Active');
    }
    if (data["Territory_Start_date"] != null) $("#p_terr_start_date").html(data["Territory_Start_date_New"]);
    if (data["Territory_End_date"] != null) $("#p_terr_end_date").html(data["Territory_End_date_New"]);
    if (data["Default_Terr_ZIP"] != null) $("#p_terr_default_zip").html(data["Default_Terr_ZIP"]);
    if (data["AZ_Territory_No"] != null) $("#p_az_terr_number").html(data["AZ_Territory_No"]);
    if (data["position_start_date"] != null) $("#p_emp_position_start_date").html(data["position_start_date_New"]);
    $("#modal_title").html('Geography Profile:  ' + data["Territory_ID"] + ' - ' + data["Territory_Name"]);
    if (data["Territory_ID"] != null) $("#TerritoryId").html(" " + data["Territory_ID"] + ' - ' + data["Territory_Name"]);
    //if (data["Territory_Status"] != null) $("#Status").html(" " + data["Territory_Status"]);
}

function FillSalesforceSection(data) {
    if (data["Salesforce_ID"] != null) {
        $("#p_salesforceID").html(data["Salesforce_ID"] + " - " + data["Salesforce_Name"]);
        $("#p_salesforceID").data('value', data["Salesforce_ID"]);
        $("#current_emp_salesforce").val(data["Salesforce_ID"]);
        $("#hidSalesforce").val(data["Salesforce_ID"]);
        
    }
    else {
        $("#p_salesforceID").data('value', '');
        $("#current_emp_salesforce").val('');
        $("#hidSalesforce").val('');
    }
    if (data["Salesforce_Start_Date"] != null) $("#p_salesforce_startdate").html(data["Salesforce_Start_Date"]);
    if (data["Salesforce_End_Date"] != null) $("#p_salesforce_enddate").html(data["Salesforce_End_Date"]);
    if (data["Salesforce_Status"] != null) $("#p_salesforce_status").html(data["Salesforce_Status"]);
}

function FillRoleDropDown(data) {
    if (data != '' && data != undefined) {
        $(".TerritoryRole").html('');
        var result_course = JSON.parse(data);
        if (result_course.Status == 1) {

            $(".TerritoryRole").append($('<option></option>', {
                value: "",
                text: "Please Select Role",
            }));
            for (var i = 0; i < result_course["dt_ReturnedTables"][0].length; i++) {

                $(".TerritoryRole").append($('<option></option>', {
                    value: result_course["dt_ReturnedTables"][0][i]["Role_Code"],
                    text: result_course["dt_ReturnedTables"][0][i]["Role_Name"]
                }));

            }
        }
        else {
            CustomAlert(result_course.Message);
            return false;
        }
    }
    else {
        CustomAlert("Something went wrong");
        return false;
    }
    return false;
}

function FillGeographyTypeDropDown(data) {
    if (data != '' && data != undefined) {
        $(".geographyType").html('');
        var result_course = JSON.parse(data);
        if (result_course.Status == 1) {

            $(".geographyType").append($('<option></option>', {
                value: "",
                text: "Please Select Geography Type",
            }));
            for (var i = 0; i < result_course["dt_ReturnedTables"][0].length; i++) {

                $(".geographyType").append($('<option></option>', {
                    value: result_course["dt_ReturnedTables"][0][i]["Territory_Type_Code"],
                    text: result_course["dt_ReturnedTables"][0][i]["Territory_Type_Name"]
                }));

            }
        }
        else {
            CustomAlert(result_course.Message);
            return false;
        }
    }
    else {
        CustomAlert("Something went wrong");
        return false;
    }
}

function FillTerritoryDropdown(data) {
    if (data != '' && data != undefined) {
        $("#new_terr_parent_territory").html('');
        var result_course = JSON.parse(data);
        if (result_course.Status == 1) {
            $("#new_terr_parent_territory").append($('<option></option>', {
                value: "",
                text: "Please Select Parent Geography",
            }));
            for (var i = 0; i < result_course["dt_ReturnedTables"][0].length; i++) {
                $("#new_terr_parent_territory").append($('<option></option>', {
                    value: result_course["dt_ReturnedTables"][0][i]["Territory_ID"],
                    text: result_course["dt_ReturnedTables"][0][i]["Territory_ID"] + ' - ' + result_course["dt_ReturnedTables"][0][i]["Territory_Name"],
                }));
            }
        }
        else {
            CustomAlert(result_course.Message);
            return false;
        }
    }
    else {
        CustomAlert("Something went wrong");
        return false;
    }
}

function LoadMoreRosterDetails(id, index) {
    $("#loading").show();
    DeGrToolWebService.GetMoreLastChangeData($('#username').val(), $('#current_emp_salesforce').val(), $('#current_emp_terr').val(), $("#current_emp_id").val(), index, AppenMoreRecenteData);
}

function AppenMoreRecenteData(data) {
    var result = JSON.parse(data);
    if (result.Status == 1) {
        var data = result["dt_ReturnedTables"][0];
        $("#Div_LoadRosterMoreData").remove();
        var newRecet = "";
        if (data != null) {
            for (var i = 0; i < data.length; i++) {
                if (data[i]["Edited_table"] == "Fieldforce Data") {
                    var resultObject = FillSaleforceHistory2(data[i]);
                }
                else if (data[i]["Edited_table"] == "Geography Data") {
                    var resultObject = FillTerritoryHistory2(data[i]);
                }
                else if (data[i]["Edited_table"] == "Employee Data") {
                    var resultObject = FillEmployeeHistory2(data[i]);
                }
                if (resultObject.NewValue.length > 0) {
                    for (var j = 0; j < resultObject.NewValue.length; j++) {
                        newRecet += '   <div > ' +
             '<div style="display: inline-table; width: 12%"> ' +
              data[i]["LastEditedDate"] + '</div> ' +
            '<div style="display: inline-table; width: 15%"> '
           + data[i]["Edited_table"] +

           '</div> ' +

                        '<div style="display: inline-table; width: 20%"> ' +
                         resultObject.NewValue[j]["name"] + '</div> ' +

                        '<div style="display: inline-table; width: 20%"> ' +
                         resultObject.OldValue[j]["value"] + '</div> ' +

                        '<div style="display: inline-table; width: 20%"> ' +
                         resultObject.NewValue[j]["value"] + '</div> ' +
                        '<div style="display:inline-table ;text-align:center;width: 10%;"> ' +
                     '' + data[i]["Edited_By"] + ' </div>  <div class="lastcol" style="float: right;"> ' +
        '</div>';
                    }
                }
            }
            if (data.length == 5) {
                newRecet += '<div id="Div_LoadRosterMoreData" style="text-align:center"><a href="javascript:void(0)" onclick="LoadMoreRosterDetails(this,' + (parseInt(data[i - 1]["num"], 10) + 1) + ')"  data-detail="EmployeeRecentChangeData" data-attr="show">Load More Details</a></div>';
            }
            $("#RecentChangeData").append(newRecet);
        }
    }
    else {
        aler(result.Message);
    }
    $("#loading").hide();
}

function openRosterLastChangeValues(historyID, TableName, SectionName) {
    $("#HistoryShowBody").html("");
    sessionStorage["ModalName"] = "myModal";
    $("#myModal .close").trigger('click');
    $("#loading").show();
    if (SectionName == "") {
        if (TableName == "Fieldforce Data") {
            DeGrToolWebService.GetSalesforceOldData(sessionStorage["username"], historyID, $("#current_emp_salesforce").val(), FillSaleforceHistory);
        }
        else if (TableName == "Geography Data") {
            DeGrToolWebService.GetTerritoryOldData(sessionStorage["username"], historyID, $("#current_emp_terr").val(), FillTerritoryHistory);
        }
        else if (TableName == "Employee Data") {
            DeGrToolWebService.GetEmployeeOldData(sessionStorage["username"], historyID, $("#current_emp_id").val(), FillEmployeeHistory);
        }
    }
    else {
        openEmployeeLastChangeValues(historyID, SectionName);
    }
}

function openEmployeeLastChangeValues(historyID, SectionName) {
    $("#HistoryShowBody").html("");
    if (sessionStorage["ModalName"] != "myModal") {
        sessionStorage["ModalName"] = "MyEmployeeModal";
        $("#MyEmployeeModal .close").trigger('click');
    }
    else {
    }

    $("#loading").show();
    sessionStorage["EditSectionName"] = SectionName;
    if (SectionName == "Shipping Address") {
        DeGrToolWebService.GetEmployeeOldData(sessionStorage["username"], historyID, $("#current_emp_id").val(), FillEmployeeAdddresHistory);
    }
    else if (SectionName == "Mailing Address") {
        DeGrToolWebService.GetEmployeeOldData(sessionStorage["username"], historyID, $("#current_emp_id").val(), FillEmployeeAdddresHistory);
    }
    else if (SectionName == "Sample Shipment Locker") {
        DeGrToolWebService.GetEmployeeOldData(sessionStorage["username"], historyID, $("#current_emp_id").val(), FillEmployeeAdddresHistory);
    }
    else {
        DeGrToolWebService.GetEmployeeOldData(sessionStorage["username"], historyID, $("#current_emp_id").val(), FillEmployeeHistory);
    }
}

function FillSaleforceHistory(data) {
    var result = JSON.parse(data)
    if (result.Status == 1) {
        var history = result.dt_ReturnedTables[0][0];
        var str = "";
        $("#HistoryShowBody").html("");
        str += '<div >' +
                    '<div style="display: inline-table; width: 30%;    text-decoration: underline ;font-weight:bold">' +
                   ' Change Type</div>' +
                   ' <div style="display: inline-table; width: 34%;    text-decoration: underline ;font-weight:bold">' +
                    'Old Value' +
                    '</div>' +
                    '<div style="display: inline-table; width: 34%;    text-decoration: underline  ;font-weight:bold">' +
                    'New Value' +
                    '</div>' +
                '</div>';
        for (var key in history) {
            var flagfordis = false;
            if (key.indexOf("_Old") != -1) {
                var newkey = key.replace('_Old', '');

                if ((history[newkey] != null)) {
                    flagfordis = true;
                }
                if (key == "Fieldforce_End__Date_Old") {
                    if ((history[key] != null) && history[newkey] == null) {
                        flagfordis = true;

                    }
                }
                if ((history[newkey] != null)) {
                    if (flagfordis)
                        if ((history[newkey].indexOf("T00") == -1 && history[newkey].indexOf("T12") == -1)) {
                            flagfordis = true;
                        }
                        else {
                            flagfordis = false;
                        }
                }

                if (flagfordis) {
                    var str2 = newkey.replace(/_/g, ' ');
                    if (history[newkey] == null) { history[newkey] = '' }
                    if (history[key] == null) { history[key] = '' }
                    str += '<div class="">' +
                      '<div style="display: inline-table; width: 30%">' + str2 +
                     ' </div>' +
                     ' <div style="display: inline-table; width: 34%">' + history[key] +

                      '</div>' +
                      '<div style="display: inline-table; width: 34%">' + history[newkey]
                      +
                      '</div>' +
                  '</div>';
                }
            }
        }
        $("#HistoryShowTitle").text("Fieldforce Changes")
        $("#HistoryShowBody").append(str);
        $("#HistoryShow").modal({
            backdrop: 'static',
            keyboard: false
        });



    } else {
        CustomAlert(result.Message);
    }
    $("#loading").hide();
}

function FillSaleforceHistory2(data) {
    var ReturnObject = new Object();
    var OldArray = new Array();
    var NewArray = new Array();
    var history = data;
        var str = "";
        for (var key in history) {
            var flagfordis = false;
            //if (history[key] != null) {
            if (key.indexOf("_Old") != -1) {
                var newkey = key.replace('_Old', '');

                if ((history[newkey] != null)) {
                    flagfordis = true;
                }
                //else if ((history[newkey] == null && history[key] != null)) {
                //    flagfordis = true;
                //}
                if (key == "Fieldforce_End__Date_Old") {
                    if ((history[key] != null) && history[newkey] == null) {
                        flagfordis = true;

                    }
                }
                if ((history[newkey] != null)) {
                    if (flagfordis)
                        if ((history[newkey].indexOf("T00") == -1 && history[newkey].indexOf("T12") == -1)) {
                            flagfordis = true;
                        }
                        else {
                            flagfordis = false;
                        }
                }

                if (flagfordis) {
                    //if ((history[newkey].indexOf("T00") == -1 && history[newkey].indexOf("T12") == -1)) {

                    var str2 = newkey.replace(/_/g, ' ');
                    if (history[newkey] == null) { history[newkey] = '' }
                    if (history[key] == null) { history[key] = '' }

                    var tempObj = new Object();
                    tempObj['name'] = str2;
                    tempObj['value'] = history[key];
                    OldArray.push(tempObj);
                    tempObj = new Object();
                    tempObj['name'] = str2;
                    tempObj['value'] = history[newkey];
                    NewArray.push(tempObj);
                    str += '<div class="">' +
                      '<div style="display: inline-table; width: 30%">' + str2 +
                     ' </div>' +
                     ' <div style="display: inline-table; width: 34%">' + history[key] +

                      '</div>' +
                      '<div style="display: inline-table; width: 34%">' + history[newkey]
                      +
                      '</div>' +
                  '</div>';
                    //}
                }
                //}


                //}
            }
        }
        ReturnObject['NewValue'] = NewArray;
        ReturnObject['OldValue'] = OldArray;
       
        return ReturnObject;
}

function FillTerritoryHistory(data) {
    var result = JSON.parse(data)
    if (result.Status == 1) {
        $("#HistoryShowBody").html("");
        var history = result.dt_ReturnedTables[0][0];
        var str = "";
        str += '<div class="">' +
                    '<div style="display: inline-table; width: 30%;    text-decoration: underline ;font-weight:bold"">' +
                   'Change Type </div>' +
                   ' <div style="display: inline-table; width: 34%;    text-decoration: underline ;font-weight:bold"">' +
                    'Old Value' +
                    '</div>' +
                    '<div style="display: inline-table; width: 34%;    text-decoration: underline ;font-weight:bold"">' +
                    'New Value' +
                    '</div>' +
                '</div>';
        for (var key in history) {
            //if (history[key] != null) {
            var flagfordis = false;
            if (key.indexOf("_New") != -1) {
                var newkey = key.replace('_New', '');
                //if (history[newkey] == history[key]) {
                //if (history[key] == null && history[newkey] != null) { }

                if ((history[key] != null)) {
                    flagfordis = true;
                }

                if (key == "Geography_End__Date_New") {
                    if ((history[key] == null) && history[newkey] != null) {
                        flagfordis = true;

                    }
                }


                if (key == "Geography_Start__Date_New") {
                    if ((history[key] == null) && history[newkey] != null) {
                        flagfordis = true;

                    }
                }
                if ((history[key] != null)) {
                    if (flagfordis)
                        if ((history[key].indexOf("T00") == -1 && history[key].indexOf("T12") == -1)) {
                            flagfordis = true;
                        }
                        else {
                            flagfordis = false;
                        }
                }

                if (flagfordis) {
                    //if ((history[key].indexOf("T00") == -1 && history[key].indexOf("T12") == -1)) {

                    var str2 = newkey.replace(/_/g, ' ');
                    if (history[newkey] == null) { history[newkey] = '' }
                    if (history[key] == null) { history[key] = '' }
                    str += '<div class="">' +
                      '<div style="display: inline-table; width: 30%">' + str2 +
                     ' </div>' +
                     ' <div style="display: inline-table; width: 34%">' + history[newkey] +

                      '</div>' +
                      '<div style="display: inline-table; width: 34%">' + history[key]
                      +
                      '</div>' +
                  '</div>';
                    //}
                }


            }
            //}
        }
        $("#HistoryShowTitle").text("Geography Changes")
        $("#HistoryShowBody").append(str);
        $("#HistoryShow").modal({
            backdrop: 'static',
            keyboard: false
        });

    }
    $("#loading").hide();
}

function FillTerritoryHistory2(data) {
    var ReturnObject = new Object();
    var OldArray = new Array();
    var NewArray = new Array();
        var history = data;
        var str = "";
        for (var key in history) {
            var flagfordis = false;
            if (key.indexOf("_New") != -1) {
                var newkey = key.replace('_New', '');
                if ((history[key] != null)) {
                    flagfordis = true;
                }

                if (key == "Geography_End__Date_New") {
                    if ((history[key] == null) && history[newkey] != null) {
                        flagfordis = true;
                    }
                }
                if (key == "Geography_Start__Date_New") {
                    if ((history[key] == null) && history[newkey] != null) {
                        flagfordis = true;
                    }
                }
                if ((history[key] != null)) {
                    if (flagfordis)
                        if ((history[key].indexOf("T00") == -1 && history[key].indexOf("T12") == -1)) {
                            flagfordis = true;
                        }
                        else {
                            flagfordis = false;
                        }
                }
                if (flagfordis) {

                    var str2 = newkey.replace(/_/g, ' ');
                    if (history[newkey] == null) { history[newkey] = '' }
                    if (history[key] == null) { history[key] = '' }
                    var tempObj = new Object();
                    tempObj['name'] = str2;
                    tempObj['value'] = history[newkey];
                    OldArray.push(tempObj);
                    tempObj = new Object();
                    tempObj['name'] = str2;
                    tempObj['value'] = history[key];
                    NewArray.push(tempObj);
                    str += '<div class="">' +
                      '<div style="display: inline-table; width: 30%">' + str2 +
                     ' </div>' +
                     ' <div style="display: inline-table; width: 34%">' + history[newkey] +
                      '</div>' +
                      '<div style="display: inline-table; width: 34%">' + history[key]
                      +
                      '</div>' +
                  '</div>';
                }
            }
        }
        
        ReturnObject['NewValue'] = NewArray;
        ReturnObject['OldValue'] = OldArray;
        
        return ReturnObject;
}

function FillEmployeeHistory(data) {
    var result = JSON.parse(data)
    if (result.Status == 1) {
        $("#HistoryShowBody").html("");
        var history = result.dt_ReturnedTables[0][0];
        var str = "";
        str += '<div class="">' +
                    '<div style="display: inline-table; width: 30%;    text-decoration: underline ;font-weight:bold"">' +
                   ' Change Type</div>' +
                   ' <div style="display: inline-table; width: 34%;    text-decoration: underline ;font-weight:bold"">' +
                    'Old Value' +
                    '</div>' +
                    '<div style="display: inline-table; width: 34%;    text-decoration: underline ;font-weight:bold"">' +
                    'New Value' +
                    '</div>' +
                '</div>';
        for (var key in history) {
            //if (history[key] != null) {
            var flagfordis = false;
            if (key.indexOf("_New") != -1) {
                var newkey = key.replace('_New', '');
                //if (history[newkey] == history[key]) {
                //if (history[key] == null && history[newkey] != null) { }

                if ((history[key] != null)) {
                    flagfordis = true;
                }
                //else if ((history[key] == null && history[newkey] != null)) {
                //    flagfordis = true;
                //}
                if (key == "Employee_Title_Desc_New") {
                    flagfordis = false;
                }
                if (key == "Employee_Type_Desc_New") {
                    flagfordis = false;
                }
                if (key == "Office_Phone_New") {
                    key == "Bussiness_Phone_New"
                }

                if (sessionStorage["EditSectionName"] == "Personal Details") {
                    if (key == "Hire__Date_New") {
                        if ((history[key] == null) && history[newkey] != null) {
                            flagfordis = true;
                        }
                    }

                    if (key == "Termination__Date_New") {
                        if ((history[key] == null) && history[newkey] != null) {
                            flagfordis = true;

                        }
                    }
                    if (key == "Field_Start__Date_New") {
                        if ((history[key] == null) && history[newkey] != null) {
                            flagfordis = true;

                        }
                    }
                }
                if ((history[key] != null)) {
                    if (flagfordis)
                        if ((history[key].indexOf("T00") == -1 && history[key].indexOf("T12") == -1)) {
                            flagfordis = true;
                        }
                        else {
                            flagfordis = false;
                        }
                }

                if (flagfordis) {
                    //if ((history[key].indexOf("T00") == -1 && history[key].indexOf("T12") == -1)) {
                    var str2 = newkey.replace(/_/g, ' ');
                    if (key == "Employee_Title_New") { key = "Employee_Title_Desc_New"; }
                    if (key == "Employee_Type_New") { key = "Employee_Type_Desc_New"; }
                    if (key == "Office_Phone_New") { str2 = "Business Phone"; }
                    var newkey = key.replace('_New', '');
                    if (history[newkey] == null) { history[newkey] = '' }
                    if (history[key] == null) { history[key] = '' }
                    str += '<div class="">' +
                      '<div style="display: inline-table; width: 30%">' + str2 +
                     ' </div>' +
                     ' <div style="display: inline-table; width: 34%">' + history[newkey] +

                      '</div>' +
                      '<div style="display: inline-table; width: 34%">' + history[key]
                      +
                      '</div>' +
                  '</div>';
                    //}
                    //}

                }
            }
            //}
        }
        $("#HistoryShowTitle").text("");
        $("#HistoryShowTitle").text("Employee Changes");
        $("#HistoryShowBody").append(str);
        $("#HistoryShow").modal({
            backdrop: 'static',
            keyboard: false
        });

    }
    $("#loading").hide();
}

function FillEmployeeHistory2(data) {
    
        var history = data;
        var str = "";
        var ReturnObject = new Object();
        var OldArray = new Array();
        var NewArray = new Array();
        for (var key in history) {
            var flagfordis = false;
            if (key.indexOf("_New") != -1) {
                var newkey = key.replace('_New', '');
                if ((history[key] != null)) {
                    flagfordis = true;
                }
                if (key == "Employee_Title_Desc_New") {
                    flagfordis = false;
                }
                if (key == "Employee_Type_Desc_New") {
                    flagfordis = false;
                }
                if (key == "Office_Phone_New") {
                    key == "Bussiness_Phone_New"
                }

                if (sessionStorage["EditSectionName"] == "Personal Details") {
                    if (key == "Hire__Date_New") {
                        if ((history[key] == null) && history[newkey] != null) {
                            flagfordis = true;
                        }
                    }
                    if (key == "Termination__Date_New") {
                        if ((history[key] == null) && history[newkey] != null) {
                            flagfordis = true;
                        }
                    }
                    if (key == "Field_Start__Date_New") {
                        if ((history[key] == null) && history[newkey] != null) {
                            flagfordis = true;
                        }
                    }
                }
                if ((history[key] != null)) {
                    if (flagfordis)
                        if ((history[key].indexOf("T00") == -1 && history[key].indexOf("T12") == -1)) {
                            flagfordis = true;
                        }
                        else {
                            flagfordis = false;
                        }
                }

                if (flagfordis) {
                    var str2 = newkey.replace(/_/g, ' ');
                    if (key == "Employee_Title_New") { key = "Employee_Title_Desc_New"; }
                    if (key == "Employee_Type_New") { key = "Employee_Type_Desc_New"; }
                    if (key == "Office_Phone_New") { str2 = "Business Phone"; }
                    var newkey = key.replace('_New', '');
                    if (history[newkey] == null) { history[newkey] = '' }
                    if (history[key] == null) { history[key] = '' }

                    var tempObj = new Object();
                    tempObj['name'] = str2;
                    tempObj['value'] = history[newkey];
                    OldArray.push(tempObj);
                    tempObj = new Object();
                    tempObj['name'] = str2;
                    tempObj['value'] = history[key];
                    NewArray.push(tempObj);
                    str += '<div class="">' +
                      '<div style="display: inline-table; width: 30%">' + str2 +
                     ' </div>' +
                     ' <div style="display: inline-table; width: 34%">' + history[newkey] +

                      '</div>' +
                      '<div style="display: inline-table; width: 34%">' + history[key]
                      +
                      '</div>' +
                  '</div>';
                }
            }
        }
        ReturnObject['NewValue'] = NewArray;
        ReturnObject['OldValue'] = OldArray;

        return ReturnObject;
}

function FillEmployeeAdddresHistory(data) {
    var result = JSON.parse(data)
    if (result.Status == 1) {
        $("#HistoryShowBody").html("");
        var OldAddress = "";
        var NewAddress = "";
        var history = result.dt_ReturnedTables[0][0];
        var str = "";


        for (var key in history) {
            //if (history[key] != null) {

            //if (history[key].indexOf("T00") == -1) {
            //if (key.indexOf("_New") != -1) {
            if (sessionStorage["EditSectionName"] == "Sample Shipment Locker") {
                if (key == "Ship_to_Address") {
                    OldAddress = "";
                    if (history["Ship_to_Address"] != null) {
                        OldAddress += history["Ship_to_Address"];
                        if (history["Ship_to_Address2"] != null && history["Ship_to_Address2"] != "") OldAddress += ', ' + history["Ship_to_Address2"];
                        if (history["Ship_to_city"] != null && history["Ship_to_city"] != "") OldAddress += ', ' + history["Ship_to_city"];
                        if (history["Ship_to_State"] != null && history["Ship_to_State"] != "") OldAddress += ', ' + history["Ship_to_State"];
                        if (history["Ship_to_Zip"] != null && history["Ship_to_Zip"] != "") OldAddress += ' ' + history["Ship_to_Zip"];
                    }
                }
                if (key == "Ship_to_Address_New") {
                    NewAddress = "";
                    if (history["Ship_to_Address_New"] != null) {
                        NewAddress += history["Ship_to_Address_New"];
                        if (history["Ship_to_Address2_New"] != null && history["Ship_to_Address2_New"] != "") NewAddress += ', ' + history["Ship_to_Address2_New"];
                        if (history["Ship_to_city_New"] != null && history["Ship_to_city_New"] != "") NewAddress += ', ' + history["Ship_to_city_New"];
                        if (history["Ship_to_State_New"] != null && history["Ship_to_State_New"] != "") NewAddress += ', ' + history["Ship_to_State_New"];
                        if (history["Ship_to_Zip_New"] != null && history["Ship_to_Zip_New"] != "") NewAddress += ' ' + history["Ship_to_Zip_New"];

                    }
                }
            }
            else if (sessionStorage["EditSectionName"] == "Mailing Address") {
                if (key == "Home_Address1_New") {
                    NewAddress = "";
                    if (history["Home_Address1_New"] != null) {
                        NewAddress += history["Home_Address1_New"];
                        if (history["Home_Address2_New"] != null && history["Home_Address2_New"] != "") NewAddress += ', ' + history["Home_Address2_New"];
                        if (history["Home_City_New"] != null && history["Home_City_New"] != "") NewAddress += ', ' + history["Home_City_New"];
                        if (history["Home_State_New"] != null && history["Home_State_New"] != "") NewAddress += ', ' + history["Home_State_New"];
                        if (history["Home_Zip_New"] != null && history["Home_Zip_New"] != "") NewAddress += ' ' + history["Home_Zip_New"];
                    }
                }
                if (key == "Home_Address1") {
                    OldAddress = "";
                    if (history["Home_Address1"] != null) {

                        OldAddress += history["Home_Address1"];
                        if (history["Home_Address2"] != null && history["Home_Address2"] != "") OldAddress += ', ' + history["Home_Address2"];
                        if (history["Home_City"] != null && history["Home_City"] != "") OldAddress += ', ' + history["Home_City"];
                        if (history["Home_State"] != null && history["Home_State"] != "") OldAddress += ', ' + history["Home_State"];
                        if (history["Home_Zip"] != null && history["Home_Zip"] != "") OldAddress += ' ' + history["Home_Zip"];
                    }
                }
            }
            else if (sessionStorage["EditSectionName"] == "Shipping Address") {


                if (key == "M_Address1") {
                    OldAddress = "";
                    if (history["M_Address1"] != null) {
                        OldAddress += history["M_Address1"];
                        if (history["M_Address2"] != null && history["M_Address2"] != "") OldAddress += ', ' + history["M_Address2"];
                        if (history["M_City"] != null && history["M_City"] != "") OldAddress += ', ' + history["M_City"];
                        if (history["M_State"] != null && history["M_State"] != "") OldAddress += ', ' + history["M_State"];
                        if (history["M_Zip"] != null && history["M_Zip"] != "") OldAddress += ' ' + history["M_Zip"];
                    }
                }
                if (key == "M_Address1_New") {
                    NewAddress = "";
                    if (history["M_Address1_New"] != null) {

                        NewAddress += history["M_Address1_New"];
                        if (history["M_Address2_New"] != null && history["M_Address2_New"] != "") NewAddress += ', ' + history["M_Address2_New"];
                        if (history["M_City_New"] != null && history["M_City_New"] != "") NewAddress += ', ' + history["M_City_New"];
                        if (history["M_State_New"] != null && history["M_State_New"] != "") NewAddress += ', ' + history["M_State_New"];
                        if (history["M_Zip_New"] != null && history["M_Zip_New"] != "") NewAddress += ' ' + history["M_Zip_New"];
                    }
                }
            }
            //}


            //}
            //}
        }
        str += '<div class="">' +
                   '<div style="display: inline-table; width: 30%">' +
                  ' Old Address</div>' +
                  ' <div style="display: inline-table; width: 69%">' + OldAddress +

                   '</div>' +
                   '<div style="display: inline-table; width: 30%">' +
                   'New Address' +
                   '</div>' +
                   ' <div style="display: inline-table; width: 69%">' + NewAddress +

                   '</div>' +
               '</div>';

        $("#HistoryShowTitle").text("");
        $("#HistoryShowTitle").text("Employee Changes");
        $("#HistoryShowBody").append(str);
        $("#HistoryShow").modal({
            backdrop: 'static',
            keyboard: false
        });

    }
    $("#loading").hide();
}

function FillEmployeeAdddresHistory2(data) {
    var ReturnObject = new Object();
    var OldArray = new Array();
    var NewArray = new Array();
        var OldAddress = "";
        var NewAddress = "";
        var history = data;
        var str = "";


        for (var key in history) {
            //if (history[key] != null) {

            //if (history[key].indexOf("T00") == -1) {
            //if (key.indexOf("_New") != -1) {
            if (sessionStorage["EditSectionName"] == "Sample Shipment Locker") {
                if (key == "Ship_to_Address") {
                    OldAddress = "";
                    if (history["Ship_to_Address"] != null) {
                        OldAddress += history["Ship_to_Address"];
                        if (history["Ship_to_Address2"] != null && history["Ship_to_Address2"] != "") OldAddress += ', ' + history["Ship_to_Address2"];
                        if (history["Ship_to_city"] != null && history["Ship_to_city"] != "") OldAddress += ', ' + history["Ship_to_city"];
                        if (history["Ship_to_State"] != null && history["Ship_to_State"] != "") OldAddress += ', ' + history["Ship_to_State"];
                        if (history["Ship_to_Zip"] != null && history["Ship_to_Zip"] != "") OldAddress += ' ' + history["Ship_to_Zip"];
                    }
                }
                if (key == "Ship_to_Address_New") {
                    NewAddress = "";
                    if (history["Ship_to_Address_New"] != null) {
                        NewAddress += history["Ship_to_Address_New"];
                        if (history["Ship_to_Address2_New"] != null && history["Ship_to_Address2_New"] != "") NewAddress += ', ' + history["Ship_to_Address2_New"];
                        if (history["Ship_to_city_New"] != null && history["Ship_to_city_New"] != "") NewAddress += ', ' + history["Ship_to_city_New"];
                        if (history["Ship_to_State_New"] != null && history["Ship_to_State_New"] != "") NewAddress += ', ' + history["Ship_to_State_New"];
                        if (history["Ship_to_Zip_New"] != null && history["Ship_to_Zip_New"] != "") NewAddress += ' ' + history["Ship_to_Zip_New"];

                    }
                }
            }
            else if (sessionStorage["EditSectionName"] == "Mailing Address") {
                if (key == "Home_Address1_New") {
                    NewAddress = "";
                    if (history["Home_Address1_New"] != null) {
                        NewAddress += history["Home_Address1_New"];
                        if (history["Home_Address2_New"] != null && history["Home_Address2_New"] != "") NewAddress += ', ' + history["Home_Address2_New"];
                        if (history["Home_City_New"] != null && history["Home_City_New"] != "") NewAddress += ', ' + history["Home_City_New"];
                        if (history["Home_State_New"] != null && history["Home_State_New"] != "") NewAddress += ', ' + history["Home_State_New"];
                        if (history["Home_Zip_New"] != null && history["Home_Zip_New"] != "") NewAddress += ' ' + history["Home_Zip_New"];
                    }
                }
                if (key == "Home_Address1") {
                    OldAddress = "";
                    if (history["Home_Address1"] != null) {

                        OldAddress += history["Home_Address1"];
                        if (history["Home_Address2"] != null && history["Home_Address2"] != "") OldAddress += ', ' + history["Home_Address2"];
                        if (history["Home_City"] != null && history["Home_City"] != "") OldAddress += ', ' + history["Home_City"];
                        if (history["Home_State"] != null && history["Home_State"] != "") OldAddress += ', ' + history["Home_State"];
                        if (history["Home_Zip"] != null && history["Home_Zip"] != "") OldAddress += ' ' + history["Home_Zip"];
                    }
                }
            }
            else if (sessionStorage["EditSectionName"] == "Shipping Address") {


                if (key == "M_Address1") {
                    OldAddress = "";
                    if (history["M_Address1"] != null) {
                        OldAddress += history["M_Address1"];
                        if (history["M_Address2"] != null && history["M_Address2"] != "") OldAddress += ', ' + history["M_Address2"];
                        if (history["M_City"] != null && history["M_City"] != "") OldAddress += ', ' + history["M_City"];
                        if (history["M_State"] != null && history["M_State"] != "") OldAddress += ', ' + history["M_State"];
                        if (history["M_Zip"] != null && history["M_Zip"] != "") OldAddress += ' ' + history["M_Zip"];
                    }
                }
                if (key == "M_Address1_New") {
                    NewAddress = "";
                    if (history["M_Address1_New"] != null) {

                        NewAddress += history["M_Address1_New"];
                        if (history["M_Address2_New"] != null && history["M_Address2_New"] != "") NewAddress += ', ' + history["M_Address2_New"];
                        if (history["M_City_New"] != null && history["M_City_New"] != "") NewAddress += ', ' + history["M_City_New"];
                        if (history["M_State_New"] != null && history["M_State_New"] != "") NewAddress += ', ' + history["M_State_New"];
                        if (history["M_Zip_New"] != null && history["M_Zip_New"] != "") NewAddress += ' ' + history["M_Zip_New"];
                    }
                }
            }
            //}


            //}
            //}
        }
        var tempObj = new Object();
        tempObj['name'] = sessionStorage["EditSectionName"];
        tempObj['value'] = OldAddress;
        OldArray.push(tempObj);
        tempObj = new Object();
        tempObj['name'] = sessionStorage["EditSectionName"];
        tempObj['value'] = NewAddress;
        NewArray.push(tempObj);
        str += '<div class="">' +
                   '<div style="display: inline-table; width: 30%">' +
                  ' Old Address</div>' +
                  ' <div style="display: inline-table; width: 69%">' + OldAddress +

                   '</div>' +
                   '<div style="display: inline-table; width: 30%">' +
                   'New Address' +
                   '</div>' +
                   ' <div style="display: inline-table; width: 69%">' + NewAddress +

                   '</div>' +
               '</div>';
       
        ReturnObject['NewValue'] = NewArray;
        ReturnObject['OldValue'] = OldArray;
        
        return ReturnObject;
     
}

function RosterIncludeInactiveRecored($this) {
    $("#loading").show();
    if ($("#IncludeInactiveRoster").prop('checked')) {
        DeGrToolWebService.GetRoasterTabelData($('#username').val(), ConvertArrayToString($('#select_Salesforce_id').val()), $("#Geography_input").val(), ConvertArrayToString($("#Territory_Type").val()), ConvertArrayToString($("#Role_filter_select").val()), today, "Yes", datatableset);
    }
    else {
        DeGrToolWebService.GetRoasterTabelData($('#username').val(), ConvertArrayToString($('#select_Salesforce_id').val()), $("#Geography_input").val(), ConvertArrayToString($("#Territory_Type").val()), ConvertArrayToString($("#Role_filter_select").val()), today, "", datatableset);
    }
}

function DownloadRosterPDF() {
    $("#loading").show();
    setTimeout(function () {
        $('#hnd_RosterPrintclick').click();
         
            $('#loading').hide();
        
    }, 0);
    
}

function OpenGeographyAddModal() {
    //DeGrToolWebService.GetRosterEmployeeType(sessionStorage["username"], FillEmployeeDropdown);
    //DeGrToolWebService.GetRosterEmployeeTitle(sessionStorage["username"], FillEmployeeTitleDropdown);
    $("#terr_save").data('data-savemode', 'Add');
    $("#new_terr_parent_territory").append($('<option></option>', {
        value: "",
        text: "Please Select Parent Geography",
    }));
    $("#new_terr_terr_type").append($('<option></option>', {
        value: "",
        text: "Please Select Geography Type",
    }));
    $("#new_terr_role_code").append($('<option></option>', {
        value: "",
        text: "Please Select Role",
    }));
    MakeFistSelcetSameAsPlaceHolder('new_terr_parent_territory');
    MakeFistSelcetSameAsPlaceHolder('new_terr_terr_type');
    MakeFistSelcetSameAsPlaceHolder('new_terr_role_code');
    var modaltitle = "Add New Geography";
    var id = "NewTerritoryAdd";
    $("#" + id + "Title").text(modaltitle);
    //$("#empAddModal").modal();
   
    var disabled_terr_field = ['newGeographyStatus']
    HidePopupField(disabled_terr_field);
    //var hidefield = ['terr_accc_crtd_row', 'terr_acc_crtd_longitude_row', 'terr_rap_addr_longitude_row', 'terr_rap_addr_latitude_row', 'terr_zip_longitude_row', 'terr_zip_latitude_row'];
    HidePopupField(GetHidenFieldsForTerritoryAdd());

    //ClearEmployeePopup();
    ClearTerritoryDataInNewPopup();
    $("#new_terr_status").val('Active');
    $("#" + id).modal({
        backdrop: 'static',
        keyboard: false
    });
}
//=============================================== Salseforce Function Start ================================================
function GetSalesForceVaues(value) {
    DeGrToolWebService.GetSalesForceDetails(sessionStorage["username"], $("#current_emp_salesforce").val(), SetSalseForceDataToEdit);
}

function SetSalseForceDataToEdit(data) {
    if (data != '' && data != undefined) {
        var result = JSON.parse(data);
        if (result.Status == 1) {
            ClearSalesForceDataPopup();
            // whatfield you want to disable just add id of that field in the following Array
            var disabled_terr_field = ['salesforceid_add_i']
            MakeFieldNonEditable(disabled_terr_field);
            SetDataInSalesforcePopup(result.dt_ReturnedTables[0][0]);
            MasterData["EditedData"] = result.dt_ReturnedTables[0][0];
            MasterData["SalesforceData"] = result.dt_ReturnedTables[0][0];
        }
        else {
            CustomAlert(result.Message);
            return false;
        }
    }
    else {
        CustomAlert("Something went wrong!");
        return false;
    }
}

function ClearSalesForceDataPopup() {
    $("#salesforceid_add_i").val('');
    $("#salesforce_name_add").val('');
    $("#salesforce_staus_add").val('');
    $("#salseforce_start_date_add").val('');
    $("#salesforce_end_date_add").val('');
    sessionStorage["SalesforceEndDate"] = "";
}

function SetDataInSalesforcePopup(data) {
    $("#salesforceid_add_i").val(data["Salesforce_ID"]);
    $("#salesforce_name_add").val(data["Salesforce_Name"]);

    $("#salseforce_start_date_add").val(data['Salesforce_Start_Date']);
    $("#salesforce_end_date_add").val(data["Salesforce_End_Date"]);
    if (data["Salesforce_End_Date"] != null) {
        sessionStorage["SalesforceEndDate"] = data["Salesforce_End_Date"];
        var SalesforceEndDateTime = new Date(sessionStorage["SalesforceEndDate"]);
        var todayDateTime = new Date(today);

        if (todayDateTime <= SalesforceEndDateTime) {
            $("#salesforce_staus_add").val('Active');
        }
        else {
            $("#salesforce_staus_add").val('Inactive');
        }
    }
    else {
        $("#salesforce_staus_add").val('Active');
    }

}

function GetSalesForcePopupData(mode) {

    var salesforceData = new Object();
    if (PrimarValidationSalesForce()) {
        salesforceData["Salesforce_ID"] = $("#salesforceid_add_i").val();
        salesforceData["Salesforce_Name"] = $("#salesforce_name_add").val();
        salesforceData["Salesforce_Status"] = $("#salesforce_staus_add").val();
        salesforceData["Salsesforce_Start_Date"] = $("#salseforce_start_date_add").val();
        salesforceData["Salesforce_End_Date"] = $("#salesforce_end_date_add").val();

        if (SlaesforceValueChange(salesforceData)) {
            return salesforceData;
        }
    }
    else {
        return null;
    }
}

function SlaesforceValueChange(NewData) {
    var EditedFlag = true;
    var count = 0;
    var OldData = MasterData["EditedData"];
    if (OldData["Salesforce_ID"] != NewData["Salesforce_ID"]) { count++; }
    else { EditedFlag = true; }
    if (OldData["Salesforce_Name"] != NewData["Salesforce_Name"]) { count++; }
    //if (OldData["Salesforce_Status"] != NewData["Salesforce_Status"]) { count++; }
    if (OldData["Salesforce_Start_Date"] == null && NewData["Salsesforce_Start_Date"] == "") { } else { if (OldData["Salesforce_Start_Date"] != NewData["Salsesforce_Start_Date"]) { count++; } }
    if (OldData["Salesforce_End_Date"] == null && NewData["Salesforce_End_Date"] == "") { } else { if (OldData["Salesforce_End_Date"] != NewData["Salesforce_End_Date"]) { count++; } }
    if (count == 0) {
        CustomAlert("No chages found.");
        return false;
    }
    else {
        return true;
    };
}

function PrimarValidationSalesForce() {
    if (!($("#salesforceid_add_i").val() != null && $("#salesforceid_add_i").val() != undefined && $("#salesforceid_add_i").val() != "")) { CustomAlert("Please enter the fieldforce id."); return false; }
    if (!($("#salesforce_name_add").val() != null && $("#salesforce_name_add").val() != undefined && $("#salesforce_name_add").val() != "")) { CustomAlert("Please enter the fieldforce name."); return false; }
    if (!($("#salesforce_staus_add").val() != null && $("#salesforce_staus_add").val() != undefined && $("#salesforce_staus_add").val() != "")) { CustomAlert("Please select fieldforce status."); return false; }
    if (!($("#salseforce_start_date_add").val() != null && $("#salseforce_start_date_add").val() != undefined && $("#salseforce_start_date_add").val() != "")) { CustomAlert('Please enter the start date.'); return false; }

    return true;
}

function SaveSalesforceData(mode, SalesForceDetails) {
    $("#loading").show();
    if (mode == "Add") {
        DeGrToolWebService.AddSalesForceDetails($('#username').val(), JSON.stringify(SalesForceDetails), popusaveSalesForce);
    }
    else if (mode == "Edit") {
        DeGrToolWebService.UpdateSalesforceDetails($('#username').val(), JSON.stringify(SalesForceDetails), popusaveSalesForce);
    }
}

    function popusaveSalesForce(data) {
        $("#loading").hide();
        if (data != "" && data != undefined) {
            var result = JSON.parse(data)
            CustomAlert(result.Message);
            if (result.Status == 1) {
                $('.close2').trigger("click");
            }
            else {
                $('.close2').trigger("click");
                return false;
            }
        }
        else {
            CustomAlert("Something went wrong!");
            $('.close2').trigger("click");
        }
    }
//================================================ Salseforce Function End =================================================

//=============================================== Territory Function Start =================================================
function GetTerritoryDetail(terr_id) {
    DeGrToolWebService.GetTerritoryDetails(sessionStorage["username"], $("#current_emp_salesforce").val(), $("#current_emp_terr").val(), OpenTerritoryDatadForEdit);
}

function OpenTerritoryDatadForEdit(data) {
    if (data != '' && data != undefined) {
        var result = JSON.parse(data);
        if (result.Status == 1) {
            ClearTerritoryDataInNewPopup();
            // whatfield you want to disable just add id of that field in the following Array
            var disabled_terr_field = ['new_terr_salesforce_id', 'new_terr_terr_id', 'new_terr_parent_terr_id', 'new_terr_manager_name', 'new_terr_manager_id', 'new_terr_zip_latitude', 'new_terr_zip_longitude']
            MakeFieldNonEditable(disabled_terr_field);
            //var hidefield = ['terr_salesforece_row', 'terr_terr_id_row', 'terr_accc_crtd_row', 'terr_acc_crtd_longitude_row', 'terr_parent_terr_row', 'terr_parent_terr_name_row', 'terr_zip_longitude_row', 'terr_zip_latitude_row', 'terr_manager_name_row', 'terr_manager_id_row'];
            //var hidefield = ['terr_salesforece_row', 'terr_accc_crtd_row', 'terr_acc_crtd_longitude_row', 'terr_rap_addr_longitude_row', 'terr_rap_addr_latitude_row', 'terr_zip_longitude_row', 'terr_zip_latitude_row'];
            
            HidePopupField(GetHidenFieldsForTerritoryEdit());
          
            SetTerritoryDataInNewPopup(result.dt_ReturnedTables[0][0]);
        }
        else {
            CustomAlert(result.Message);
            return false;
        }
    }
    else {
        CustomAlert("Something went wrong");
        return false;
    }
}

function ClearTerritoryDataInNewPopup() {
    $("#terr_modal_footer").html('');
    $("#new_terr_salesforce_id").val('');
    $("#new_terr_terr_id").val('');
    $("#new_terr_terr_name").val('');
    $("#new_terr_terr_type").val('');
    $("#new_terr_parent_territory").val('');
    $("#new_terr_parent_terr_id").val('');
    $("#new_terr_manager_id").val('');
    $("#new_terr_manager_name").val('');
    $("#new_terr_status").val('');
    $("#new_terr_start_date").val('');
    $("#new_terr_end_date").val('');
    sessionStorage["TerritoriedData"] = "";
    $("#new_terr_default_zip").val('');
    $("#new_terr_zip_latitude").val('');
    $("#new_terr_az_territory_no").val('');
    $("#new_terr_zip_longitude").val('');
    $("#new_terr_rep_add_latitude").val('');
    $("#new_terr_rep_addr_longitude").val('');
    $("#new_terr_acc_crtd_latitude").val('');
    $("#new_terr_acc_crtd_longitude").val('');
    $("#new_terr_role_code").val('');
}

function SetTerritoryDataInNewPopup(data) {
    if (data != '' && data != undefined) {
        MasterData["EditedData"] = null;
        MasterData["EditedData"] = data;
        var value = $("#p_terr").data('value');
        if (data["Salesforce_ID"] != null && data["Salesforce_ID"] != undefined) $("#new_terr_salesforce_id").val(data["Salesforce_ID"]);
        if (data["Territory_ID"] != null && data["Territory_ID"] != undefined) $("#new_terr_terr_id").val(data["Territory_ID"]);
        if (data["Territory_Name"] != null && data["Territory_Name"] != undefined) $("#new_terr_terr_name").val(data["Territory_Name"]);
        if (data["Territory_Type"] != null && data["Territory_Type"] != undefined) $("#new_terr_terr_type").val(data["Territory_Type"]);
        if (data["AZ_Territory_No"] != null && data["AZ_Territory_No"] != undefined) $("#new_terr_az_territory_no").val(data["AZ_Territory_No"]);
        if (data["Parent_Terr_ID"] != null && data["Parent_Terr_ID"] != undefined) $("#new_terr_parent_territory").val(data["Parent_Terr_ID"]);
        if (data["Manager_ID"] != null && data["Manager_ID"] != undefined) $("#new_terr_manager_id").val(data["Manager_ID"]);
        if (data["Manager_Name"] != null && data["Manager_Name"] != undefined) $("#new_terr_manager_name").val(data["Manager_Name"]);
        if (data["Territory_Status"] != null && data["Territory_Status"] != undefined) $("#new_terr_status").val(data["Territory_Status"]);
        if (data["Territory_Start_date"] != null && data["Territory_Start_date"] != undefined) $("#new_terr_start_date").val(data["Territory_Start_date"]);
        if (data["Territory_ZIP_latitude"] != null && data["Territory_ZIP_latitude"] != undefined) $("#new_terr_zip_latitude").val(data["Territory_ZIP_latitude"]);
        if (data["Territory_ZIP_longitude"] != null && data["Territory_ZIP_longitude"] != undefined) $("#new_terr_zip_longitude").val(data["Territory_ZIP_longitude"]);
        if (data["Territory_End_date"] != null && data["Territory_End_date"] != undefined) {
            $("#new_terr_end_date").val(data["Territory_End_date"]);
            sessionStorage["TerritoriedData"] = data["Territory_End_date"];
        }
        else {
            sessionStorage["TerritoriedData"] = "";
        }
        if (data["Default_Terr_ZIP"] != null && data["Default_Terr_ZIP"] != undefined) $("#new_terr_default_zip").val(data["Default_Terr_ZIP"]);
        if (data["Terr_ZIP_Latitude"] != null && data["Terr_ZIP_Latitude"] != undefined) $("#new_terr_zip_latitude").val(data["Terr_ZIP_Latitude"]);
        if (data["Terr_ZIP_Longitude"] != null && data["Terr_ZIP_Longitude"] != undefined) $("#new_terr_zip_longitude").val(data["Terr_ZIP_Longitude"]);
        if (data["Rep_Addr_Latitude"] != null && data["Rep_Addr_Latitude"] != undefined) $("#new_terr_rep_add_latitude").val(data["Rep_Addr_Latitude"]);
        if (data["Rep_Addr_Longitude"] != null && data["Rep_Addr_Longitude"] != undefined) $("#new_terr_rep_addr_longitude").val(data["Rep_Addr_Longitude"]);
        if (data["Acct_Cntrd_Latitude"] != null && data["Acct_Cntrd_Latitude"] != undefined) $("#new_terr_acc_crtd_latitude").val(data["Acct_Cntrd_Latitude"]);
        if (data["Acct_Cntrd_Longitude"] != null && data["Acct_Cntrd_Longitude"] != undefined) $("#new_terr_acc_crtd_longitude").val(data["Acct_Cntrd_Longitude"]);
        if (data["role_code"] != null && data["role_code"] != undefined) $("#new_terr_role_code").val(data["role_code"]);
    }
    else {
        CustomAlert(" issue to  retrive data from server");
    }
}

function GetTerritoryPopupData(mode) {
    var TerritoryValueObj = new Object();
    if (PrimaryValidationOnTerritoryPopup(mode)) {
        TerritoryValueObj["Salesforce_ID"] = $("#new_terr_salesforce_id").val();
        TerritoryValueObj["Territory_ID"] = $("#new_terr_terr_id").val();
        TerritoryValueObj["Territory_Name"] = $("#new_terr_terr_name").val();
        TerritoryValueObj["Territory_Type"] = $("#new_terr_terr_type").val();
        TerritoryValueObj["Parent_Terr_ID"] = $("#new_terr_parent_territory").val();
        TerritoryValueObj["Territory_Status"] = $("#new_terr_status").val();
        TerritoryValueObj["Territory_Start_Date"] = $("#new_terr_start_date").val();
        TerritoryValueObj["Territory_End_Date"] = $("#new_terr_end_date").val();
        TerritoryValueObj["Territory_Default_Zip"] = $("#new_terr_default_zip").val();
        TerritoryValueObj["Territory_Zip_Latitude"] = $("#new_terr_zip_latitude").val();
        TerritoryValueObj["AZ_Territory_No"] = $("#new_terr_az_territory_no").val();
        
        TerritoryValueObj["Territory_Zip_Longitude"] = $("#new_terr_zip_longitude").val();
        TerritoryValueObj["Rep_Addr_Latitude"] = $("#new_terr_rep_add_latitude").val();
        TerritoryValueObj["Rep_Addr_Longitude"] = $("#new_terr_rep_addr_longitude").val();
        TerritoryValueObj["Acct_Cntrd_Latitude"] = $("#new_terr_acc_crtd_latitude").val();
        TerritoryValueObj["Acct_Cntrd_Longitude"] = $("#new_terr_acc_crtd_longitude").val();
        TerritoryValueObj["Role_Code"] = $("#new_terr_role_code").val();
        if (mode != "Add") {
            if (ChageDetectionInTerritory(TerritoryValueObj)) {
                return TerritoryValueObj;
            }
            else {
                return null;
            }
        }
        else {
            return TerritoryValueObj;
        }
    }
    else {
        return null;
    }
}

function ChageDetectionInTerritory(NewData) {

    var count = 0;
    var OldData = MasterData["EditedData"];
    if (OldData["Salesforce_ID"] != NewData["Salesforce_ID"]) { count++; }

    if (OldData["Territory_ID"] == null && NewData["Territory_ID"] == "") { } else if (OldData["Territory_ID"] != NewData["Territory_ID"]) { count++; }
    if (OldData["Territory_Name"] == null && NewData["Territory_Name"] == "") { } else if (OldData["Territory_Name"] != NewData["Territory_Name"]) { count++; }
    if (OldData["Territory_Type"] == null && NewData["Territory_Type"] == "") { } else if (OldData["Territory_Type"] != NewData["Territory_Type"]) { count++; }
    if (OldData["Territory_Status"] == null && NewData["Territory_Status"] == "") { } else if (OldData["Territory_Status"] != NewData["Territory_Status"]) { count++; }
    if (OldData["Territory_Start_date"] == null && NewData["Territory_Start_Date"] == "") { } else { if (OldData["Territory_Start_date"] != NewData["Territory_Start_Date"]) { count++; } }
    if (OldData["Territory_End_date"] == null && NewData["Territory_End_Date"] == "") { } else { if (OldData["Territory_End_date"] != NewData["Territory_End_Date"]) { count++; } }
    if (OldData["Default_Terr_ZIP"] == null && NewData["Territory_Default_Zip"] == "") { } else { if (OldData["Default_Terr_ZIP"] != NewData["Territory_Default_Zip"]) { count++; } }
    if (OldData["Territory_ZIP_latitude"] == null && NewData["Territory_Zip_Latitude"] == "") { } else { if (OldData["Territory_ZIP_latitude"] != NewData["Territory_Zip_Latitude"]) { count++; } }
    if (OldData["Territory_ZIP_longitude"] == null && NewData["Territory_Zip_Longitude"] == "") { } else { if (OldData["Territory_ZIP_longitude"] != NewData["Territory_Zip_Longitude"]) { count++; } }
    if (OldData["Rep_Addr_Latitude"] == null && NewData["Rep_Addr_Latitude"] == "") { } else { if (OldData["Rep_Addr_Latitude"] != NewData["Rep_Addr_Latitude"]) { count++; } }
    if (OldData["Rep_Addr_Longitude"] == null && NewData["Rep_Addr_Longitude"] == "") { } else { if (OldData["Rep_Addr_Longitude"] != NewData["Rep_Addr_Longitude"]) { count++; } }
    if (OldData["Acct_Cntrd_Latitude"] == null && NewData["Acct_Cntrd_Latitude"] == "") { } else { if (OldData["Acct_Cntrd_Latitude"] != NewData["Acct_Cntrd_Latitude"]) { count++; } }
    if (OldData["Acct_Cntrd_Longitude"] == null && NewData["Acct_Cntrd_Longitude"] == "") { } else { if (OldData["Acct_Cntrd_Longitude"] != NewData["Acct_Cntrd_Longitude"]) { count++; } }
    if (OldData["AZ_Territory_No"] == null && NewData["AZ_Territory_No"] == "") { } else { if (OldData["AZ_Territory_No"] != NewData["AZ_Territory_No"]) { count++; } }
    
    if (OldData["role_code"] != NewData["Role_Code"]) { count++; }

    if (count == 0) {
        CustomAlert("No chages found.");
        return false;
    }
    else {
        return true;
    };
}

function PrimaryValidationOnTerritoryPopup(mode) {
    if (mode == "Add") {
        if (!($("#new_terr_salesforce_id").val() != null && $("#new_terr_salesforce_id").val() != undefined && $("#new_terr_salesforce_id").val() != '')) { CustomAlert("Please select fieldforce id."); return false; }
        if (!($("#new_terr_terr_id").val() != null && $("#new_terr_terr_id").val() != undefined && $("#new_terr_terr_id").val() != '')) { CustomAlert("Please enter geography id."); return false; }
        if (!($("#new_terr_terr_name").val() != null && $("#new_terr_terr_name").val() != undefined && $("#new_terr_terr_name").val() != '')) { CustomAlert("Please enter geography name."); return false; }
        //if (!($("#new_terr_parent_territory").val() != null && $("#new_terr_parent_territory").val() != undefined && $("#new_terr_parent_territory").val() != '')) { CustomAlert('Please select parent geography.'); return false; }
        //if (!($("#new_terr_terr_type").val() != null && $("#new_terr_terr_type").val() != undefined && $("#new_terr_terr_type").val() != '')) { CustomAlert("Please select geography type."); return false; }
        //if (!($("#new_terr_role_code").val() != null && $("#new_terr_role_code").val() != undefined && $("#new_terr_role_code").val() != '')) { CustomAlert('Please select role.'); return false; }
        //if (!($("#new_terr_status").val() != null && $("#new_terr_status").val() != undefined && $("#new_terr_status").val() != '')) { CustomAlert('Please select geography status.'); return false; }
        if (!($("#new_terr_start_date").val() != null && $("#new_terr_start_date").val() != undefined && $("#new_terr_start_date").val() != '')) { CustomAlert('Please enter geography start date.'); return false; }
        
        

    }
    else {

        if (!($("#new_terr_salesforce_id").val() != null && $("#new_terr_salesforce_id").val() != undefined && $("#new_terr_salesforce_id").val() != '')) { CustomAlert("Please select fieldforce id."); return false; }
        if (!($("#new_terr_terr_id").val() != null && $("#new_terr_terr_id").val() != undefined && $("#new_terr_terr_id").val() != '')) { CustomAlert("Please enter geography id."); return false; }
        if (!($("#new_terr_terr_name").val() != null && $("#new_terr_terr_name").val() != undefined && $("#new_terr_terr_name").val() != '')) { CustomAlert("Please enter geography name."); return false; }
        if (!($("#new_terr_terr_type").val() != null && $("#new_terr_terr_type").val() != undefined && $("#new_terr_terr_type").val() != '')) { CustomAlert("Please enter geography type."); return false; }
        //if (!($("#new_terr_status").val() != null && $("#new_terr_status").val() != undefined && $("#new_terr_status").val() != '')) { CustomAlert('Please select geography status.'); return false; }
    }
    return true;
}

function SaveTerritoriesDetails(mode, TerritoryObject) {
    $("#loading").show();
    if (mode == "Add") {
        DeGrToolWebService.AddTerritoryDetails($('#username').val(), JSON.stringify(TerritoryObject), GeoGraphyAddSucess);
    }
    else if (mode == "Edit") {
        DeGrToolWebService.UpdateTerritoryDetails($('#username').val(), JSON.stringify(TerritoryObject), popusaveSalesForce);
    }
}

function GeoGraphyAddSucess(data) {
    if (data != "" && data != undefined) {
        var result = JSON.parse(data)
        CustomAlert(result.Message);
        var IncludeInvalid = "No";
        if (result.Status == 1) {
            $('#NewTerritoryAdd .close').trigger("click");

            if ($("#IncludeInactiveRoster").prop('checked')) {
                IncludeInvalid = "Yes";
            }
            DeGrToolWebService.GetRoasterTabelData($('#username').val(), ConvertArrayToString($('#select_Salesforce_id').val()), $("#Geography_input").val(), ConvertArrayToString($("#Territory_Type").val()), ConvertArrayToString($("#Role_filter_select").val()), today, IncludeInvalid, datatableset);
            
        }
        else {
            if ($("#IncludeInactiveEmployee").prop('checked')) {
                IncludeInvalid = "Yes";
            }
            DeGrToolWebService.GetRoasterTabelData($('#username').val(), ConvertArrayToString($('#select_Salesforce_id').val()), $("#Geography_input").val(), ConvertArrayToString($("#Territory_Type").val()), ConvertArrayToString($("#Role_filter_select").val()), today, IncludeInvalid, datatableset);

            return false;
        }
    }
    else {
        CustomAlert("Something went wrong!");
        $('.close2').trigger("click");
    }
}
//================================================ Territory Function End ==================================================

//================================================ Employee Function Starts ================================================
function GetEmployeeVaues(value) {
    DeGrToolWebService.GetEmployeeDetailsWeb(sessionStorage["username"], $("#current_emp_id").val(), OpenEmployeeDatadForEdit);

}

function OpenEmployeeDatadForEdit(data) {
    if (data != '' && data != undefined) {
        var result = JSON.parse(data);
        if (result.Status == 1) {
            ClearEmployeePopup();

            // whatfield you want to disable just add id of that field in the following Array
            var disabled_terr_field = ['new_emp_id', 'new_emp_user_id']
            MakeFieldNonEditable(disabled_terr_field);
            //var hidefields = ['FullNamePrefered', 'FullName'];
            HidePopupField(GetHidenFieldsForEmployeeEdit())
            SetEmployeeDataPopup(result.dt_ReturnedTables[0][0]);
        }
        else {
            CustomAlert(result.Message);
            return false;
        }
    }
    else {
        CustomAlert("Something went wrong!");
        return false;
    }
}

function ClearEmployeePopup() {

    $("#new_emp_id").val('');
    $("#new_emp_user_id").val('');
    $("#new_emp_empID").val('');
    $("#new_emp_department").val('');
    $("#new_emp_title").val('');

    //var sel = document.getElementById('new_emp_title');
    //    sel.style.color = "#000";
    //    sel.style.fontSize = "13px";
    //    sel.style.fontStyle = "italic";
    //    sel.style.opacity = "0.5";

    $("#new_emp_first_name").val('');
    $("#new_emp_middle_name").val('');
    $("#new_emp_last_name").val('');
    $("#new_emp_suffix").val('');
    $("#new_emp_preferred_name").val('');
    //$("#new_emp_full_name").val('');
    //$("#new_emp_full_name_preffered").val('');
    $("#new_emp_hire_date").val('');
    $("#new_emp_field_start_date").val('');
    //$("#new_emp_field_end_date").val('');
    $("#new_emp_position_start_date").val('');
    $("#new_emp_position_end_date").val('');
    $("#new_emp_type").val('');
    //var sel = document.getElementById('new_emp_type');
    //sel.style.color = "#000";
    //sel.style.fontSize = "13px";
    //sel.style.fontStyle = "italic";
    //sel.style.opacity = "0.5";
    $("#new_emp_status").val('');
    $("#new_emp_prid").val('');
    $("#new_emp_az_employee_id").val('');
    $("#new_emp_az_email_id").val('');
    
    
    $("#new_emp_termination_date").val('');

    sessionStorage["EmpEndDate"] = "";
    //$("#new_emp_home_addr1").val('');
    //$("#new_emp_home_addr2").val('');
    //$("#emp_home_city").val('');
    //$("#emp_home_zip").val('');
    //$("#emp_home_state").val('');

    //$("#new_emp_mail_addr1").val('');
    //$("#new_emp_mail_addr2").val('');
    //$("#new_emp_mial_city").val('');
    //$("#new_emp_mailing_zip").val('');
    //$("#new_emp_mailing_state").val('');

    //$("#new_emp_company_email").val('');
    //$("#new_emp_personal_emial").val('');

    //$("#new_emp_bussiness_phone").val('');
    //$("#new_emp_mobile_phone").val('');
    //$("#new_emp_home_phone").val('');

}

function SetEmployeeDataPopup(data) {
    MasterData["EditedData"] = null;
    MasterData["EditedData"] = data;
    $("#new_emp_id").val(data['Employee_ID']);
    $("#new_emp_user_id").val(data['User_ID']);
    $("#new_emp_empID").val(data['EmpID']);
    $("#new_emp_department").val(data['Department']);
    if (data["Employee_Title"] != null && data["Employee_Title"] != undefined && data["Employee_Title"] != "") { $("#new_emp_title").val(data['Employee_Title']); MakeSelcetnormal("new_emp_title"); }
    else {
        MakeFistSelcetSameAsPlaceHolder('new_emp_title');
    }
    ////$("#new_emp_title").val(data['Employee_Title']);
    $("#new_emp_first_name").val(data['First_Name']);
    $("#new_emp_middle_name").val(data['Middle_Name']);
    $("#new_emp_last_name").val(data['Last_Name']);
    $("#new_emp_suffix").val(data['Suffix']);
    $("#new_emp_preferred_name").val(data['Preferred_Name']);
    //$("#new_emp_full_name").val(data['Full_Name']);
    //$("#new_emp_full_name_preffered").val(data['Full_Name_Preferred']);
    $("#new_emp_hire_date").val(data['Hire_Date']);
    $("#new_emp_field_start_date").val(data['Field_Start_Date']);
    //$("#new_emp_field_end_date").val(data['Fiels_End_Date']);
    $("#new_emp_position_start_date").val(data['Position_Start_Date']);
    $("#new_emp_position_end_date").val(data['Position_End_Date']);
    $("#new_emp_prid").val(data["PRID"]);
    $("#new_emp_az_employee_id").val(data["AZ_Employee_ID"]);
    $("#new_emp_az_email_id").val(data["AZ_Email_ID"]);
    
    //$("#new_emp_type").val(data['Employee_Type_Code']);
    if (data["Employee_Type"] != null && data["Employee_Type"] != undefined && data["Employee_Type"] != "") { $("#new_emp_type").val(data['Employee_Type']); MakeSelcetnormal("new_emp_type"); }
    else {
        MakeFistSelcetSameAsPlaceHolder('new_emp_type');
    }

    //$("#new_emp_status").val(data['Employee_Status']);
    $("#new_emp_termination_date").val(data['Termination_Date']);
    if (data['Termination_Date'] != null) {
        sessionStorage["EmpEndDate"] = data['Termination_Date'];
        var SalesforceEndDateTime = new Date(data['Termination_Date']);
        var todayDateTime = new Date(today);

        if (todayDateTime <= SalesforceEndDateTime) {
            $("#new_emp_status").val('Active');
        }
        else {
            $("#new_emp_status").val('Inactive');
        }
    }
    else {
        $("#new_emp_status").val('Active');
    }
    //$("#new_emp_home_addr1").val(data['emp_homeaddr1']);
    //$("#new_emp_home_addr2").val(data['emp_homeaddr2']);
    //$("#emp_home_city").val(data['emp_homecity']);
    //$("#emp_home_zip").val(data['emp_home_zip']);
    //$("#emp_home_state").val(data['emp_home_state']);

    //$("#new_emp_mail_addr1").val(data['emp_m_addr1']);
    //$("#new_emp_mail_addr2").val(data['emp_m_addr2']);
    //$("#new_emp_mial_city").val(data['emp_m_city']);
    //$("#new_emp_mailing_zip").val(data['emp_m_state']);
    //$("#new_emp_mailing_state").val(data['emp_m_zip']);

    //$("#new_emp_company_email").val(data["emp_company_email"]);
    //$("#new_emp_personal_emial").val(data['emp_personal_email']);


    //$("#new_emp_bussiness_phone").val(data['emp_office_phone']);
    //$("#new_emp_mobile_phone").val(data['emp_mobile_phone']);
    //$("#new_emp_home_phone").val(data['emp_home_phone']);

}
//================================================= Employee Function Ends =================================================

//=================================================== Roster Tab Fucntions ends =================================================


//======================================================== Employee Tab Functions Start ====================================================
function employeeTableSet(data) {
    if (data != "" && data != undefined) {
        var rosterTblData = JSON.parse(data);
        if (rosterTblData.Status == 1) {
            $("#RosterCounts").hide();
            $("#EmployeeCounts").show();
        
            $("#EmployeeCount").text(rosterTblData["dt_ReturnedTables"][1][0]["total_employee"]);
            $("#ActiveEmployeeCount").text(rosterTblData["dt_ReturnedTables"][1][0]["active_employee"]);
            $("#InactiveEmployeeCount").text(rosterTblData["dt_ReturnedTables"][1][0]["inactive_employee"]);
            var RosterTabel = rosterTblData["dt_ReturnedTables"][0];

            //debugger;
            var table = "<table class='table stripe cust_table' id='tbl_employee' ><thead> " +
                          "<tr class='cust_table_header'><th style='text-align:center' id='btn_view'>View </th>" +
                          "<th style='text-align:center;padding-left:0px'>Employee Name </th>  <th style='text-align:center'>Role</th>" +
                          "  <th style='text-align:center'>Assigned Geography</th>    <th style='text-align:center'>Company Email</th>" +
                          " <th style='text-align:center'> Mobile Phone</th><th style='text-align:center'> Office Phone</th> <th style='text-align:center'>Hire Date </th>" +
                          " <th style='text-align:center'>Field Start Date </th> <th style='text-align:center'> Manager Name</th>  <th style='text-align:center'> Parent Geography</th>" +
                          "<th style='text-align:center'>2nd Level Parent</th>" +
                          "<th style='text-align:center'>Employee Status</th>" +
                          " </thead>  <tbody>";
            for (var i = 0 ; i < RosterTabel.length; i++) {
                var row = RosterTabel[i];
                var row = RosterTabel[i];
                var row = replaceNull(row);
                var fullName = '';
                if (row["Last_Name"] != '') fullName += row["Last_Name"] + ', ';
                if (row["First_Name"] != '') fullName += row["First_Name"] + ' ';
                if (row["Middle_Name"] != '') fullName += row["Middle_Name"];
                if (row["Suffix"] != '' && data["Suffix"] != null) fullName += ', ' + row["Suffix"];
                var fullManagerName = '';
                if (row["manager_last_name"] != '') fullManagerName += row["manager_last_name"] + ', ';
                if (row["manager_first_name"] != '') fullManagerName += row["manager_first_name"] + ' ';
                if (row["manager_middle_name"] != '') fullManagerName += row["manager_middle_name"];
                if (row["manager_suffix"] != '' && row["manager_suffix"] != null) fullManagerName += ', ' + row["manager_suffix"];

                table += '<tr style="text-align:left">' +
                    '<td class="text-center"><button class="btn btn-sm btn-link" title="View"  onclick="return  view_popup_employee(this,\'' + row["Employee_ID"] + '\',\'' + row['Territory_ID'] + '\',\'' + row['Salesforce_ID'] + '\')"  ><i class="fa fa-expand" aria-hidden="true"></i></button></td>' +
                '<td style="text-align:left">' + fullName + '</td>' + '<td >' + row["role_code"] + '</td>';
                if (row["Territory_ID"] != "") { table += '<td >' + row["Territory_ID"] + ' - ' + row["Territory_Name"] + '</td>'; }
                else { table += '<td style="text-align:left"> N/A</td>'; }
                table += '<td style="color:#1e88e5;;cursor:pointer;" onClick=OpenOutlookDoc(\'' + row["Company_Email"] + '\')>' + row["Company_Email"] + '</td>' +
                '<td>' + row["Mobile_Phone"] + '</td><td>' + row["Office_Phone"] + '</td>' + '<td>' + row["Employee_hire_date"] + '</td>' + '<td>' + row["Employee_field_Start_date"] + '</td>';
                table += '<td>' + fullManagerName + '</td>';
                if (row["pID"] != null && row["pID"] != "") { table += "<td style='text-align:left'>" + row["pID"] + ' - ' + row["p_name"].toUpperCase() + "</td>"; }
                else { table += "<td style='text-align:left'> N/A </td>"; }
                
                if (row["gID"] != null && row["gID"] != "") { table += "<td style='text-align:left'>" + row["gID"] + ' - ' + row["g_name"].toUpperCase() + "</td>"; }
                else { table += "<td style='text-align:left'> N/A </td>"; }
                var terrStatus = "";
                if (row["Employee_termination_date"] != "" && row["Employee_termination_date"] != null) {
                    var CompareDatetime = new Date(row["Employee_termination_date"]);
                    var todayDateTime = new Date(today);
                    if (todayDateTime <= CompareDatetime) { terrStatus = 'Active'; }
                    else { terrStatus = 'Inactive'; }
                }
                else { terrStatus = 'Active'; }
                table += "<td style='text-align:left'> " + terrStatus + " </td>";
                table += '</tr>';
            }
            table += "</tbody></table>";
            var IncludeInvalidFlg = "";
            if ($("#IncludeInactiveEmployee").prop('checked')) {
                IncludeInvalidFlg = true;
            }
            $('#employeeTable').html(table);
            var $table = $("#employeeTable table");
            if (!$.fn.DataTable.isDataTable("#tbl_employee")) {
                oTableEmployee = $('#tbl_employee').DataTable({
                    "dom": '<<"#EmpInactive.top"lf>>rtip',
                    "bInfo": false,
                    "autoWidth": false,
                    "lengthMenu": [[10, 12, 15, 20, 25, 50, -1], [10, 12, 15, 20, 25, 50, "All"]],
                    "pageLength": 15,
                    "bFilter": true,
                    "scrollX": true,
                    stateSave: true,
                    //fixedHeader: {
                    //    header: true,
                    //},
                    //"sScrollY": "0px",
                    "fixedColumns": { leftColumns: 3 },
                    "order": [[1, "asc"]],
                    //"sScrollY": "0px",
                    //"fnDrawCallback": function () {
                    //    var $dataTable = $table.dataTable();
                    //    $dataTable.fnAdjustColumnSizing(false);

                    //    // TableTools
                    //    if (typeof (TableTools) != "undefined") {
                    //        var tableTools = TableTools.fnGetInstance(table);
                    //        if (tableTools != null && tableTools.fnResizeRequired()) {
                    //            tableTools.fnResizeButtons();
                    //        }
                    //    }
                    //    //
                    //    var $dataTableWrapper = $table.closest(".dataTables_wrapper");
                    //    var panelHeight = $dataTableWrapper.parent().height() - $("#EmpInactive").height();

                    //    var toolbarHeights = 0;
                    //    $dataTableWrapper.find(".fg-toolbar").each(function (i, obj) {
                    //        toolbarHeights = toolbarHeights + $(obj).height();
                    //    });

                    //    var scrollHeadHeight = $dataTableWrapper.find(".dataTables_scrollHead").height();
                    //    var height = panelHeight - toolbarHeights - scrollHeadHeight;
                    //    $dataTableWrapper.find(".dataTables_scrollBody").height(height - 41);

                    //    $dataTable._fnScrollDraw();
                    //}
                });
            }
            var str   ="";
            if (EmployeeEditRights) {
                str = '<div style="margin: 0 auto;width:70%;position: relative;text-align: center;font-size: 14px;"><label>Include Inactive</label>  <label class="switch">' +
                 '<input type="checkbox" onchange="EmployeeIncludeInactiveRecored(this);" id="IncludeInactiveEmployee">' +
                 '<div class="slider round"></div>' +
                 '</label><div style="display: inline-table;float: right;">' +
                                    '<input type="button" class="btn btn-success fadeInRight cust_btn1" Title="Add New Employee" style="margin-right:10px;height: 26px;vertical-align: middle;padding: 0px 15px;" value="Add New Employee" onclick="OpenEmployeeAddModal()">' +
                                '</div></div>';
            }
            else {
                str = '<div style="margin: 0 auto;width:70%;position: relative;text-align: center;font-size: 14px;"><label>Include Inactive</label>  <label class="switch">' +
                 '<input type="checkbox" onchange="EmployeeIncludeInactiveRecored(this);" id="IncludeInactiveEmployee">' +
                 '<div class="slider round"></div>' +
                 '</label><div style="display: inline-table;float: right;">' +
                                    
                                '</div></div>';
            }
            $("#EmpInactive").append(str);

            if (IncludeInvalidFlg) {
                $("#IncludeInactiveEmployee").prop('checked', true)
            }
            setTimeout(function () {
                oTableEmployee.draw();
            }, 300);
        }
        else {
            CustomAlert(rosterTblData["Message"]);
        }
    }
    else {
        CustomAlert(" No data found.");
    }
    $("#loading").hide();
}

function view_popup_employee(id, emp_id) {
    $("#loading").show();
    DeGrToolWebService.GetRosterEmployeeDataForEmployeePopup($('#username').val(), emp_id, SetEmployeeTabPopupData);

    DeGrToolWebService.GetRosterEmployeeType(sessionStorage["username"], FillEmployeeDropdown);

    DeGrToolWebService.GetRosterEmployeeTitle(sessionStorage["username"], FillEmployeeTitleDropdown);

    DeGrToolWebService.GetRosterTerritoryEmployeeStaus(sessionStorage["username"], FillTerritoryEmployeeStausDropdown);

    return false;
}

function SetEmployeeTabPopupData(data) {
    clearEmployeeTabPopup();
    $("#RecentChangeData").html('');
    if (data != null && data != undefined) {
        var Result = JSON.parse(data)
        if (Result.Status == 1) {
            NotAssignAsperment = false;
            MasterData["EmpData"] = Result["dt_ReturnedTables"][0][0];
            FillEmployeeTabPopupData(Result["dt_ReturnedTables"][0][0]);
            $('#MyEmployeeModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $("#EmployeeRecentChangeData").html('');
            $("#EmpassignTableBody").html('');
            if (Result["dt_ReturnedTables"][1] != null) {
                fillRoleInEmployeeData(Result["dt_ReturnedTables"][1]);
            }
            if (Result["dt_ReturnedTables"][2] != null) {
                FillEmployeeLastEditSection(Result["dt_ReturnedTables"][2]);
            }
        }
        else {
            CustomAlert(Result.Message);
        }
        $("#loading").hide();
    }
    else {
    }
}

function fillRoleInEmployeeData(data) {
    var str = '';
    var startdate = "";
    var endDate = "";

    for (var i = 0 ; i < data.length; i++) {
        if (data[i]['Position_Start_Date'] == null) { startdate = ""; }
        else {
            startdate = data[i]['Position_Start_Date']
        }
        if (data[i]['Position_End_Date'] == null) { endDate = ""; }
        else {
            endDate = data[i]['Position_End_Date']
        }
        var TodayDateTime = new Date(today);
        var PositionEndDateTime = new Date(endDate);

        if (data[i]["Employee_ID"] != 'vacant') {
            if (!NotAssignAsperment) {
                if (PositionEndDateTime > TodayDateTime) {
                    if (data[i]["Employee_Geography_Status"] == PrimaryVacancyCode) {
                        NotAssignAsperment = true;
                    }
                    else {
                        NotAssignAsperment = false;
                    }
                }
                else if (endDate == "") {
                    NotAssignAsperment = true;
                }
            }
        }
        data[i] = replaceNull(data[i]);
        var fullName = '';
        if (data[i]["Last_Name"] != '') fullName += data[i]["Last_Name"] + ', ';
        if (data[i]["First_Name"] != '') fullName += data[i]["First_Name"] + ' ';
        if (data[i]["Middle_Name"] != '') fullName += data[i]["Middle_Name"];
        if (data[i]["Suffix"] != '') fullName += ', ' + data[i]["Suffix"] + '';
        str += '  <tr > <td>' + data[i]['Salesforce_ID'] + ' - ' + data[i]['Salesforce_Name'] + '</td> <td> ' + data[i]['Territory_ID'] + ' - ' + data[i]['Territory_Name'] + '</td> <td> ' + data[i]['Role_Code'] + ' </td> <td> ' + startdate + '</td> <td> ' + endDate + '</td> <td> ' + data[i]['Status_Desc'] + '</td> <td style="float: right;"> ';
        if (RemoveEmployeeRightsFlag) {
            str += '<div style="cursor: pointer;color:#1e88e5;font-weight:normal" data-emppostitonenddate="' + endDate + '" data-empname="' + fullName + '" data-terrname="' + data[i]['Territory_ID'] + ' - ' + data[i]['Territory_Name'] + '"  onClick="ClearEndDatePopup(this,\'' + data[i]['Territory_ID'] + '\',\'' + data[i]['Salesforce_ID'] + '\',\'' + data[i]['sr_no'] + '\')">Edit</div></td></tr>'
        }
        else {
            str += '</td></tr>';
        }



    }
    $("#EmpassignTableBody").html(str);
}

function FillEmployeeTabPopupData(data) {
    if (data["Employee_ID"] != null) {
        $("#emp_tab_emp_id").html(data["Employee_ID"]);
        $("#current_emp_id").val(data["Employee_ID"]);
        $("#hidEmployee").val(data["Employee_ID"]);
        
    }

    else { $("#current_emp_id").val(''); $("#hidEmployee").val(''); }
    if (data["Department"] != null) $("#emp_tab_emp_department").html(data["Department"]);
    if (data["User_ID"] != null) $("#emp_tab_emp_user_id").html(data["User_ID"]);
    if (data["Employee_title_desc"] != null) $("#emp_tab_emp_title").html(data["Employee_title_desc"]);
    var fullName = '';
    if (data["Last_Name"] != '') fullName += data["Last_Name"] + ', ';
    if (data["First_Name"] != '') {fullName += data["First_Name"] + ' '; $("#hidEmployeeName").val(data["First_Name"]);}
    if (data["Middle_Name"] != '') fullName += data["Middle_Name"];
    if (data["Suffix"] != '') fullName += ', ' + data["Suffix"] + '';
    $("#emp_tab_emp_Name").html(fullName);
    $("#EmployeeProfile").html(fullName)
    $("#MyEmployeeModalTitle").html('Employee Profile:   ' + fullName);
    var fullName = '';
    if (data["Last_Name"] != '') fullName += data["Last_Name"] + ', ';
    if (data["Preferred_Name"] != '') { fullName += data["Preferred_Name"] + ' '; }
    else { if (data["First_Name"] != '') fullName += data["First_Name"] + ' '; }
    if (data["Middle_Name"] != '') fullName += data["Middle_Name"];
    if (data["Suffix"] != '') fullName += ', ' + data["Suffix"] + '';
    $("#emp_tab_emp_Prefferd_name").html(data["Preferred_Name"]);
    //if (data["Preferred_Name"] != '') { $("#emp_tab_emp_Prefferd_name").html(fullName); }
    if (data["emp_hire_date"] != null) $("#emp_tab_emp_hire_date").html(data["emp_hire_date"]);
    if (data["emp_field_start_date"] != null) $("#emp_tab_emp_field_Start_Date").html(data["emp_field_start_date"]);
    if (data["emp_termination_date"] != null) $("#emp_tab_emp_Termination_Date").html(data["emp_termination_date"]);
    if (data["Employee_type_desc"] != null) $("#emp_tab_emp_type").html(data["Employee_type_desc"]);
    if (data["AZ_Email_ID"] != null) $("#emp_tab_az_email_id").html(data["AZ_Email_ID"]);
    if (data["AZ_Employee_ID"] != null) $("#emp_tab_az_emp_id").html(data["AZ_Employee_ID"]);
    if (data["PRID"] != null) $("#emp_tab_emp_prid").html(data["PRID"]);
    //if (data["Employee_Status"] != null) $("#emp_tab_emp_status").html(data["Employee_Status"]);

    if (data['emp_termination_date'] != null) {
        var SalesforceEndDateTime = new Date(data['emp_termination_date']);
        var todayDateTime = new Date(today);
        if (todayDateTime <= SalesforceEndDateTime) { $("#emp_tab_emp_status").html('Active'); }
        else { $("#emp_tab_emp_status").html('Inactive'); }
    }
    else { $("#emp_tab_emp_status").html('Active'); }
    var homeaddrstr = '';
    if (data["Home_Address1"] != null) {
        homeaddrstr += data["Home_Address1"];
        if (data["Home_Address2"] != null && data["Home_Address2"] != "") homeaddrstr += ', ' + data["Home_Address2"];
        if (data["Home_City"] != null && data["Home_City"] != "") homeaddrstr += ', ' + data["Home_City"];
        if (data["Home_State"] != null && data["Home_State"] != "") homeaddrstr += ', ' + data["Home_State"];
        if (data["Home_Zip"] != null && data["Home_Zip"] != "") homeaddrstr += ' ' + data["Home_Zip"];
        $("#emp_tab_emp_home_address").html(homeaddrstr);
    }
    var mobileAddr = '';
    if (data["M_Address1"] != null) {
        mobileAddr += data["M_Address1"];
        if (data["M_Address2"] != null && data["M_Address2"] != "") mobileAddr += ', ' + data["M_Address2"];
        if (data["M_City"] != null && data["M_City"] != "") mobileAddr += ', ' + data["M_City"];
        if (data["M_State"] != null && data["M_State"] != "") mobileAddr += ', ' + data["M_State"];
        if (data["M_Zip"] != null && data["M_Zip"] != "") mobileAddr += ' ' + data["M_Zip"];
        $("#emp_tab_emp_mail_address").html(mobileAddr);
    }
    var ShipingAddr = '';
    if (data["Ship_to_Address"] != null) {
        ShipingAddr += data["Ship_to_Address"];
        if (data["Ship_to_Address2"] != null && data["Ship_to_Address2"] != "") ShipingAddr += ', ' + data["Ship_to_Address2"];
        if (data["Ship_to_city"] != null && data["Ship_to_city"] != "") ShipingAddr += ', ' + data["Ship_to_city"];
        if (data["Ship_to_State"] != null && data["Ship_to_State"] != "") ShipingAddr += ', ' + data["Ship_to_State"];
        if (data["Ship_to_Zip"] != null && data["Ship_to_Zip"] != "") ShipingAddr += ' ' + data["Ship_to_Zip"];
        $("#emp_tab_emp_shiping_address").html(ShipingAddr);
    }
    if (data["Company_Email"] != null) $("#emp_tab_company_email").html(data["Company_Email"]);
    if (data["Personal_Email"] != null) $("#emp_tab_personal_email").html(data["Personal_Email"]);
    if (data["Office_Phone"] != null) $("#emp_tab_bussiness_phone").html(data["Office_Phone"]);
    if (data["Mobile_Phone"] != null) $("#emp_tab_mobile_phone").html(data["Mobile_Phone"]);
    if (data["Home_Phone"] != null) $("#emp_tab_emp_home_phone").html(data["Home_Phone"]);
}

function FillEmployeeLastEditSection(data) {
    $("#RecentChangeData").html('');
    var newRecet = "";
    for (var i = 0; i < data.length; i++) {
        if (data[i]["Edited_table"] == "Fieldforce Data") {
            var resultObject = FillSaleforceHistory2(data[i]);
        }
        else if (data[i]["Edited_table"] == "Geography Data") {
            var resultObject = FillTerritoryHistory2(data[i]);
        }
        else if (data[i]["Edited_table"] == "Employee Data") {
            sessionStorage["EditSectionName"] = data[i]["Edited_Section"];
            if (data[i]["Edited_Section"] == "Shipping Address") {
                var resultObject = FillEmployeeAdddresHistory2(data[i]);
            }
            else if (data[i]["Edited_Section"] == "Mailing Address") {
                var resultObject = FillEmployeeAdddresHistory2(data[i]);
            }
            else if (data[i]["Edited_Section"] == "Sample Shipment Locker") {
                var resultObject = FillEmployeeAdddresHistory2(data[i]);
            }
            else {
                var resultObject = FillEmployeeHistory2(data[i]);
            }
        }
        if (resultObject.NewValue.length > 0) {
            for (var j = 0; j < resultObject.NewValue.length; j++) {

                if (resultObject.NewValue[j]["name"] == "EmpID") {
                    resultObject.NewValue[j]["name"] = "Employee ID";
                }

                newRecet += '   <div > ' +
     '<div style="display: inline-table; width: 12%"> ' +
      data[i]["LastEditedDate"] + '</div> ' +
    '<div style="display: inline-table; width: 15%"> '
    + data[i]["Edited_table"] +
    '</div> ' +
                '<div style="display: inline-table; width: 20%"> ' +
                 resultObject.NewValue[j]["name"] + '</div> ' +

                '<div style="display: inline-table; width: 20%"> ' +
                 resultObject.OldValue[j]["value"] + '</div> ' +

                '<div style="display: inline-table; width: 20%"> ' +
                 resultObject.NewValue[j]["value"] + '</div> ' +
                '<div style="display:inline-table;width: 10%;text-align:center"> ' +
             '' + data[i]["Edited_By"] + ' </div>  <div class="lastcol" style="float: right;"> ' +
    '</div>';
            }

        }
    }
    if (data.length == 5) {
        newRecet += '<div id="Div_LoadMoreData" style="text-align:center"><a href="javascript:void(0)" onclick="LoadMoreEmployeeDetails(this,6)"  data-detail="EmployeeRecentChangeData" data-attr="show">Load More Details</a></div>';
    }
    $("#EmployeeRecentChangeData").append(newRecet);
}

function clearEmployeeTabPopup() {
    $("input[name=address]").css('display', 'none');
    $("#emp_tab_emp_id").html('');
    $("#emp_tab_emp_department").html('');
    $("#emp_tab_emp_user_id").html('');
    $("#emp_tab_emp_title").html('');
    $("#emp_tab_emp_Name").html('');
    $("#emp_tab_emp_Prefferd_name").html('');
    $("#emp_tab_emp_hire_date").html('');
    $("#emp_tab_emp_field_Start_Date").html('');
    $("#emp_tab_emp_Termination_Date").html('');
    $("#emp_tab_az_email_id").html('');
    $("#emp_tab_az_emp_id").html('');
    $("#emp_tab_emp_prid").html('');
    
    
    
    $("#emp_tab_emp_geo_status").html('');
    $("#emp_tab_emp_type").html('');
    $("#emp_tab_emp_status").html('');
    $("#emp_tab_emp_home_address").html('');
    $("#emp_tab_emp_mail_address").html('');
    $("#emp_tab_emp_shiping_address").html('');
    $("#emp_tab_company_email").html('');
    $("#emp_tab_personal_email").html('');
    $("#emp_tab_bussiness_phone").html('');
    $("#emp_tab_mobile_phone").html('');
    $("#emp_tab_emp_home_phone").html('');

}

function LoadMoreEmployeeDetails(id, index) {
    $("#loading").show();
    DeGrToolWebService.GetMoreLastChangeData($('#username').val(), '', '', $("#current_emp_id").val(), index, AppenMoreEmployeeRecenteData);

}

function AppenMoreEmployeeRecenteData(data) {
    var result = JSON.parse(data);
    if (result.Status == 1) {
        var newRecet = "";
        $("#Div_LoadMoreData").remove();
        if (result["dt_ReturnedTables"][0] != null) {
            var data = result["dt_ReturnedTables"][0];
            
            for (var i = 0; i < data.length; i++) {
                if (data[i]["Edited_table"] == "Fieldforce Data") {
                    var resultObject = FillSaleforceHistory2(data[i]);
                }
                else if (data[i]["Edited_table"] == "Geography Data") {
                    var resultObject = FillTerritoryHistory2(data[i]);
                }
                else if (data[i]["Edited_table"] == "Employee Data") {
                    sessionStorage["EditSectionName"] = data[i]["Edited_Section"];
                    if (data[i]["Edited_Section"] == "Shipping Address") {
                        var resultObject = FillEmployeeAdddresHistory2(data[i]);
                    }
                    else if (data[i]["Edited_Section"] == "Mailing Address") {
                        var resultObject = FillEmployeeAdddresHistory2(data[i]);
                    }
                    else if (data[i]["Edited_Section"] == "Sample Shipment Locker") {
                        var resultObject = FillEmployeeAdddresHistory2(data[i]);
                    }
                    else {
                        var resultObject = FillEmployeeHistory2(data[i]);
                    }
                }
                if (resultObject.NewValue.length > 0) {
                    for (var j = 0; j < resultObject.NewValue.length; j++) {
                        newRecet += '   <div > ' +
             '<div style="display: inline-table; width: 12%"> ' +
              data[i]["LastEditedDate"] + '</div> ' +
            '<div style="display: inline-table; width: 15%"> '
            + data[i]["Edited_table"] +

            '</div> ' +

                        '<div style="display: inline-table; width: 20%"> ' +
                         resultObject.NewValue[j]["name"] + '</div> ' +

                        '<div style="display: inline-table; width: 20%"> ' +
                         resultObject.OldValue[j]["value"] + '</div> ' +

                        '<div style="display: inline-table; width: 20%"> ' +
                         resultObject.NewValue[j]["value"] + '</div> ' +
                        '<div style="display:inline-table;width: 10%;text-align:center"> ' +
                     '' + data[i]["Edited_By"] + ' </div>  <div class="lastcol" style="float: right;"> ' +
            '</div>';
                    }

                }
            }
            if (data.length == 5) {
                newRecet += '<div id="Div_LoadMoreData"  style="text-align:center;"><a href="javascript:void(0)" onclick="LoadMoreEmployeeDetails(this,' + (parseInt(data[i - 1]["num"], 10) + 1) + ')"  data-detail="EmployeeRecentChangeData" data-attr="show">Load More Details</a></div>';

            }
            $("#EmployeeRecentChangeData").append(newRecet);
        }

        //else {
        //    CustomAlert(result.Message);
        //}
    }
    $("#loading").hide();

}

function EmployeeIncludeInactiveRecored($this) {
    $("#loading").show();
    if ($("#IncludeInactiveEmployee").prop('checked')) {
        //DeGrToolWebService.GetAllEmployeeDetails($('#username').val(), ConvertArrayToString($('#Employee_Salesforce_Id').val()), $("#Employee_Geography_ID").val(), ConvertArrayToString($("#Employee_Role").val()), ConvertArrayToString($("#Employee_Territory_Type").val()), IncludeInvalid, employeeTableSet, onFail);
        DeGrToolWebService.GetAllEmployeeDetails($('#username').val(), ConvertArrayToString($('#Employee_Salesforce_Id').val()), $("#Employee_Geography_ID").val(), ConvertArrayToString($("#Employee_Role").val()), ConvertArrayToString($("#Employee_Territory_Type").val()), "Yes", employeeTableSet, onFail);
    }
    else {
        DeGrToolWebService.GetAllEmployeeDetails($('#username').val(), ConvertArrayToString($('#Employee_Salesforce_Id').val()), $("#Employee_Geography_ID").val(), ConvertArrayToString($("#Employee_Role").val()), ConvertArrayToString($("#Employee_Territory_Type").val()), "", employeeTableSet, onFail);
    }
}

function EmployeeIncludeInactiveRecoredInReport($this) {
    //$("#loading").show();
    //DeGrToolWebService.GetRoasterExcelData($('#username').val(), ConvertArrayToString($('#report_Salesforce_ID').val()), $("#report_geographyID").val(), ConvertArrayToString($("#report_TerritoryTyep").val()), ConvertArrayToString($("#report_role_Name").val()), $("#ReportSelectedDate").val(), ConvertArrayToString($("#EmployeeAssignmentType").val()), EndDate, AsonRoster, datatablesetExcel);
    $("#previewExcelID").trigger('click');
}

function OpenEmployeeAddModal() {
    DeGrToolWebService.GetRosterEmployeeType(sessionStorage["username"], FillEmployeeDropdown);
    DeGrToolWebService.GetRosterEmployeeTitle(sessionStorage["username"], FillEmployeeTitleDropdown);
    $("#employee_save").data('data-savemode', 'Add');
    MakeFistSelcetSameAsPlaceHolder('new_emp_title');
    MakeFistSelcetSameAsPlaceHolder('new_emp_type');
    HidePopupField(GetHidenFieldsForEmployeeAdd());
    var modaltitle = "Add New Employee";
    var id = "empAddModal";
    $("#" + id + "Title").text(modaltitle);
    //$("#empAddModal").modal();
    var disabled_terr_field = ['new_emp_id', 'new_emp_user_id']
    MakeFieldEditable(disabled_terr_field);
    HidePopupField(GetHidenFieldsForTerritoryAdd());
    ClearEmployeePopup();
    $("#empAddModal").modal({
        backdrop: 'static',
        keyboard: false
    });
}

function PrintEmployeeDetails() {
    $('#hnd_EmployeePrintClick').click();
}

function ClearEmployeeFiltersRecord() {

    $('.SalesforceMul').val('');
    $('.SalesforceMul').multiselect('refresh');
    $("#Employee_Geography_ID").val('');
    $('#Employee_Territory_Type').val('');
    $("#Employee_Territory_Type").multiselect('refresh');
    $('#Employee_Role').val('');
    $("#Employee_Role").multiselect('refresh');
    //$("#tbl_roster_filter > label > input[type='search']").val('');
    $("#loading").show();

    var IncludeInvalid = "";
    if ($("#IncludeInactiveEmployee").prop('checked')) {
        IncludeInvalid = "Yes";
    }
    DeGrToolWebService.GetAllEmployeeDetails($('#username').val(), ConvertArrayToString($('#Employee_Salesforce_Id').val()), $("#Employee_Geography_ID").val(), ConvertArrayToString($("#Employee_Role").val()), ConvertArrayToString($("#Employee_Territory_Type").val()), IncludeInvalid, employeeTableSet, onFail);

    DeGrToolWebService.GetRoasterHomePageData(sessionStorage["username"], "Firsts", onSuccess, onFail);

}

function SearchEmployeeFoRecord() {
    var IncludeInvalid = "";
    if ($("#IncludeInactiveEmployee").prop('checked')) {
        IncludeInvalid = "Yes";
    }
    DeGrToolWebService.GetAllEmployeeDetails($('#username').val(), ConvertArrayToString($('#Employee_Salesforce_Id').val()), $("#Employee_Geography_ID").val(), ConvertArrayToString($("#Employee_Role").val()), ConvertArrayToString($("#Employee_Territory_Type").val()), IncludeInvalid, employeeTableSet, onFail);

}
//================================================ Employee Personal Details Function Start ================================================
function FillEmployeeTitleDropdown(data) {
    var Result = JSON.parse(data)

    $("#new_emp_title").html('');
    var result_course = JSON.parse(data);
    if (result_course.Status == 1) {
        $("#new_emp_title").append($('<option></option>', {
            value: "",
            text: "Please Select  Title",
        }));

        for (var i = 0; i < result_course["dt_ReturnedTables"][0].length; i++) {

            $("#new_emp_title").append($('<option></option>', {
                value: result_course["dt_ReturnedTables"][0][i]["Employee_title_code"],
                text: result_course["dt_ReturnedTables"][0][i]["Employee_title_desc"],
            }));

        }
    }
    else {
        //CustomAlert(Result.Message);
    }

}

function FillEmployeeDropdown(data) {
    var Result = JSON.parse(data)

    $("#new_emp_type").html('');
    var result_course = JSON.parse(data);
    if (result_course.Status == 1) {
        $("#new_emp_type").append($('<option></option>', {
            value: "",
            text: "Please Select Employee Type",
        }));

        for (var i = 0; i < result_course["dt_ReturnedTables"][0].length; i++) {

            $("#new_emp_type").append($('<option></option>', {
                value: result_course["dt_ReturnedTables"][0][i]["Employee_type_code"],
                text: result_course["dt_ReturnedTables"][0][i]["Employee_type_desc"],
            }));

        }
    }
    else {
        //CustomAlert(Result.Message);
    }

}

function FillTerritoryEmployeeStausDropdown(data) {
    var Result = JSON.parse(data)

    $("#emp_assign_emp_terr_Status").html('');
    var result_course = JSON.parse(data);
    if (result_course.Status == 1) {
        $("#emp_assign_emp_terr_Status").append($('<option></option>', {
            value: "",
            text: "Please Select Status",
        }));

        for (var i = 0; i < result_course["dt_ReturnedTables"][0].length; i++) {

            $("#emp_assign_emp_terr_Status").append($('<option></option>', {
                value: result_course["dt_ReturnedTables"][0][i]["Status_Code"],
                text: result_course["dt_ReturnedTables"][0][i]["Status_Desc"],
            }));

        }
        $("#EmployeeAssignmentType").html('');
        $("#EmployeeAssignmentType").multiselect('destroy');
        for (var i = 0; i < result_course["dt_ReturnedTables"][0].length; i++) {
            $("#EmployeeAssignmentType").append($('<option></option>', {
                value: result_course["dt_ReturnedTables"][0][i]["Status_Code"],
                text: result_course["dt_ReturnedTables"][0][i]["Status_Desc"],
            }));
        }
        $("#EmployeeAssignmentType").val('');
        $('#EmployeeAssignmentType').attr('multiple', 'multiple');

        $('#EmployeeAssignmentType').multiselect({
            nonSelectedText: 'Select Assignment Type',
            includeSelectAllOption: true,
            numberDisplayed: 0,
            nSelectedText: ' - Assignment Type selected',
            allSelectedText: 'All Assignment Type Selected',
             
        });
 
       
    }
    else {
        //CustomAlert(Result.Message);
    }

}

function CompareHireDate($this) {
    if (!Coparedate($this, 'new_emp_hire_date', 'new_emp_field_start_date', 'Employee hire date should be greater than or equal to field start date. ', ''))
        Coparedate($this, 'new_emp_termination_date', 'new_emp_hire_date', 'Employee hire date should be less than termination date. ', 'new_emp_status');
}

function ConpareEmpfieldStartDate($this) {
    if (!Coparedate($this, 'new_emp_termination_date', 'new_emp_hire_date', 'Employee hire date should be less than termination date. ', 'new_emp_status')) {
        Coparedate($this, 'new_emp_field_start_date', 'new_emp_hire_date', 'Employee hire date should be less than or equal to field start date. ', '');
    }
}

function CompareEmpTerminationDate($this) {
    if (!Coparedate($this, 'new_emp_termination_date', 'new_emp_hire_date', 'Employee termination date should be greater than hire date. ', 'new_emp_status')) {
        Coparedate($this, 'new_emp_termination_date', 'new_emp_field_start_date', 'Employee termination date should be greater than field start date. ', 'new_emp_status');
    }
}

function GetEmployeePopupData(mode) {
    var EmployeePopupData = new Object();
    if (PrimaryValidationOnEmployeePopup(mode)) {
        if (mode == "Add") { EmployeePopupData["Employee_ID"] = $("#new_emp_user_id").val(); }
        else if (mode == "Edit") {
            EmployeePopupData["Employee_ID"] = $("#new_emp_id").val();
        }
        
        //newEmployeeId = $("#new_emp_user_id").val();
        EmployeePopupData["User_ID"] = $("#new_emp_user_id").val();
        EmployeePopupData["EmpID"] = $("#new_emp_empID").val();
        EmployeePopupData["Department"] = $("#new_emp_department").val();
        EmployeePopupData["Employee_Title"] = $("#new_emp_title").val()
        EmployeePopupData["First_Name"] = $("#new_emp_first_name").val();
        EmployeePopupData["Middle_Name"] = $("#new_emp_middle_name").val();
        EmployeePopupData["Last_Name"] = $("#new_emp_last_name").val();
        EmployeePopupData["Suffix"] = $("#new_emp_suffix").val();
        if ($("#new_emp_preferred_name").val() != null && $("#new_emp_preferred_name").val() != "") {
            EmployeePopupData["Prefferd_Name"] = $("#new_emp_preferred_name").val();
        }
        else {
            EmployeePopupData["Prefferd_Name"] = "";
            //EmployeePopupData["Prefferd_Name"] = EmployeePopupData["First_Name"];//+ ' ' + EmployeePopupData["Last_Name"];
        }
        //EmployeePopupData["Full_Name"] = $("#new_emp_full_name").val();
        //EmployeePopupData["Full_Prefferd_Name"] = $("#new_emp_full_name_preffered").val();
        EmployeePopupData["Hire_Date"] = $("#new_emp_hire_date").val();
        EmployeePopupData["Field_Start_Date"] = $("#new_emp_field_start_date").val();
        //EmployeePopupData["Field_End_Date"] = $("#new_emp_field_end_date").val();
        //EmployeePopupData["Position_Start_Date"] = $("#new_emp_position_start_date").val();
        //EmployeePopupData["Position_End_Date"] = $("#new_emp_position_end_date").val();
        EmployeePopupData["Employee_Type"] = $("#new_emp_type").val();
        EmployeePopupData["Employee_Status"] = $("#new_emp_status").val();
        EmployeePopupData["Employee_Termination_Date"] = $("#new_emp_termination_date").val();
        EmployeePopupData["PRID"] = $("#new_emp_prid").val()
        EmployeePopupData["AZ_Employee_ID"] = $("#new_emp_az_employee_id").val()
        EmployeePopupData["AZ_Email_ID"] = $("#new_emp_az_email_id").val()

        //EmployeePopupData["Employee_Home_Addr1"] = $("#new_emp_home_addr1").val();
        //EmployeePopupData["Employee_Home_Addr2"] = $("#new_emp_home_addr2").val();
        //EmployeePopupData["Employee_Home_City"] = $("#emp_home_city").val();
        //EmployeePopupData["Employee_Home_Zip"] = $("#emp_home_zip").val();
        //EmployeePopupData["Employee_Home_State"] = $("#emp_home_state").val();

        //EmployeePopupData["Employee_Mail_Addr1"] = $("#new_emp_mail_addr1").val();
        //EmployeePopupData["Employee_Mail_Addr2"] = $("#new_emp_mail_addr2").val();
        //EmployeePopupData["Employee_Mail_City"] = $("#new_emp_mial_city").val();
        //EmployeePopupData["Employee_Mail_Zip"] = $("#new_emp_mailing_zip").val();
        //EmployeePopupData["Employee_Mail_State"] = $("#new_emp_mailing_state").val();

        //EmployeePopupData["Employee_Comapany_Email"] = $("#new_emp_company_email").val();
        //EmployeePopupData["Employee_Personal_Email"] = $("#new_emp_personal_emial").val();

        //EmployeePopupData["Employee_Bussiness_Phone"] = $("#new_emp_bussiness_phone").val();
        //EmployeePopupData["Employee_Mobile_Phone"] = $("#new_emp_mobile_phone").val();
        //EmployeePopupData["Employee_Home_Phone"] = $("#new_emp_home_phone").val();
        if (mode == "Edit") {
            if (ChageDetectionInEmployee(EmployeePopupData)) {
                return EmployeePopupData;
            }
            else {
                return null;
            }
        }
        else {
            return EmployeePopupData;
        }
    }
    else {
        return null;
    }
}

function ChageDetectionInEmployee(NewData) {

    var count = 0;
    var OldData = MasterData["EditedData"];
    if (OldData["Employee_ID"] == null && NewData["Employee_ID"] == "") { } else if (OldData["Employee_ID"] != NewData["Employee_ID"]) { count++; }
    if (OldData["User_ID"] == null && NewData["User_ID"] == "") { } else if (OldData["User_ID"] != NewData["User_ID"]) { count++; }
    if (OldData["EmpID"] == null && NewData["EmpID"] == "") { } else if (OldData["EmpID"] != NewData["EmpID"]) { count++; }
    if (OldData["Department"] == null && NewData["Department"] == "") { } else if (OldData["Department"] != NewData["Department"]) { count++; }
    if (OldData["Employee_Title"] == null && NewData["Employee_Title"] == "") { } else if (OldData["Employee_Title"] != NewData["Employee_Title"]) { count++; }
    if (OldData["First_Name"] == null && NewData["First_Name"] == "") { } else if (OldData["First_Name"] != NewData["First_Name"]) { count++; }
    //if (OldData["Territory_Start_Date"] != NewData["Territory_Start_Date"]) { count++; }
    if (OldData["Hire_Date"] == null && NewData["Hire_Date"] == "") { } else { if (OldData["Hire_Date"] != NewData["Hire_Date"]) { count++; } }
    if (OldData["Field_Start_Date"] == null && NewData["Field_Start_Date"] == "") { } else { if (OldData["Field_Start_Date"] != NewData["Field_Start_Date"]) { count++; } }
    if (OldData["Termination_Date"] == null && NewData["Employee_Termination_Date"] == "") { } else { if (OldData["Termination_Date"] != NewData["Employee_Termination_Date"]) { count++; } }
    //if (OldData["Territory_End_Date"] != NewData["Territory_End_Date"]) { count++; }
    if (OldData["Middle_Name"] == null && NewData["Middle_Name"] == "") { } else if (OldData["Middle_Name"] != NewData["Middle_Name"]) { count++; }
    if (OldData["Last_Name"] == null && NewData["Last_Name"] == "") { } else if (OldData["Last_Name"] != NewData["Last_Name"]) { count++; }
    if (OldData["Suffix"] == null && NewData["Suffix"] == "") { } else if (OldData["Suffix"] != NewData["Suffix"]) { count++; }
    if (OldData["Preferred_Name"] == null && NewData["Prefferd_Name"] == "") { } else if (OldData["Preferred_Name"] != NewData["Prefferd_Name"]) { count++; }
    if (OldData["Employee_Type"] == null && NewData["Employee_Type"] == "") { } else if (OldData["Employee_Type"] != NewData["Employee_Type"]) { count++; }
    if (OldData["PRID"] == null && NewData["PRID"] == "") { } else if (OldData["PRID"] != NewData["PRID"]) { count++; }
    if (OldData["AZ_Employee_ID"] == null && NewData["AZ_Employee_ID"] == "") { } else if (OldData["AZ_Employee_ID"] != NewData["AZ_Employee_ID"]) { count++; }
    if (OldData["AZ_Email_ID"] == null && NewData["AZ_Email_ID"] == "") { } else if (OldData["AZ_Email_ID"] != NewData["AZ_Email_ID"]) { count++; }
    if (count == 0) {
        CustomAlert("No changes found.");
        return false;
    }
    else {
        return true;
    };
}

function PrimaryValidationOnEmployeePopup(mode) {
    if (mode == "Edit") {
        //var emp_id = $("#new_emp_id").val();
        //if (!(emp_id != "" && emp_id != undefined && emp_id != "")) { CustomAlert('Please enter employee id.'); return false; }

        var emp_user_id = $("#new_emp_user_id").val();
        if (!(emp_user_id != "" != emp_user_id != undefined && emp_user_id != "")) { CustomAlert('Please enter user id.'); return false; }

        var emp_empid = $("#new_emp_empID").val();
        if (!(emp_empid != "" != emp_empid != undefined && emp_empid != "")) { CustomAlert('Please enter employee id.'); return false; }
        

        var firstname = $("#new_emp_first_name").val();
        if (!(firstname != null && firstname != undefined && firstname != "")) { CustomAlert("Please enter first name."); return false; }
        var lastName = $("#new_emp_last_name").val();
        if (!(lastName != null && lastName != undefined && lastName != "")) { CustomAlert("Please enter last name."); return false; }
        var suffix = $("#new_emp_suffix").val();
        var prefeerd_name = $("#new_emp_preferred_name").val();
       
        var field_start_date = $("#new_emp_field_start_date").val();

        var positionStartDate = $("#new_emp_position_start_date").val();
        var positionEndDate = $("#new_emp_position_end_date").val();

        var Emptype = $("#new_emp_type").val();
        if (!(Emptype != null && Emptype != undefined && Emptype != "")) { CustomAlert("Please select employee type."); return false; }

        EmpterminationDate = $("#new_emp_termination_date").val();

    }
    else {
        //var emp_id = $("#new_emp_id").val();
        //if (!(emp_id != "" && emp_id != undefined && emp_id != "")) { CustomAlert('Please enter employee id.'); return false; }

        var emp_user_id = $("#new_emp_user_id").val();
        if (!(emp_user_id != "" != emp_user_id != undefined && emp_user_id != "")) { CustomAlert('Please enter user id.'); return false; }
        var firstname = $("#new_emp_first_name").val();
        if (!(firstname != null && firstname != undefined && firstname != "")) { CustomAlert("Please  enter first name."); return false; }
        var lastName = $("#new_emp_last_name").val();
        if (!(lastName != null && lastName != undefined && lastName != "")) { CustomAlert("Please enter last name."); return false; }
        var suffix = $("#new_emp_suffix").val();
        var prefeerd_name = $("#new_emp_preferred_name").val();
        var emp_hire_date = $("#new_emp_hire_date").val();
        if (!(emp_hire_date != null && emp_hire_date != undefined && emp_hire_date != "")) { CustomAlert("Please enter hire date."); return false; }

        var field_start_date = $("#new_emp_field_start_date").val();

        var positionStartDate = $("#new_emp_position_start_date").val();
        var positionEndDate = $("#new_emp_position_end_date").val();

        var Emptype = $("#new_emp_type").val();
        if (!(Emptype != null && Emptype != undefined && Emptype != "")) { CustomAlert("Please enter employee type."); return false; }
    }


    return true;
}

function EmployeeSave(mode, EmployeeObject) {
    $("#loading").show();
    if (mode == "Add") {
        DeGrToolWebService.AddEmployeeDetails($('#username').val(), JSON.stringify(EmployeeObject), EmployeeAddSuccess);
    }
    else if (mode == "Edit") {
        DeGrToolWebService.UpdateEmplyoeeDetails($('#username').val(), JSON.stringify(EmployeeObject), popusaveSalesForce);
    }
}

function EmployeeAddSuccess(data) {
    if (data != "" && data != undefined) {
        var result = JSON.parse(data);
        var resultSucessArray = result.Message.split('@@@');
        CustomAlert(resultSucessArray[0]);
        var IncludeInvalid = "No";
        if (result.Status == 1) {
            console.log("new Employee id is " + newEmployeeId);
            var newEmployeeId = resultSucessArray[1];
            $('#empAddmodalcolse').trigger("click");
           
            if ($("#IncludeInactiveEmployee").prop('checked')) {
                IncludeInvalid = "Yes";
            }
            DeGrToolWebService.GetAllEmployeeDetails($('#username').val(), ConvertArrayToString($('#Employee_Salesforce_Id').val()), $("#Employee_Geography_ID").val(), ConvertArrayToString($("#Employee_Role").val()), ConvertArrayToString($("#Employee_Territory_Type").val()), IncludeInvalid, employeeTableSet, onFail);
            view_popup_employee('', newEmployeeId);
        }
        else {
            if ($("#IncludeInactiveEmployee").prop('checked')) {
                IncludeInvalid = "Yes";
            }
            DeGrToolWebService.GetAllEmployeeDetails($('#username').val(), ConvertArrayToString($('#Employee_Salesforce_Id').val()), $("#Employee_Geography_ID").val(), ConvertArrayToString($("#Employee_Role").val()), ConvertArrayToString($("#Employee_Territory_Type").val()), IncludeInvalid, employeeTableSet, onFail);
            return false;
        }
    }
    else {
        CustomAlert("Something went wrong!");
        $('.close2').trigger("click");
    }
}
//================================================= Employee Personal Details Function End =================================================

//================================================ Employee Home Address Detail Function Start =============================================
function OpenHomeDatadForEdit() {

    ClearHomePopup();
    // whatfield you want to disable just add id of that field in the following Array
    var disabled_terr_field = ['new_emp_id']
    MakeFieldNonEditable(disabled_terr_field);
    MasterData["EditedData"] = null;
    MasterData["EditedData"] = MasterData["EmpData"];

    SetHomeAddressPopup(MasterData["EmpData"]);
}

function ClearHomePopup() {
    $("#new_emp_home_addr1").val('');
    $("#new_emp_home_addr2").val('');
    $("#emp_home_city").val('');
    $("#emp_home_zip").val('');
    $("#emp_home_state").val('');
}

function SetHomeAddressPopup(data) {
    $("#new_emp_home_addr1").val(data['Home_Address1']);
    $("#new_emp_home_addr2").val(data['Home_Address2']);
    $("#emp_home_city").val(data['Home_City']);
    $("#emp_home_zip").val(data['Home_Zip']);
    $("#emp_home_state").val(data['Home_State']);
    if (data['Home_State'] == "" || data['Home_State'] == null) MakeFistSelcetSameAsPlaceHolder('emp_home_state');
    else { MakeSelcetnormal("emp_home_state"); }
}

function GetHomeAddressPopupData(mode) {
    if (PimaryValidationOnHomeAddress()) {
        var EmployeePopupData = new Object();
        //($("#current_emp_id").val() == "" )
        EmployeePopupData["Employee_ID"] = $("#current_emp_id").val();
        //EmployeePopupData["User_ID"] = $("#current_emp_id").val();
        EmployeePopupData["Employee_Home_Addr1"] = $("#new_emp_home_addr1").val();
        EmployeePopupData["Employee_Home_Addr2"] = $("#new_emp_home_addr2").val();
        EmployeePopupData["Employee_Home_City"] = $("#emp_home_city").val();
        EmployeePopupData["Employee_Home_Zip"] = $("#emp_home_zip").val();
        EmployeePopupData["Employee_Home_State"] = $("#emp_home_state").val();

        //if()
        if (ChageDetectionInEmployeeHomeAddress(EmployeePopupData)) {
            return EmployeePopupData
        }
        else {
            return null;
        }
    }
    else {
        return null;
    }
}

function ChageDetectionInEmployeeHomeAddress(NewData) {
    var count = 0;
    var OldData = MasterData["EditedData"];
    if (OldData["Home_Address1"] == null && NewData["Employee_Home_Addr1"] == "") { } else if (OldData["Home_Address1"] != NewData["Employee_Home_Addr1"]) { count++; }

    if (OldData["Home_Address2"] == null && NewData["Employee_Home_Addr2"] == "") { } else if (OldData["Home_Address2"] != NewData["Employee_Home_Addr2"]) { count++; }
    if (OldData["Home_City"] == null && NewData["Employee_Home_City"] == "") { } else if (OldData["Home_City"] != NewData["Employee_Home_City"]) { count++; }
    if (OldData["Home_Zip"] == null && NewData["Employee_Home_Zip"] == "") { } else if (OldData["Home_Zip"] != NewData["Employee_Home_Zip"]) { count++; }

    if (OldData["Home_State"] != NewData["Employee_Home_State"]) { count++; }
    if (count == 0) {
        CustomAlert("No changes found.");
        return false;
    }
    else {
        return true;
    };
}

function PimaryValidationOnHomeAddress() {
    var emp_Addr1 = $("#new_emp_home_addr1").val();
    if (!(emp_Addr1 != "" != emp_Addr1 != undefined && emp_Addr1 != "")) { CustomAlert('Please enter address.'); return false; }
    var City = $("#emp_home_city").val();
    if (!(City != null && City != undefined && City != "")) { CustomAlert("Please enter city."); return false; }
    var State = $("#emp_home_state").val();
    if (!(State != null && State != undefined && State != "")) { CustomAlert("Please enter state."); return false; }
    var ZIp = $("#emp_home_zip").val();
    if (!(ZIp != null && ZIp != undefined && ZIp != "")) { CustomAlert("Please enter zip."); return false; }
    return true;

}
//================================================= Employee Home Address Detail Function End =============================================

//============================================== Employee mailing Address details function Start ==========================================
function OpenMailDatadForEdit() {

    ClearMailPopup();
    // whatfield you want to disable just add id of that field in the following Array
    var disabled_terr_field = ['new_emp_id']
    MakeFieldNonEditable(disabled_terr_field);
    MasterData["EditedData"] = MasterData["EmpData"];
    SetMailAddressPopup(MasterData["EmpData"]);
}

function ClearMailPopup() {
    $("#new_emp_mail_addr1").val('');
    $("#new_emp_mail_addr2").val('');
    $("#new_emp_mial_city").val('');
    $("#new_emp_mailing_zip").val('');
    $("#new_emp_mailing_state").val('');
    MakeFistSelcetSameAsPlaceHolder('new_emp_mailing_state');
}

function SetMailAddressPopup(data) {
    $("#sameAsHome").prop('checked', false);
    $("#new_emp_mail_addr1").val(data['M_Address1']);
    $("#new_emp_mail_addr2").val(data['M_Address2']);
    $("#new_emp_mial_city").val(data['M_City']);
    $("#new_emp_mailing_zip").val(data['M_Zip']);
    if (data['M_State'] == "" || data['M_State'] == null) MakeFistSelcetSameAsPlaceHolder('new_emp_mailing_state');
    else { MakeSelcetnormal("new_emp_mailing_state"); }
    $("#new_emp_mailing_state").val(data['M_State']);


}

function setMailSameAsAddressPopup(data) {
    $("#new_emp_mail_addr1").val(data['Home_Address1']);
    $("#new_emp_mail_addr2").val(data['Home_Address2']);
    $("#new_emp_mial_city").val(data['Home_City']);
    $("#new_emp_mailing_zip").val(data['Home_Zip']);
    if (data['Home_State'] == "" || data['Home_State'] == null) MakeFistSelcetSameAsPlaceHolder('new_emp_mailing_state');
    else { MakeSelcetnormal("new_emp_mailing_state"); }
    $("#new_emp_mailing_state").val(data['Home_State']);

}

function GetMailAddressPopupData(mode) {
    var EmployeePopupData = new Object();
    if (primaryValidatioOnMailAddress()) {
        EmployeePopupData["Employee_ID"] = $("#current_emp_id").val();
        EmployeePopupData["Employee_Mail_Addr1"] = $("#new_emp_mail_addr1").val();
        EmployeePopupData["Employee_Mail_Addr2"] = $("#new_emp_mail_addr2").val();
        EmployeePopupData["Employee_Mail_City"] = $("#new_emp_mial_city").val();
        EmployeePopupData["Employee_Mail_Zip"] = $("#new_emp_mailing_zip").val();
        EmployeePopupData["Employee_Mail_State"] = $("#new_emp_mailing_state").val();

        if (ChageDetectionInEmployeeMailAddress(EmployeePopupData)) {
            return EmployeePopupData;
        }
        else {
            return null;
        }
    }
    else {
        return null;
    }
}

function ChageDetectionInEmployeeMailAddress(NewData) {
    var count = 0;
    var OldData = MasterData["EditedData"];
    if (OldData["M_Address1"] == null && NewData["Employee_Mail_Addr1"] == "") { } else if (OldData["M_Address1"] != NewData["Employee_Mail_Addr1"]) { count++; }

    if (OldData["M_Address2"] == null && NewData["Employee_Mail_Addr2"] == "") { } else if (OldData["M_Address2"] != NewData["Employee_Mail_Addr2"]) { count++; }
    if (OldData["M_City"] == null && NewData["Employee_Mail_City"] == "") { } else if (OldData["M_City"] != NewData["Employee_Mail_City"]) { count++; }
    if (OldData["M_Zip"] == null && NewData["Employee_Mail_Zip"] == "") { } else if (OldData["M_Zip"] != NewData["Employee_Mail_Zip"]) { count++; }

    if (OldData["M_State"] == null && NewData["Employee_Mail_State"] == "") { } else if (OldData["M_State"] != NewData["Employee_Mail_State"]) { count++; }
    if (count == 0) {
        CustomAlert("No changes found.");
        return false;
    }
    else {
        return true;
    };
}

function primaryValidatioOnMailAddress() {

    var emp_Addr1 = $("#new_emp_mail_addr1").val();
    if (!(emp_Addr1 != "" != emp_Addr1 != undefined && emp_Addr1 != "")) { CustomAlert('Please enter address'); return false; }
    var City = $("#new_emp_mial_city").val();
    if (!(City != null && City != undefined && City != "")) { CustomAlert("Please enter city"); return false; }
    var State = $("#new_emp_mailing_state").val();
    if (!(State != null && State != undefined && State != "")) { CustomAlert("Please enter state"); return false; }
    var ZIp = $("#new_emp_mailing_zip").val();
    if (!(ZIp != null && ZIp != undefined && ZIp != "")) { CustomAlert("Please enter zip"); return false; }

    return true;
}
//============================================= Employee mailing Address details function end =============================================

//============================================= Employee Ship To Address details fuction Starts ===========================================
function OpenShipingDatadForEdit() {

    ClearShippingPopup();
    MasterData["EditedData"] = null;
    MasterData["EditedData"] = MasterData["EmpData"];
    SetShippingAddressPopup(MasterData["EmpData"]);
}

function ClearShippingPopup() {
    $("#new_emp_shipping_addr1").val('');
    $("#new_emp_shipping_addr2").val('');
    $("#new_emp_shipping_city").val('');
    $("#new_emp_shippin_zip").val('');
    $("#new_emp_shipping_state").val('');
    MakeFistSelcetSameAsPlaceHolder('new_emp_shipping_state');
}

function SetShippingAddressPopup(data) {
    $("#SameAsMaling").prop('checked', false);
    $("#new_emp_shipping_addr1").val(data['Ship_to_Address']);
    $("#new_emp_shipping_addr2").val(data['Ship_to_Address2']);
    $("#new_emp_shipping_city").val(data['Ship_to_city']);
    $("#new_emp_shippin_zip").val(data['Ship_to_Zip']);
    $("#new_emp_shipping_state").val(data['Ship_to_State']);
    if (data['Ship_to_State'] == "" || data['Ship_to_State'] == null) MakeFistSelcetSameAsPlaceHolder('new_emp_shipping_state');
    else { MakeSelcetnormal("new_emp_shipping_state"); }
}

function setShippingSameAsAddressPopup(data) {
    $("#new_emp_shipping_addr1").val(data['Home_Address1']);
    $("#new_emp_shipping_addr2").val(data['Home_Address2']);
    $("#new_emp_shipping_city").val(data['Home_City']);
    $("#new_emp_shippin_zip").val(data['Home_Zip']);
    $("#new_emp_shipping_state").val(data['Home_State']);
    if (data['Home_State'] == "" || data['Home_State'] == null) MakeFistSelcetSameAsPlaceHolder('new_emp_shipping_state');
    else { MakeSelcetnormal("new_emp_shipping_state"); }

}

function GetShipingAddressPopupDate(mode) {

    var EmployeePopupData = new Object();
    if (PrimariValidationOnShippingAddress()) {
        EmployeePopupData["Employee_ID"] = $("#current_emp_id").val();
        //EmployeePopupData["User_ID"] = "acarter@zspharma.com";
        EmployeePopupData["Employee_Shiping_to_Addr1"] = $("#new_emp_shipping_addr1").val();
        EmployeePopupData["Employee_Shiping_to_Addr2"] = $("#new_emp_shipping_addr2").val();
        EmployeePopupData["Employee_Shiping_to_City"] = $("#new_emp_shipping_city").val();
        EmployeePopupData["Employee_Shiping_to_State"] = $("#new_emp_shipping_state").val();
        EmployeePopupData["Employee_Shiping_to_Zip"] = $("#new_emp_shippin_zip").val();

        //EmployeePopupData["Employee_Ship_to_Addr"] = $("#new_emp_shipping_addr1").val();
        //EmployeePopupData["Employee_Ship_to_Addr2"] = $("#new_emp_shipping_addr2").val();
        //EmployeePopupData["Employee_Ship_to_City"] = $("#new_emp_shipping_city").val();
        //EmployeePopupData["Employee_Ship_to_State"] = $("#new_emp_shipping_state").val();
        //EmployeePopupData["Employee_Ship_to_Zip"] = $("#new_emp_shippin_zip").val();
        if (ChageDetectionInEmployeeShipToAddress(EmployeePopupData)) {
            return EmployeePopupData;
        }
        else {
            return null;
        }
    }
    else {
        return null;
    }

}

function ChageDetectionInEmployeeShipToAddress(NewData) {
    var count = 0;
    var OldData = MasterData["EditedData"];
    if (OldData["Ship_to_Address"] == null && NewData["Employee_Shiping_to_Addr1"] == "") { } else if (OldData["Ship_to_Address"] != NewData["Employee_Shiping_to_Addr1"]) { count++; }

    if (OldData["Ship_to_Address2"] == null && NewData["Employee_Shiping_to_Addr2"] == "") { } else if (OldData["Ship_to_Address2"] != NewData["Employee_Shiping_to_Addr2"]) { count++; }
    if (OldData["Ship_to_city"] == null && NewData["Employee_Shiping_to_City"] == "") { } else if (OldData["Ship_to_city"] != NewData["Employee_Shiping_to_City"]) { count++; }
    if (OldData["Ship_to_Zip"] == null && NewData["Employee_Shiping_to_Zip"] == "") { } else if (OldData["Ship_to_Zip"] != NewData["Employee_Shiping_to_Zip"]) { count++; }

    if (OldData["Ship_to_State"] == null && NewData["Employee_Shiping_to_State"] == "") { } else if (OldData["Ship_to_State"] != NewData["Employee_Shiping_to_State"]) { count++; }
    if (count == 0) {
        CustomAlert("No changes found.");
        return false;
    }
    else {
        return true;
    };
}

function PrimariValidationOnShippingAddress() {
    var emp_Addr1 = $("#new_emp_shipping_addr1").val();
    if (!(emp_Addr1 != "" != emp_Addr1 != undefined && emp_Addr1 != "")) { CustomAlert('Please enter address.'); return false; }
    var City = $("#new_emp_shipping_city").val();
    if (!(City != null && City != undefined && City != "")) { CustomAlert("Please enter city."); return false; }
    var State = $("#new_emp_shipping_state").val();
    if (!(State != null && State != undefined && State != "")) { CustomAlert("Please enter state."); return false; }
    var ZIp = $("#new_emp_shippin_zip").val();
    if (!(ZIp != null && ZIp != undefined && ZIp != "")) { CustomAlert("Please enter zip."); return false; }
    return true;
}
//============================================== Employee Ship To Address details fuction ends =============================================

//======================================================= Employee Email Fucntion Starts ===================================================
function OpenEmailDatadForEdit() {

    ClearEmailPopup();
    // whatfield you want to disable just add id of that field in the following Array
    //var disabled_terr_field = ['new_emp_id']
    //MakeFieldNonEditable(disabled_terr_field);
    SetEmailAddressPopup(MasterData["EmpData"]);
}

function ClearEmailPopup() {
    $("#new_emp_company_email").val('');
    $("#new_emp_personal_emial").val('');
}

function SetEmailAddressPopup(data) {
    MasterData["EditedData"] = null;
    MasterData["EditedData"] = data;
    $("#new_emp_company_email").val(data["Company_Email"]);
    $("#new_emp_personal_emial").val(data['Personal_Email']);
}

function GetEmailPopupData(mode) {
    var EmployeePopupData = new Object();
    EmployeePopupData["Employee_ID"] = $("#current_emp_id").val();
    if ($("#new_emp_company_email").val() != "") {
        if (EmailValidation($("#new_emp_company_email").val(), "Company email", domain)) {
        }
        else {
            return null;
        }
    }
    if ($("#new_emp_personal_emial").val() != "") {
        if (EmailValidation($("#new_emp_personal_emial").val(), "Personal email","")) {
        }
        else {
            return null;
        }
    }
    EmployeePopupData["Employee_Comapany_Email"] = $("#new_emp_company_email").val();
    EmployeePopupData["Employee_Personal_Email"] = $("#new_emp_personal_emial").val();
    if (EmailChangeCheck(EmployeePopupData)) {
        return EmployeePopupData;
    }
    else {
        return null;
    }
}

function EmailChangeCheck(NewData) {
    var OldData = MasterData["EditedData"];
    var count = 0;
    if (OldData["Company_Email"] == null && NewData["Employee_Comapany_Email"] == "") { } else if (OldData["Company_Email"] != NewData["Employee_Comapany_Email"]) { count++; }

    if (OldData["Personal_Email"] == null && NewData["Employee_Personal_Email"] == "") { } else if (OldData["Personal_Email"] != NewData["Employee_Personal_Email"]) { count++; }
    if (count == 0) {
        CustomAlert("No changes found.");
        return false;
    }
    else { return true; }

}

//======================================================== Employee Email Function end ====================================================

//====================================================== Employee Phone Function Starts ===================================================
function OpenPhoneDatadForEdit() {

    ClearPhonePopup();
    SetPhoneAddressPopup(MasterData["EmpData"]);
}

function ClearPhonePopup() {
    $("#new_emp_bussiness_phone").val('');
    $("#new_emp_mobile_phone").val('');
    $("#new_emp_home_phone").val('');
}

function SetPhoneAddressPopup(data) {
    MasterData["EditedData"] = data;
    $("#new_emp_bussiness_phone").val(data['Office_Phone']);
    $("#new_emp_mobile_phone").val(data['Mobile_Phone']);
    $("#new_emp_home_phone").val(data['Home_Phone']);
}

function GetPhonePopupData(mode) {
    var EmployeePopupData = new Object();
    EmployeePopupData["Employee_ID"] = $("#current_emp_id").val();
    //EmployeePopupData["User_ID"] = $("#current_emp_id").val();
    EmployeePopupData["Employee_Bussiness_Phone"] = $("#new_emp_bussiness_phone").val();
    EmployeePopupData["Employee_Mobile_Phone"] = $("#new_emp_mobile_phone").val();
    EmployeePopupData["Employee_Home_Phone"] = $("#new_emp_home_phone").val();
    if (PhoneChangeCheck(EmployeePopupData)) {
        return EmployeePopupData;
    } else {
        return null;
    }
}

function PhoneChangeCheck(NewData) {
    var OldData = MasterData["EditedData"];
    var count = 0;
    if (OldData["Office_Phone"] == null && NewData["Employee_Bussiness_Phone"] == "") { } else if (OldData["Office_Phone"] != NewData["Employee_Bussiness_Phone"]) { count++; }

    if (OldData["Mobile_Phone"] == null && NewData["Employee_Mobile_Phone"] == "") { } else if (OldData["Mobile_Phone"] != NewData["Employee_Mobile_Phone"]) { count++; }
    if (OldData["Home_Phone"] == null && NewData["Employee_Home_Phone"] == "") { } else if (OldData["Home_Phone"] != NewData["Employee_Home_Phone"]) { count++; }
    if (count == 0) {
        CustomAlert("No changes found.");
        return false;
    }
    else { return true; }

}
//======================================================== Employee Phone Fucntion end =====================================================

//==================================================== Employee Assignment function Starts =================================================
function ClearEployeeAssigment() {
    $("#emp_assign_salesforce").val('');
    MakeFistSelcetSameAsPlaceHolder('emp_assign_salesforce');
    $("#emp_assign_territory").html('');
    $("#emp_assign_territory").append($('<option></option>', {
        value: "",
        text: "Please Select Geography",
    }));
    MakeFistSelcetSameAsPlaceHolder('emp_assign_territory');
    $("#emp_assign_Start_date").val('');
    $("#emp_assign_emp_terr_Status").val('');
    MakeFistSelcetSameAsPlaceHolder('emp_assign_emp_terr_Status');
    $("#emp_assign_end_date").val('');
}

function FillEmployeeAssignTerritory(data) {
    var Result = JSON.parse(data)

    $("#emp_assign_territory").html('');
    $("#emp_assign_territory").append($('<option></option>', {
        value: "",
        text: "Please Select Geography",
    }));

    var result_course = JSON.parse(data);
    if (result_course.Status == 1) {

        for (var i = 0; i < result_course["dt_ReturnedTables"][0].length; i++) {

            $("#emp_assign_territory").append($('<option></option>', {
                value: result_course["dt_ReturnedTables"][0][i]["Territory_ID"],
                text: result_course["dt_ReturnedTables"][0][i]["Territory_ID"] + ' - ' + result_course["dt_ReturnedTables"][0][i]["Territory_Name"],
            }));

        }
    }
    else {
        //CustomAlert(Result.Message);
    }

}

function AssignEmployeeValidation() {
    var NewAssignFieldforce = $("#emp_assign_salesforce").val();
    var NewAssignTerritory = $("#emp_assign_territory").val();
    var NewAssignStartDate = $("#emp_assign_Start_date").val();
    var NewAssignEmployeeTerritoryStatusDate = $("#emp_assign_emp_terr_Status").val();
    var NewAssignEmpEndDate = $("#emp_assign_end_date").val();

    if (!(NewAssignFieldforce != "" && NewAssignFieldforce != undefined && NewAssignFieldforce != null)) {
        CustomAlert("Please select fieldforce.");
        return false;
    }
    if (!(NewAssignTerritory != "" && NewAssignTerritory != undefined && NewAssignTerritory != null)) {
        CustomAlert("Please select geography.");
        return false;
    }
    if (!(NewAssignEmployeeTerritoryStatusDate != "" && NewAssignEmployeeTerritoryStatusDate != undefined && NewAssignEmployeeTerritoryStatusDate != null)) {
        CustomAlert("Please select status.");
        return false;
    }
    if (NotAssignAsperment) {
        if (NewAssignEmployeeTerritoryStatusDate == PrimaryVacancyCode) {
            CustomAlert("You cannot have more than one ‘Primary Assignment’ for an employee!");
            return false;
        }
    }
    if (!(NewAssignStartDate != "" && NewAssignStartDate != undefined && NewAssignStartDate != null)) {
        CustomAlert("Please enter start date.");
        return false;
    }

    
    return true;
}

function DisplayPromtMessage(data) {

    if (data != "" && data != undefined) {
        var result = JSON.parse(data)
        //CustomAlert(result.Message);
        if (result.Status == 3) {
            $("#loading").hide();

            BootstrapDialog.show({
                message: result.Message,
                title: 'Warning!',
                closable: false,
                type: [BootstrapDialog.TYPE_WARNING],
                buttons: [{
                    label: 'OK',
                    cssClass: 'btn-primary',
                    action: function (dialogItself) {
                        //var NewAssignFieldforce = $("#emp_assign_salesforce").val();
                        //var NewAssignTerritory = $("#emp_assign_territory").val();
                        //var NewAssignStartDate = $("#emp_assign_Start_date").val();
                        //var NewAssignEmployeeTerritoryStatusDate = $("#emp_assign_emp_terr_Status").val();
                        //var NewAssignEmpEndDate = $("#emp_assign_end_date").val();

                        //DeGrToolWebService.RosterEmployeeAssign($('#username').val(), $("#current_emp_id").val(), NewAssignFieldforce, NewAssignTerritory, NewAssignEmployeeTerritoryStatusDate, NewAssignStartDate, NewAssignEmpEndDate, "Y", popusaveSalesForce);

                        dialogItself.close();
                    }
                }]
            });
        }
        else if (result.Status == 1) {
            CustomAlert(result.Message);
            sessionStorage["ModalName"] = "MyEmployeeModal";
            $('.close2').trigger('click');
        }
        else {
            $("#loading").hide();
            CustomAlert(result.Message);
            return false;
        }
    }
    else {
        CustomAlert("Something went wrong!");
        $('.close2').trigger("click");
    }
}

function ClearEndDatePopup(objThis, terr_id, salseforceId, pk) {
    sessionStorage["ModalName"] = "MyEmployeeModal";
    var massage = "Are you sure you want to remove " + $(objThis).data('empname') + " from " + $(objThis).data('terrname') + " ?";
    $("#removeEmployeeMsg").html(massage);
    //sessionStorage['EmployeeID'] = empID;
    sessionStorage['Territory_id'] = terr_id;
    sessionStorage['salseforceId'] = salseforceId;
    var endDate = $(objThis).data('emppostitonenddate');
    $("#EmployeeRemoveDate").data('pk', pk);
    if (endDate != null) {
        $("#EmployeeRemoveDate").val(endDate);
    }
    else {
        $("#EmployeeRemoveDate").val('');
    }
    sessionStorage["ModalName"] = "MyEmployeeModal";
    $("#myEmployeeModalClose").trigger('click');
    $("#EmployeRemoveModal").modal({
        backdrop: 'static',
        keyboard: false
    });

}
//===================================================== Employee Assignment function ends ==================================================

//========================================================= Employee Tab Functins End ======================================================


//============================================================== Report Tab functions Starts ===============================================
function fillColumnselectDropdown(headerData) {
    $("#TotalFields").html('');
    $("#FieldsToShow").html('');
    headerData.sort(function compare(a, b) {
        if (a.header < b.header)
            return -1;
        if (a.header > b.header)
            return 1;
        return 0;
    });
    str = ''
    for (var i = 0; i < headerData.length; i++) {
        if (headerData[i]["value"] != -1) {
            str += '<div value="' + headerData[i]["value"] + '" ondblclick="SelctAndMove(this)" >' + headerData[i]["header"] + '</div>'
        }
    }
    $("#customreports").html('');
    var fiedchoose = '<div id="FieldChooser" class="col-xs-8 col-md-8 col-lg-8"><div   class="col-xs-12 col-md-12 col-lg-12">' +
        '<div class="row">' +
    '<div class="col-md-5" style="padding:0px"><h6>All fields:</h6>' +
    '<div id="TotalFields" style="width: 100%; height: 250px; overflow: auto;border:1px solid" ondrop="OnDropOnTofiels(this)">' +
    '                            </div><h6 class="" style="padding-bottom:0px">Drag &amp; drop columns for report</h6></div><div class="col-md-2" style="padding:0px">' +
    '                            <div style="height: 250px; padding-top: 85px"><div style="text-align: center" >' +
    '                                    <img id="SelectFieldsToShow" src="Images_air/rightarrow.png" style="height: 50px;" onclick="MoveSelectedRight()"/>' +
    '                                </div>' +
    '                                <div style="text-align: center">' +
    '                                    <img id="SelectTotalFields" src="Images_air/leftarrow.png" style="height: 50px" onclick="MoveSelectedLeft()"/>' +
    '                                </div></div></div>' +
    '                        <div class="col-md-5" style="padding:0px">' +
    '                            <h6>Fields in Report:</h6>' +
    '                            <div id="FieldsToShow" style="width: 100%; height: 250px;overflow: auto;border:1px solid"></div>' +
    '                        </div>' +
    '                    </div> </div></div>';
    $("#customreports").html(fiedchoose);
    $("#TotalFields").append(str);
    var $chooser = $("#FieldChooser").fieldChooser(TotalFields, FieldsToShow);
    _chooser = $chooser;
    fieldChooser = _chooser;
}

function RosterReportData(data) {
    var result_course = JSON.parse(data);
    if (result_course.Status == 1) {
        FillReportData(result_course["dt_ReturnedTables"][0]);

    }
    else {
        //CustomAlert(result_course["Messsage"]);
    }
}

function FillReportData(data) {
    var str = '';
    $("#predefienedReports").html('');
    for (var i = 0; i < data.length; i++) {

        str += '<div id="pre_' + data[i]["reportID"] + '" class="fc-field predefineReports" tabindex="0" onclick="OpenreportInEdit(this)" ondblclick="OpenPredefinedReports(this)" data-name="' + data[i]["Report_Name"] + '" data-salesforce="' + data[i]["Selected_Fieldfore"] + '" data-geography="' + data[i]["Selected_Geography"] +
            '" data-role="' + data[i]["Selected_Role"] + '" data-reportid="' + data[i]["reportID"] + '" data-includeinactiveemps="' + data[i]["Include_Inactive_Employee"] + '" data-type="' + data[i]["Selected_Type"] + '" data-columns="' + data[i]["Selected_Columns"] + '" data-assignmenttype ="' + data[i]["Selected_AssignmentType"] + '" >' + data[i]["Report_Name"] + '</div>'
    }
    $("#predefienedReports").append(str);
}

function datatablesetExcel(data) {
    if (data != "" && data != undefined) {
        var rosterTblData = JSON.parse(data);
        if (rosterTblData.Status == 1) {
            RosterTabel = new Object();
            RosterTabel = rosterTblData["dt_ReturnedTables"][0];
            if (RosterTabel == null) {
                $("#loading").hide();
                CustomAlert("No Data Found");
                //CustomAlert("No relation found on " + $("#ReportSelectedDate").val() + ". ");
                return false;
            }
            var total_vacant = $.grep(RosterTabel, function (item) {
                return item["Employee_ID"] == "vacant";
            });
            $("#vacant").text(total_vacant.length);
            $("#vacantTerritories").text(total_vacant.length);
            $("label[for='territories']").text(rosterTblData["dt_ReturnedTables"][1][0]["total_territories"]);
            $("#pro").text(rosterTblData["dt_ReturnedTables"][1][0]["total_active"]);
            $("#ActiveTerritories").text(rosterTblData["dt_ReturnedTables"][1][0]["total_active"]);
            PreviewExcelID(RosterTabel);
            
            if (visibleColumnsOrder.length > 0) {

                
                    $("#ExcelPreview").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                
            }
            else { CustomAlert("Please select columns."); }

            setTimeout(function () {
                oTableExcle
        .order([[0, 'desc']])
        .draw(false);
                setTimeout(function () {
                    oTableExcle
            .order([[0, 'desc']])
            .draw(false);
                }, 0);
            }, 300);
            //setTimeout(function () {
            //    $(".InActiveEmployee").css({ 'height': $(".dt-buttons").height() });
            //    $(".InActiveEmployee").css({ 'margin-top': '-' + $(".dt-buttons").height() + 'px' });
            //}, 300);
        }
        else {
            CustomAlert(rosterTblData.Message);
        }
        $("#loading").hide();
    }
}

function PreviewExcelID(RosterTabel) {
    sessionStorage["ModalName"] = "";
    var IsPredefiendreport = false;
    visibleColumnsOrder = new Array();
    var hiddenColumnsa = new Array();
    var DeleteBtn = false;
    if (PrevieFlag) {

        $('#FieldsToShow > div').each(function () {
            visibleColumnsOrder.push($(this).attr('value'));

        });
        $('#TotalFields > div').each(function () {

            hiddenColumnsa.push($(this).attr('value'));
        });
        var option = $('#report_Salesforce_ID').val();
        var optionRole = $('#report_role_Name').val();
        var optionType = $('#report_TerritoryTyep').val();
        $("#ReportPreviewGeographyFilters").val($("#report_geographyID").val());
        if ($("#previewExcelID").data('reportID') != "" && $("#previewExcelID").data('reportID') != undefined) {
            ExcelName = $("#previewExcelID").data('reportname') + '_' + $('#ReportSelectedDate').val();
            DeleteBtn = true;
            $("#ExcelPreviewTitle").html('');
            $("#ExcelPreviewTitle").html('Report Preview - ' + $("#previewExcelID").data('reportname'));
        }
        else {

            ExcelName = "RosterOn_" + $('#ReportSelectedDate').val();
            $("#ExcelPreviewTitle").html('Report Preview');
        }

    }
    else {
        PrevieFlag = true;
        visibleColumnsOrder = ConvertStringToArray(ReportParameter["Columns"]);
        $("#TotalFields div").each(function (item) {
            for (var z = 0; z < visibleColumnsOrder.length; z++) {
                if ($(this).attr('value') == visibleColumnsOrder[z]) {
                    $(this).addClass('fc-selected');
                    MoveSelectedRight();
                }
            }

        });
        $('#TotalFields > div').each(function () {

            hiddenColumnsa.push($(this).attr('value'));
        });

        var option = ConvertStringToArray(ReportParameter["Salesforce"]);
        var optionRole = ConvertStringToArray(ReportParameter["Role"]);
        var optionType = ConvertStringToArray(ReportParameter["Type"]);
        $("#report_geographyID").val(ReportParameter["Geography"])
        ExcelName = ReportParameter["Name"] + '_' + $('#ReportSelectedDate').val();
        $('#report_Salesforce_ID').multiselect('select', option);
        $('#report_TerritoryTyep').multiselect('select', optionType);
        $('#report_role_Name').multiselect('select', optionRole);
        IsPredefiendreport = true;

    }
    switch (getCompanyType()) {
        case "ZS":
            break;
        case "Neurocrine":
            hiddenColumnsa.push(53);
            hiddenColumnsa.push(54);
            hiddenColumnsa.push(55);
            break;

    }
    var table = "<table class='table stripe cust_table' id='tbl_Excel' ><thead> " +
                 //"<th style='text-align:center' id='btn_view'>View </th> " +
                  "<th style='text-align:center'>Fieldforce ID</th> <th style='text-align:center'>Geography # </th> <th style='text-align:center'>Geography Name</th>" +
                  "<th style='text-align:center'>Role</th> <th style='text-align:center'>Geography Type</th> <th style='text-align:center'>User ID</th>" +
                  "<th style='text-align:center'>Full Name</th><th>Employee Title</th> <th style='text-align:center'>Status</th> " +
                    "<th style='text-align:center'>Manager Name</th>" +

                  //hidden columns
                  "<th style='text-align:center'>First Name </th>" +
            "<th style='text-align:center'>Middle Name </th>" +
            "<th style='text-align:center'>Last Name </th>" +
            "<th style='text-align:center'>Employee ID</th>" +
            "<th style='text-align:center'>Suffix</th>" +

            "<th style='text-align:center'>Manager ID</th>" +

            "<th style='text-align:center'>Shipping Address</th>" +
            "<th style='text-align:center'>Shipping Address2</th>" +
            "<th style='text-align:center'>Shipping City</th>" +
            "<th style='text-align:center'>Shipping State</th>" +
            "<th style='text-align:center'>Shipping Zip</th>" +
            "<th style='text-align:center'>Mailing Address</th>" +
            "<th style='text-align:center'>Mailing Address2</th>" +
            "<th style='text-align:center'>Mailing City</th>" +
            "<th style='text-align:center'>Mailing State</th>" +
            "<th style='text-align:center'>Mailing Zip</th>" +
            "<th style='text-align:center'>Office Phone</th>" +
            "<th style='text-align:center'>Company Email</th>" +
            "<th style='text-align:center'>Home Phone</th>" +
            "<th style='text-align:center'>Cell Phone</th>" +
            "<th style='text-align:center'>Personal Email</th>" +
            "<th style='text-align:center'>Hire Date</th>" +
            "<th style='text-align:center'>Position Start Date</th>" +
            "<th style='text-align:center'>Position End Date</th>" +
            "<th style='text-align:center'>Termination Date</th>" +
            "<th style='text-align:center'>Field Start Date</th>" +
            "<th style='text-align:center'>Employee Type</th>" +
            "<th style='text-align:center'>Employee Status</th>" +
               "<th style='text-align:center'>Fieldforce Name</th>" +
            "<th style='text-align:center'>Fieldforce Start Date</th>" +
            "<th style='text-align:center'>Fieldforce End Date</th>" +
            "<th style='text-align:center'>Geography Start Date</th>" +
            "<th style='text-align:center'>Geography End Date</th>" +
            //"<th style='text-align:center'>Default Geography ZIP</th>" +
            //"<th style='text-align:center'>Geography ZIP Latitude</th>" +
            //"<th style='text-align:center'>Geography ZIP Longitude</th>" +
            //"<th style='text-align:center'>Rep Address Latitude</th>" +
            //"<th style='text-align:center'>Rep Address Longitude</th>" +
            //"<th style='text-align:center'>Account Centroid Latitude</th>" +
            //"<th style='text-align:center'>Account Centroid Longitude</th>" +
             "<th style='text-align:center'>Assignment Type</th>" +
             "<th style='text-align:center'>Sample Shipment Address </th>" +
             "<th style='text-align:center'>Sample Shipment Address 2</th>" +
             "<th style='text-align:center'>Sample Shipment City</th>" +
             "<th style='text-align:center'>Sample Shipment State</th>" +
             "<th style='text-align:center'>Sample Shipment Zip</th>" +
             "<th style='text-align:center'>Parent Geography #</th>" +
             "<th style='text-align:center'>Parent Geography Name</th>" +
             "<th style='text-align:center'>2nd Level Parent Geography #</th>" +
             "<th style='text-align:center'>2nd Level Parent Geography Name</th>" +
             "<th style='text-align:center'>AZ Territory Number</th>" +
             "<th style='text-align:center'>PRID</th>" +
             "<th style='text-align:center'>AZ Employee ID</th>" +
             "<th style='text-align:center'>AZ Email ID</th>" +

            "  </thead>  <tbody>";
    var rowsData = "";
    for (var i = 0 ; i < RosterTabel.length; i++) {
        var row = RosterTabel[i];
        var row = replaceNull(row);
        var fullName = '';
        if (row["Last_Name"] != '') fullName += row["Last_Name"] + ', ';
        if (row["First_Name"] != '') fullName += row["First_Name"] + ' ';
        if (row["Middle_Name"] != '') fullName += row["Middle_Name"];
        if (row["Suffix"] != '' && row["Suffix"] != null) fullName += ', ' + row["Suffix"];

        var fullManagerName = '';
        if (row["manager_last_name"] != '') fullManagerName += row["manager_last_name"] + ', ';
        if (row["manager_first_name"] != '') fullManagerName += row["manager_first_name"] + ' ';
        if (row["manager_middle_name"] != '') fullManagerName += row["manager_middle_name"];
        if (row["manager_suffix"] != '' && row["manager_suffix"] != null) fullManagerName += ', ' + row["manager_suffix"];

        
        var EmpStaus = "";
        if (row["Employee_termination_date"] != "" && row["Employee_termination_date"] != null) {
            var CompareDatetime = new Date(row["Employee_termination_date"]);
            var todayDateTime = new Date(today);
            if (todayDateTime <= CompareDatetime) { EmpStaus = 'Active'; }
            else { EmpStaus = 'Inactive'; }
        }
        else { EmpStaus = 'Active'; }

        var terrStatus = "";
        if (row["terr_end_date"] != "" && row["terr_end_date"] != null) {
            var CompareDatetime = new Date(row["terr_end_date"]);
            var todayDateTime = new Date(today);
            if (todayDateTime <= CompareDatetime) { terrStatus = 'Active'; }
            else { terrStatus = 'Inactive'; }
        }
        else { terrStatus = 'Active'; }



        rowsData += "<tr style='text-align:center'>" +
             //'<td><div style="text-align:center"><button type="button" class="cls_view_data btn btn-info" onclick="view_popup(this,\'' + row["Employee_ID"] + '\',\'' + row['Territory_ID'] + '\',\'' + row['Salesforce_ID'] + '\')"  style="cursor: pointer;">' + "View" + '</button></div></td>' +
            "<td>" + row["Salesforce_ID"] + "</td><td>" + row["Territory_ID"] + "</td>" +
            "<td>" + row["Territory_Name"].toUpperCase() + "</td><td>" + row["role_code"] + "</td><td>" + row["Territory_Type_Name"] + "</td><td>" + row["User_ID"] + "</td><td>" + fullName + "</td><td>" + row["Employee_title_desc"] + "</td><td>" + terrStatus + "</td>" +
                           //"<td style='text-align:center'>Manager Name</td>" +
"<td style='text-align:center'>" + fullManagerName + "</td>";

        //"<td style='text-align:center'>First Name </td>" +
        rowsData += "<td style='text-align:center'>" + row['First_Name'] + "  </td>" +
//"<td style='text-align:center'>Middle Name </td>" +
"<td style='text-align:center'>" + row['Middle_Name'] + " </td>" +
//"<td style='text-align:center'>Last Name </td>" +
"<td style='text-align:center'>" + row['Last_Name'] + " </td>" +
//"<td style='text-align:center'>Employee ID</td>" +
//"<td style='text-align:center'>" + row['Employee_ID'] + "</td>" +
"<td style='text-align:center'>" + row['EmpID'] + "</td>" +
//"<td style='text-align:center'>Suffix</td>" +
"<td style='text-align:center'>" + row['Suffix'] + "</td>" +
//"<td style='text-align:center'>Manager ID</td>" +
"<td style='text-align:center'>" + row['manager_id'] + "</td>" +
//"<td style='text-align:center'>Shiping Address</td>" +
"<td style='text-align:center'>" + row['M_Address1'] + "</td>" +
//"<td style='text-align:center'>Shiping Address2</td>" +
"<td style='text-align:center'>" + row['M_Address2'] + "</td>" +
//"<td style='text-align:center'>Shiping city</td>" +
"<td style='text-align:center'>" + row['M_City'] + "</td>" +
//"<td style='text-align:center'>Shiping state</td>" +
"<td style='text-align:center'>" + row['M_State'] + "</td>" +
//"<td style='text-align:center'>Shiping zip</td>" +
"<td style='text-align:center'>" + row['M_Zip'] + "</td>" +
//"<td style='text-align:center'>Mailing Address</td>" +
"<td style='text-align:center'>" + row['Home_Address1'] + "</td>" +
//"<td style='text-align:center'>Mailing address2</td>" +
"<td style='text-align:center'>" + row['Home_Address2'] + "</td>" +
//"<td style='text-align:center'>Mailing City</td>" +
"<td style='text-align:center'>" + row['Home_City'] + "</td>" +
//"<td style='text-align:center'>Home State</td>" +
"<td style='text-align:center'>" + row['Home_State'] + "</td>" +
//"<td style='text-align:center'>Mailing Zip</td>" +
"<td style='text-align:center'>" + row['Home_Zip'] + "</td>" +
//"<td style='text-align:center'>Office Phone</td>" +
"<td style='text-align:center'>" + row['Office_Phone'] + "</td>" +
//"<td style='text-align:center'>Company Email</td>" +
"<td style='text-align:center'>" + row['Company_Email'] + "</td>" +
//"<td style='text-align:center'>Home Phone</td>" +
"<td style='text-align:center'>" + row['Home_Phone'] + "</td>" +
//"<td style='text-align:center'>CellPhone</td>" +
"<td style='text-align:center'>" + row['Mobile_Phone'] + "</td>" +
//"<td style='text-align:center'>Personal Email</td>" +
"<td style='text-align:center'>" + row['Personal_Email'] + "</td>" +
//"<td style='text-align:center'>Hire Date</td>" +
"<td style='text-align:center'>" + row['Employee_hire_date'] + "</td>" +
//"<td style='text-align:center'>Position Start Date</td>" +
"<td style='text-align:center'>" + row['emp_position_start_date'] + "</td>" +
//"<td style='text-align:center'>Position End Date</td>" +
"<td style='text-align:center'>" + row['emp_position_end_date'] + "</td>" +
//"<td style='text-align:center'>Termination Date</td>" +
"<td style='text-align:center'>" + row['Employee_termination_date'] + "</td>" +
//"<td style='text-align:center'>Field Start Date</td>" +
"<td style='text-align:center'>" + row['Employee_field_Start_date'] + "</td>" +
"<td style='text-align:center'>" + row['Employee_type_desc'] + "</td>" +
//"<td style='text-align:center'>Employee Status</td>" +
"<td style='text-align:center'>" + EmpStaus + "</td>" +
            //"<th style='text-align:center'>Fieldforce Name</th>" +
                "<td style='text-align:center'>" + row['Salesforce_Name'] + "</td>" +
                //"<th style='text-align:center'>Fieldforce Start Date</th>" +
                "<td style='text-align:center'>" + row['fieldforce_start_date'] + "</td>" +
                //"<th style='text-align:center'>Fieldforce End Date</th>" +
                "<td style='text-align:center'>" + row['fieldforce_end_date'] + "</td>" +
                //"<th style='text-align:center'>Geography Start Date</th>" +
                "<td style='text-align:center'>" + row['terr_start_date'] + "</td>" +
                //"<th style='text-align:center'>Geography End Date</th>" +
                "<td style='text-align:center'>" + row['terr_end_date'] + "</td>" +
                ////"<th style='text-align:center'>Default Geography ZIP</th>" +
                //"<td style='text-align:center'>" + row['Default_Terr_ZIP'] + "</td>" +
                ////"<th style='text-align:center'>Geography ZIP Latitude</th>" +
                //"<td style='text-align:center'>" + row['Territory_ZIP_latitude'] + "</td>" +
                ////"<th style='text-align:center'>Geography ZIP Longitude</th>" +
                //"<td style='text-align:center'>" + row['Territory_ZIP_longitude'] + "</td>" +
                ////"<th style='text-align:center'>Rep Address Latitude</th>" +
                //"<td style='text-align:center'>" + row['Rep_Addr_Latitude'] + "</td>" +
                ////"<th style='text-align:center'>Rep Address Longitude</th>" +
                //     "<td style='text-align:center'>" + row['Rep_Addr_Longitude'] + "</td>" +
                ////"<th style='text-align:center'>Account Centroid Latitude</th>" +
                //     "<td style='text-align:center'>" + row['Acct_Cntrd_Latitude'] + "</td>" +
                ////"<th style='text-align:center'>Account Centroid Longitude</th>" +
                //     "<td style='text-align:center'>" + row['Acct_Cntrd_Longitude'] + "</td>" +
                //"<th style='text-align:center'>Employee Geography Status</th>" +
                     "<td style='text-align:center'>" + row['Status_Desc'] + "</td>" +
             //"<th style='text-align:center'>Sample Shipment Address </th>" +
             "<td style='text-align:center'>" + row['Ship_to_Address'] + "</td>" +
             //"<th style='text-align:center'>Sample Shipment Address 2</th>" +
             "<td style='text-align:center'>" + row['Ship_to_Address2'] + "</td>" +
             //"<th style='text-align:center'>Sample Shipment City</th>" +
             "<td style='text-align:center'>" + row['Ship_to_city'] + "</td>" +
             //"<th style='text-align:center'>Sample Shipment State</th>" +
             "<td style='text-align:center'>" + row['Ship_to_State'] + "</td>" +
             //"<th style='text-align:center'>Sample Shipment Zip</th>" +
                     "<td style='text-align:center'>" + row['Ship_to_Zip'] + "</td>" +
        //"<th style='text-align:center'>Parent Geography #</th>" +
                   "<td style='text-align:center'>" + row['pID'] + "</td>" +
        //    "<th style='text-align:center'>Parent Geography Name</th>" +
                   "<td style='text-align:center'>" + row['p_name'] + "</td>" +
        //    "<th style='text-align:center'>2nd Level Parent Geography #</th>" +
                   "<td style='text-align:center'>" + row['gID'] + "</td>" +
        //    "<th style='text-align:center'>2nd Level Parent Geography Name</th>";
                   "<td style='text-align:center'>" + row['g_name'] + "</td>" +
             //      "<th style='text-align:center'>AZ Territory Number</th>" +
             "<td style='text-align:center'>" + row['AZ_Territory_No'] + "</td>" +
             //"<th style='text-align:center'>PRID</th>"+
             "<td style='text-align:center'>" + row['PRID'] + "</td>"+
        //"<th style='text-align:center'>AZ Employee ID</th>"+
        "<td style='text-align:center'>" + row['AZ_Employee_ID'] + "</td>"+
        //"<th style='text-align:center'>AZ Email ID</th>"+
        "<td style='text-align:center'>" + row['AZ_Email_ID'] + "</td>";

        rowsData += '</tr>';
    }
    rowsData += "</table>";

    table += rowsData;
    //table2 += rowsData;
    //var IncludeInactiveFlg = "";
    //if ($("#includeInactiveInreport").prop('checked')) {
    //    IncludeInactiveFlg = true;
    //}
    $('#ExelPreview').html('');
    $('#ExelPreview').html(table);
    var tbl = $("#tbl_Excel");
    var indexofColumn = 0;
    var b = visibleColumnsOrder.map(function (item) {
        return parseInt(item, 10);
    });
    var moveColmn = 0;
    var newMoveCount = 0;
    for (var i = 0; i < b.length; i++) {
        if (indexofColumn == b[i]) {
            indexofColumn++;
            moveColmn = indexofColumn;
        }
        else {
            jQuery.moveColumn(tbl, b[i], indexofColumn++);
            for (var j = i + 1; j < visibleColumnsOrder.length; j++) {
                if (b[j] < b[i]) {
                    b[j] = b[j] + newMoveCount + 1;
                }
            }
        }
        moveColmn = b[i];
    }
    oTableExcle = new Object();
    
    if (!$.fn.DataTable.isDataTable("#tbl_Excel")) {
        if (!IsPredefiendreport) {
            if (DeleteBtn) {
                oTableExcle = $('#tbl_Excel').DataTable({
                    "dom": '<B><".InActiveEmployee"><"top"l>rti<<"#leftDesc.col-lg-6 col-md-6 left">".col-lg-4 col-md-4"p>',
                    "bInfo": false,
                    "autoWidth": false,
                    "bLengthChange": false,
                    "pageLength": 10,
                    "bFilter": true,
                    select: true,
                    buttons: [
                        {
                            'extend': 'excel', 'className': 'btn btn-success btn-spacing', title: ExcelName, text: 'Excel',
                            'exportOptions': {
                                columns: ':visible'
                            }, customize: function (xlsx) {
                                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                                var lastCol = sheet.getElementsByTagName('row').length - 1;
                                var rows = $('row', sheet);
                                var AppendFilterIndex = rows.length + 4;
                                var appendFilter = AppendFilterAfterDataInExcel(AppendFilterIndex, ExcelName);
                                $('sheetData', sheet).append(appendFilter);

                            }
                        }, {
                            extend: 'csv', 'className': 'btn btn-success btn-spacing',
                            extension: '.csv',
                            title: ExcelName,
                            text: 'CSV',
                            'exportOptions': {
                                columns: ':visible'
                            }

                        }, {
                            extend: 'csv', 'className': 'btn btn-success btn-spacing',
                            title: ExcelName,
                            text: 'PSV',
                            'exportOptions': {
                                columns: ':visible'
                            },

                            fieldSeparator: '|',
                            extension: '.txt',
                            customize: function (csv) {
                                //debugger;
                                return csv.replace(/"/g, '')
                            }
                        },
                             {
                                 text: 'Exit', 'className': 'btn btn-danger btn-spacing',
                                 action: function (e, dt, node, config) {
                                     $('#ExcelPreviewClose').trigger('click');
                                 }
                             },
                            
                             {
                                 text: 'Report as of: ' + $('#ReportSelectedDate').val(), 'className': 'ReportDateClass ',
                             },
                    ],
                    "scrollX": true,
                    "fixedColumns": {
                        leftColumns: 3
                    },
                    "order": [[1, "asc"]],
                });
            } 
            else {
                oTableExcle = $('#tbl_Excel').DataTable({
                    "dom": '<B><".InActiveEmployee"><"top"l>rti<<"#leftDesc.col-lg-6 col-md-6 left">".col-lg-4 col-md-4"p>',
                    "bInfo": false,
                    "autoWidth": false,
                    "bLengthChange": false,
                    "pageLength": 10,
                    "bFilter": true,
                    buttons: [
                         {
                             'extend': 'excel', 'className': 'btn btn-success btn-spacing', title: ExcelName, text: 'Excel',
                             'exportOptions': {
                                 columns: ':visible'
                             }, customize: function (xlsx) {
                                 var sheet = xlsx.xl.worksheets['sheet1.xml'];
                                 var lastCol = sheet.getElementsByTagName('row').length - 1;
                                 var rows = $('row', sheet);
                                 var AppendFilterIndex = rows.length + 4;
                                 var appendFilter = AppendFilterAfterDataInExcel(AppendFilterIndex, ExcelName);
                                 $('sheetData', sheet).append(appendFilter);

                             }
                         }, {
                             extend: 'csv', 'className': 'btn btn-success btn-spacing',
                             extension: '.csv',
                             title: ExcelName,
                             text: 'CSV',
                             'exportOptions': {
                                 columns: ':visible'
                             }

                         }, {
                             extend: 'csv', 'className': 'btn btn-success btn-spacing',
                             title: ExcelName,
                             text: 'PSV',
                             'exportOptions': {
                                 columns: ':visible'
                             },

                             fieldSeparator: '|',
                             extension: '.txt',
                             customize: function (csv) {
                                 //debugger;
                                 return csv.replace(/"/g, '')
                             }
                         }, {
                             text: 'Exit', 'className': 'btn btn-danger btn-spacing',
                             action: function (e, dt, node, config) {
                                 $('#ExcelPreviewClose').trigger('click');
                             }
                         }, {
                             text: 'Report as of: ' + $('#ReportSelectedDate').val(), 'className': 'ReportDateClass ',
                         },
                    ],

                    "scrollX": true,
                    "fixedColumns": {
                        leftColumns: 3
                    },
                    "order": [[1, "asc"]],
                });
            }

            //var str = "";
            //if (IncludeInactiveFlg) {
            //    //$("#includeInactiveInreport").prop('checked', true)
            //    var str = '<div style="width:100%;position: relative;text-align: center;font-size: 14px;display:inline-block"><label style="color:black">Include Inactive</label>  <label class="switch">' +
            //    '<input type="checkbox" onchange="EmployeeIncludeInactiveRecoredInReport(this);" id="includeInactiveInreport" checked>' +
            //    '<div class="slider round"></div>' +
            //    '</label></div>';
            //}
            //else {
            //    var str = '<div style="width:100%;position: relative;text-align: center;font-size: 14px;display:inline-block"><label style="color:black">Include Inactive</label>  <label class="switch">' +
            //    '<input type="checkbox" onchange="EmployeeIncludeInactiveRecoredInReport(this);" id="includeInactiveInreport">' +
            //    '<div class="slider round"></div>' +
            //    '</label></div>';
            //}
            
            //$(".InActiveEmployee").append(str);
            //$(".InActiveEmployee").css({ 'height': $(".dt-buttons").height() });
            //$(".InActiveEmployee").css({ 'margin-top': '-' + $(".dt-buttons").height() + 'px' });
            
        }
        //if (!$("#AsOnDateRoster").prop('checked')) {
        //    $('.ReportDateClass').html('<span> Report Form Date : ' + $('#ReportSelectedDate').val() + ' - To Date :' + $('#ReportSelectEndDate').val() + '</span>');
        //}
       
        var visible = visibleColumnsOrder.length;
        for (var i = 0; i < hiddenColumnsa.length ; i++) {
            oTableExcle.column(i + visible).visible(false);
        }
        
        var newStr = "";
        var FielForceNames = "";
        var opionSalseforce = $('#report_Salesforce_ID').val();
        var salseforceIndex = 0;
        if (opionSalseforce != null) {
            var displayText = new Array();
            for (var j = 0 ; j < opionSalseforce.length; j++) {
                salseforceIndex = 0;
                $('#report_Salesforce_ID option').each(function () {
                    var $this = $(this);
                    if ($this.length) {
                        var ovalue = this.value;
                        var otext = $this.text();

                        if (opionSalseforce[j] == ovalue) {
                            FielForceNames += otext + ', ';

                        }
                    }
                    salseforceIndex++;
                });
            }
            FielForceNames = FielForceNames.substring(0, FielForceNames.length - 2);
            if (opionSalseforce.length == salseforceIndex) {
                FielForceNames = "ALL";
            }
        } else {
            FielForceNames = "ALL";
        }
        var geographyName = $("#report_geographyID").val();
        var GName = "";
        if (geographyName != "") {
        }
        else {
            geographyName = "All";
        }
        var RoleNames = "";
        var optionReportRole = $('#report_role_Name').val();
        var roleIndex = 0;
        if (optionReportRole != null) {
            var displayText = new Array();
            for (var j = 0 ; j < optionReportRole.length; j++) {
                var pre = "";
                roleIndex = 0;
                $('#report_role_Name option').each(function () {

                    var $this = $(this);
                    if ($this.length) {
                        var ovalue = this.value;
                        var otext = $this.text();
                        if (pre == "" || pre != ovalue) {
                            if (optionReportRole[j] == ovalue) {

                                pre = ovalue;
                                if (!RoleNames.includes(',' + otext + ','))
                                    RoleNames += otext + ', ';
                            }
                        }
                    }
                    roleIndex++;
                });

            }
            RoleNames = RoleNames.substring(0, RoleNames.length - 2);
            if (optionReportRole.length == roleIndex) {
                RoleNames = "ALL";
            }
        } else {
            RoleNames = "ALL";
        }
        var TypesNames = "";
        var optionReportType = $('#report_TerritoryTyep').val();
        var TypeIndex = 0;
        if (optionReportType != null) {
            for (var j = 0 ; j < optionReportType.length; j++) {
                TypeIndex = 0;
                $('#report_TerritoryTyep option').each(function () {

                    var $this = $(this);
                    if ($this.length) {
                        var ovalue = this.value;
                        var otext = $this.text();
                        if (optionReportType[j] == ovalue) {
                            if (!TypesNames.includes(',' + otext + ', '))
                                TypesNames += otext + ', ';
                        }
                    }
                    TypeIndex++;
                });
            }
            TypesNames = TypesNames.substring(0, TypesNames.length - 2);
            if (optionReportType.length == TypeIndex) {
                TypesNames = "ALL";
            }
        } else {
            TypesNames = "ALL";
        }

        var AssignmentTypes = "";
        var optionAssignmentType = $('#EmployeeAssignmentType').val();
        var TypeIndex = 0;
        if (optionAssignmentType != null) {
            for (var j = 0 ; j < optionAssignmentType.length; j++) {
                TypeIndex = 0;
                $('#EmployeeAssignmentType option').each(function () {

                    var $this = $(this);
                    if ($this.length) {
                        var ovalue = this.value;
                        var otext = $this.text();
                        if (optionAssignmentType[j] == ovalue) {
                            if (!AssignmentTypes.includes(',' + otext + ', '))
                                AssignmentTypes += otext + ', ';
                        }
                    }
                    TypeIndex++;
                });
            }
            AssignmentTypes = AssignmentTypes.substring(0, AssignmentTypes.length - 2);
            if (optionAssignmentType.length == TypeIndex) {
                AssignmentTypes = "ALL";
            }
        } else {
            AssignmentTypes = "ALL";
        }
        newStr = " <div style='text-decoration: underline;font-weight: 500;'> Filtering Criteria: </div> "+
        "Fieldforce: <b style='padding-right:5px'>" + FielForceNames + "</b>   Geography: <b style='padding-right:5px'>" + geographyName + "</b>    Role: <b style='padding-right:5px'>" + RoleNames + "</b>    Type: <b>" + TypesNames + "</b>    Assignment Type: <b>" + AssignmentTypes + "</b> ";
        $("#leftDesc").append(newStr);
        $("#leftDesc").css('white-space','normal');
    }


}

function MoveSelectedLeft() {
    var _sourceList =
       new fieldList(TotalFields, 0).addClass("fc-source-fields");
    _sourceList.add($('#FieldsToShow div.fc-selected'));
    $('#FieldsToShow div').removeClass('fc-selected');
    $('#TotalFields div').removeClass('fc-selected');
    SortTotalfieldlist();
    return false;
}

function MoveSelectedRight() {
    var _destinationList =
       new fieldList(FieldsToShow, 0).addClass("fc-destination-fields");
    _destinationList.add($('#TotalFields div.fc-selected'));
    $('#TotalFields div').removeClass('fc-selected');
    $('#FieldsToShow div').removeClass('fc-selected');
    return false;
}

function SelctAndMove($this) {
    var parent = $($this).parent();
    $($this).addClass('fc-selected');
    if ($($($this).parent()).attr('id') == 'TotalFields') { MoveSelectedRight(); }
    else { MoveSelectedLeft(); SortTotalfieldlist(); }
    $($this).removeClass('fc-selected');
}

function SortTotalfieldlist() {
    var TototalFieldsArray = new Array();
    var index = 0;
    $("#TotalFields div").each(function (item) {
        var tempObj = new Object();
        tempObj["text"] = $(this).text();
        tempObj["value"] = $(this).attr('value');
        TototalFieldsArray.push(tempObj);
    });
    TototalFieldsArray.sort(function compare(a, b) {
        if (a.text < b.text)
            return -1;
        if (a.text > b.text)
            return 1;
        return 0;
    });
    str = ''
    for (var i = 0; i < TototalFieldsArray.length; i++) {
        if (headerData[i]["value"] != -1) {
            str += '<div value="' + TototalFieldsArray[i]["value"] + '" class="fc-field"  ondblclick="SelctAndMove(this)">' + TototalFieldsArray[i]["text"] + '</div>';
        }
    }
    $("#TotalFields").html('');
    $("#TotalFields").html(str);
    var _sourceList = new fieldList(TotalFields, 0).addClass("fc-source-fields");

}

function OpenreportInEdit($this) {
    ClearFiltersRecordForReport();
    if ($($this).data('assignmenttype') == null) {
        $($this).data('assignmenttype', '');
    }
    $("#previewExcelID").data('reportID', $($this).data('reportid'));
    $("#previewExcelID").data('reportname', $($this).data('name'));
    $("#previewExcelID").data('reportcolumns', $($this).data('columns'));
    $("#previewExcelID").data('reportgeography', $($this).data('geography'));
    $("#previewExcelID").data('reportrole', $($this).data('role'));
    $("#previewExcelID").data('reporttype', $($this).data('type'));
    $("#previewExcelID").data('reportsalesforce', $($this).data('salesforce'));
    $("#previewExcelID").data('reportassignmenttype', $($this).data('assignmenttype'));
    $("#previewExcelID").data('reportincludeinactiveemps', $($this).data('includeinactiveemps'));
    $("#DeleteExistingReport").show();
    $("#RptName").html('');
    $("#RptName").html($($this).data('name'));
    $("#FieldsToShow div").each(function (item) {
        $(this).addClass('fc-selected');
        MoveSelectedLeft();
        $(this).removeClass('fc-selected');
    });
    ReportParameter["Salesforce"] = $($this).data('salesforce');
    ReportParameter["Geography"] = $($this).data('geography');
    ReportParameter["Role"] = $($this).data('role');
    ReportParameter["Type"] = $($this).data('type');
    ReportParameter["Columns"] = $($this).data('columns');
    ReportParameter["Name"] = $($this).data('name');
    ReportParameter["Assignmenttype"] = $($this).data('assignmenttype');
    ReportParameter["IncludeInactive"] = $($this).data('includeinactiveemps');

    var option = ConvertStringToArray(ReportParameter["Salesforce"]);
    var optionRole = ConvertStringToArray(ReportParameter["Role"]);
    var optionType = ConvertStringToArray(ReportParameter["Type"]);
    var Assignmenttype = ConvertStringToArray(ReportParameter["Assignmenttype"]);
    var includeInactivEmpReport = $($this).data('includeinactiveemps');
    $("#report_geographyID").val(ReportParameter["Geography"])
    ExcelName = ReportParameter["Name"] + '_' + $('#ReportSelectedDate').val();
    $('#report_Salesforce_ID').multiselect('select', option);
    $('#report_TerritoryTyep').multiselect('select', optionType);
    $('#report_role_Name').multiselect('select', optionRole);
    $('#EmployeeAssignmentType').multiselect('select', Assignmenttype);
    if(includeInactivEmpReport=="Y")
        $("#includeInactiveInreport").prop('checked', true);
    else
        $("#includeInactiveInreport").prop('checked', false);
    visibleColumnsOrder = ConvertStringToArray(ReportParameter["Columns"]);
    for (var z = 0; z < visibleColumnsOrder.length; z++) {
        $("#TotalFields div").each(function (item) {
            if ($(this).attr('value') == visibleColumnsOrder[z]) {
                $(this).addClass('fc-selected');
                MoveSelectedRight();
                $(this).removeClass('fc-selected');
            }
        });
    }
}

function OpenPredefinedReports(id) {
    
    $this = id;
    OpenreportInEdit(id);
    ReportParameter = new Object();
    PrevieFlag = true;
    ReportParameter["Salesforce"] = $(id).data('salesforce');
    ReportParameter["Geography"] = $(id).data('geography');
    ReportParameter["Role"] = $(id).data('role');
    ReportParameter["Type"] = $(id).data('type');
    ReportParameter["Columns"] = $(id).data('columns');
    ReportParameter["Name"] = $(id).data('name');
    
    ReportParameter["Assignmenttype"] = $(id).data('assignmenttype');
    ReportParameter["IncludeInactive"] = $(id).data('includeinactiveemps');
    var AsonRoster = "";
    if ($("#AsOnDateRoster").prop('checked')) {
       
        var EndDate = $("#ReportSelectedDate").val();
        AsonRoster = "Yes";
    }
    else {
        if ($("#ReportSelectEndDate").val() == "") {
            CustomAlert("Please enter to date.");
            return false;
        }
        var EndDate = $("#ReportSelectEndDate").val();
        AsonRoster = "No";
    }
    $("#loading").show();
    //DeGrToolWebService.GetRoasterExcelData($('#username').val(), ReportParameter["Salesforce"], ReportParameter["Geography"], ReportParameter["Role"], ReportParameter["Type"], $("#ReportSelectedDate").val(), ReportParameter["Assignmenttype"], EndDate, AsonRoster, datatablesetExcel);
    if ($("#includeInactiveInreport").prop('checked')) {
        var IncludeInactiveEmployee = "Yes";
    } else {
        var IncludeInactiveEmployee = "No";
    }
    DeGrToolWebService.GetRoasterExcelData($('#username').val(), ConvertArrayToString($('#report_Salesforce_ID').val()), $("#report_geographyID").val(), ConvertArrayToString($("#report_TerritoryTyep").val()), ConvertArrayToString($("#report_role_Name").val()), $("#ReportSelectedDate").val(), ConvertArrayToString($("#EmployeeAssignmentType").val()), EndDate, AsonRoster, IncludeInactiveEmployee, datatablesetExcel);
}

function ClearFiltersRecordForReport() {
    $('#report_Salesforce_ID').val('');
    $('#report_Salesforce_ID').multiselect('refresh');
    $("#report_geographyID").val('');
    $('#report_TerritoryTyep').val('');
    $("#report_TerritoryTyep").multiselect('refresh');
    $('#report_role_Name').val('');
    $("#report_role_Name").multiselect('refresh');
    $('#EmployeeAssignmentType').val('');
    $("#EmployeeAssignmentType").multiselect('refresh');
    $("#includeInactiveInreport").prop('checked',false);
}

function ClearReportSelection() {

    $("#FieldsToShow div").each(function (item) {
        $(this).addClass('fc-selected');
        MoveSelectedLeft();
    });
    SortTotalfieldlist();
    $("#previewExcelID").data('reportID', "");
    $("#previewExcelID").data('reportname', "");
    $("#previewExcelID").data('reportcolumns', "");
    $("#previewExcelID").data('reportgeography', "");
    $("#previewExcelID").data('reportrole', "");
    $("#previewExcelID").data('reporttype', "");
    $("#previewExcelID").data('reportsalesforce', "");
    $("#previewExcelID").data('reportassignmenttype', "");
    $("#previewExcelID").data('reportincludeinactiveemps', "");
    
    ClearFiltersRecordForReport();
    $("#RptName").html('');
    $("#RptName").html('new');
    DeGrToolWebService.GetRoleMasterValues($('#username').val(), '', FillFilterRoleDropDown);
    DeGrToolWebService.GetTerritoryTypeValues($('#username').val(), '', FillFilterGeographyTypeDropDown);
}

function SaveNewReport() {
    if ($("#previewExcelID").data('reportID') != "" && $("#previewExcelID").data('reportID') != undefined) {
        var Repordata = new Object();
        Repordata["Columns"] = $("#previewExcelID").data('reportcolumns');
        Repordata["Geography"] = $("#previewExcelID").data('reportgeography');
        Repordata["Role"] = $("#previewExcelID").data('reportrole');
        Repordata["Type"] = $("#previewExcelID").data('reporttype');
        Repordata["Salesforce"] = $("#previewExcelID").data('reportsalesforce');
        Repordata["AccountType"] = $("#previewExcelID").data('reportassignmenttype');
        Repordata["IncludeInactiveEmp"] = $("#previewExcelID").data('reportincludeinactiveemps');
        var OldData = new Object();
        var IncludeInactiveEmps = "";
        if ($("#includeInactiveInreport").prop('checked')) {
            IncludeInactiveEmps = "Y";
        }
        else {
            IncludeInactiveEmps = "N";
        }
        visibleColumnsOrder = new Array();
        $('#FieldsToShow > div').each(function () {
            visibleColumnsOrder.push($(this).attr('value'));
        });
        if (visibleColumnsOrder.length <= 0) {
            CustomAlert('Please select columns.');
        }
        OldData["Columns"] = visibleColumnsOrder;
        OldData["Geography"] = $("#report_geographyID").val();
        OldData["Role"] = ConvertArrayToString($("#report_role_Name").val());
        OldData["Type"] = ConvertArrayToString($("#report_TerritoryTyep").val())
        OldData["Salesforce"] = ConvertArrayToString($('#report_Salesforce_ID').val());
        OldData["AccountType"] = ConvertArrayToString($('#EmployeeAssignmentType').val());
        OldData["IncludeInactiveEmp"] = IncludeInactiveEmps;
        if (CopareReportDataWithOld(Repordata, OldData)) {
            $("#loading").show();
          
            DeGrToolWebService.AddNewReport($('#username').val(), $("#previewExcelID").data('reportname'), ConvertArrayToString($('#report_Salesforce_ID').val()), $("#report_geographyID").val(), ConvertArrayToString($("#report_role_Name").val()), ConvertArrayToString($("#report_TerritoryTyep").val()), ConvertArrayToString((visibleColumnsOrder)), $("#previewExcelID").data('reportID'),  ConvertArrayToString($('#EmployeeAssignmentType').val()),IncludeInactiveEmps, ReportSaveMsg);
        }
        else {
            return false;
        }
    }
    else {
        visibleColumnsOrder = new Array();
        $('#FieldsToShow > div').each(function () {
            visibleColumnsOrder.push($(this).attr('value'));
        });
        if (visibleColumnsOrder.length <= 0) {
            CustomAlert('Please select columns.');
            return false;
        }
        SeveNewReport();
    }
}

function CopareReportDataWithOld(NewData, OldData) {
    var count = 0;
    if (NewData["Columns"] !== ConvertArrayToString(OldData["Columns"])) { count++; }
    if (NewData["Geography"] !== OldData["Geography"]) { count++; }
    if (arr_diff(ConvertStringToArray(NewData["Role"]), ConvertStringToArray(OldData["Role"])).length > 0) { count++; }
    if (arr_diff(ConvertStringToArray(NewData["Type"]), ConvertStringToArray(OldData["Type"])).length > 0) { count++; }
    if (arr_diff(ConvertStringToArray(NewData["Salesforce"]), ConvertStringToArray(OldData["Salesforce"])).length > 0) { count++; }
    if (arr_diff(ConvertStringToArray(NewData["AccountType"]), ConvertStringToArray(OldData["AccountType"])).length > 0) { count++; }
    if (NewData["IncludeInactiveEmp"] !== OldData["IncludeInactiveEmp"]) { count++; }
    
    if (count == 0) {
        CustomAlert("No changes found.")
        return false;
    }
    else { return true; }


}

function SaveNewReportInDb() {
    $("#loading").show();
    var IncludeInactiveEmps = "";
    if ($("#includeInactiveInreport").prop('checked')) {
        IncludeInactiveEmps = "Y";
    }
    else {
        IncludeInactiveEmps = "N";
    }
    DeGrToolWebService.AddNewReport($('#username').val(), $("#NewReportName").val(), ConvertArrayToString($('#report_Salesforce_ID').val()), $("#report_geographyID").val(), ConvertArrayToString($("#report_role_Name").val()), ConvertArrayToString($("#report_TerritoryTyep").val()), ConvertArrayToString((visibleColumnsOrder)), "", ConvertArrayToString($("#EmployeeAssignmentType").val()),IncludeInactiveEmps, ReportSaveMsg);
}

function SeveNewReport() {
    $("#Confirmation").modal({
        backdrop: 'static',
        keyboard: false
    });
}

function ReportSaveMsg(data) {
    $("#loading").hide();
    if (data != "" && data != undefined) {
        var result = JSON.parse(data)
        CustomAlert(result.Message);
        if (result.Status == 1) {
            $('.close2').trigger("click");
            DeGrToolWebService.GetRosterReports(sessionStorage["username"], RosterReportData, onFail);
            $("#ExcelPreviewClose").trigger('click');
            ClearReportSelection();
        }
        else {
            $('.close2').trigger("click");
            return false;
        }
    }
    else {
        CustomAlert("Something went wrong!");
        $('.close2').trigger("click");
    }
}

function DeleteExistingReport1() {
    if ($("#previewExcelID").data('reportID') != "" && $("#previewExcelID").data('reportID') != undefined) {
        BootstrapDialog.show({
            message: "Are you sure you want to remove " + $("#previewExcelID").data('reportname') + "?",
            title: 'Warning!',
            closable: false,
            type: [BootstrapDialog.TYPE_WARNING],
            buttons: [{
                label: 'Proceed',
                cssClass: 'btn-primary',
                action: function (dialogItself) {
                    DeGrToolWebService.DeleteReport($('#username').val(), $("#previewExcelID").data('reportID'), ReportSaveMsg);
                    dialogItself.close();
                }
            }, {
                label: 'Cancel',
                action: function (dialogItself) {
                    dialogItself.close();
                }
            }]
        });
    }
    else {
        CustomAlert("Predefined Report is not selected.");
    }
}

function AppendFilterAfterDataInExcel(addIndex, ExcelName) {
    var SelectedSalesforce = new Array();
    var SelectedRole = new Array();
    var SelectedType = new Array();
    var AssignmentTypeArray = new Array();
    $('#report_Salesforce_ID option:selected').each(function () {
        var $this = $(this);
        if ($this.length) {
            SelectedSalesforce.push($this.text())
        }
    });
    $('#report_role_Name option:selected').each(function () {
        var $this = $(this);
        if ($this.length) {
            SelectedRole.push($this.text())
        }
    });

    $('#report_TerritoryTyep option:selected').each(function () {
        var $this = $(this);
        if ($this.length) {
            SelectedType.push($this.text())
        }
    });
    $('#EmployeeAssignmentType option:selected').each(function () {
        var $this = $(this);
        if ($this.length) {
            AssignmentTypeArray.push($this.text())
        }
    });

    var FilterCriteria = '<row r ="' + addIndex + '"><c t="inlineStr" r="A' + addIndex + '" s="2"><is><t>Filter Criteria </t></is></c></row>';
    addIndex++;
    var ReportName = '<row r="' + addIndex + '"><c t="inlineStr" r="A' + addIndex + '" s="2"><is><t>Report Name </t></is></c><c t="inlineStr" r="B' + addIndex + '"><is><t>' + ExcelName + ' </t></is></c></row>';
    addIndex++;
    var ReportDate = ""
    if ($("#AsOnDateRoster").prop('checked')) {
        ReportDate = '<row r="' + addIndex + '"><c t="inlineStr" r="A' + addIndex + '" s="2"><is><t>Report as of </t></is></c><c t="inlineStr" r="B' + addIndex + '"><is><t>' + $('#ReportSelectedDate').val() + ' </t></is></c></row>';
        addIndex++;
    }
    else {
        ReportDate = '<row r="' + addIndex + '"><c t="inlineStr" r="A' + addIndex + '" s="2"><is><t>Report From Date </t></is></c><c t="inlineStr" r="B' + addIndex + '"><is><t>' + $('#ReportSelectedDate').val() + ' </t></is></c></row>';
        addIndex++;
        ReportDate += '<row r="' + addIndex + '"><c t="inlineStr" r="A' + addIndex + '" s="2"><is><t>Report To Date </t></is></c><c t="inlineStr" r="B' + addIndex + '"><is><t>' + $('#ReportSelectEndDate').val() + ' </t></is></c></row>';
        addIndex++;
    }
    var SalesforceRow = '<row r="' + addIndex + '"><c t="inlineStr" r="A' + addIndex + '" s="2"><is><t>Fieldforce </t></is></c>';
    if (SelectedSalesforce != null) {
        if (SelectedSalesforce.length > 0) {
            for (var i = 0; i < SelectedSalesforce.length; i++) {
                var as = createCellPos(i + 1);
                SalesforceRow += '<c t="inlineStr" r="' + as + '' + addIndex + '"><is><t>' + SelectedSalesforce[i] + ' </t></is></c>';
            }
        }
    }
    SalesforceRow += '</row>';
    addIndex++;
    var GeographyRow = '<row r="' + addIndex + '"><c t="inlineStr" r="A' + addIndex + '" s="2"><is><t>Geography </t></is></c><c t="inlineStr" r="B' + addIndex + '"><is><t>' + $('#report_geographyID').val() + ' </t></is></c></row>';
    addIndex++;
    var RoleRow = '<row r="' + addIndex + '"><c t="inlineStr" r="A' + addIndex + '" s="2"><is><t>Role </t></is></c> ';
    if (SelectedType != null) {
        if (SelectedType.length > 0) {
            for (var i = 0; i < SelectedType.length; i++) {
                var as = createCellPos(i + 1);
                RoleRow += '<c t="inlineStr" r="' + as + '' + addIndex + '"><is><t>' + SelectedType[i] + ' </t></is></c>';
            }
        }
    }
    RoleRow += '</row>';
    addIndex++;
    var TerritoryTypeRow = '<row r="' + addIndex + '"><c t="inlineStr" r="A' + addIndex + '" s="2"><is><t>Geography Type </t></is></c>';
    if (SelectedRole != null) {
        if (SelectedRole.length > 0) {
            for (var i = 0; i < SelectedRole.length; i++) {
                var as = createCellPos(i + 1);
                TerritoryTypeRow += '<c t="inlineStr" r="' + as + '' + addIndex + '"><is><t>' + SelectedRole[i] + ' </t></is></c>';
            }
        }
    }
    TerritoryTypeRow += '</row>';

    addIndex++;
    var AssignmentTypeROW = '<row r="' + addIndex + '"><c t="inlineStr" r="A' + addIndex + '" s="2"><is><t>Assignment Type  </t></is></c>';
    if (AssignmentTypeArray != null) {
        if (AssignmentTypeArray.length > 0) {
            for (var i = 0; i < AssignmentTypeArray.length; i++) {
                var as = createCellPos(i + 1);
                AssignmentTypeROW += '<c t="inlineStr" r="' + as + '' + addIndex + '"><is><t>' + AssignmentTypeArray[i] + ' </t></is></c>';
            }
        }
    }
    AssignmentTypeROW += '</row>';
    addIndex++;
    var IncludeInactiveEmployeeRow = '<row r="' + addIndex + '"><c t="inlineStr" r="A' + addIndex + '" s="2"><is><t>Include InactiveEmployees</t></is></c>';
    if ($("#includeInactiveInreport").prop('checked')) {
        IncludeInactiveEmployeeRow += '<c t="inlineStr" r="B' + addIndex + '"><is><t> Y  </t></is></c>';
    }
    else {
        IncludeInactiveEmployeeRow += '<c t="inlineStr" r="B' + addIndex + '"><is><t> N  </t></is></c>';
    }
    
    IncludeInactiveEmployeeRow += '</row>';
    addIndex++;
    FilterCriteria += ReportName + ReportDate + SalesforceRow + GeographyRow + RoleRow + TerritoryTypeRow + AssignmentTypeROW + IncludeInactiveEmployeeRow;
    return FilterCriteria;
}

function createCellPos(n) {
    var ordA = 'A'.charCodeAt(0);
    var ordZ = 'Z'.charCodeAt(0);
    var len = ordZ - ordA + 1;
    var s = "";
    while (n >= 0) {
        s = String.fromCharCode(n % len + ordA) + s;
        n = Math.floor(n / len) - 1;
    }
    return s;
}
//============================================================== Report tab functions Ends =================================================


//============================== genaral functions Starts =====================================
function ConvertArrayToString(array) {
    var str = "";
    if (array != null) {
        if (array.length != 0) {
            for (var i = 0 ; i < array.length; i++) {
                if (i < array.length - 1) {
                    str += '\'' + array[i] + '\',';
                }
                else {
                    str += '\'' + array[i] + '\'';
                }
            }
        }
    }
    return str;
}

function ConvertStringToArray(str) {
    var array = new Array();
    if (str != "") {
        str = str.replace(/\'/g, "");
        array = str.split(',');
    }
    return array;
}

function CustomAlert(message) {

    BootstrapDialog.show({

        title: 'Message',
        message: message,

        //buttons: [
        //  {
        //      label: 'Close',
        //      cssClass: 'btn btn-rounded  btn-primary',
        //      action: function (dialogItself) {

        //          dialogItself.close();

        //          return false;
        //      }
        //  }]
    });
}

function replaceNull(obj) {
    var newobj = new Object();
    for (var key in obj) {
        if (obj[key] == null) {
            newobj[key] = "";
        }
        else {
            newobj[key] = obj[key];
        }
    }
    return newobj;
}

function ConvertToEditMode(id, modalID, objThis) {
    //debugger;

    var value = $("#" + id).data('value');
    switch (id) {
        case "p_terr":
            $("#terr_save").data('data-savemode', 'Edit');
            GetTerritoryDetail(value);
            break;
        case "p_salesforceID":
            $("#saleforce_save").data('data-savemode', 'Edit');
            GetSalesForceVaues(value);
            break;
        case "emp_tab_emp_home_address":
            OpenHomeDatadForEdit();
            break;
        case "p_role":
            $("#role_save").data('data-savemode', 'Edit');
            GetRoleVaues(value);
            break;
        case "emp_tab_emp_id":
            $("#employee_save").data('data-savemode', 'Edit');
            GetEmployeeVaues(value);
            break;
        case "emp_tab_company_email":
            OpenEmailDatadForEdit();
            break;
        case "emp_tab_bussiness_phone":
            OpenPhoneDatadForEdit();
            break;
        case "emp_tab_emp_mail_address":
            OpenMailDatadForEdit(id);
            break;
        case "emp_assign_new":
            ClearEployeeAssigment();
            break;

        case "emp_tab_emp_shiping_address":
            OpenShipingDatadForEdit();
            break;
        case "emp_addressRadio":
            HideShowRadioButtons();
            break;

    }

}

function HideShowRadioButtons() {
    if ($("input[name=address]").css('display') != "none") {

        $("input[name=address]").css('display', 'none');
    }
    else {
        $("input[name=address]").css('display', 'block');
        $("input[name=address]:checked").prop('checked', false);
    }
}

function HidePopupField(ids) {
    for (var i = 0; i < ids.length; i++) {
        $("#" + ids[i]).hide();
    }
}

function ShowPopupField(ids) {
    for (var i = 0; i < ids.length; i++) {
        $("#" + ids[i]).show();
    }
}

function MakeFieldNonEditable(ids) {
    for (var i = 0; i < ids.length; i++) {
        $("#" + ids[i]).prop('disabled', true);
    }
}

function MakeFieldEditable(ids) {
    for (var i = 0; i < ids.length; i++) {
        $("#" + ids[i]).prop('disabled', false);
    }
}

function FillStatusDropdown() {
    $(".statusDrp").html('');
    $(".statusDrp").append($('<option></option>', {
        value: "",
        text: "Please Select Status",
    }));
    $(".statusDrp").append($('<option></option>', {
        value: "Active",
        text: "Active",
    }));
    $(".statusDrp").append($('<option></option>', {
        value: "Inactive",
        text: "Inactive",
    }));
}

function MakeFistSelcetSameAsPlaceHolder(id) {
    var sel = document.getElementById(id);
    sel.style.color = "#c3c3c3";
    sel.style.fontSize = "13px";
    sel.style.fontStyle = "italic";
    //sel.style.opacity = "0.5";
}

function MakeSelcetnormal(id) {
    var sel = document.getElementById(id);
    sel.style.color = "black";
    sel.style.opacity = "1";
    sel.style.fontSize = "14px";
    sel.style.fontStyle = "normal";
}

function FillStateDropdown(data) {
    $(".stateMst").html('');
    $(".stateMst").append($('<option></option>', {
        value: "",
        text: "Please Select State",
    }));

    for (var i = 0; i < data.length; i++) {
        $(".stateMst").append($('<option></option>', {
            value: data[i]["State_Code"],
            text: data[i]["State_Code"],
        }));
    }
}

function Coparedate(change, compare, comparewith, Message, ChangeStatus) {
    var comparedate = $("#" + compare).val();
    var comparewithdate = $("#" + comparewith).val();
    var comp = true;

    if (comparedate == "") {
        comp = false;
    }
    if (comparewithdate == "") {
        comp = false;
    }
    if (comp) {
        var d1 = new Date(comparedate);
        var d2 = new Date(comparewithdate);
        if (d1 < d2) {
            CustomAlert(Message);
            $(change).val('')
            return false;
        }
    }
    if (compare != "new_emp_field_start_date") {
        if (comparedate != "") {
            var CompareDatetime = new Date(comparedate);
            var todayDateTime = new Date(today);

            if (todayDateTime <= CompareDatetime) {
                $("#" + ChangeStatus).val('Active');
            }
            else {
                $("#" + ChangeStatus).val('Inactive');
            }
        }
        else {
            $("#" + ChangeStatus).val('Active');
        }
    }

}

function EmailValidation(value, Text,domain) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!regex.test(value)) {
      
        CustomAlert(Text + " is not valid.");
        return false;
    }
    else  if (domain != "") {
        if (value.indexOf(domain, value.length - domain.length) === -1) {
            CustomAlert(Text + " is not valid.");
            return false;

        }
        else {
            return true
        }
    }
    else {
        return true;
    }
}

function OpenOutlookDoc(id) {
    try {
        var subject = '';
        var emailBody = '';
        window.location = 'mailto:' + id + '?subject=' + subject + '&body=' + emailBody;
    }
    catch (e) {
        CustomAlert(e);
    }
}

jQuery.moveColumn = function (table, from, to) {
    var rows = jQuery('tr', table);
    var cols;
    rows.each(function () {
        cols = jQuery(this).children('th, td');
        cols.eq(from).detach().insertBefore(cols.eq(to));
    });
    var i = 0;
    var header = jQuery('tr th', table);
    header.each(function () {
        jQuery(this).data('columnindex', i++)
    });
}

function arr_diff(a1, a2) {
    var a = [], diff = [];
    for (var i = 0; i < a1.length; i++) {
        a[a1[i]] = true;
    }
    for (var i = 0; i < a2.length; i++) {
        if (a[a2[i]]) {
            delete a[a2[i]];
        } else {
            a[a2[i]] = true;
        }
    }
    for (var k in a) {
        diff.push(k);
    }
    return diff;
};

function ResizeTables() {

    if ($.fn.DataTable.isDataTable("#tbl_roster")) {
        oTable.draw();
    } 
    if ($.fn.DataTable.isDataTable("#tbl_employee")) {
        oTableEmployee.draw();
    }
    if ($.fn.DataTable.isDataTable("#tbl_Excel")) {
        oTableExcle
           .order([[0, 'desc']])
           .draw();
    }
    $('#Report').css('height', ($(window).height() - $('#Report').position()["top"]));
}

function ChangeEmpIdOfCurrEmp($this) {
    $("#new_emp_empID").val($this.value);

}
//============================== genaral functions ends =====================================

//========================================== Extra Code for FieldChooser Starts ================================================
function getBounds(element) {
    var offset = element.offset();
    return {
        left: offset.left,
        right: offset.left + element.width(),
        top: offset.top,
        bottom: offset.top + element.height()
    }
}
function translateBounds(bounds, newPosition) {
    return {
        left: newPosition.left,
        right: bounds.right + newPosition.left - bounds.left,
        top: newPosition.top,
        bottom: bounds.bottom + newPosition.top - bounds.top
    }
}
function hitTest(container, element, newPosition) {
    var containerBounds = getBounds(container);
    var elementBounds = getBounds(element);
    elementBounds = translateBounds(elementBounds, newPosition);
    var hit = true;
    if (elementBounds.right < containerBounds.left) {
        hit = false;
    }
    else if (elementBounds.left > containerBounds.right) {
        hit = false;
    }
    else if (elementBounds.bottom < containerBounds.top) {
        hit = false;
    }
    else if (elementBounds.top > containerBounds.bottom) {
        hit = false;
    }
    return hit;
}
var _chooser;
var fieldChooser;
var _lastSelectionList = null;
function onListSelectionChanged(event, list) {
    if (_lastSelectionList) {
        if (_lastSelectionList == list) { }
        else {
            var otherList = _chooser.getSourceList();
            if (list == _chooser.getSourceList()) {
                otherList = _chooser.getDestinationList();
            }
            otherList.clearSelection(true);
        }
    }
    _lastSelectionList = list;
}
var fieldList = function (parent, tabIndex) {
    var _list = $(parent);
    _list.addClass("fc-field-list");
    _list.attr("tabIndex", tabIndex);
    var _selectedIndex = -1;
    var _extendedSelectionIndex = -1;
    var prepareListFields = function (content) {
        content.addClass("fc-field");
        content.attr("tabIndex", tabIndex);
        content.off("click.field");
        content.on("click.field", function (event) {
            event.stopPropagation();
            event.preventDefault();
            var $this = $(this);
            var currentList = _chooser.getFieldList($this);
            if (event.ctrlKey || event.metaKey) {
                currentList.toggleFieldSelection($this);
            }
            else if (event.shiftKey) {
                currentList.selectTo($this);
            }
            else {
                currentList.selectField($this);
            }
            _currentList = currentList;
        });
    };
    prepareListFields(_list.children());
    _list.selectAt = function (index) {
        this.clearSelection(true);
        var fields = _list.getFields();
        if (index >= fields.length) {
            index = fields.length - 1;
        }
        var selectedField = null;
        if (index >= 0) {
            selectedField = fields.eq(index);
        }
        if (selectedField) {
            selectedField.addClass("fc-selected");
            _selectedIndex = index;
            _list.scrollToField(selectedField);
        }
        else {
            _selectedIndex = -1;
        }
        _list.trigger("selectionChanged", [_list]);
    }
    _list.extendSelection = function (up) {
        var selectedIndex = this.getSelectedIndex();
        var extendedIndex = this.getExtendedSelectionIndex();
        var newIndex = extendedIndex;
        var extend = true;
        if (up) {
            if (newIndex < 0) {
                newIndex = _list.getFields().length;
                _selectedIndex = newIndex - 1;
            }
            if (extendedIndex > selectedIndex) { extend = false; }
            else { newIndex--; }
        }
        else {
            if (newIndex < 0) { _selectedIndex = 0; }
            if (extendedIndex < selectedIndex) { extend = false; }
            else { newIndex++; }
        }
        var fields = _list.getFields();
        if (newIndex < 0 || newIndex >= fields.length) {
        }
        else {
            var selectedField = fields.eq(newIndex);
            if (extend) {
                selectedField.addClass("fc-selected");
            }
            else {
                selectedField.removeClass("fc-selected");
                if (up) {
                    newIndex--;
                }
                else {
                    newIndex++;
                }
                if (newIndex >= 0 && newIndex < fields.length) {
                    selectedField = fields.eq(newIndex);
                }
            }
            _list.scrollToField(selectedField);
            _list.trigger("selectionChanged", [_list]);
            _extendedSelectionIndex = newIndex;
        }
    }
    _list.selectField = function (field) {
        this.clearSelection(true);
        field.addClass("fc-selected");
        _selectedIndex = field.index();
        _list.trigger("selectionChanged", [_list]);
    }
    _list.toggleFieldSelection = function (field) {
        field.toggleClass("fc-selected");
        if (field.hasClass("fc-selected")) {
            _selectedIndex = field.index();
            _extendedSelectionIndex = -1;
        }
        else {
            if (_selectedIndex == field.index()) {
                _selectedIndex = _list.children(".fc-selected").first().index();
            }
        }
        _list.trigger("selectionChanged", [_list]);
    }
    _list.selectTo = function (fieldOrIndex) {
        var fieldIndex = fieldOrIndex;
        if (typeof fieldOrIndex == "object") {
            fieldIndex = fieldOrIndex.index();
        }
        if (_selectedIndex == -1) {
            _selectedIndex = 0;
        }
        var children = _list.children();
        if (fieldIndex > _selectedIndex) {
            for (var counter = _selectedIndex;
                 counter < children.length;
                 counter++) {
                if (counter <= fieldIndex) {
                    children.eq(counter).addClass("fc-selected");
                }
                else {
                    children.eq(counter).removeClass("fc-selected");
                }
            }
        }
        else {
            for (var counter = _selectedIndex;
                 counter >= 0;
                 counter--) {
                if (counter >= fieldIndex) {
                    children.eq(counter).addClass("fc-selected");
                }
                else {
                    children.eq(counter).removeClass("fc_selected");
                }
            }
        }
        _extendedSelectionIndex = fieldIndex;
        _list.trigger("selectionChanged", [_list]);
    }
    _list.getSelectedIndex = function () {
        return _selectedIndex;
    }
    _list.getExtendedSelectionIndex = function () {
        var index = _extendedSelectionIndex;
        if (index < 0) {
            index = _selectedIndex;
        }
        return index;
    }
    _list.getSelection = function () {
        return this.children(".fc-selected");
    }
    _list.clearSelection = function (suppressEvent) {
        _selectedIndex = -1;
        _extendedSelectionIndex = -1;
        this.children().removeClass("fc-selected");
        if (suppressEvent) {
        }
        else {
            _list.trigger("selectionChanged", [_list]);
        }
    };
    _list.add = function (content) {
        prepareListFields(content);
        content.appendTo(_list);
        return _list;
    };
    _list.getFields = function () {
        return _list.children();
    }
    _list.scrollToField = function (field) {
        var listTop = _list.position().top;
        var listScrollTop = _list.scrollTop();
        var listHeight = _list.height();
        var listScrollBottom = listScrollTop + listHeight;
        var fieldHeight = field.outerHeight();
        var fieldTop = listScrollTop + field.position().top - listTop;
        var fieldBottom = fieldTop + fieldHeight;
        if (fieldTop < listScrollTop) {
            _list.scrollTop(fieldTop);
        }
        else if (fieldBottom > listScrollBottom) {
            _list.scrollTop(listScrollTop + (fieldBottom - listScrollBottom));
        }
    };
    _list.sortable({
        connectWith: ".fc-field-list",
        cursor: "move",
        opacity: 0.75,
        cursorAt: { left: 5, top: 5 },
        helper: function (event, field) {
            if (field.hasClass("fc-selected")) { }
            else {
                _list.clearSelection(true);
                field.addClass("fc-selected");
            }
            var helper = fieldChooser.getOptions().helper(_list);
            var selection = _list.getSelection().clone();
            if (helper) {
            }
            else {
                helper = $("<div/>").append(selection);
            }
            field.data("selection", selection);
            field.siblings(".fc-selected").remove();
            return helper;
        }
    });
    return _list;
};
var _options = $.extend({}, $.fn.fieldChooser.defaults, options);
var tabIndex = parseInt(this.attr("tabIndex"));
if (isNaN(tabIndex)) {
    tabIndex = 0;
}
this.removeAttr("tabIndex");
var _sourceList = new fieldList(sourceList, tabIndex).addClass("fc-source-fields");
var _destinationList =
    new fieldList(destinationList, tabIndex).addClass("fc-destination-fields");
var _currentList = null
this.getOptions = function () {
    return _options;
};
this.getSourceList = function () {
    return _sourceList;
};
this.getDestinationList = function () {
    return _destinationList;
};
this.getFieldList = function (field) {
    var list = _destinationList;
    if (field.parent().hasClass("fc-source-fields")) {
        list = _sourceList;
    }
    return list;
}
this.destroy = function () {
    this.getOptions = null;
    _sourceList.sortable("destroy");
    _destinationList.sortable("destroy");
};
_destinationList.on("sortstop", function (event, ui) {
    var selection = ui.item.data("selection");
    if (hitTest(_sourceList, ui.item, ui.offset)) {
        _currentList = _sourceList;
        _sourceList.add(selection);
        _destinationList.clearSelection();
        _sourceList.trigger("selectionChanged", [_sourceList]);
        _chooser.trigger("listChanged", [selection, _sourceList]);
        ui.item.after(selection).remove();
        var TototalFieldsArray = new Array();
        var index = 0;
        $(_sourceList).find('div').each(function (item) {
            var tempObj = new Object();
            tempObj["text"] = $(this).text();
            tempObj["value"] = $(this).attr('value');
            TototalFieldsArray.push(tempObj);
        });
        TototalFieldsArray.sort(function compare(a, b) {
            if (a.text < b.text)
                return -1;
            if (a.text > b.text)
                return 1;
            return 0;
        });
        str = ''
        for (var i = 0; i < TototalFieldsArray.length; i++) {
            if (headerData[i]["value"] != -1) {
                str += '<div value="' + TototalFieldsArray[i]["value"] + '" class="fc-field"  ondblclick="SelctAndMove(this)">' + TototalFieldsArray[i]["text"] + '</div>';
            }
        }
        $(_sourceList).html('');
        $(_sourceList).html(str);
        fieldList(_sourceList, 0);
    }
    else if (hitTest(_destinationList, ui.item, ui.offset)) {
        ui.item.after(selection).remove();
    }
    else {
        _destinationList.getSelection().remove();
        _sourceList.add(selection);
        _sourceList.scrollToField(selection);
        _destinationList.clearSelection();
        _sourceList.trigger("selectionChanged", [_sourceList]);
        _currentList = _sourceList;
        _chooser.trigger("listChanged", [selection, _sourceList]);
    }
});
_sourceList.on("sortstop", function (event, ui) {
    var selection = ui.item.data("selection");
    if (hitTest(_destinationList, ui.item, ui.offset)) {
        _currentList = _destinationList;
        _destinationList.add(selection);
        _sourceList.clearSelection();
        _destinationList.trigger("selectionChanged", [_destinationList]);
        _chooser.trigger("listChanged", [selection, _destinationList]);
    }
    else if (hitTest(_sourceList, ui.item, ui.offset)) {
        // continue
        var TototalFieldsArray = new Array();
        var index = 0;
        $(_sourceList).find('div').each(function (item) {
            var tempObj = new Object();
            tempObj["text"] = $(this).text();
            tempObj["value"] = $(this).attr('value');
            TototalFieldsArray.push(tempObj);
        });
        TototalFieldsArray.sort(function compare(a, b) {
            if (a.text < b.text)
                return -1;
            if (a.text > b.text)
                return 1;
            return 0;
        });
        str = ''
        for (var i = 0; i < TototalFieldsArray.length; i++) {
            if (headerData[i]["value"] != -1) {
                str += '<div value="' + TototalFieldsArray[i]["value"] + '" class="fc-field"  ondblclick="SelctAndMove(this)">' + TototalFieldsArray[i]["text"] + '</div>';
            }
        }
        $(_sourceList).html('');
        $(_sourceList).html(str);
        fieldList(_sourceList, 0);
    }
    else { _sourceList.sortable("cancel"); }


    ui.item.after(selection).remove();
});
_destinationList.on("selectionChanged", onListSelectionChanged);
_sourceList.on("selectionChanged", onListSelectionChanged);
_destinationList.on("focusin", function () {
    _currentList = _destinationList;
});
_destinationList.on("focusout", function () {
    if (_currentList == _destinationList) {
        _currentList = null;
    }
});
_sourceList.on("focusin", function () {
    _currentList = _sourceList;
});
_sourceList.on("focusout", function () {
    if (_currentList == _sourceList) {
        _currentList = null;
    }
});
$(document).keydown(function (event) {
    if (_currentList) {
        if (event.which == 38) { // up arrow
            event.stopPropagation();
            event.preventDefault();
            if (event.shiftKey) {
                _currentList.extendSelection(true);
            }
            else {
                var selectedIndex = _currentList.getSelectedIndex();
                var newIndex = selectedIndex - 1;
                if (newIndex < 0) {
                    newIndex = 0;
                }
                _currentList.selectAt(newIndex);
            }
        }
        else if (event.which == 40) { // down arrow 
            event.stopPropagation();
            event.preventDefault();
            if (event.shiftKey) {
                _currentList.extendSelection(false);
            }
            else {
                var selectedIndex = _currentList.getSelectedIndex();
                var newIndex = selectedIndex + 1;
                if (selectedIndex < 0) {
                    newIndex = _currentList.getFields().length - 1;
                }
                _currentList.selectAt(newIndex);
            }
        }
        else if (event.which == 27) { // escape
            _currentList.selectAt(-1);
        }
    }
});
$.fn.fieldChooser.defaults = {
    helper: function (list) { return null; }
};
//=========================================== Extra Code for FieldChooser Ends =================================================

//============================================== function Ends =================================================================
//jQuery(function () {
//    jQuery('#ReportSelectedDate').datetimepicker({
//        format: 'm/d/Y',
//        scrollInput: false,
//        closeOnDateSelect: true,
//        onShow: function (ct) {
//            this.setOptions({
//                maxDate: jQuery('#ReportSelectEndDate').val() ? jQuery('#ReportSelectEndDate').val() : false
//            })
//        },
//        timepicker: false
//    });
//    jQuery('#ReportSelectEndDate').datetimepicker({
//        format: 'm/d/Y',
//        scrollInput: false,
//        closeOnDateSelect: true,
//        onShow: function (ct) {
//            this.setOptions({
//                minDate: jQuery('#ReportSelectedDate').val() ? jQuery('#ReportSelectedDate').val() : false
//            })
//        },
//        timepicker: false
//    });
//});