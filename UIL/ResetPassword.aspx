﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ResetPassword.aspx.cs" Inherits="ResetPassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reset Password</title>
    <link href="Menu/menu.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        div#menu
        {
            width: 98%;
            float: left;
            margin-left: 0.8%;
        }
        div#copyright
        {
            margin: 0 auto;
            width: 80%;
            font: 11px 'Trebuchet MS';
            color: #124a6f;
            text-indent: 20px;
            padding: 40px 0 0 0;
        }
        div#copyright a
        {
            color: #4682b4;
        }
        div#copyright a:hover
        {
            color: #124a6f;
        }
        .header
        {
            width: 100%;
            height: 100px;
            float: left; /*background: url(images/header.png) no-repeat;*/
        }
        .clogo
        {
            width: 100%;
            height: 86px;
            float: left;
            margin-top: 0.7%;
        }
        img, object
        {
            max-width: 100%;
        }
        object
        {
            margin: 0 auto 1em;
            max-width: 100%;
        }
        .headermidd
        {
            width: auto;
            float: left;
            font-size: 20px;
            color: #448fb9;
            padding: 35px;
        }
        .headerleft
        {
            height: 100px;
            float: left;
            width: 1.2%;
            background: url(Images/leftheader.png) no-repeat;
            z-index: 2;
        }
        .headermiddle
        {
            height: 100px;
            float: left;
            width: 97%;
            background: url(Images/header.png) repeat-x;
        }
        .headerright
        {
            height: 100px;
            float: left;
            width: 1.8%;
            background: url(Images/rigthheader.png) no-repeat;
            z-index: 2;
        }
        .font
        {
            font-family: Calibri;
            font-size: 13px;
        }
        .button
        {
            width: 120px;
            height: 24px;
            background: #5486a6;
            cursor: pointer;
            color: #fff;
            padding: 3px;
            border: 1px Solid #5486a6;
            border-radius: 3px;
        }
        .button:hover
        {
            width: 120px;
            height: 24px;
            background: #175279;
            cursor: pointer;
            color: #fff;
            padding: 3px;
            border: 1px Solid #175279;
            border-radius: 3px;
        }
        .buttonNavigate
        {
            font-size: 15px;
            height: 25px;
            background: Silver;
            cursor: pointer;
            color: Black;
            font-weight: bold;
            cursor: pointer;
            border: 2px Solid #CCCCCC;
        }
        .buttonNavigate:hover
        {
            font-size: 15px;
            height: 25px;
            background: #CCCCCC;
            cursor: pointer;
            color: Black;
            font-weight: bold;
            cursor: pointer;
            border: 2px Solid Silver;
        }
        .buttoncommand
        {
            float: left;
            width: 50px;
            height: 20px;
            border: none;
            background: #448fb9;
            cursor: pointer;
            color: #fff;
            z-index: 22;
            border: 2px Solid #419cce;
        }
        #contact
        {
            /* the form*/
            position: relative; /* relative positioning */            
            padding-left: 1em;
            padding-top: 1em;
            padding-bottom: 2em;
            width:98%;
        }
        
        
        
        .abc
        {
            /* positions these elements 13em from left */
            position: absolute;
            left: 15em;
        }
        .def
        {
            /* positions these elements 13em from left */
            position: absolute;
            left: 23em;
        }
        #contact div
        {
            /*spacing*/
            margin-bottom: 1em;
        }
        
        .cc
        {
            font-family: "Trebuchet MS" , Verdana, Arial, Helvetica, sans-serif;
            font-size: larger;
            text-align:center;
        }
        
        .BarBorder
        {
            border-style: solid;
            border-width: 1px;
            padding: 2px 2px 2px 2px;
            width: 200px;
            height: 10px;
            vertical-align: middle;
        }
        
        .barInternal
        {
            background: Red;
        }
        
        .barInternalGreen
        {
            background: green;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function ClearText() {
            document.getElementById("tbRepid").value = "";
            document.getElementById("tbNpasswd").value = "";
            document.getElementById("tbCpasswd").value = "";
            document.getElementById("divresult").innerHTML = "";
        }

        function ResetPassword() {
            var usnm = document.getElementById("tbRepid").value;
            var npasswd = document.getElementById("tbNpasswd").value;
            var cpasswd = document.getElementById("tbCpasswd").value;

            if (usnm.trim() == "") {
                alert("Enter RepId");
                document.getElementById('tbRepid').focus();
                return false;
            }
            else if (npasswd == "") {
                alert("Enter New Password");
                document.getElementById('tbNpasswd').focus();
                return false;
            }
            else if (npasswd.trim() == "") {
                alert("Password cannot be Empty");
                document.getElementById("tbNpasswd").value = "";
                document.getElementById('tbNpasswd').focus();
                return false;
            }
            else if (cpasswd.trim() == "") {
                alert("Enter Confirm Password");
                document.getElementById('tbCpasswd').focus();
                return false;
            }
            
            if (npasswd === cpasswd) {
             //   alert("here :" + usnm + " " + npasswd);
                DeGrToolWebService.ResetPassword(usnm, npasswd, OutputMsg);
            }
            else {
                alert("New and Confirm Password does not Match");
                document.getElementById("tbNpasswd").value = "";
                document.getElementById("tbCpasswd").value = "";
                document.getElementById('tbNpasswd').focus();
            }

        }
        function OutputMsg(result) {

            var obj = JSON.parse(result);

            ClearText();

            if (obj.Status == "2") {
                document.getElementById("divresult").innerHTML = "<font color='red'>" + obj.Message + "</font>";
                document.getElementById('tbRepid').focus();
            }
            else {
                document.getElementById("divresult").innerHTML = "<font color='green'><b>" + obj.Message + "</b></font>";
            }
           

        }
   
    </script>
    <script src="ui/CookiesCreate.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function EraseCookieOnLogout() {
            eraseCookie("LoginUser");
            window.location.href = "Login.aspx";
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        <Services>
            <asp:ServiceReference Path="~/DeGrToolWebService.asmx" />
        </Services>
    </asp:ScriptManager>
    <div class="header">
        <div class="headerleft">
        </div>
        <div class="headermiddle">
            <div>
                <table class="clogo">
                    <tr>
                        <td>
                            <img src="Images/Banner.png" alt="" width="100%" height="62px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="DivHeaderMain" align="center" style="font-size: 12px; font-family: Arial;
                                margin: auto;">
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <%--   <div class="clogo">
            </div> class="headermidd"--%>
        </div>
        <div class="headerright">
        </div>
    </div>
    <br />
    <br />
    <div id="menu" runat="server">
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
          <div id="contact">
                <div class="cc">
                    <table style="width: 98%;">
                        <tr>
                            <td> 
                                <h1 style="color: #1f497d; font-family: Verdana; font-weight: bold; font-size: 20px;">
                                    Reset Password</h1>
                                <hr />                            
                            </td>
                        </tr>
                    </table>                    
                </div>
                <div>
                    <label for="RepId">
                        Rep Id</label>
                    <asp:TextBox ID="tbRepid" runat="server" CssClass="abc" autocomplete="off"></asp:TextBox>
                   
                </div>
                <div>
                    <label for="nPassword">
                        New Password</label>
                    <asp:TextBox ID="tbNpasswd" runat="server" CssClass="abc" TextMode="Password" autocomplete="off" ></asp:TextBox>
                </div>
                <div>
                    <label for="cPassword">
                        Confirm Password</label>
                    <asp:TextBox ID="tbCpasswd" runat="server" CssClass="abc" TextMode="Password"></asp:TextBox>
                </div>
                <div>
                    <input type="button" value="Reset Password" class="button" id="Button1" onclick="Javascript:return ResetPassword()" />
                    <input type="button" value="Clear" class="button" onclick="Javascript:return ClearText()" />
                </div>
                <div>
                    <div ID="divresult"></div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
