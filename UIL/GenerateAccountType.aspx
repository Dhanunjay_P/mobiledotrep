﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="GenerateAccountType.aspx.cs" Inherits="GenerateAccountType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderTitle" runat="Server">
    Account Type
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="ClientScripts/dist/css/bootstrap-multiselect.css" type="text/css">
    <script type="text/javascript" src="ClientScripts/dist/js/bootstrap-multiselect.js"></script>
    <style type="text/css">
        ul.multiselect-container.dropdown-menu
        {
            height: 500px;
            overflow-y: auto;
        }
    </style>
    <script language="javascript" type="text/javascript">

        var OriginalData = new Object();
        var NewUploadData = new Object();
        var OldUploadData = new Object();
        var oTableAccount = null;
        var validator;
        var NoOfRow = 0;
            
        $("legend").live('click', function () {
            $(this).siblings().slideToggle("slow");
        });

        function pageLoad() {

            //SessionData = JSON.parse(document.getElementById("ctl00_ContentPlaceHolder1_hidSession").value);

            window.prettyPrint() && prettyPrint();

            PrepareBasicObjects("AccountType", "GetInitialData", "M", "S", "M001", "M003N");

            AutoShowMessage = false;
            CallWebService(OnPageLoadSuccess);
        }

        function OnPageLoadSuccess() {

            if (ResultObject != null) {
                if (ResultObject.responseObjectInfo.Status == 1) {
                    BindDropDown(ResultObject.responseObjectInfo.dt_ReturnedTables[0], "ddlDatabase", "code", "desc", FirstIndexAS.Select);
                }
                else
                    $("#ddlDatabase").empty();
            }
        }

        function GetAccountList() {

            $("#DataListCatagory").hide();
            $("#DataListAttributeCatagory").hide();
            $("#RightsMultiselect").hide();

            $("#NewEntryAndModifyAccount").hide();
            $("#AccountListContent").hide();
            // $("#chkAccountTypeGroupId").empty();
            $("#chkAttributeOperationGroupId").empty();
            $("#ddlTerritoryNum").empty();

            PrepareBasicObjects("AccountType", "GetAccountDetails", "M", "S", "M001", "M003N");

            var database = $("#ddlDatabase").val();

            OpArgsObject["database"] = database;

            AutoShowMessage = false;
            CallWebService(OnGetAccountListSuccess);
        }

        function OnGetAccountListSuccess() {
            debugger;
            if (ResultObject != null) {

                if (ResultObject.responseObjectInfo.Status == 1) {

                    if (ResultObject.responseObjectInfo.dt_ReturnedTables[0] != null) {
                        DisplayAccountData(ResultObject.responseObjectInfo.dt_ReturnedTables[0]);
                        $("#AccountListContent").show();
                    }
                    else
                        $("#AccountListContent").hide();

                    if (ResultObject.responseObjectInfo.dt_ReturnedTables[2] != null) {
                        BindDropDown(ResultObject.responseObjectInfo.dt_ReturnedTables[2], "example-multiple-selected", "AccountTypeGroupId", "AccountTypeGroupId", FirstIndexAS.Select);
                        $('#example-multiple-selected').multiselect({
                            maxheight: 200,
                            includeSelectAllOption: true,
                            includeSelectAllIfMoreThan: 0,
                            selectAllText: ' Select all',
                            numberDisplayed: 8

                        });
                    }
                    else {
                        $("#example-multiple-selected").empty();
                    }

                    //                    if (ResultObject.responseObjectInfo.dt_ReturnedTables[2] != null)
                    //                        BindDropDown(ResultObject.responseObjectInfo.dt_ReturnedTables[3], "chkAccountTypeGroupId", "AccountTypeGroupId", "AccountTypeGroupDesc", FirstIndexAS.Select);
                    //                    else
                    //                        $("#chkAccountTypeGroupId").empty();

                    if (ResultObject.responseObjectInfo.dt_ReturnedTables[4] != null)
                        BindDropDown(ResultObject.responseObjectInfo.dt_ReturnedTables[4], "chkAttributeOperationGroupId", "AttributeOperationGroupId", "AttributeOperationGroupDesc", FirstIndexAS.Select);
                    else
                        $("#chkAttributeOperationGroupId").empty();
                }
                else {
                    $("#AccountListContent").hide();
                    // $("#chkAccountTypeGroupId").empty();
                    $("#chkAttributeOperationGroupId").empty();
                    $("#ddlTerritoryNum").empty();
                }
            }
        }

        function DisplayAccountData(data) {
            try {
                if (oTableAccount != null) {
                    oTableAccount.fnDestroy();
                    $("#AccountList").html(' <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped" id="AccountTbl"><thead></thead><tbody> </tbody><tfoot> </tfoot></table>');
                }
                oTableAccount = $("#AccountTbl").dataTable({
                    "iDisplayLength": 25,
                    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                    "aaData": data,
                    "aoColumns": [
                            {
                                "sTitle": '<div style="width:40px;text-align:center;margin-bottom:2px;"><i class="icon-plus-sign" title="Add Account" id="btnAddAccount" onclick="AddAccount()" data-mode="ADD"></i></div>',
                                "mData": null,
                                "sClass": "center",
                                "sWidth": "20px",
                                "sDefaultContent": '<i id="imgEditAccount" title="Edit Account" class="icon-pencil"></i>&nbsp;<i id="imgDeleteAccount" title="Delete Account" class="icon-trash"></i>',
                                "bSearchable": false,
                                "bSortable": false
                            },
                            { "sTitle": "AccountType", "mData": "AccountType", "sClass": "left", "sWidth": "80px" },
                            { "sTitle": "AccountTypeDescription", "mData": "AccountTypeDescription", "sClass": "left", "sWidth": "100px" },
                            { "sTitle": "TableName", "mData": "TableName", "sClass": "left", "sWidth": "80px" },
                            { "sTitle": "AccountIdColumn", "mData": "AccountIdColumn", "sClass": "left", "sWidth": "80px" },
                            { "sTitle": "AccountParentIdColumn", "mData": "AccountParentIdColumn", "sClass": "left", "sWidth": "80px" },
                            { "sTitle": "AccountNameColumn", "mData": "AccountNameColumn", "sClass": "left", "sWidth": "80px" },
                            { "sTitle": "AccountAddressColumn", "mData": "AccountAddressColumn", "sClass": "left", "sWidth": "80px" },
                            { "sTitle": "AccountCityStateZipColumn", "mData": "AccountCityStateZipColumn", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "LatitudeColumn", "mData": "LatitudeColumn", "sClass": "left", "sWidth": "50px" },
                            { "sTitle": "LongitudeColumn", "mData": "LongitudeColumn", "sClass": "left", "sWidth": "50px"}//,
                    //      { "sTitle": "RepTerritoryMappedList", "mData": "TerritoryNum", "sClass": "left", "sWidth": "200px" }
                          ]
                });

                //$('#AccountList').css('width', screen.width - 100);
                $('#AccountList').css('width', $(window).width() - 100);
            }
            catch (err) {
                alert(err.message);
            }
        }

        function ChangeAccountType() {

            $("#txtEmail").val($("#txtAccountType").val());
        }

        function AddAccount() {

            NoOfRow = 1;
            $("#txtNewEditAccount").val("New");
            ClearAccountData();
            $("#txtAccountType").prop("disabled", false);
            $("#txtEmail").prop("disabled", true);
            $("#NewEntryAndModifyAccount").show();
            $("#DataListCatagory").show();
            $("#DataListAttributeCatagory").show();
            $("#RightsMultiselect").show();

            $("#PasswordRow").show();
            $("#AccountList").hide();
        }

        //Click on Edit Image
        $('#AccountTbl tbody tr td i.icon-pencil').live('click', function (e) {
            debugger;
            var row = $(this).closest("tr").get(0);
            var aData = oTableAccount.fnGetData(row);

            //$.setFormData("Config_AccountTypeMst", aData);
            GetAccountDetails(aData["AccountType"]);
            return false;
        });

        function GetAccountDetails(AccountType) {

            NoOfRow = 1;
            $("#txtSrNo").val(NoOfRow);
            PrepareBasicObjects("AccountType", "GetAccountDetails", "M", "S", "M001", "M003N");

            var database = $("#ddlDatabase").val();

            OpArgsObject["database"] = database;
            OpArgsObject["AccountType"] = AccountType;

            AutoShowMessage = false;
            CallWebService(OnGetAccountDetailsSuccess);
        }

        function OnGetAccountDetailsSuccess() {

            if (ResultObject != null) {
                if (ResultObject.responseObjectInfo.Status == 1) {

                    $("#NewEntryAndModifyAccount").show();
                    $("#DataListCatagory").show();
                    $("#DataListAttributeCatagory").show();
                    $("#RightsMultiselect").show();

                    $("#AccountList").hide();
                    var aData = ResultObject.responseObjectInfo.dt_ReturnedTables[0][0];
                    $.setFormData("Config_AccountTypeMst", aData);

                    $("#txtNewEditAccount").val("Edit");
                    $("#txtAccountType").prop("disabled", true);
                    $("#txtEmail").prop("disabled", true);
                    $("#PasswordRow").hide();

                    if (ResultObject.responseObjectInfo.dt_ReturnedTables[1] != null) {

                        DisplayRepTerritoryMapData(ResultObject.responseObjectInfo.dt_ReturnedTables[1]);

                        NoOfRow = ResultObject.responseObjectInfo.dt_ReturnedTables[1].length;
                    }
                    NoOfRow++;
                    $("#txtSrNo").val(NoOfRow);
                }
                else
                    ClearAccountData();
            }
        }

        function DisplayRepTerritoryMapData(data) {
            debugger;
            try {

                $("#tbodyDtls").html("");

                OriginalData["Config_RegionTerritoryMap"] = data;

                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {

                        var CategoryCode = data[i]["CategoryCode"];
                        var CategoryDescription = data[i]["CategoryDescription"];
                        var CategorySeqNo = data[i]["CategorySeqNo"];
                        var IsActive = data[i]["IsActive"];
                        var IsActiveDesc = "Yes";
                        if (IsActive == "N")
                            IsActiveDesc = "No";
                        var FileName = data[i]["FileName"];
                        var ImageURL = data[i]["ImageURL"];
                        var AllowAdd = data[i]["AllowAdd"];
                        var AllowAddDesc = "No";
                        if (AllowAdd == "Y")
                            AllowAddDesc = "Yes";
                        var sr_no = i + 1;
                        var rowToReplace = "";

                        rowToReplace += "<tr id='" + sr_no + "'>";

                        rowToReplace += "<td><i id='imgEdit' title='Edit' class='icon-pencil' onclick='editRow(\"" + sr_no + "\")'></i> ";

                        rowToReplace += "<i id='imgDelete' title='Delete' class='icon-trash' onclick='deleteRow(\"" + sr_no + "\")'></i></td>";

                        rowToReplace += "<td abbr='" + CategoryCode + "'>" + CategoryCode + "</td>";
                        rowToReplace += "<td abbr='" + CategoryDescription + "'>" + CategoryDescription + "</td>";
                        rowToReplace += "<td abbr='" + CategorySeqNo + "'>" + CategorySeqNo + "</td>";
                        rowToReplace += "<td abbr='" + IsActive + "'>" + IsActiveDesc + "</td>";
                        rowToReplace += "<td abbr='" + FileName + "'>" + FileName + "</td>";
                        rowToReplace += "<td abbr='" + ImageURL + "'>" + ImageURL + "</td>";
                        rowToReplace += "<td abbr='" + AllowAdd + "'>" + AllowAddDesc + "</td>";

                        rowToReplace += "<td style='display:none'>" + sr_no + "</td>";

                        rowToReplace += "</tr>";

                        $("#example tbody").append(rowToReplace);
                    }
                }
            }
            catch (e) {
                alert("Exception : " + e.message);
            }
        }

        //Click on Delete Image
        $('#AccountTbl tbody tr td i.icon-trash').live('click', function (e) {
            debugger;
            var row = $(this).closest("tr").get(0);
            var aData = oTableAccount.fnGetData(row);
            //$.setFormData("Config_AccountTypeMst", aData);
            var answer = confirm("Are Sure You Want to Delete Account Type : " + aData["AccountType"] + " ?");
            if (answer) {
                GetAccountDetailForDelete(aData["AccountType"]);
            }
            return false;
        });

        function GetAccountDetailForDelete(AccountType) {
            debugger;
            NoOfRow = 1;
            $("#txtSrNo").val(NoOfRow);
            PrepareBasicObjects("AccountType", "GetAccountDetails", "M", "S", "M001", "M003N");

            var database = $("#ddlDatabase").val();

            OpArgsObject["database"] = database;
            OpArgsObject["AccountType"] = AccountType;

            AutoShowMessage = false;
            CallWebService(OnGetAccountDetailForDeleteSuccess);
        }

        function OnGetAccountDetailForDeleteSuccess() {
            debugger;
            if (ResultObject != null) {
                if (ResultObject.responseObjectInfo.Status == 1) {

                    PrepareBasicObjects("AccountType", "SaveAccountData", "M", "S", "M001", "M003N");

                    var listofrecords = new Array();
                    var form_header = ResultObject.responseObjectInfo.dt_ReturnedTables[0][0];
                    form_header["IsActive"] = "N";

                    listofrecords[0] = form_header;

                    NewUploadData["Config_AccountTypeMst"] = listofrecords;

                    var RepTerritoryMap = ResultObject.responseObjectInfo.dt_ReturnedTables[1];
                    //for (var i = 0; i < RepTerritoryMap.length; i++) {

                    //RepTerritoryMap[i]["IsActive"] = "N";
                    // }

                    NewUploadData["Config_AccountCategoryMap"] = RepTerritoryMap;

                    //OldUploadData = {};

                    UploadDataObject["New"] = NewUploadData;
                    UploadDataObject["Old"] = null;

                    $("#txtNewEditAccount").val("Edit");
                    var database = $("#ddlDatabase").val();
                    var AccountType = form_header["AccountType"]; //$("#txtAccountType").val();
                    var NewEdit = $("#txtNewEditAccount").val();

                    OpArgsObject["database"] = database;
                    OpArgsObject["AccountType"] = AccountType;
                    OpArgsObject["NewEdit"] = NewEdit;

                    AutoShowMessage = true;
                    CallWebService(OnSaveAccountDataSuccess);
                }
            }
        }

        function addrow() {
            debugger;
            try {

                if ($("#txtCategoryCode").val() == null || $("#txtCategoryCode").val() == "") {

                    alert("Please insert Category Code.");
                    $("#txtCategoryCode").focus();
                    return false;
                }
                else if ($("#txtCategoryDescription").val() == null || $("#txtCategoryDescription").val() == "") {

                    alert("Please insert Category Description.");
                    $("#txtCategoryDescription").focus();
                    return false;
                }
                else if ($("#txtCatAccountType").val() == null || $("#txtCatAccountType").val() == "") {

                    alert("Please insert Account Type.");
                    $("#txtCatAccountType").focus();
                    return false;
                }
                else if ($("#ddlIsActive").val() == null || $("#ddlIsActive").val() == "") {

                    alert("Please select Active/Deactive.");
                    $("#ddlIsActive").focus();
                    return false;
                }
                else if ($("#txtCatFileName").val() == null || $("#txtCatFileName").val() == "") {

                    alert("Please insert File Name.");
                    $("#txtCatFileName").focus();
                    return false;
                }
                else if ($("#txtCatImageURL").val() == null || $("#txtCatImageURL").val() == "") {

                    alert("Please insert Image URL.");
                    $("#txtCatImageURL").focus();
                    return false;
                }
                else if ($("#ddlAllowAdd").val() == null || $("#ddlAllowAdd").val() == "") {

                    alert("Please select TerritoryOwner (Yes/No).");
                    $("#ddlAllowAdd").focus();
                    return false;
                }
                else {

                    var CategoryCode = $("#txtCategoryCode").val();
                    var CategoryDescription = $("#txtCategoryDescription").val();
                    var AccountType = $("#txtCatAccountType").val();
                    var CategorySeqNo = $("#txtCategorySeqNo").val();
                    var IsActive = $("#ddlIsActive").val();
                    var IsActiveDesc = $("#ddlIsActive option:selected").text();
                    var FileName = $("#txtCatFileName").val();
                    var ImageURL = $("#txtCatImageURL").val();
                    var AllowAdd = $("#ddlAllowAdd").val();
                    var AllowAddDesc = $("#ddlAllowAdd option:selected").text();
                    var sr_no = $("#txtSrNo").val();
                    var rowToReplace = "";

                    rowToReplace += "<tr id='" + sr_no + "'>";

                    rowToReplace += "<td><i id='imgEdit' title='Edit' class='icon-pencil' onclick='editRow(\"" + sr_no + "\")'></i> ";

                    rowToReplace += "<i id='imgDelete' title='Delete' class='icon-trash' onclick='deleteRow(\"" + sr_no + "\")'></i></td>";

                    rowToReplace += "<td abbr='" + CategoryCode + "'>" + CategoryCode + "</td>";
                    rowToReplace += "<td abbr='" + CategoryDescription + "'>" + CategoryDescription + "</td>";
                    rowToReplace += "<td abbr='" + AccountType + "'>" + AccountType + "</td>";
                    rowToReplace += "<td abbr='" + CategorySeqNo + "'>" + CategorySeqNo + "</td>";
                    rowToReplace += "<td abbr='" + IsActive + "'>" + IsActiveDesc + "</td>";
                    rowToReplace += "<td abbr='" + FileName + "'>" + FileName + "</td>";
                    rowToReplace += "<td abbr='" + ImageURL + "'>" + ImageURL + "</td>";
                    rowToReplace += "<td abbr='" + AllowAdd + "'>" + AllowAddDesc + "</td>";

                    rowToReplace += "<td style='display:none'>" + sr_no + "</td>";

                    rowToReplace += "</tr>";

                    if ($("#btnAdd").data('mode') == "UPDATE") {

                        $("#" + sr_no).replaceWith(rowToReplace);

                        $("#btnAdd").data("mode", "ADD");
                    }
                    else {

                        if ($("#" + sr_no).length > 0) {
                            $("#" + sr_no).replaceWith(rowToReplace);
                        }
                        else {
                            $("#example tbody").append(rowToReplace);
                        }

                        NoOfRow++;
                    }

                    $("#txtSrNo").val(NoOfRow);
                    $("#ddlTerritoryNum").val("");
                    $("#ddlIsActive").val("");
                    $("#ddlTerritoryOwner").val("");
                }
            }
            catch (e) {

                alert("Exception : " + e.message);
                return false;
            }
        }

        //----- Edit Details Row ----- 
        function editRow(rowID) {
            try {
                debugger;
                $("#" + rowID + " td").each(function (i) {
                    if ($(this).attr("abbr") != undefined) {
                        if ($("#controlRow td:nth-child(" + (i + 1) + ")").children().length > 1) {
                            $("#controlRow td:nth-child(" + (i + 1) + ")").find("input[type='hidden']").val($(this).attr("abbr"));
                            $("#controlRow td:nth-child(" + (i + 1) + ")").find("input[type='text']").val($(this).html());
                            $("#controlRow td:nth-child(" + (i + 1) + ")").find("label").html($(this).html());
                        }
                        else {
                            $("#controlRow td:nth-child(" + (i + 1) + ")").children().val($(this).attr("abbr"));
                        }
                    }
                    else {
                        $("#controlRow td:nth-child(" + (i + 1) + ")").children().val($(this).html());
                        $("#controlRow td:nth-child(" + (i + 1) + ")").find("label").html($(this).html());
                    }
                });

                $("#btnAdd").data("mode", "UPDATE");

                return false;
            }
            catch (e) {
                alert("Exception : " + e.message);
                return false;
            }
        }

        //----- Delete Details Row ----- 
        function deleteRow(rowID) {

            try {

                var TerritoryNum = $("#" + rowID + " td")[1].firstChild.data;
                var answer = confirm("Are Sure You Want to Delete Row : " + TerritoryNum + " ?");
                if (answer) {

                    $("#" + rowID).remove();
                }
                return false;
            }
            catch (e) {
                alert("Exception : " + e.message);
            }
        }

        function PrepareUploadAccountDataObject() {
            debugger;
            var listofrecords = new Array();
            var form_header = new Object();

            form_header = $.getFormData("Config_AccountTypeMst");

            listofrecords[0] = form_header;

            NewUploadData["Config_AccountTypeMst"] = listofrecords;

            var table = document.getElementById("example");
            var rowCollection = table.rows;
            var dtlList = [];
            var sr_no = 1;

            for (var i = 2; i < rowCollection.length; i++) {

                var dtl = {};

                dtl["AccountType"] = form_header["AccountType"];

                if (rowCollection[i].cells[1].abbr != null)
                    dtl["CategoryCode"] = rowCollection[i].cells[1].abbr;
                else
                    dtl["CategoryCode"] = null;
                if (rowCollection[i].cells[2].abbr != "")
                    dtl["CategoryDescription"] = rowCollection[i].cells[2].abbr;
                else
                    dtl["CategoryDescription"] = null;
                if (rowCollection[i].cells[3].abbr != "")
                    dtl["AccountType"] = rowCollection[i].cells[3].abbr;
                else
                    dtl["AccountType"] = null;
                if (rowCollection[i].cells[4].abbr != "")
                    dtl["CategorySeqNo"] = rowCollection[i].cells[4].abbr;
                else
                    dtl["CategorySeqNo"] = null;
                if (rowCollection[i].cells[5].abbr != null)
                    dtl["IsActive"] = rowCollection[i].cells[5].abbr;
                else
                    dtl["IsActive"] = null;
                if (rowCollection[i].cells[3].abbr != "")
                    dtl["FileName"] = rowCollection[i].cells[6].abbr;
                else
                    dtl["FileName"] = null;
                if (rowCollection[i].cells[6].abbr != "")
                    dtl["ImageURL"] = rowCollection[i].cells[7].abbr;
                else
                    dtl["ImageURL"] = null;
                if (rowCollection[i].cells[7].abbr != "")
                    dtl["AllowAdd"] = rowCollection[i].cells[7].abbr;
                else
                    dtl["AllowAdd"] = null;
                dtl["sr_no"] = sr_no++;

                dtlList.push(dtl);
            }

            NewUploadData["Config_RepTerritoryMap"] = dtlList;

            OldUploadData = {};

            UploadDataObject["New"] = NewUploadData;
            UploadDataObject["Old"] = null;

            var database = $("#ddlDatabase").val();
            var AccountType = $("#txtAccountType").val();
            var NewEdit = $("#txtNewEditAccount").val();

            OpArgsObject["database"] = database;
            OpArgsObject["AccountType"] = AccountType;
            OpArgsObject["NewEdit"] = NewEdit;
        }

        function SaveAccountData() {

            if ($("#txtAccountType").val() == null || $("#txtAccountType").val() == "") {
                alert("Please enter Account Type.");
                $("#txtAccountType").focus();
                return false;
            }
            else if ($("#txtAccountTypeDescription").val() == null || $("#txtAccountTypeDescription").val() == "") {
                alert("Please enter Account Type Description.");
                $("#txtAccountTypeDescription").focus();
                return false;
            }
            //            else if ($("#txtDesignation").val() == null || $("#txtDesignation").val() == "") {
            //                alert("Please enter Designation.");
            //                $("#txtDesignation").focus();
            //                return false;
            //            }
            else if ($("#txtTableName").val() == null || $("#txtTableName").val() == "") {
                alert("Please enter TableName.");
                $("#txtTableName").focus();
                return false;
            }
            else if ($("#txtAccountIdColumn").val() == null || $("#txtAccountIdColumn").val() == "") {
                alert("Please enter AccountIdColumn.");
                $("#txtAccountIdColumn").focus();
                return false;
            }
            else if ($("#txtAccountParentIdColumn").val() == null || $("#txtAccountParentIdColumn").val() == "") {
                alert("Please enter AccountParentIdColumn.");
                $("#txtAccountParentIdColumn").focus();
                return false;
            }
            //            else if ($("#txtAccountTypeGroupId").val() == null || $("#txtAccountTypeGroupId").val() == "") {
            //                alert("Please enter AccountTypeGroupId.");
            //                $("#txtAccountTypeGroupId").focus();
            //                return false;
            //            }
            else if ($("#txtAccountTypeColumn").val() == null || $("#txtAccountTypeColumn").val() == "") {
                alert("Please enter AccountTypeColumn.");
                $("#txtAccountTypeColumn").focus();
                return false;
            }
            else if ($("#txtAccountAddressColumn").val() == null || $("#txtAccountAddressColumn").val() == "") {
                alert("Please enter AccountAddressColumn.");
                $("#txtAccountAddressColumn").focus();
                return false;
            }
            else if ($("#txtAccountCityStateZipColumn").val() == null || $("#txtAccountCityStateZipColumn").val() == "") {
                alert("Please enter AccountCityStateZipColumn.");
                $("#txtAccountCityStateZipColumn").focus();
                return false;
            }
            else if ($("#txtLatitudeColumn").val() == null || $("#txtLatitudeColumn").val() == "") {
                alert("Please enter LatitudeColumn.");
                $("#txtLatitudeColumn").focus();
                return false;

            }
            else if ($("#txtLongitudeColumn").val() == null || $("#txtLongitudeColumn").val() == "") {
                alert("Please enter LongitudeColumn.");
                $("#txtLongitudeColumn").focus();
                return false;

            }
            else if ($("#txtNoOfRecord").val() == null || $("#txtNoOfRecord").val() == "") {
                alert("Please enter NoOfRecord.");
                $("#txtNoOfRecord").focus();
                return false;

            }
            else if ($("#txtSeqNo").val() == null || $("#txtSeqNo").val() == "") {
                alert("Please enter SeqNo.");
                $("#txtSeqNo").focus();
                return false;

            }
            else if ($("#txtFileName").val() == null || $("#txtFileName").val() == "") {
                alert("Please enter FileName.");
                $("#txtFileName").focus();
                return false;

            }
            else if ($("#txtCallType").val() == null || $("#txtCallType").val() == "") {
                alert("Please enter CallType.");
                $("#txtCallType").focus();
                return false;

            }
            else if ($("#txtImageURL").val() == null || $("#txtImageURL").val() == "") {
                alert("Please enter ImageURL.");
                $("#txtImageURL").focus();
                return false;

            }
            else if ($("#txtAccountListDependOn").val() == null || $("#txtAccountListDependOn").val() == "") {
                alert("Please enter AccountListDependOn.");
                $("#txtAccountListDependOn").focus();
                return false;

            }
            else if ($("#txtAddressMstTableName").val() == null || $("#txtAddressMstTableName").val() == "") {
                alert("Please enter AddressMstTableName.");
                $("#txtAddressMstTableName").focus();
                return false;
            }
            else if ($("#txtPageFooterView").val() == null || $("#txtPageFooterView").val() == "") {
                alert("Please enter PageFooterView.");
                $("#txtPageFooterView").focus();
                return false;

            }
            else if ($("#txtAccountSelectionCriterion").val() == null || $("#txtAccountSelectionCriterion").val() == "") {
                alert("Please enter AccountSelectionCriterion.");
                $("#txtAccountSelectionCriterion").focus();
                return false;

            }
            else if ($("#txtPinColor").val() == null || $("#txtPinColor").val() == "") {
                alert("Please enter PinColor.");
                $("#txtPinColor").focus();
                return false;

            }
            else if ($("#txtPrimaryPinColor").val() == null || $("#txtPrimaryPinColor").val() == "") {
                alert("Please enter PrimaryPinColor.");
                $("#txtPrimaryPinColor").focus();
                return false;
            }


            //            var table = document.getElementById("example");
            //            if (table.rows.length <= 2) {
            //                alert("Please enter at least one Rep Territory Map row to Save Data.");
            //                return false;
            //            }

            var answer = confirm("Are Sure You Want To Save Data?");
            if (answer) {

                PrepareBasicObjects("AccountType", "SaveAccountData", "M", "S", "M001", "M003N");

                PrepareUploadAccountDataObject();

                AutoShowMessage = true;
                CallWebService(OnSaveAccountDataSuccess);
            }
        }

        function OnSaveAccountDataSuccess() {

            if (ResultObject != null) {
                if (ResultObject.responseObjectInfo.Status == 1) {

                    ClearAccountData();

                    $("#NewEntryAndModifyAccount").hide();
                    $("#DataListCatagory").hide();
                    $("#DataListAttributeCatagory").hide();
                    $("#RightsMultiselect").hide();

                    $("#AccountList").show();

                    if (ResultObject.responseObjectInfo.dt_ReturnedTables[0] != null)
                        DisplayAccountData(ResultObject.responseObjectInfo.dt_ReturnedTables[0]);
                }
            }
        }

        function BackAccountData() {

            $("#NewEntryAndModifyAccount").hide();
            $("#DataListCatagory").hide();
            $("#DataListAttributeCatagory").hide();
            $("#RightsMultiselect").hide();

            $("#AccountList").show();
        }

        function ClearAccountData() {

            if ($("#txtNewEditAccount").val() == "New") {

                $("#txtAccountType").val("");
                $("#txtEmail").val("");
                $("#txtPassword").val("");
                $("#txtConformPassword").val("");
            }

            $("#txtRepName").val("");
            $("#txtDesignation").val("");
            // $("#chkAccountTypeGroupId").val("");
            $("#chkAttributeOperationGroupId").val("");
            $("#chkSendDKPAlert").val("N");
            $("#chkIsActiveAccount").val("Y");
            $("#chkAccountStatusFlag").val("A");

            NoOfRow = 1;
            $("#txtSrNo").val(NoOfRow);
            $("#ddlTerritoryNum").val("");
            $("#ddlIsActive").val("");
            $("#ddlTerritoryOwner").val("");
            $("#tbodyDtls").html("");
        }                
    </script>
    <div class="row-fluid">
        <div class="span12">
            <fieldset>
                <legend>Database</legend>
                <div id="Database" style="display: block;">
                    <table cellpadding="4" cellspacing="1">
                        <tr>
                            <td>
                                <table id="tblDataBaseAccountType" cellpadding="2" cellspacing="0">
                                    <tr>
                                        <td align="right">
                                            <span class="star">*</span><b>Database :</b>
                                        </td>
                                        <td align="left">
                                            <select id="ddlDatabase" onchange="javascript:return GetAccountList();">
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="row-fluid">
        <div id="AccountListContent" class="span12" style="display: none;">
            <fieldset>
                <legend>Account Type</legend>
                <div>
                    <div id="AccountList" style="margin-top: 20px; overflow: auto;">
                        <table style="width: 100%" cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped"
                            id="AccountTbl">
                            <thead>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                    <div id="NewEntryAndModifyAccount" style="display: none;">
                        <table cellpadding="4" cellspacing="1">
                            <tr>
                                <td>
                                    <table id="tblAccountDetail" cellpadding="2" cellspacing="0">
                                        <tr style="display: none;">
                                            <td align="right">
                                                <b>New/Edit :</b>
                                            </td>
                                            <td align="left" colspan="3">
                                                <input type="text" id="txtNewEditAccount" data-field-name="NewEdit" maxlength="5"
                                                    disabled="disabled" class="Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>AccountType :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAccountType" data-field-name="AccountType" maxlength="100"
                                                    onchange="javascript:return ChangeAccountType();" class="Config_AccountTypeMst" />
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span><b>AccountTypeDescription :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAccountTypeDescription" data-field-name="AccountTypeDescription"
                                                    maxlength="100" class="Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>TableName :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtTableName" data-field-name="TableName" maxlength="100"
                                                    class="Config_AccountTypeMst" />
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span><b>AccountIdColumn :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAccountIdColumn" data-field-name="AccountIdColumn" style="text-transform: uppercase"
                                                    maxlength="15" class="Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr id="PasswordRow">
                                            <td align="right">
                                                <span class="star">*</span><b>AccountParentIdColumn :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAccountParentIdColumn" data-field-name="AccountParentIdColumn"
                                                    maxlength="30" class="Config_AccountTypeMst" />
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span><b>AccountNameColumn :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAccountNameColumn" data-field-name="AccountNameColumn"
                                                    maxlength="30" class="Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <%--<td align="right">
                                                <span class="star">*</span><b>AccountTypeGroupId :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAccountTypeGroupId" data-field-name="AccountTypeGroupId"
                                                    class="Config_AccountTypeMst" />
                                            </td>--%>
                                            <td align="right">
                                                <span class="star">*</span><b>AccountTypeColumn :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAccountTypeColumn" data-field-name="AccountTypeColumn"
                                                    class="Config_AccountTypeMst" />
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span><b>AccountAddressColumn :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAccountAddressColumn" data-field-name="AccountAddressColumn"
                                                    class="Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>AccountCityStateZipColumn :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAccountCityStateZipColumn" data-field-name="AccountCityStateZipColumn"
                                                    class="Config_AccountTypeMst" />
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span><b>LatitudeColumn :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtLatitudeColumn" data-field-name="LatitudeColumn" class="Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>LongitudeColumn :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtLongitudeColumn" data-field-name="LongitudeColumn" class="Config_AccountTypeMst" />
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span><b>NoOfRecord :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtNoOfRecord" data-field-name="NoOfRecord" class="Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <b>Active :</b>
                                            </td>
                                            <td align="left">
                                                <select id="chkIsActiveAccount" data-field-name="IsActive" class="Config_AccountTypeMst">
                                                    <option value="N">No</option>
                                                    <option value="Y" selected="true">Yes</option>
                                                </select>
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span><b>SeqNo :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtSeqNo" data-field-name="SeqNo" class="Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>FileName :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtFileName" data-field-name="FileName" maxlength="30" class="Config_AccountTypeMst" />
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span><b>CallType :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtCallType" data-field-name="CallType" maxlength="30" class="Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>ImageURL :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtImageURL" data-field-name="ImageURL" maxlength="30" class="Config_AccountTypeMst" />
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span><b>AccountListDependOn :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAccountListDependOn" data-field-name="AccountListDependOn"
                                                    maxlength="30" class="Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>AddressMstTableName :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAddressMstTableName" data-field-name="AddressMstTableName"
                                                    maxlength="30" class="Config_AccountTypeMst" />
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span><b>AddressMstIdColumn :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAddressMstIdColumn" data-field-name="AddressMstIdColumn"
                                                    maxlength="30" class="Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>PageFooterView :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtPageFooterView" data-field-name="PageFooterView" maxlength="30"
                                                    class="Config_AccountTypeMst" />
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span> <b>AccountSelectionCriterion :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtAccountSelectionCriterion" data-field-name="AccountSelectionCriterion"
                                                    maxlength="30" class="Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>PinColor :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtPinColor" data-field-name="PinColor" maxlength="30" class="Config_AccountTypeMst" />
                                            </td>
                                            <td align="right">
                                                <span class="star">*</span><b>DefaultAttributeForPinColor :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtDefaultAttributeForPinColor" data-field-name="DefaultAttributeForPinColor"
                                                    maxlength="30" class="Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <span class="star">*</span><b>PrimaryPinColor :</b>
                                            </td>
                                            <td align="left">
                                                <input type="text" id="txtPrimaryPinColor" data-field-name="PrimaryPinColor" maxlength="30"
                                                    class="Config_AccountTypeMst" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4">
                                                <input type="button" id="btnSaveCategory" value="Save" onclick="javascript:return SaveAccountData();"
                                                    style="width: 90px; font-weight: bold;" class="btn" />
                                                <input type="button" id="btnClearCategory" value="Clear" onclick="javascript:return ClearAccountData();"
                                                    style="width: 90px; font-weight: bold;" class="btn" />
                                                <input type="button" id="btnBackCategory" value="Back" onclick="javascript:return BackAccountData();"
                                                    style="width: 90px; font-weight: bold;" class="btn" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="DataListCatagory" style="margin-top: 20px; display: none">
                        <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped"
                            id="example">
                            <thead>
                                <tr class="header">
                                    <th style="text-align: left; width: 40px;">
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>CategoryCode</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>CategoryDescription</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>AccountType</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>CategorySeqNo</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Active</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>FileName</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>ImageURL</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>AllowAdd</b>
                                    </th>
                                    <th style="text-align: left; width: 40px; display: none">
                                        SrNo
                                    </th>
                                </tr>
                                <tr id="controlRow">
                                    <td>
                                        <i class="icon-plus-sign" title="Add" id="btnAdd" onclick="addrow()" data-mode="ADD">
                                        </i>
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtCategoryCode" data-field-name="CategoryCode"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtCategoryDescription" data-field-name="CategoryDescription"
                                            class="Config_RepTerritoryMap" />
                                        <%--<select id="ddlTerritoryNum" style="width: 150px;" data-field-name="TerritoryNum"
                                            class="Config_RepTerritoryMap">
                                        </select>--%>
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtCatAccountType" data-field-name="AccountType"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtCategorySeqNo" data-field-name="CategorySeqNo"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <select id="ddlIsActive" style="width: 85px;" data-field-name="IsActive" class="Config_RepTerritoryMap">
                                            <option value="Y" selected="true">Yes</option>
                                            <option value="N">No</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtCatFileName" data-field-name="FileName"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtCatImageURL" data-field-name="ImageURL"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <select id="ddlAllowAdd" style="width: 85px;" data-field-name="AllowAdd" class="Config_RepTerritoryMap">
                                            <option value="N" selected="true">No</option>
                                            <option value="Y">Yes</option>
                                        </select>
                                    </td>
                                    <td style="display: none;">
                                        <input type="text" id="txtSrNo" style="width: 40px; display: none" data-field-name="SrNo"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                </tr>
                            </thead>
                            <tbody id="tbodyDtls">
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                    <div id="DataListAttributeCatagory" style="margin-top: 20px; display: none">
                        <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped"
                            id="tblAttributeCatagory">
                            <thead>
                                <tr class="header">
                                    <th style="text-align: left; width: 40px;">
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Attribute Code</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Attribute Description</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Account Type</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Category Code</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Attribute SeqNo</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Show When Empty</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Show In Filter</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Active</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Table Name</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Id Column</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Distinct Id Column</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Column Name</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Is Editable</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Edit Type</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Edit Options</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Display Type</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Is Address Attribute</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>ImageLink</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>IsNumeric</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Is PhoneNumber</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Seperator</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Format</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>No Of Lines</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Color Value</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Conditional Attribute Req</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Conditional Attribute</b>
                                    </th>
                                    <th style="text-align: left; max-width: 85px;">
                                        <b>Show In Pin Color Options</b>
                                    </th>
                                    <th style="text-align: left; width: 40px; display: none">
                                        SrNo
                                    </th>
                                </tr>
                                <tr id="controlRowAttributeCatMap">
                                    <td>
                                        <i class="icon-plus-sign" title="Add" id="I1" onclick="addrow()" data-mode="ADD">
                                        </i>
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtAttributeCode" data-field-name="AttributeCode"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtAttributeDescription" data-field-name="AttributeDescription"
                                            class="Config_RepTerritoryMap" />
                                        <%--<select id="ddlTerritoryNum" style="width: 150px;" data-field-name="TerritoryNum"
                                            class="Config_RepTerritoryMap">
                                        </select>--%>
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtAccountType" data-field-name="AccountType"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtCategoryCode" data-field-name="CategoryCode"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtAttributeSeqNo" data-field-name="AttributeSeqNo"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtShowWhenEmpty" data-field-name="ShowWhenEmpty"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtShowInFilter" data-field-name="ShowInFilter"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <select id="Select1" style="width: 85px;" data-field-name="IsActive" class="Config_RepTerritoryMap">
                                            <option value="Y" selected="true">Yes</option>
                                            <option value="N">No</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtTableName" data-field-name="TableName"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtIdColumn" data-field-name="IdColumn"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtDistinctIdColumn" data-field-name="DistinctIdColumn"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtColumnName" data-field-name="ColumnName"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtIsEditable" data-field-name="IsEditable"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtEditType" data-field-name="EditType"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtEditOptions" data-field-name="EditOptions"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtDisplayType" data-field-name="DisplayType"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtIsAddressAttribute" data-field-name="IsAddressAttribute"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtImageLink" data-field-name="ImageLink"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtIsNumeric" data-field-name="IsNumeric"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtIsPhoneNumber" data-field-name="IsPhoneNumber"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtSeperator" data-field-name="Seperator"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtFormat" data-field-name="Format" class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtNoOfLines" data-field-name="NoOfLines"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtColorValue" data-field-name="ColorValue"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtConditionalAttributeReq" data-field-name="ConditionalAttributeReq"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <input type="text" style="width: 85px;" id="txtConditionalAttribute" data-field-name="ConditionalAttribute"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                    <td>
                                        <select id="ddlShowInPinColorOptions" style="width: 85px;" data-field-name="ShowInPinColorOptions"
                                            class="Config_RepTerritoryMap">
                                            <option value="N" selected="true">No</option>
                                            <option value="Y">Yes</option>
                                        </select>
                                    </td>
                                    <td style="display: none;">
                                        <input type="text" id="Text7" style="width: 40px; display: none" data-field-name="SrNo"
                                            class="Config_RepTerritoryMap" />
                                    </td>
                                </tr>
                            </thead>
                            <tbody id="tbody1">
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                    <div id="RightsMultiselect" style="margin-top: 20px; display: none;">
                        <!-- Note the missing multiple attribute! -->
                        <label id="lblTitleRights">
                            Rights to Account Type
                        </label>
                        <select id="example-multiple-selected" multiple="multiple">
                        </select>
                        <%-- <select id="example-multiple-selected" multiple="multiple">
                        </select>--%>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</asp:Content>
