﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintRosterData.aspx.cs" Inherits="PrintRosterData" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     
      <script src="css/jquery-1.12.3.js"></script>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <%--<link href="Roster/CSS/bootstrap.css" rel="stylesheet"  type='text/css' />--%>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.12.0/semantic.min.css" />
     <link href="Roster/CSS/style.css" rel="stylesheet" />
    <link href="Roster/CSS/font-awesome.min.css" rel="stylesheet" />
    <link href="ClientScripts/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="//cdn.rawgit.com/MrRio/jsPDF/master/dist/jspdf.min.js">
    </script>
    <script type="text/javascript" src="//cdn.rawgit.com/niklasvh/html2canvas/0.5.0-alpha2/dist/html2canvas.min.js">
    </script>
    
     <script type="text/javascript">
         var today = new Date();
         var dd = today.getDate();
         var mm = today.getMonth() + 1; //January is 0!
         var visibleColumnsOrder = new Array();
         var yyyy = today.getFullYear();
         if (dd < 10) {
             dd = '0' + dd;
         }
         if (mm < 10) {
             mm = '0' + mm;
         }
         today = mm + '/' + dd + '/' + yyyy;

         $(document).ready(function () {
        

             ///  var username = getParameterByName('username');
             var saleforce = getParameterByName('saleforce');
             var territory = getParameterByName('Terriotory');
             var Employee = getParameterByName('employee');
             var username = getParameterByName('username');
             DeGrToolWebService.GetRosterEmployeeAllData(username, Employee, territory, saleforce, SetPopupData);

         });

         function clearPopup() {

             $("#p_salesforceID").html('');
             $("#p_terr").html('');
             $("#p_terr_type").html('');
             $("#p_role").html('');
             $("#p_parent_terr").html('');
             $("#p_terr_manager_id").html('');
             $("#p_terr_manager_name").html('');
             $("#p_terr_status").html('');
             $("#p_terr_start_date").html('');
             $("#p_terr_end_date").html('');
             $("#p_terr_default_zip").html('');
             $("#p_emp_id").html('');
             $("#p_emp_department").html('');
             $("#p_user_id").html('');
             $("#p_emp_title").html('');
             $("#p_emp_name").html('');
             $("#p_preferredName").html('');
             $("#p_emp_hire_date").html('');
             $("#p_emp_field_start_date").html('');
             $("#p_emp_termination_date").html('');
             $("#p_emp_position_start_date").html('');
             //$("#p_emp_position_end_date").html('');
             $("#p_emp_terr_status").html('');
             $("#p_emp_type").html('');
             $("#p_emp_staus").html('');
             $("#emp_home_address").html('');
             $("#p_mailing_address").html('');
             $("#p_company_emial").html('');
             $("#p_shipping_to_address").html('');
             $("#p_personal_email").html('');
             $("#p_emp_buss_phone").html('');
             $("#p_emp_mobie_no").html('');
             $("#p_emp_home_no").html('');
             return false;
         }

         function SetPopupData(data) {
             clearPopup()
             if (data != null && data != undefined) {
                 var Result = JSON.parse(data)
                 if (Result.Status == 1) {
                     FillPopupData(Result["dt_ReturnedTables"]);

                      
                     $("#loading").hide();

                 }
                 else {
                     CustomAlert(Result.Message);
                 }
             }
             else {
             }
             return false;
         }

         function FillPopupData(data) {
             if (data[0] != null)
                 FillEmployeeSection(data[0][0]);

             if (data[1] != null)
                 FillTerritorySection(data[1][0]);

             if (data[2] != null)
                 FillSalesforceSection(data[2][0]);

             if (data[3] != null) {
                 FillRosterLastEditSection(data[3]);
             }

             if (data[4] != null) {
                 FillRosterTerritoryEmployeeRelations(data[4]);

             }

             return false;
         }
         function FillEmployeeSection(data) {
             if (data["Employee_ID"] != null) {
                 $("#p_emp_id").html(data["Employee_ID"]);
                 $("#current_emp_id").val(data["Employee_ID"]);
                 $("#hidEmployee").val(data["Employee_ID"]);
             }
             else {
                 $("#current_emp_id").val('');
                 $("#hidEmployee").val('');
             }
             if (data["Department"] != null) $("#p_emp_department").html(data["Department"]);
             if (data["User_ID"] != null) $("#p_user_id").html(data["User_ID"]);
             if (data["Employee_title_desc"]) $("#p_emp_title").html(data["Employee_title_desc"]);
             var fullName = '';
             if (data["Last_Name"] != '') fullName += data["Last_Name"] + ', ';
             if (data["First_Name"] != '') fullName += data["First_Name"] + ' ';
             if (data["Middle_Name"] != '') fullName += data["Middle_Name"];
             if (data["Suffix"] != '') fullName += ', ' + data["Suffix"];
             if (data["First_Name"] != null) $("#p_emp_name").html(fullName);
             if (fullName != null && fullName != "")
             { $("#TerritoryManager").html(fullName); }
             else {
                 $("#TerritoryManager").html("VACANT");
             }
             if (data["Preferred_Name"] != null) {
                 var fullName = '';
                 if (data["Last_Name"] != '') fullName += data["Last_Name"] + ', ';
                 if (data["Preferred_Name"] != '') fullName += data["Preferred_Name"] + ' ';
                 else {
                     if (data["First_Name"] != '') fullName += data["First_Name"] + ' ';
                 }
                 if (data["Middle_Name"] != '') fullName += data["Middle_Name"];
                 if (data["Suffix"] != '') fullName += ', ' + data["Suffix"];
             }
             if (data["Preferred_Name"] != null) $("#p_preferredName").html(data["Preferred_Name"]);
             if (data["hire_date"] != null) $("#p_emp_hire_date").html(data["hire_date"]);
             if (data["field_start_date"] != null) $("#p_emp_field_start_date").html(data["field_start_date"]);
             if (data["termination_date"] != null) $("#p_emp_termination_date").html(data["termination_date"]);
             if (data["emp_terr_status"] != null) $("#p_emp_terr_status").html(data["emp_terr_status"]);
             if (data["Employee_type_desc"] != null) $("#p_emp_type").html(data["Employee_type_desc"]);
             if (data["Employee_Status"] != null) $("#p_emp_staus").html(data["Employee_Status"]);
             var homeaddrstr = '';
             if (data["Home_Address1"] != null) {
                 homeaddrstr += data["Home_Address1"];
                 if (data["Home_Address2"] != null && data["Home_Address2"] != "") homeaddrstr += ', ' + data["Home_Address2"];
                 if (data["Home_City"] != null && data["Home_City"] != "") homeaddrstr += ', ' + data["Home_City"];
                 if (data["Home_State"] != null && data["Home_State"] != "") homeaddrstr += ', ' + data["Home_State"];
                 if (data["Home_Zip"] != null && data["Home_Zip"] != "") homeaddrstr += ' ' + data["Home_Zip"];
                 $("#emp_home_address").html(homeaddrstr);
             }
             var mobileAddr = '';
             if (data["M_Address1"] != null) {
                 mobileAddr += data["M_Address1"];
                 if (data["M_Address2"] != null && data["M_Address2"] != "") mobileAddr += ', ' + data["M_Address2"];
                 if (data["M_City"] != null && data["M_City"] != "") mobileAddr += ', ' + data["M_City"];
                 if (data["M_State"] != null && data["M_State"] != "") mobileAddr += ', ' + data["M_State"];
                 if (data["M_Zip"] != null && data["M_Zip"] != "") mobileAddr += ' ' + data["M_Zip"];
                 $("#p_mailing_address").html(mobileAddr);
             }
             var ShipingAddr = '';
             if (data["Ship_to_Address"] != null) {
                 ShipingAddr += data["Ship_to_Address"];
                 if (data["Ship_to_Address2"] != null && data["Ship_to_Address2"] != "") ShipingAddr += ', ' + data["Ship_to_Address2"];
                 if (data["Ship_to_city"] != null && data["Ship_to_city"] != "") ShipingAddr += ', ' + data["Ship_to_city"];
                 if (data["Ship_to_State"] != null && data["Ship_to_State"] != "") ShipingAddr += ', ' + data["Ship_to_State"];
                 if (data["Ship_to_Zip"] != null && data["Ship_to_Zip"] != "") ShipingAddr += ' ' + data["Ship_to_Zip"];
                 $("#p_shipping_to_address").html(ShipingAddr);
             }
             if (data["Company_Email"] != null) $("#p_company_emial").html(data["Company_Email"]);
             if (data["Personal_Email"] != null) $("#p_personal_email").html(data["Personal_Email"]);
             if (data["Office_Phone"] != null) $("#p_emp_buss_phone").html(data["Office_Phone"]);
             if (data["Mobile_Phone"] != null) $("#p_emp_mobie_no").html(data["Mobile_Phone"]);
             if (data["Home_Phone"] != null) $("#p_emp_home_no").html(data["Home_Phone"]);
         }

         function FillTerritorySection(data) {
             if (data["Territory_ID"] != null) {
                 $("#p_terr").html(data["Territory_ID"] + " - " + data["Territory_Name"]);
                 $("#p_terr").data('value', data["Territory_ID"])
                 $("#current_emp_terr").val(data["Territory_ID"]);
                 $("#hidTerritory").val(data["Territory_ID"]);

             }
             else {
                 $("#p_terr").data('value', data["Territory_ID"])
                 $("#current_emp_terr").val('');
                 $("#hidTerritory").val('');
             }
             if (data["Territory_Type"] != null) $("#p_terr_type").html(data["Territory_Type_Name"]);
             if (data["Role_Name"] != null) $("#p_role").html(data["role_code"] + ' - ' + data["Role_Name"]);
             if (data["Parent_Terr_ID"] != null && data["terr_name"] != null) $("#p_parent_terr").html(data["Parent_Terr_ID"] + ' - ' + data["terr_name"]);
             if (data["manager_id"] != null) $("#p_terr_manager_id").html(data["manager_id"]);
             var fullName = '';

             if (data["manager_last_name"] != '' && data["manager_last_name"] != null) fullName += data["manager_last_name"] + ', ';
             if (data["manager_first_name"] != '' && data["manager_first_name"] != null) fullName += data["manager_first_name"] + ' ';
             if (data["manager_middle_name"] != '' && data["manager_middle_name"] != null) fullName += data["manager_middle_name"];
             if (data["manager_suffix"] != '' && data["manager_suffix"] != null) fullName += ', ' + data["manager_suffix"];
             if (fullName != null && fullName != "") { $("#p_terr_manager_name").html(fullName); }
             else { $("#p_terr_manager_name").html("VACANT"); }
             if (data["Territory_End_date_New"] != "" && data["Territory_End_date_New"] != null) {
                 var SalesforceEndDateTime = new Date(data["Territory_End_date_New"]);
                 var todayDateTime = new Date(today);
                 if (todayDateTime <= SalesforceEndDateTime) {
                     $("#p_terr_status").html('Active');
                     $("#Status").html('Active');
                 }
                 else {
                     $("#p_terr_status").html('Inactive');
                     $("#Status").html('Inactive');
                 }
             } else {
                 $("#p_terr_status").html('Active');
                 $("#Status").html('Active');
             }
             if (data["Territory_Start_date"] != null) $("#p_terr_start_date").html(data["Territory_Start_date_New"]);
             if (data["Territory_End_date"] != null) $("#p_terr_end_date").html(data["Territory_End_date_New"]);
             if (data["Default_Terr_ZIP"] != null) $("#p_terr_default_zip").html(data["Default_Terr_ZIP"]);
             if (data["position_start_date"] != null) $("#p_emp_position_start_date").html(data["position_start_date_New"]);
             $("#modal_title").html('Geography Profile:  ' + data["Territory_ID"] + ' - ' + data["Territory_Name"]);
             if (data["Territory_ID"] != null) $("#TerritoryId").html(" " + data["Territory_ID"] + ' - ' + data["Territory_Name"]);
             //if (data["Territory_Status"] != null) $("#Status").html(" " + data["Territory_Status"]);
         }

         function FillSalesforceSection(data) {
             if (data["Salesforce_ID"] != null) {
                 $("#p_salesforceID").html(data["Salesforce_ID"] + " - " + data["Salesforce_Name"]);
                 $("#p_salesforceID").data('value', data["Salesforce_ID"]);
                 $("#current_emp_salesforce").val(data["Salesforce_ID"]);
                 $("#hidSalesforce").val(data["Salesforce_ID"]);

             }
             else {
                 $("#p_salesforceID").data('value', '');
                 $("#current_emp_salesforce").val('');
                 $("#hidSalesforce").val('');
             }
             if (data["Salesforce_Start_Date"] != null) $("#p_salesforce_startdate").html(data["Salesforce_Start_Date"]);
             if (data["Salesforce_End_Date"] != null) $("#p_salesforce_enddate").html(data["Salesforce_End_Date"]);
             if (data["Salesforce_Status"] != null) $("#p_salesforce_status").html(data["Salesforce_Status"]);
         }

         function FillRosterTerritoryEmployeeRelations(data) {
             var str = '';
             var startdate = "";
             var endDate = "";

             for (var i = 0 ; i < data.length; i++) {
                 if (data[i]['Position_Start_Date'] == null) { startdate = ""; }
                 else {
                     startdate = data[i]['Position_Start_Date']
                 }
                 if (data[i]['Position_End_Date'] == null) { endDate = ""; }
                 else {
                     endDate = data[i]['Position_End_Date']
                 }

                 var fullName = '';
                 if (data[i]["Last_Name"] != '') fullName += data[i]["Last_Name"] + ', ';
                 if (data[i]["First_Name"] != '') fullName += data[i]["First_Name"] + ' ';
                 if (data[i]["Middle_Name"] != '') fullName += data[i]["Middle_Name"];
                 if (data[i]["Suffix"] != '') fullName += ', ' + data[i]["Suffix"] + '';
                 str += '  <tr> <td>' + data[i]['Salesforce_ID'] + ' - ' + data[i]['Salesforce_Name'] + '</td> <td> ' + fullName + '</td> <td> ' + data[i]['Role_Code'] + ' </td> <td> ' + startdate + '</td> <td> ' + endDate + '</td> <td> ' + data[i]['Status_Desc'] + '</td>'
                 //+' <td style="float: right;"> ';
                 //str += '<div style="cursor: pointer;color:#1e88e5;font-weight:normal" data-emppostitonenddate="' + endDate + '" data-empname="' + fullName + '" data-terrname="' + data[i]['Territory_ID'] + ' - ' + data[i]['Territory_Name'] + '"  onClick="ClearEndDatePopup(this,\'' + data[i]['Territory_ID'] + '\',\'' + data[i]['Salesforce_ID'] + '\',\'' + data[i]['sr_no'] + '\')">Edit</div>'
                     + '</tr>';



             }
             $("#TerritoryEmployeeRelationBody").html(str);

         }

         function FillRosterLastEditSection(data) {
             $("#RecentChangeData").html('');
             var newRecet = "";
             for (var i = 0; i < data.length; i++) {
                 if (data[i]["Edited_table"] == "Fieldforce Data") {
                     var resultObject = FillSaleforceHistory2(data[i]);
                 }
                 else if (data[i]["Edited_table"] == "Geography Data") {
                     var resultObject = FillTerritoryHistory2(data[i]);
                 }
                 else if (data[i]["Edited_table"] == "Employee Data") {
                     sessionStorage["EditSectionName"] = data[i]["Edited_Section"];
                     if (data[i]["Edited_Section"] == "Shipping Address") {
                         var resultObject = FillEmployeeAdddresHistory2(data[i]);
                     }
                     else if (data[i]["Edited_Section"] == "Mailing Address") {
                         var resultObject = FillEmployeeAdddresHistory2(data[i]);
                     }
                     else if (data[i]["Edited_Section"] == "Sample Shipment Locker") {
                         var resultObject = FillEmployeeAdddresHistory2(data[i]);
                     }
                     else {
                         var resultObject = FillEmployeeHistory2(data[i]);
                     }
                 }
                 if (resultObject.NewValue.length > 0) {
                     for (var j = 0; j < resultObject.NewValue.length; j++) {
                         newRecet += '   <div > ' +
              '<div style="display: inline-table; width: 12%"> ' +
               data[i]["LastEditedDate"] + '</div> ' +
             '<div style="display: inline-table; width: 15%"> '
            + data[i]["Edited_table"] +

            '</div> ' +

                         '<div style="display: inline-table; width: 20%"> ' +
                          resultObject.NewValue[j]["name"] + '</div> ' +

                         '<div style="display: inline-table; width: 20%"> ' +
                          resultObject.OldValue[j]["value"] + '</div> ' +

                         '<div style="display: inline-table; width: 20%"> ' +
                          resultObject.NewValue[j]["value"] + '</div> ' +
                         '<div style="display:inline-table"> ' +
                      '' + data[i]["Edited_By"] + ' </div>  <div class="lastcol" style="float: right;"> ' +
         '</div>';
                     }
                 }
             }
             
             $("#RecentChangeData").append(newRecet);
         }

         function FillSaleforceHistory2(data) {
             var ReturnObject = new Object();
             var OldArray = new Array();
             var NewArray = new Array();
             var history = data;
             var str = "";
             for (var key in history) {
                 var flagfordis = false;
                 //if (history[key] != null) {
                 if (key.indexOf("_Old") != -1) {
                     var newkey = key.replace('_Old', '');

                     if ((history[newkey] != null)) {
                         flagfordis = true;
                     }
                     //else if ((history[newkey] == null && history[key] != null)) {
                     //    flagfordis = true;
                     //}
                     if (key == "Fieldforce_End__Date_Old") {
                         if ((history[key] != null) && history[newkey] == null) {
                             flagfordis = true;

                         }
                     }
                     if ((history[newkey] != null)) {
                         if (flagfordis)
                             if ((history[newkey].indexOf("T00") == -1 && history[newkey].indexOf("T12") == -1)) {
                                 flagfordis = true;
                             }
                             else {
                                 flagfordis = false;
                             }
                     }

                     if (flagfordis) {
                         //if ((history[newkey].indexOf("T00") == -1 && history[newkey].indexOf("T12") == -1)) {

                         var str2 = newkey.replace(/_/g, ' ');
                         if (history[newkey] == null) { history[newkey] = '' }
                         if (history[key] == null) { history[key] = '' }

                         var tempObj = new Object();
                         tempObj['name'] = str2;
                         tempObj['value'] = history[key];
                         OldArray.push(tempObj);
                         tempObj = new Object();
                         tempObj['name'] = str2;
                         tempObj['value'] = history[newkey];
                         NewArray.push(tempObj);
                         str += '<div class="">' +
                           '<div style="display: inline-table; width: 30%">' + str2 +
                          ' </div>' +
                          ' <div style="display: inline-table; width: 34%">' + history[key] +

                           '</div>' +
                           '<div style="display: inline-table; width: 34%">' + history[newkey]
                           +
                           '</div>' +
                       '</div>';
                         //}
                     }
                     //}


                     //}
                 }
             }
             ReturnObject['NewValue'] = NewArray;
             ReturnObject['OldValue'] = OldArray;

             return ReturnObject;
         }

         function FillTerritoryHistory2(data) {
             var ReturnObject = new Object();
             var OldArray = new Array();
             var NewArray = new Array();
             var history = data;
             var str = "";
             for (var key in history) {
                 //if (history[key] != null) {
                 var flagfordis = false;
                 if (key.indexOf("_New") != -1) {
                     var newkey = key.replace('_New', '');
                     //if (history[newkey] == history[key]) {
                     //if (history[key] == null && history[newkey] != null) { }

                     if ((history[key] != null)) {
                         flagfordis = true;
                     }

                     if (key == "Geography_End__Date_New") {
                         if ((history[key] == null) && history[newkey] != null) {
                             flagfordis = true;

                         }
                     }


                     if (key == "Geography_Start__Date_New") {
                         if ((history[key] == null) && history[newkey] != null) {
                             flagfordis = true;

                         }
                     }
                     if ((history[key] != null)) {
                         if (flagfordis)
                             if ((history[key].indexOf("T00") == -1 && history[key].indexOf("T12") == -1)) {
                                 flagfordis = true;
                             }
                             else {
                                 flagfordis = false;
                             }
                     }

                     if (flagfordis) {
                         //if ((history[key].indexOf("T00") == -1 && history[key].indexOf("T12") == -1)) {

                         var str2 = newkey.replace(/_/g, ' ');
                         if (history[newkey] == null) { history[newkey] = '' }
                         if (history[key] == null) { history[key] = '' }
                         var tempObj = new Object();
                         tempObj['name'] = str2;
                         tempObj['value'] = history[newkey];
                         OldArray.push(tempObj);
                         tempObj = new Object();
                         tempObj['name'] = str2;
                         tempObj['value'] = history[key];
                         NewArray.push(tempObj);
                         str += '<div class="">' +
                           '<div style="display: inline-table; width: 30%">' + str2 +
                          ' </div>' +
                          ' <div style="display: inline-table; width: 34%">' + history[newkey] +

                           '</div>' +
                           '<div style="display: inline-table; width: 34%">' + history[key]
                           +
                           '</div>' +
                       '</div>';
                         //}
                     }


                 }
                 //}
             }

             ReturnObject['NewValue'] = NewArray;
             ReturnObject['OldValue'] = OldArray;

             return ReturnObject;

         }
         function FillEmployeeHistory2(data) {

             var history = data;
             var str = "";
             var ReturnObject = new Object();
             var OldArray = new Array();
             var NewArray = new Array();
             for (var key in history) {
                 //if (history[key] != null) {
                 var flagfordis = false;
                 if (key.indexOf("_New") != -1) {
                     var newkey = key.replace('_New', '');
                     //if (history[newkey] == history[key]) {
                     //if (history[key] == null && history[newkey] != null) { }

                     if ((history[key] != null)) {
                         flagfordis = true;
                     }
                     //else if ((history[key] == null && history[newkey] != null)) {
                     //    flagfordis = true;
                     //}
                     if (key == "Employee_Title_Desc_New") {
                         flagfordis = false;
                     }
                     if (key == "Employee_Type_Desc_New") {
                         flagfordis = false;
                     }
                     if (key == "Office_Phone_New") {
                         key == "Bussiness_Phone_New"
                     }

                     if (sessionStorage["EditSectionName"] == "Personal Details") {
                         if (key == "Hire__Date_New") {
                             if ((history[key] == null) && history[newkey] != null) {
                                 flagfordis = true;

                             }
                         }


                         if (key == "Termination__Date_New") {
                             if ((history[key] == null) && history[newkey] != null) {
                                 flagfordis = true;

                             }
                         }
                         if (key == "Field_Start__Date_New") {
                             if ((history[key] == null) && history[newkey] != null) {
                                 flagfordis = true;

                             }
                         }
                     }
                     if ((history[key] != null)) {
                         if (flagfordis)
                             if ((history[key].indexOf("T00") == -1 && history[key].indexOf("T12") == -1)) {
                                 flagfordis = true;
                             }
                             else {
                                 flagfordis = false;
                             }
                     }

                     if (flagfordis) {
                         //if ((history[key].indexOf("T00") == -1 && history[key].indexOf("T12") == -1)) {
                         var str2 = newkey.replace(/_/g, ' ');
                         if (key == "Employee_Title_New") { key = "Employee_Title_Desc_New"; }
                         if (key == "Employee_Type_New") { key = "Employee_Type_Desc_New"; }
                         if (key == "Office_Phone_New") { str2 = "Business Phone"; }
                         var newkey = key.replace('_New', '');
                         if (history[newkey] == null) { history[newkey] = '' }
                         if (history[key] == null) { history[key] = '' }

                         var tempObj = new Object();
                         tempObj['name'] = str2;
                         tempObj['value'] = history[newkey];
                         OldArray.push(tempObj);
                         tempObj = new Object();
                         tempObj['name'] = str2;
                         tempObj['value'] = history[key];
                         NewArray.push(tempObj);
                         str += '<div class="">' +
                           '<div style="display: inline-table; width: 30%">' + str2 +
                          ' </div>' +
                          ' <div style="display: inline-table; width: 34%">' + history[newkey] +

                           '</div>' +
                           '<div style="display: inline-table; width: 34%">' + history[key]
                           +
                           '</div>' +
                       '</div>';
                         //}
                         //}

                     }
                 }
                 //}
             }
             ReturnObject['NewValue'] = NewArray;
             ReturnObject['OldValue'] = OldArray;

             return ReturnObject;
         }
         function FillEmployeeAdddresHistory2(data) {
             var ReturnObject = new Object();
             var OldArray = new Array();
             var NewArray = new Array();
             var OldAddress = "";
             var NewAddress = "";
             var history = data;
             var str = "";


             for (var key in history) {
                 //if (history[key] != null) {

                 //if (history[key].indexOf("T00") == -1) {
                 //if (key.indexOf("_New") != -1) {
                 if (sessionStorage["EditSectionName"] == "Sample Shipment Locker") {
                     if (key == "Ship_to_Address") {
                         OldAddress = "";
                         if (history["Ship_to_Address"] != null) {
                             OldAddress += history["Ship_to_Address"];
                             if (history["Ship_to_Address2"] != null && history["Ship_to_Address2"] != "") OldAddress += ', ' + history["Ship_to_Address2"];
                             if (history["Ship_to_city"] != null && history["Ship_to_city"] != "") OldAddress += ', ' + history["Ship_to_city"];
                             if (history["Ship_to_State"] != null && history["Ship_to_State"] != "") OldAddress += ', ' + history["Ship_to_State"];
                             if (history["Ship_to_Zip"] != null && history["Ship_to_Zip"] != "") OldAddress += ' ' + history["Ship_to_Zip"];
                         }
                     }
                     if (key == "Ship_to_Address_New") {
                         NewAddress = "";
                         if (history["Ship_to_Address_New"] != null) {
                             NewAddress += history["Ship_to_Address_New"];
                             if (history["Ship_to_Address2_New"] != null && history["Ship_to_Address2_New"] != "") NewAddress += ', ' + history["Ship_to_Address2_New"];
                             if (history["Ship_to_city_New"] != null && history["Ship_to_city_New"] != "") NewAddress += ', ' + history["Ship_to_city_New"];
                             if (history["Ship_to_State_New"] != null && history["Ship_to_State_New"] != "") NewAddress += ', ' + history["Ship_to_State_New"];
                             if (history["Ship_to_Zip_New"] != null && history["Ship_to_Zip_New"] != "") NewAddress += ' ' + history["Ship_to_Zip_New"];

                         }
                     }
                 }
                 else if (sessionStorage["EditSectionName"] == "Mailing Address") {
                     if (key == "Home_Address1_New") {
                         NewAddress = "";
                         if (history["Home_Address1_New"] != null) {
                             NewAddress += history["Home_Address1_New"];
                             if (history["Home_Address2_New"] != null && history["Home_Address2_New"] != "") NewAddress += ', ' + history["Home_Address2_New"];
                             if (history["Home_City_New"] != null && history["Home_City_New"] != "") NewAddress += ', ' + history["Home_City_New"];
                             if (history["Home_State_New"] != null && history["Home_State_New"] != "") NewAddress += ', ' + history["Home_State_New"];
                             if (history["Home_Zip_New"] != null && history["Home_Zip_New"] != "") NewAddress += ' ' + history["Home_Zip_New"];
                         }
                     }
                     if (key == "Home_Address1") {
                         OldAddress = "";
                         if (history["Home_Address1"] != null) {

                             OldAddress += history["Home_Address1"];
                             if (history["Home_Address2"] != null && history["Home_Address2"] != "") OldAddress += ', ' + history["Home_Address2"];
                             if (history["Home_City"] != null && history["Home_City"] != "") OldAddress += ', ' + history["Home_City"];
                             if (history["Home_State"] != null && history["Home_State"] != "") OldAddress += ', ' + history["Home_State"];
                             if (history["Home_Zip"] != null && history["Home_Zip"] != "") OldAddress += ' ' + history["Home_Zip"];
                         }
                     }
                 }
                 else if (sessionStorage["EditSectionName"] == "Shipping Address") {


                     if (key == "M_Address1") {
                         OldAddress = "";
                         if (history["M_Address1"] != null) {
                             OldAddress += history["M_Address1"];
                             if (history["M_Address2"] != null && history["M_Address2"] != "") OldAddress += ', ' + history["M_Address2"];
                             if (history["M_City"] != null && history["M_City"] != "") OldAddress += ', ' + history["M_City"];
                             if (history["M_State"] != null && history["M_State"] != "") OldAddress += ', ' + history["M_State"];
                             if (history["M_Zip"] != null && history["M_Zip"] != "") OldAddress += ' ' + history["M_Zip"];
                         }
                     }
                     if (key == "M_Address1_New") {
                         NewAddress = "";
                         if (history["M_Address1_New"] != null) {

                             NewAddress += history["M_Address1_New"];
                             if (history["M_Address2_New"] != null && history["M_Address2_New"] != "") NewAddress += ', ' + history["M_Address2_New"];
                             if (history["M_City_New"] != null && history["M_City_New"] != "") NewAddress += ', ' + history["M_City_New"];
                             if (history["M_State_New"] != null && history["M_State_New"] != "") NewAddress += ', ' + history["M_State_New"];
                             if (history["M_Zip_New"] != null && history["M_Zip_New"] != "") NewAddress += ' ' + history["M_Zip_New"];
                         }
                     }
                 }
                 //}


                 //}
                 //}
             }
             var tempObj = new Object();
             tempObj['name'] = sessionStorage["EditSectionName"];
             tempObj['value'] = OldAddress;
             OldArray.push(tempObj);
             tempObj = new Object();
             tempObj['name'] = sessionStorage["EditSectionName"];
             tempObj['value'] = NewAddress;
             NewArray.push(tempObj);
             str += '<div class="">' +
                        '<div style="display: inline-table; width: 30%">' +
                       ' Old Address</div>' +
                       ' <div style="display: inline-table; width: 69%">' + OldAddress +

                        '</div>' +
                        '<div style="display: inline-table; width: 30%">' +
                        'New Address' +
                        '</div>' +
                        ' <div style="display: inline-table; width: 69%">' + NewAddress +

                        '</div>' +
                    '</div>';

             ReturnObject['NewValue'] = NewArray;
             ReturnObject['OldValue'] = OldArray;

             return ReturnObject;

         }

         function getParameterByName(name) {
             name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
             var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
             results = regex.exec(location.search);
             return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
         }
    </script>
        <style>
        option {
            font-size: 14px !important;
            font-style: normal !important;
            opacity: 1;
        }
        .text-center {
        text-align :center
        }

            option:first-child {
                font-size: 13px !important;
                font-style: italic !important;
            }

        .Editfield {
            display: inline;
            cursor: pointer;
            color: #1e88e5;
        }

        .modal-body {
            max-height: 500px;
            overflow: auto;
        }
        .showdetails {
        display: none;
        }
        div.ajax-loading {
            position: fixed;
            top: 0;
            left: 0;
            z-index: 9999999;
            background-color: white;
            opacity: 0.7;
            filter: alpha(opacity=70); /* ie */
            -moz-opacity: 0.7; /* mozilla */
            /*display: none;*/
        }

            div.ajax-loading * {
                background-color: white;
                background-position: center center;
                background-repeat: no-repeat;
                opacity: 1;
                filter: alpha(opacity=100); /* ie */
                -moz-opacity: 1; /* mozilla */
            }
        .underlineTable{
            text-decoration: underline;
            text-align:left
        }
        .switch {
  position: relative;
  display: inline-block;
  width: 40px;
  height: 20px;
  margin: auto;
    vertical-align: middle;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
    margin: 0 auto;
    vertical-align:middle
}

.slider:before {
  position: absolute;
  content: "";
  height: 16px;
  width: 16px;
  left: 3px;
  bottom: 2px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #5cb85c;
}

input:focus + .slider {
  box-shadow: 0 0 1px #5cb85c;
}

input:checked + .slider:before {
  -webkit-transform: translateX(19px);
  -ms-transform: translateX(19px);
  transform: translateX(19px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
  .fc-field {
            margin: 5px;
            padding-left: 10px;
            background-color: #ffffff;
            box-shadow: 0px 0px 10px 0px rgb(154, 154, 154);
            line-height: 30px;
        }

        .fc-selected {
            box-shadow: 0px 0px 10px 0px #92b3e8; /**rgb(154, 154, 154)*/
            border: 2px solid #92b3e8;
            border-radius: 2px;
        }
           #report_geographyID::-webkit-input-placeholder, #ReportPreviewGeographyFilters::-webkit-input-placeholder {
            font-size: 13px;
            font-style: normal !important;
            opacity: 1;
            color: #333333;
        }
           #Geography_input::-webkit-input-placeholder {
            font-size: 13px;
            font-style: normal !important;
            opacity: 1;
            color: #333333;
        }
           .ReportDateClass {
            color: black;
            float: right;
            cursor: default;
            padding-right: 10px;
            font-size: 15px;
        }
           .ReportDateClass:focus {
                text-decoration: none !important;
                outline: none;
            }

            .ReportDateClass:hover {
                text-decoration: none !important;
                outline: none;
                color: black;
            }
        input.form-control {
        padding-left:15px !important;
        }
        table tbody th, table  tbody td {
    line-height: 15px; /* e.g. change 8x to 4px here */
}
        legend
        {
            background: none repeat scroll 0 0 transparent;
            background-color:#3c5b68;
            border-radius: 0.5em 0.5em 0.5em 0.5em;
            border: 2px solid #3c5b68;
            padding: 5px 100px 5px 10px;
            color: #1d5987 !important;
                width: auto;
    margin-left: 16px;
    background: none !important;
    border: none !important;
    padding: 0px 12px;
    font-size: 17px;
        }
        legend:hover
        {
            background-color:#b2c8db;
            cursor: pointer;
            color:#3c5b68;
        }
        .fieldset {
            border: 2px solid #e5e5e5;
            border-radius: .5em .5em .5em .5em;
            position:relative;
        }
        .right-cornar{    
           display:none;
}
        .logoff{
              padding: 13px;
        font-size: 24px;
        border-radius: 50%;
        background: rgba(0,0,0,0.5);
        margin-top: 10px;
        }
        
        .modal-dialog.modal-lg {
        width : 70%;
        }
        .modal-header {
        background-color:#2a3542;
        color:white;
        text-align:center;
        }
        .frame {
            display: inline;
            white-space: nowrap;
            /*text-align: center;
            margin: 1em 0;*/
        }
         .helper {
            display: inline-block;
            height: 100%;
            vertical-align: middle;
        }
          #web_logo {
            vertical-align: middle;
            /*height: 100%;*/
             
            float: left;
        }

        #cat_logo {
            vertical-align: middle;
            margin-right: 125px;
            padding: 5px;
        }

    </style>
</head>
<body>
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
            <Services>
                <asp:ServiceReference Path="~/DeGrToolWebService.asmx" />
            </Services>
        </asp:ScriptManager>
       <div style="">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="row" style="padding:5px 0px;">
                    <div style="width:33.33333333%;display:inline-block">
                         <img src="Images_air/NeuroLogo.png" class="logo" /> 
                        <%--<img src="Images_air/zs_phardma_logo.png" class="logo" />--%> 
                    </div>
                    <div  style="text-align:center;width:33.33333333%;display:inline-block">
                        <img src="Roster/Images/logo.png" /> 
                    </div>
                    
                </div>
            </div>
        </nav>
        <div class=" " style="padding:20px;">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="background-color: #f0f3f6;font-size: 16px;">
                                <div class="pull-left">Geography: <span class="value" id="TerritoryId"></span></div>
                                <div class="pull-right">Status: <span class="value" id="Status"></span></div>
                                <div class="text-center">Active Manager: <span class="value" id="TerritoryManager"></span></div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body" id="scrollTopDiv">
                                <div class="row">
                                    <div class="col-md-12">
                                        
                                        <div id="fiedlforceSection" class="section ">

                                            <div class="section-heading" style="display:none">
                                                Fieldforce
                                            <div class="pull-right Editfield" data-tab="myModal" data-modal="salesforceAddModal" data-edit-field="p_salesforceID" data-add-field="salesforceAddModal" data-opmode="EditData" data-message="Salesforce ID" data-modaltitle="Edit Salesforce"><a href="#" class="link"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Edit</a></div>
                                            </div>
                                             <fieldset class="fieldset" style="width: auto;">
                                                <legend class="ui-state-default">Fieldforce</legend>
                                            <div class="right-cornar"><div class="pull-right Editfield" data-tab="myModal" data-modal="salesforceAddModal" data-edit-field="p_salesforceID" data-add-field="salesforceAddModal" data-opmode="EditData" data-message="Salesforce ID" data-modaltitle="Edit Salesforce"><div style="color:white" ><i class=" " aria-hidden="true"></i>&nbsp;Edit</div></div></div>
                                          
                                            <div class="columns" id="primarySalesForceDetail">
                                                <div class="section-content">
                                                    <div class="label">Fieldforce:</div>
                                                    <div class="value" id="p_salesforceID"></div>
                                                </div>
                                            </div>

                                            <div id="moreSalesforceDetails" class="moreinfo">
                                                <div class="section-content">
                                                    <div class="label">Fieldforce Status:</div>
                                                    <div class="value" id="p_salesforce_status"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Fieldforce Start Date:</div>
                                                    <div class="value" id="p_salesforce_startdate"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Fieldforce End Date:</div>
                                                    <div class="value" id="p_salesforce_enddate"></div>
                                                </div>
                                            </div>
                                                 
                                            <div class="text-center">
                                                <a href="javascript:void(0)" class="showdetails" data-detail="moreSalesforceDetails" data-attr="show">Show More</a>
                                            </div>
</fieldset>
                                        </div>

                                        <div id="GeographySection" class="section">

                                            <div class="section-heading" style="display:none">
                                                Geography
                                            <div class="pull-right Editfield" data-tab="myModal" data-edit-field="p_terr" data-add-field="NewTerritoryAdd" data-opmode="EditData" data-message="Territory" data-modaltitle="Edit Geography"><a href="#" class="link"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Edit</a></div>
                                            </div>
 <fieldset class="fieldset" style="width: auto;">
                                                <legend class="ui-state-default">Geography</legend>
                                            <div class="right-cornar"><div class="pull-right Editfield" data-tab="myModal" data-modal="NewTerritoryAdd" data-edit-field="p_terr" data-add-field="NewTerritoryAdd" data-opmode="EditData" data-message="Territory" data-modaltitle="Edit Geography"><div style="color:white" ><i class=" " aria-hidden="true"></i>&nbsp;Edit</div></div></div>
                                          
                                            <div class="columns" id="primarygerographydetails">
                                                <div class="section-content">
                                                    <div class="label">Geography:</div>
                                                    <div class="value" id="p_terr"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Geography Type:</div>
                                                    <div class="value" id="p_terr_type"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Role:</div>
                                                    <div class="value" id="p_role"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Parent Geography:</div>
                                                    <div class="value" id="p_parent_terr"></div>
                                                </div>
                                            </div>
                                            <div id="TerrMoreInfo" class="moreinfo">
                                                <div class="section-content">
                                                    <div class="label">Geography Status:</div>
                                                    <div class="value" id="p_terr_status"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Geography Start Date:</div>
                                                    <div class="value" id="p_terr_start_date"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Geography End Date:</div>
                                                    <div class="value" id="p_terr_end_date"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Default Geography ZIP:</div>
                                                    <div class="value" id="p_terr_default_zip"></div>
                                                </div>
                                            </div>
                                            <div class="text-center"><a href="javascript:void(0)" class="showdetails" data-detail="TerrMoreInfo" data-attr="show">Show More</a></div>
                                       
                                            </fieldset>
                                             </div>

                                        <div id="EmployeeSection" class="section">
                                            <div class="section-heading" style="display:none">
                                                Employee
                                                 <%--<div class="pull-right Editfield" data-tab="MyEmployeeModal" data-modal="empAddModal" data-edit-field="emp_tab_emp_id" data-add-field="empAddModal" data-opmode="EditData" data-message="Employee" data-modaltitle="Edit Employee">
                                            <a href="#" class="link"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Edit</a></div>--%>
                                            </div>

                                            <fieldset class="fieldset" style="width: auto;">
                                                <legend class="ui-state-default">Employee</legend>
                                          
                                            <div class="columns" id="EmployeePrimaryDetails">
                                                <div class="section-content">
                                                    <div class="label">User ID:</div>
                                                    <div class="value" id="p_user_id"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Department:</div>
                                                    <div class="value" id="p_emp_department"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Employee Title:</div>
                                                    <div class="value" id="p_emp_title"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Full name:</div>
                                                    <div class="value" id="p_emp_name"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Preferred Name:</div>
                                                    <div class="value" id="p_preferredName"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Manager ID:</div>
                                                    <div class="value" id="p_terr_manager_id"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Manager Name:</div>
                                                    <div class="value" id="p_terr_manager_name"></div>
                                                </div>
                                            </div>
                                            <div id="empDetailInfo" class="moreinfo">
                                                <div class="section-content">
                                                    <div class="label">Position Start Date:</div>
                                                    <div class="value" id="p_emp_position_start_date"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Employee Type:</div>
                                                    <div class="value" id="p_emp_type"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Employee Status:</div>
                                                    <div class="value" id="p_emp_staus"></div>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <a href="javascript:void(0)" class="showdetails" data-detail="empDetailInfo" data-attr="show">Show More</a>
                                            </div>
                                                </fieldset>
                                        </div>
                                        
                                        <div id="TerritoryAssignmentSection" class="section">
                                            <div class="section-heading" style="display:none">
                                                Territory Assignments
                                                 <%--<div class="pull-right Editfield" data-tab="MyEmployeeModal" data-modal="empAddModal" data-edit-field="emp_tab_emp_id" data-add-field="empAddModal" data-opmode="EditData" data-message="Employee" data-modaltitle="Edit Employee">
                                            <a href="#" class="link"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Edit</a></div>--%>
                                            </div>

                                            <fieldset class="fieldset" style="width: auto;">
                                                <legend class="ui-state-default">Territory Assignments</legend>
                                          <div class="columns" style="margin:0 auto; width:95%;padding-bottom:10px;">
                                        <table id="TerritoryEmployeeRelation" style="width: 100%">
                                            <thead>
                                                <tr>

                                                    <th class="underlineTable">Fieldforce
                                                    </th>
                                                    <th class="underlineTable">Employee Name
                                                    </th>
                                                    <th class="underlineTable">Role
                                                    </th>
                                                    <th class="underlineTable">Start Date
                                                    </th>
                                                    <th class="underlineTable">End Date
                                                    </th>
                                                    <th class="underlineTable">Assignment Type
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody id="TerritoryEmployeeRelationBody">
                                            </tbody>
                                        </table>
                                    </div>
                                          
                                                </fieldset>
                                        </div>


                                        <div id="rosterEmployeeChanges" class="section">
                                           
                                                    <div class="section-heading" style="display:none">
                                                        Recent Changes 
                                            
                                                    </div>
                                             <fieldset class="fieldset" style="width: auto;">
                                                <legend class="ui-state-default">Recent Changes</legend>
                                                <div class="columns" style="margin: 0 auto; width: 95%;padding-bottom:10px;">
                                                    <div>
                                                           <div style="display: inline-table; width: 12%">
                                                <span style="border-bottom: 1px solid black">Last Edit Date</span>
                                            </div>
                                            <div style="display: inline-table; width: 15%;">
                                                <span style="border-bottom: 1px solid black">Edit Type</span>
                                            </div>
                                            <div style="display: inline-table;width: 20%;"">
                                                <span style="border-bottom: 1px solid black">Change Type</span>
                                            </div>
                                            <div style="display: inline-table;width: 20%;"">
                                                <span style="border-bottom: 1px solid black">Old Value</span>
                                            </div>
                                            <div style="display: inline-table;width: 20%;"">
                                                <span style="border-bottom: 1px solid black">New value</span>
                                            </div>
                                             <div style="display: inline-table;width: 10%; text-align:center">
                                                <span style="border-bottom: 1px solid black">Edited By</span>
                                            </div>
                                                    </div>
                                                    <div id="RecentChangeData">
                                                    </div>
                                                </div>
                                        </fieldset>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
           </div>
    </form>
</body>
</html>
