﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BLL.Utilities;
using BLL.Masters;

public partial class frmAttachmentUpload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    
    }
    protected void btnUploadFile_Click(object sender, EventArgs e)
    {
        try
        {
            string message = string.Empty;
            if (!FileUpload1.HasFile)
            {
                message = "Please select the file.";
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "mess", "alert('" + message + "');", true);

                return;
            }
            if (FileUpload1.HasFile)
            {
                FileInfo f = new FileInfo(FileUpload1.PostedFile.FileName);
                
                string strExtension = Path.GetExtension(f.ToString()).ToLower();
                if (strExtension != ".wav" && strExtension != ".mp3" && strExtension != ".wma" && strExtension != ".pdf")
                {
                    message = "Please upload only Audio or PDF file.";
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "mess", "alert('" + message + "');", true);
                    return;
                }

                Attachment objAttachment = new Attachment();
                String AttachmentNo = objAttachment.GetNextAttachmentNo(ref message);
                if (AttachmentNo == null || AttachmentNo.Trim() == String.Empty)
                {
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "mess", "alert('" + message + "');", true);
                    return;
                }

                string filePath = Server.MapPath("~/Attachments/");
                FileInfo fi = new FileInfo(filePath + AttachmentNo + strExtension);
                if (fi.Exists)
                    fi.Delete();
                HttpCookie userCookie = new HttpCookie("AttachmentName");
                userCookie.Value = FileUpload1.FileName;
                Response.Cookies.Add(userCookie);
                HttpCookie userCookieType = new HttpCookie("AttachmentType");
                userCookieType.Value = strExtension;
                Response.Cookies.Add(userCookieType);
                HttpCookie userCookiePath = new HttpCookie("AttachmentPath");
                userCookiePath.Value = "/Attachments/" + AttachmentNo + FileUpload1.FileName;
                Response.Cookies.Add(userCookiePath);
                FileUpload1.PostedFile.SaveAs(filePath + AttachmentNo + FileUpload1.FileName);

                //ScriptManager.RegisterStartupScript(Page, this.GetType(), "mess", "alert('File Upload Sucessfully.');", true);

                this.Page.ClientScript.RegisterStartupScript(typeof(Page), "closeThickBox", "self.parent.updated();", true);
            }
        }
        catch (Exception ex)
        {
            ServerLog.ExceptionLog(ex.Message.ToString() + Environment.NewLine + ex.StackTrace);
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "mess", "alert('" + ex.Message.ToString() + "');", true);
            this.Page.ClientScript.RegisterStartupScript(typeof(Page), "closeThickBox", "self.parent.updated();", true);
            Response.Write("Problem in uploading the data" + ex.Message + " " + ex.StackTrace);
            return;
        }
    }
}