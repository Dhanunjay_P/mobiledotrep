﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
    <script src="ui/CookiesCreate.js" type="text/javascript"></script>
    <style type="text/css">
        .header
        {
            width: 100%;
            height: 100px;
            float: left; /*background: url(images/header.png) no-repeat;*/
        }
        .clogo
        {
            width: 100%;
            height: 86px;
            float: left;
            margin-top: 0.7%;
        }
        img, object
        {
            max-width: 100%;
        }
        object
        {
            margin: 0 auto 1em;
            max-width: 100%;
        }
        .headermidd
        {
            width: auto;
            float: left;
            font-size: 20px;
            color: #448fb9;
            padding: 35px;
        }
        .headerleft
        {
            height: 100px;
            float: left;
            width: 1.2%;
            background: url(Images/leftheader.png) no-repeat;
            z-index: 2;
        }
        .headermiddle
        {
            height: 100px;
            float: left;
            width: 97%;
            background: url(Images/header.png) repeat-x;
        }
        .headerright
        {
            height: 100px;
            float: left;
            width: 1.8%;
            background: url(Images/rigthheader.png) no-repeat;
            z-index: 2;
        }
        #tblLogin td
        {
            padding: 5px 10px;
            text-align: left;
        }
        #tblLogin
        {
            border: 1px solid #000;
            background-color: #F5F5F5;
        }
        #tblCheck
        {
            border: 1px solid #000;
            border-top: 0;
            background-color: #F0F8FF;
        }
        #tblCheck td
        {
            height: 65px;
            padding: 5px 10px;
            text-align: left;
        }
        label, .login
        {
            font-family: Verdana;
            font-size: 14px;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function ValidateDetails() {
            var UserName;
            var Password;
            if (document.getElementById("username").value == "") {
                alert("Please enter username.");
                document.getElementById("username").style.borderColor = 'red';
                document.getElementById("username").focus();
                return false;
            }
            else {
                UserName = document.getElementById("username").value;
                document.getElementById("username").style.borderColor = '';
            }

            if (document.getElementById("password").value == "") {
                alert("Please enter password.");
                document.getElementById("password").style.borderColor = 'red';
                document.getElementById("password").focus();
                return false;
            }
            else {
                Password = document.getElementById("password").value;
                document.getElementById("password").style.borderColor = '';
            }

            DeGrToolWebService.LoginWebSite(UserName, Password, SuccessfulLogin);
        }
        function SuccessfulLogin(result) {
            var resSplit = result.split('|');
            if (resSplit[0] == "Successful") {
                createCookie("LoginUser", document.getElementById("username").value);
                createCookie("GroupType", resSplit[1]);
                window.location.href = "Home.aspx";
            }
            else {
                alert(resSplit[0]);
                document.getElementById("password").style.borderColor = 'red';
                document.getElementById("password").focus();
                return false;
            }
        }
        function focus() {
            document.getElementById("username").focus();

            var viewportwidth = 640;
            var viewportheight = 480;
            if (typeof window.innerWidth != 'undefined') {
                viewportwidth = window.innerWidth;
                viewportheight = window.innerHeight;
            }
            // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
            else if (typeof document.documentElement != 'undefined'
                     && typeof document.documentElement.clientWidth !=
                     'undefined' && document.documentElement.clientWidth != 0) {
                viewportwidth = document.documentElement.clientWidth;
                viewportheight = document.documentElement.clientHeight;
            }
            // older versions of IE
            else {
                viewportwidth = document.getElementsByTagName('body')[0].clientWidth;
                viewportheight = document.getElementsByTagName('body')[0].clientHeight;
            }
            document.getElementById("tblLoginMain").style.height = viewportheight;
        }
  
    </script>
</head>
<body onload="focus();">
    <form id="form1" runat="server" onsubmit="ValidateDetails();">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        <Services>
            <asp:ServiceReference Path="~/DeGrToolWebService.asmx" />
        </Services>
    </asp:ScriptManager>
    <table style="width: 101%; margin-left:-5px;  margin-top:-5px;">
        <tr>
            <td>
                <div class="header">
                    <div class="headerleft">
                    </div>
                    <div class="headermiddle">
                        <div>
                            <table class="clogo">
                                <tr>
                                    <td>
                                        <img src="Images/Banner.png" alt="" width="100%" height="62px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="DivHeaderMain" align="center" style="font-size: 12px; font-family: Arial;
                                            margin: auto;">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <%--   <div class="clogo">
            </div> class="headermidd"--%>
                    </div>
                    <div class="headerright">
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <table id="tblLoginMain" style="width: 100%; height: 460px;">
                    <tr>
                        <td valign="middle">
                            <div align="center">
                                <table id="tblLogin" style="width: 320px;">
                                    <tr>
                                        <td>
                                            <label>
                                                Username:</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="username" type="text" placeholder="Enter Username" maxlength="50" style="padding: 5px;
                                                width: 250px; margin-right: 10px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>
                                                Password:</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="password" type="password" placeholder="Enter Password" maxlength="25"
                                                style="padding: 5px; width: 250px; margin-right: 10px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                                <table id="tblCheck" style="width: 320px;">
                                    <tr>
                                        <td>
                                            <input type="checkbox" />
                                            <label>
                                                Remember Me</label>
                                        </td>
                                        <td>
                                            <input type="submit" style="height: 25px; width: 75px; margin-right: 10px;" value="Login"
                                                class="login" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
