﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
//using System.Data.OracleClient;
//using MySql.Data.MySqlClient;

/// <summary>
/// Summary description for DBObjectFactory
/// </summary>

    public class DBObjectFactory
    {
        private static string connectionString = ConfigurationManager.ConnectionStrings["DeGrToolLiveConnectionString"].ConnectionString;
        private static string Provider = ConfigurationManager.ConnectionStrings["DeGrToolLiveConnectionString"].ProviderName;
        //public static string Provider = "System.Data.OracleClient"; //"ORAOLEDB.ORACLE";
        //private static string Provider = "MySql.Data.MySqlClient";

        public static IDbConnection GetConnectionObject()
        {
            switch (Provider)
            {
                case "System.Data.SqlClient":
                    return new SqlConnection();
                //case "System.Data.OracleClient":
                //    return new OracleConnection();
                //case "MySql.Data.MySqlClient":
                //    return new MySqlConnection();
            }
            return null;
        }

        public static IDbCommand GetCommandObject()
        {
            switch (Provider)
            {
                case "System.Data.SqlClient":
                    SqlCommand DBCommand = new SqlCommand();
                    if (ConfigurationManager.AppSettings.Get("CommandTimeout") != null && ConfigurationManager.AppSettings.Get("CommandTimeout").Trim() != String.Empty)
                        DBCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings.Get("CommandTimeout"));
                    else
                        DBCommand.CommandTimeout = 180;
                    return DBCommand;
                //case "System.Data.OracleClient":
                //    return new OracleCommand();
                //case "MySql.Data.MySqlClient":
                //    return new MySqlCommand();
            }
            return null;
        }

        public static IDbDataAdapter GetDataAdapterObject(IDbCommand DBCommand)
        {
            switch (Provider)
            {
                case "System.Data.SqlClient":
                    return new SqlDataAdapter((SqlCommand)DBCommand);
                //case "System.Data.OracleClient":
                //    return new OracleDataAdapter((OracleCommand)DBCommand);
                //case "MySql.Data.MySqlClient":
                //    return new MySqlDataAdapter((MySqlCommand)DBCommand);
            }
            return null;
        }

        public static IDataParameter GetParameterObject()
        {
            switch (Provider)
            {
                case "System.Data.SqlClient":
                    return new SqlParameter();
                //case "System.Data.OracleClient":
                //    return new OracleParameter();
                //case "MySql.Data.MySqlClient":
                //    return new MySqlParameter();
            }
            return null;
        }

        public static IDataParameter MakeParameter(String ParameterName, DbType ParameterType, Object ParameterValue)
        {
            IDataParameter parameter = DBObjectFactory.GetParameterObject();
            parameter.ParameterName = ParameterName;
            parameter.DbType = ParameterType;
            parameter.Value = ParameterValue;
            return parameter;
        } 
    }
