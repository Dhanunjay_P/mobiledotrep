﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;

/// <summary>
/// Summary description for ServerBase
/// </summary>
public class ServerBase : IDisposable
{
    protected IDbConnection DBConnection;
    protected IDbTransaction DBTransactionObject = null;
    protected IDbCommand DBCommand;
    protected IDbDataAdapter DBDataAdpterObject;

    public ServerBase()
    {
        DBConnection = DBObjectFactory.GetConnectionObject();
        DBConnection.ConnectionString = ConfigurationManager.ConnectionStrings["DeGrToolLiveConnectionString"].ToString();
        DBCommand = DBObjectFactory.GetCommandObject();
        DBCommand.Connection = DBConnection;
        if (ConfigurationManager.AppSettings.Get("CommandTimeout") != null && ConfigurationManager.AppSettings.Get("CommandTimeout").Trim() != String.Empty)
            DBCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings.Get("CommandTimeout"));
        else
            DBCommand.CommandTimeout = 180;
        DBDataAdpterObject = DBObjectFactory.GetDataAdapterObject(DBCommand);
        if (ConfigurationManager.AppSettings.Get("CommandTimeout") != null && ConfigurationManager.AppSettings.Get("CommandTimeout").Trim() != String.Empty)
            DBDataAdpterObject.SelectCommand.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings.Get("CommandTimeout"));
        else
            DBDataAdpterObject.SelectCommand.CommandTimeout = 180; 
    }

    public void Dispose()
    {
        if (DBConnection != null)
        {
            if (DBConnection.State == ConnectionState.Open) DBConnection.Close();
        }
    }
}