﻿<%@ WebHandler Language="C#" Class="AttributeImageUpload" %>

using System;
using System.Web;

public class AttributeImageUpload : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        try
        {
            string str = "";
            string fname = "";
            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;
                String fileConcat = context.Request.Form["currentDateTime"];

                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    str = fileConcat + "___" + file.FileName;
                    fname = context.Server.MapPath("~/AttributeImageUpload/" + str);
                    file.SaveAs(fname);
                }
                context.Response.ContentType = "text/plain";
                context.Response.Write(fname);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}