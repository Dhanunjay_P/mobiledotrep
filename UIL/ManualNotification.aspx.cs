﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.Masters;
using System.Data;

public partial class ManualNotification : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            //Display Menu according to GroupType
            HttpCookie groupType = Request.Cookies["GroupType"];
            if (groupType != null)
            {
                if (groupType.Value != null && groupType.Value != "")
                {
                    DeGrToolWebService obj = new DeGrToolWebService();
                    menu.InnerHtml = obj.GetGroupRights(groupType.Value);
                }
            }
            else
                return;            
        }
    }    
}