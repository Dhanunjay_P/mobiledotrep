﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.Services;

public partial class ResetPassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        HttpCookie groupType = Request.Cookies["GroupType"];
        if (groupType != null)
        {
            if (groupType.Value != null && groupType.Value != "")
            {
                DeGrToolWebService obj = new DeGrToolWebService();
                menu.InnerHtml = obj.GetGroupRights(groupType.Value);

            }
        }
    }
}