﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Report.aspx.cs" Inherits="Report" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>User Activity Log</title>
    <!-- for Menu CSS -->
    <link href="Menu/menu.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        table thead  {
            cursor: pointer;
        }
         body
        {
            font-family: Arial;font-size: 15px;
        }
        
        div#menu
        {
            width: 98%;
            float: left;
            margin-left: 0.8%;
        }
        div#copyright
        {
            margin: 0 auto;
            width: 80%;
            font: 11px 'Arial MS';
            color: #124a6f;
            text-indent: 20px;
            padding: 40px 0 0 0;
        }
        div#copyright a
        {
            color: #4682b4;
        }
        div#copyright a:hover
        {
            color: #124a6f;
        }
        .header
        {
            width: 100%;
            height: 100px;
            float: left; /*background: url(images/header.png) no-repeat;*/
        }
        .clogo
        {
            width: 100%;
            height: 86px;
            float: left;
            margin-top: 0.7%;
        }
        img, object
        {
            max-width: 100%;
        }
        object
        {
            margin: 0 auto 1em;
            max-width: 100%;
        }
        .headermidd
        {
            width: auto;
            float: left;
            font-size: 20px;
            color: #448fb9;
            padding: 35px;
        }
        .headerleft
        {
            height: 100px;
            float: left;
            width: 1.2%;
            background: url(Images/leftheader.png) no-repeat;
            z-index: 2;
        }
        .headermiddle
        {
            height: 100px;
            float: left;
            width: 97%;
            background: url(Images/header.png) repeat-x;
        }
        .headerright
        {
            height: 100px;
            float: left;
            width: 1.8%;
            background: url(Images/rigthheader.png) no-repeat;
            z-index: 2;
        }
        .font
        {
            font-family: Arial;
            font-size: 15px;
        }
        .button
        {
            width: 60px;
            height: 24px;
            background: #5486a6;
            cursor: pointer;
            color: #fff;
            padding: 3px;
            border: 1px Solid #5486a6;
            border-radius: 3px;
        }
        .button:hover
        {
            width: 60px;
            height: 24px;
            background: #175279;
            cursor: pointer;
            color: #fff;
            padding: 3px;
            border: 1px Solid #175279;
            border-radius: 3px;
        }
        .buttonNavigate
        {
            font-size: 15px;
            height: 25px;
            background: Silver;
            cursor: pointer;
            color: Black;
            font-weight: bold;
            cursor: pointer;
            border: 2px Solid #CCCCCC;
        }
        .buttonNavigate:hover
        {
            font-size: 15px;
            height: 25px;
            background: #CCCCCC;
            cursor: pointer;
            color: Black;
            font-weight: bold;
            cursor: pointer;
            border: 2px Solid Silver;
        }
        .buttoncommand
        {
            float: left;
            width: 50px;
            height: 20px;
            border: none;
            background: #448fb9;
            cursor: pointer;
            color: #fff;
            z-index: 22;
            border: 2px Solid #419cce;
        }
        #tblDepotProductDetail
        {
            border: #d5d4d4 0.5px solid;
            border-collapse: collapse;
            width: -moz-max-content;
        }
        
        #tblDepotProductDetail tr
        {
            border-top: #d5d4d4 1px solid;
        }
        
        #tblDepotProductDetail td
        {
            font-family: Arial;
            font-size: 13px;
            text-align: left;
            border: #d5d4d4 1px solid;
            border-top: #d5d4d4 1px solid;
        }
        #tblDepotProductDetail td input[type="text"]
        {
            font-family: Arial;
            background-color: transparent;
            border: 0px solid;
        }
        #tblDepotProductDetail td select
        {
            background-color: transparent;
            border: 0px solid;
        }
        #ddlDepotName
        {
            height: 16px;
        }
    </style>
    <!-- End Menu CSS -->
    <!-- getcookie mthod -->
    <script src="ui/CookiesCreate.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function EraseCookieOnLogout() {
            eraseCookie("LoginUser");
            window.location.href = "Login.aspx";
        }
    </script>
    <script language="javascript" type="text/javascript">
        function GetInitialData() {

            var cookieName = getCookie("LoginUser");
            if (cookieName == null) {

                window.location.href = "Login.aspx";
            }
            else {

                DeGrToolWebService.GetInitialDataForReport(SetInitialData);
                //DeGrToolWebService.GetProductDetails(document.getElementById("txtManuId").value, SetProductDtls);
            }
        }
        function SetInitialData(result) {


            var DetailsMain = result.split("|");

            var SubDetails = "";
            var optn;
            if (DetailsMain != null && DetailsMain != "") {

                document.getElementById("ddlAppName").options.length = 0;
                optn = document.createElement("OPTION");
                optn.text = "";
                optn.value = "";
                document.getElementById("ddlAppName").options.add(optn);
                for (var i = 0; i < DetailsMain.length; i++) {

                    SubDetails = DetailsMain[i].split("~");

                    if (SubDetails != null && SubDetails != "") {

                        optn = document.createElement("OPTION");
                        optn.text = SubDetails[1].toString();
                        optn.value = SubDetails[0].toString();
                        document.getElementById("ddlAppName").options.add(optn);
                    }
                }
            }
            document.getElementById("ddlLogType").options.length = 0;
            optn = document.createElement("OPTION");
            optn.text = "";
            optn.value = "";
            document.getElementById("ddlLogType").options.add(optn);

            optn = document.createElement("OPTION");
            optn.text = "Sync";//changed from S to Sync 11April
            optn.value = "S";
            document.getElementById("ddlLogType").options.add(optn);

            optn = document.createElement("OPTION");
            optn.text = "Launch"; //changed from L to Launch 11April
            optn.value = "L";
            document.getElementById("ddlLogType").options.add(optn);
        }
    </script>
    <script language="javascript" type="text/javascript">
        function GetSelectedAppData() {

            var e = document.getElementById("ddlAppName");
            var appName = e.options[e.selectedIndex].text;

            DeGrToolWebService.GetSelectedAppData(appName, SetInitialAppData);

        }

        function SetInitialAppData(result) {

            var DetailsMain = result.split("|");

            var SubDetails = "";
            var optn;
            if (DetailsMain != null && DetailsMain != "") {
                document.getElementById("tblappDetails").style.display = "block";
                document.getElementById("ddlRepId").options.length = 0;
                optn = document.createElement("OPTION");
                optn.text = "";
                optn.value = "";
                document.getElementById("ddlRepId").options.add(optn);
                for (var i = 0; i < DetailsMain.length; i++) {

                    SubDetails = DetailsMain[i].split("~");

                    if (SubDetails != null && SubDetails != "") {

                        optn = document.createElement("OPTION");
                        optn.text = SubDetails[1].toString();
                        optn.value = SubDetails[0].toString();
                        document.getElementById("ddlRepId").options.add(optn);
                    }
                }
            } //end Details Main
            getSumaaryReport();
        }
    </script>
    <!-- getSummaryReport -->
    <script language="javascript" type="text/javascript">
        function getSumaaryReport() {

            var e = document.getElementById("ddlAppName");
            var appName = e.options[e.selectedIndex].text;

            if (appName == "") {
                alert("Please Select AppName");
                return false;
            }

            var repId = "";
            var e1 = document.getElementById("ddlRepId");
            if (e1 != undefined && e1 != null) {
                repId = e1.options[e1.selectedIndex].text;
            }
            // alert(repId);
            var LogType = "";
            var e2 = document.getElementById("ddlLogType");
            if (e2 != undefined && e2 != null) {
                //LogType = e2.options[e2.selectedIndex].text; commented on 11april
                LogType = e2.options[e2.selectedIndex].value;
            }
            //  alert(LogType);
            var txtFrom = ""; var txtTo = "";

            if ((document.getElementById("Txtfromdate").value != "") && (document.getElementById("Txttodate").value != "")) {
                var e3 = document.getElementById("Txtfromdate").value;
                if (e3 != undefined && e3 != null && e3 != "") {
                    txtFrom = e3 + " 12:00:00 AM";
                    //added
                    /*
                    var txtFrom_utc = new Date(txtFrom);
                    txtFrom = new Date(txtFrom_utc.getUTCFullYear(), txtFrom_utc.getUTCMonth(),
                    txtFrom_utc.getUTCDate(), txtFrom_utc.getUTCHours(), txtFrom_utc.getUTCMinutes(), txtFrom_utc.getUTCSeconds());
                    */
                    //start
                    // 1. req from india,,,,,,,,,,timezone=india to UTC
                    // 2. req from UTC to UTC(Amazon) 
                    // 3. req from india,,,,,,,,,,timezone=india(DataServer)
                    var now = new Date();
                    //   alert(txtFrom);
                    var utcdt = (now.getUTCDate()); //getcurrentUTCdate
                    var nowdt = (now.getDate()); //getDate
                    if (utcdt == nowdt) {//req from UTC to UTC
                        var utchr = (now.getUTCHours());
                        var nowhr = (now.getHours());
                        if (utchr == nowhr) {//date and hrs both are same so utc to utc
                            //alert("utc to utc"); //donothing
                        }
                        else {
                            //req from India to UTC or req from India to dataserver
                            //alert("inside utc"); //utc and current time=differ::::://do processing

                            var serverDate = new Date('<%= DateTime.Now.ToString() %>');
                            var clientDate = new Date();
                            var diffMin = (serverDate.getTime() - clientDate.getTime()) / (1000 * 60);


                            if ((diffMin > 5) || (diffMin < -5))//request is to Amazon
                            {
                                var txtFrom_utc = new Date(txtFrom);
                                //alert(txtFrom);
                                //alert(txtFrom_utc);
                                txtFrom = txtFrom_utc.getUTCFullYear() + "/" + parseInt(txtFrom_utc.getUTCMonth() + 1) + "/" +
                        txtFrom_utc.getUTCDate() + " " + txtFrom_utc.getUTCHours() + ":" + txtFrom_utc.getUTCMinutes() + ":" + txtFrom_utc.getUTCSeconds();
                            }
                            else {
                                //req is to dataserver
                            }


                            //  alert(txtFrom);

                        }
                    } //end req from UTC to UTC
                    else {
                        //req from India to UTC or req from India to dataserver
                        //alert("ntdone"); //do processing

                        var serverDate = new Date('<%= DateTime.Now.ToString() %>');
                        var clientDate = new Date();
                        var diffMin = (serverDate.getTime() - clientDate.getTime()) / (1000 * 60);

                        if ((diffMin > 5) || (diffMin < -5))//request is to Amazon
                        {
                            var txtFrom_utc = new Date(txtFrom);
                            //                        txtFrom = new Date(txtFrom_utc.getUTCFullYear(), txtFrom_utc.getUTCMonth(),
                            //                        txtFrom_utc.getUTCDate(), txtFrom_utc.getUTCHours(), txtFrom_utc.getUTCMinutes(), txtFrom_utc.getUTCSeconds());
                            txtFrom = txtFrom_utc.getUTCFullYear() + "/" + parseInt(txtFrom_utc.getUTCMonth() + 1) + "/" +
                        txtFrom_utc.getUTCDate() + " " + txtFrom_utc.getUTCHours() + ":" + txtFrom_utc.getUTCMinutes() + ":" + txtFrom_utc.getUTCSeconds();
                        }
                        else {
                            //req is to dataserver
                        }



                    } //end req from India to UTC
                    //end
                }

                var e4 = document.getElementById("Txttodate").value;
                if (e4 != undefined && e4 != null && e4 != "") {
                    txtTo = e4 + " 11:59:59 PM";

                    //added
                    /*
                    var txtTo_utc = new Date(txtTo);
                    txtTo = new Date(txtTo_utc.getUTCFullYear(), txtTo_utc.getUTCMonth(),
                    txtTo_utc.getUTCDate(), txtTo_utc.getUTCHours(), txtTo_utc.getUTCMinutes(), txtTo_utc.getUTCSeconds());
                    */
                    // 1. req from india,,,,,,,,,,timezone=india to UTC
                    // 2. req from UTC to UTC(Amazon) 
                    // 3. req from india,,,,,,,,,,timezone=india(DataServer)
                    var now = new Date();

                    var utcdt = (now.getUTCDate()); //getcurrentUTCdate
                    var nowdt = (now.getDate()); //getDate
                    if (utcdt == nowdt) {//req from UTC to UTC
                        var utchr = (now.getUTCHours());
                        var nowhr = (now.getHours());
                        if (utchr == nowhr) {//date and hrs both are same so utc to utc
                            //       alert("utc to utc"); //donothing
                        }
                        else {
                            //req from India to UTC or req from India to dataserver
                            //     alert("inside utc"); //utc and current time=differ::::://do processing



                            var serverDate = new Date('<%= DateTime.Now.ToString() %>');
                            var clientDate = new Date();
                            var diffMin = (serverDate.getTime() - clientDate.getTime()) / (1000 * 60);

                            if ((diffMin > 5) || (diffMin < -5))//request is to Amazon
                            {
                                var txtTo_utc = new Date(txtTo);
                                //                            txtTo = new Date(txtTo_utc.getUTCFullYear(), txtTo_utc.getUTCMonth(),
                                //                        txtTo_utc.getUTCDate(), txtTo_utc.getUTCHours(), txtTo_utc.getUTCMinutes(), txtTo_utc.getUTCSeconds());
                                txtTo = txtTo_utc.getUTCFullYear() + "/" + parseInt(txtTo_utc.getUTCMonth() + 1) + "/" +
                        txtTo_utc.getUTCDate() + " " + txtTo_utc.getUTCHours() + ":" + txtTo_utc.getUTCMinutes() + ":" + txtTo_utc.getUTCSeconds();
                            }
                            else {
                                //do nothing
                            }


                        }
                    } //end req from UTC to UTC
                    else {
                        //req from India to UTC or req from India to dataserver
                        //                        alert("ntdone"); //do processing


                        var serverDate = new Date('<%= DateTime.Now.ToString() %>');
                        var clientDate = new Date();
                        var diffMin = (serverDate.getTime() - clientDate.getTime()) / (1000 * 60);

                        if ((diffMin > 5) || (diffMin < -5))//request is to Amazon 
                        {
                            var txtTo_utc = new Date(txtTo);
                            //txtTo = new Date(txtTo_utc.getUTCFullYear(), txtTo_utc.getUTCMonth(),
                            //txtTo_utc.getUTCDate(), txtTo_utc.getUTCHours(), txtTo_utc.getUTCMinutes(), txtTo_utc.getUTCSeconds());
                            txtTo = txtTo_utc.getUTCFullYear() + "/" + parseInt(txtTo_utc.getUTCMonth() + 1) + "/" +
                        txtTo_utc.getUTCDate() + " " + txtTo_utc.getUTCHours() + ":" + txtTo_utc.getUTCMinutes() + ":" + txtTo_utc.getUTCSeconds();
                        }
                        else {
                            //do nothing
                        }

                    } //end req from India to UTC
                }
            }
            else if ((document.getElementById("Txtweeklyfrom").value != "") && (document.getElementById("TxtweeklyTo").value != "")) {
                var e5 = document.getElementById("Txtweeklyfrom").value;
                if (e5 != undefined && e5 != null && e5 != "") {
                    txtFrom = e5 + " 12:00:00 AM";

                    //added
                    /*
                    var txtFrom_utc = new Date(txtFrom);
                    txtFrom = new Date(txtFrom_utc.getUTCFullYear(), txtFrom_utc.getUTCMonth(),
                    txtFrom_utc.getUTCDate(), txtFrom_utc.getUTCHours(), txtFrom_utc.getUTCMinutes(), txtFrom_utc.getUTCSeconds());
                    */
                    // 1. req from india,,,,,,,,,,timezone=india to UTC
                    // 2. req from UTC to UTC(Amazon) 
                    // 3. req from india,,,,,,,,,,timezone=india(DataServer)
                    var now = new Date();

                    var utcdt = (now.getUTCDate()); //getcurrentUTCdate
                    var nowdt = (now.getDate()); //getDate
                    if (utcdt == nowdt) {//req from UTC to UTC
                        var utchr = (now.getUTCHours());
                        var nowhr = (now.getHours());
                        if (utchr == nowhr) {//date and hrs both are same so utc to utc
                            //                            alert("utc to utc"); //donothing
                        }
                        else {
                            //req from India to UTC or req from India to dataserver
                            //                          alert("inside utc"); //utc and current time=differ::::://do processing

                            //start

                            var serverDate = new Date('<%= DateTime.Now.ToString() %>');
                            var clientDate = new Date();
                            var diffMin = (serverDate.getTime() - clientDate.getTime()) / (1000 * 60);

                            if ((diffMin > 5) || (diffMin < -5))//request is to Amazon
                            {
                                var txtFrom_utc = new Date(txtFrom);
                                //   txtFrom = new Date(txtFrom_utc.getUTCFullYear(), txtFrom_utc.getUTCMonth(),
                                //txtFrom_utc.getUTCDate(), txtFrom_utc.getUTCHours(), txtFrom_utc.getUTCMinutes(), txtFrom_utc.getUTCSeconds());
                                txtFrom = txtFrom_utc.getUTCFullYear() + "/" + parseInt(txtFrom_utc.getUTCMonth() + 1) + "/" +
                        txtFrom_utc.getUTCDate() + " " + txtFrom_utc.getUTCHours() + ":" + txtFrom_utc.getUTCMinutes() + ":" + txtFrom_utc.getUTCSeconds();

                            }
                            else {
                                //do nothing
                            }


                        }
                    } //end req from UTC to UTC
                    else {
                        //req from India to UTC or req from India to dataserver
                        //                        alert("ntdone"); //do processing

                        //start
                        var serverDate = new Date('<%= DateTime.Now.ToString() %>');
                        var clientDate = new Date();
                        var diffMin = (serverDate.getTime() - clientDate.getTime()) / (1000 * 60);

                        if ((diffMin > 5) || (diffMin < -5))//request is to Amazon
                        {
                            var txtFrom_utc = new Date(txtFrom);
                            // txtFrom = new Date(txtFrom_utc.getUTCFullYear(), txtFrom_utc.getUTCMonth(),
                            //txtFrom_utc.getUTCDate(), txtFrom_utc.getUTCHours(), txtFrom_utc.getUTCMinutes(), txtFrom_utc.getUTCSeconds());
                            txtFrom = txtFrom_utc.getUTCFullYear() + "/" + parseInt(txtFrom_utc.getUTCMonth() + 1) + "/" +
                        txtFrom_utc.getUTCDate() + " " + txtFrom_utc.getUTCHours() + ":" + txtFrom_utc.getUTCMinutes() + ":" + txtFrom_utc.getUTCSeconds();

                        }
                        else {
                            //do nothing
                        }
                    } //end req from India to UTC
                }
                var e6 = document.getElementById("TxtweeklyTo").value;
                if (e6 != undefined && e6 != null && e6 != "") {
                    txtTo = e6 + " 11:59:59 PM";
                    //added
                    /*
                    var txtTo_utc = new Date(txtTo);
                    txtTo = new Date(txtTo_utc.getUTCFullYear(), txtTo_utc.getUTCMonth(),
                    txtTo_utc.getUTCDate(), txtTo_utc.getUTCHours(), txtTo_utc.getUTCMinutes(), txtTo_utc.getUTCSeconds());
                    */
                    // 1. req from india,,,,,,,,,,timezone=india to UTC
                    // 2. req from UTC to UTC(Amazon) 
                    // 3. req from india,,,,,,,,,,timezone=india(DataServer)
                    var now = new Date();

                    var utcdt = (now.getUTCDate()); //getcurrentUTCdate
                    var nowdt = (now.getDate()); //getDate
                    if (utcdt == nowdt) {//req from UTC to UTC
                        var utchr = (now.getUTCHours());
                        var nowhr = (now.getHours());
                        if (utchr == nowhr) {//date and hrs both are same so utc to utc
                            //    alert("utc to utc"); //donothing

                        }
                        else {
                            //req from India to UTC or req from India to dataserver
                            //    alert("inside utc"); //utc and current time=differ::::://do processing

                            //start
                            var serverDate = new Date('<%= DateTime.Now.ToString() %>');
                            var clientDate = new Date();
                            var diffMin = (serverDate.getTime() - clientDate.getTime()) / (1000 * 60);

                            if ((diffMin > 5) || (diffMin < -5))//request is to Amazon
                            {
                                var txtTo_utc = new Date(txtTo);
                                //txtTo = new Date(txtTo_utc.getUTCFullYear(), txtTo_utc.getUTCMonth(),
                                //txtTo_utc.getUTCDate(), txtTo_utc.getUTCHours(), txtTo_utc.getUTCMinutes(), txtTo_utc.getUTCSeconds());
                                txtTo = txtTo_utc.getUTCFullYear() + "/" + parseInt(txtTo_utc.getUTCMonth() + 1) + "/" +
                        txtTo_utc.getUTCDate() + " " + txtTo_utc.getUTCHours() + ":" + txtTo_utc.getUTCMinutes() + ":" + txtTo_utc.getUTCSeconds();

                            }
                            else {
                                //do nothing
                            }
                        }
                    } //end req from UTC to UTC
                    else {
                        //req from India to UTC or req from India to dataserver
                        //                        alert("ntdone"); //do processing
                        //start

                        var serverDate = new Date('<%= DateTime.Now.ToString() %>');
                        var clientDate = new Date();
                        var diffMin = (serverDate.getTime() - clientDate.getTime()) / (1000 * 60);

                        if ((diffMin > 5) || (diffMin < -5))//request is to Amazon
                        {
                            var txtTo_utc = new Date(txtTo);
                            //txtTo = new Date(txtTo_utc.getUTCFullYear(), txtTo_utc.getUTCMonth(),
                            //txtTo_utc.getUTCDate(), txtTo_utc.getUTCHours(), txtTo_utc.getUTCMinutes(), txtTo_utc.getUTCSeconds());
                            txtTo = txtTo_utc.getUTCFullYear() + "/" + parseInt(txtTo_utc.getUTCMonth() + 1) + "/" +
                        txtTo_utc.getUTCDate() + " " + txtTo_utc.getUTCHours() + ":" + txtTo_utc.getUTCMinutes() + ":" + txtTo_utc.getUTCSeconds();

                        }
                        else {
                            //do nothing
                        }
                    } //end req from India to UTC
                }
            }
            var act = $(".font").width();
            var reptypewidth = $("#Label9").width();
            $("#DrpCycle").css('margin-left', act - reptypewidth - 3);


            DeGrToolWebService.GenerateSumaryReport(appName, repId, LogType, txtFrom, txtTo, GenerateSumaryReport);
            //DeGrToolWebService.GetSelectedAppData(appName, SetInitialAppData);
        }

        function GenerateSumaryReport(result) {

            //start 14april
            //document.getElementById("divReport").innerHTML = result;
            //added 14april
            var content = ""; $("#divReport").html("");
            var txtFrom = ""; var txtTo = ""; var appName = ""; var LogType = ""; var douQuo = '"';

            //appName
            var e = document.getElementById("ddlAppName");
            appName = e.options[e.selectedIndex].text;
            //LogType
            var e2 = document.getElementById("ddlLogType");
            if (e2 != undefined && e2 != null) {
               
                LogType = e2.options[e2.selectedIndex].value;
            }

            if ((document.getElementById("Txtfromdate").value != "") && (document.getElementById("Txttodate").value != "")) {
                var e3 = document.getElementById("Txtfromdate").value;
                if (e3 != undefined && e3 != null && e3 != "") {
                    txtFrom = e3 + " 12:00:00 AM";
                    //added
                    /*
                    var txtFrom_utc = new Date(txtFrom);
                    txtFrom = new Date(txtFrom_utc.getUTCFullYear(), txtFrom_utc.getUTCMonth(),
                    txtFrom_utc.getUTCDate(), txtFrom_utc.getUTCHours(), txtFrom_utc.getUTCMinutes(), txtFrom_utc.getUTCSeconds());
                    */
                    //start
                    // 1. req from india,,,,,,,,,,timezone=india to UTC
                    // 2. req from UTC to UTC(Amazon) 
                    // 3. req from india,,,,,,,,,,timezone=india(DataServer)
                    var now = new Date();
                    //   alert(txtFrom);
                    var utcdt = (now.getUTCDate()); //getcurrentUTCdate
                    var nowdt = (now.getDate()); //getDate
                    if (utcdt == nowdt) {//req from UTC to UTC
                        var utchr = (now.getUTCHours());
                        var nowhr = (now.getHours());
                        if (utchr == nowhr) {//date and hrs both are same so utc to utc
                            //alert("utc to utc"); //donothing
                        }
                        else {
                            //req from India to UTC or req from India to dataserver
                            //alert("inside utc"); //utc and current time=differ::::://do processing

                            var serverDate = new Date('<%= DateTime.Now.ToString() %>');
                            var clientDate = new Date();
                            var diffMin = (serverDate.getTime() - clientDate.getTime()) / (1000 * 60);


                            if ((diffMin > 5) || (diffMin < -5))//request is to Amazon
                            {
                                var txtFrom_utc = new Date(txtFrom);
                                //alert(txtFrom);
                                //alert(txtFrom_utc);
                                txtFrom = txtFrom_utc.getUTCFullYear() + "/" + parseInt(txtFrom_utc.getUTCMonth() + 1) + "/" +
                        txtFrom_utc.getUTCDate() + " " + txtFrom_utc.getUTCHours() + ":" + txtFrom_utc.getUTCMinutes() + ":" + txtFrom_utc.getUTCSeconds();
                            }
                            else {
                                //req is to dataserver
                            }


                            //  alert(txtFrom);

                        }
                    } //end req from UTC to UTC
                    else {
                        //req from India to UTC or req from India to dataserver
                        //alert("ntdone"); //do processing

                        var serverDate = new Date('<%= DateTime.Now.ToString() %>');
                        var clientDate = new Date();
                        var diffMin = (serverDate.getTime() - clientDate.getTime()) / (1000 * 60);

                        if ((diffMin > 5) || (diffMin < -5))//request is to Amazon
                        {
                            var txtFrom_utc = new Date(txtFrom);
                            //                        txtFrom = new Date(txtFrom_utc.getUTCFullYear(), txtFrom_utc.getUTCMonth(),
                            //                        txtFrom_utc.getUTCDate(), txtFrom_utc.getUTCHours(), txtFrom_utc.getUTCMinutes(), txtFrom_utc.getUTCSeconds());
                            txtFrom = txtFrom_utc.getUTCFullYear() + "/" + parseInt(txtFrom_utc.getUTCMonth() + 1) + "/" +
                        txtFrom_utc.getUTCDate() + " " + txtFrom_utc.getUTCHours() + ":" + txtFrom_utc.getUTCMinutes() + ":" + txtFrom_utc.getUTCSeconds();
                        }
                        else {
                            //req is to dataserver
                        }



                    } //end req from India to UTC
                    //end
                }

                var e4 = document.getElementById("Txttodate").value;
                if (e4 != undefined && e4 != null && e4 != "") {
                    txtTo = e4 + " 11:59:59 PM";

                    //added
                    /*
                    var txtTo_utc = new Date(txtTo);
                    txtTo = new Date(txtTo_utc.getUTCFullYear(), txtTo_utc.getUTCMonth(),
                    txtTo_utc.getUTCDate(), txtTo_utc.getUTCHours(), txtTo_utc.getUTCMinutes(), txtTo_utc.getUTCSeconds());
                    */
                    // 1. req from india,,,,,,,,,,timezone=india to UTC
                    // 2. req from UTC to UTC(Amazon) 
                    // 3. req from india,,,,,,,,,,timezone=india(DataServer)
                    var now = new Date();

                    var utcdt = (now.getUTCDate()); //getcurrentUTCdate
                    var nowdt = (now.getDate()); //getDate
                    if (utcdt == nowdt) {//req from UTC to UTC
                        var utchr = (now.getUTCHours());
                        var nowhr = (now.getHours());
                        if (utchr == nowhr) {//date and hrs both are same so utc to utc
                            //       alert("utc to utc"); //donothing
                        }
                        else {
                            //req from India to UTC or req from India to dataserver
                            //     alert("inside utc"); //utc and current time=differ::::://do processing



                            var serverDate = new Date('<%= DateTime.Now.ToString() %>');
                            var clientDate = new Date();
                            var diffMin = (serverDate.getTime() - clientDate.getTime()) / (1000 * 60);

                            if ((diffMin > 5) || (diffMin < -5))//request is to Amazon
                            {
                                var txtTo_utc = new Date(txtTo);
                                //                            txtTo = new Date(txtTo_utc.getUTCFullYear(), txtTo_utc.getUTCMonth(),
                                //                        txtTo_utc.getUTCDate(), txtTo_utc.getUTCHours(), txtTo_utc.getUTCMinutes(), txtTo_utc.getUTCSeconds());
                                txtTo = txtTo_utc.getUTCFullYear() + "/" + parseInt(txtTo_utc.getUTCMonth() + 1) + "/" +
                        txtTo_utc.getUTCDate() + " " + txtTo_utc.getUTCHours() + ":" + txtTo_utc.getUTCMinutes() + ":" + txtTo_utc.getUTCSeconds();
                            }
                            else {
                                //do nothing
                            }


                        }
                    } //end req from UTC to UTC
                    else {
                        //req from India to UTC or req from India to dataserver
                        //                        alert("ntdone"); //do processing


                        var serverDate = new Date('<%= DateTime.Now.ToString() %>');
                        var clientDate = new Date();
                        var diffMin = (serverDate.getTime() - clientDate.getTime()) / (1000 * 60);

                        if ((diffMin > 5) || (diffMin < -5))//request is to Amazon 
                        {
                            var txtTo_utc = new Date(txtTo);
                            //txtTo = new Date(txtTo_utc.getUTCFullYear(), txtTo_utc.getUTCMonth(),
                            //txtTo_utc.getUTCDate(), txtTo_utc.getUTCHours(), txtTo_utc.getUTCMinutes(), txtTo_utc.getUTCSeconds());
                            txtTo = txtTo_utc.getUTCFullYear() + "/" + parseInt(txtTo_utc.getUTCMonth() + 1) + "/" +
                        txtTo_utc.getUTCDate() + " " + txtTo_utc.getUTCHours() + ":" + txtTo_utc.getUTCMinutes() + ":" + txtTo_utc.getUTCSeconds();
                        }
                        else {
                            //do nothing
                        }

                    } //end req from India to UTC
                }
            }
            else if ((document.getElementById("Txtweeklyfrom").value != "") && (document.getElementById("TxtweeklyTo").value != "")) {
                var e5 = document.getElementById("Txtweeklyfrom").value;
                if (e5 != undefined && e5 != null && e5 != "") {
                    txtFrom = e5 + " 12:00:00 AM";

                    //added
                    /*
                    var txtFrom_utc = new Date(txtFrom);
                    txtFrom = new Date(txtFrom_utc.getUTCFullYear(), txtFrom_utc.getUTCMonth(),
                    txtFrom_utc.getUTCDate(), txtFrom_utc.getUTCHours(), txtFrom_utc.getUTCMinutes(), txtFrom_utc.getUTCSeconds());
                    */
                    // 1. req from india,,,,,,,,,,timezone=india to UTC
                    // 2. req from UTC to UTC(Amazon) 
                    // 3. req from india,,,,,,,,,,timezone=india(DataServer)
                    var now = new Date();

                    var utcdt = (now.getUTCDate()); //getcurrentUTCdate
                    var nowdt = (now.getDate()); //getDate
                    if (utcdt == nowdt) {//req from UTC to UTC
                        var utchr = (now.getUTCHours());
                        var nowhr = (now.getHours());
                        if (utchr == nowhr) {//date and hrs both are same so utc to utc
                            //                            alert("utc to utc"); //donothing
                        }
                        else {
                            //req from India to UTC or req from India to dataserver
                            //                          alert("inside utc"); //utc and current time=differ::::://do processing

                            //start

                            var serverDate = new Date('<%= DateTime.Now.ToString() %>');
                            var clientDate = new Date();
                            var diffMin = (serverDate.getTime() - clientDate.getTime()) / (1000 * 60);

                            if ((diffMin > 5) || (diffMin < -5))//request is to Amazon
                            {
                                var txtFrom_utc = new Date(txtFrom);
                                //   txtFrom = new Date(txtFrom_utc.getUTCFullYear(), txtFrom_utc.getUTCMonth(),
                                //txtFrom_utc.getUTCDate(), txtFrom_utc.getUTCHours(), txtFrom_utc.getUTCMinutes(), txtFrom_utc.getUTCSeconds());
                                txtFrom = txtFrom_utc.getUTCFullYear() + "/" + parseInt(txtFrom_utc.getUTCMonth() + 1) + "/" +
                        txtFrom_utc.getUTCDate() + " " + txtFrom_utc.getUTCHours() + ":" + txtFrom_utc.getUTCMinutes() + ":" + txtFrom_utc.getUTCSeconds();

                            }
                            else {
                                //do nothing
                            }


                        }
                    } //end req from UTC to UTC
                    else {
                        //req from India to UTC or req from India to dataserver
                        //                        alert("ntdone"); //do processing

                        //start
                        var serverDate = new Date('<%= DateTime.Now.ToString() %>');
                        var clientDate = new Date();
                        var diffMin = (serverDate.getTime() - clientDate.getTime()) / (1000 * 60);

                        if ((diffMin > 5) || (diffMin < -5))//request is to Amazon
                        {
                            var txtFrom_utc = new Date(txtFrom);
                            // txtFrom = new Date(txtFrom_utc.getUTCFullYear(), txtFrom_utc.getUTCMonth(),
                            //txtFrom_utc.getUTCDate(), txtFrom_utc.getUTCHours(), txtFrom_utc.getUTCMinutes(), txtFrom_utc.getUTCSeconds());
                            txtFrom = txtFrom_utc.getUTCFullYear() + "/" + parseInt(txtFrom_utc.getUTCMonth() + 1) + "/" +
                        txtFrom_utc.getUTCDate() + " " + txtFrom_utc.getUTCHours() + ":" + txtFrom_utc.getUTCMinutes() + ":" + txtFrom_utc.getUTCSeconds();

                        }
                        else {
                            //do nothing
                        }
                    } //end req from India to UTC
                }
                var e6 = document.getElementById("TxtweeklyTo").value;
                if (e6 != undefined && e6 != null && e6 != "") {
                    txtTo = e6 + " 11:59:59 PM";
                    //added
                    /*
                    var txtTo_utc = new Date(txtTo);
                    txtTo = new Date(txtTo_utc.getUTCFullYear(), txtTo_utc.getUTCMonth(),
                    txtTo_utc.getUTCDate(), txtTo_utc.getUTCHours(), txtTo_utc.getUTCMinutes(), txtTo_utc.getUTCSeconds());
                    */
                    // 1. req from india,,,,,,,,,,timezone=india to UTC
                    // 2. req from UTC to UTC(Amazon) 
                    // 3. req from india,,,,,,,,,,timezone=india(DataServer)
                    var now = new Date();

                    var utcdt = (now.getUTCDate()); //getcurrentUTCdate
                    var nowdt = (now.getDate()); //getDate
                    if (utcdt == nowdt) {//req from UTC to UTC
                        var utchr = (now.getUTCHours());
                        var nowhr = (now.getHours());
                        if (utchr == nowhr) {//date and hrs both are same so utc to utc
                            //    alert("utc to utc"); //donothing

                        }
                        else {
                            //req from India to UTC or req from India to dataserver
                            //    alert("inside utc"); //utc and current time=differ::::://do processing

                            //start
                            var serverDate = new Date('<%= DateTime.Now.ToString() %>');
                            var clientDate = new Date();
                            var diffMin = (serverDate.getTime() - clientDate.getTime()) / (1000 * 60);

                            if ((diffMin > 5) || (diffMin < -5))//request is to Amazon
                            {
                                var txtTo_utc = new Date(txtTo);
                                //txtTo = new Date(txtTo_utc.getUTCFullYear(), txtTo_utc.getUTCMonth(),
                                //txtTo_utc.getUTCDate(), txtTo_utc.getUTCHours(), txtTo_utc.getUTCMinutes(), txtTo_utc.getUTCSeconds());
                                txtTo = txtTo_utc.getUTCFullYear() + "/" + parseInt(txtTo_utc.getUTCMonth() + 1) + "/" +
                        txtTo_utc.getUTCDate() + " " + txtTo_utc.getUTCHours() + ":" + txtTo_utc.getUTCMinutes() + ":" + txtTo_utc.getUTCSeconds();

                            }
                            else {
                                //do nothing
                            }
                        }
                    } //end req from UTC to UTC
                    else {
                        //req from India to UTC or req from India to dataserver
                        //                        alert("ntdone"); //do processing
                        //start

                        var serverDate = new Date('<%= DateTime.Now.ToString() %>');
                        var clientDate = new Date();
                        var diffMin = (serverDate.getTime() - clientDate.getTime()) / (1000 * 60);

                        if ((diffMin > 5) || (diffMin < -5))//request is to Amazon
                        {
                            var txtTo_utc = new Date(txtTo);
                            //txtTo = new Date(txtTo_utc.getUTCFullYear(), txtTo_utc.getUTCMonth(),
                            //txtTo_utc.getUTCDate(), txtTo_utc.getUTCHours(), txtTo_utc.getUTCMinutes(), txtTo_utc.getUTCSeconds());
                            txtTo = txtTo_utc.getUTCFullYear() + "/" + parseInt(txtTo_utc.getUTCMonth() + 1) + "/" +
                        txtTo_utc.getUTCDate() + " " + txtTo_utc.getUTCHours() + ":" + txtTo_utc.getUTCMinutes() + ":" + txtTo_utc.getUTCSeconds();

                        }
                        else {
                            //do nothing
                        }
                    } //end req from India to UTC
                }
            }

            if (result != "" && result != null) {
                var obj = JSON.parse(result);
                var schedulerows = parseInt(obj.length);


                content += "<table align='left' id='myTable' style='width: 60%; border-collapse: collapse;'>";
                content += "<thead><tr style='height: 25px; background-color: #0e4366; position: relative;font-size: 17px;'>";
                content += "<th style='color: White;'>No.</th>";
                content += "<th style='color: White;'>User Id</th>";
                content += "<th style='color: White;'>Sync</th>";
                content += "<th style='color: White;'>Launch</th>";
                content += "<th style='color: White;'>Last Activity</th>";
                content += "<th style='color: White;'></th></thead>";
                content += "</tr><tbody>";

                for (var k = 0; k < schedulerows; k++) {
                    content += "<tr style='border: 1px solid #D5D4D4;font-family: Verdana;font-size: 13px;text-align: left;height: 25px;'>";
                    content += "<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + obj[k].No + "</td>";
                    content += "<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + obj[k].RepId + "</td>";
                    content += "<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: center;'>" + obj[k].Stype + "</td>"; //for Launch

                    content += "<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: center;'>" + obj[k].Ltype + "</td>"; //for Launch
                    //start
                    var now = new Date();

                    var utcdt = (now.getUTCDate()); //getcurrentUTCdate
                    var nowdt = (now.getDate()); //getDate
                    if (utcdt == nowdt) {//req from UTC to UTC

                        var utchr = (now.getUTCHours()); //returns the hour (from 0 to 23)
                        var nowhr = (now.getHours()); //returns the hour (from 0 to 23)

                        if (utchr == nowhr) {
                            //date and hrs of UTC and client ,both are same so utc to utc



                            var res;
                            var str = obj[k].LastActTime;
                            if (str.indexOf('-') > 0) {

                                res = obj[k].LastActTime.replace(/-/g, "/");

                                if (str.indexOf('T') > 0) {
                                    //      alert("Found T");
                                    res = res.replace(/T/g, " ");
                                }
                            }
                            //  alert("res1 :"+ res);
                            else if (str.indexOf('T') > 0) {
                                //alert("Found T");
                                res = obj[k].LastActTime.replace(/T/g, " ");
                            }
                            //2014/02/24 04:43:40.97

                            if (str.indexOf('.') > 0) {
                                res = res.substring(0, str.indexOf('.'));
                            }
                            // alert(res);
                            var today = new Date(res);

                            var dd = today.getDate();

                            var mm = today.getMonth() + 1; //January is 0!

                            var yyyy = today.getFullYear();

                            var hh = today.getHours();
                            var mins = today.getMinutes();
                            var secs = today.getSeconds();

                            if (dd < 10) {
                                dd = '0' + dd
                            }
                            if (mm < 10) {
                                mm = '0' + mm
                            }

                            if (hh < 10) {
                                hh = '0' + hh
                            }
                            if (mins < 10) {
                                mins = '0' + mins
                            }
                            if (secs < 10) {
                                secs = '0' + secs
                            }

                            today = mm + '/' + dd + '/' + yyyy + " " + hh + ":" + mins + ":" + secs;
                            //end
                            //content += ("<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + today + "</td>");
                            content += "<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + today + "</td>"; //for LastActTime
                        }
                        else {
                            //date of UTC and Client are same BUT hours are different

                            //req from India to UTC or req from India to dataserver
                            //   alert("inside utc"); //utc and current time=differ::::://do processing
                            //start
                            var serverDate = new Date('<%= DateTime.Now.ToString() %>');
                            var clientDate = new Date();
                            var diffMin = (serverDate.getTime() - clientDate.getTime()) / (1000 * 60);


                            if ((diffMin > 5) || (diffMin < -5))//request is to Amazon
                            {
                                //req from otherCountry to UTC

                                // var date = convertUTCDateToLocalDate(new Date(obj[i].LogDateTime));
                                //  content += ("<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + date.toLocaleString() + "</td>");
                                //  var logdatetime = new Date(obj[i].LogDateTime+"Z");
                                // if (logdatetime == "NaN") {
                                var res;
                                var str = obj[k].LastActTime;
                                if (str.indexOf('-') > 0) {
                                    //  alert("Found -");
                                    res = obj[k].LastActTime.replace(/-/g, "/");

                                    if (str.indexOf('T') > 0) {
                                        //      alert("Found T");
                                        res = res.replace(/T/g, " ");
                                    }
                                }
                                //  alert("res1 :"+ res);
                                else if (str.indexOf('T') > 0) {
                                    //alert("Found T");
                                    res = obj[k].LastActTime.replace(/T/g, " ");
                                }
                                //2014/02/24 04:43:40.97

                                if (str.indexOf('.') > 0) {
                                    res = res.substring(0, str.indexOf('.'));
                                }
                                // alert(res);
                                var ldate = convertUTCDateToLocalDate(new Date(res)); //(new Date(res));
                                //content += ("<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + ldate + "</td>");
                                content += "<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + ldate + "</td>"; //for LastActTime
                                //   }
                                //    else {
                                //    var date = convertUTCDateToLocalDate(new Date(obj[i].LogDateTime));
                                //   content += ("<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + date.toLocaleString() + "</td>");
                                //    }
                            }
                            else {
                                //do nothing::::::sameTimeZone
                                //req from India to DataServer
                                // alert("here");
                                var res;
                                var str = obj[k].LastActTime;
                                if (str.indexOf('-') > 0) {
                                    //  alert("Found -");
                                    res = obj[k].LastActTime.replace(/-/g, "/");

                                    if (str.indexOf('T') > 0) {
                                        //      alert("Found T");
                                        res = res.replace(/T/g, " ");
                                    }
                                }
                                //  alert("res1 :"+ res);
                                else if (str.indexOf('T') > 0) {
                                    //alert("Found T");
                                    res = obj[k].LastActTime.replace(/T/g, " ");
                                }
                                //2014/02/24 04:43:40.97

                                if (str.indexOf('.') > 0) {
                                    res = res.substring(0, str.indexOf('.'));
                                }
                                //   alert(res);
                                var today = new Date(res);
                                //   alert(res);
                                //   alert(today);
                                //  alert(new Date(obj[i].LogDateTime));
                                var dd = today.getDate();

                                var mm = today.getMonth() + 1; //January is 0!

                                var yyyy = today.getFullYear();

                                var hh = today.getHours();
                                var mins = today.getMinutes();
                                var secs = today.getSeconds();
                                //   alert(dd + " " + mm + " " + yyyy);
                                // var hh = today.get
                                if (dd < 10) {
                                    dd = '0' + dd
                                }
                                if (mm < 10) {
                                    mm = '0' + mm
                                }

                                if (hh < 10) {
                                    hh = '0' + hh
                                }
                                if (mins < 10) {
                                    mins = '0' + mins
                                }
                                if (secs < 10) {
                                    secs = '0' + secs
                                }

                                today = mm + '/' + dd + '/' + yyyy + " " + hh + ":" + mins + ":" + secs;
                                if (today == undefined || today == NaN) {
                                    today = obj[k].LastActTime;
                                }
                                //content += ("<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + today + "</td>");
                                content += "<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + today + "</td>"; //for LastActTime
                            }
                        }
                    } //end req from UTC to UTC
                    else {
                        //UTC date and Client Date is not same

                        //req from India to UTC or req from India to dataserver
                        //   alert("ntdone"); //do processing

                        //start
                        var serverDate = new Date('<%= DateTime.Now.ToString() %>');
                        var clientDate = new Date();
                        var diffMin = (serverDate.getTime() - clientDate.getTime()) / (1000 * 60);

                        if ((diffMin > 5) || (diffMin < -5))//request is from otherCountry to Amazon
                        {

                            //  var date = convertUTCDateToLocalDate(new Date(obj[i].LogDateTime));
                            //  content += ("<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + date.toLocaleString() + "</td>");
                            //    var logdatetime = new Date(obj[i].LogDateTime+"Z");
                            //     if (logdatetime == "NaN") {
                            var res;
                            var str = obj[k].LastActTime;
                            if (str.indexOf('-') > 0) {
                                //  alert("Found -");
                                res = obj[k].LastActTime.replace(/-/g, "/");

                                if (str.indexOf('T') > 0) {
                                    //      alert("Found T");
                                    res = res.replace(/T/g, " ");
                                }
                            }
                            //  alert("res1 :"+ res);
                            else if (str.indexOf('T') > 0) {
                                //alert("Found T");
                                res = obj[k].LastActTime.replace(/T/g, " ");
                            }
                            //2014/02/24 04:43:40.97

                            if (str.indexOf('.') > 0) {
                                res = res.substring(0, str.indexOf('.'));
                            }
                            // alert(res);
                            var ldate = convertUTCDateToLocalDate(new Date(res)); //(new Date(res));
                            //content += ("<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + ldate + "</td>");
                            content += "<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + ldate + "</td>"; //for LastActTime
                            //   }
                            //  else {
                            //      var date = convertUTCDateToLocalDate(new Date(obj[i].LogDateTime));
                            //      content += ("<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + date.toLocaleString() + "</td>");
                            //  }
                        }
                        else {
                            //do nothing-----sameTimeZone
                            //  alert("yep");
                            //  var res = obj[i].LogDateTime.replace(/-/g, "/");
                            //   var res = obj[i].LogDateTime.replace(/T/g, " ");
                            var res;
                            var str = obj[k].LastActTime;
                            if (str.indexOf('-') > 0) {
                                //  alert("Found -");
                                res = obj[k].LastActTime.replace(/-/g, "/");

                                if (str.indexOf('T') > 0) {
                                    //      alert("Found T");
                                    res = res.replace(/T/g, " ");
                                }
                            }
                            //  alert("res1 :"+ res);
                            else if (str.indexOf('T') > 0) {
                                //alert("Found T");
                                res = obj[k].LastActTime.replace(/T/g, " ");
                            }
                            //2014/02/24 04:43:40.97

                            if (str.indexOf('.') > 0) {
                                res = res.substring(0, str.indexOf('.'));
                            }
                            // alert(res);
                            var today = new Date(res);
                            //   alert(res);
                            //   alert(today);
                            //  alert(new Date(obj[i].LogDateTime));
                            var dd = today.getDate();

                            var mm = today.getMonth() + 1; //January is 0!

                            var yyyy = today.getFullYear();

                            var hh = today.getHours();
                            var mins = today.getMinutes();
                            var secs = today.getSeconds();
                            //   alert(dd + " " + mm + " " + yyyy);
                            // var hh = today.get
                            if (dd < 10) {
                                dd = '0' + dd
                            }
                            if (mm < 10) {
                                mm = '0' + mm
                            }

                            if (hh < 10) {
                                hh = '0' + hh
                            }
                            if (mins < 10) {
                                mins = '0' + mins
                            }
                            if (secs < 10) {
                                secs = '0' + secs
                            }

                            today = mm + '/' + dd + '/' + yyyy + " " + hh + ":" + mins + ":" + secs;
                            //content += ("<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + today + "</td>");
                            content += "<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + today + "</td>"; //for LastActTime

                            
                        
                        
                        }
                    } //end req from India to UTC
                    //end
                    //content += "<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'><a href=\"javascript:void(0)\" onclick=" + "javascript:getReport('" + appName + "', '" + obj[k].RepId + "','" + LogType + "', '" + txtFrom + "','" + txtTo + "')" + ">ViewDetail </a></td>";
                    content += "<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'> <a href=\"javascript:void(0)\" onclick=" + douQuo + "javascript:getReport('" + appName + "', '" + obj[k].RepId + "','" + LogType + "', '" + txtFrom + "','" + txtTo + "')" + douQuo + ">ViewDetail </a> </td>";
                    content += "</tr>";
                }
                content += "</tbody></table>";
                content += "</br>";
                document.getElementById("divReport").innerHTML = content;
            }
            var t = new SortableTable(document.getElementById('myTable'), 100);
            //end 14april
            

        }
    </script>
    <!-- getReport -->
    <script language="javascript" type="text/javascript">
        function getReport(appName, repId, LogType, txtFrom, txtTo) {

            DeGrToolWebService.getReport(appName, repId, LogType, txtFrom, txtTo, generateReport);
            //DeGrToolWebService.GetSelectedAppData(appName, SetInitialAppData);
        }

        function convertUTCDateToLocalDate1(date) {
            var newDate = new Date(date.getTime());

            var offset = date.getTimezoneOffset() / 60;
            var hours = date.getHours();

            newDate.setHours(hours - offset);

            return newDate;
        }

        function convertUTCDateToLocalDate(utc) {
            // Create a local date from the UTC string
            var t = new Date(Number(utc));

            // Get the offset in ms
            var offset = t.getTimezoneOffset() * 60000;

            // Subtract from the UTC time to get local
            t.setTime(t.getTime() - offset);

            // do whatever
            var d = [t.getFullYear(), t.getMonth() + 1, t.getDate()].join('/');
            d += ' ' + t.toLocaleTimeString();
            return d;
        }

        function generateReport(result) {
            //  alert(result);


            //alert(schedulerows);

            var content = ""; $("#divReportDetail").html("");
            if (result != "" && result != null) {
                var obj = JSON.parse(result);
                var schedulerows = parseInt(obj.length);


                content = "<table align='center' style=' border-collapse: collapse;'>";

                content += ("<tr style='height: 25px; background-color: #0e4366; position: relative;font-size: 14px;'>");

                content += ("<th style='color: White;text-align: center;'>No.</th>");
                content += ("<th style='color: White;text-align: center;'>User Id</th>");
                content += ("<th style='color: White;text-align: center;'>Sync</th>");
                content += ("<th style='color: White;text-align: center;'>Launch</th>");
                content += ("<th style='color: White;text-align: center;'>DateTime</th>");
                var cnt = 0; var syncCount = 0; var lCount = 0;
                for (var i = 0; i < schedulerows; i++) {
                    content += ("<tr style='border: 1px solid #D5D4D4;font-family: Verdana;font-size: 13px;text-align: left;height: 25px;'>");

                    content += ("<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + obj[i].No + "</td>");

                    if (cnt == 0) {
                        content += ("<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + obj[i].RepId + "</td>");

                        cnt++;
                    }
                    else {


                        content += "<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;font-weight:700;'>&nbsp;</td>";
                    }
                    // alert(obj[i].LogType);

                    if (obj[i].LogType == "S") {
                        syncCount++;
                        content += "<td align='center'><img src='Check-icon.png' width='10px'></img></td>"; //for synch
                        content += "<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'></td>"; //for Launch
                    }
                    else if (obj[i].LogType == "L") {
                        lCount++;
                        content += "<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'></td>"; //for synch
                        content += "<td align='center'><img  src='Check-icon.png' width='10px'></img></td>"; //for Launch
                    }


                    {
                        // 1. req from india,,,,,,,,,,timezone=india to UTC
                        // 2. req from UTC to UTC(Amazon) 
                        // 3. req from india,,,,,,,,,,timezone=india(DataServer)
                        var now = new Date();

                        var utcdt = (now.getUTCDate()); //getcurrentUTCdate
                        var nowdt = (now.getDate()); //getDate
                        if (utcdt == nowdt) {//req from UTC to UTC

                            var utchr = (now.getUTCHours()); //returns the hour (from 0 to 23)
                            var nowhr = (now.getHours()); //returns the hour (from 0 to 23)
                            // alert("time at utc:"+utchr+" time  here:"+nowhr);
                            if (utchr == nowhr) {
                                //date and hrs of UTC and client ,both are same so utc to utc
                                //     alert("utc to utc"); //donothing

                                // var date = convertUTCDateToLocalDate(new Date(obj[i].LogDateTime));
                                // var today = new Date(obj[i].LogDateTime);

                                // var dd = today.getDate();
                                // var mm = today.getMonth() + 1; //January is 0!

                                //  var yyyy = today.getFullYear();


                                // if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm }
                                // today = mm + '/' + dd + '/' + yyyy + " " + today.getHours() + ":" + today.getMinutes() + ":"
                                //+ today.getSeconds();

                                //start
                                var res;
                                var str = obj[i].LogDateTime;
                                if (str.indexOf('-') > 0) {
                                    //  alert("Found -");
                                    res = obj[i].LogDateTime.replace(/-/g, "/");

                                    if (str.indexOf('T') > 0) {
                                        //      alert("Found T");
                                        res = res.replace(/T/g, " ");
                                    }
                                }
                                //  alert("res1 :"+ res);
                                else if (str.indexOf('T') > 0) {
                                    //alert("Found T");
                                    res = obj[i].LogDateTime.replace(/T/g, " ");
                                }
                                //2014/02/24 04:43:40.97

                                if (str.indexOf('.') > 0) {
                                    res = res.substring(0, str.indexOf('.'));
                                }
                                // alert(res);
                                var today = new Date(res);
                                // alert("today2222 :" + today);
                                //    alert(res);
                                //    alert(today);
                                //   alert(new Date(obj[i].LogDateTime));
                                var dd = today.getDate();

                                var mm = today.getMonth() + 1; //January is 0!

                                var yyyy = today.getFullYear();

                                var hh = today.getHours();
                                var mins = today.getMinutes();
                                var secs = today.getSeconds();
                                //   alert(dd + " " + mm + " " + yyyy);
                                // var hh = today.get
                                if (dd < 10) {
                                    dd = '0' + dd
                                }
                                if (mm < 10) {
                                    mm = '0' + mm
                                }

                                if (hh < 10) {
                                    hh = '0' + hh
                                }
                                if (mins < 10) {
                                    mins = '0' + mins
                                }
                                if (secs < 10) {
                                    secs = '0' + secs
                                }

                                today = mm + '/' + dd + '/' + yyyy + " " + hh + ":" + mins + ":" + secs;
                                //end
                                content += ("<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + today + "</td>");

                            }
                            else {
                                //date of UTC and Client are same BUT hours are different

                                //req from India to UTC or req from India to dataserver
                                //   alert("inside utc"); //utc and current time=differ::::://do processing
                                //start
                                var serverDate = new Date('<%= DateTime.Now.ToString() %>');
                                var clientDate = new Date();
                                var diffMin = (serverDate.getTime() - clientDate.getTime()) / (1000 * 60);


                                if ((diffMin > 5) || (diffMin < -5))//request is to Amazon
                                {
                                    //req from otherCountry to UTC

                                    // var date = convertUTCDateToLocalDate(new Date(obj[i].LogDateTime));
                                    //  content += ("<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + date.toLocaleString() + "</td>");
                                    //  var logdatetime = new Date(obj[i].LogDateTime+"Z");
                                    // if (logdatetime == "NaN") {
                                    var res;
                                    var str = obj[i].LogDateTime;
                                    if (str.indexOf('-') > 0) {
                                        //  alert("Found -");
                                        res = obj[i].LogDateTime.replace(/-/g, "/");

                                        if (str.indexOf('T') > 0) {
                                            //      alert("Found T");
                                            res = res.replace(/T/g, " ");
                                        }
                                    }
                                    //  alert("res1 :"+ res);
                                    else if (str.indexOf('T') > 0) {
                                        //alert("Found T");
                                        res = obj[i].LogDateTime.replace(/T/g, " ");
                                    }
                                    //2014/02/24 04:43:40.97

                                    if (str.indexOf('.') > 0) {
                                        res = res.substring(0, str.indexOf('.'));
                                    }
                                    // alert(res);
                                    var ldate = convertUTCDateToLocalDate(new Date(res)); //(new Date(res));
                                    content += ("<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + ldate + "</td>");
                                    //   }
                                    //    else {
                                    //    var date = convertUTCDateToLocalDate(new Date(obj[i].LogDateTime));
                                    //   content += ("<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + date.toLocaleString() + "</td>");
                                    //    }
                                }
                                else {
                                    //do nothing::::::sameTimeZone
                                    //req from India to DataServer
                                    // alert("here");
                                    var res;
                                    var str = obj[i].LogDateTime;
                                    if (str.indexOf('-') > 0) {
                                        //  alert("Found -");
                                        res = obj[i].LogDateTime.replace(/-/g, "/");

                                        if (str.indexOf('T') > 0) {
                                            //      alert("Found T");
                                            res = res.replace(/T/g, " ");
                                        }
                                    }
                                    //  alert("res1 :"+ res);
                                    else if (str.indexOf('T') > 0) {
                                        //alert("Found T");
                                        res = obj[i].LogDateTime.replace(/T/g, " ");
                                    }
                                    //2014/02/24 04:43:40.97

                                    if (str.indexOf('.') > 0) {
                                        res = res.substring(0, str.indexOf('.'));
                                    }
                                    //   alert(res);
                                    var today = new Date(res);
                                    //   alert(res);
                                    //   alert(today);
                                    //  alert(new Date(obj[i].LogDateTime));
                                    var dd = today.getDate();

                                    var mm = today.getMonth() + 1; //January is 0!

                                    var yyyy = today.getFullYear();

                                    var hh = today.getHours();
                                    var mins = today.getMinutes();
                                    var secs = today.getSeconds();
                                    //   alert(dd + " " + mm + " " + yyyy);
                                    // var hh = today.get
                                    if (dd < 10) {
                                        dd = '0' + dd
                                    }
                                    if (mm < 10) {
                                        mm = '0' + mm
                                    }

                                    if (hh < 10) {
                                        hh = '0' + hh
                                    }
                                    if (mins < 10) {
                                        mins = '0' + mins
                                    }
                                    if (secs < 10) {
                                        secs = '0' + secs
                                    }

                                    today = mm + '/' + dd + '/' + yyyy + " " + hh + ":" + mins + ":" + secs;
                                    if (today == undefined || today == NaN) {
                                        today = obj[i].LogDateTime;
                                    }
                                    content += ("<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + today + "</td>");
                                }
                            }
                        } //end req from UTC to UTC
                        else {
                            //UTC date and Client Date is not same

                            //req from India to UTC or req from India to dataserver
                            //   alert("ntdone"); //do processing

                            //start
                            var serverDate = new Date('<%= DateTime.Now.ToString() %>');
                            var clientDate = new Date();
                            var diffMin = (serverDate.getTime() - clientDate.getTime()) / (1000 * 60);

                            if ((diffMin > 5) || (diffMin < -5))//request is from otherCountry to Amazon
                            {

                                //  var date = convertUTCDateToLocalDate(new Date(obj[i].LogDateTime));
                                //  content += ("<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + date.toLocaleString() + "</td>");
                                //    var logdatetime = new Date(obj[i].LogDateTime+"Z");
                                //     if (logdatetime == "NaN") {
                                var res;
                                var str = obj[i].LogDateTime;
                                if (str.indexOf('-') > 0) {
                                    //  alert("Found -");
                                    res = obj[i].LogDateTime.replace(/-/g, "/");

                                    if (str.indexOf('T') > 0) {
                                        //      alert("Found T");
                                        res = res.replace(/T/g, " ");
                                    }
                                }
                                //  alert("res1 :"+ res);
                                else if (str.indexOf('T') > 0) {
                                    //alert("Found T");
                                    res = obj[i].LogDateTime.replace(/T/g, " ");
                                }
                                //2014/02/24 04:43:40.97

                                if (str.indexOf('.') > 0) {
                                    res = res.substring(0, str.indexOf('.'));
                                }
                                // alert(res);
                                var ldate = convertUTCDateToLocalDate(new Date(res)); //(new Date(res));
                                content += ("<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + ldate + "</td>");
                                //   }
                                //  else {
                                //      var date = convertUTCDateToLocalDate(new Date(obj[i].LogDateTime));
                                //      content += ("<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + date.toLocaleString() + "</td>");
                                //  }
                            }
                            else {
                                //do nothing-----sameTimeZone
                                //  alert("yep");
                                //  var res = obj[i].LogDateTime.replace(/-/g, "/");
                                //   var res = obj[i].LogDateTime.replace(/T/g, " ");
                                var res;
                                var str = obj[i].LogDateTime;
                                if (str.indexOf('-') > 0) {
                                    //  alert("Found -");
                                    res = obj[i].LogDateTime.replace(/-/g, "/");

                                    if (str.indexOf('T') > 0) {
                                        //      alert("Found T");
                                        res = res.replace(/T/g, " ");
                                    }
                                }
                                //  alert("res1 :"+ res);
                                else if (str.indexOf('T') > 0) {
                                    //alert("Found T");
                                    res = obj[i].LogDateTime.replace(/T/g, " ");
                                }
                                //2014/02/24 04:43:40.97

                                if (str.indexOf('.') > 0) {
                                    res = res.substring(0, str.indexOf('.'));
                                }
                                // alert(res);
                                var today = new Date(res);
                                //   alert(res);
                                //   alert(today);
                                //  alert(new Date(obj[i].LogDateTime));
                                var dd = today.getDate();

                                var mm = today.getMonth() + 1; //January is 0!

                                var yyyy = today.getFullYear();

                                var hh = today.getHours();
                                var mins = today.getMinutes();
                                var secs = today.getSeconds();
                                //   alert(dd + " " + mm + " " + yyyy);
                                // var hh = today.get
                                if (dd < 10) {
                                    dd = '0' + dd
                                }
                                if (mm < 10) {
                                    mm = '0' + mm
                                }

                                if (hh < 10) {
                                    hh = '0' + hh
                                }
                                if (mins < 10) {
                                    mins = '0' + mins
                                }
                                if (secs < 10) {
                                    secs = '0' + secs
                                }

                                today = mm + '/' + dd + '/' + yyyy + " " + hh + ":" + mins + ":" + secs;
                                content += ("<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'>" + today + "</td>");
                            }
                        } //end req from India to UTC

                    }
                    content += ("</tr>");

                    //   alert(content);
                } //end for
                content += "<tr><td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'></td>";
                content += "<td  style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: center;font-weight:bold;'>" + syncCount + "</td>";
                content += "<td  style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: center;font-weight:bold;'>" + lCount + "</td>";
                content += "<td style='border: 1px solid #D5D4D4;font-family: Verdana; font-size: 13px;text-align: left;'></td>";
                content += "</tr>";
                content += "</table>";
                $("#divReportDetail").append(content);

            } //end result
            else {

                $("#divReportDetail").append("<center>No Data To Show</center>");
            }

            tb_show("", "#TB_inline?width=480&height=auto&inlineId=divReportDetail", "");


            return false;
        }
    </script>
    <script language="javascript" type="text/javascript">
        function KeyPressforMainScreen(e) {
            try {
                var e = event || evt; // for trans-browser compatibility
                var charCode = e.which || e.keyCode;


                if (charCode == 13) {
                    return false;
                }
            }
            catch (err) {
                alert(err.Message);
            }
        }
    </script>
    <script src="Thickbox/jquery-1.2.6.min.js" type="text/javascript"></script>
    <script src="Thickbox/thickbox-compressed.js" type="text/javascript"></script>
    <script src="Javascript/jquery_1.9.0.min.js" type="text/javascript"></script>
    <script src="Javascript/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
    <link href="css/smoothness/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <%-- <script src="Javascript/jquery_1.9.0.min.js" type="text/javascript"></script>  --%>
    <link href="Thickbox/thickbox.css" rel="stylesheet" type="text/css" />
    <%--  <script src="Thickbox/thickbox-compressed.js" type="text/javascript"></script>--%>
    <script src="Thickbox/thickbox.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">

        function pageLoad(sender, args) {
            if (args.get_isPartialLoad()) {
                //  reapply the thick box stuff
                //   tb_init('a.thickbox');
                //   alert(sender);
                //    alert(args);
                $("#Txtfromdate").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'mm/dd/yy'


                });
                $("#Txtweeklyfrom").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'mm/dd/yy'

                });

            }

        }
       
    </script>

    <script>
        /**
        *
        *  Sortable HTML table
        *  http://www.webtoolkit.info/
        *
        **/
        function SortableTable(tableEl) {
            this.tbody = tableEl.getElementsByTagName('tbody');
            this.thead = tableEl.getElementsByTagName('thead');
            this.tfoot = tableEl.getElementsByTagName('tfoot');
            this.getInnerText = function (el) {
                if (typeof (el.textContent) != 'undefined') return el.textContent;
                if (typeof (el.innerText) != 'undefined') return el.innerText;
                if (typeof (el.innerHTML) == 'string') return el.innerHTML.replace(/<[^<>]+>/g, '');
            }
            this.getParent = function (el, pTagName) {
                if (el == null) return null;
                else if (el.nodeType == 1 && el.tagName.toLowerCase() == pTagName.toLowerCase())
                    return el;
                else
                    return this.getParent(el.parentNode, pTagName);
            }
            this.sort = function (cell) {
                var column = cell.cellIndex;
                var itm = this.getInnerText(this.tbody[0].rows[1].cells[column]);
                var sortfn = this.sortCaseInsensitive;
                if (itm.match(/\d\d[-]+\d\d[-]+\d\d\d\d/)) sortfn = this.sortDate; // date format mm-dd-yyyy
                if (itm.replace(/^\s+|\s+$/g, "").match(/^[\d\.]+$/)) sortfn = this.sortNumeric;
                this.sortColumnIndex = column;
                var newRows = new Array();
                for (j = 0; j < this.tbody[0].rows.length; j++) {
                    newRows[j] = this.tbody[0].rows[j];
                }
                newRows.sort(sortfn);
                if (cell.getAttribute("sortdir") == 'down') {
                    newRows.reverse();
                    cell.setAttribute('sortdir', 'up');
                } else {
                    cell.setAttribute('sortdir', 'down');
                }
                for (i = 0; i < newRows.length; i++) {
                    this.tbody[0].appendChild(newRows[i]);
                }
            }
            this.sortCaseInsensitive = function (a, b) {
                aa = thisObject.getInnerText(a.cells[thisObject.sortColumnIndex]).toLowerCase();
                bb = thisObject.getInnerText(b.cells[thisObject.sortColumnIndex]).toLowerCase();
                if (aa == bb) return 0;
                if (aa < bb) return -1;
                return 1;
            }
            this.sortDate = function (a, b) {
                aa = thisObject.getInnerText(a.cells[thisObject.sortColumnIndex]);
                bb = thisObject.getInnerText(b.cells[thisObject.sortColumnIndex]);
                date1 = aa.substr(6, 4) + aa.substr(3, 2) + aa.substr(0, 2);
                date2 = bb.substr(6, 4) + bb.substr(3, 2) + bb.substr(0, 2);
                if (date1 == date2) return 0;
                if (date1 < date2) return -1;
                return 1;
            }
            this.sortNumeric = function (a, b) {
                aa = parseFloat(thisObject.getInnerText(a.cells[thisObject.sortColumnIndex]));
                if (isNaN(aa)) aa = 0;
                bb = parseFloat(thisObject.getInnerText(b.cells[thisObject.sortColumnIndex]));
                if (isNaN(bb)) bb = 0;
                return aa - bb;
            }
            // define variables
            var thisObject = this;
            var sortSection = this.thead;
            // constructor actions
            if (!(this.tbody && this.tbody[0].rows && this.tbody[0].rows.length > 0)) return;
            if (sortSection && sortSection[0].rows && sortSection[0].rows.length > 0) {
                var sortRow = sortSection[0].rows[0];
            } else {
                return;
            }
            for (var i = 0; i < sortRow.cells.length; i++) {
                sortRow.cells[i].sTable = this;
                sortRow.cells[i].onclick = function () {
                    this.sTable.sort(this);
                    return false;
                }
            }
        }
    </script>
</head>
    <body onload="Javascript:GetInitialData();">
    <form id="form1" runat="server">
    &nbsp&nbsp;<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        <Services>
            <asp:ServiceReference Path="~/DeGrToolWebService.asmx" />
        </Services>
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="header">
                <div class="headerleft">
                </div>
                <div class="headermiddle">
                    <div>
                        <table class="clogo">
                            <tr>
                                <td>
                                    <img src="Images/Banner.png" alt="" width="100%" height="62px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="DivHeaderMain" align="center" style="font-size: 12px; font-family: Arial;
                                        margin: auto;">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <%--   <div class="clogo">
            </div> class="headermidd"--%>
                </div>
                <div class="headerright">
                </div>
            </div>
            <br />
            <br />
            <div id="menu" runat="server">
            </div>
            <div style="width: 98%; margin-left: 1%; margin-top: 10px; float: left; position: relative;">
                <table style="width: 98%; border: 1px Solid #E5E5E5;">
                    <tr style="background-color: White;">
                        <td colspan="3" align="center" style="width:100%;display:inline-block;border-bottom: 1px Solid #E5E5E5;">
                            <h1 style="color: #1f497d; font-family: Verdana; font-weight: bold; font-size: 20px;">
                                User Activity Log</h1>
                        </td>
                        <td style="width:40%;border-bottom: 1px Solid #E5E5E5;"></td>
                    </tr>
                     <tr style="background-color: White;">
                        <td colspan="3" align="center" style="width:100%;display:inline-block;border-bottom: 1px Solid #E5E5E5;">
                            App Name:
                            <select id="ddlAppName" style="width: 30%;" onchange="Javascript:GetSelectedAppData();">
                            </select>
                        </td>
                        <td style="width:30%;border-bottom: 1px Solid #E5E5E5;"></td>
                       
                    </tr>



                    <tr>
                        <td>
                            <table id="tblappDetails" style="display: none;">
                                <tr style="background-color: White;">
                                    <td class="font">
                                        User ID:
                                    </td>
                                    <td colspan="2" style="border-bottom: 0px Solid #E5E5E5;">
                                        <select id="ddlRepId" style="width: 30%;" onchange="Javascript:getSumaaryReport();">
                                        </select>
                                    </td>
                                </tr>
                                <tr style="background-color: White;">
                                    <td style="width: 10%;" class="font">
                                        Log Type:
                                    </td>
                                    <td colspan="2" style="border-bottom: 0px Solid #E5E5E5;">
                                        <select id="ddlLogType" style="width: 30%;" onchange="Javascript:getSumaaryReport();">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label9" runat="server" Text="Report Type :"></asp:Label>
                                                            <asp:DropDownList ID="DrpCycle" CssClass="font" runat="server" AutoPostBack="true"
                                                                OnSelectedIndexChanged="DrpCycle_SelectedIndexChanged">
                                                                <asp:ListItem Text="" Value="0" />
                                                                <asp:ListItem Text="Daily" Value="1" />
                                                                <asp:ListItem Text="Weekly" Value="2" />
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <div id="daily" style="display: block;" runat="server">
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="Label10" runat="server" Text="From Date: "></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="Txtfromdate" AutoPostBack="true" runat="server" Width="90px" OnTextChanged="Txtfromdate_TextChanged"></asp:TextBox>
                                                                            <%--    <ajaxtoolkit:CalendarExtender ID="CalendarExtenfrom" CssClass="ajax__calendar" runat="server"
                                                            TargetControlID="Txtfromdate" Format="MM/dd/yyyy" Enabled="True" />--%>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="Label11" runat="server" Text="To Date: "></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="Txttodate" ReadOnly="true" runat="server" Width="90px" onkeypress="Javascript:return KeyPressforMainScreen(event);"></asp:TextBox>
                                                                            <%--  <ajaxtoolkit:CalendarExtender ID="CalendarExtento" CssClass="ajax__calendar" runat="server"
                                                TargetControlID="Txttodate" Format="MM/dd/yyyy" Enabled="True" />--%>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div id="weekly" style="display: none;" runat="server">
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="Label2" runat="server" Text="From Date:"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="Txtweeklyfrom" AutoPostBack="true" runat="server" Width="90px" OnTextChanged="Txtweeklyfrom_TextChanged"></asp:TextBox>
                                                                            <%--<ajaxtoolkit:CalendarExtender ID="CalendarExtender1" CssClass="ajax__calendar" runat="server"
                                                            TargetControlID="Txtweeklyfrom" Format="MM/dd/yyyy" Enabled="True" />--%>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="Label1" runat="server" Text="To Date:"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="TxtweeklyTo" ReadOnly="true" Width="90px" runat="server" onkeypress="Javascript:return KeyPressforMainScreen(event);"></asp:TextBox>
                                                                            <%-- <ajaxtoolkit:CalendarExtender ID="CalendarExtender2" CssClass="ajax__calendar" runat="server"
                                                                TargetControlID="Txttodate" Format="MM/dd/yyyy" Enabled="True" />--%>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                         <td>
                                                            <input type="button" value="Refresh" class="button" id="Button1" onclick="Javascript:getSumaaryReport();" />
                                                         </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="DrpCycle" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div id="divReport">
                </div>
                <div id="divReportDetail" style="display: none;">
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
