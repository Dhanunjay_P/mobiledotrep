﻿var AutoShowMessage = false;
var ServiceURL = '';
var RequestType = 2;//SubmitRetrieve
var SessionData = new Object();
var RequestObject = new Object();
var ResponseObject = new Object();
var GenerelArgsObject = new Object();
var ObjectProfile = new Object();
var OpArgsObject = new Object();
var UploadDataObject = new Object();
var SendJSONObject = new Object();
var ResultObject = new Object();
var CallbackOnSucess = null;

SendJSONObject["requestObjectInfo"] = RequestObject;
SendJSONObject["responseObjectInfo"] = null;

function PrepareBasicObjects(ClassName, MethodName, FactoryType, ModuleName, MenuId, TransactionType) {

    CallbackOnSucess = null;
    GenerelArgsObject["SessionID"] = SessionData["SessionId"];
    GenerelArgsObject["CompanyID"] = SessionData["CompanyId"];
    GenerelArgsObject["HostIP"] = SessionData["HostIP"];
    GenerelArgsObject["UserID"] = SessionData["UserId"];
    GenerelArgsObject["SaId"] = SessionData["SaId"];

    ObjectProfile["ClassName"] = ClassName;
    ObjectProfile["MethodName"] = MethodName;
    ObjectProfile["FactoryType"] = FactoryType;
    ObjectProfile["ModuleName"] = ModuleName;
    ObjectProfile["MenuID"] = MenuId;
    ObjectProfile["TransactionType"] = TransactionType;

    OpArgsObject = {};

    RequestObject["RequestType"] = RequestType;
    RequestObject["GeneralArgs"] = GenerelArgsObject;
    RequestObject["ObjectProfile"] = ObjectProfile;
    RequestObject["OpArgs"] = OpArgsObject;
    RequestObject["ds_UploadData"] = UploadDataObject;
}

function CallWebService(OnSucessCallback) {
    CallbackOnSucess = OnSucessCallback;
    var jsonParameter = JSON.stringify(SendJSONObject);
    DeGrToolWebService.ExecuteClientRequest(jsonParameter, OnSuccess, OnError);    
}

function OnSuccess(result, status) {

    ResultObject = JSON.parse(result);
    if (ResultObject != null) {
        if (ResultObject.responseObjectInfo.Message != null) {
            if (ResultObject.responseObjectInfo.Status == 1) {
                if (AutoShowMessage)
                    DataSavedNotify(ResultObject.responseObjectInfo.Message, ResultObject.responseObjectInfo.Status);                
                CallbackOnSucess();
            }
            else {
                DataSavedNotify(ResultObject.responseObjectInfo.Message, ResultObject.responseObjectInfo.Status);                
            }
        }
    }    
}

function OnError(errorMessage) {

    DataSavedNotify('error in PagePreInit:' + errorMessage, 2);    
}

function DataSavedNotify(msg, status) {

    //alert(msg);
    //$('#saved').show();

    if (status == 1) {
        document.getElementById('saved').style.backgroundColor = "#5BB75B";
        document.getElementById('saved').style.backgroundImage = "linear-gradient(to bottom, #62C462, #51A351)";        
    }
    else {
        document.getElementById('saved').style.backgroundColor = "#FF0000";
        document.getElementById('saved').style.backgroundImage = "linear-gradient(to bottom, #FF0000, #FF6600)";        
    }

    document.getElementById('lblMessage').innerHTML = msg;
    $('#saved').slideDown();
    setTimeout(function () {
        $('#saved').slideUp();
    }, 5000);
}

function CloseMessage() {
    $('#saved').slideUp();
    //$('#saved').hide();
}

var FirstIndexAS = { "None" : 0, "Blank" : 1, "Select" : 2, "ALL" : 3 };

//----- Bind Dropdown List -----
function BindDropDown(objdata, dropdown_id, bind_id, bind_name, objFirstIndexAS) {
    $("#" + dropdown_id).empty();
    if (objdata != null) {
        if (objFirstIndexAS == FirstIndexAS.Blank)
            $('#' + dropdown_id).append('<option value=""></option>');
        else if (objFirstIndexAS == FirstIndexAS.Select)
            $('#' + dropdown_id).append('<option value="">-- Select --</option>');
        else if (objFirstIndexAS == FirstIndexAS.ALL)
            $('#' + dropdown_id).append('<option value="ALL">-- ALL --</option>');
        $.each(objdata, function (i, val) {
            $('#' + dropdown_id).append('<option value="' + val[bind_id] + '">' + val[bind_name] + '</option>');
        });
    }
}

//----- Bind Dropdown List 2 Name Field-----
function BindDropDown2NameField(objdata, dropdown_id, bind_id, bind_name1, bind_name2, objFirstIndexAS) {
    $("#" + dropdown_id).empty();
    if (objdata != null) {
        if (objFirstIndexAS == FirstIndexAS.Blank)
            $('#' + dropdown_id).append('<option value=""></option>');
        else if (objFirstIndexAS == FirstIndexAS.Select)
            $('#' + dropdown_id).append('<option value="">-- Select --</option>');
        else if (objFirstIndexAS == FirstIndexAS.ALL)
            $('#' + dropdown_id).append('<option value="ALL">-- ALL --</option>');
        $.each(objdata, function (i, val) {
            $('#' + dropdown_id).append('<option value="' + val[bind_id] + '">' + val[bind_name1] + ', ' + val[bind_name2] + '</option>');
        });
    }
}

//function CallWebService(mode) {
//    var jsonParameter = JSON.stringify(SendJSONObject);
//    jQuery.support.cors = true;
//    $.ajax({
//        async: false,
//        cache: false,
//        type: 'POST',
//        url: ServiceURL + "/ExecuteClientRequest",
//        data: JSON.stringify({ 'jsonParameter': jsonParameter }),
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (result) {
//            ResultObject = JSON.parse(result.d);
//            if (ResultObject != null) {
//                if (ResultObject.responseObjectInfo.Message != null)
//                    DataSavedNotify(ResultObject.responseObjectInfo.Message);
//                    //alert(ResultObject.responseObjectInfo.Message);
//                if (ResultObject.responseObjectInfo.Status == 4) {
//                    //If Session has expired than trasfer to login page.
//                    window.location.reload();
//                }                
//            }
//            ServiceCallSuccess(mode);
//        },
//        error: function (result) {
//            // alert(result.status);
//            alert("jQuery Error:" + result.statusText);
//        },
//        statusCode: {
//            404: function () {
//                alert('Could not contact server.');
//            },
//            500: function () {
//                alert('A server-side error has occurred.');
//            }
//        }
//    });
//}