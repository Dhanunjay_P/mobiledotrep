﻿jQuery.extend({
    converToUSDate: function (date) {
        var Cur_date = date;
        if (Cur_date != "" && Cur_date != null) {
            var USDate = $.datepicker.parseDate("mm-dd-yy", date);
            return ($.datepicker.formatDate("yy-mm-dd", USDate));
        }
        else {
            return null;
        }
    },
    getFormData: function (param) {

        var ob = {};

        $("input[type='text']." + param).each(function (i) {

            if ($(this).data("text-mode") == "datepicker") {
                var date = $(this).val();
                if (date != "") {
                    var USDate = $.datepicker.parseDate("mm-dd-yy", date);
                    ob[$(this).data("field-name")] = $.datepicker.formatDate("yy-mm-dd", USDate);
                }
                else {
                    ob[$(this).data("field-name")] = null;
                }
            }
            else {
                ob[$(this).data("field-name")] = ($(this).val() == undefined || $(this).val() == null || String($(this).val()) == "") ? null : $(this).val();
            }
        });

        $("input[type='password']." + param).each(function (i) {
            ob[$(this).data("field-name")] = ($(this).val() == undefined || $(this).val() == null || String($(this).val()) == "") ? null : $(this).val();
        });

        $("textarea." + param).each(function (i) {
            ob[$(this).data("field-name")] = ($(this).val() == undefined || $(this).val() == null || String($(this).val()) == "") ? null : $(this).val();
        });

        $("select." + param).each(function (i) {

            ob[$(this).data("field-name")] = ($(this).val() == undefined || $(this).val() == null || String($(this).val()) == "") ? null : $(this).val();
        });

        $("." + param + ".chkGroup").each(function (i) {

            var value = $('input[name=' + $(this).data("field-name") + ']:checked').val();
            ob[$(this).data("field-name")] = (value == undefined || value == null || String(value) == "") ? null : value;
        });

        $("input[type='checkbox']." + param).not(".chkGroup").each(function (i) {
            var value = $(this).is(":checked");
            ob[$(this).data("field-name")] = (value == true) ? "Y" : "N";
        });
        return ob;
    },

    clearFormData: function (param) {


        $("." + param + ".chkGroup ").each(function (i) {
            id = $(this).attr("id");
            if (id != undefined) {
                $("#" + id + " input[type='radio']").each(function (item) {
                    if ($(this).attr("id") == (id + "_N"))
                        $(this).attr("checked", true);
                });
            }
        });

        $("input[type='text']." + param).each(function (i) {
            //alert($(this).id);
            $(this).val("");
        });

        $("input[type='password']." + param).each(function (i) {
            //alert($(this).id);
            $(this).val("");
        });

        $("textarea." + param).each(function (i) {
            $(this).val("");
        });

        $("select." + param).each(function (i) {
            $(this).val("");
        });

        $("." + param + ".chkGroup").each(function (i) {
            $(this).prop("checked", false);
        });

        $("input[type='checkbox']." + param).not(".chkGroup").each(function (i) {
            $(this).prop("checked", false);
        });
    },

    setFormData: function (tableName, dataOb) {
        var param = tableName;

        $("input[type='text']." + param).each(function (i) {
            //alert("hello");
            //alert($(this).data("field-name") + "  " + dataOb[$(this).data("field-name")]);
            if (dataOb[$(this).data("field-name")]) {
                if ($(this).data("text-mode") == "datepicker") {
                    var date = dataOb[$(this).data("field-name")].split("T");
                    var USDate = new Date(date[0]);

                    $(this).val($.datepicker.formatDate("mm-dd-yy", USDate));
                }
                else if ($(this).data("text-mode") == "timepicker") {
                    //alert(dataOb[$(this).data("field-name")]);
                    var time = dataOb[$(this).data("field-name")].split(":");
                    $(this).val(time[0] + ":" + time[1]);
                }
                else {
                    $(this).val(dataOb[$(this).data("field-name")]);
                }
            }
            else
                $(this).val("");
        });

        $("input[type='password']." + param).each(function (i) {
            if (dataOb[$(this).data("field-name")])
                $(this).val(dataOb[$(this).data("field-name")]);
            else
                $(this).val("");
        });

        $("textarea." + param).each(function (i) {
            if (dataOb[$(this).data("field-name")])
                $(this).val(dataOb[$(this).data("field-name")]);
            else
                $(this).val("");

        });

        $("select." + param).each(function (i) {
            if (dataOb[$(this).data("field-name")])
                $(this).val(dataOb[$(this).data("field-name")]);
            else
                $(this).val("");
        });

        $("." + param + ".chkGroup").each(function (i) {
            if (dataOb[$(this).data("field-name")]) {
                $('input[type="radio"][name="' + $(this).data("field-name") + '"][value="' + dataOb[$(this).data("field-name")] + '"]').prop("checked", true);
                // $(this).val(dataOb[$(this).data("field-name")]);
            }
        });

        $("input[type='checkbox']." + param).not(".chkGroup").each(function (i) {
            if (dataOb[$(this).data("field-name")]) {
                var chkVal = (dataOb[$(this).data("field-name")] == "Y") ? true : false;
                $(this).attr("checked", chkVal);
            }
            else
                $(this).attr("checked", false);
        });
    },

    DefaultCheck: function (param) {
        $("." + param + ".chkGroup ").each(function (i) {
            id = $(this).attr("id");
            if (id != undefined) {
                $("#" + id + " input[type='radio']").each(function (item) {
                    if ($(this).attr("id") == (id + "_N"))
                        $(this).attr("checked", true);
                });
            }
        });
    },

    setIgnore: function setIgnore(section) {
        rules = Main_Rule["rules"];
        validator.resetForm();
        for (var name in rules) {
            if ($("[name='" + name + "']").closest("#" + section).length > 0) {
                $("#" + section + "  [name='" + name + "']").removeClass("ignore");
            }
            else {

                $("[name='" + name + "']").removeClass("ignore");
                $("[name='" + name + "']").addClass("ignore");
            }
        }
    }
        
});

