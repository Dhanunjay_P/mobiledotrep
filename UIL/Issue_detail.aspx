﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Air.master" AutoEventWireup="true"
    CodeFile="Issue_detail.aspx.cs" Inherits="Issue_datail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" media="all" rel="stylesheet" />--%>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="css/bootstrap-dialog.min.css" rel="stylesheet" />
    <%-- <link href="css/ rel="stylesheet" />--%>
    <link href="https://cdn.datatables.net/fixedcolumns/3.2.2/css/fixedColumns.dataTables.min.css" rel="stylesheet" />
    <script src="css/jquery-1.12.3.js"></script>
    <script src="css/jquery.dataTables.min.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900'
        rel='stylesheet' type='text/css' />
    <%--  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">--%>
    <script src="css/bootstrap.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src=" https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
    <script src="https://cdn.datatables.net/fixedcolumns/3.2.2/js/dataTables.fixedColumns.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.7/js/bootstrap-dialog.min.js"></script>
    <%--<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"> </script>--%>

    <link href="CSS%20Styles/swtitch.css" rel="stylesheet" />
    <script>
        var obj = new Object();
        //var obj = [];
        $(document).ready(function () {


            var hindcaseno;
            window.tmp;
            $('#myModal').modal('hide');

            if (sessionStorage["username"] != null) {
                $("#loading").show();
                DeGrToolWebService.GetUserdata_air(sessionStorage["username"], onSuccess, onFail);

            } else {
                location.href = "Login_Air.aspx";

            }
            $("#id_status").on('focus', function () {
                window.tmp = this.value;
            });
            //$("#id_status").change(function (e) {
            //    if ($('#id_status').val() == "Closed") {
            //        if ($('#id_assing').val() == "-") {
            //            alert(" Please assign a case manager to this request prior to closing");
            //            $("#id_status").val("Open");
            //            return false;
            //        }
            //    }
            //    if ($('#id_status').val() == "Closed") {
            //        var r = confirm("You are about to close this CR. No further changes may be made after closing, and closed CRs may not be re-opened. Do you want to proceed?");
            //        if (r == true) {


            //        } else {

            //            $("#id_status").val(window.tmp);
            //            return false;
            //        }


            //    }
            //});
            $("#btn_save").click(function (e) {
                var salesnote = "";
                // alert(makeStringFromObject(obj));
                //console.log(makeStringFromObject(obj));
                if ($("#select_pic").html() == undefined) {
                    salesnote = makeStringFromObject(obj);
                } else {

                    var pdata = $("#select_pic").html()
                    var selectdorpdata = makeStringFromObject(obj);
                    salesnote = pdata + selectdorpdata;
                }
                //makeStringFromObject(obj);
                console.log(salesnote);
                if ($('#id_assing').val() == "-") {
                    BootstrapDialog.show({

                        title: 'Message',
                        message: 'Please select  Assign To ',

                        buttons: [
                          {
                              label: 'Close',
                              cssClass: 'btn btn-rounded  btn-primary',
                              action: function (dialogItself) {
                                  dialogItself.close();
                                  $("#id_status").val("Open");
                                  return false;
                              }
                          }]
                    });
                    if ($('#id_status').val() == "Closed") {
                        if ($('#id_assing').val() == "-") {

                            //BootstrapDialog.show({

                            //    title: 'Message',
                            //    message: 'Please assign a case manager to this request prior to closing.',

                            //    buttons: [
                            //      {
                            //          label: 'Close',
                            //          cssClass: 'btn btn-rounded  btn-primary',
                            //          action: function (dialogItself) {
                            //              dialogItself.close();
                            //              $("#id_status").val("Open");
                            //              return false;
                            //          }
                            //      }]
                            //});

                        }
                    }

                }
                else {

                    if ($('#id_status').val() == "Closed") {

                        BootstrapDialog.show({

                            title: 'Message',
                            message: 'You are about to close this CR. Do you want to proceed?',
                            buttons: [
                            {
                                label: 'OK',
                                cssClass: 'btn btn-rounded  btn-primary',
                                action: function (dialogItself) {
                                    dialogItself.close();
                                    $("#loading").show();
                                    // $("#loading").show();
                                    //$("#comment").attr('disabled', true);
                                    //$("#message").attr('disabled', true);
                                    $("#id_status").attr('disabled', true);
                                    //$("#id_assing").attr('disabled', true);
                                    var str = "";

                                    $("#comment div").each(function () { str += $(this).html() + '<br/>' })
                                    DeGrToolWebService.SaveAirRpts($('#username').val(), window.hindcaseno, $('#id_assing').val(), str, salesnote, $('#id_status').val(), $('#airtype').val(), popusave);

                                    return false;
                                }
                            },
                            {
                                label: 'Close',
                                cssClass: 'btn btn-rounded btn-warning',
                                action: function (dialogItself) {
                                    dialogItself.close();
                                    $("#id_status").val(window.tmp);

                                    return false;
                                }
                            }
                            ]

                        });

                        //var r = confirm("You are about to close this CR. No further changes may be made after closing, and closed CRs may not be re-opened. Do you want to proceed?");

                        //if (r == true) {

                        //    //if ($('#id_status').val() == "Closed") {
                        //    //    $("#comment").attr('disabled', true);
                        //    //    $("#message").attr('disabled', true);
                        //    //    $("#id_status").attr('disabled', true);
                        //    //    $("#id_assing").attr('disabled', true);


                        //    //}
                        //    debugger;
                        //    DeGrToolWebService.SaveAirRpts($('#username').val(), window.hindcaseno, $('#id_assing').val(), $('#comment').val(), $('#message').val(), $('#id_status').val(), $('#airtype').val(), popusave);
                        //    DeGrToolWebService.getAir($('#username').val(), $('#select_air_type').val(), UpdateCount);
                        //    DeGrToolWebService.GetAirwisedata($('#username').val(), $('#select_air_type').val(), datatableset);
                        //    //by nitu on 27-12-2016 start


                        //} else {
                        //    debugger;
                        //    return false;
                        //    //DeGrToolWebService.SaveAirRpts($('#username').val(), window.hindcaseno, $('#id_assing').val(), $('#comment').val(), $('#message').val(), $('#id_status').val(), $('#airtype').val(), popusave);
                        //    //DeGrToolWebService.getAir($('#username').val(), $('#select_air_type').val(), UpdateCount);
                        //    //DeGrToolWebService.GetAirwisedata($('#username').val(), $('#select_air_type').val(), datatableset);
                        //    ////by nitu on 27-12-2016 start

                        //}
                    } else {
                        debugger;
                        $("#loading").show();
                        var str = "";

                        $("#comment div").each(function () { str += $(this).html() + '<br/>' })
                        DeGrToolWebService.SaveAirRpts($('#username').val(), window.hindcaseno, $('#id_assing').val(), str, salesnote, $('#id_status').val(), $('#airtype').val(), popusave);
                        //DeGrToolWebService.getAir($('#username').val(), $('#select_air_type').val(), UpdateCount);
                        //DeGrToolWebService.GetAirwisedata($('#username').val(), $('#select_air_type').val(), datatableset);
                        //by nitu on 27-12-2016 start


                    }
                    //  DeGrToolWebService.SaveAirRpts($('#username').val(), window.hindcaseno, $('#id_assing').val(), $('#comment').val(), $('#message').val(), $('#id_status').val(), $('#airtype').val(), popusave);
                    //DeGrToolWebService.getAir($('#username').val(), $('#select_air_type').val(), UpdateCount);
                    //DeGrToolWebService.GetAirwisedata($('#username').val(), $('#select_air_type').val(), datatableset);

                }




                return false;
            });




            $("#search").click(function (event) {

                $("#loading").show();
                // alert("search");
                $("#myonoffswitch").prop('checked', false)

                //by nitu on 27-12-2016 start
                DeGrToolWebService.getAir($('#username').val(), $('#select_air_type').val(), UpdateCount);
                DeGrToolWebService.GetAirwisedata($('#username').val(), $('#select_air_type').val(), datatableset);

                //by nitu on 27-12-2016 end


            });




            //save button click
            //$(Document).on("click", ".cust_btn", function (e) {
            //    debugger;
            //    //  alert(e.target.id);
            //    //alert($(this).closest('tr').find('.cls_drp_assingby').val());

            //    //  alert($(this).closest('tr').find('.cls_drp_status_air').val());
            //    if ($(this).closest('tr').find('.cls_drp_assingby').val() == "*") {
            //        alert("please Assigned ");
            //        return false;

            //    }
            //    var caseno = e.target.id;
            //    var ass = $(this).closest('tr').find('.cls_drp_assingby').val();
            //    var status = $(this).closest('tr').find('.cls_drp_status_air').val();
            //    DeGrToolWebService.SaveAirRpts($('#username').val(), caseno, ass, null, null, status, datasaved);
            //});




            //$(".cls_drp_assingby").change(function (event) {

            //    var index = ($(this).closest('tr')).index();

            //    $(this).closest('tr').children().each(function () { if ($(this).children().hasClass('cls_drp_status_air')) { $(this).children().val('Progress'); } });

            //});
            //$(Document).on("change", ".cls_drp_assingby", function (e) {

            //    var index = ($(this).closest('tr')).index();

            //    $(this).closest('tr').children().each(function () { if ($(this).children().hasClass('cls_drp_status_air')) { $(this).children().val('Progress'); } });
            //    //$('#tbl_air tbody').on('click', 'tr', function () {
            //    //    alert('Row index: ' + table.row(this).index());
            //    //});
            //});


            //$(Document).on("click", ".cls_view_data", function (e) {
            //    debugger;
            //    alert(e.target.id);
            //    window.hindcaseno = e.target.id;
            //    $('#hnd_caseid').val(window.hindcaseno);
            //    //databindview();
            //    DeGrToolWebService.GetAirPortalData(window.hindcaseno, $('#username').val(), databindview);
            //});
            //$(".cls_view_data").on('click', function (event) {
            //    //alert("view datail");
            //    //window.hindcaseno = e.target.id;
            //    //$('#hnd_caseid').val(window.hindcaseno);
            //    ////databindview();
            //    //DeGrToolWebService.GetAirPortalData(window.hindcaseno, $('#username').val(), databindview);
            //});

            $("#myonoffswitch").click(function (e) {
                $("#loading").show();
                if (!$("#myonoffswitch").prop('checked')) {

                    ////checked True 
                    //alert('ON');

                    //by nitu on 27-12-2016 start
                    DeGrToolWebService.getAir($('#username').val(), null, UpdateCount);
                    DeGrToolWebService.GetAirwisedata($('#username').val(), "admin", datatableset);
                }
                else {
                    //alert('Off');

                    //by nitu on 27-12-2016 start
                    DeGrToolWebService.get_airwisedata_Archive_Count($('#username').val(), null, UpdateCount);
                    DeGrToolWebService.get_airwisedata_Archive($('#username').val(), null, datatableset);

                }
            });


            //$('#tbl_air thead th:eq(1)').unbind('click').click(function () {
            //    alert("called");
            //    var aaSorting = oTable.fnSettings().aaSorting;
            //    if ( aaSorting[0][0] != 2 || aaSorting[0][1] == 'desc' ) {
            //        oTable.fnSort( [[2,"asc"], [3,"asc"]] );
            //    } else {
            //        oTable.fnSort( [[2,"desc"], [3,"desc"]] );
            //    }
            //} );


            //oTable.fnSortListener(oTable, 2);
            //$("#Status_air").click(function () {
            //    var aaSorting = oTable.fnSettings().aaSorting;
            //    if (aaSorting[0][0] != 2 || aaSorting[0][1] == "desc") {
            //        alert("asd");
            //        oTable.fnSort([[2, "asc"], [3, "asc"]]);
            //    } else {
            //        oTable.fnSort([[2, "desc"], [3, "desc"]]);
            //    }
            //});
            //var aaSorting = oTable.fnSettings().aaSorting;
            //if (aaSorting[0][0] != 2 || aaSorting[0][1] == 'desc') {
            //    oTable.fnSort([[2, "asc"], [3, "asc"]]);
            //} else {
            //    oTable.fnSort([[2, "desc"], [3, "desc"]]);
            //}

            $("#pick_list_dropdown").change(function () {
                //  alert("this");
                debugger;
                var dropid = $("#pick_list_dropdown").val();
                if (obj.hasOwnProperty(dropid) == false) {
                    if (dropid != "-") {
                        var today = new Date();
                        $("#pick_list_select").append('<a id="' + dropid + '" class=" cust_select_a ui label transition visible" data-value="angular" style="display: inline-block !important;" >' + formatDate(Date.now()) + " - " + $("#pick_list_dropdown option:selected").text() + '<span class="cancelplicklist" id="' + dropid + '" data-attrval ="123" onclick="spanremove(\'' + dropid + '\')"><i style="padding-left: 5px;font-weight: bold;" >x</i><span></a>');
                        obj[dropid] = $("#pick_list_dropdown option:selected").text();
                    }
                } else {
                    BootstrapDialog.show({

                        title: 'Message',
                        message: "already selected",

                        buttons: [
                          {
                              label: 'Close',
                              cssClass: 'btn btn-rounded  btn-primary',
                              action: function (dialogItself) {
                                  dialogItself.close();
                                  //$("#id_status").val("Open");
                                  //window.location.href = URLForPageRedirection + "tutor.html";
                                  return false;
                              }
                          }]
                    });
                    // alert("already selected");
                }
                // console.log(obj);

            });

            $("#add").click(function () {
                //  alert("this");
                debugger;
                //$('#add_text').css('display', 'block');
                $('#add_text').removeAttr('style');
                $('.img_cust').removeAttr('style');
                //$("#add_text").show();
            });
            $("#img_correct").click(function () {

                $("#comment").append('<div>' + formatDate(Date.now()) + "  -  " + $('#add_text').val() + "</div>");
                $("#img_worng").trigger('click');

            });
            $("#img_worng").click(function () {

                $("#add_text").val("");

                $('#add_text').css("display", "none");
                $('.img_cust').css("display", "none");
            });
        });//ready end  

        function formatDate(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [month, day, year].join('/');
        }
        function makeStringFromObject(obj1) {
            debugger;

            // console.log(obj1)
            debugger;
            var today = new Date();
            var dd = today.getDate();
            var str = "";
            for (var key in obj1) {
                // console.log(obj1)
                str += formatDate(Date.now()) + ' - ' + obj1[key] + '<br>';
            }
            // alert(str);
            return str;
        }

        function spanremove(id) {
            // alert(id);
            debugger;
            //obj[id].romove();
            delete obj[id];
            //delete array[id];
            $("#" + id).remove();

        }
        function view_popup(id) {
            // alert("this");
            // alert(id.id);
            $("#add_text").val("");

            $('#add_text').css("display", "none");
            $('.img_cust').css("display", "none");
            $("#loading").show();
            window.hindcaseno = id.id;
            $('#hnd_caseid').val(window.hindcaseno);

            //databindview();
            DeGrToolWebService.GetAirPortalData(window.hindcaseno, $('#username').val(), databindview);
        }


        //$(Document).on("click", ".cls_view_data", function (e) {
        //    debugger;
        //    alert(e.target.id);
        //    window.hindcaseno = e.target.id;
        //    $('#hnd_caseid').val(window.hindcaseno);
        //    //databindview();
        //    DeGrToolWebService.GetAirPortalData(window.hindcaseno, $('#username').val(), databindview);
        //});
        $(".btn_close").click(function (event) {
            $('#myModal').empty();
        });
        //$(Document).on("click", ".btn_close", function (e) {
        //    $('#myModal').empty();
        //});
        function printpotion() {
            debugger;

            $('#hnd_caseid').val($('#CaseNo').text());
            //  alert($('#hnd_caseid').val());
            $('#hnd_printclick').click();
        }
        function datasaved(data) {
            var json = $.parseJSON(data);
            if (json["Status"] == 1) {
                alert(json["Message"]);
            }
        }
        function popusave(data) {
            var json = $.parseJSON(data);
            if (json["Status"] == 1) {
                $("#loading").hide();
                BootstrapDialog.show({

                    title: 'Message',
                    message: json["Message"],

                    buttons: [
                      {
                          label: 'Close',
                          cssClass: 'btn btn-rounded  btn-primary',
                          action: function (dialogItself) {
                              dialogItself.close();
                              //$("#id_status").val("Open");
                              //window.location.href = URLForPageRedirection + "tutor.html";
                              return false;
                          }
                      }]
                });
                // alert(json["Message"]);
                //if ($('#id_status').val() == "Closed") {

                //    $("#comment").attr('disabled', true);
                //    $("#message").attr('disabled', true);
                //}
                DeGrToolWebService.getAir($('#username').val(), $('#select_air_type').val(), UpdateCount);
                DeGrToolWebService.GetAirwisedata($('#username').val(), $('#select_air_type').val(), datatableset);
            }
            else {
                BootstrapDialog.show({

                    title: 'Message',
                    message: json["Message"],

                    buttons: [
                      {
                          label: 'Close',
                          cssClass: 'btn btn-rounded  btn-primary',
                          action: function (dialogItself) {
                              dialogItself.close();
                              $("#id_status").val("Open");
                              //window.location.href = URLForPageRedirection + "tutor.html";
                              return false;
                          }
                      }]
                });
                //alert(json["Message"]);
            }
            $("#loading").hide();

        }
        function onSuccess(data) {
            debugger;
            var json = $.parseJSON(data);
            if (json["Status"] == 1) {
                $('#username').val(sessionStorage["username"]);
                $('#type').val(json["dt_ReturnedTables"][0][0]["AccessIssuetypeid"]);


                DeGrToolWebService.getAir($('#username').val(), $('#select_air_type').val(), dropdown);


                //$(".sh").hide();

                // DeGrToolWebService.GetAirwisedata($('#username').val(), $('#type').val(), datatableset);

            } else {
                $("#loading").hide();
                BootstrapDialog.show({

                    title: 'Message',
                    message: "You don't have rights to any Access Issue Type",

                    buttons: [
                      {
                          label: 'Close',
                          cssClass: 'btn btn-rounded  btn-primary',
                          action: function (dialogItself) {
                              dialogItself.close();

                              return false;
                          }
                      }]
                });
                //location.href = "Login_Air.aspx";
            }
        }

        function onFail(error) {
            $("#loading").hide();
        }
        function dropdown(data) {

            var result_course = JSON.parse(data);
            if (result_course["Status"] == 1) {
                $("label[for='open']").text(result_course["dt_ReturnedTables"][1][0]["total_open"]);
                //$('#open').val(result_course["dt_ReturnedTables"][1][0]["total_open"]);
                $('#pro').text(result_course["dt_ReturnedTables"][1][0]["total_Progress"]);

                $('#closed').text(result_course["dt_ReturnedTables"][1][0]["total_Closed"]);


                $("#select_air_type").append($('<option></option>', {
                    value: "admin",
                    text: "ALL",
                }));
                for (var i = 0; i < result_course["dt_ReturnedTables"][0].length; i++) {

                    $("#select_air_type").append($('<option></option>', {
                        //value: result_course["dt_ReturnedTables"][0][i]["AccessIssueTypeId"],
                        //text: result_course["dt_ReturnedTables"][0][i]["Description"],
                        value: result_course["dt_ReturnedTables"][0][i]["AccessIssuetypeid"],
                        text: result_course["dt_ReturnedTables"][0][i]["Description"],
                    }));
                }
                DeGrToolWebService.GetAirwisedata($('#username').val(), 'admin', datatableset);
            } else {
                $("#loading").hide();

            }
        }


        function UpdateCount(data) {
            debugger
            var result_course = JSON.parse(data);
            $("label[for='open']").text(result_course["dt_ReturnedTables"][1][0]["total_open"]);
            // $('#open').val(result_course["dt_ReturnedTables"][1][0]["total_open"]);
            $('#pro').text(result_course["dt_ReturnedTables"][1][0]["total_Progress"]);

            $('#closed').text(result_course["dt_ReturnedTables"][1][0]["total_Closed"]);

        }

        var oTable;
        function datatableset(data) {

            var jsontable = $.parseJSON(data);
            if (jsontable["Status"] == 1) {
                var dropdowdisbale = jsontable["dt_ReturnedTables"][0][0]["is_status_change"];
            }
            if (jsontable["Status"] == 1) {
                if (jsontable["dt_ReturnedTables"][0].length != 0) {

                    var table = "<table class='table table-hover cust_table ' id='tbl_air' style='text-align:center'><thead> <tr class='cust_table_header'><th>View </th> <th id='Status_air'>Status</th><th>Case No</th><th>Account ID </th><th class='LeftAlign'>Account Name</th><th id='tbl_issue_type'>Issue Type </th><th class='MinWidth'>Created By </th><th>Created Date</th><th class='minwidth_ass'>Assigned To</th> <th>Completion Date </th> <th>Territory </th><th>Region </th>  </thead>  <tbody>";
                    for (i = 0; i < jsontable["dt_ReturnedTables"][0].length; i++) {

                        var row = "<tr>";
                        //row += '<td>' + jsontable["dt_ReturnedTables"][0][i]["CaseNum"] + '</td>' + '<td>' + jsontable["dt_ReturnedTables"][0][i]["ZSPID"] + '</td>' + '<td>' + jsontable["dt_ReturnedTables"][0][i]["AccountName"] + '</td>' + '<td>' + jsontable["dt_ReturnedTables"][0][i]["AIRType"] + '</td>' + '<td>' + jsontable["dt_ReturnedTables"][0][i]["RepId"] + '</td>' + '<td>' + jsontable["dt_ReturnedTables"][0][i]["name"] + '</td>' + '<td>' + jsontable["dt_ReturnedTables"][0][i]["Phone_number"] + '</td>' + '<td><select class="cls_status_air">' + stutusbind(); + '</select></td>';
                        row += '<td><button type="button" class="cls_view_data btn btn-info" onclick="view_popup(this)" data-university-id="' + jsontable["dt_ReturnedTables"][0][i]["CaseNum"] + '" style="cursor: pointer;" id=' + jsontable["dt_ReturnedTables"][0][i]["CaseNum"] + '>' + "View" + '</button></td>';

                        row += '<td> <span class= "lbl" style="';

                        if (jsontable["dt_ReturnedTables"][0][i]["Status"] == "Closed") {
                            row += ' background-color: #337ab7;padding: 0.2em 1.5em .3em;';
                        }

                        else if (jsontable["dt_ReturnedTables"][0][i]["Status"] == "InProgress") {
                            row += 'background-color: #5cb85c;padding: 0.2em 0.6em .2em;';
                        }
                        else if (jsontable["dt_ReturnedTables"][0][i]["Status"] == "Open") {
                            row += 'background-color: #f0ad4e;padding: 0.2em 2.0em .3em;';
                        }

                        row += '">' + jsontable["dt_ReturnedTables"][0][i]["ModifiedStatus"] + '</span></td>';


                        //cust_td
                        row += '<td class=" ">' + jsontable["dt_ReturnedTables"][0][i]["CaseNum"] + '</td>' + '<td>' + jsontable["dt_ReturnedTables"][0][i]["ZSPID"] + '</td>' +
                            '<td class="LeftAlign">' + jsontable["dt_ReturnedTables"][0][i]["AccountName"] + '</td>' + '<td>' + jsontable["dt_ReturnedTables"][0][i]["Description"] + '</td>' +
                            '<td class="MinWidth">' + jsontable["dt_ReturnedTables"][0][i]["RepName"] + '</td>'
                        row += '<td>' + jsontable["dt_ReturnedTables"][0][i]["OpenDate"] + ' </td ><td class="minwidth_ass">';
                        if ((jsontable["dt_ReturnedTables"][0][i]["Assinged_To"]) == null) {
                            row += "-";
                        } else {
                            row += jsontable["dt_ReturnedTables"][0][i]["Assinged_To"];
                        }
                        row += '</td>';
                        row += '<td>' + jsontable["dt_ReturnedTables"][0][i]["CompletedDateFormated"] + '</td>';
                        row += '<td style=padding-left:14px;>' + jsontable["dt_ReturnedTables"][0][i]["TerritoryDetails"] + '</td>';
                        row += '<td>' + jsontable["dt_ReturnedTables"][0][i]["RegionDetails"] + '</td>';

                        row += '</tr>';
                        table += row;
                    }
                    table += '</tbody></table>';
                    $('#airtable').html(table);
                    if (!$.fn.DataTable.isDataTable("#tbl_air")) {
                        oTable = $('#tbl_air').DataTable({

                            //                    "columnDefs": [
                            //{ "className": "dt-center", "targets": "_all" }
                            //                    ],
                            "scrollX": true,
                            "bLengthChange": false,
                            "pageLength": 10,
                            "bFilter": true,
                            "fixedColumns": {
                                leftColumns: 6
                            },
                            "aaSorting": [],
                            "dom": 'Bfrtip',
                            buttons: [
                                  'excel'
                            ],
                            //"order": [[1, "desc"]],
                            //"aaSortingFixed": [[1, 'desc']],
                            "aoColumnDefs": [
                                 { "aDataSort": [1, 2, 7], "aTargets": [1] },
                                 { "asSorting": ["desc"], "aTargets": [1] }
                            ]

                        });
                    }

                    //var width1 = $(".dataTables_scrollHeadInner").outerWidth();

                    //$(".dataTables_scrollBody").css({ 'width': width1 });

                    $('#tbl_air thead th').css('text-align', 'left');
                    //$('#tbl_air thead th').css('padding-left', '2px');



                    //$("#tbl_issue_type").trigger('click');

                    $('#tbl_air tbody tr').each(function () {
                        var hdn_status = $(this).find('.cls_hdn_status_air').val();
                        $(this).find('.cls_drp_status_air').val(hdn_status);



                        //if (hdn_status == "Closed") {
                        //    $('.cls_drp_status_air').attr("disabled", true);
                        //    $('.cls_drp_assingby').attr("disabled", true);
                        //}

                        var hdn_assing = $(this).find('.cls_hdn_assign').val();
                        if (hdn_assing == "null") {
                            $(this).find('.cls_drp_assingby').val("*");

                        } else {
                            $(this).find('.cls_drp_assingby').val(hdn_assing);
                        }
                        //if (jsontable["dt_ReturnedTables"][0][0]["Assinged_to"] != "undefined") {
                        //    $(this).find('.cls_drp_assingby').val(hdn_ass_to);
                        //} 
                        if (dropdowdisbale == 'N') {
                            $('.cls_drp_status_air').attr("disabled", true);
                            $('.cls_drp_assingby').attr("disabled", true);
                        } else {
                            $('.cls_drp_status_air').attr("disabled", false);
                            $('.cls_drp_assingby').attr("disabled", false);
                        }

                    });
                }
            } else {
                $('#airtable').html("<p style='padding-left: 10px;align-content: center;'><b><align:center>NO Data Found </p>");
            }
            $("#loading").hide();

            if (navigator.userAgent.indexOf('Mac') >= 0) {
                if (navigator.userAgent.indexOf('Chrome') >= 0) {
                    $('#myonoffswitch').parent().css('margin-top', '-8px');
                }
                else if (navigator.userAgent.indexOf('Safari') >= 0) {
                    $('#myonoffswitch').parent().css('margin-top', '-8px');
                    $('#tbl_air_filter label').css('position', 'absolute');
                    $('#tbl_air_filter label').css('margin-left', '-240px');
                    $('#tbl_air_filter label').css('margin-top', '-35px');
                    $('#airtable').parent().css('margin-top', '60px');
                }
            }
        }
        function databindview(data) {
            // OpacityReduce("myModal", '1');
            debugger;
            var str = "";
            var pe = "";
            var desc = "";
            //$("#scrollTopDiv").scrollTop(0);
            //console.log( $("#scrollTopDiv").scrollTop());
            $("#comment").html('');
            //$("textarea#comment").val("");
            //$("textarea#message").val("");
            $('#rem').html("");
            $('#title_issue').html("");
            $('#issue_put').html("");
            $("#uldata").html("");
            $('#els').html("");
            $("#pick_list_select").html("");
            $("#message").html("");

            //$('#add_text').css('display', 'none');
            for (var key in obj) {

                for (var key in obj) {
                    delete obj[key]
                }
            }
            var jsontable = $.parseJSON(data);
            if (jsontable["Status"] == "1") {

                $('#CaseNo').text(jsontable["dt_ReturnedTables"][0][0]["CaseNum"]);

                $('#accid').text(jsontable["dt_ReturnedTables"][0][0]["Description"]);
                $('#Accname').text(jsontable["dt_ReturnedTables"][0][0]["ZSPID"] + " - " + jsontable["dt_ReturnedTables"][0][0]["AccountName"]);
                if (jsontable["dt_ReturnedTables"][0][0]["InternalMessage"] == null || jsontable["dt_ReturnedTables"][0][0]["InternalMessage"] == "") { } else {
                    var str = jsontable["dt_ReturnedTables"][0][0]["InternalMessage"];
                    str = str.replace(/<br\/>/g, "</div><div>");
                    str = '<div>' + str;
                    str = str.substring(0, str.length - 5);
                    $('#comment').html(str);
                }
                if (jsontable["dt_ReturnedTables"][0][0]["Message"] == null || jsontable["dt_ReturnedTables"][0][0]["Message"] == "") {
                } else {
                    // $('#message').val(jsontable["dt_ReturnedTables"][0][0]["Message"]);
                    $('#message').html("<p id=select_pic>" + jsontable["dt_ReturnedTables"][0][0]["Message"] + "</p>");
                }


                $('#rem').html(jsontable["dt_ReturnedTables"][0][0]["ActionItem_Desp"]);
                $('#title_issue').html("Issue Type: " + jsontable["dt_ReturnedTables"][0][0]["Description"]);
                $('#airtype').val(jsontable["dt_ReturnedTables"][0][0]["AIRType"]);

                if (jsontable["dt_ReturnedTables"][0][0]["QuestionId"] == null || jsontable["dt_ReturnedTables"][0][0]["QuestionId"] == "") {

                    $('#issue').css("display", "none");
                } else {

                    $('#issue').css("display", "block");
                    var strtable = '<table  width=100%>';
                    for (var i = 0; i < jsontable["dt_ReturnedTables"][1].length; i++) {

                        strtable += '<ol>';
                        strtable += '<tr>';

                        strtable += '<td style="vertical-align: top;" class="width_cust_table"><li></li></td><td class="Que firstCol">' + jsontable["dt_ReturnedTables"][1][i]["QuestionDescription"] + ":" + '</td>';





                        if ((jsontable["dt_ReturnedTables"][1][i]["Value"]).trim().length == 0) { strtable += '<td class="BlankTd"><td class="ans" style="vertical-align: top;">' + "-" + '</td>'; } else {
                            strtable += '<td class="BlankTd"></td><td class="ans LastCol" style="vertical-align: top;">' + jsontable["dt_ReturnedTables"][1][i]["Value"] + '</td>';
                        }

                        strtable += '</tr>';


                    }
                    strtable += '</table >';

                    $('#issue_put').html(strtable);
                }

                //for (var j = 0; j < 3; j++) {
                //    str += '<li>' + "Recommend the office perform a manual product addition if possible using the ZS-9 PI" + '</li>';


                //}
                var repstr = jsontable["dt_ReturnedTables"][0][0]["RecomemededAction_Desp"];

                var res = repstr.replace(/~/g, "<br/>");

                // alert(res);
                $("#uldata").html(res);


                if (jsontable["dt_ReturnedTables"][0][0]["EscalationId"] == null || jsontable["dt_ReturnedTables"][0][0]["EscalationId"] == "") {
                    $('#escalation').css("display", "none");
                } else {
                    $('#escalation').css("display", "block");
                    desc += '<table  width=100%>';
                    for (var j = 0; j < jsontable["dt_ReturnedTables"][2].length; j++) {
                        desc += '<ol>';
                        desc += '<tr>';
                        desc += '<td style="vertical-align: top;" class="width_cust_table"><li></li></td><td class="Que firstCol ">' + jsontable["dt_ReturnedTables"][2][j]["QuestionDescription"] + ":" + '</td>';
                        if ((jsontable["dt_ReturnedTables"][2][j]["Value"]).trim().length == 0) { desc += '<td class="BlankTd"><td class="ans">' + "-" + '</td>'; } else {
                            desc += '<td class="BlankTd"></td><td class="ans LastCol" style="vertical-align: top;">' + jsontable["dt_ReturnedTables"][2][j]["Value"] + '</td>';
                        }
                        desc += '</tr>';
                    }
                    desc += '</table>';
                    $('#els').html(desc);

                }

                if (jsontable["dt_ReturnedTables"][3].length > 0) {

                    debugger;

                    $("#id_assing").html("");
                    $("#id_assing").append($('<option></option>', {
                        value: "-",
                        text: "Select  ",
                    }));
                    for (var i = 0; i < jsontable["dt_ReturnedTables"][3].length; i++) {

                        $("#id_assing").append($('<option></option>', {
                            value: jsontable["dt_ReturnedTables"][3][i]["User_id"],
                            text: jsontable["dt_ReturnedTables"][3][i]["Name"],
                        }));

                    }
                }

                if (jsontable["dt_ReturnedTables"][4].length > 0) {

                    debugger;

                    $("#pick_list_dropdown").html("");
                    $("#pick_list_dropdown").append($('<option></option>', {
                        value: "-",
                        text: "Select Field Staff Note  ",
                    }));
                    for (var i = 0; i < jsontable["dt_ReturnedTables"][4].length; i++) {

                        $("#pick_list_dropdown").append($('<option></option>', {
                            value: jsontable["dt_ReturnedTables"][4][i]["Code"],
                            text: jsontable["dt_ReturnedTables"][4][i]["Description"],
                        }));

                    }
                }

                if (jsontable["dt_ReturnedTables"][0][0]["Assinged_To"] == "" || jsontable["dt_ReturnedTables"][0][0]["Assinged_To"] == null) {
                    $("#id_assing").val("-");
                }
                else {

                    $("#id_assing").val(jsontable["dt_ReturnedTables"][0][0]["Assinged_To"]);
                }

                $("#id_status").val(jsontable["dt_ReturnedTables"][0][0]["Status"]);



                if (jsontable["dt_ReturnedTables"][0][0]["Status"] == "Closed") {

                    $("#id_status").prop('disabled', true);
                    //$('#id_assing').prop('disabled', true);
                    //$("#comment").attr('disabled', true);
                    //$("#message").attr('disabled', true);
                    //$("#btn_save").hide();

                } else {

                    $("#id_status").prop('disabled', false);
                    //$('#id_assing').prop('disabled', false);
                    //$("#comment").attr('disabled', false);
                    //$("#message").attr('disabled', false);
                    //$("#btn_save").show();
                }
                if (jsontable["dt_ReturnedTables"][0][0]["is_status_change"] == "N") {
                    $('.id_assing').attr("disabled", true);
                    $('.id_status').attr("disabled", true);
                }

                $("#id_assing").change(function () {
                    if ($('#id_status').val() == "Closed") {

                    } else {
                        $('#id_status').val("InProgress");
                    }
                });
                if (jsontable["dt_ReturnedTables"][0][0]["CompletedDateFormated"] == null || jsontable["dt_ReturnedTables"][0][0]["CompletedDateFormated"] == "") { } else {
                    $('#comdate').text(jsontable["dt_ReturnedTables"][0][0]["CompletedDateFormated"]);
                }

                $('#myModal').modal();




            }
            $("#loading").hide();
        }



        //$('#toggle_event_editing button').click(function () {
        //    if ($(this).hasClass('locked_active') || $(this).hasClass('unlocked_inactive')) {
        //        /* code to do when unlocking */
        //        //$('#switch_status').html('Switched on.');
        //        alert("on");

        //    } else {
        //        /* code to do when locking */
        //        //$('#switch_status').html('Switched off.');
        //        alert("off");
        //    }

        //    /* reverse locking status */
        //    $('#toggle_event_editing button').eq(0).toggleClass('locked_inactive locked_active btn-default btn-info');
        //    $('#toggle_event_editing button').eq(1).toggleClass('unlocked_inactive unlocked_active btn-info btn-default');
        //});

        //$("#myonoffswitch").click(function (e) {
        //    if ($('#myonoffswitch').is(":checked")) {

        //    }
        //});

        function OpacityReduce(id, opacity) {
            $("#" + id).css({ 'opacity': opacity });
        }



    </script>
    <div class="container  custom">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12 headerbar">
                <div style="margin-left: 175px;">
                    <span class="status_text">Open:</span>
                    <label for="open" class="status_circle_text" style="margin-top: -5px;">
                        0</label>
                </div>
                <div style="margin-left: 513px; position: absolute; top: 8px;">
                    <span class="status_text">In Progress:</span> <span id="pro" class="status_circle_text"
                        style="margin-top: -3px;">0</span>
                </div>
                <div class="closed_text">
                    <span style="margin-left: 475px; margin-bottom: -59px;" class="status_text ">Closed:</span>
                    <span id="closed" class="status_circle_text" style="margin-top: -4px;">0</span>
                </div>
            </div>
        </div>
        <div class="row leftside  sh">
            <div class="col-xs-4 col-md-2">
                <select id="select_air_type" style="width: 150%; background-color: white;">
                </select>
                <input type="button" class="button  fadeInRight cust_btn1" value="Filter" id="search" />
            </div>

            <%--            <div class="toggle toggle-select" data-type="select" style="height: 22px; width: 118px;">
                <div class="toggle-slide toggle-select">
                    <div class="toggle-inner" style="width: 118px; margin-left: 0px;">
                        <div class="toggle-on" style="height: 22px; width: 59px; line-height: 22px;">ON</div>
                        <div class="toggle-blob" style="height: 22px; width: 22px; margin-left: -11px; display: none;"></div>
                        <div class="toggle-off active" style="height: 22px; width: 59px; line-height: 22px;">OFF</div>
                    </div>
                </div>
            </div>--%>
            <div class="onoffswitch">
                <label class="switch-light switch-candy" onclick="" style="width: 10em">
                    <input type="checkbox" id="myonoffswitch" />
                    <span>
                        <span>Recent</span>
                        <span>Archive</span>
                        <a></a>
                    </span>
                </label>
            </div>
        </div>
        <div class="row">
            <div id="airtable">
            </div>
        </div>
        <%--modul--%>
    </div>
    <div class="modal fade" id="myModal" role="dialog" style="display: none;" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog2">
            <!-- Modal content-->
            <div class="modal-content model_font">
                <div class="modal-header header_modal">
                    <button type="button" class="close" data-dismiss="modal">
                        &times;</button>
                    <h4 class="modal-title" style="font-size: 20px">Issue Details</h4>
                    <%--  <img src="Images_air/AIR.png" alt="Site title" class="logo-icon" style="margin-top: 0px; width: 100px;">--%>
                </div>
                <div class="DivPrint">
                    <div class="modal-body" style="overflow: hidden">
                        <div>
                            <label class="abc">
                                Case Number:</label>
                            <label for="CaseNo" id="CaseNo" class="abc" style="margin-right: 10%; font-weight: bold;">
                            </label>
                            <label class="abc">
                                Account:
                            </label>
                            <label for="Accname" class="abc" id="Accname" style="margin-right: 10%; font-weight: bold;">
                            </label>
                            <label class="abc">
                                Issue Type:
                            </label>
                            <label for="accid" id="accid" class="abc" style="font-weight: bold;">
                            </label>
                        </div>
                        <hr />
                        <div id="scrollTopDiv" style="max-height: 500px; overflow: auto;">
                            <div id="textdata" style="display: none">
                                <h5 style="margin-top: 10px;" class="he">
                                    <b>REMINDERS</b></h5>
                                <%-- <ul>
                            <li>Have the ZS-9 e-Prescribing Time Table available to see if ZS-9 should be in the office’s e-prescribing system already
                            </li>
                            <li>Confirm  with the office that ZS-9 5 g and 10 g are still NOT in system (the system may have been updated since the last time the office tried prescribing ZS-9)
                            </li>
                            <li>Ensure the office is looking for a 5 g or 10 g dose (these are the only strengths available). If needed, a dose strength override might be possible in the system.
                            </li>
                        </ul>--%>
                                <p id="rem">
                                </p>
                            </div>
                            <div id="issue">
                                <h5 style="margin-top: 10px;" class="he" id="title_issue"></h5>
                                <div id="issue_put">
                                </div>
                            </div>
                            <div id="actionitem">
                                <h5 style="margin-top: 10px;" class="he">
                                    <b>Recommended Action Item </b>
                                </h5>
                                <div>
                                    <table style="width: 100%;">
                                        <ol>
                                            <tr>
                                                <td style="vertical-align: top;" class="width_cust_table">
                                                    <li></li>
                                                </td>
                                                <td id="uldata">
                                                    <%--<p id="uldata">
                                                    </p>--%>
                                                </td>
                                            </tr>
                                        </ol>
                                    </table>
                                    <%--24-jan-17--%>
                                    <%-- <p id="P1">
                                                    </p>--%>
                                </div>
                            </div>
                            <div id="escalation">
                                <h5 style="margin-top: 10px;" class="he">
                                    <b>Sales Representantive's Input </b>
                                </h5>

                                <div id="els">
                                </div>
                            </div>
                            <div id="dropdown">
                                <div style="padding-top: 15px;">
                                    <span for="ASSINGTO" style="padding-right: 61px;" class="he col_h5">
                                        <b>Assign To:</b></span>
                                    <select id="id_assing" style="width: 30%; color: gray;">
                                    </select>
                                </div>
                                <div>
                                    <span for="Status" style="padding-right: 83px;" class="he col_h5">
                                        <b>Status:</b></span>
                                    <select id="id_status" style="width: 30%">
                                        <option value="Open">Open</option>
                                        <option value="InProgress">In Progress</option>
                                        <option value="Closed">Closed</option>
                                    </select>
                                </div>
                                <div style="padding-top: 10px;">
                                    <span for="datecom" style="padding-right: 20px;" class="he col_h5">
                                        <b>Completed Date:</b></span>
                                    <label for="comdate" id="comdate">
                                    </label>
                                </div>
                            </div>
                            <div class="form-group" style="padding-top: 15px;">
                                <span for="comment" class="he col_h5 ">

                                    <b>Internal Notes</b>
                                    <button type="button" class="btn btn-default " id="add">
                                        Add</button>
                                </span>
                                <br />
                                <div id="int_note">
                                    <input type="text" class="txtbox" id="add_text" style="display: none" />
                                    <img src="Images_air/9cRLBM99i.jpg" class="img_cust" id="img_correct" style="display: none"></img>
                                    <img src="Images_air/x-wrong-no-cross-hi.png" class="img_cust" id="img_worng" style="display: none"></img>
                                </div>
                                <div id="comment" class="myDivClass" style=""></div>
                                <%-- <textarea class="form-control cust_text_area" id="comment" style="min-height: 130px;"></textarea>--%>
                            </div>
                            <div class="form-group" style="padding-top: 15px; padding-bottom: 15px;">
                                <span class="he col_h5">
                                    <b>Notes to Field Representative<b></span>
                                <div id="pick_list" style="padding-bottom: 10px;">
                                    <div>
                                        <select id="pick_list_dropdown" style="width: 60%">
                                        </select>
                                    </div>
                                    <div id="pick_list_select" class="cust_select" style="margin-top: 10px; width: 600px; line-height: 16px;">
                                    </div>
                                </div>
                                <div class="textarea_select" id="message">
                                </div>
                                <%-- <textarea class="form-control" id="message" style="min-height: 130px;"></textarea>--%>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default modal_save" data-dismiss="modal" id="btn_save">
                        Save</button>
                    <button type="button" class="btn btn-default modal_close" data-dismiss="modal" id="btn_close" onclick="$('#scrollTopDiv').scrollTop(0);">
                        Exit</button>
                    <button type="button" onclick="printpotion()" class="btn btn-default modal_save">
                        Save PDF</button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="hnd_caseid" runat="server" clientidmode="Static" />
    <div style="display: none">
        <asp:Button ID="hnd_printclick" OnClick="donwload_print" runat="server" ClientIDMode="Static" />
    </div>
    <input type="hidden" id="username" />
    <input type="hidden" id="type" />
    <input type="hidden" id="airtype" />
    <%--<div class="loader">
        <img src="Images_air/gif-load.gif" />
    </div>--%>
    <div id="loading" class="ajax-loading" style="height: 100%; width: 100%; vertical-align: middle; text-align: center">
        <img src="Images_air/gif-load.gif" style="margin-top: 25%" alt="Loading please wait......" />
    </div>

    <style>

        #select_pic {
        margin-top:0px !important;
        color: #7d808c !important;
        }
        .img_cust {
            width: 15px;
            height: 15px;
        }

        .txtbox {
            width: 550px;
            margin-left: 13px;
        }

        .myDivClass {
            padding-left: 25px;
            font-family: "Roboto", sans-serif, Helvetica, Arial, sans-serif;
            line-height: 15px;
            color: #7d808c !important;
            font-weight: 300 !important;
            margin-top: 10px !important;
        }

        #add {
            font-size: 12px !important;
            margin-left: 10px !important;
        }

        .col_h5 {
            color: #152559;
            margin: 0 0 10px;
            line-height: normal;
        }

        div.ajax-loading {
            position: fixed;
            top: 0;
            left: 0;
            z-index: 9999999;
            background-color: white;
            opacity: 0.7;
            filter: alpha(opacity=70); /* ie */
            -moz-opacity: 0.7; /* mozilla */
            display: none;
        }

            div.ajax-loading * {
                background-color: white;
                background-position: center center;
                background-repeat: no-repeat;
                opacity: 1;
                filter: alpha(opacity=100); /* ie */
                -moz-opacity: 1; /* mozilla */
            }

        @media screen and (min-width: 480px) {
            body {
                font-family: "Roboto", sans-serif, Helvetica, Arial, sans-serif;
            }
        }
        /*07*/
        /*07*/

        .onoffswitch {
            position: relative;
            width: 90px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            margin-left: 560px;
            padding-top: 5px !important;
            font-size: 15px !important;
        }

        .switch-light > span {
            height: 29px;
        }

        .onoffswitch-checkbox {
            display: none;
        }

        .onoffswitch-label {
            display: block;
            overflow: hidden;
            cursor: pointer;
            /*border: 2px solid #999999;*/
            border-radius: 6px;
        }

        .onoffswitch-inner {
            display: block;
            width: 200%;
            margin-left: -80%;
            transition: margin 0.3s ease-in 0s;
        }

            .onoffswitch-inner:before, .onoffswitch-inner:after {
                display: block;
                float: left;
                width: 50%;
                height: 30px;
                padding: 0;
                line-height: 30px;
                font-size: 14px;
                color: white;
                font-family: Trebuchet, Arial, sans-serif;
                font-weight: bold;
                box-sizing: border-box;
            }

            .onoffswitch-inner:before {
                content: "Current";
                padding-left: 10px;
                background-color: #4f81bd;
                color: #FFFFFF;
            }

            .onoffswitch-inner:after {
                content: "Archive";
                padding-right: 10px;
                background-color: #4f81bd;
                color: #FFFFFF;
                /*text-align: right;*/
            }

        .onoffswitch-switch {
            display: block;
            /*width: 18px;*/
            margin: 6px;
            background: #FFFFFF;
            /*position: absolute;*/
            top: 0;
            bottom: 0;
            right: 56px;
            border: 2px solid #999999;
            border-radius: 20px;
            transition: all 0.3s ease-in 0s;
        }

        .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
            margin-left: 0;
        }

        .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
            right: 0px;
        }
        /*07*/
        .t1 {
            border: 1px solid black;
        }

        .leftside {
            margin-top: 10PX; /*margin-left: 100px;*/
        }

        .headerbar {
            background-color: gray;
            padding-left: 0px;
            padding-right: 0px;
            color: white; /*margin-top: 25px;*/
            padding: 10px;
            border-bottom: 10px solid #66a634; /*nirav*/
        }

        .header_modal {
            background-color: gray;
            text-align: center;
        }

        #baseinfo {
            line-height: 12px;
            font-weight: bold;
        }

        .Que {
            /*font-weight: bold;*/
            font-size: 13px;
            color: #0c0c0c;
            width: 70%;
            vertical-align: top;
            white-space: normal;
        }

        .ans {
            padding-left: 10px;
            width: 30%;
            white-space: normal;
        }

        .he {
            font-size: 15px;
        }

        .abc {
            font-size: 15px;
        }

        .custom {
            margin-top: 27px;
            border: 1px solid rgba(0, 0, 0, 0.48);
        }

        .cust_table .cust_table_header {
            width: 99% !important;
            padding: 0px 14px 3px 0px;
        }

        .cust_table_header {
            font-family: "Roboto", sans-serif, Helvetica, Arial, sans-serif;
        }

        .cust_td {
            text-align: center;
            padding-right: 50px !important;
        }

        .cust_search {
            background-color: white;
            color: black;
            border: 2px solid #dedfe2;
            border-right: 0 !important;
            border-top: 0 !important;
            border-left: 0 !important;
        }

        .textarea {
            border: none !important;
        }

        .dt-buttons {
            padding: 0px !important;
            margin: -60px;
        }

        #tbl_air_filter {
            margin: -31px 15px 0px 0px !important;
            padding: 5px !important;
        }

            #tbl_air_filter > label {
                /*margin: -28px 15px 0px 0px !important;*/
                font-size: 15px;
            }

        .modal-header {
            padding: 0px 0px !important;
        }

            .modal-header .close {
                padding-right: 13px !important;
                color: white !important;
            }

        .cust_btn1 {
            display: inline-block;
            font-weight: 400;
            line-height: 0.7;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .5rem 1rem;
            font-size: 1rem;
            border-radius: .25rem;
            color: #fff;
            background-color: #5cb85c;
            border-color: #5cb85c;
            margin: -56px 0px 10px 265px;
        }

        .btn-info {
            color: #fff;
            background-color: hsla(49, 17%, 16%, 0.53);
            border-color: #5bc0de;
            display: inline-block;
            font-weight: 400;
            line-height: 1.25;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .5rem 1rem;
            font-size: 1rem;
            border-radius: .25rem;
            padding: .25rem .5rem;
            font-size: .875rem;
            border-radius: .2rem;
        }

        .cust_btn {
            color: #fff;
            background-color: #5cb85c;
            border-color: #5cb85c;
            display: inline-block;
            font-weight: 400;
            line-height: 1.25;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .5rem 1rem;
            font-size: 1rem;
            border-radius: .25rem;
            padding: .25rem .5rem;
            font-size: .875rem;
            border-radius: .2rem;
        }

        .cust_select {
            padding-bottom: 0px;
            padding-top: 0px;
            width: 83%;
        }

        .status_text {
            font-family: "Roboto", sans-serif, Helvetica, Arial, sans-serif;
            font-size: 16px;
            font-weight: bold;
            margin-right: 10px;
        }

        .status_circle_text {
            font-family: "Roboto", sans-serif, Helvetica, Arial, sans-serif;
            font-size: 16px;
            color: white;
            display: inline-block;
            min-width: 10px;
            padding: 3px 16px;
            font-size: 12px;
            font-weight: 700;
            line-height: 1;
            color: #000;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            background-color: hsl(0, 0%, 100%);
            border-radius: 3px;
        }

        .closed_text {
            margin-top: 10px;
            position: absolute;
            top: 0px;
            margin-left: 402px;
        }

        .model_font {
            font-family: "Roboto", sans-serif, Helvetica, Arial, sans-serif;
            line-height: 24px;
        }

        .modal_save {
            display: inline-block;
            font-weight: 400;
            line-height: 0.7;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .5rem 1rem;
            font-size: 1rem;
            border-radius: .25rem;
            color: #fff;
            background-color: #5cb85c;
            border-color: #5cb85c;
            margin: 8px 0px 0px -93px;
        }

        .modal_close {
            display: inline-block;
            font-weight: 400;
            line-height: 0.7;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .5rem 1rem;
            font-size: 1rem;
            border-radius: .25rem;
            color: #fff;
            background-color: #d9534f;
            border-color: #d43f3a;
            margin: 8px 0px 0px -93px;
        }

        #logout {
            color: white;
            background-color: rgba(0,0,0,0.3); /* border-color: #5cb85c; */
            width: 85px;
            padding: 7px !important;
            margin-top: 18px;
            border-radius: 7px;
        }

        #tbl_air_info {
            padding-left: 11px;
        }

        .buttons-html5 {
            color: #fff;
            background-color: #286090;
            border-color: #204d74; /*display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;*/
            text-decoration: none;
            display: inline-block;
            font-weight: 400;
            line-height: 0.7;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .5rem 1rem;
            font-size: 1rem;
            border-radius: .25rem;
            color: #fff;
            background-color: #4f81bd;
            border-color: #4f81bd;
            margin-left: 415px;
            margin-top: -126px;
        }

        .lbl {
            display: inline;
            font-size: 75%;
            font-weight: 700;
            line-height: 1;
            color: #fff;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: .25em;
        }

        th, td {
            white-space: nowrap;
        }

        tab_cust {
            text-align: center;
        }
        /*div.dataTables_wrapper {
        width: 800px;
        margin: 0 auto;
    }*/
        /*.dataTables_scrollHeadInner {
            width: 0px !important;
        }*/
        /*.dataTables_scrollHead {
               width: 100% !important;
        }*/
        .table.dataTable tr.odd {
            background-color: #f2f2f2 !important;
        }

        table.dataTable tr.even {
            background-color: #E0DFDD !important;
        }


        table.dataTable thead th, table.dataTable thead td {
            padding: 10px 18px;
            border-bottom: 1px solid #020202;
            border-top: 1px solid #020202;
        }

        /*---*/
        /*.btn-group > .btn:first-child:not(:last-child):not(.dropdown-toggle) {
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }

        .btn-info:hover, .btn-info:focus, .btn-info.focus, .btn-info:active, .btn-info.active, .open > .dropdown-toggle.btn-info {
            color: #fff;
            background-color: #31b0d5;
            border-color: #269abc;
        }

        btn:hover, .btn:focus, .btn.focus {
            color: #333;
            text-decoration: none;
        }

        .btn {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
        }

        button {
            overflow: visible;
        }

        button, select {
            text-transform: none;
        }*/
        /*---*/
        table.dataTable {
            margin: 0px;
        }

        .DTFC_LeftBodyWrapper .dataTables_empty {
            display: none;
        }

        /*.loader {
        display:none;
            position:relative;
        }*/
        .loader {
            display: none;
            /*width:100px;
    height: 100px;*/
            position: fixed;
            top: 50%;
            left: 50%;
            text-align: center;
            margin-left: -50px;
            margin-top: -100px;
            z-index: 2;
            overflow: auto;
        }

        /*.loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #3498db;
            width: 120px;
            height: 120px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }

        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }*/
        table#tbl_air > tbody > tr > td:last-child {
            padding-left: 13px;
            padding-right: 5px;
        }

        .BlankTd {
            width: 10%;
        }

        .firstCol {
            width: 40%;
        }

        .LastCol {
            width: 45%;
        }

        td {
            white-space: nowrap;
        }

        .LeftAlign {
            text-align: left;
        }

        .MinWidth {
            min-width: 190px;
        }

        .minwidth_ass {
            min-width: 120px;
        }

        .modal-dialog {
            background-color: rgb(218, 213, 213) !important;
            color: #181717 !important;
        }

        .bootstrap-dialog-footer-buttons {
            background-color: #808080;
        }

        .bootstrap-dialog.type-primary .modal-header {
            background-color: #808080;
        }

        .modal-footer {
            background-color: #808080 !important;
        }

        .bootstrap-dialog-title {
            padding-left: 12px !important;
        }

        #pick_list_dropdown {
            color: #7d808c !important;
        }

        .cust_select_a {
            /*padding: 3px;*/
            color: #7d808c !important;
            font-weight: normal !important;
            padding-right: 18px !important;
            margin-left: 21px !important;
            line-height: 12px !important;
        }

        .textarea_select {
                padding-top: 0px !important;
            min-height: 130px;
            display: block;
            /*resize: vertical;*/
            padding: 10px 20px;
            /*border-radius: 10px;
            border: 2px solid #dedfe2;*/
            font-family: "Roboto", sans-serif, Helvetica, Arial, sans-serif;
            font-size: 1em;
            font-weight: 300;
            width: 60%;
            outline: none;
            transition: .3s ease;
            overflow: auto;
            vertical-align: top;
            margin: 0;
            -webkit-appearance: textarea;
            background-color: white;
            -webkit-rtl-ordering: logical;
            user-select: text;
            flex-direction: column;
            cursor: auto;
            white-space: pre-wrap;
            word-wrap: break-word;
            text-rendering: auto;
            color: initial;
            letter-spacing: normal;
            word-spacing: normal;
            text-transform: none;
            text-indent: 0px;
            text-shadow: none;
            text-align: start;
            /*font: 13.3333px Arial;*/
        }

        .cust_text_area {
            resize: none !important;
            border-radius: 0px !important;
            border: none !important;
        }

        .cancelplicklist {
            cursor: pointer;
        }

        .width_cust_table {
            width: 5.3% !important;
        }
    </style>
</asp:Content>
