﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.Services;

public partial class Report : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        HttpCookie groupType = Request.Cookies["GroupType"];
        if (groupType != null)
        {
            if (groupType.Value != null && groupType.Value != "")
            {
                DeGrToolWebService obj = new DeGrToolWebService();
                menu.InnerHtml = obj.GetGroupRights(groupType.Value);

            }
        }

        //Txtfromdate.Attributes.Add("readonly", "readonly");
        //Txttodate.Attributes.Add("readonly", "readonly");
        //Txtweeklyfrom.Attributes.Add("readonly", "readonly");
        //TxtweeklyTo.Attributes.Add("readonly", "readonly");


        
    }

    protected void Txtfromdate_TextChanged(object sender, EventArgs e)
    {
        int val = DrpCycle.SelectedIndex;

        if (val == 1)
        {
            Txttodate.Text = Txtfromdate.Text;

            string str = string.Empty;
            str = "<script type=\"text/JavaScript\" language=\"javascript\">";
            str += "getSumaaryReport();";
            str += "</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "key", str, false);
        }
        else
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "alert", string.Format("alert('{0}');", "Please Select Report Type"), true);     
        }
    }

    protected void Txtweeklyfrom_TextChanged(object sender, EventArgs e)
    {
        
        int val = DrpCycle.SelectedIndex;

        if (val == 2)
        {
            DateTime dt = Convert.ToDateTime(Txtweeklyfrom.Text);

            Txtweeklyfrom.Text = dt.ToString("MM/dd/yyyy");
            TxtweeklyTo.Text = dt.AddDays(+6).ToString("MM/dd/yyyy");

            string str = string.Empty;
            str = "<script type=\"text/JavaScript\" language=\"javascript\">";
            str += "getSumaaryReport();";
            str += "</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "key", str, false);
        }
        else
        {
            ScriptManager.RegisterStartupScript(Page, this.GetType(), "alert", string.Format("alert('{0}');", "Please Select Report Type"), true);
        }
    }

    protected void DrpCycle_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (DrpCycle.SelectedValue == "1")
        {
           
            daily.Style.Add("display", "block");
            weekly.Style.Add("display", "none");
            Txtfromdate.Text = System.DateTime.Now.Date.ToString("MM/dd/yyyy");
            Txttodate.Text = System.DateTime.Now.Date.ToString("MM/dd/yyyy") ;
            Txtweeklyfrom.Text = "";
            TxtweeklyTo.Text = "";
        }
        else if (DrpCycle.SelectedValue == "2")
        {
            daily.Style.Add("display", "none");
            weekly.Style.Add("display", "block");

            //add

            Txtweeklyfrom.Text = System.DateTime.Now.Date.AddDays(-6).ToString("MM/dd/yyyy") ;
            TxtweeklyTo.Text = System.DateTime.Now.Date.ToString("MM/dd/yyyy") ;
            Txtfromdate.Text = "";
            Txttodate.Text = "";
        }
        else
        {

            Txtweeklyfrom.Text = "";
            TxtweeklyTo.Text = "";
            Txtfromdate.Text="";
            Txttodate.Text = "";
        }
        string str = string.Empty;
        str = "<script type=\"text/JavaScript\" language=\"javascript\">";
        str += "getSumaaryReport();";
        str += "</script>";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "key", str, false);
       
    }

    
}