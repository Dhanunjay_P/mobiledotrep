﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Roster.aspx.cs" Inherits="Roster" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Roster</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link href="Roster/CSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <link href="Roster/CSS/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i" rel="stylesheet" />
    <link href="Roster/CSS/style.css" rel="stylesheet" />
    <link href="Roster/CSS/font-awesome.min.css" rel="stylesheet" />
    
    <link href="ClientScripts/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/jquery-1.12.4.js"  type="text/javascript"></script>    
    <script src="ClientScripts/jquery-ui.1.9.1min.js" type="text/javascript"></script>
    <%--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>--%>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="Roster/Scripts/bootstrap-multiselect.js"></script>
    <script type="text/javascript" src="Roster/Scripts/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="Roster/Scripts/roster.js"></script>
     <link href="Scripts/jquery.datetimepicker.css" rel="stylesheet" /> 
    <script src="Scripts/jquery.datetimepicker.js"></script>
    <script src="Scripts/fieldChooser.js"></script> 
    <style>
        option {
            font-size: 14px !important;
            font-style: normal !important;
            opacity: 1;
        }

            option:first-child {
                font-size: 13px !important;
                font-style: italic !important;
            }

        .Editfield {
            display: inline;
            cursor: pointer;
            color: #1e88e5;
        }

        .moreinfo {
            display: none;
        }

        .modal-body {
            max-height: 500px;
            overflow: auto;
        }

        div.ajax-loading {
            position: fixed;
            top: 0;
            left: 0;
            z-index: 9999999;
            background-color: white;
            opacity: 0.7;
            filter: alpha(opacity=70); /* ie */
            -moz-opacity: 0.7; /* mozilla */
            /*display: none;*/
        }

            div.ajax-loading * {
                background-color: white;
                background-position: center center;
                background-repeat: no-repeat;
                opacity: 1;
                filter: alpha(opacity=100); /* ie */
                -moz-opacity: 1; /* mozilla */
            }
        .underlineTable{
            text-decoration: underline;
        }
        .switch {
  position: relative;
  display: inline-block;
  width: 40px;
  height: 20px;
  margin: auto;
    vertical-align: middle;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
    margin: 0 auto;
    vertical-align:middle
}

.slider:before {
  position: absolute;
  content: "";
  height: 16px;
  width: 16px;
  left: 3px;
  bottom: 2px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #5cb85c;
}

input:focus + .slider {
  box-shadow: 0 0 1px #5cb85c;
}

input:checked + .slider:before {
  -webkit-transform: translateX(19px);
  -ms-transform: translateX(19px);
  transform: translateX(19px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
  .fc-field {
            margin: 5px;
            padding-left: 10px;
            background-color: #ffffff;
            box-shadow: 0px 0px 10px 0px rgb(154, 154, 154);
            line-height: 30px;
        }

        .fc-selected {
            box-shadow: 0px 0px 10px 0px #92b3e8; /**rgb(154, 154, 154)*/
            border: 2px solid #92b3e8;
            border-radius: 2px;
        }
           #report_geographyID::-webkit-input-placeholder, #ReportPreviewGeographyFilters::-webkit-input-placeholder {
            font-size: 13px;
            font-style: normal !important;
            opacity: 1;
            color: #333333;
        }
           #Geography_input::-webkit-input-placeholder {
            font-size: 13px;
            font-style: normal !important;
            opacity: 1;
            color: #333333;
        }
           .ReportDateClass {
            color: black;
            float: right;
            cursor: default;
            padding-right: 10px;
            font-size: 15px;
        }
           .ReportDateClass:focus {
                text-decoration: none !important;
                outline: none;
            }

            .ReportDateClass:hover {
                text-decoration: none !important;
                outline: none;
                color: black;
            }
        input.form-control {
        padding-left:15px !important;
        }
        table tbody th, table  tbody td {
    line-height: 15px; /* e.g. change 8x to 4px here */
}
        legend
        {
            background: none repeat scroll 0 0 transparent;
            background-color:#3c5b68;
            border-radius: 0.5em 0.5em 0.5em 0.5em;
            border: 2px solid #3c5b68;
            padding: 5px 100px 5px 10px;
            color: #1d5987 !important;
                width: auto;
    margin-left: 16px;
    background: none !important;
    border: none !important;
    padding: 0px 12px;
    font-size: 17px;
        }
        legend:hover
        {
            background-color:#b2c8db;
            cursor: pointer;
            color:#3c5b68;
        }
        .fieldset {
            border: 2px solid #e5e5e5;
            border-radius: .5em .5em .5em .5em;
            position:relative;
        }
        .right-cornar{    
            background-color: #fff;
     
    border: none;
    position: absolute;
    top: 10px;
    right: 0;
    border-radius: .5em .5em .5em .5em;
    padding: 10px;
    height: 20px;
    padding-top: 0px;
    border-top-left-radius: 0px;
    border-bottom-right-radius: 0px;
    background: #1e88e5;
}
        .logoff{
              padding: 13px;
        font-size: 24px;
        border-radius: 50%;
        background: rgba(0,0,0,0.5);
        margin-top: 10px;
        }
        .columns {
        margin-top:-20px;
        }
        .modal-dialog.modal-lg {
        width : 70%;
        }
        .modal-header {
        background-color:#2a3542;
        color:white;
        text-align:center;
        }
    </style>

</head>

<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
            <Services>
                <asp:ServiceReference Path="~/DeGrToolWebService.asmx" />
            </Services>
        </asp:ScriptManager>

        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
               <%-- <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                     
                    
                </div>--%>
               <%-- <div class="collapse navbar-collapse text-center" id="myNavbar">
                    <ul class="nav navbar-nav navbar-left">
                            <li>
                         <a class="navbar-brand" href="#">
                        <img src="Images_air/NeuroLogo.png" class="logo" /></a></li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li><a href="#">
                                <img src="Roster/Image/logo.png" /></a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#" class="logoff" id="logout" title="Logout"><i class="fa fa-power-off" aria-hidden="true"></i></a></li>
                        </ul>

                    </div>--%>
                <div class="row" style="padding:5px 0px;">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                         <img src="Images_air/NeuroLogo.png" class="logo" /> 
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4" align="center">
                        <img src="Roster/Image/logo.png" /> 
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                       <ul class="nav navbar-nav navbar-right" style="margin:0px">
                        <li><a href="#" class="logoff" id="logout" title="Logout" style="margin:0px"><i class="fa fa-power-off" aria-hidden="true"></i></a></li>
                    </ul>
                    </div>
                </div>
            </div>
        </nav>

        <div class="container-fluid">
            <div class="panel panel-default">
                <div class="panel-heading" id="CountingSection">
                    <div class="pull-right">
                        <div id="RosterCounts">
                        <span>Territories: </span><span class="count" id="RosterTerritoryCount"></span>
                        <span>Active: </span><span class="count" id="RosterTerritoryActiveCount"></span>
                        <span>Vacant: </span><span class="count" id="RosterTerritoryVacantCount"></span>
                            </div>
                        <div id="EmployeeCounts" style="display:none">
                        <span>Employees: </span><span class="count" id="EmployeeCount"></span>
                        <span>Active: </span><span class="count" id="ActiveEmployeeCount"></span>
                        <span>Inactive: </span><span class="count" id="InactiveEmployeeCount"></span>
                            </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li id="RosterTab" class="active"><a data-toggle="tab" href="#Roster">ROSTER</a></li>
                        <li id="EmployeeTab"><a data-toggle="tab" href="#Employee">EMPLOYEE</a></li>
                        <li id="ReportTab"><a data-toggle="tab" href="#Report">REPORT</a></li>
                    </ul>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div id="Roster" class="tab-pane fade in active">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <select class="form-control multiselect SalesforceMul" id="select_Salesforce_id">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Geography" value="" maxlength="50" id="Geography_input" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <select class="form-control multiselect roleMul" id="Role_filter_select">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <select class="form-control multiselect terrTypeMul" id="Territory_Type">
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="text-right">
                                            <button type="button" class="btn btn-success btn-spacing" onclick="SearchFoRecord()">Filter</button>
                                            <button type="button" class="btn btn-default" onclick="ClearFiltersRecord()">Clear Filter</button>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="rosterTable" class="table-responsive" style="white-space:nowrap">
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div id="Employee" class="tab-pane fade" style="white-space:nowrap">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="employeeTable" class="table-responsive">
                                        </div>
                                    </div>
                                </div>
                            </div>

                              <div id="Report" class="tab-pane fade">

                               <div class="row">
                    <div class="col-xs-12 col-md-12 col-lg-12" style=" ">
                        <div class="col-lg-3 col-md-3 col-xs-3" style=" padding-top: 1%; padding-bottom: 1%">
                            <div class="col-xs-12 col-md-12 col-lg-12" style="padding-top:30px;padding-bottom:30px">
                                <h4 style="text-align: center;">Predefined Reports</h4>
                            </div>
                            <div class="row" style="border: 1px solid black;margin: 0px;border-radius: 25px;">
                            <div class="col-lg-12 col-md-12 col-xs-12" style="  ">
                                <div style="padding-top: 24px">
                                    <div id="predefienedReports" style="width: 100%; height: 250px; overflow: auto; border: 1px solid">
                                    </div>
                                </div>
                                <h6 style="padding-bottom:30px">Double click to open a predefined report</h6>
                            </div>
                             </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-xs-9" style="padding-top: 1%;">

                            <div class="col-xs-12 col-md-12 col-lg-12" style="padding-bottom: 30px;">
                                <div style="    font-size: 18px;color: #152559;font-family: inherit;font-weight: 500;">
                                    <div class="col-lg-6 col-md-6 col-xs-6" style="text-align:end;padding:0px;margin: 0 0 10px;line-height: normal;padding-top: 30px;">Report:</div>
                                    <div id="RptName" class="col-lg-6 col-md-6 col-xs-6" style="padding:0 5px;color: black;font-weight: normal;margin: 0 0 10px;line-height: normal;padding-top: 30px;">new</div>
                                </div>
                            </div>
                            <div class="row" style="margin:0px;border: 1px solid black;border-radius: 25px;">
                                <div class="col-lg-12 col-md-12">
                                <div id="customreports">
                                    <div id="FieldChooser" class="col-xs-8 col-md-8 col-lg-8">
                                        <div class="col-md-5">
                                            <h6>All fields:</h6>
                                            <div id="TotalFields" style="width: 100%; height: 250px; overflow: auto; border: 1px solid" ondrop="OnDropOnTofiels(this)">
                                            </div>

                                        </div>

                                        <div class="col-md-1" style="padding: 0px">
                                            <div style="height: 250px; padding-top: 85px">
                                                <div style="text-align: center">
                                                    <img id="SelectFieldsToShow" src="Images_air/rightarrow.png" style="height: 50px;" />
                                                </div>
                                                <div style="text-align: center">
                                                    <img id="SelectTotalFields" src="Images_air/leftarrow.png" style="height: 50px" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <h6>Fields in Report:</h6>
                                            <div id="FieldsToShow" style="width: 100%; height: 250px; overflow: auto; border: 1px solid"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-xs-4  form-horizontal ">
                                    <div style="color: #152559">
                                        <h6 style="padding-left: 5px">Filter Criteria:</h6>
                                    </div>
                                    <div class="form-group">
                                          <div class="col-sm-12">
                                        <label style="padding-left: 5px; font-size: 13px; font-weight: 300">Report as of:</label>
                                        <input type="text" class="datepicker" maxlength="50" id="ReportSelectedDate" style="border: none; border-bottom: 1px solid #dedfe2; padding-left: 15px; width: 100px" />
                                              </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12" id="div_report_Salesforce_ID">
                                            <select id="report_Salesforce_ID" class=' filterInput SalesforceMul'>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12" id="div_report_geographyID">
                                            <input id="report_geographyID" maxlength="50" class='filterInput ' title="Use % as wildcard character" placeholder="Geography" style="width: 100%;padding:10px " />
                                        </div>
                                    </div>
                                      <div class="form-group">
                                        <div class="col-sm-12">
                                             <select id="report_role_Name" class='filterInput roleMul '>
                                        </select>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <div class="col-sm-12">
                                             <select id="report_TerritoryTyep" class='filterInput terrTypeMul  '>
                                        </select>
                                        </div>
                                    </div>
                                   
                                </div>
                                    </div>
                                <div class="col-lg-12 col-md-12">
                                     <div><h6 class="col-lg-6 col-md-6" style="padding-bottom:30px">Drag &amp; drop columns for report</h6>
                                    <div class="col-lg-6 col-md-6" style="text-align:right">
                                        <input type="button" class="btn btn-default" value="Reset" onclick="ClearReportSelection()" style="margin-left: 5px;margin-top: 5px;" title="Clear All Selection" />
                                         <input type="button" class="btn btn-success" value="Save" onclick="SaveNewReport()" style="margin-left: 5px; margin-top: 5px;" title="Save Report" />
                                       <input type="button" id="DeleteExistingReport" class="btn btn-danger" value="Delete" onclick="DeleteExistingReport1()" style="margin-left: 5px; margin-top: 5px;border: none;" title="Delete Report" />
                                        <input type="button" class="btn btn-success" id="previewExcelID" value="Preview" style="margin-left: 5px; margin-top: 5px;" />

                                        </div>
                                    </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" id="myModalclose" data-dismiss="modal" onclick="$('#scrollTopDiv').scrollTop(0);">&times;</button>
                        <h4 class="modal-title" id="modal_title"></h4>
                    </div>

                    <div class="modal-body" style="padding:0px;">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="background-color: #f0f3f6;font-size: 16px;">
                                <div class="pull-left">Geography: <span class="value" id="TerritoryId"></span></div>
                                <div class="pull-right">Status: <span class="value" id="Status"></span></div>
                                <div class="text-center">Active Manager: <span class="value" id="TerritoryManager"></span></div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body max-height" id="scrollTopDiv">
                                <div class="row">
                                    <div class="col-md-12">
                                        
                                        <div id="fiedlforceSection" class="section ">

                                            <div class="section-heading" style="display:none">
                                                Fieldforce
                                            <div class="pull-right Editfield" data-tab="myModal" data-modal="salesforceAddModal" data-edit-field="p_salesforceID" data-add-field="salesforceAddModal" data-opmode="EditData" data-message="Salesforce ID" data-modaltitle="Edit Salesforce"><a href="#" class="link"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Edit</a></div>
                                            </div>
                                             <fieldset class="fieldset" style="width: auto;">
                                                <legend class="ui-state-default">Fieldforce</legend>
                                            <div class="right-cornar"><div class="pull-right Editfield" data-tab="myModal" data-modal="salesforceAddModal" data-edit-field="p_salesforceID" data-add-field="salesforceAddModal" data-opmode="EditData" data-message="Salesforce ID" data-modaltitle="Edit Salesforce"><div style="color:white" ><i class=" " aria-hidden="true"></i>&nbsp;Edit</div></div></div>
                                          
                                            <div class="columns" id="primarySalesForceDetail">
                                                <div class="section-content">
                                                    <div class="label">Fieldforce:</div>
                                                    <div class="value" id="p_salesforceID"></div>
                                                </div>
                                            </div>

                                            <div id="moreSalesforceDetails" class="moreinfo">
                                                <div class="section-content">
                                                    <div class="label">Fieldforce Status:</div>
                                                    <div class="value" id="p_salesforce_status"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Fieldforce Start Date:</div>
                                                    <div class="value" id="p_salesforce_startdate"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Fieldforce End Date:</div>
                                                    <div class="value" id="p_salesforce_enddate"></div>
                                                </div>
                                            </div>
                                                 
                                            <div class="text-center">
                                                <a href="javascript:void(0)" class="showdetails" data-detail="moreSalesforceDetails" data-attr="show">Show More</a>
                                            </div>
</fieldset>
                                        </div>

                                        <div id="GeographySection" class="section">

                                            <div class="section-heading" style="display:none">
                                                Geography
                                            <div class="pull-right Editfield" data-tab="myModal" data-edit-field="p_terr" data-add-field="NewTerritoryAdd" data-opmode="EditData" data-message="Territory" data-modaltitle="Edit Geography"><a href="#" class="link"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Edit</a></div>
                                            </div>
 <fieldset class="fieldset" style="width: auto;">
                                                <legend class="ui-state-default">Geography</legend>
                                            <div class="right-cornar"><div class="pull-right Editfield" data-tab="myModal" data-modal="NewTerritoryAdd" data-edit-field="p_terr" data-add-field="NewTerritoryAdd" data-opmode="EditData" data-message="Territory" data-modaltitle="Edit Geography"><div style="color:white" ><i class=" " aria-hidden="true"></i>&nbsp;Edit</div></div></div>
                                          
                                            <div class="columns" id="primarygerographydetails">
                                                <div class="section-content">
                                                    <div class="label">Geography:</div>
                                                    <div class="value" id="p_terr"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Geography Type:</div>
                                                    <div class="value" id="p_terr_type"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Role:</div>
                                                    <div class="value" id="p_role"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Parent Geography:</div>
                                                    <div class="value" id="p_parent_terr"></div>
                                                </div>
                                            </div>
                                            <div id="TerrMoreInfo" class="moreinfo">
                                                <div class="section-content">
                                                    <div class="label">Geography Status:</div>
                                                    <div class="value" id="p_terr_status"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Geography Start Date:</div>
                                                    <div class="value" id="p_terr_start_date"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Geography End Date:</div>
                                                    <div class="value" id="p_terr_end_date"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Default Geography ZIP:</div>
                                                    <div class="value" id="p_terr_default_zip"></div>
                                                </div>
                                            </div>
                                            <div class="text-center"><a href="javascript:void(0)" class="showdetails" data-detail="TerrMoreInfo" data-attr="show">Show More</a></div>
                                       
                                            </fieldset>
                                             </div>

                                        <div id="EmployeeSection" class="section">
                                            <div class="section-heading" style="display:none">
                                                Employee
                                                 <%--<div class="pull-right Editfield" data-tab="MyEmployeeModal" data-modal="empAddModal" data-edit-field="emp_tab_emp_id" data-add-field="empAddModal" data-opmode="EditData" data-message="Employee" data-modaltitle="Edit Employee">
                                            <a href="#" class="link"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Edit</a></div>--%>
                                            </div>

                                            <fieldset class="fieldset" style="width: auto;">
                                                <legend class="ui-state-default">Employee</legend>
                                          
                                            <div class="columns" id="EmployeePrimaryDetails">
                                                <div class="section-content">
                                                    <div class="label">User ID:</div>
                                                    <div class="value" id="p_user_id"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Department:</div>
                                                    <div class="value" id="p_emp_department"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Employee Title:</div>
                                                    <div class="value" id="p_emp_title"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Full name:</div>
                                                    <div class="value" id="p_emp_name"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Preferred Full Name:</div>
                                                    <div class="value" id="p_preferredName"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Manager ID:</div>
                                                    <div class="value" id="p_terr_manager_id"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Manager Name:</div>
                                                    <div class="value" id="p_terr_manager_name"></div>
                                                </div>
                                            </div>
                                            <div id="empDetailInfo" class="moreinfo">
                                                <div class="section-content">
                                                    <div class="label">Position Start Date:</div>
                                                    <div class="value" id="p_emp_position_start_date"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Employee Type:</div>
                                                    <div class="value" id="p_emp_type"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Employee Status:</div>
                                                    <div class="value" id="p_emp_staus"></div>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <a href="javascript:void(0)" class="showdetails" data-detail="empDetailInfo" data-attr="show">Show More</a>
                                            </div>
                                                </fieldset>
                                        </div>
                                        
                                        <div id="TerritoryAssignmentSection" class="section">
                                            <div class="section-heading" style="display:none">
                                                Territory Assignments
                                                 <%--<div class="pull-right Editfield" data-tab="MyEmployeeModal" data-modal="empAddModal" data-edit-field="emp_tab_emp_id" data-add-field="empAddModal" data-opmode="EditData" data-message="Employee" data-modaltitle="Edit Employee">
                                            <a href="#" class="link"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Edit</a></div>--%>
                                            </div>

                                            <fieldset class="fieldset" style="width: auto;">
                                                <legend class="ui-state-default">Territory Assignments</legend>
                                          <div class="columns" style="padding-top: 10px;margin:0 auto; width:95%;padding-bottom:10px;">
                                        <table id="TerritoryEmployeeRelation" style="width: 100%">
                                            <thead>
                                                <tr>

                                                    <th class="underlineTable">Fieldforce
                                                    </th>
                                                    <th class="underlineTable">Employee Name
                                                    </th>
                                                    <th class="underlineTable">Role
                                                    </th>
                                                    <th class="underlineTable">Start Date
                                                    </th>
                                                    <th class="underlineTable">End Date
                                                    </th>
                                                    <th class="underlineTable">Assignment Type
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody id="TerritoryEmployeeRelationBody">
                                            </tbody>
                                        </table>
                                    </div>
                                          
                                                </fieldset>
                                        </div>


                                        <div id="rosterEmployeeChanges" class="section">
                                           
                                                    <div class="section-heading" style="display:none">
                                                        Recent Changes 
                                            
                                                    </div>
                                             <fieldset class="fieldset" style="width: auto;">
                                                <legend class="ui-state-default">Recent Changes</legend>
                                                <div class="columns" style="margin: 0 auto; width: 95%;padding-bottom:10px">
                                                    <div>
                                                           <div style="display: inline-table; width: 12%">
                                                <span style="border-bottom: 1px solid black">Last Edit Date</span>
                                            </div>
                                            <div style="display: inline-table; width: 15%;">
                                                <span style="border-bottom: 1px solid black">Edit Type</span>
                                            </div>
                                            <div style="display: inline-table;width: 20%;"">
                                                <span style="border-bottom: 1px solid black">Change Type</span>
                                            </div>
                                            <div style="display: inline-table;width: 20%;"">
                                                <span style="border-bottom: 1px solid black">Old Value</span>
                                            </div>
                                            <div style="display: inline-table;width: 20%;"">
                                                <span style="border-bottom: 1px solid black">New value</span>
                                            </div>
                                             <div style="display: inline-table;width: 10%;"">
                                                <span style="border-bottom: 1px solid black">Edited By</span>
                                            </div>
                                                    </div>
                                                    <div id="RecentChangeData">
                                                    </div>
                                                </div>
                                        </fieldset>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <%--<button type="button" class="btn btn-default" data-dismiss="modal" onclick="DownloadRosterPDF()">Save As PDF</button>--%>
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="$('#scrollTopDiv').scrollTop(0);">Close</button>
                    </div>

                </div>

            </div>
        </div>

        <input type="hidden" id="username" />
        <input type="hidden" id="hnd_caseid" runat="server" data-clientidmode="Static" />
        <input type="hidden" id="current_emp_salesforce" />
        <input type="hidden" id="current_emp_terr" />
        <input type="hidden" id="current_emp_id" />
        <input type="hidden" id="type" />
        <div id="loading" class="ajax-loading" style="height: 100%; width: 100%; vertical-align: middle; text-align: center">
            <img src="Images_air/gif-load.gif" style="margin-top: 25%" alt="Loading please wait......" />
        </div>
         <input type="hidden" id="hidSalesforce" runat="server" clientidmode="Static" />
         <input type="hidden" id="hidTerritory" runat="server" clientidmode="Static" />
         <input type="hidden" id="hidEmployee" runat="server" clientidmode="Static" />
         <div style="display: none">
        <asp:Button ID="hnd_RosterPrintclick" OnClick="Download_Roster" runat="server" ClientIDMode="Static" />
    </div>

    </form>

    <div id="salesforceAddModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close2" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="salesforceAddModalTitle">Edit Geography</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-sm-5">Fieldforce ID:</label>
                            <div class="col-sm-5">
                                <input class="form-control" id="salesforceid_add_i"  maxlength="50" placeholder="Fieldforce ID" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Fieldforce Name:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="salesforce_name_add"  maxlength="50" placeholder="Fieldforce Name" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Fieldforce Status:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="salesforce_staus_add" maxlength="50"  disabled />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Fieldforce Start Date:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control datepicker" id="salseforce_start_date_add"  maxlength="50"  placeholder="Fieldforce Start Date" onchange="Coparedate(this,'salesforce_end_date_add','salseforce_start_date_add','Field force Start date is less than the field force end date ')" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Fieldforce End Date:</label>
                            <div class="col-sm-5">
                                <input class="form-control datepicker" id="salesforce_end_date_add" maxlength="50"  onchange="Coparedate(this,'salesforce_end_date_add','salseforce_start_date_add','Field force End date is greater than the field force start date ')" placeholder="Fieldforce End Date" />
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="saleforce_save">Confirm</button>
                    <button type="button" class="btn btn-default close2" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div>
    </div>

    <div id="NewTerritoryAdd" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close2" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="NewTerritoryAddTitle">Edit Geography</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div class="form-group" id="terr_salesforece_row">
                            <label class="control-label col-sm-5">Fieldforce:</label>
                            <div class="col-sm-5">
                                <select class="form-control salesforce" id="new_terr_salesforce_id"></select>
                            </div>
                        </div>
                        <div class="form-group" id="terr_terr_id_row">
                            <label class="control-label col-sm-5">Geography ID:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="new_terr_terr_id"  maxlength="50" placeholder="Geography ID" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Geography Name:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="new_terr_terr_name"  maxlength="50" placeholder="Geography Name" />
                            </div>
                        </div>
                        <div class="form-group" id="terr_parent_terr_row">
                            <label class="control-label col-sm-5">Parent Geography:</label>
                            <div class="col-sm-5">
                                <select class="form-control" id="new_terr_parent_territory" >
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Geography Type:</label>
                            <div class="col-sm-5">
                                <select class="form-control geographyType" id="new_terr_terr_type">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Role:</label>
                            <div class="col-sm-5">
                                <select class="form-control TerritoryRole" id="new_terr_role_code">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Geography Status:</label>
                            <div class="col-sm-5">
                             <input class="form-control inputControl statusDrp" id="new_terr_status" maxlength="50" disabled />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 ">Geography Start Date:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control datepicker currentDate"  maxlength="50" id="new_terr_start_date" onchange="Coparedate(this,'new_terr_end_date','new_terr_start_date','Territory start date is less than the territory end date ')" placeholder="Enter Geography Start Date" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5 datepicker ">Geography End Date:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control datepicker"  maxlength="50"  id="new_terr_end_date" onchange="Coparedate(this,'new_terr_end_date','new_terr_start_date','Territory end date is greated than the territory start date ')" placeholder="Enter Geography End Date" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Default Geography ZIP:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="new_terr_default_zip"  maxlength="5" placeholder="Enter Default Geography ZIP" />
                            </div>
                        </div>
                        <div class="form-group" id="terr_zip_latitude_row">
                            <label class="control-label col-sm-5">Geography ZIP latitude:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="new_terr_zip_latitude"  maxlength="50" placeholder="Geography Zip Lantitude" />
                            </div>
                        </div>
                        <div class="form-group" id="terr_zip_longitude_row">
                            <label class="control-label col-sm-5">Geography ZIP longitude:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="new_terr_zip_longitude"  maxlength="50" placeholder="Territory Zip longitude" />
                            </div>
                        </div>
                        <div class="form-group" id="terr_rap_addr_latitude_row">
                            <label class="control-label col-sm-5">Rep Address Latitude:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="new_terr_rep_add_latitude"  maxlength="50" placeholder="Rep Address Latitude" />
                            </div>
                        </div>
                        <div class="form-group" id="terr_rap_addr_longitude_row">
                            <label class="control-label col-sm-5">Rep Address Longitude:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="new_terr_rep_addr_longitude"  maxlength="50" placeholder="Rep Address Longitude" />
                            </div>
                        </div>
                        <div class="form-group"  id="terr_accc_crtd_row">
                            <label class="control-label col-sm-5">Account Centroid Latitude:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="new_terr_acc_crtd_latitude" maxlength="50"  placeholder="Account Centroid Latitude" />
                            </div>
                        </div>
                        <div class="form-group" id="terr_acc_crtd_longitude_row">
                            <label class="control-label col-sm-5">Account Centroid Longitude:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="new_terr_acc_crtd_longitude" maxlength="50"  placeholder="Account Centroid Longitude" />
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="terr_save">Confirm</button>
                    <button type="button" class="btn btn-default close2" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div>
    </div>

    <div id="empAddModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close2" data-dismiss="modal" id="empAddmodalcolse">&times;</button>
                    <h4 class="modal-title" id="empAddModalTitle"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-sm-5">Employee ID:</label>
                            <div class="col-sm-5">
                                <input class="form-control" id="new_emp_id" maxlength="50"  placeholder="Employee ID" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">User ID:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="new_emp_user_id" maxlength="50"  placeholder="User ID" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Department:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="new_emp_department"  maxlength="50" placeholder="Department" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Title:</label>
                            <div class="col-sm-5">
                                <select class="form-control" id="new_emp_title"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">First Name:</label>
                            <div class="col-sm-5">
                                <input class="form-control " id="new_emp_first_name" maxlength="50"  placeholder="First Name" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Middle Name:</label>
                            <div class="col-sm-5">
                                <input class="form-control " id="new_emp_middle_name" maxlength="50"  placeholder="Middle Name" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Last Name:</label>
                            <div class="col-sm-5">
                                <input class="form-control " id="new_emp_last_name" maxlength="50"  placeholder="Last Name" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Suffix:</label>
                            <div class="col-sm-5">
                                <input class="form-control " id="new_emp_suffix" maxlength="50"  placeholder="Suffix" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Preferred Name:</label>
                            <div class="col-sm-5">
                                <input class="form-control " id="new_emp_preferred_name" maxlength="50"  placeholder="Prefferd Name" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-5">Hire Date:</label>
                            <div class="col-sm-5">
                                <input class="form-control datepicker" id="new_emp_hire_date" maxlength="50"  onchange="CompareHireDate(this);" placeholder="Hire Date" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-5">Field Start Date</label>
                            <div class="col-sm-5">
                                <input class="form-control datepicker" id="new_emp_field_start_date" maxlength="50"  onchange="ConpareEmpfieldStartDate(this)" placeholder="Field Start Date" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Termination Date:</label>
                            <div class="col-sm-5">
                                <input class="form-control datepicker" id="new_emp_termination_date" maxlength="50"  onchange="CompareEmpTerminationDate(this);" placeholder="Termination Date" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Employee Type:</label>
                            <div class="col-sm-5">
                                <select class="form-control " id="new_emp_type"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Employee Status:</label>
                            <div class="col-sm-5">
                                <input class="form-control " id="new_emp_status" maxlength="50"  placeholder="Employee Status" disabled />
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="employee_save">Confirm</button>
                    <button type="button" class="btn btn-default close2" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div>
    </div>

    <div id="HistoryShow" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close2" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="HistoryShowTitle"></h4>
                </div>
                <div class="modal-body" id="HistoryShowBody">
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default close2" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <div id="MyEmployeeModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" id="myEmployeeModalClose" data-dismiss="modal" onclick="$('#employeeScroll').scrollTop(0);">&times;</button>
                    <h4 class="modal-title" id="MyEmployeeModalTitle">Employee Details </h4>
                </div>

                <div class="modal-body">
                    <div class="panel panel-default">
                        <div class="panel-body max-height" id="employeeScroll">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="EmployeeTabEmployeeSaction" class="section">
                                        <div id="EmployeeData" class="section">

                                            <div class="section-heading" style="display:none">
                                                Employee
                                            <div class="pull-right Editfield" data-tab="MyEmployeeModal" data-modal="empAddModal" data-edit-field="emp_tab_emp_id" data-add-field="empAddModal" data-opmode="EditData" data-message="Employee" data-modaltitle="Edit Employee"><a href="#" class="link"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Edit</a></div>
                                            </div>
                                            <fieldset  class="fieldset" style="width: auto;">
                                                <legend  class="ui-state-default">Employee</legend>
                                                <div class="right-cornar"><div class="pull-right Editfield" data-tab="MyEmployeeModal" data-modal="empAddModal" data-edit-field="emp_tab_emp_id" data-add-field="empAddModal" data-opmode="EditData" data-message="Employee" data-modaltitle="Edit Employee"><div style="color:white" ><i class=" " aria-hidden="true"></i>&nbsp;Edit</div></div></div>
                                                <div>
                                            <div class="columns" id="EmpTabEmpPrimarydata">
                                                <div class="section-content">
                                                    <div class="label">User ID:</div>
                                                    <div class="value" id="emp_tab_emp_user_id"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Department:</div>
                                                    <div class="value" id="emp_tab_emp_department"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Title:</div>
                                                    <div class="value" id="emp_tab_emp_title"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Full Name:</div>
                                                    <div class="value" id="emp_tab_emp_Name"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Preferred Full Name:</div>
                                                    <div class="value" id="emp_tab_emp_Prefferd_name"></div>
                                                </div>

                                            </div>
                                            <div id="EmpTabEmpMoreDetails" class="moreinfo">
                                                <div class="section-content">
                                                    <div class="label">Employee Type:</div>
                                                    <div class="value" id="emp_tab_emp_type"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Employee Status:</div>
                                                    <div class="value" id="emp_tab_emp_status"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Hire Date:</div>
                                                    <div class="value" id="emp_tab_emp_hire_date"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Field Start Date:</div>
                                                    <div class="value" id="emp_tab_emp_field_Start_Date"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Termination Date:</div>
                                                    <div class="value" id="emp_tab_emp_Termination_Date"></div>
                                                </div>

                                            </div>
                                            <div class="text-center">
                                                <a href="javascript:void(0)" class="showdetails" data-detail="EmpTabEmpMoreDetails" data-attr="show">Show More</a>
                                            </div>
                                                </div>
                                            </fieldset>
                                            
                                        </div>
                                        <div id="AddressSection" class="section">
                                            <div class="section-heading" style="display:none">
                                                Address
                                            </div>
                                            <fieldset class="fieldset" style="width: auto;">
                                                <legend class="ui-state-default">Address</legend>
                                            
                                            <div class="columns" id="EmpTabAddressDetails">
                                                <div class="section-content">
                                                    <div class="label">Mailing Address:</div>
                                                    <div class="value" id="emp_tab_emp_home_address"></div>
                                                    <div class="Editfield" data-tab="MyEmployeeModal" style="float: right; padding: 5px" data-modal="HomeAddrEditModal" data-edit-field="emp_tab_emp_home_address" data-add-field="HomeAddrEditModal" data-opmode="EditData" data-message="Territory" data-modaltitle="Edit Mailing Address"><a href="#" class="link"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Edit</a></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Shipping Address:</div>
                                                    <div class="value" id="emp_tab_emp_mail_address"></div>
                                                    <div class="Editfield" data-tab="MyEmployeeModal" style="float: right; padding: 5px" data-modal="mailAddrEditModal" data-edit-field="emp_tab_emp_mail_address" data-add-field="mailAddrEditModal" data-opmode="EditData" data-message="Territory" data-modaltitle="Edit Shiping Address"><a href="#" class="link"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Edit</a></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Sample Shipment Locker:</div>
                                                    <div class="value" id="emp_tab_emp_shiping_address"></div>
                                                    <div class="Editfield" data-tab="MyEmployeeModal" style="float: right; padding: 5px" data-modal="ShipingAddrEditModal" data-edit-field="emp_tab_emp_shiping_address" data-add-field="ShipingAddrEditModal" data-opmode="EditData" data-message="Territory" data-modaltitle="Edit Sample Shipment Locker"><a href="#" class="link"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Edit</a></div>
                                                </div>

                                            </div>
</fieldset>
                                        </div>

                                        <div id="EmpployeeEmailDetails" class="section">
                                            <div class="section-heading" style ="display:none">
                                                Email
                                                 <div class="pull-right Editfield" data-tab="MyEmployeeModal" data-modal="emailEditModal" data-edit-field="emp_tab_company_email" data-add-field="emailEditModal" data-opmode="EditData" data-message="Employee" data-modaltitle="Edit Emails">
                                                     <a href="#" class="link"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Edit</a>
                                                 </div>
                                            </div>
                                            <fieldset class="fieldset" style="width: auto;">
                                                <legend class="ui-state-default">Email</legend>
                                            <div class="right-cornar"><div class="pull-right Editfield" data-tab="MyEmployeeModal" data-modal="emailEditModal" data-edit-field="emp_tab_company_email" data-add-field="emailEditModal" data-opmode="EditData" data-message="Employee" data-modaltitle="Edit Emails"><div style="color:white" ><i class=" " aria-hidden="true"></i>&nbsp;Edit</div></div></div>
                                            <div class="columns" id="EmpTabEmailDetails">
                                                <div class="section-content">
                                                    <div class="label">Company Email:</div>
                                                    <div class="value" id="emp_tab_company_email"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Personal Email:</div>
                                                    <div class="value" id="emp_tab_personal_email"></div>
                                                </div>
                                            </div>
                                                </fieldset>
                                        </div>

                                        <div id="EmployeePhoneDetails" class="section">
                                            <div class="section-heading" style="display:none">
                                                Phone
                                                 <div class="pull-right Editfield" data-tab="MyEmployeeModal" data-modal="phoneEditModal" data-edit-field="emp_tab_bussiness_phone" data-add-field="phoneEditModal" data-opmode="EditData" data-message="Employee" data-modaltitle="Edit Phone Numbers">
                                                     <a href="#" class="link"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Edit</a>
                                                 </div>
                                            </div>
                                             <fieldset class="fieldset" style="width: auto;">
                                                <legend class="ui-state-default">Phone</legend>
                                            <div class="right-cornar"><div class="pull-right Editfield" data-tab="MyEmployeeModal" data-modal="phoneEditModal" data-edit-field="emp_tab_bussiness_phone" data-add-field="phoneEditModal" data-opmode="EditData" data-message="Employee" data-modaltitle="Edit Phone Numbers"><div style="color:white" ><i class=" " aria-hidden="true"></i>&nbsp;Edit</div></div></div>
                                          
                                            <div class="columns">

                                                <div class="section-content">
                                                    <div class="label">Business Phone:</div>
                                                    <div class="value" id="emp_tab_bussiness_phone"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Mobile Phone:</div>
                                                    <div class="value" id="emp_tab_mobile_phone"></div>
                                                </div>
                                                <div class="section-content">
                                                    <div class="label">Home Phone:</div>
                                                    <div class="value" id="emp_tab_emp_home_phone"></div>
                                                </div>
                                            </div>
                                                 </fieldset>
                                        </div>

                                    </div>
                                    <div id="employeeAssignment" class="section">
                                         <div class="section-heading" style="display:none">
                                                Employee Assignment
                                                 <div class="pull-right Editfield" data-tab="MyEmployeeModal" data-modal="AddEmployeeAssignModal" data-edit-field="emp_assign_new" data-add-field="AddEmployeeAssignModal" data-opmode="EditData" data-message="Employee" data-modaltitle="Employee Assignment">
                                                     <a href="#" class="link"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Add</a>
                                                 </div>
                                            </div>
                                         <div>
                                             <fieldset class="fieldset" style="width: auto;">
                                                <legend class="ui-state-default">Employee Assignment</legend>
                                            <div class="right-cornar"><div class="pull-right Editfield" data-tab="MyEmployeeModal" data-modal="AddEmployeeAssignModal" data-edit-field="emp_assign_new" data-add-field="AddEmployeeAssignModal" data-opmode="EditData" data-message="Employee" data-modaltitle="Employee Assignment"><div style="color:white" ><i class=" " aria-hidden="true"></i>&nbsp;Add</div></div></div>
                                          
                                    <div class="columns" style="padding-top: 10px;margin:0 auto; width:95%;padding-bottom:10px;">
                                        <table id="EmployeeAssigmentDataTable" style="width: 100%">
                                            <thead>
                                                <tr>

                                                    <th class="underlineTable">Fieldforce
                                                    </th>
                                                    <th class="underlineTable">Geography
                                                    </th>
                                                    <th class="underlineTable">Role
                                                    </th>
                                                    <th class="underlineTable">Start Date
                                                    </th>
                                                    <th class="underlineTable">End Date
                                                    </th>
                                                    <th class="underlineTable">Assignment Type
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody id="EmpassignTableBody">
                                            </tbody>
                                        </table>
                                    </div>
                                                 </fieldset>
                                </div>
                                    </div>

                                    <div class="section">
                                        <div class="section-heading" style="display:none">
                                            Recent Changes 
                                        </div>
                                             <fieldset class="fieldset" style="width: auto;">
                                                <legend class="ui-state-default">Recent Changes</legend>
                                                 <div style="padding-bottom:20px;margin:0 auto;width:95%">
                                        <div class="columns">
                                            <div style="display: inline-table; width: 12%">
                                                <span style="border-bottom: 1px solid black">Last Edit Date</span>
                                            </div>
                                            <div style="display: inline-table; width: 15%;">
                                                <span style="border-bottom: 1px solid black">Edit Type</span>
                                            </div>
                                            <div style="display: inline-table;width: 20%;"">
                                                <span style="border-bottom: 1px solid black">Change Type</span>
                                            </div>
                                            <div style="display: inline-table;width: 20%;"">
                                                <span style="border-bottom: 1px solid black">Old Value</span>
                                            </div>
                                            <div style="display: inline-table;width: 20%;"">
                                                <span style="border-bottom: 1px solid black">New value</span>
                                            </div>
                                             <div style="display: inline-table;width: 10%;"">
                                                <span style="border-bottom: 1px solid black">Edited By</span>
                                            </div>
                                        </div>
                                        <div id="EmployeeRecentChangeData">
                                        </div>
                                                     </div>
                                                 </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="$('#employeeScroll').scrollTop(0);">Close</button>
                </div>

            </div>

        </div>
    </div>

    <div id="HomeAddrEditModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close2" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="HomeAddrEditModalTitle"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-sm-5">Address:</label>
                            <div class="col-sm-5">
                                <input class="form-control" id="new_emp_home_addr1" maxlength="400" placeholder="Address" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Address 2:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="new_emp_home_addr2" maxlength="400" placeholder="Address 2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">City:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control " id="emp_home_city" maxlength="50"  placeholder="City" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">State:</label>
                            <div class="col-sm-5">
                                <select class="form-control stateMst" id="emp_home_state"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">ZIP:</label>
                            <div class="col-sm-5">
                                <input class="form-control " id="emp_home_zip" maxlength="5"  oninput="this.value=this.value.replace(/[^0-9]/g,'');" placeholder="ZIP" />
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="home_add_save">Confirm</button>
                    <button type="button" class="btn btn-default close2" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div>
    </div>

    <div id="mailAddrEditModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close2" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="mailAddrEditModalTitle"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-sm-5">Same As Maling Address:</label>
                            <div class="col-sm-5">
                                <input type="checkbox" id="sameAsHome" class=" " />

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Address:</label>
                            <div class="col-sm-5">
                                <input class="form-control" id="new_emp_mail_addr1" maxlength="400"  placeholder="Address" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Address 2:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="new_emp_mail_addr2" maxlength="400" placeholder="Address 2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">City:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control " id="new_emp_mial_city" maxlength="50" placeholder="City" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">State:</label>
                            <div class="col-sm-5">
                                <select class="form-control stateMst" id="new_emp_mailing_state"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">ZIP:</label>
                            <div class="col-sm-5">
                                <input class="form-control " id="new_emp_mailing_zip" maxlength="5"  oninput="this.value=this.value.replace(/[^0-9]/g,'');" placeholder="ZIP" />
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="m_address_save">Confirm</button>
                    <button type="button" class="btn btn-default close2" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div>
    </div>

    <div id="ShipingAddrEditModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close2" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="ShipingAddrEditModalTitle"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-sm-5">Same As Maling Address:</label>
                            <div class="col-sm-5">
                                <input type="checkbox" id="SameAsMaling" class=" " />

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Address:</label>
                            <div class="col-sm-5">
                                <input class="form-control" id="new_emp_shipping_addr1" maxlength="400" placeholder="Address" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Address 2:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="new_emp_shipping_addr2" maxlength="400" placeholder="Address 2" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">City:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control " id="new_emp_shipping_city" maxlength="50" placeholder="City" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">State:</label>
                            <div class="col-sm-5">
                                <select class="form-control stateMst" id="new_emp_shipping_state"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">ZIP:</label>
                            <div class="col-sm-5">
                                <input class="form-control " id="new_emp_shippin_zip" maxlength="5"  oninput="this.value=this.value.replace(/[^0-9]/g,'');" placeholder="ZIP" />
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="m_shippin_address_save">Confirm</button>
                    <button type="button" class="btn btn-default close2" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div>
    </div>

    <div id="emailEditModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close2" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="emailEditModalTitle"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-sm-5">Company Email:</label>
                            <div class="col-sm-5">
                                <input class="form-control" id="new_emp_company_email"  maxlength="50" placeholder="Company Email" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Personal Email:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="new_emp_personal_emial" maxlength="50"  placeholder="Personal Email" />
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="email_save">Confirm</button>
                    <button type="button" class="btn btn-default close2" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div>
    </div>

    <div id="phoneEditModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close2" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="phoneEditModalTitle"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-sm-5">Business Phone:</label>
                            <div class="col-sm-5">
                                <input class="form-control phoneMask" id="new_emp_bussiness_phone" maxlength="50"  placeholder="Bussiness Phone" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Mobile Phone:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control phoneMask" id="new_emp_mobile_phone" maxlength="50"  placeholder="Mobile Phone" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Home Phone:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control phoneMask" id="new_emp_home_phone"  maxlength="50" placeholder="Home Phone" />
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="phone_save">Confirm</button>
                    <button type="button" class="btn btn-default close2" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div>
    </div>

    <div id="AddEmployeeAssignModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close2" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="AddEmployeeAssignModalTitle">Employee Details</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-sm-5">Fieldforce:</label>
                            <div class="col-sm-5">
                                
                                <select id="emp_assign_salesforce" class='form-control salesforce'>
                                    </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Geography:</label>
                            <div class="col-sm-5">
                                <select id="emp_assign_territory" class=' form-control'>
                                    </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Status:</label>
                            <div class="col-sm-5">
                              <select id="emp_assign_emp_terr_Status" class=' form-control'>
                                    </select>
                            </div>
                        </div>
                          <div class="form-group">
                            <label class="control-label col-sm-5">Start Date:</label>
                            <div class="col-sm-5">
                                <input class="form-control datepicker" id="emp_assign_Start_date"  maxlength="50"  onchange="Coparedate(this, 'emp_assign_end_date', 'emp_assign_Start_date', 'Employee position start date should be less than position end date. ', 'new_emp_status');" placeholder="Position Start Date" required  />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">End Date:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control datepicker" id="emp_assign_end_date"  maxlength="50"  onchange="Coparedate(this, 'emp_assign_end_date', 'emp_assign_Start_date', 'Employee position start date should be less than position end date. ', 'new_emp_status');" placeholder="Position End Date"/>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="emp_assign">Confirm</button>
                    <button type="button" class="btn btn-default close2" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div>
    </div>

    <div id="EmployeRemoveModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close2" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="EmployeRemoveModalTitle">Remove Employee</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div style="padding-bottom:15px">
                                <span id="removeEmployeeMsg"></span>
                            </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5">Position End Date:</label>
                            <div class="col-sm-5">
                                
                                <input id="EmployeeRemoveDate" class='form-control datepicker'  maxlength="50" placeholder="Position End Date" />
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="remove_employee">Confirm</button>
                    <button type="button" class="btn btn-default close2" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div>
    </div>
    
    <div class="modal fade" id="ExcelPreview" role="dialog" style="display: none;">
        <div class="modal-dialog modal-lg" style="width:80%">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="ExcelPreviewClose" data-dismiss="modal">
                        &times;</button>
                    <h4 id="ExcelPreviewTitle" class="modal-title" style="font-size: 20px; font-weight: 300;">Report Preview</h4>
                </div>
                <div class="modal-body" style="white-space:nowrap">
                    <div class=" ">

                        <div id="reportTableSearch"></div>
                    </div>
                    <div>
                        <div id="reportPreviewp">
                            <div id="ReportAppileFilters">
                            </div>
                            <div id="ExelPreview">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default close2" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="Confirmation" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close2" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="ConfirmationTitle">Reprot Name</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                         
                        <div class="form-group">
                            <label class="control-label col-sm-5">Report Name:</label>
                            <div class="col-sm-5">
                                
                                <input id="NewReportName" class='form-control'  maxlength="50" placeholder="Report Name" />
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                     <button type="button" class="btn btn-success" data-dismiss="modal" id="NewReportSave" onclick="SaveNewReportInDb()">
                        Confirm</button>
                    
                    <button type="button" class="btn btn-default close2" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div>
    </div>

    <%--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>
    <script src="https://cdn.datatables.net/fixedcolumns/3.2.2/js/dataTables.fixedColumns.min.js"></script>
    <script src=" https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.9/jquery.mask.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.7/js/bootstrap-dialog.min.js"></script>
    
</body>
</html>
